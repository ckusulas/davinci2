<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_materiales extends CI_Controller {



	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
        $this->load->library('session');

    }
 
	public function index()
	{	

    if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }

         $programaSelected = $this->session->userdata('ProgramasSelecc');
     

		 $listMunicipios = $this->ffinanciamiento_model->getMunicipios();
        $listComites = $this->ffinanciamiento_model->getListComites($programaSelected);

        $data = array(     
          
            'listMunicipios' => $listMunicipios,
            'listComites' => $listComites,

        );

		$this->load->view('registro_materiales',$data);
	}


    public function guardarMaterialDifusion(){

        $catalogoMaterial         = $this->input->post('catalogoMaterial');
        $archivoMaterial        = $this->input->post('archivoMaterial');
        $cantidadProducida      = $this->input->post('cantidadProducida');
        $tipoMaterial           = $this->input->post('tipoMaterial');
        $entidadFederativa      = "Querétaro";

        $id_material =  $this->input->post('id_material');  
        $metodo =  $this->input->post('metodo');  
    

     /*   echo "catalogoMaterial: " . $catalogoMaterial . "<br>";
        echo "archivoMaterial: " . $archivoMaterial . "<br>";
        echo "cantidadProducida: " . $cantidadProducida . "<br>";
        echo "entidadFederativa: " . $entidadFederativa . "<br>";
        echo "tipoMaterial: " . $tipoMaterial . "<br>";*/

 


        $fk_program =  $this->session->userdata('ProgramasSelecc');
        $fk_departamento =  $this->session->userdata('departamento');
     
 

        $this->db->set('fk_nombre_material', $catalogoMaterial);
        $this->db->set('archivo_material', $archivoMaterial);
        $this->db->set('cantidad_producida', $cantidadProducida);
        $this->db->set('fk_programa', $fk_program);
        $this->db->set('fk_departamento', $fk_departamento);
        $this->db->set('fk_tipo_material', $tipoMaterial);
        $this->db->set('entidad', "Querétaro");


      //INSERT FF
        if($id_material=="" && $metodo!="edit"){
            //echo "insertando";
            $this->db->insert('cs_registro_material');
            $lastId = $this->db->insert_id();
        }
        else{
            //echo "actualizando";
            $metodo = "edit";
             $this->db->where('id', $id_material);      
             $this->db->update('cs_registro_material');
            $lastId = $id_material;
        }
 

 
 

    }

      public function changeFormatDateSpEng($data){

        $arrayDate = explode("/", $data);
        $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
        return($newData);
    }


    public function guardarMaterialDistribucion(){

        $programa           = $this->input->post('programa');
        $nombreMaterial     = $this->input->post('nombreMaterial');
        $tipoMaterial       = $this->input->post('tipoMaterial');
        $municipio          = $this->input->post('municipio');
        $localidad          = $this->input->post('localidad');
        $cantidadDistribuir = $this->input->post('cantidadDistribuir');
        $fechaDistribuicion = $this->input->post('fechaDistribuicion');

         if($fechaDistribuicion!="")
            $fechaDistribuicion = $this->changeFormatDateSpEng($fechaDistribuicion);


          echo "programa: " . $programa . "<br>";
          echo "nombreMaterial: " . $nombreMaterial . "<br>";
          echo "tipoMaterial: " . $tipoMaterial . "<br>";
          echo "municipio: " . $municipio . "<br>";
          echo "localidad: " . $localidad . "<br>";
          echo "cantidadDistribuir: " . $cantidadDistribuir . "<br>";
          echo "fechaDistribuicion: " . $fechaDistribuicion . "<br>";

    }


     public function populate_material($findMaterial) {
        
        $listMaterial = $this->ffinanciamiento_model->findCatalogoMaterial($findMaterial);
        echo json_encode($listMaterial);
    }


    public function getMaterial($idMaterial){


     //   echo "buscando:" . $idMaterial . "<br>";


 
          //select campo as material, cantidad_producida from cs_registro_material material left join cs_catalogo_general cat on(material.nombre_material = cat.id)

          //select *, campo from cs_registro_material as material left join  cs_catalogo_general as catalogo on (fk_nombre_material = catalogo.id) where material.id=12;


          $this->db->select("cat.campo, material.cantidad_producida");
          $this->db->from("cs_registro_material material");
          $this->db->join("cs_catalogo_general cat", 'material.fk_nombre_material = cat.id', 'left');
          $this->db->where("material.id", $idMaterial);
          $query = $this->db->get();

          //echo "sql:" . $this->db->last_query();

          $info_material =  $query->result_array();

         // echo print_r($info_material);

      
 
           

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "ok";
        }


          $datos = array(   
            'nombre' => $info_material[0]['campo'],
            'cantidad' => $info_material[0]['cantidad_producida'],
        );


        echo json_encode($datos);
    }


}
