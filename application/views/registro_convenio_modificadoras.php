<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>

       <?php $this->view('template/header.php'); ?>   
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
</head>

<body>

    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                           <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                           <h1>
                                               
                                            </h1>
                                         </td>
                                        <td align="right"> 

                                             <button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='cleanElement()'><i class="fa fa-floppy-o"></i> Nuevo</button> 
                                          <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='saveElement()'><i class="fa fa-floppy-o"></i> Editar</button> 
                                          

                                            <button type="button" class="btn btn-success waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Convenio Modificadoras</i>
                                </li>
                                <li class="active">Registro Convenio Modificadoras</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
                
                
                 
                

                    




                 <!-- Funcion del Comite -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Convenio Modificatorio</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#funciones"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="funciones" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_material_distribuir">
                                                


                                                

                                                
                                                <div class="form-group">
                                                     <label for="textInput" class="col-sm-3  ">Número de Obra:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="numDocumento" name="numDocumento"  >
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Nombre de Obra:</label>
                                                    <div class="col-sm-3" ">
                                                                  
                                                        <select id="listObras"   id="accionConexion" name="accionConexion[]" class="form-control" style="text-overflow:ellipsis">
                                                       
                                                            
                                                            <?php 
                                                            if(strlen($editData['accion_conexion'])>0){
                                                                $listA = explode(",",$editData['accion_conexion']);
                                                            }


                                                                  




                                                            if(count($listObras)>0){ 
                                                            echo "<optgroup label='Obras'>";

                                                                
                                                               
                                                                foreach($listObras as $row){
                                                                    $selected = "";
                                                                    if(in_array($row['id'],$listA)){
                                                                        $selected = "selected";

                                                                    }

                                                                    $title = $row["nombre_proyecto"];
                                                                    $subtitle = substr($title, 0,112);

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."...</option>";
                                                                }

                                                                echo "</optgroup>";
                                                            }
                                                            ?>

                                                            <?php 
                                                            if(count($listApoyos)>0){ 
                                                            echo "<optgroup label='Apoyos'>";
                                                                 
                                                                foreach($listApoyos as $row){
                                                                    $selected = "";
                                                                   if(in_array($row['id'],$listA)){
                                                                        $selected = "selected";

                                                                    }

                                                                   
                                                                    $title = $row["nombre_proyecto"];
                                                                    $subtitle = substr($title, 0,112);

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."...</option>";
                                                                }

                                                                echo "</optgroup>";
                                                            }
                                                            ?>
                                                            
                                                            <?php 
                                                            if(count($listServicios)>0){ 
                                                            echo "<optgroup label='Servicios'>";
                                                                
                                                                foreach($listServicios as $row){
                                                                    $selected = "";
                                                                    if(in_array($row['id'],$listA)){
                                                                        $selected = "selected";

                                                                    }

                                                                   
                                                                    $title = $row["nombre_proyecto"];
                                                                    $subtitle = substr($title, 0,112);

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."...</option>";
                                                                }

                                                                echo "</optgroup>";
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>

                                                   
                                                </div>

                                                
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Documento:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento">
                                                            <option value="">Seleccione..</option>
                                                            <option value="1">Convenio</option>
                                                            <option value="2">Contrato</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                                          <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Número Documento:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="numDocumento" name="numDocumento"  >
                                                    </div>



                                             
                                                    <label for="textInput" class="col-sm-3  ">Fecha Firma:</label>
                                                    <div class="col-sm-3 ">

                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaFirma" name="fechaFirma"  >
                                                    </div>
                                                    </div>
                                              
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-3 ">Adjuntar convenio:</label>
                                                    <div class="col-sm-6">
                                                        <input type="file">
                                                    </div>
                                                </div> 

                                                      <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Modificacion:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento" onChange="selectModulo(this.value)">
                                                            <option value="">Seleccione..</option>
                                                            <option value="fecha">Fecha</option>
                                                            <option value="monto">Monto</option>
                                                            <option value="alcance">Alcance</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                     
                                              <div class="form-group" style="display:none" id="modulo_fecha">
                                                    <label for="textInput" class="col-sm-3  ">Fecha Inicio:</label>
                                                    <div class="col-sm-3" ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaInicio" name="fechaInicio"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Fecha Modificada:</label>
                                                    <div class="col-sm-3 ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaModificada" name="fechaModificada"  >
                                                    </div>
                                                    </div>
                                                </div>


                                          
                                      
                                     
                                           
                                                <div class="form-group" style="display:none" id="modulo_monto">
                                                    <label for="textInput" class="col-sm-3  ">Monto Original:</label>
                                                    <div class="col-sm-3" ">
                                                           <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoOriginal" name="montoOriginal"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Monto Modificado:</label>
                                                    <div class="col-sm-3" ">
                                                            <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoModificado" name="montoModificado"  ></div>
                                                    </div>
                                                </div>

                                                <div style="display:none" id="modulo_alcance">
                                               <div class="form-group" >
                                                    <label for="textInput" class="col-sm-3  ">Alcance Original:</label>
                                                    <div class="col-sm-9" ">
                                                          <textarea   class="form-control" rows="3" id="alcanceOriginal" name="alcanceOriginal">
                                                              
                                                          </textarea>
                                                    </div>

                                                    
                                                </div>

                                                   <div class="form-group">
                                                   

                                                    <label for="textInput" class="col-sm-3  ">Alcance Modificado:</label>
                                                    <div class="col-sm-9" ">
                                                         <textarea   class="form-control" rows="3" id="alcanceModificado" name="alcanceModificado">
                                                               
                                                          </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                                 
                                                 

 
 


                                               
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del Comite -->






                  
 


            
                
             

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>

    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
 

    <script type="text/javascript" src="<?php echo asset_url();?>js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="<?php echo asset_url();?>css/bootstrap-multiselect.css" type="text/css"/>


  

      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   
    


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">

           var v_esquema = 0;

         $(document).ready(function () {


         $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */
 
 

            changeProgram();
            
                $('#upload').on('click', function () {
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'registro_comite/upload_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });



            $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });


            function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");


                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   





             $.datepicker.regional['es'] = {
                  closeText: 'Cerrar',
                  prevText: '<Ant',
                  nextText: 'Sig>',
                  currentText: 'Hoy',
                  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                  monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                  dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                  dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                  weekHeader: 'Sm',
                  dateFormat: 'dd/mm/yy',
                  firstDay: 1,
                  isRTL: false,
                  showMonthAfterYear: false,
                  yearSuffix: ''
                };

                $.datepicker.setDefaults($.datepicker.regional['es']);



            $('#cantidadProducida').numeric();
            $('#cantidadDistribuir').numeric();
            



           


               $( function() {
            $( "#fechaInicio" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaModificada" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaFirma" ).datepicker({ dateFormat: 'dd/mm/yy' });
           
            
          } );



            

          });    


         function selectModulo(valor){
            if(valor == "fecha"){
                $("#modulo_fecha").show("slow");
                 $("#modulo_monto").hide("");
                  $("#modulo_alcance").hide("");

            }

             if(valor == "monto"){
                $("#modulo_monto").show("slow");
                 $("#modulo_fecha").hide("");
                  $("#modulo_alcance").hide("");
            }

             if(valor == "alcance"){
                $("#modulo_alcance").show("slow");
                 $("#modulo_monto").hide("");
                  $("#modulo_fecha").hide("");

            }
         }


        function changeProgram(){


            var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }



               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            recurso = obj.recurso;
                            esquema = obj.esquema;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").fadeIn("slow");



                     if(recurso == 1){
                        $("#patcs_menu").hide();
                         $("#petcs_menu").show("slow");
    $("#documentos_menu").show("slow");

                          $("#seguimiento_menu").hide("slow");
                       

                             $("#reuniones_menu").show("slow");
                           $("#informes_menu").show("slow");
                             $("#formatos_menu").hide("slow");


                    }


                    
                    v_esquema = 0;

                    if(esquema==1){
                             v_esquema = 1;
                    }
                     

                     if(recurso == 2 || v_esquema == 1){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow"); 
    $("#documentos_menu").show("slow");
                        
                        $("#seguimiento_menu").show("slow");  
                      

                         $("#reuniones_menu").hide("slow");
                           $("#informes_menu").hide("slow");
                             $("#formatos_menu").show("slow");
                        v_esquema = 0;

                        
                    }

    
                    }
                });

 
         
               
        }


         function sumaPeople(){
            
            var total = 0;

                 
                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                   //     alert("value:" + this.value);
                        total += parseFloat(this.value);
                    }
                }); 


                //total = parseFloat($("#total_people_womans").val()) + parseFloat($("#total_people_mans").val());

            $("#total_personas").val(total);




        }


        function guardar(){



            $.ajax({
                    url: "<?php echo site_url('registro_materiales_difusion/guardarMaterialDifusion') ?>",
                    type: "POST",
                    data: $('#form_registrar_material_difusion').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


                  $.ajax({
                    url: "<?php echo site_url('registro_materiales_difusion/guardarMaterialDistribucion') ?>",
                    type: "POST",
                    data: $('#form_material_distribuir').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });



            








            swal("Bien hecho!", "El convenio modificador ha sido guardado.", "success");
        }


         $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });


              function resetearForma(){
 
           
           location.href="<?php echo site_url();?>/registro_convenio_modificadoras";
            /*$(".form-control").each(function() {
               $(this).val("");  */
                
   
          
        }

          function cleanElement(){

            swal({
              title: "Estas seguro de Crear nuevo Registro?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: false,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Listo!, Empezamos de nuevo la captura", {
                  icon: "success",
                });
                resetearForma();

              } else {
                swal("Lo dejamos como estaba..");
              }
            });
        }


            function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");


                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   







    </script>

</body>

</html>
