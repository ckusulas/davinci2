<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_informes extends CI_Controller {



	public function __construct() {
        parent::__construct();
        $this->load->model('informes_model');
        $this->load->library('session');

    }
 
	public function index()
	{	

        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }

		$listInformes = $this->informes_model->getListInformes();
        
        // "informes:" . print_r($listInformes);
        $data = array(     
          
            'listInformes' => $listInformes
        );

		$this->load->view('registro_informes',$data);
	}

    


    public function changeFormatDateSpEng($data){

        $arrayDate = explode("/", $data);
        $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
        return($newData);
    }



    function guardarCapturaInforme(){
         
        $nombreComite = $this->input->post('nombreComite');
        $informe = $this->input->post('informe');
        $apartadoInforme = $this->input->post('apartadoInforme');
        $numero = $this->input->post('numero');
        $claveComite = $this->input->post('claveComite');
        $nombreComite2 = $this->input->post('nombreComite2');
        $fechaCaptura = $this->input->post('fechaCaptura');
        $numeroRegistroCCS = $this->input->post('numeroRegistroCCS');
        $nombreProyecto = $this->input->post('nombreProyecto');
        $fechaLlenado = $this->input->post('fechaLlenado');
        $periodoEjecucion = $this->input->post('periodoEjecucion');
        $entidadFederativa = $this->input->post('entidadFederativa');
        $claveMunicipio = $this->input->post('claveMunicipio');
        $claveLocalidad = $this->input->post('claveLocalidad');
        $entidadFederativa = $this->input->post('entidadFederativa');


         $fk_program =  $this->session->userdata('ProgramasSelecc');
         $fk_subprogram =  $this->session->userdata('SubProgramasSelecc');

        if($fechaCaptura!=""){
            $fechaCaptura = $this->changeFormatDateSpEng($fechaCaptura);
        }

        if($fechaLlenado!=""){
            $fechaLlenado = $this->changeFormatDateSpEng($fechaLlenado);
        }
    
    
  

        $this->db->set('fk_program', $fk_program);
        $this->db->set('fk_subprogram', $fk_subprogram);        
        $this->db->set('nombre_comite', $nombreComite);
        $this->db->set('informe', $informe);
        $this->db->set('apartado_informe', $apartadoInforme);
        $this->db->set('numero', $numero);
        $this->db->set('clave_comite', $claveComite);
        $this->db->set('nombre_comite2', $nombreComite2);
        $this->db->set('fecha_captura', $fechaCaptura);
        $this->db->set('no_registro_css', $numeroRegistroCCS);
        $this->db->set('nombre_proyecto', $nombreProyecto);
        $this->db->set('fecha_llenado', $fechaLlenado);
        $this->db->set('periodo_ejecucion', $periodoEjecucion);
        $this->db->set('clave_entidad', $entidadFederativa);
        $this->db->set('clave_municipio', $claveMunicipio);
        $this->db->set('clave_localidad', $claveLocalidad);
     

       
        //INSERT FF
        $this->db->insert('cs_registro_informe');
        $lastId = $this->db->insert_id();


           $data = array(        
             'registro_informe' => $lastId,                               
          );
 

        $this->session->set_userdata($data);


         
                                                
                                                
                                                
/*
        echo "nombreComite:" . $nombreComite . "<br>";
        echo "informe:" . $informe . "<br>";
        echo "apartadoInforme:" . $apartadoInforme . "<br>";
        echo "numero:" . $numero . "<br>";
        echo "claveComite:" . $claveComite . "<br>";
        echo "nombreComite:" . $nombreComite . "<br>";
        echo "fechaCaptura:" . $fechaCaptura . "<br>";
        echo "numeroRegistroCCS:" . $numeroRegistroCCS . "<br>";
        echo "nombreProyecto:" . $nombreProyecto . "<br>";
        echo "fechaLlenado:" . $fechaLlenado . "<br>";
        echo "periodoEjecucion:" . $periodoEjecucion . "<br>";
        echo "entidadFederativa:" . $entidadFederativa . "<br>";
        echo "claveMunicipio:" . $claveMunicipio . "<br>";
        echo "claveLocalidad:" . $claveLocalidad . "<br>";*/

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId;
             
        } else {
            $status = false;
            $msg = $lastId;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

        


    }




    






                                             



    function guardarCuestionario(){
         
        $cumplenRequisitosBenefic = $this->input->post('cumplenRequisitosBenefic');
        $igualdadHyM = $this->input->post('igualdadHyM');
        $ProgramaEntreOportunam = $this->input->post('ProgramaEntreOportunam');
        $proyectoCumplePrograma = $this->input->post('proyectoCumplePrograma');
        $ProgramaFinPolitico = $this->input->post('ProgramaFinPolitico');
        $recibieronQuejas = $this->input->post('recibieronQuejas');
        $entregaronQuejasAutoridad = $this->input->post('entregaronQuejasAutoridad');
        $recibieronQuejaAutoridadCompetente = $this->input->post('recibieronQuejaAutoridadCompetente');
 
        $lastId_informe = $this->session->userdata('registro_informe');

                                         
                                                 
                                                

        echo "cumplenRequisitosBenefic:" . $cumplenRequisitosBenefic . "<br>";
        echo "igualdadHyM:" . $igualdadHyM . "<br>";
        echo "ProgramaEntreOportunam:" . $ProgramaEntreOportunam . "<br>";
        echo "proyectoCumplePrograma:" . $proyectoCumplePrograma . "<br>";
        echo "ProgramaFinPolitico:" . $ProgramaFinPolitico . "<br>";
        echo "recibieronQuejas:" . $recibieronQuejas . "<br>";
        echo "entregaronQuejasAutoridad:" . $entregaronQuejasAutoridad . "<br>";
        echo "recibieronQuejaAutoridadCompetente:" . $recibieronQuejaAutoridadCompetente . "<br>";
       

        $this->db->set('cumplenRequisitosBenefic', $cumplenRequisitosBenefic);
        $this->db->set('igualdadHyM', $igualdadHyM);        
        $this->db->set('ProgramaEntreOportunam', $ProgramaEntreOportunam);     
        $this->db->set('proyectoCumplePrograma', $proyectoCumplePrograma);     
        $this->db->set('ProgramaFinPolitico', $ProgramaFinPolitico);     
        $this->db->set('recibieronQuejas', $recibieronQuejas);     
        $this->db->set('entregaronQuejasAutoridad', $entregaronQuejasAutoridad);     
        $this->db->set('recibieronQuejaAutoridadCompetente', $recibieronQuejaAutoridadCompetente);     
 
        $this->db->where('id', $lastId_informe);       
        $this->db->update('cs_registro_informe');


              

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId;
             
        } else {
            $status = false;
            $msg = $lastId;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));


    }


    


     function guardarCuestionario2(){
         
        $csInformacionConocen = $this->input->post('csInformacionConocen');
        $csActividades = $this->input->post('csActividades');
        $csUtilidad = $this->input->post('csUtilidad');

        $list_csInformacionConocen = ""; 
        $list_csActividades = "";
        $list_csUtilidad = "";

        $lastId_informe = $this->session->userdata('registro_informe');



        if(!empty($csInformacionConocen)){           

             $list_csInformacionConocen = implode(",", $csInformacionConocen);
            echo "list_csInformacionConocen:" . $list_csInformacionConocen;

        }

        

        if(!empty($csActividades)){
        
            $list_csActividades = implode(",", $csActividades);
            echo "list_csInformacionConocen:" . $list_csInformacionConocen;

            
        }
 

        if(!empty($csUtilidad)){
            $list_csUtilidad = implode(",", $csUtilidad);
         
              echo "list_csUtilidad:" . $list_csUtilidad;
        }


        $this->db->set('csInformacionConocen', $list_csInformacionConocen);     
        $this->db->set('csActividades', $list_csActividades);     
        $this->db->set('csUtilidad', $list_csUtilidad);     
 
        $this->db->where('id', $lastId_informe);       
        $this->db->update('cs_registro_informe');
 

         
        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_informe;
             
        } else {
            $status = false;
            $msg = $lastId_informe;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

           
   


    }



}
