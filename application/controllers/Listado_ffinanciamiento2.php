<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listado_ffinanciamiento2 extends CI_Controller {

	/**
	 * Developed by Constantino Kusulas Vazquez   - 2018.
	 */


   	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
  		$this->load->library('session');


    }




	public function index()
	{

		if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }


		$fk_programa = $this->session->userdata('ProgramasSelecc');
 
         $listFFinanciamiento = $this->ffinanciamiento_model->getListFF($fk_programa);
        


        $data = array(
            
            'listFFinanciamiento' => $listFFinanciamiento, 
             
          
        );


        $this->load->view('listado_ffinanciamiento2', $data);
	}



	public function getListadoFF($program = ""){
	



		if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');
          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }





		$fk_programa = $this->session->userdata('ProgramasSelecc');
 
         $listFFinanciamiento = $this->ffinanciamiento_model->getListFF($fk_programa);
        
 
        $data = array(
            
            'listFFinanciamiento' => $listFFinanciamiento, 
            'recurso' => $this->session->userdata('RecursoSelecc'),
            'ffinanciamiento_model' =>$this->ffinanciamiento_model
             
          
        );


        $this->load->view('template/tableGeneral_ffinanciamiento2', $data);
       
	}


    public function getListadoFFpIndicadores($program = ""){
  



    if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');
          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }





    $fk_programa = $this->session->userdata('ProgramasSelecc');
 
         $listFFinanciamiento = $this->ffinanciamiento_model->getListFF($fk_programa);
        


        $data = array(
            
            'listFFinanciamiento' => $listFFinanciamiento, 
            'recurso' => $this->session->userdata('RecursoSelecc'),
             
          
        );


        $this->load->view('template/tableGeneral_ffinanciamiento_indicadores', $data);
       
  }


 



}
