
                
    <form id="form_listadoProyectos" action="registro_proyecto" method="post">

    <?php
 
     $programaSelected = $this->session->userdata('ProgramasSelecc');
    ?>

    <table id="example" class="display" class="col-lg-12">
        <thead>
            <tr >
                <th>Folio</th>
                <?php
               // if($programaSelected == "todos") 
            
                    //{
                        ?>

                <th>Programa</th>
                <?php
               //  }
                ?>
                <th>Ejecutora</th>
                <th>Tipo de Beneficio</th>
                <th>Proyecto</th>                
               
                
                <th>Estatus</th>
                <th>Municipio</th>
                <th>Localidad</th>
                
                <th>Total Beneficiados</th>
                <th>M. Asignado Federal</th>
                <th>M. Asignado Estatal</th>
                <th>M. Asignado Municipal</th>
                <th>M. Asignado Otros</th>
                <th>Total R. Asignado</th>

                <th>M. Ejecutado Federal</th>
                <th>M. Ejecutado Estatal</th>
                <th>M. Ejecutado Municipal</th>
                <th>M. Ejecutado Otros</th>
                <th>Total Ejecutado</th>
                <th>Total Comites</th>
              
              
               
                
               
                
               

                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

            <?php

            $counter = 1;



            foreach ($listProyectos as $row){
              //  $rowPrograma = "";

                  //  if($programaSelected == "todos") {

                $rowPrograma = $row["programa"];
             

                $bandData = "Iniciado";
                $date_now =  new DateTime();
                $date_final_obra = new DateTime($row["fecha_final_programada"]);

                if($date_now>$date_final_obra){
                  $bandData = "Terminado";
                }


                $this->db->set('estatus_proyecto', $bandData); 
                $this->db->where('id', $row["id"]);      
                $this->db->update('cs_registro_proyecto');


                echo "<tr id='tr_".$row["id"]."'><td>". $counter . "</td><td>". $rowPrograma ."</td><td>". $row["instancia_ejecutora"] . "</td><td>". $row["beneficio"] . "</td><td>". $row["nombre_proyecto"] . "</td><td id='td_".$row["id"]."'>". $bandData . "</td><td>". $row["municipio"] . "</td><td>". $row["localidad"] . "</td><td>" .  number_format($row["total_beneficiados"]) . "</td><td>$" . number_format($row["monto_recurso_asignado_vig_federal"]) ."</td><td>$" . number_format($row["monto_recurso_asignado_vig_estatal"]) ."</td><td>$" . number_format($row["monto_recurso_asignado_vig_municipal"]) ."</td><td>$" . number_format($row["monto_recurso_asignado_vig_otros"]) ."</td><td>$" . number_format($row["recurso_asignado_vigilar"]) ."</td><td>$" . number_format($row["monto_recurso_asignado_ejec_federal"]) ."</td><td>$" . number_format($row["monto_recurso_asignado_ejec_estatal"]) ."</td><td>$" . number_format($row["monto_recurso_asignado_ejec_municipal"]) ."</td><td>$" . number_format($row["monto_recurso_asignado_ejec_otros"]) ."</td><td>$" . number_format($row["recurso_ejecutado_vigilado"]) ."</td><td>" . $row["total_comites"] . "</td>";
                ?>

                 <td>

              <button type="button" class="btn btn-xs btn-warning waves-effect w-md waves-light m-b-15"  onClick='viewProy(<?=$row['id']?>)' data-toggle="modal" title="Ver" data-target=". "><i class="fa fa-file"></i></button>
                      
              <button type="button" class="btn btn-xs btn-primary waves-effect w-md waves-light m-b-15"  onClick='editProy(<?=$row['id']?>)' data-toggle="modal" title="Editar" data-target=". "><i class="fa fa-pencil"></i></button>  
              
               <button type="button" class="btn btn-xs btn-danger waves-effect w-md waves-light m-b-15"  onClick='delProy(<?=$row['id']?>,<?=$row['fk_departamento']?>)' data-toggle="modal" title="Borrar" data-target=". "><i class="fa fa-trash"></i></button>


               <button type="button" class="btn btn-xs btn-success waves-effect w-md waves-light m-b-15"  onClick='cancelProy(<?=$row['id']?>,<?=$row['fk_departamento']?>)' data-toggle="modal" title="Cancelar" data-target=". "><i class="fa fa-trash"></i></button>

             </td></tr>

              
              
 


          

          <?php

            $counter++;
            
            }


            ?>
       
           
         
        </tbody>
       
    </table>


    <input type="hidden" name="id_proyecto" id="id_proyecto">
    <input type="hidden" name="fk_departamento" id="fk_departamento">
    <input type="hidden" name="method" id="method">
 



    </form>


  

    <script type="text/javascript">


    $(document).ready(function () {     
     
 


        $('#example tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="'+title+'" />' );
        } );
 


        var tableProy = $('#example').DataTable( {
           language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
            dom: 'Bfrtip',
            scrollX: 'true',
            pagingType: 'full_numbers',
            bAutoWidth: false,
        
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
            bInfo: false,
            buttons: [
                
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8 ]
                    }   
                },
                
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8 ]
                    }
                }
            ],

        } );


      /*   tableProy.columns().every( function () {
            var that = this;
     
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );*/


               


           
 
            
            

          });    



   function editProy(id_proyecto)
        {

            $("#id_proyecto").val(id_proyecto);
            $("#method").val("edit");
           
            $("#form_listadoProyectos").submit();



        }


     function viewProy(id_proyecto)
        {

            $("#id_proyecto").val(id_proyecto);
            $("#method").val("view");
           
            $("#form_listadoProyectos").submit();



        }



    

     function delProy(id_proyecto,fk_departamento){


            $("#id_proyecto").val(id_proyecto);
            $("#fk_departamento").val(fk_departamento);

            $.ajax({
                    url: "<?php echo site_url('registro_proyecto/existeComite') ?>",
                    type: "POST",
                    data: $('#form_listadoProyectos').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                      if(status){
                           swal("Ups..", "No se puede borrar Obra, esta vinculado a un Comité", "error");
                       }else{
                           

                            swal({
                              title: "Estás seguro de querer borrar la obra?",
                              text: "",
                              icon: "warning",
                              buttons: true,
                              dangerMode: false,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                                swal("Listo!, Se ha Borrado exitosamente", {
                                  icon: "success",
                                });
                                eliminarProyectoAjax(id_proyecto, fk_departamento);

                              } else {
                                swal("Lo dejamos como estaba..");
                              }
                            });
                       }
                        
 
                    }
                });


            return;

           
        }



         function cancelProy(id_proyecto,fk_departamento){


            $("#id_proyecto").val(id_proyecto);
            $("#fk_departamento").val(fk_departamento);

            $.ajax({
                    url: "<?php echo site_url('registro_proyecto/existeComite') ?>",
                    type: "POST",
                    data: $('#form_listadoProyectos').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

/*
                      if(status){
                           swal("Ups..", "No se puede cancelar Obra, esta vinculado a un Comité", "error");
                       }else{*/
                           

                            swal({
                              title: "Estás seguro de querer cancelar la obra?",
                              text: "",
                              icon: "warning",
                              buttons: true,
                              dangerMode: false,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                                swal("Listo!, Se ha Cancelado exitosamente", {
                                  icon: "success",
                                });
                                cancelarProyectoAjax(id_proyecto, fk_departamento);

                              } else {
                                swal("Lo dejamos como estaba..");
                              }
                            });
                      // }
                        
 
                    }
                });


            return;

           
        }

 
        


    function eliminarProyectoAjax(id_proyecto, fk_departamento){

         $("#id_proyecto").val(id_proyecto);
         $("#fk_departamento").val(fk_departamento);

            $.ajax({
                    url: "<?php echo site_url('registro_proyecto/eliminarObra') ?>",
                    type: "POST",
                    data: $('#form_listadoProyectos').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                       if(message=="ok"){
                         $("#tr_"+id_proyecto).hide('slow');
                       }

                       if(message == "comite"){
                        alert("No se puede borrar Obra, si esta vinculado a un Comite.")
                       }
                        
 
                    }
                });
    }


    function cancelarProyectoAjax(id_proyecto, fk_departamento){

         $("#id_proyecto").val(id_proyecto);
         $("#fk_departamento").val(fk_departamento);

            $.ajax({
                    url: "<?php echo site_url('registro_proyecto/cancelarObra') ?>",
                    type: "POST",
                    data: $('#form_listadoProyectos').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                      if(message=="ok"){
                         $("#td_"+id_proyecto).html('Cancelado');
                       }

                       if(message == "comite"){
                        alert("No se puede borrar Obra, si esta vinculado a un Comite.")
                       }
                        
 
                    }
                });
    }

        


    </script>