<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdfexample5 extends CI_Controller {
   


function __construct() { 
     parent::__construct();

        $this->load->model('ffinanciamiento_model');

} 



public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $img_file = asset_url()."/formats/capacitacion/capacitacion1.jpg";
       // $this->Image($img_file, 0, -10, 210, 297, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }



function index()
    {
          $this->load->library('Pdf');
          
          //$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
          $pdf = new Pdf('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


          $id_comite =  $this->input->post('id_comite');

                  
          $getDataComite = $this->ffinanciamiento_model->getInfoComite($id_comite);


          $listIntegrantes = $this->ffinanciamiento_model->getIntegrantesComite($id_comite);
/*
  echo print_r($listIntegrantes);
   die();*/

/*
echo print_r($getDataComite);
die();*/

    /*   echo "programa:" . $getDataComite[0]['programa'];
       die();*/
// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);


 

// add a page
$pdf->AddPage('P');


// -- set new background ---

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
   $img_file = asset_url()."/formats/comite_integracion/integracion_modificado.jpg";
  // $pdf->setJPEGQuality(75);

   // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)

$pdf->Image($img_file, 0, 0, 210, 280, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();



//Municipio
$pdf->SetXY(40, 53);
//$txtPrograma = "SAN JUAN DEL RIO";

$txtPrograma = strtoupper($getDataComite[0]['municipio']);
$pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


//Fecha Dia
$pdf->SetXY(156, 53);
$txtPrograma = "10";
$pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);

//Fecha Mes
$pdf->SetXY(166, 53);
$txtPrograma = "SEP";
$pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);

//Fecha Anio
$pdf->SetXY(180, 53);
$txtPrograma = "82";
$pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);




//Localidad
$pdf->SetXY(40, 60);
//$txtPrograma = "Ampliacion Nuevo San Isidro Primera Seccion";
$txtPrograma = strtoupper($getDataComite[0]['localidad']);
$pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);




//Programa
$pdf->SetXY(40, 45);


//$txtPrograma = "Ampliacion Nuevo San Isidro Primera Seccion";
$txtPrograma = $getDataComite[0]['programa']; //"3X1 PARA MIGRANTES";
$pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);

 

//Nombre de la Obra
$pdf->SetXY(20, 87);
$nombreObra = strtoupper($getDataComite[0]['nombre_proyecto']); 
$separador = "";
if(strlen($nombreObra)>54){
  $separador = "_";
}

$nombreObraTmp = substr($nombreObra,0,54).$separador;


$separador="";
$nombreObraTmp2 = substr($nombreObra,54,54);
if(strlen($nombreObraTmp2)>54){
  $separador="_";
  $nombreObraTmp2 = $nombreObraTmp2.$separador;
}



$separador="";
$nombreObraTmp3 = substr($nombreObra,108,54);
if(strlen($nombreObraTmp3)>54){
  $separador="_";
  $nombreObraTmp3 = $nombreObraTmp3.$separador;
}




$separador="";
$nombreObraTmp4 = substr($nombreObra,162,54)."_";
if(strlen($nombreObraTmp4)>54){
  $separador="...";
  $nombreObraTmp4 = $nombreObraTmp3.$separador;
}





//$txtPrograma = strtoupper($getDataComite[0]['nombre_proyecto']); 
$pdf->Write(0, $nombreObraTmp, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetXY(20, 90);
$pdf->Write(0, $nombreObraTmp2, '', 0, 'L', true, 0, false, false, 0);

$pdf->SetXY(20, 93);
$pdf->Write(0, $nombreObraTmp3, '', 0, 'L', true, 0, false, false, 0);

$pdf->SetXY(20, 96);
$pdf->Write(0, $nombreObraTmp4, '', 0, 'L', true, 0, false, false, 0);





   

//PRIMER INTEGRANTE
       $nombre = strtoupper($listIntegrantes[0]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[0]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[0]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(20, 127);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Cargo
    $pdf->SetXY(140, 130);
    $txtPrograma = strtoupper($listIntegrantes[0]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Telefono
     $pdf->SetXY(118, 135);
    $txtPrograma = $listIntegrantes[0]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Direccion  
    $calle = strtoupper($listIntegrantes[0]['calle_integrante']);
    $num = $listIntegrantes[0]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[0]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    $pdf->SetXY(20, 135);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);





    //segundo INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(20, 141);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Cargo
    $pdf->SetXY(139, 144);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Telefono
     $pdf->SetXY(118, 149);
    $txtPrograma = $listIntegrantes[1]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
        $pdf->SetXY(20, 150);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);




     //tercer INTEGRANTE
       $nombre = strtoupper($listIntegrantes[2]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[2]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[2]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(20, 156);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Cargo
    $pdf->SetXY(140, 158);
    $txtPrograma = strtoupper($listIntegrantes[2]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Telefono
     $pdf->SetXY(118, 163);
    $txtPrograma = $listIntegrantes[2]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Direccion    
    $calle = strtoupper($listIntegrantes[2]['calle_integrante']);
    $num = $listIntegrantes[2]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[2]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 161);
        $pdf->SetXY(20, 164);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);






     //cuarto INTEGRANTE
       $nombre = strtoupper($listIntegrantes[3]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[3]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[3]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(20, 170);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Cargo
        $pdf->SetXY(140, 173);
    $txtPrograma = strtoupper($listIntegrantes[3]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Telefono
     $pdf->SetXY(118, 177);
    $txtPrograma = $listIntegrantes[3]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Direccion    
    $calle = strtoupper($listIntegrantes[3]['calle_integrante']);
    $num = $listIntegrantes[3]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[3]['colonia_integrante']);

    $direccion = $calle . " " . $num . " " . $colonia;
    $pdf->SetXY(20, 178);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);





     //quinto INTEGRANTE
       $nombre = strtoupper($listIntegrantes[4]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[4]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[4]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(20, 185);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Cargo
        $pdf->SetXY(140, 188);
    $txtPrograma = strtoupper($listIntegrantes[4]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Telefono
     $pdf->SetXY(118, 191);
    $txtPrograma = $listIntegrantes[4]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Direccion    
    $calle = strtoupper($listIntegrantes[4]['calle_integrante']);
    $num = $listIntegrantes[4]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[4]['colonia_integrante']);

    $direccion = $calle . " " . $num . " " . $colonia;
    $pdf->SetXY(20, 193);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    




     //sexto INTEGRANTE
       $nombre = strtoupper($listIntegrantes[5]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[5]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[5]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(20, 199);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Cargo
        $pdf->SetXY(140, 202);
    $txtPrograma = strtoupper($listIntegrantes[5]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Telefono
     $pdf->SetXY(118, 205);
    $txtPrograma = $listIntegrantes[5]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Direccion    
    $calle = strtoupper($listIntegrantes[5]['calle_integrante']);
    $num = $listIntegrantes[5]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[5]['colonia_integrante']);

    $direccion = $calle . " " . $num . " " . $colonia;
    $pdf->SetXY(21, 207);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);







 

// ---------------------------------------------------------

 









 ob_end_clean();
$pdf->Output('example_007.pdf', 'I');
    }


}
?>
