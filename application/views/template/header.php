
<!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->
<link href="<?php echo asset_url(); ?>css/plugins/pace/pace.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/plugins/pace/pace.js"></script>

<!-- GLOBAL STYLES - Include these on every page. -->
<link href="<?php echo asset_url(); ?>css/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="<?php echo asset_url(); ?>fonts_/ubuntu.css" rel="stylesheet" type="text/css">
<link href="<?php echo asset_url(); ?>fonts_/openSans.css" rel="stylesheet" type="text/css">

<!-- PAGE LEVEL PLUGIN STYLES -->

<!-- THEME STYLES - Include these on every page. -->
<link href="<?php echo asset_url(); ?>css/style.css" rel="stylesheet">
<link href="<?php echo asset_url(); ?>css/plugins.css" rel="stylesheet">

<!-- THEME DEMO STYLES - Use these styles for reference if needed. Otherwise they can be deleted. -->
<link href="<?php echo asset_url(); ?>css/demo.css" rel="stylesheet">
<script src="<?php echo asset_url(); ?>js/plugins/jquery/jquery.min.js"></script>

<script src="<?php echo asset_url(); ?>js/plugins/jcarousel/jquery.jcarousel.min.js"></script>
<script src="<?php echo asset_url(); ?>js/plugins/jcarousel/jcarousel.responsive.js"></script>


<link href="<?php echo asset_url(); ?>DataTables/datatables.min.css" rel="stylesheet"> 

<script src="<?php echo asset_url(); ?>DataTables/datatables.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/features/searchHighlight/dataTables.searchHighlight.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>

<script src="https://bartaz.github.io/sandbox.js/jquery.highlight.js"></script>


<!--<link href="<?php echo asset_url(); ?>css/jquery.dataTables.min.css" rel="stylesheet"> 

<script src="<?php echo asset_url(); ?>js/plugins/datatables/jquery.dataTables.js"></script>-->


  <!--          <link href="https://cdn.datatables.net/plug-ins/1.10.13/features/mark.js/datatables.mark.min.css" rel="stylesheet"> 

            <script src="https://cdn.datatables.net/plug-ins/1.10.13/features/mark.js/datatables.mark.js"></script>-->




 <!--<link href="<?php echo asset_url(); ?>js/plugins/datatables/mark/datatables.mark.min.css" rel="stylesheet">-->
  <!--<link href="<?php echo asset_url(); ?>js/datatables/datatables.min.css" rel="stylesheet">-->

<script src="<?php echo asset_url(); ?>js/bootstrap-select.min.js"></script>
<link href="<?php echo asset_url(); ?>css/buttons.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/plug-ins/1.10.19/features/searchHighlight/dataTables.searchHighlight.css" rel="stylesheet">
 
<link href="<?php echo asset_url(); ?>font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo asset_url(); ?>css/plugins/glyphicons/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap-select.min.css">

<link href="<?php echo asset_url(); ?>css/plugins/jcarousel/jcarousel.responsive.css" rel="stylesheet">








<script src="<?php echo asset_url(); ?>js/plugins/popupoverlay/logout.js"></script>
<!-- HISRC Retina Images -->
<script src="<?php echo asset_url(); ?>js/plugins/hisrc/hisrc.js"></script>

<!-- PAGE LEVEL PLUGIN SCRIPTS -->

<!-- THEME SCRIPTS -->
<script src="<?php echo asset_url(); ?>js/flex.js"></script>

<script src="<?php echo asset_url(); ?>js/plugins/sweetalert/sweetalert.min.js"></script>









   <!--<script src="<?php echo asset_url(); ?>js/plugins/datatables/mark/datatables.mark.js"></script>-->
   <!-- <script src="<?php echo asset_url(); ?>js/datatables/datatables.min.js"></script>-->
<script src="<?php echo asset_url(); ?>js/dataTables.buttons.min.js"></script>
<script src="<?php echo asset_url(); ?>js/jszip.min.js"></script>
<!--<script src="<?php echo asset_url(); ?>js/pdfmake.min.js"></script>-->
<script src="<?php echo asset_url(); ?>js/vfs_fonts.js"></script>
<script src="<?php echo asset_url(); ?>js/buttons.html5.min.js"></script>




<!--[if lt IE 9]>
  <script src="<?php echo asset_url(); ?>js/html5shiv.js"></script>
  <script src="<?php echo asset_url(); ?>js/respond.min.js"></script>
<![endif]-->

<link rel="stylesheet" href="<?php echo asset_url(); ?>css/jquery-ui.css">
 


<script type="text/javascript">

    var existeFF = 0;
    
    

    // var band_populate_programs = 1;

    $(document).ready(function () {


        // $('body').css('zoom','80%'); /* Webkit browsers */
        // $('body').css('zoom','0.8'); 




        $("#recurso_sel").change(function () {


            var recursoSelected = $("#recurso_sel option:selected").val();

            //if(band_populate_programs){
            populate_programas(recursoSelected);
            //  }

        });


    });


    function populate_programas(fromRecurso) {

        var cantElement = 0;
        $('#localidad').empty();
        $('#localidad').append("<option>Cargando ....</option>");

        if (fromRecurso != "") {
            $.ajax({
                type: "GET",
                url: "<?php echo site_url('indicadores_generales/populate_programas') ?>/" + fromRecurso,
                contentType: "application/json;charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    $('#programaSeleccionado').empty();
                    $('#programaSeleccionado').append('<option>Seleccione Programa</option>');


                    // $('#programaSeleccionado').append("<option>Selecciona Localidad..</option>");

                    var titulo;
                    //echo "<option $select value='todos'>TODOS LOS PROGRAMAS</option>";

                    if (fromRecurso == 0) {
                        $('#programaSeleccionado').append('<option value="todos">TODOS LOS PROGRAMAS</option>');
                    
                    }

                    $.each(data, function (i, name) {


                        if (fromRecurso == 0) {

                            titulo = data[i].programa;

                            res = titulo.substr(0, 50);
                            res = res + "...";
                            $('#programaSeleccionado').append('<option value="' + data[i].id + '">' + res + '</option>');
                            cantElement++;
                        }
                        else if (data[i].fk_recurso == fromRecurso) {

                            titulo = data[i].programa;

                            res = titulo.substr(0, 50);
                            res = res + "...";
                            $('#programaSeleccionado').append('<option value="' + data[i].id + '">' + res + '</option>');
                            cantElement++;

                        }

                    });
                    $('#programaSeleccionado').append('<option value="agregar">AGREGAR NUEVO PROGRAMA...</option>');



                }
            });
        }
    }


    function selectRecurso(elem) {

        if (elem == 0) {
            $("#petcs_menu").hide("slow");
            $("#patcs_menu").hide("slow");
            $("#seguimiento_menu").hide("slow");
        }

    }



    function guardarPrograma() {


        $("#nombrePrograma").css({"background-color": "white"});
        $("#recurso").css({"background-color": "white"});
        $("#esquema_normativo").css({"background-color": "white"});

        var msgError = "";

        if ($("#nombrePrograma").val() == "") {
            msgError = "Debe escribir Nombre Programa, ";
            $("#nombrePrograma").css({"background-color": "yellow"});
        }


        if ($("#recurso option:selected").val() == "Seleccione..") {
            msgError += "\n Debe escoger un Recurso, ";
            $("#recurso").css({"background-color": "yellow"});

        }

        if ($("#esquema_normativo option:selected").val() == "Seleccione..") {
            msgError += "\n Debe escoger el Esquema Normativo, ";
            $("#esquema_normativo").css({"background-color": "yellow"});
        }





        if (msgError == "") {


            $.ajax({
                url: "<?php echo site_url('indicadores_generales/guardarNuevoPrograma') ?>",
                type: "POST",
                data: $('#form_programa').serialize(),
                dataType: "JSON",
                success: function (data)
                {
                    var status, message;
                    $.each(data, function (index, obj) {
                        status = true;
                        titulo_programa = "";
                    });

                    if (status) {

                        $('.modal-nuevo').modal("hide");

                        swal("Bien hecho!", "El programa ha sido guardado.  Ahora puedes usarlo.  Seleccionando en la Lista de Programas", "success");



                        location.reload(true);

                    }


                }
            });






        } else {
            swal("Error!", msgError, "error");
        }

    }




    function registrarFinanciamiento() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/registro_ffinanciamiento";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }

    function listadoFFinanciamiento() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/listado_ffinanciamiento";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }

    function registrarProyecto() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/registro_proyecto";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }

    function listadoProyecto() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/listado_obras";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }


 

    function listadoComites() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/listado_comites";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }

       function listadoCapacitacion() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/listado_comites";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }

 
     function listadoCapacitacion() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/listado_capacitacion";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }


    function listadoSeguimiento() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/listado_seguimiento";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }

    


    function registrarComite() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/registro_comite";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }

    function registrarSeguimiento() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/registro_seguimiento";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }




    function registrarCapacitacion() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/registro_capacitacion";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }

    function registrarMaterial() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/registro_materiales";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }


    function registrarReunion() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/registro_minutas";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }


    function registrarInforme() {

        if ($("#programaSeleccionado").val() != "todos")
        {
            location.href = "<?php echo site_url(); ?>/registro_informes";
        } else {
            swal("Advertencia!", "Debes elegir un Programa", "warning");
        }


    }











</script>

