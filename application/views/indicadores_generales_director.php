
<!DOCTYPE html>
<html lang="es">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SIQSEQ V2.0</title>

        <?php $this->view('template/header.php'); ?>   

    </head>

    <body>

        <div id="wrapper">

            <!-- begin TOP NAVIGATION -->
            <?php $this->view('template/nav_top_messages.php'); ?>       
            <!-- /.navbar-top -->
            <!-- end TOP NAVIGATION -->

            <!-- begin SIDE NAVIGATION -->
            <?php $this->view('template/nav_left_menus.php'); ?>       
            <!-- /.navbar-side -->
            <!-- end SIDE NAVIGATION -->

            <!-- begin MAIN PAGE CONTENT -->
            <div id="page-wrapper">

                <div class="page-content">



                    <!-- begin PAGE TITLE ROW -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-title">
                                <div id="titleProgram"></div>
                                <h1>
                                    <small>Formulario</small>
                                </h1>
                                <ol class="breadcrumb">

                                    <li><i class="fa fa-edit"></i>Indicadores Generales</i>
                                    </li>
                                    <li class="active">Principal</li>


                                    <!--submenu Programas -->
                                    <?php $this->view('template/nav_submenu_programas.php'); ?>       

                                </ol>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <!-- end PAGE TITLE ROW -->

                    <!-- begin MAIN PAGE ROW -->
                    <div class="row">





                        <!-- begin LEFT COLUMN -->
                        <div class="col-lg-12">

                            <div class="row">

                                <!-- Basic Form Example -->
                                <div class="col-lg-12">

                                    <div class="portlet portlet-default">
                                        <div class="portlet-heading">
                                            <div class="portlet-title">
                                                <h4>Listados de Obras 2018 - Programa 1</h4>
                                            </div>
                                            <div class="portlet-widgets">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div id="basicFormExample" class="panel-collapse collapse in">
                                            <div class="portlet-body">

                                                <!-- begin DASHBOARD CIRCLE TILES -->
                                                <div class="row">
                                                    <div class="col-lg-2 col-sm-6">
                                                        <div class="circle-tile">
                                                            <a href="#">
                                                                <div class="circle-tile-heading dark-blue">
                                                                    <i class="fa fa-users fa-fw fa-3x"></i>
                                                                </div>
                                                            </a>
                                                            <div class="circle-tile-content dark-blue">
                                                                <div class="circle-tile-description text-faded">
                                                                    Usuarios Beneficiados
                                                                </div>
                                                                <div class="circle-tile-number text-faded">
                                                                    10,450
                                                                    <span id="sparklineA"><canvas style="display: inline-block; width: 29px; height: 24px; vertical-align: top;" width="29" height="24"></canvas></span>
                                                                </div>
                                                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-sm-6">
                                                        <div class="circle-tile">
                                                            <a href="#">
                                                                <div class="circle-tile-heading green">
                                                                    <i class="fa fa-money fa-fw fa-3x"></i>
                                                                </div>
                                                            </a>
                                                            <div class="circle-tile-content green">
                                                                <div class="circle-tile-description text-faded">
                                                                    Presupuesto total  
                                                                </div>
                                                                <div class="circle-tile-number text-faded">
                                                                    $30,384,233
                                                                </div>
                                                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-sm-6">
                                                        <div class="circle-tile">
                                                            <a href="#">
                                                                <div class="circle-tile-heading blue">
                                                                    <i class="fa fa-balance-scale fa-fw fa-3x"></i>
                                                                </div>
                                                            </a>
                                                            <div class="circle-tile-content blue">
                                                                <div class="circle-tile-description text-faded">
                                                                    Presupuesto Ejecutado
                                                                </div>
                                                                <div class="circle-tile-number text-faded">
                                                                    $20,084,233
                                                                    <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                                                </div>
                                                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-sm-6">
                                                        <div class="circle-tile">
                                                            <a href="#">
                                                                <div class="circle-tile-heading orange">
                                                                    <i class="fa fa-cubes fa-fw fa-3x"></i>
                                                                </div>
                                                            </a>
                                                            <div class="circle-tile-content orange">
                                                                <div class="circle-tile-description text-faded">
                                                                    Obras
                                                                </div>
                                                                <div class="circle-tile-number text-faded">
                                                                    9 
                                                                </div>
                                                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-sm-6">
                                                        <div class="circle-tile">
                                                            <a href="#">
                                                                <div class="circle-tile-heading purple">
                                                                    <i class="fa fa-handshake-o fa-fw fa-3x"></i>
                                                                </div>
                                                            </a>
                                                            <div class="circle-tile-content purple">
                                                                <div class="circle-tile-description text-faded">
                                                                    Apoyos
                                                                </div>
                                                                <div class="circle-tile-number text-faded">
                                                                    18
                                                                    <span id="sparklineC"></span>
                                                                </div>
                                                                <a href="#" class="circle-tile-footer">Mas Detalles <i class="fa fa-chevron-circle-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--   <div class="col-lg-2 col-sm-6">
                                                         <div class="circle-tile">
                                                             <a href="#">
                                                                 <div class="circle-tile-heading dark-blue">
                                                                     <i class="fa fa-ambulance fa-fw fa-3x"></i>
                                                                 </div>
                                                             </a>
                                                             <div class="circle-tile-content dark-blue">
                                                                 <div class="circle-tile-description text-faded">
                                                                    Servicios
                                                                 </div>
                                                                 <div class="circle-tile-number text-faded">
                                                                     20
                                                                     <span id="sparklineC"></span>
                                                                 </div>
                                                                 <a href="#" class="circle-tile-footer">Mas Detalles <i class="fa fa-chevron-circle-right"></i></a>
                                                             </div>
                                                         </div>
                                                     </div>-->
                                                    <div class="col-lg-2 col-sm-4">
                                                        <div class="circle-tile">
                                                            <a href="#">
                                                                <div class="circle-tile-heading red">
                                                                    <i class="fa fa-book fa-fw fa-3x"></i>
                                                                </div>
                                                            </a>
                                                            <div class="circle-tile-content red">
                                                                <div class="circle-tile-description text-faded">
                                                                    Total de Programas
                                                                </div>
                                                                <div class="circle-tile-number text-faded">
                                                                    18
                                                                    <span id="sparklineC"></span>
                                                                </div>
                                                                <a href="#" class="circle-tile-footer">Mas Detalles <i class="fa fa-chevron-circle-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                                <!-- end DASHBOARD CIRCLE TILES -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.portlet -->
                                </div>

                                <!-- /.col-lg-12 (nested) -->
                                <!-- End Basic Form Example -->

                                <!-- Inline Form Example -->

                                <div class="col-lg-12">

                                    <div class="portlet portlet-green">
                                        <div class="portlet-heading">
                                            <div class="portlet-title">
                                                <h4>Listados de Obras 2018 - Programa 1</h4>
                                            </div>
                                            <div class="portlet-widgets">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div id="basicFormExample" class="panel-collapse collapse in">
                                            <div class="portlet-body">
                                                <table id="example" class="display" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Folio</th>
                                                            <th>Programa</th>
                                                            <th>Recurso</th>
                                                            <th>Accion</th>
                                                            <th>Inversion</th>
                                                            <th>Capacitacion</th>
                                                            <th>Comites</th>
                                                            <th>Beneficiados</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>0048</td>
                                                            <td>Hombro con hombro en tu calle</td>
                                                            <td>Estatal</td>
                                                            <td>Obra</td>
                                                            <td>$11,570,642.80</td>
                                                            <td>5</td>
                                                            <td>3</td>
                                                            <td>148</td>
                                                        </tr>
                                                        <tr>
                                                            <td>0047</td>
                                                            <td>3x1 para migrantes</td>
                                                            <td>Estatal</td>
                                                            <td>Obra</td>
                                                            <td>$5,570,642.80</td>
                                                            <td>2</td>
                                                            <td>5</td>
                                                            <td>850</td>
                                                        </tr>
                                                        <tr>
                                                            <td>0046</td>
                                                            <td>Programa especial CEA</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$1,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>

                                                        <tr>
                                                            <td>0045</td>
                                                            <td>Programa de infraestrucutra indigena</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$2,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>
                                                        <tr>
                                                            <td>0044</td>
                                                            <td>Hombro con hombro en tu calle</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$3,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>

                                                        <tr>
                                                            <td>0043</td>
                                                            <td>Programa especial CEA</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$1,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>

                                                        <tr>
                                                            <td>0042</td>
                                                            <td>Programa de infraestrucutra indigena</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$2,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>
                                                        <tr>
                                                            <td>0041</td>
                                                            <td>Hombro con hombro en tu calle</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$3,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>

                                                        <tr>
                                                            <td>0040</td>
                                                            <td>Programa especial CEA</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$1,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>

                                                        <tr>
                                                            <td>0039</td>
                                                            <td>Programa de infraestrucutra indigena</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$2,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>
                                                        <tr>
                                                            <td>0038</td>
                                                            <td>Hombro con hombro en tu calle</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$3,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>
                                                        <tr>
                                                            <td>0037</td>
                                                            <td>Programa de infraestrucutra indigena</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$2,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>
                                                        <tr>
                                                            <td>0036</td>
                                                            <td>Hombro con hombro en tu calle</td>
                                                            <td>Estatal</td>
                                                            <td>Servicio</td>
                                                            <td>$3,570,642.80</td>
                                                            <td>1</td>
                                                            <td>0</td>
                                                            <td>250</td>
                                                        </tr>

                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Folio</th>
                                                            <th>Programa</th>
                                                            <th>Recurso</th>
                                                            <th>Accion</th>
                                                            <th>Inversion</th>
                                                            <th>Capacitacion</th>
                                                            <th>Comites</th>
                                                            <th>Beneficiados</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.portlet -->
                                </div>
                                <!-- /.col-lg-12 (nested) -->
                                <!-- End Basic Form Example -->

                                <!-- Inline Form Example -->


                            </div>


                        </div>
                        <!-- /.col-lg-6 -->
                        <!-- end LEFT COLUMN -->



                    </div>
                    <!-- /.row -->
                    <!-- end MAIN PAGE ROW -->



                </div>
                <!-- /.page-content -->

            </div>
            <!-- /#page-wrapper -->
            <!-- end MAIN PAGE CONTENT -->

        </div>
        <!-- /#wrapper -->

        <!-- GLOBAL SCRIPTS -->
        <script src="<?php echo asset_url(); ?>js/plugins/jquery/jquery.min.js"></script>
        <script src="<?php echo asset_url(); ?>js/plugins/jquery/jquery.numeric.js"></script>
        <script src="<?php echo asset_url(); ?>js/plugins/bootstrap/bootstrap.min.js"></script>

        <script src="<?php echo asset_url(); ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo asset_url(); ?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
        <script src="<?php echo asset_url(); ?>js/plugins/popupoverlay/defaults.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Salir Notification Box -->
        <div id="logout">
            <div class="logout-message">
                <?php
                $photo = $this->session->userdata('photo');
                ?>

                <img class="img-circle img-Salir" src="<?php echo asset_url(); ?>img/<?= $photo ?>" alt="">
                <h3>
                    <i class="fa fa-sign-out text-green"></i> Listo para Salir?
                </h3>
                <p>Estas seguro, de salir de tu session actual?.</p>
                <ul class="list-inline">
                    <li>
                        <a href="<?php echo site_url() ?>" class="btn btn-green">
                            <strong>Salir</strong>
                        </a>
                    </li>
                    <li>
                        <button class="logout_close btn btn-green">Cancelar</button>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /#Salir -->
        <!-- Salir Notification jQuery -->
        <script src="<?php echo asset_url(); ?>js/plugins/popupoverlay/logout.js"></script>
        <!-- HISRC Retina Images -->
        <script src="<?php echo asset_url(); ?>js/plugins/hisrc/hisrc.js"></script>

        <!-- PAGE LEVEL PLUGIN SCRIPTS -->

        <!-- THEME SCRIPTS -->
        <script src="<?php echo asset_url(); ?>js/flex.js"></script>





        <script src="<?php echo asset_url(); ?>js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>








        <script type="text/javascript">

            $(document).ready(function () {

                changeProgram();


                $('#example').DataTable({
                     language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
                    dom: 'Bfrtip',
                    pagingType: 'full_numbers',
                    dom: 'T<"clear">lfrtip',
                            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                    bInfo: false,
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            text: '<i class="fa fa-files-o"></i>',
                            titleAttr: 'Copy'
                        },
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-excel-o"></i>',
                            titleAttr: 'Excel'
                        },
                        {
                            extend: 'csvHtml5',
                            text: '<i class="fa fa-file-text-o"></i>',
                            titleAttr: 'CSV'
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-pdf-o"></i>',
                            titleAttr: 'PDF'
                        }
                    ]
                });

                $("#catalogResources").change(function () {

                    var resourceSelected = $("#catalogResources option:selected").val();

                    populate_programs(resourceSelected);

                });

                function populate_programs(fromResource) {
                    var cantElement = 0;
                    $('#catalogPrograms').empty();
                    $('#catalogPrograms').append("<option>Loading ....</option>");
                    $.ajax({
                        type: "GET",
                        url: "<?php echo site_url('registro_ffinanciamiento/populate_programs') ?>/" + fromResource,
                        contentType: "application/json;charset=utf-8",
                        dataType: 'json',
                        success: function (data) {
                            $('#catalogPrograms').empty();
                            $('#catalogPrograms').append("<option>Selecciona Programa..</option>");


                            $.each(data, function (i, name) {
                                $('#catalogPrograms').append('<option value="' + data[i].id + '">' + data[i].programa + '</option>');
                                cantElement++;
                            });


                        }
                    });
                }


                $("#catalogPrograms").change(function () {

                    var programSelected = $("#catalogPrograms option:selected").val();

                    populate_subprograms(programSelected);

                });


                function populate_subprograms(fromProgram) {
                    var cantElement = 0;
                    $('#catalogSubprograms').empty();
                    $('#catalogSubprograms').append("<option>Loading ....</option>");
                    $.ajax({
                        type: "GET",
                        url: "<?php echo site_url('registro_ffinanciamiento/populate_subprograms') ?>/" + fromProgram,
                        contentType: "application/json;charset=utf-8",
                        dataType: 'json',
                        success: function (data) {
                            $('#catalogSubprograms').empty();
                            $('#catalogSubprograms').append("<option>Selecciona subprograma..</option>");


                            $.each(data, function (i, name) {
                                $('#catalogSubprograms').removeAttr("disabled")
                                $('#catalogSubprograms').append('<option value="' + data[i].id + '">' + data[i].subprograma + '</option>');
                                cantElement++;
                            });

                            if (cantElement == 0) {
                                $('#catalogSubprograms').attr('disabled', 'disabled');
                            }
                        }
                    });
                }



                $('#vigila_federal').numeric(",");
                $('#vigila_estatal').numeric(",");
                $('#vigila_municipal').numeric(",");
                $('#vigila_otros').numeric(",");
                $('#total_acciones').numeric();
                $('#total_people_womans').numeric();
                $('#total_people_mans').numeric();



            });


            function sumaVigilado() {

                var total = 0;

                $(".t_vigilado").each(function () {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

                $("#total_vigilado").val(total);

            }



            function sumaPeople() {

                var total = 0;

                $(".t_people").each(function () {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });



                $("#total_people").val(total);

            }






            function jsUcfirst(string)
            {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }


            function guardarFF() {

                swal("Bien hecho!", "La Fuente de Financiamiento ha sido guardado.", "success");

            }



            function changeProgram() {


                var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                $("#titleProgram").html("<h1>" + programaSeleccionado + "</h1>");
                $("#titleProgram").fadeIn("slow");



            }







        </script>



    </body>

</html>
