 <!-- begin DASHBOARD CIRCLE TILES -->
                <div class="row">
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-university fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                   <br> P.Asignado PEF<br>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <center>$<?php echo  ($ffData['presupuesto_pef']) ?></center>
                                     <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                 
                                </div>
                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-search fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content purple">
                                <div class="circle-tile-description text-faded">
                                    <br>
                                    P. Vigilar por CS
                                </div>
                                <div class="circle-tile-number text-faded">
                                   <center> $<?php echo $ffData['presupuesto_vigilar_cs'] ?></center>
                                    <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                </div>
                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading orange">
                                    <i class="fa fa-balance-scale fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content orange">
                                <div class="circle-tile-description text-faded">
                                    <br>T.Recurso Asignado<br>
                                </div>
                                <div class="circle-tile-number text-faded">
                                  <center>$<?=$ffData['resumen_total_asignado']?></center>
                                   <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                </div>
                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                     <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading green">
                                    <i class="fa fa-handshake-o fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content green">
                                <div class="circle-tile-description text-faded">
                                   <br>Total Beneficiarios
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <center><?=$ffData['total_beneficiados']?></center>
                                     <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                </div>
                                <a href="#" class="circle-tile-footer" onClick="detallesFF_Beneficiarios()">Mas Detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-blue">
                                    <i class="fa fa-newspaper-o fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-blue">
                                <div class="circle-tile-description text-faded">
                                    <br>Convenio Suscrito?
                                </div>
                                <div class="circle-tile-number text-faded">
                                      <center><?=$ffData['convenio_suscrito']?></center>
                                    <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                </div>
                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    

       
                   
               
               
                </div>
                <!-- end DASHBOARD CIRCLE TILES -->










                   <div id="modalNew" class="modal fade modal-beneficiarios" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <div id="subTitle"><h4 class="modal-title"><strong>Detalle Beneficiarios - Fuente de Financiamiento</strong></h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form  action="#" id="form_programa" class="form-horizontal"   >


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Mujeres Beneficiadas:</label>
                                    <div class="col-md-6">
                                        <input  name="nombrePrograma" id="nombrePrograma" type="text" class="form-control"  value="<?=$ffData['mujeres_beneficiadas']?>" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Hombres Beneficiados:</label>
                                    <div class="col-md-6">
                                        <input  name="nombrePrograma" id="nombrePrograma" type="text" class="form-control" value="<?=$ffData['hombres_beneficiados']?>"  >
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Total Beneficiados</label>
                                    <div class="col-md-6">
                                        <input  name="nombrePrograma" id="nombrePrograma" type="text" class="form-control" value="<?=$ffData['total_beneficiados']?>" >
                                    </div>
                                </div>
                   

                           
                        </form>

                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->