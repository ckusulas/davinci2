<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ffinanciamiento_model extends CI_Model {

    
    public function __construct() {
        parent::__construct();
    }

   

    public function getListQuestions($find_year = "") {


        $sql = "SELECT * FROM  cs_catalogo_ffinanciamiento where año='$find_year' and status=1 order by orden";
        $query = $this->db->query($sql);

        $listQuestions =  $query->result();
     

        return($listQuestions);
    }


   public function getListRecourses($find_year = "") {

        $sql = "SELECT * FROM  cs_recursos where  status=1 ";
        $query = $this->db->query($sql);

        $listRecourses =  $query->result_array();
     

        return($listRecourses);
    }



    //se cambio de getFFInanciamientoDepto por getProgramaRecurso
   /* public function getFFinanciamientoDepto($fk_program, $departamento) {

    $this->db->where('fk_programa_proyectos', $fk_program);
        $this->db->where('fk_departamento', $departamento);       
       
        $q = $this->db->get('cs_registro_programa_recursos');

        $fk_programa_recurso = "";
        //$fk_programa_recurso = false;

          if ($q->num_rows() > 0) {

            //$fk_programa_recurso = true;

               foreach ($q->result() as $row) {
                    //$fk_programa_recurso = $row->id;                    

                $fk_programa_recurso = $row->id;                    
                }

        }

         return($fk_programa_recurso);
    }*/



   public function getListPrograms($fromResource, $find_year='2018') {

        $sql = "SELECT id, (programa) as programa FROM  cs_programas where  fk_recurso=" . $fromResource;
        $query = $this->db->query($sql);

        $listPrograms =  $query->result_array();     

        return($listPrograms);
    }

    



    public function getListSubprograms($fromProgram, $find_year='2018') {

        $sql = "SELECT id, subprograma FROM  cs_subprogramas where  fk_programa=" . $fromProgram;
        $query = $this->db->query($sql);

        $listSubprograms =  $query->result_array();
     

        return($listSubprograms);
    }


    public function getListInstanciaPromotora($find_year='2018') {

        $sql = "SELECT * FROM  cs_instancia_promotora order by instancia_promotora";
        $query = $this->db->query($sql);

        $listInstancia =  $query->result_array();
     

        return($listInstancia);
    }


    public function getListFondoEstatal() {

        $sql = "SELECT * FROM  cs_catalogo_fondo_estatal order by fondo";
        $query = $this->db->query($sql);

        $listFondoEstatal =  $query->result_array();
     

        return($listFondoEstatal);
    }


      public function getListInstanciaEjecutora($find_year='2018') {

        $sql = "SELECT * FROM  cs_instancia_ejecutora order by instancia_ejecutora";
        $query = $this->db->query($sql);

        $listInstancia =  $query->result_array();
     

        return($listInstancia);
    }



 




     public function getListCatalogFromQuestions($find_year = "") {
 

        $this->db->where('año', $find_year);
        $this->db->where('status', 1);
        $this->db->where('fk_contenido', 2);
        $query = $this->db->get("cs_catalogo_ffinanciamiento");
        //return($query);

        $listTablaForanea =  $query->result_array();

        $data = array();

        foreach($listTablaForanea as $row)
        {
        	$categoria_foranea = trim($row['categoria_foraneo']);

            $this->db->where('año', $find_year);
        	$this->db->where('categoria', $categoria_foranea);
	        $this->db->where('status', 1); 
	        $this->db->order_by("orden", "asc");

	        $query = $this->db->get("cs_catalogo_general");



        	$categoria_foranea =  $query->result_array();

        	$data = array(
        		"tabla_foranea" => $categoria_foranea,
        		"listado_catalogo" => $categoria_foranea


        	);

        	 


        } 

        return($data);
    }



     public function getListProgramasInscrito($fk_user, $departamento, $find_year='2019', $recurso="") {

        $sub_q = "";
        if($recurso!=""){
            $sub_q = " and fk_recurso={$recurso}";
        }

        
        //Privilegios de Admon, podran ver todos los programas
        $departam_Admon = array(1,3,4);
        if(in_array($departamento, $departam_Admon)) {
            $sub_user = "";        
        
        } else{
            
            $sub_user = " and usr_prog.fk_user=$fk_user ";    
        }


        $sql = "select prog.id as id, prog.programa as programa, prog.fk_recurso as fk_recurso from  cs_programas as prog left join cs_usuario_programa usr_prog on(prog.id = usr_prog.fk_programa)  where periodo='$find_year' $sub_user $sub_q  group by prog.id, prog.programa, prog.fk_recurso order by programa";
         echo $sql;
        $query = $this->db->query($sql);

        $listInstancia =  $query->result_array();
     

        return($listInstancia);
    }

     public function getRecurso($programa) {

        if($programa!="agregar"){
            $sql = "SELECT fk_recurso FROM  cs_programas where id={$programa} ";
            $query = $this->db->query($sql);

            $result =  $query->result_array();     
           

            return($result[0]['fk_recurso']);
        }
    }


    public function getNamePrograma($id_programa) {

        $sql = "SELECT programa FROM  cs_programas where id={$id_programa} ";
        $query = $this->db->query($sql);

        $result =  $query->result_array();     
       

        return($result[0]['programa']);
    }

 



     public function getMunicipios() {

        $sql = "SELECT * FROM  cs_municipios order by municipio ";
        $query = $this->db->query($sql);

        $listMunicipios =  $query->result_array();     

        return($listMunicipios);
    }


    public function findLocalidades($municipio) {

        $sql = "SELECT * FROM  cs_localidades where  fk_municipio=" . $municipio . " order by localidad";
            //   echo $sql;
        $query = $this->db->query($sql);



        $listLocalidades =  $query->result_array();
     

        return($listLocalidades);
    }

     public function findCatalogoMaterial($tipoMaterial) {

        if($tipoMaterial =="2"){
            $tipo_material = "material_capacitacion";
        }

        if($tipoMaterial =="1"){
            $tipo_material = "material_difusion";
        }

        $sql = "SELECT * FROM  cs_catalogo_general where  categoria='" . $tipo_material . "' order by campo";
        $query = $this->db->query($sql);

        $listMaterial =  $query->result_array();
     

        return($listMaterial);
    }


    public function getListMaterial($idProgram, $departamento=""){

      
        $subQ_program = "";

      
        if($idProgram!="" && $idProgram!="todos"){
            $subQ_program = " and cmat.fk_programa = $idProgram ";
        }


        $subQ_dep = "";
      
        if($departamento == ""){
            $departamento = $this->session->userdata('departamento');
        }

        //Deptos-Perfil de Administradores
        $subQ_dep = "";
        $departam_Admon = array(1,3,4);
               
        if( $idProgram == "todos") {                  
            
             $subQ_program = "";
        }

        
        
        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_dep = " and cmat.fk_departamento = $departamento ";

        }




        $sql = "select cmat.id, programa, entidad, cat.campo as nombre_material, cantidad_producida, cantidad_distribuida, archivo_material,tipo_material from cs_registro_material as cmat left join cs_programas prog on(fk_programa=prog.id) left join cs_catalogo_general cat on (cat.id=fk_nombre_material) left join cs_material mater on(cmat.fk_tipo_material = mater.id)  {$subQ_program} {$subQ_dep}";

       // echo $sql;
         $query = $this->db->query($sql);
         $listMaterial =  $query->result_array();     

        return($listMaterial);


    }


    public function getMaterialInfo(){
        $sql = "select campo as material, cantidad_producida from cs_registro_material material left join cs_catalogo_general cat on(material.nombre_material = cat.id)";

    }



    public function getListProyectos($idProgram="", $departamento="", $id_proy=""){

        $subQ_program = "";

      
        if($idProgram!="" && $idProgram!="todos"){
            $subQ_program = " and cs_p.fk_program = $idProgram ";
        }

    
        if($departamento == ""){
            $departamento = $this->session->userdata('departamento');
        }


        //Deptos-Perfil de Administradores
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);
               
        if( $idProgram == "todos") {                  
            
             $subQ_program = "";
        }

         if( $idProgram == "federales") {                  
            
             $subQ_program = " and cs_prog.fk_recurso=1 ";
        }

          if( $idProgram == "estatales") {                  
            
             $subQ_program = " and cs_prog.fk_recurso=2 ";
        }
        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and cs_p.fk_departamento = $departamento ";

        }
        


        $find_proy = "";
        if($id_proy!=""){
            $find_proy = " and cs_p.id=$id_proy ";
        }
        
     

       $sql = "select  fk_programa_recurso,fk_program, cs_p.id, cs_prog.programa, cs_p.nombre_proyecto , cs_rec.nombre as recurso, depto.departamento, depto.id as id_depto, beneficio,recurso_asignado_vigilar,total_beneficiados, cs_p.fk_departamento, (select localidad from cs_localidades as loc2 where reg_com.fk_localidad=loc2.id) as localidad_comite, estatus_proyecto,eje.instancia_ejecutora,hombres_beneficiados,mujeres_beneficiadas,municipio,localidad, en_picaso,  capac.nombre_capacitacion, (select  campo FROM  cs_catalogo_general as catalogo where categoria='capacitacion_figura' and catalogo.id = capac.fk_figura_capacitada) as figura_capacitada, fecha_imparticion, recurso_ejecutado_vigilado, fecha_inicio_programada,fecha_final_programada, nombre_comite, reg_com.id as fk_comite, capac.id as fk_capacitacion, fecha_constitucion,clave_registro,(select count(id) from cs_registro_comite_integrantes where fk_comite=reg_com.id and nombre_integrante!='' and sexo_integrante='Masculino') as Masculino, (select count(id) from cs_registro_comite_integrantes where fk_comite=reg_com.id and nombre_integrante!='' and sexo_integrante='Femenino') as Femenino, (select count(id) from cs_registro_comite_integrantes where fk_comite=reg_com.id and nombre_integrante!='')  as TotBeneficiarios, denominacion   from cs_registro_proyecto as cs_p left join cs_programas cs_prog on cs_p.fk_program = cs_prog.id left join cs_beneficios as benef on cs_p.fk_beneficio = benef.id left join cs_instancia_ejecutora  eje on cs_p.fk_instancia_ejecutora = eje.id left join  cs_recursos cs_rec on cs_prog.fk_recurso=cs_rec.id  left join cs_municipios as muni on cs_p.fk_municipio=muni.id  left join cs_localidades as local on cs_p.fk_localidad=local.id left join cs_departamento as depto on depto.id = cs_p.fk_departamento  left join cs_registro_comite as reg_com on(reg_com.accion_conexion = cs_p.id and reg_com.eliminado is null)  left join cs_registro_capacitacion as capac on capac.fk_accion_conexion = reg_com.id and capac.eliminado is null     where cs_p.eliminado is null  {$subQ_program} {$subQ_departamento} {$find_proy} order by cs_p.id ";

      //echo $sql;

       

        $query = $this->db->query($sql);
        $listProyectos =  $query->result_array();
     

        return($listProyectos);
    }



    public function getListProyectos_tabla($idProgram="", $departamento="", $id_proy=""){

        $subQ_program = "";

      
        if($idProgram!="" && $idProgram!="todos"){
            $subQ_program = " and cs_p.fk_program = $idProgram ";
        }

    
        if($departamento == ""){
            $departamento = $this->session->userdata('departamento');
        }


        //Deptos-Perfil de Administradores
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);
               
        if( $idProgram == "todos") {                  
            
             $subQ_program = "";
        }


         if( $idProgram == "federales") {                  
            
             $subQ_program = " and cs_prog.fk_recurso=1 ";
        }

          if( $idProgram == "estatales") {                  
            
             $subQ_program = " and cs_prog.fk_recurso=2 ";
        }
        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and cs_p.fk_departamento = $departamento ";

        }
        


        $find_proy = "";
        if($id_proy!=""){
            $find_proy = " and cs_p.id=$id_proy ";
        }
        
 //left join cs_registro_comite as reg_com on(reg_com.accion_conexion = cs_p.id and reg_com.eliminado is null)

       $sql = "select  fk_programa_recurso,fk_program, cs_p.id, cs_prog.programa, cs_p.nombre_proyecto , cs_rec.nombre as recurso, depto.departamento, beneficio,recurso_asignado_vigilar,monto_recurso_asignado_vig_federal, monto_recurso_asignado_vig_estatal,monto_recurso_asignado_vig_municipal, monto_recurso_asignado_vig_otros,monto_recurso_asignado_ejec_federal,monto_recurso_asignado_ejec_estatal,monto_recurso_asignado_ejec_municipal,monto_recurso_asignado_ejec_otros, total_beneficiados, cs_p.fk_departamento, estatus_proyecto,eje.instancia_ejecutora,hombres_beneficiados,mujeres_beneficiadas,municipio,localidad, en_picaso, recurso_ejecutado_vigilado,fecha_inicio_programada,fecha_final_programada,   denominacion, ( select count(reg_com.id) from  cs_registro_comite as reg_com where reg_com.accion_conexion = cs_p.id and reg_com.eliminado is null )  as total_comites from cs_registro_proyecto as cs_p left join cs_programas cs_prog on cs_p.fk_program = cs_prog.id left join cs_beneficios as benef on cs_p.fk_beneficio = benef.id left join cs_instancia_ejecutora  eje on cs_p.fk_instancia_ejecutora = eje.id left join  cs_recursos cs_rec on cs_prog.fk_recurso=cs_rec.id  left join cs_municipios as muni on cs_p.fk_municipio=muni.id  left join cs_localidades as local on cs_p.fk_localidad=local.id left join cs_departamento as depto on depto.id = cs_p.fk_departamento where cs_p.eliminado is null  {$subQ_program} {$subQ_departamento} {$find_proy}   order by cs_p.id ";

      //   echo $sql;

        $query = $this->db->query($sql);
        $listProyectos =  $query->result_array();
     

        return($listProyectos);
    }


    public function getListSeguimientos_tabla($idProgram="", $departamento="", $id_proy=""){

        $subQ_program = "";

      
        if($idProgram!="" && $idProgram!="todos"){
            $subQ_program = " and cs_p.fk_program = $idProgram ";
        }

    
        if($departamento == ""){
            $departamento = $this->session->userdata('departamento');
        }


        //Deptos-Perfil de Administradores
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);
               
        if( $idProgram == "todos") {                  
            
             $subQ_program = "";
        }


        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and cs_p.fk_departamento = $departamento ";

        }
        

 
        
 //left join cs_registro_comite as reg_com on(reg_com.accion_conexion = cs_p.id and reg_com.eliminado is null)

        //municipio, localidad
       $sql = "select  cs_s.id as id_seguimiento, cs_p.id as id_proyecto, cs_p.nombre_proyecto, cs_com.id as id_comite, cs_com.nombre_comite, cs_s.updated as fecha_seguimiento,  municipio, localidad from cs_registro_seguimiento as cs_s left join cs_registro_comite cs_com on cs_s.fk_comite = cs_com.id left join cs_registro_proyecto as cs_p on cs_p.id = cs_com.accion_conexion left join cs_municipios as cs_muni on cs_p.fk_municipio = cs_muni.id left join cs_localidades as cs_local on cs_p.fk_localidad=cs_local.id  where cs_s.eliminado is null  {$subQ_program} {$subQ_departamento}   order by cs_s.id ";

        //echo $sql;

        $query = $this->db->query($sql);
        $listProyectos =  $query->result_array();
     

        return($listProyectos);
    }


    public function getProgramaRecurso($ejercicioFiscal="2019", $fk_programa, $fk_departamento){

        //$sql = "select id from cs_registro_programa_recursos where ejercicio_fiscal "

        //ejercicio_fiscal, fk_programa_proyectos, fk_departamento


        $this->db->where('fk_programa_proyectos', $fk_programa);
        $this->db->where('ejercicio_fiscal', $ejercicioFiscal);
        $this->db->where('fk_departamento', $fk_departamento);

      
        $q = $this->db->get('cs_registro_programa_recursos');

         $sql = $this->db->last_query();

        // echo "sql:" . $sql;

        $id_programaRecurso = "";
        if ($q->num_rows() > 0) {
          foreach ($q->result() as $row) {
            $id_programaRecurso = $row->id;
          }
        }

        return $id_programaRecurso;
    }


     public function getPresupuestoVigilar($id_programa_recurso){

   
        $this->db->where('id', $id_programa_recurso);

      
        $q = $this->db->get('cs_registro_programa_recursos');

         $sql = $this->db->last_query();

        // echo "sql:" . $sql;

        $presupuesto_vigilar = "";
        if ($q->num_rows() > 0) {
          foreach ($q->result() as $row) {
            $presupuesto_vigilar = $row->presupuesto_vigilar_cf;
          }
        }

        return $presupuesto_vigilar;
    }


     public function getListProyectosFull($idProgram=""){

        $subQ_program = "";

       //  echo "<b>idPrograma:".$idProgram."</b>";

        if($idProgram!="" && $idProgram!="todos"){
            $subQ_program = " and cs_p.fk_program = $idProgram ";
        }

       // echo "<b>subQ_progam:" . $subQ_program . "</b><br>";


        $departamento = $this->session->userdata('departamento');
        /*if($idProgram =="todos"){
            $subQ_program = "";
        } */

        //Deptos-Perfil de Administradores
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);
        if(in_array($departamento,$departam_Admon) && $idProgram == "todos") {                  
            
             $subQ_program = "";
        }else{
             if(!in_array($departamento,$departam_Admon)){
                $subQ_departamento = " and cs_p.fk_departamento = $departamento ";
             }
        }


         if( $idProgram == "federales") {                  
            
             $subQ_program = " and cs_prog.fk_recurso=1 ";
        }

          if( $idProgram == "estatales") {                  
            
             $subQ_program = " and cs_prog.fk_recurso=2 ";
        }
        
 

       $sql = "select  cs_p.id, cs_prog.programa, cs_p.nombre_proyecto , cs_rec.nombre as recurso, depto.departamento, beneficio,recurso_asignado_vigilar,total_beneficiados, eje.instancia_ejecutora,hombres_beneficiados,mujeres_beneficiadas,municipio,localidad, recurso_ejecutado_vigilado   from cs_registro_proyecto as cs_p left join cs_programas cs_prog on cs_p.fk_program = cs_prog.id left join cs_beneficios as benef on cs_p.fk_beneficio = benef.id left join cs_instancia_ejecutora  eje on cs_p.fk_instancia_ejecutora = eje.id left join  cs_recursos cs_rec on cs_prog.fk_recurso=cs_rec.id  left join cs_municipios as muni on cs_p.fk_municipio=muni.id  left join cs_localidades as local on cs_p.fk_localidad=local.id left join cs_departamento as depto on depto.id = cs_p.fk_departamento  where cs_p.eliminado is null  {$subQ_program} {$subQ_departamento} order by cs_p.id ";

      //echo $sql;

       

        $query = $this->db->query($sql);
        $listProyectos =  $query->result_array();
     

        return($listProyectos);
    }



    public function getTotalRecursoAsignadoVigilar($programa = "")
    {

          $subQ_program = "";

        if($programa!="" ){
            $subQ_program = " and fk_program = $programa ";
        }

        if($programa=="todos" ){
            $subQ_program = "";
        }


         $departamento = $this->session->userdata('departamento');

 
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);


        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and cs_p.fk_departamento = $departamento ";

        }

        if($programa=="" || $programa =="todos"){
            $sql = "select sum(recurso_asignado_vigilar) as total from cs_registro_proyecto as cs_p where eliminado is null  {$subQ_program} {$subQ_departamento} ";
        }
        else{
             $sql = "select sum(recurso_asignado_vigilar) as total from cs_registro_proyecto as cs_p where fk_program= $programa and eliminado is null  {$subQ_program} {$subQ_departamento} ";
        }

 

        $query = $this->db->query($sql);
        $result =  $query->result_array();
         
        $total = 0;

        foreach ($result as $row){
            $total = $row['total'];
        }
        

        return($total);
     


    }


    public function getTotalBeneficiadosFF($programa = "")
    {


        if($programa=="todos" ){
            $subQ_program = "";
        }

          if($programa=="federales" ){
            $subQ_program = " and fk_resource=1 ";
        }

           if($programa=="estatales" ){
            $subQ_program = " and fk_resource=2 ";
        }

        

    $sql = "select mujeres_beneficiadas, hombres_beneficiados, total_beneficiados from cs_registro_programa_recursos where fk_program=$programa $subQ_program and eliminado is null";



    $query = $this->db->query($sql);
    $result =  $query->result_array();
         
        return($result);

    }

    


   public function getTotalBeneficiados($programa = "")
    {


        $subQ_program = "";

        if($programa!="" ){
            $subQ_program = " and fk_program = $programa ";
        }

        if($programa=="todos" ){
            $subQ_program = "";
        }


         $departamento = $this->session->userdata('departamento');

 
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);


        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and cs_p.fk_departamento = $departamento ";

        }

        if($programa=="" || $programa == "todos"){
            $sql = "select sum(total_beneficiados) as total from cs_registro_proyecto as cs_p where eliminado is null {$subQ_program} {$subQ_departamento} ";
        }
        else{
             $sql = "select sum(total_beneficiados) as total from cs_registro_proyecto as cs_p where   eliminado is null {$subQ_program} {$subQ_departamento} ";
        }

        //echo $sql;

        $query = $this->db->query($sql);
        $result =  $query->result_array();
         
        $total = 0;

        foreach ($result as $row){
            $total = $row['total'];
        }
        if($total == ""){
            $total = 0;
        }

        return($total);
     


    }


    public function getTotalBeneficiadosHombres($programa = "")
    {


        $subQ_program = "";

        if($programa!="" ){
            $subQ_program = " and fk_program = $programa ";
        }

        if($programa=="todos" ){
            $subQ_program = "";
        }


         $departamento = $this->session->userdata('departamento');

 
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);


        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and cs_p.fk_departamento = $departamento ";

        }






        if($programa==""){
            $sql = "select sum(hombres_beneficiados) as total from cs_registro_proyecto as cs_p where eliminado is null {$subQ_program} {$subQ_departamento} ";
        }
        else{
             $sql = "select sum(hombres_beneficiados) as total from cs_registro_proyecto as cs_p where eliminado is null {$subQ_program} {$subQ_departamento} ";
        }

 

        $query = $this->db->query($sql);
        $result =  $query->result_array();
         
        $total = 0;

        foreach ($result as $row){
            $total = $row['total'];
        }
        if($total == ""){
            $total = 0;
        }

        return($total);
     


    }

    public function getTotalBeneficiadosMujeres($programa = "")
    {   



         $subQ_program = "";

        if($programa!="" ){
            $subQ_program = " and fk_program = $programa ";
        }

        if($programa=="todos" ){
            $subQ_program = "";
        }


         $departamento = $this->session->userdata('departamento');

 
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);


        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and cs_p.fk_departamento = $departamento ";

        }










        if($programa==""){
            $sql = "select sum(mujeres_beneficiadas) as total from cs_registro_proyecto as cs_p where eliminado is null {$subQ_program} {$subQ_departamento} ";
        }
        else{
             $sql = "select sum(mujeres_beneficiadas) as total from cs_registro_proyecto as cs_p where eliminado is null {$subQ_program} {$subQ_departamento} ";
        }

 

        $query = $this->db->query($sql);
        $result =  $query->result_array();
         
        $total = 0;

        foreach ($result as $row){
            $total = $row['total'];
        }
        if($total == ""){
            $total = 0;
        }

        return($total);
     


    }







    public function getQtyBeneficio($beneficio,$programa = "")
    {


        $subQ_program = "";

        if($programa!="" ){
            $subQ_program = " and fk_program = $programa ";
        }

        if($programa=="todos" ){
            $subQ_program = "";
        }


         $departamento = $this->session->userdata('departamento');

      //   echo "departamento:" . $departamento;

 
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);


        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and cs_p.fk_departamento = $departamento ";

        }





        if($programa==""){
            $sql = "select count(fk_beneficio) as qty from cs_registro_proyecto as cs_p where fk_beneficio=$beneficio and eliminado is null {$subQ_program} {$subQ_departamento} ";
        }
        else{
             $sql = "select count(fk_beneficio) as qty from cs_registro_proyecto as cs_p where fk_beneficio=$beneficio and eliminado is null {$subQ_program} {$subQ_departamento} ";
        }

 

 

        $query = $this->db->query($sql);
        $result =  $query->result_array();
         
        $total = 0;

        foreach ($result as $row){
            $total = $row['qty'];
        }
        if($total == ""){
            $total = 0;
        }

        return($total);
     


    }

    public function getLocalidadedMunic($fk_municipio){
        $sql = " select id, localidad from cs_localidades where fk_municipio={$fk_municipio} order by localidad";
        $query = $this->db->query($sql);
        $listFF =  $query->result_array();
     

        return($listFF);

    }


     public function getQtyComites($programa){

        //$sql = "select count(id) as qty from cs_registro_comite where fk_programa={$programa} and eliminado is null";
        //echo $sql;

        $subQ_program = "";

        if($programa!="" ){
            $subQ_program = " and proy.fk_program = $programa ";
        }

        if($programa=="todos" ){
            $subQ_program = "";
        }


         $departamento = $this->session->userdata('departamento');



        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);


        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and proy.fk_departamento = $departamento ";

        }
        


        /*if(in_array($departamento,$departam_Admon)) {                  
            
            $subQ_program = "";
        }else{
             $subQ_departamento = " and cs_p.fk_departamento = $departamento ";
        }*/


           $sql = " select comi.id, nombre_comite, program.ejercicio_fiscal, comi.fecha_constitucion, proy.nombre_proyecto, comi.clave_registro, comi.qty_hombres, comi.qty_mujeres,  proy.id as id_obra, proy.estatus_proyecto,  proy.recurso_ejecutado_vigilado ,  proy.recurso_asignado_vigilar, ejec.instancia_ejecutora, muni.municipio, localidad from cs_registro_comite as comi LEFT JOIN cs_registro_comite_obra as rel on (comi.id = rel.fk_comite)  JOIN cs_registro_proyecto as proy ON(rel.fk_obra = proy.id  )  LEFT JOIN cs_registro_programa_recursos as program on (proy.fk_program = program.fk_programa_proyectos and program.eliminado=NULL) LEFT JOIN cs_instancia_ejecutora  as ejec ON(proy.fk_instancia_ejecutora = ejec.id) LEFT JOIN cs_municipios as muni ON(proy.fk_municipio =  muni.id) LEFT JOIN cs_localidades as loca ON(proy.fk_localidad = loca.id) where comi.eliminado is null {$subQ_program} {$subQ_departamento}  order by comi.id";


        $query = $this->db->query($sql);
        $result =  $query->num_rows();
         
        $total = $result;

         

        return($total);


     }

     public function getQtyDocumentoNormativo($fk_program, $fk_departamento){

        $sql = "select count(*) as cantidad from cs_adjuntos where seccion='doc_normativo' and (objeto='patcs' or objeto='esquema_cs' or objeto='oficio_envio') and fk_program=$fk_program and fk_departamento=$fk_departamento";

        $query = $this->db->query($sql);
        $qtyDocs =  $query->result_array();

        return($qtyDocs);

     }



     public function getSumaRecursosProyecto($fk_ffinanciamiento){
 
        $sql = "select sum(monto_recurso_asignado_vig_federal) as sum_monto_vig_federal, sum(monto_recurso_asignado_vig_estatal) as sum_monto_vig_estatal, sum(monto_recurso_asignado_vig_municipal) as sum_monto_vig_municipal, sum(monto_recurso_asignado_vig_otros) as sum_monto_vig_otros from cs_registro_proyecto where fk_programa_recurso=$fk_ffinanciamiento";


        $query = $this->db->query($sql);
        $dataFF =  $query->result_array();

        return($dataFF);
    }



    public function getDataFFinanciamiento($fk_ffinanciamiento){

        $sql = "select ffinanciam.*, cuadrof,cuadroe,cuadrom,cuadroo  from cs_registro_programa_recursos as ffinanciam LEFT JOIN cs_programas on ffinanciam.fk_programa_proyectos=cs_programas.id where ffinanciam.id=$fk_ffinanciamiento";


        $query = $this->db->query($sql);
        $dataFF =  $query->result_array();

        return($dataFF);
    }



    public function getListFF($idProgram=""){

        $subQ_program = "";

        if($idProgram!="" && $idProgram!="todos" ){
            $subQ_program = " and fk_programa_proyectos = $idProgram ";
        }
 
       
        $departamento = $this->session->userdata('departamento');
       

        //Deptos-Perfil de Administradores
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);
               
         

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and  fk_departamento = $departamento ";

        }
        






       $sql = "select cs_prec.id, cs_programas.programa,cs_programas.id as programa_id, departamento, federal_recurso_fuente_financiamiento_recurso1,fondo,municipal_recurso_fuente_financiamiento_recurso1,otros_recurso_fuente_financiamiento_recurso1, cs_recursos.nombre,estatal_recurso_fuente_financiamiento_recurso1,  ejercicio_fiscal, presupuesto_pef, presupuesto_vigilar_cf, mujeres_beneficiadas, hombres_beneficiados, total_beneficiados, convenio_suscrito, resumen_total_federal, resumen_total_estatal, resumen_total_municipal, resumen_total_otros, resumen_total_asignado  from cs_registro_programa_recursos as cs_prec left join cs_recursos on fk_resource = cs_recursos.id left join cs_programas on fk_programa_proyectos=cs_programas.id left join cs_departamento on cs_prec.fk_departamento=cs_departamento.id  left join cs_catalogo_fondo_estatal fondoE on cs_prec.municipal_recurso_fuente_financiamiento_recurso1=fondoE.id  where eliminado is NULL {$subQ_program}  {$subQ_departamento}   ";

     // echo $sql;

        $query = $this->db->query($sql);
        $listFF =  $query->result_array();
     

        return($listFF);
    }



    public function getListComites($idProgram=""){

        $subQ_program = "";

        if($idProgram!="" && $idProgram!="todos"){
            $subQ_program = " and fk_program = $idProgram ";
        }


        $departamento = $this->session->userdata('departamento');

              

        //Deptos-Perfil de Administradores
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);
        

        if( $idProgram == "todos") {                  
            
             $subQ_program = "";
        }
        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and proy.fk_departamento = $departamento ";

        }
        



       $sql = " select comi.id, nombre_comite, program.ejercicio_fiscal, ejec.instancia_ejecutora, ejec.tipo_ejecutora, cs_prog.programa, comi.fecha_constitucion, proy.nombre_proyecto, comi.clave_registro, comi.qty_hombres, comi.qty_mujeres,  proy.id as id_obra, proy.estatus_proyecto,  proy.recurso_ejecutado_vigilado ,  proy.recurso_asignado_vigilar, ejec.instancia_ejecutora, muni.municipio, loca.localidad, (select localidad from cs_localidades as loca2 where comi.fk_localidad=loca2.id) as localidad_comite, proy.fk_program from cs_registro_comite as comi LEFT JOIN cs_registro_comite_obra as rel on (comi.id = rel.fk_comite)  JOIN cs_registro_proyecto as proy ON(rel.fk_obra = proy.id and proy.eliminado is null)  LEFT JOIN cs_registro_programa_recursos as program on (proy.fk_program = program.fk_programa_proyectos and program.eliminado=NULL) left join cs_programas cs_prog on proy.fk_program = cs_prog.id LEFT JOIN cs_instancia_ejecutora  as ejec ON(proy.fk_instancia_ejecutora = ejec.id) LEFT JOIN cs_municipios as muni ON(proy.fk_municipio =  muni.id) LEFT JOIN cs_localidades as loca ON(proy.fk_localidad = loca.id) where comi.eliminado is null {$subQ_program} {$subQ_departamento} order by comi.id 

  ";



    
 
         

        $query = $this->db->query($sql);
        $listFF =  $query->result_array();
     

        return($listFF);
    }



      public function getInfoComite($id_comite){

        $subQ_program = "";

       

    /*    if($idProgram!="" && $idProgram!="todos"){
            $subQ_program = " and fk_program = $idProgram ";
        }
*/

        $departamento = $this->session->userdata('departamento');

              

        //Deptos-Perfil de Administradores
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);
        

       /* if( $idProgram == "todos") {                  
            
             $subQ_program = "";
        }*/
        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and proy.fk_departamento = $departamento ";

        }
        



       $sql = " select comi.id, nombre_comite, program.ejercicio_fiscal, ejec.instancia_ejecutora, ejec.tipo_ejecutora, cs_prog.programa, comi.fecha_constitucion, proy.nombre_proyecto, comi.clave_registro, comi.qty_hombres, comi.qty_mujeres,  proy.id as id_obra, proy.estatus_proyecto,  proy.recurso_ejecutado_vigilado ,  proy.recurso_asignado_vigilar, ejec.instancia_ejecutora, muni.municipio, loca.localidad, (select localidad from cs_localidades as loca2 where comi.fk_localidad=loca2.id) as localidad_comite, proy.fk_program from cs_registro_comite as comi LEFT JOIN cs_registro_comite_obra as rel on (comi.id = rel.fk_comite)  JOIN cs_registro_proyecto as proy ON(rel.fk_obra = proy.id and proy.eliminado is null)  LEFT JOIN cs_registro_programa_recursos as program on (proy.fk_program = program.fk_programa_proyectos and program.eliminado=NULL) left join cs_programas cs_prog on proy.fk_program = cs_prog.id LEFT JOIN cs_instancia_ejecutora  as ejec ON(proy.fk_instancia_ejecutora = ejec.id) LEFT JOIN cs_municipios as muni ON(proy.fk_municipio =  muni.id) LEFT JOIN cs_localidades as loca ON(proy.fk_localidad = loca.id) where comi.eliminado is null {$subQ_program} {$subQ_departamento} and comi.id={$id_comite} ";


       //echo $sql;
    
 
         

        $query = $this->db->query($sql);
        $listFF =  $query->result_array();
     

        return($listFF);
    }





    public function getListCapacitacion($idProgram=""){


        $subQ_program = "";

        if($idProgram!="" && $idProgram!="todos"){
            $subQ_program = " and cap.fk_programa = $idProgram ";
        }


        $departamento = $this->session->userdata('departamento');

              

        //Deptos-Perfil de Administradores
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);
        

        if( $idProgram == "todos") {                  
            
             $subQ_program = "";
        }
        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and cap.fk_departamento = $departamento ";

        }

     
        //select municipio from cs_municipios cs_m where cs_m.id = com.accion_conexion


      /* $sql = "select cap.id, nombre_capacitacion, cat_g.campo as tema,  cat_g2.campo as fig_cap, oficio_aprobacion, select municipio from cs_municipios cs_m where cs_m.id = com.accion_conexion,fecha_imparticion, num_participantes, beneficiarios_hombres_capacitados, beneficiarios_mujeres_capacitados, integrantes_hombres_capacitados, integrantes_mujeres_capacitados from cs_registro_capacitacion as cap LEFT JOIN cs_municipios as mun on cap.fk_municipio=mun.id  LEFT JOIN cs_localidades as loc on cap.fk_localidad = loc.id  LEFT JOIN cs_registro_comite as com on cap.fk_accion_conexion = com.id  LEFT JOIN cs_catalogo_general as cat_g on cap.fk_tematica=cat_g.id left JOIN cs_catalogo_general as cat_g2 on cap.fk_figura_capacitada = cat_g2.id  where eliminado is null  {$subQ_program} {$subQ_departamento}";*/


        $sql = "select cap.id, nombre_capacitacion, cat_g.campo as tema,  cat_g2.campo as fig_cap, oficio_aprobacion, (select municipio from cs_municipios cs_m where cs_m.id = proy.fk_municipio) as municipio,fecha_imparticion, num_participantes, beneficiarios_hombres_capacitados, beneficiarios_mujeres_capacitados, integrantes_hombres_capacitados, integrantes_mujeres_capacitados from cs_registro_capacitacion as cap LEFT JOIN cs_municipios as mun on cap.fk_municipio=mun.id  LEFT JOIN cs_localidades as loc on cap.fk_localidad = loc.id  LEFT JOIN cs_registro_comite as com on cap.fk_accion_conexion = com.id left JOIN cs_registro_proyecto as proy on proy.id=com.accion_conexion  LEFT JOIN cs_catalogo_general as cat_g on cap.fk_tematica=cat_g.id left JOIN cs_catalogo_general as cat_g2 on cap.fk_figura_capacitada = cat_g2.id  where cap.eliminado is null  {$subQ_program} {$subQ_departamento}";

 
 
        //echo $sql;
      

        $query = $this->db->query($sql);
        $listCap =  $query->result_array();
     

        return($listCap);
    }




    public function getListProysBeneficios($idProgram="",$fk_beneficio){



        $subQ_program = "";

        if($idProgram!="" && $idProgram!="todos"){
            $subQ_program = " and fk_program = $idProgram ";
        }


        $departamento = $this->session->userdata('departamento');

              

        //Deptos-Perfil de Administradores
        $subQ_departamento = "";
        $departam_Admon = array(1,3,4);
        

        if( $idProgram == "todos") {                  
            
             $subQ_program = "";
        }
        

        if(!in_array($departamento,$departam_Admon)){
        
           $subQ_departamento = " and proy.fk_departamento = $departamento ";

        }

       

       $sql = "select id, nombre_proyecto, oficio_aprobacion_picaso, oficio_aprobacion_equivalente from cs_registro_proyecto as proy where fk_beneficio=$fk_beneficio and eliminado is null  {$subQ_program} {$subQ_departamento} order by id ";
 

        $query = $this->db->query($sql);
        $listProys =  $query->result_array();
     

        return($listProys);
    }


    public function getListRamos(){
        $sql = "select * from cs_catalogo_ramo where status=1 order by id";

        $query = $this->db->query($sql);
        $listRamos =  $query->result_array();

        return($listRamos);
    }




    public function getIntegrantesComite($fk_comite){

        $sql = "select * from cs_registro_comite_integrantes where fk_comite={$fk_comite} ";

        $query = $this->db->query($sql);
        $listIntegrantes =  $query->result_array();


        return($listIntegrantes);
    }

    public function getProyectoMetas($fk_proyecto){

        $sql = "select * from cs_registro_proyecto_metas where fk_proyecto={$fk_proyecto} ";

        $query = $this->db->query($sql);
        $listMetas =  $query->result_array();


        return($listMetas);
    }
    


    public function getQtyIntegrantesComite($fk_comite){

        $sql = "select * from cs_registro_comite_integrantes where fk_comite={$fk_comite} and nombre_integrante <>''";

        $query = $this->db->query($sql);
        $qtyIntegrantes =  $query->num_rows();


        return($qtyIntegrantes);
    }


    public function getProgramaSuscrito($programa = "")
    {

        $sql = "select convenio_suscrito from cs_registro_programa_recursos where fk_programa_proyectos=$programa";

        $query = $this->db->query($sql);
        $result =  $query->result_array();
             
            return($result);

    }
 

    public function getURL_adjunto($fk_program, $fk_departamento, $seccion, $objeto){

        $sql = "select URL from cs_adjuntos where fk_program={$fk_program} and fk_departamento={$fk_departamento} and seccion='$seccion' and objeto='$objeto'  order by id desc limit 1";

    // echo $sql;


        $query = $this->db->query($sql);        
        $listRecourses =  $query->result_array();
     

        return($listRecourses);
    }



    
     

}
