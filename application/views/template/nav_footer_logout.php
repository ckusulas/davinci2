 <div id="logout">
        <div class="logout-message">
            <?php 
            $photo = $this->session->userdata('photo')
            ?>
            <img class="img-circle img-logout" width="150px" height="150px" src="<?php echo asset_url();?>img/<?=$photo?>" alt="">
            <h3>
                <i class="fa fa-sign-out text-green"></i> Listo para Salir?
            </h3>
            <p>Estas seguro, de salir de tu session actual?.</p>
            <ul class="list-inline">
                <li>
                    <a href="<?php echo site_url()?>/" class="btn btn-green">
                        <strong>Salir</strong>
                    </a>
                </li>
                <li>
                    <button class="logout_close btn btn-green">Cancelar</button>
                </li>
            </ul>
        </div>
    </div>