
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>

       <?php $this->view('template/header.php'); ?>   
</head>

<body>

    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                   <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                           <h1>
                                               
                                            </h1>
                                         </td>
                                        <td align="right"> 
                                              <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='resetearForma()'><i class="fa fa-floppy-o"></i> Limpiar</button> 
                                          <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='saveElement()'><i class="fa fa-floppy-o"></i> Editar</button> 
                                           <button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='deleteElement()'><i class="fa fa-floppy-o"></i> Nuevo</button> 

                                            <button type="button" class="btn btn-success waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Material de Capacitación</i>
                                </li>
                                <li class="active">Registar Material de Capacitación</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
                
                
                 
                
 




                 <!-- Funcion del Comite -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Distribuir Materiales de Capacitación</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#funciones"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="funciones" class="panel-collapse collapse in">
                                            <div class="portlet-body">
                                            <form class="form-horizontal" id="form_distribuir_mat_capacitacion">

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">Material:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="material" name="material"  >
                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">Producido:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="producido" name="producido"  >
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label  class="col-sm-4 ">Municipio:</label>
                                                    <div class="col-sm-6">
                                                         <select class="form-control" id="municipio" name="municipio">
                                                            <option>Seleccione...</option>

                                                            

                                                            <?php
                                                                foreach ($listMunicipios as $row) {
                                                                    $selected = "";
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['municipio']) . "</option>\n";
                                                                }
                                                            ?>
                                                       
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                
                                                    <label  class="col-sm-4">Localidad:</label>
                                                    <div class="col-sm-6">
                                                         <select class="form-control" id="localidad" name="localidad">
                                                            <option>Seleccione...</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">Cantidad a Distribuir:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="nombreMaterial" name="cantidadDistribuir"  >
                                                    </div>
                                                </div>
                                              

                                             
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 ">Fecha Distribuición:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="fechaDistribuicion" name="fechaDistribuicion">
                                                    </div>
                                                </div>


 
 


                                               
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del Comite -->






                  
 


            
                
             

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
 
      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   
    


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">

         $(document).ready(function () {

              $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */


            changeProgram(); 
                


                $('#upload').on('click', function () {
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'registro_comite/upload_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });


                
         $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });


            function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");


                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   







             $.datepicker.regional['es'] = {
                  closeText: 'Cerrar',
                  prevText: '<Ant',
                  nextText: 'Sig>',
                  currentText: 'Hoy',
                  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                  monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                  dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                  dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                  weekHeader: 'Sm',
                  dateFormat: 'dd/mm/yy',
                  firstDay: 1,
                  isRTL: false,
                  showMonthAfterYear: false,
                  yearSuffix: ''
                };

                $.datepicker.setDefaults($.datepicker.regional['es']);



            $('#cantidadProducida').numeric();
            $('#cantidadDistribuir').numeric();


           


               $( function() {
            $( "#fechaDistribuicion" ).datepicker({ dateFormat: 'dd/mm/yy' });
           
            
          } );



            

          });    




          function changeProgram(){


            var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }



               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").fadeIn("slow");

    
                    }
                });

 
         
               
        }


         function sumaPeople(){
            
            var total = 0;

                 
                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                   //     alert("value:" + this.value);
                        total += parseFloat(this.value);
                    }
                }); 


                //total = parseFloat($("#total_people_womans").val()) + parseFloat($("#total_people_mans").val());

            $("#total_personas").val(total);




        }


        function guardar(){



             $.ajax({
                    url: "<?php echo site_url('registro_materiales_capacitacion/guardarMaterialCapacitacion') ?>",
                    type: "POST",
                    data: $('#form_material_capacitacion').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


                  $.ajax({
                    url: "<?php echo site_url('registro_materiales_capacitacion/guardarMaterialDistribucion') ?>",
                    type: "POST",
                    data: $('#form_distribuir_mat_capacitacion').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });

 








            swal("Bien hecho!", "El comité ha sido guardado.", "success");
        }


         $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });


            function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");


                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   







    </script>

</body>

</html>
