
                
  <form id="form_listadoCapacitacion" action="registro_capacitacion" method="post">


    <table id="example2" class="display" class="col-lg-12">
        <thead> 
            <tr>
                <th>id</th>
               
                <th>Nombre Material</th>
                <th>Tipo Material</th>
                <th>Cantidad Distribuida</th>
                <th>Comité</th>
                
                <th>Archivo Material</th>                
                <th>Fecha Distribucion</th>
                <th>Acción</th>
                
            </tr>
        </thead>
        <tbody>

            <?php
            $counter = 1;

            foreach ($listCapacitacion as $row){
                

                 echo "<tr id='tr_".$row["id"]."'><td>".$counter."</td><td>". $row['nombre_capacitacion'] ."</td><td>". $row['tema'] ."</td><td>". $row['fig_cap'] ."</td><td>". $row['municipio'] ."</td><td>". $row['fecha_imparticion'] ."</td><td>Archivo Material</td>";
                ?>

                 <td>

              <button type="button" class="btn btn-xs btn-warning waves-effect w-md waves-light m-b-15"   onClick='verCap(<?=$row['id']?>)' data-toggle="modal" title="ver" data-target=". "><i class="fa fa-file"></i></button>
                      
              <button type="button" class="btn btn-xs btn-primary waves-effect w-md waves-light m-b-15"  onClick='editCap(<?=$row['id']?>)' data-toggle="modal" title="editar" data-target=". "><i class="fa fa-pencil"></i></button>  
              
               <button type="button" class="btn btn-xs btn-danger waves-effect w-md waves-light m-b-15"  onClick='delCap(<?=$row['id']?>)' data-toggle="modal" title="borrar" data-target=". "><i class="fa fa-trash"></i></button></td></tr>



              
              
 


          

          <?php
          $counter++;
            
            }


            ?>
       
           
         
        </tbody>
 
    </table>   



    <input type="hidden" name="id_capacitacion" id="id_capacitacion">
    <input type="hidden" name="method" id="method">




    </form>



     


  

    <script type="text/javascript">

        $(document).ready(function () {

       
     
 


    $('#example tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );
 


    var tableProy = $('#example2').DataTable( {
       language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
        dom: 'Bfrtip',
        scrollX: 'true',
        pagingType: 'full_numbers',
        bAutoWidth: false,
    
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        bInfo: false,
        buttons: [
            
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                  exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6]
                    }
            },
            
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                  exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6]
                    }
            }
        ],

    } );


     tableProy.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


           


           
 
            
            

          });    


   




///////////////////////////


         function editCap(id_capacitacion)
        {
     
            $("#id_capacitacion").val(id_capacitacion);
            $("#method").val("edit");
           
            $("#form_listadoCapacitacion").submit();



        }



        function verCap(id_capacitacion)
        {   

     

            $("#id_capacitacion").val(id_capacitacion);
            $("#method").val("view");


           
            $('#form_listadoCapacitacion').attr('action', 'registro_capacitacion');
           
            $("#form_listadoCapacitacion").submit();



        }



    

     function delCap(id_capacitacion){

            swal({
              title: "Estás seguro de querer borrar la capacitación?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: false,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Listo!, Se ha borrado exitosamente", {
                  icon: "success",
                });
                eliminarCapacAjax(id_capacitacion);

              } else {
                swal("Lo dejamos como estaba..");
              }
            });
        }


    function eliminarCapacAjax(id_capacitacion){

         $("#id_capacitacion").val(id_capacitacion);

            $.ajax({
                    url: "<?php echo site_url('registro_capacitacion/eliminarCapacitacion') ?>",
                    type: "POST",
                    data: $('#form_listadoCapacitacion').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                       if(message=="ok"){
                         $("#tr_"+id_capacitacion).hide('slow');
                       }
                        
 
                    }
                });
    }














    </script>