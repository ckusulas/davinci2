<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    
    public function __construct() {
        parent::__construct();
    }

   

     // Read data using username and password
    public function login_success($data) {

        $username = $data["username"];
        $password = $data["password"];

  

        $this->db->where('login', $username);
        $this->db->where('password', $password);
        $query = $this->db->get("cs_usuarios");

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }


     

}
