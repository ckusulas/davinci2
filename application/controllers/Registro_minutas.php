<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class registro_minutas extends CI_Controller {



	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
        $this->load->library('session');

    }
 
	public function index()
	{	

        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }

		 $listMunicipios = $this->ffinanciamiento_model->getMunicipios();

        $data = array(     
          
            'listMunicipios' => $listMunicipios
        );

		$this->load->view('registro_minutas',$data);
	}



    function guardarFuncionariosAsiste(){
         
        $oficioAprobacion = $this->input->post('oficioAprobacion');
        $instancia = $this->input->post('instancia');
        $responsable = $this->input->post('responsable');
        $cargo = $this->input->post('cargo');
        $nombreComite = $this->input->post('nombreComite');

        echo "oficioAprobacion:" . $oficioAprobacion . "<br>";
        echo "instancia:" . $instancia . "<br>";
        echo "responsable:" . $responsable . "<br>";
        echo "cargo:" . $cargo . "<br>";
        echo "nombreComite:" . $nombreComite . "<br>";

        $this->db->set('oficio_aprobacion', $oficioAprobacion);
        $this->db->set('instancia', $instancia);        
        $this->db->set('cargo', $cargo);
        $this->db->set('minuta_firmada', '');
        $this->db->set('nombre_comite', $nombreComite);
       

        //INSERT FF
        $this->db->insert('cs_registro_minuta');
        $lastId = $this->db->insert_id();
 

 
         $data = array(        
             'registro_minuta' => $lastId,                               
          ); 

        $this->session->set_userdata($data);

   

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId;
             
        } else {
            $status = false;
            $msg = $lastId;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos)); 

    }
 




    function guardarDatosReunion(){

 
        $numeroReunion = $this->input->post('numeroReunion');
        $lugarReunion = $this->input->post('lugarReunion');
        $fechaReunion = $this->input->post('fechaReunion');
        $municipio = $this->input->post('municipio');
        $recibioQueja = $this->input->post('recibioQueja');
        $detallesQueja = $this->input->post('detallesQueja');
        $temasTratados = $this->input->post('temasTratados');

        echo "numeroReunion:" . $numeroReunion . "<br>";
        echo "lugarReunion:" . $lugarReunion . "<br>";
        echo "fechaReunion:" . $fechaReunion . "<br>";
        echo "recibioQueja:" . $recibioQueja . "<br>";
        echo "temasTratados:" . $temasTratados . "<br>";

         $lastId_minuta = $this->session->userdata('registro_minuta');



        $this->db->set('numero_reunion', $numeroReunion);     
        $this->db->set('lugar_reunion', $lugarReunion);     
        $this->db->set('fecha_reunion', $fechaReunion);     
        $this->db->set('fk_municipio', $municipio);     
        $this->db->set('detalles_queja', $detallesQueja);     
        $this->db->set('recibio_queja', $recibioQueja);     
        $this->db->set('temas_tratados', $temasTratados);     
        
        $this->db->where('id', $lastId_minuta);       
        $this->db->update('cs_registro_minuta');

         if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_minuta;
             
        } else {
            $status = false;
            $msg = $lastId_minuta;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos)); 

           


    }



    function guardarBeneficiarios(){ 

         
        $nombreBenefic1 = $this->input->post('nombreBenefic');

        $lastId_minuta = $this->session->userdata('registro_minuta');


        $this->db->where('id', $lastId_minuta);
        $this->db->where('tipo', 'beneficiarios');
        $this->db->delete('cs_registro_minuta_asistencia');


         foreach($this->input->post('nombreBenefic') as $nombre){
            echo "nombre:" . $nombre . "<br>";

            if($nombre!=""){
                $this->db->set('nombre_completo', $nombre);     
                $this->db->set('tipo', 'beneficiarios');     
                $this->db->set('fk_minuta', $lastId_minuta);     
                $this->db->insert('cs_registro_minuta_asistencia');   
            }
        

            
         }


        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_minuta;
             
        } else {
            $status = false;
            $msg = $lastId_minuta;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos)); 



 


    }




    function guardarIntegrantes(){ 
        
        $lastId_minuta = $this->session->userdata('registro_minuta');


        $this->db->where('fk_minuta', $lastId_minuta);
        $this->db->where('tipo', 'integrantes');
        $this->db->delete('cs_registro_minuta_asistencia');


         
         $nombreIntegrante = $this->input->post('nombreIntegrante');
  

           /*id
            nombre_completo
            asistio
            tipo
            fk_minuta
            updated*/

        
        if(!empty($nombreIntegrante)){        
 

            for($i=0; $i<count($nombreIntegrante);$i++){
                $n = $i+1;

                
                $nombreBeneficiario = $nombreIntegrante[$i];
                $elem = "asistio".$n;
                $asistio = $this->input->post($elem);

                if($nombreBeneficiario!=""){
                    $this->db->set('nombre_completo', $nombreBeneficiario);     
                    $this->db->set('tipo', 'integrantes');     
                    $this->db->set('asistio', $asistio);     
                    $this->db->set('fk_minuta', $lastId_minuta);     
                    $this->db->insert('cs_registro_minuta_asistencia');   
                }
        
            }
         

        }


        

        die();      


    }


    


 

    



}
