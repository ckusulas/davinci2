
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>
     <?php $this->view('template/header.php'); ?>   

</head>

<body>

    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING --Salir>
 
          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->




        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

               <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                <tr>
                                    <td>
                                       <h1>
                                           
                                        </h1>
                                     </td>
                                     <td align="right">
                                         &nbsp;<input type="hidden" name="submenuproxim"> 
                                     </td>
                                </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Fuente de Financiamiento</i>
                                </li>
                                <li class="active">Listado de FF</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
				
				
				 
				

                    <!-- begin LEFT COLUMN -->
                    <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                             <div id="subtitleProgram"></div>  
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                         <div class="portlet-body">
                                           <div id="tableGeneralFederal"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- /.row -->
                <!-- end MAIN PAGE ROW -->
				
	 

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
   
   <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
 
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   


      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

  
    
    
    
    




    <script type="text/javascript">

        $(document).ready(function () {

         
         $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */
 

            changeProgram();


 

            $("#catalogResources").change(function () {

                var resourceSelected = $("#catalogResources option:selected").val();     

                populate_programs(resourceSelected);

            });

            function populate_programs(fromResource) {
                var cantElement = 0;
                $('#catalogPrograms').empty();
                $('#catalogPrograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_programs') ?>/" + fromResource,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogPrograms').empty();
                        $('#catalogPrograms').append("<option>Selecciona Programa..</option>");


                        $.each(data, function (i, name) {
                            $('#catalogPrograms').append('<option value="' + data[i].id + '">' + data[i].programa + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   


             $("#catalogPrograms").change(function () {

                var programSelected = $("#catalogPrograms option:selected").val();   

                populate_subprograms(programSelected);

            });


           function populate_subprograms(fromProgram) {
                var cantElement = 0;
                $('#catalogSubprograms').empty();
                $('#catalogSubprograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_subprograms') ?>/" + fromProgram,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogSubprograms').empty();
                        $('#catalogSubprograms').append("<option>Selecciona subprograma..</option>");
        

                        $.each(data, function (i, name) {
                            $('#catalogSubprograms').removeAttr("disabled")
                            $('#catalogSubprograms').append('<option value="' + data[i].id + '">' + data[i].subprograma + '</option>');
                            cantElement++;
                        });                         

                        if(cantElement==0){
                            $('#catalogSubprograms').attr('disabled', 'disabled');
                        }
                    }
                });
            }



            $('#vigila_federal').numeric(",");
            $('#vigila_estatal').numeric(",");
            $('#vigila_municipal').numeric(",");
            $('#vigila_otros').numeric(",");
            $('#total_acciones').numeric();
            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();
            
            

          });    


        function sumaVigilado(){

            var total = 0;

                $(".t_vigilado").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

            $("#total_vigilado").val(total);

        }



        function sumaPeople(){

            var total = 0;

                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

              

            $("#total_people").val(total);

        }


        



        function jsUcfirst(string) 
        {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }


        function guardarFF(){

            swal("Bien hecho!", "La Fuente de Financiamiento ha sido guardado.", "success");

        }


         function changeProgram(){



            var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();

    

             if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");

                    return;
                }

              

               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            recurso = obj.recurso;
                             esquema = obj.esquema;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                     $("#subtitleProgram").html("<h4>Listados de F.Financiamiento 2018 -" + titulo_programa +"</h4>").show("slow");
                   //$("#titleProgram").show("slow");

                
                    if(recurso == 1){
                        $("#patcs_menu").hide();
                        $("#petcs_menu").show("slow");
                            $("#documentos_menu").show("slow");

                        $("#seguimiento_menu").hide("slow");
                        $("#reuniones_menu").show("slow");

                           $("#informes_menu").show("slow");
                             $("#formatos_menu").hide("slow");


                    }

                      v_esquema = 0;

                    if(esquema==1){
                             v_esquema = 1;
                    }
                     

                     if(recurso == 2 || v_esquema == 1){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow");
                            $("#documentos_menu").show("slow");
                       
                        $("#reuniones_menu").hide("slow");
                        $("#seguimiento_menu").show("slow");
                       // v_esquema = 0;


                           $("#informes_menu").hide("slow");
                             $("#formatos_menu").show("slow");
                        
                    }
    
                    }
                    });


                  if(id_programSelected != "agregar"){
                         
                     var idSel = $("#programaSeleccionado").val();
 

                        $.ajax({
                        url: "<?php echo site_url('listado_ffinanciamiento2/getListadoFF') ?>/"+idSel,
                        aSync: false,
                        dataType: "html",
                               success: function(data) {
                                            $("#tableGeneralFederal").html(data).show("slow");
                                             // $("#tablaGeneral").show();
                                        },
                                
                        });


                         

                }


 
                     //location.reload(true);

               
        }




        


 



    </script>



</body>

</html>
