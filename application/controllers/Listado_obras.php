<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listado_obras extends CI_Controller {

   /**
     * Developed by Constantino Kusulas Vazquez   - 2018.
     */


   	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');


       /* $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }*/
    }




	public function index()
	{  

      if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }

        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);

        


        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor
          
        );


        $this->load->view('listado_obras', $data);
	}




	

	public function getTablaGeneralProyectos($program = ""){

     // sleep(0.7);
          
        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        

       
        if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');
          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }

 $departamento = $this->session->userdata('departamento');


        $listProyectos = $this->ffinanciamiento_model->getListProyectos_tabla($fk_programa, $departamento);
        $totalRecursoAsignado = $this->ffinanciamiento_model->getTotalRecursoAsignadoVigilar($fk_programa);
        $totalBeneficiados = $this->ffinanciamiento_model->getTotalBeneficiados($fk_programa);
        $qtyObras = $this->ffinanciamiento_model->getQtyBeneficio(1,$fk_programa);
        $qtyApoyo = $this->ffinanciamiento_model->getQtyBeneficio(2,$fk_programa);
        $qtyServicio = $this->ffinanciamiento_model->getQtyBeneficio(3,$fk_programa);
        $qtyComites = $this->ffinanciamiento_model->getQtyComites($fk_programa);

        $data = array();

        unset($data);
 
        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor,
            'listProyectos' => $listProyectos,
            'totalRecursoAsignado' => $totalRecursoAsignado,        
            'totalBeneficiados' => $totalBeneficiados,           
            'qtyObras' => $qtyObras,           
            'qtyApoyos' => $qtyApoyo,           
            'qtyServicios' => $qtyServicio,    
            'qtyComites' => $qtyComites,    
            'success' => true,       
            'status' => true,       
          
          
        );
 
       	 
          $this->load->view('template/tableGeneral_proyectos2', $data);
       
    }



     



 



}
