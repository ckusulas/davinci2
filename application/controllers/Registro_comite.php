<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_comite extends CI_Controller {


    /**
     * Developed by Constantino Kusulas Vazquez   - 2018.
     */




    public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
        $this->load->model('catalogos_model');
        $this->load->library('session');
 
    }


	 
	public function index()
	{
        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }


         $metodo = $this->input->post('method');
         $id_comite = $this->input->post('id_comite');

         $programaSelected = $this->input->post('seleccPgma');
         $id_proyecto = $this->input->post('id_proyecto');
       
       /* echo "metodo:" . $metodo . "<br>";
          echo "id_comite:" . $id_comite . "<br>"; */
 
         $onlyView = "";
        if($metodo == "view"){
            $onlyView = 1;
        }


         
           
        $id_departamento = ""; //$this->input->post('departamento');
        $id_departamento = $this->session->userdata('departamento'); //$this->input->post('departamento');

         
        if($programaSelected == "") {
            $programaSelected = $this->session->userdata('ProgramasSelecc');
        }

        //echo "programa_seleccionado:" . $programaSelected;


        //VERIFICANDO EXISTENCIA OBRA
        if($programaSelected!="todos"){ 
            $this->db->where('fk_programa', $programaSelected);
        }
        $this->db->where('id', $id_comite);       
        $q = $this->db->get('cs_registro_comite');


  

             $editData = array (
                    'id' => '',
                    'id_comite' => '',
                    'nombre_comite' => '',
                    'metodo' => '',
                    
                    'fecha_constitucion' => '',
                    'clave_registro' => '',
                     'accion_conexion' => '',
                    'funciones_comite' => '',
                    'proporciono_resumen' => '',

                    'qty_hombres' => '',
                    'qty_mujeres' => '',
                    
                    'asamblea_beneficiarios' => '',
                    'equidad_genero' => '',
                    'ficha_informativa' => '',
                    'reglas_operacion' => '',
                    'formatos_cs' => '',
                    'disposicion_quejas' => '',
                    
                    'servidor_publico_constancia' => '',
                    'cargo_servidor_publico' => '',
                    'acta_asamblea' => '',
                    'constancia_firmada' => '',
                    'escrito_libre' => '',
                    'comentarios' => '', 
                    'onlyView' => '',
                    'seleccPgma' => $programaSelected,
                    'url_actasamblea' => '',
                    'url_constancia' => '',
                    'url_escritolibre' => '',

            );

               $listIntegrantes = $this->ffinanciamiento_model->getIntegrantesComite(0);




        if ($q->num_rows() > 0) {

           // $metodo = "edit";

            
              $qtyIntegrantes = $this->ffinanciamiento_model->getQtyIntegrantesComite($id_comite);
              $listIntegrantes = $this->ffinanciamiento_model->getIntegrantesComite($id_comite);


          
          

              /*  $this->db->select('id');
                $this->db->where('fk_comite',$id_comite);
                $query = $this->db->get('cs_registro_comite_integrantes');
                $numIntegrantes = $query->num_rows();*/

            foreach ($q->result() as $row) {

         /*       $id_proyecto = intval($row->accion_conexion);

                $departamento = "";*/

             /*   $this->db->where('id', $id_proyecto);       
                $q = $this->db->get('cs_registro_proyecto');*/

                $programa =  $row->fk_programa;
                $departamento = $row->fk_departamento;
                $id_proyecto = $row->accion_conexion;

                $fk_localidad = $row->fk_localidad;

                $listLocalidad = "";

                if($fk_localidad!=""){


                    $this->db->select("fk_municipio,fk_localidad");
                    $this->db->where('id', $id_proyecto);       
                    $x = $this->db->get('cs_registro_proyecto');
                    
                    foreach ($x->result() as $row2) {
                        $resource_municipio = $row2->fk_municipio;            
                    }    

                 
                    $listLocalidad = $this->ffinanciamiento_model->findLocalidades($resource_municipio);


                 }

               $dataProyecto = $this->ffinanciamiento_model->getListProyectos($programaSelected, $departamento, $id_proyecto);

                $editData = array (                   
                    'id' => $row->id,
                    'id_comite' => $id_comite,
                    'nombre_comite' => $row->nombre_comite,
                    'metodo' => $metodo,
                    
                    'fecha_constitucion' => $this->transformDateToView($row->fecha_constitucion),
                    'clave_registro' => $row->clave_registro,
                    'accion_conexion' => $row->accion_conexion,
                    'funciones_comite' => $row->funciones_comite,
                    'proporciono_resumen' => $row->proporciono_resumen,

                    'qty_hombres' => $row->qty_hombres,
                    'qty_mujeres' => $row->qty_mujeres,
                    
                    
                    'disposicion_quejas' => $row->disposicion_quejas,
                    'servidor_publico_constancia' => $row->servidor_publico_constancia,
                    'cargo_servidor_publico' => $row->cargo_servidor_publico,
                    'acta_asamblea' => $row->acta_asamblea,
                    'constancia_firmada' => $row->constancia_firmada,
                    'escrito_libre' => $row->escrito_libre,
                    'comentarios' => $row->comentarios,
                    'qtyIntegrantes' => $qtyIntegrantes,
                    //'listIntegrantes' => $listIntegrantes,
                    'onlyView' => $onlyView,
                    'dataProy' => $dataProyecto,
                    'seleccPgma' => $programaSelected,
                    'fk_localidad' => $fk_localidad,
                    'listLocalidades' => $listLocalidad,
                   // 'numIntegrantes' => $numIntegrantes,
                    'url_actasamblea' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $departamento, "comite", "actaasamblea"),
                    'url_constancia' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $departamento, "comite", "constancia"),
                    'url_escritolibre' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $departamento, "comite", "escritolibre"),

                );
            }
        }

        //print_r($editData);

        
        $listComiteFunciones = $this->catalogos_model->getListComiteFunciones(2018);
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);


        $listObras = $this->ffinanciamiento_model->getListProysBeneficios($programaSelected,1);
        $listApoyos = $this->ffinanciamiento_model->getListProysBeneficios($programaSelected,2);
        $listServicios = $this->ffinanciamiento_model->getListProysBeneficios($programaSelected,3);

        $tituloPrograma = "";

        if($programaSelected!="todos"){   
            $tituloPrograma =  $this->ffinanciamiento_model->getNamePrograma($programaSelected);
        }

       // echo print_r($listIntegrantes);

 
        $data = array(     
            'listRecourses' => $listRecourses,
            'listComiteFunciones' => $listComiteFunciones,             
            'editData' => $editData,    
            'listObras' => $listObras,
            'listApoyos' => $listApoyos,
            'listServicios' => $listServicios,
            'tituloPrograma' => $tituloPrograma,
            'listIntegrantes' => $listIntegrantes,
            'fk_programa' => $programaSelected,
            'departamento' => $this->session->userdata('name'),
            'fk_departamento' => $id_departamento,

        );

     //   if($metodo=="edit" || $metodo=="view"){  

       //       $this->load->view('registro_comite_edit', $data);

        //}else{

		      $this->load->view('registro_comite', $data);

        //}
	}



   public function ajax_list_comite($fk_programa){

        $fk_departamento = $this->session->userdata('departamento');

      

        $this->db->select("id, nombre_comite");
        $this->db->where('fk_programa', $fk_programa);       
        $this->db->where('fk_departamento', $fk_departamento);       
        $this->db->where('eliminado', NULL);       
        $q = $this->db->get('cs_registro_comite');


        $listComites = $this->ffinanciamiento_model->getListComites($fk_programa);
       
        //$listComites = $q->result();

        echo json_encode($listComites);




   }


   public function populate_localidad_proyecto($findProject) {

        $this->db->select("fk_municipio,fk_localidad");
        $this->db->where('id', $findProject);       
        $q = $this->db->get('cs_registro_proyecto');
        
        foreach ($q->result() as $row) {
            $findResource = $row->fk_municipio;
            $selectLocalidad = $row->fk_localidad;
        }    

        $listLocalidad = $this->ffinanciamiento_model->findLocalidades($findResource);


       /* $datos = array(
            'listLocalidad' => $listLocalidad,
            'selectLocalidad' => $selectLocalidad,
        );*/


       // echo json_encode(array($datos));
 
        echo json_encode($listLocalidad);
    }



      public function changeFormatDateSpEng($data){

        $arrayDate = explode("/", $data);
        $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
        return($newData);
    }



	 public function guardarDatosDelComite(){


        //print_r($_POST); die();
     
        $nombreComite = $this->input->post('nombreComite');
        $fechaConstitucion = $this->input->post('fechaConstitucion');
        $claveRegistro = $this->input->post('claveRegistro');
        $accionConexion = $this->input->post('accionConexion');
        $nombreIntegrante = $this->input->post('nombreIntegrante_tmp');
          $localidad = $this->input->post('localidad2');

        $servidorPublicoNombre = $this->input->post('servidorPublicoNombre'); 
        $servidorPublicoCargo = $this->input->post('servidorPublicoCargo');  
        $comentarios = $this->input->post('comentarioComite');  

   
        $accionConexion = $accionConexion[0];


        $id_comite =  $this->input->post('id_comite');  
        $metodo =  $this->input->post('metodo');  
         
 
 
        $fk_programa = $this->session->userdata('ProgramasSelecc');
        $fk_departamento = $this->session->userdata('departamento');


         if($fechaConstitucion!=""){
            $fechaConstitucion = $this->changeFormatDateSpEng($fechaConstitucion);
        }else{
            $fechaConstitucion = NULL;
        }



        $this->db->set('nombre_comite', $nombreComite);
        $this->db->set('fk_localidad', $localidad);
        $this->db->set('fecha_constitucion', $fechaConstitucion);        
        $this->db->set('clave_registro', $claveRegistro);
        $this->db->set('accion_conexion', $accionConexion);
        $this->db->set('fk_programa', $fk_programa);
        $this->db->set('fk_departamento', $fk_departamento);


     /*   if($nombreIntegrante == "" && $metodo!="edit"){
            $this->db->set('eliminado', 1);
        }else{
            $this->db->set('eliminado', NULL);
        }*/
            


      //INSERT FF
        if($id_comite=="" && $metodo!="edit"){
            //echo "insertando";
            $this->db->insert('cs_registro_comite');
            $lastId = $this->db->insert_id();
        }
        else{
            //echo "actualizando";

             $this->db->where('id', $id_comite);      
             $this->db->update('cs_registro_comite');
            $lastId = $id_comite;
        }


        $this->guardarDocumentosComite($lastId);
        $this->guardarFunciones($lastId);
        $this->guardarPreguntasClave($lastId);
        $this->guardarIntegrante1($lastId);
        

        if($accionConexion!=NULL){

            //Relacion - Vinculacion con Obras
            $this->db->where('fk_comite', $lastId);     
            $this->db->delete('cs_registro_comite_obra');

            $listObras = explode(",",$accionConexion);

            foreach($listObras as $row){
                    $this->db->set('fk_comite', $lastId);
                    $this->db->set('fk_obra', $row);


                  $this->db->insert('cs_registro_comite_obra');
            }
        }


  


       // if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId;
             
        /*} else {
            $status = false;
            $msg = $lastId;
        }*/



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
        
 
    }


    function guardarFunciones($lastId_comite){


    	//$lastId_comite =  $this->input->post('id_comite_funciones');

        $funcionesComite = $this->input->post('funcionesComite');
        
        if(count($funcionesComite)>0){
            $funcionesComite = implode(",", $funcionesComite);
        }else{
            $funcionesComite = NULL;
        }




        $this->db->set('funciones_comite', $funcionesComite);
    

        //update comite
        $this->db->where('id',$lastId_comite);
        $this->db->update('cs_registro_comite');
       
 
        
        /*

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_comite;
             
        } else {
            $status = false;
            $msg = $lastId_comite;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));*/


        if ($this->db->affected_rows() > 0) {
         return true;
             
        } else {
           return false;
        }

    }





    function guardarPreguntasClave($lastId_comite){

    	//$lastId_comite = $this->input->post('id_comite_preguntasclave'); 

    	$resumen_ejecutivo = $this->input->post('proporcionoResumenEjecutivo'); 
    	$disposicion_quejas = $this->input->post('disposicionQuejas');  


        $this->db->set('proporciono_resumen', $resumen_ejecutivo);
        $this->db->set('disposicion_quejas', $disposicion_quejas);
      
       

       
        //INSERT comite
        $this->db->where('id',$lastId_comite);
        $this->db->update('cs_registro_comite');
       
 

 
      

          /*


        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_comite;
             
        } else {
            $status = false;
            $msg = $lastId_comite;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));*/


        if ($this->db->affected_rows() > 0) {
           return true;
        } else {
            return false;
        }
    }




      function guardarIntegrante1($lastId_comite){


    	//$lastId_comite =  $this->input->post('id_comite_integrantes');         
        $this->db->where('fk_comite', $lastId_comite);     
        $this->db->delete('cs_registro_comite_integrantes');


        $nombreIntegrante = $this->input->post('nombreIntegrante'); 
        $apPIntegrante = $this->input->post('apPIntegrante'); 
        $apMIntegrante = $this->input->post('apMIntegrante'); 
        $edadIntegrante = $this->input->post('edadIntegrante'); 
        $cargoIntegrante = $this->input->post('cargoIntegrante'); 

      /*  if(trim($cargoIntegrante) == "Seleccione.."){
            $cargoIntegrante = "";
        }*/

       // echo print_r($_POST);

        $calleIntegrante = $this->input->post('calleIntegrante'); 
        $numeroIntegrante = $this->input->post('numeroIntegrante'); 
        $coloniaIntegrante = $this->input->post('coloniaIntegrante'); 
        $codigoPostalIntegrante = $this->input->post('codigoPostalIntegrante'); 
        $telefonoIntegrante = $this->input->post('telefonoIntegrante'); 
        
    	

        // echo "calleIntegrante:" . print_r($calleIntegrante); 

        $counter = 0;
        $qtyMasculino = 0;
        $qtyFemenino = 0;

        $qtyIntegrantes = $this->input->post("countResourceIntegrantes");
        //echo "cantidad:" . $qtyIntegrantes;

        foreach($nombreIntegrante as $row){
            $nombreIntegrante = $row;
         
        
            $st = $counter+1;
            $st = (string)$st;
            
            $p_sexo = "sexoIntegrante".$st;
            $sexoIntegrante = $this->input->post($p_sexo); 

           /* echo "integrante" . $st . "<br>";
            echo "sexo:$sexoIntegrante";*/

            $p_constancia = "firmaRegistroConstancia".$st;
            $constanciaRegistro = $this->input->post($p_constancia);

            
            $p_domicilioCortejado = "domicilioCortejadoIntegrante". $st;
            $domicilioCortejado = $this->input->post($p_domicilioCortejado);


          /*  echo "sexo:" . $sexoIntegrante . "<br>";
            echo "constanciaRegistro:" . $constanciaRegistro . "<br>";
            echo "domicilioCortejado:" . $domicilioCortejado . "<br>";*/

            if($sexoIntegrante == "Femenino"){
                $qtyFemenino++;
            }


             if($sexoIntegrante == "Masculino"){
                $qtyMasculino++;
            }

 

            $this->db->set('nombre_integrante', $nombreIntegrante);
            $this->db->set('appaterno_integrante', $apPIntegrante[$counter]);
            $this->db->set('apmaterno_integrante', $apMIntegrante[$counter]);
            $this->db->set('sexo_integrante', $sexoIntegrante);
            $this->db->set('edad_integrante', $edadIntegrante[$counter]);

          //  $cargo = "";
            $cargo = $cargoIntegrante[$counter];
            if($cargo=="Seleccione.."){
                $cargo ="";
            }
            $this->db->set('cargo_integrante', $cargo);          
            $this->db->set('firma_constancia_registro_integrante', $constanciaRegistro);
            $this->db->set('domicilio_conocido_integrante', $domicilioCortejado);
            $this->db->set('calle_integrante', $calleIntegrante[$counter]);
            $this->db->set('numero_integrante', $numeroIntegrante[$counter]);
            $this->db->set('colonia_integrante', $coloniaIntegrante[$counter]);
            $this->db->set('cpostal_integrante', $codigoPostalIntegrante[$counter]);
            $this->db->set('telefono_integrante', $telefonoIntegrante[$counter]);
            

            $this->db->set('fk_comite', $lastId_comite);

            //echo "lastId:" . $lastId_comite;
      


            
            $this->db->insert('cs_registro_comite_integrantes');



            $counter++;
        }

        

        //Cantidad Hombres y Mujeres
        $this->db->set('qty_hombres', $qtyMasculino);
        $this->db->set('qty_mujeres', $qtyFemenino);
 
        $this->db->where('id',$lastId_comite);
        $this->db->update('cs_registro_comite');
       


    


       

       
        //INSERT comite
        //$this->db->where('id',$lastId_comite);
        //$this->db->update('cs_registro_comite');
       
 
          
/*

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_comite;
             
        } else {
            $status = false;
            $msg = $lastId_comite;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));*/



        if ($this->db->affected_rows() > 0) {
            return true;
             
        } else {
            return false;
        }


    }



     public function eliminarComite(){

        $id_comite = $this->input->post('id_comite');

          $this->db->set('eliminado', 1); 
          $this->db->where('id', $id_comite);      
          $this->db->update('cs_registro_comite');

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "ok";
        }

        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }





     function guardarDocumentosComite($lastId_comite){


    	//$lastId_comite = $this->input->post('id_comite_documentos');

    	$servidorPublicoNombre = $this->input->post('servidorPublicoNombre'); 
    	$servidorPublicoCargo = $this->input->post('servidorPublicoCargo');  
    	$comentarios = $this->input->post('comentarioComite');  



        $this->db->set('servidor_publico_constancia', $servidorPublicoNombre);
        $this->db->set('cargo_servidor_publico', $servidorPublicoCargo);
  
        $this->db->set('comentarios', $comentarios);
       

       
        //INSERT comite
        $this->db->where('id',$lastId_comite);
        $this->db->update('cs_registro_comite');
       
 
          


       /* if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_comite;
             
        } else {
            $status = false;
            $msg = $lastId_comite;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));*/

        if ($this->db->affected_rows() > 0) {
            return true;
             
        } else {
            return false;
        }
    }




	function upload_file() {

	        //upload file
	        $config['upload_path'] = 'uploads/';
	        $config['allowed_types'] = '*';
	        $config['max_filename'] = '255';
	        $config['encrypt_name'] = TRUE;
	        $config['max_size'] = '1024'; //1 MB

	        if (isset($_FILES['file']['name'])) {
	            if (0 < $_FILES['file']['error']) {
	                echo 'Error al cargar el archivo' . $_FILES['file']['error'];
	            } else {
	                if (file_exists('uploads/' . $_FILES['file']['name'])) {
	                    echo 'El archivo ya existia.. : uploads/' . $_FILES['file']['name'];
	                } else {
	                    $this->load->library('upload', $config);
	                    if (!$this->upload->do_upload('file')) {
	                        echo $this->upload->display_errors();
	                    } else {
	                        echo 'Archivo satisfactoriamente cargado : uploads/' . $_FILES['file']['name'];
	                    }
	                }
	            }
	        } else {
	            echo 'Porfavor eliga un archivo';
	        }
	    }




      function upload_file2() {

      	 
    	$fk_programa = $this->session->userdata('ProgramasSelecc');
    	$lastId_comite = $this->session->userdata('registro_comite');




         die();
       

        $saveTo = 'upload/atachment/' . $fk_programa . "/comite/" . $lastId_comite . "/";

        if (isset($_FILES['files']) && !empty($_FILES['files'])) {
            $no_files = count($_FILES["files"]['name']);
            for ($i = 0; $i < $no_files; $i++) {
                if ($_FILES["files"]["error"][$i] > 0) {
                    echo "Error: " . $_FILES["files"]["error"][$i] . "<br>";
                } else {

                    if (!file_exists($saveTo)) {
                        mkdir($saveTo, 0777, true);
                    }


                    if (file_exists($saveTo . $_FILES["files"]["name"][$i])) {

                        echo 'El archivo ya existia: <b>' . $_FILES["files"]["name"][$i] . "</b>";
                    } else {
                        move_uploaded_file($_FILES["files"]["tmp_name"][$i], $saveTo . $_FILES["files"]["name"][$i]);
                        echo 'Archivo exitosamente guardado...';


                        //get Extension
                        $name = $_FILES["files"]["name"][$i];
                        $temp = explode(".", $name);
                        $ext = "";
                        if (count($temp) > 1) {
                            $ext = $temp[1];
                        }

                        $typeExt = "";
                        switch (strtolower($ext)) {
                            case "pdf":
                                $typeExt = "<i class='fa fa-file-pdf-o' aria-hidden='true'></i>";
                                break;

                            case "doc":
                            case "docx":
                                $typeExt = "<i class='fa fa-file-word-o' aria-hidden='true'></i>";
                                break;

                            case "xls":
                            case "csv":
                            case "xlsx":
                                $typeExt = "<i class='fa fa-table' aria-hidden='true'></i>";
                                break;

                            case "zip":
                            case "rar":
                                $typeExt = "<i class='fa fa-file-archive-o' aria-hidden='true'></i>";
                                break;

                            case "avi":
                            case "mp4":
                                $typeExt = "<i class='fa fa-file-video-o' aria-hidden='true'></i>";
                                break;

                            case "jpg":
                            case "bmp":
                            case "png":
                                $typeExt = "<i class='fa fa-file-image-o' aria-hidden='true'></i>";
                                break;



                            default:
                                $typeExt = "<i class='fa fa-file-o' aria-hidden='true'></i>";
                                break;
                        }

                  

                        $this->db->set('fk_program', $fk_programa);
                        $this->db->set('tipo', "comite");
                        $this->db->set('fk_tipo', $lastId_comite);
                        $this->db->set('url', $saveTo);
                        $this->db->set('fk_user', strtolower($ext));
                        $this->db->set('extension', strtolower($ext));
                        $this->db->set('typeExtHTML', $typeExt);
                        $this->db->set('name', $_FILES['files']['name'][$i]);
                        $this->db->update('cs_adjuntos');
                    }
                    $qtyAtachment = 0;
                    $this->db->where('fk_trainingRecords', $idCourseRecord);
                    $this->db->where('tipo', "comite");
                    $this->db->where('status', 1);
                    $this->db->order_by('id', 'DESC');
                    $query = $this->db->get('cs_adjuntos');

                    $qtyAtachment = $query->num_rows();
                    echo '<input type="hidden" id="idcomiteTMP" name="idcomiteTMP" value="' . $lastId_comite . '">';
                    echo "<br>";

                    foreach ($query->result_array() as $row) {

                        echo "<div class='atachment' id='atachment_" . $row['id'] . "'>";
                        echo $row['typeExtHTML'] . "&nbsp;<a href='" . base_url() . $row['url'] . $row['name'] . "'>" . $row['name'] . "</a>&nbsp;<a href='#' onClick='deleteAtachment(" . $row['id'] . "," . $lastId_comite . ")' border='0'><img width='14px' height='14px' src='../assets/images/delete-icon.png'></a></div>";
                    }
  
                    
                }
            }
        } else {
            echo 'Porfavor, seleccione al menos un archivo.';
        }
    }




    public function transformDateToView($data){

        if($data == "0000-00-00"){
            return "";
        }



        $arrayDate = explode("-", $data);
        if(strlen($data)>6){
            $newData = $arrayDate[2] . "/" . $arrayDate[1] . "/"  . $arrayDate[0];
            return($newData);
        }
    }






    
}
