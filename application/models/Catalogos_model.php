<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogos_model extends CI_Model {

    
    public function __construct() {
        parent::__construct();
    }

   

    public function getListComiteFunciones($find_year = "2018") {

      $sql = "SELECT * FROM  cs_catalogo_general where categoria='comite_funciones' and año='$find_year' and status=1 order by orden";
      $query = $this->db->query($sql);

         
      $listQuestions =  $query->result_array(); 

      return($listQuestions);
    }


    


    public function getListCapacitacionTematica($find_year = "2018") {

      $sql = "SELECT * FROM  cs_catalogo_general where categoria='capacitacion_tematica' and año='$find_year' and status=1 order by orden";
      $query = $this->db->query($sql);

         
      $listQuestions =  $query->result_array(); 
      
      return($listQuestions);
    }


    public function getListCapacitacionFigCapacitada($find_year = "2018") {

      $sql = "SELECT * FROM  cs_catalogo_general where categoria='capacitacion_figura' and año='$find_year' and status=1 order by orden";
      $query = $this->db->query($sql);

         
      $listQuestions =  $query->result_array(); 
      
      return($listQuestions);
    }

 

    

     

}
