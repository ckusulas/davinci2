<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listado_materiales extends CI_Controller {

	/**
	 * Developed by Constantino Kusulas Vazquez   - 2018.
	 */


   	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');


       /* $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }*/
    }




	public function index()
	{
    if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      } 
   $programaSelected = $this->session->userdata('ProgramasSelecc');
     

           $listMunicipios = $this->ffinanciamiento_model->getMunicipios(); 
              $listComites = $this->ffinanciamiento_model->getListComites($programaSelected);

        
           $data = array(
            
         
            'listMunicipios' => $listMunicipios,    
            'listComites' => $listComites,
                
          
          
        );
   
       

        $this->load->view('listado_materiales', $data);
	}




	

	  public function getTablaGeneralMaterial($program = ""){

     // sleep(0.7);
          
        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        

       
        if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');

          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }


        $listMaterial = $this->ffinanciamiento_model->getListMaterial($fk_programa);
   
        $data = array();

        unset($data);
 
        $data = array(
            
         
            'listMaterial' => $listMaterial,       
            'success' => true,       
            'status' => true,       
          
          
        );
 
         
        $this->load->view('template/tableGeneral_material', $data);
       
    }


    public function guardarNuevaDistribucion()
    {

        $cantidad_distribuir = $this->input->post('cantidad_distribuir');
        $fecha_distribucion = $this->input->post('fecha_distribucion');
        $accionConexion = $this->input->post('accionConexion');
        $fk_material = $this->input->post('fk_material');

        echo "cantidad_distribuir:" . $cantidad_distribuir;
        echo "fecha_distribucion:" . $fecha_distribucion;
        echo "fk_material:" . $fk_material;

        $subQ = "CAST(cantidad_distribuida as INT) + ".$cantidad_distribuir;

        $this->db->set('cantidad_distribuida', $subQ, false);
        $this->db->where('id', $fk_material);

        
        //$fk_user = $this->session->userdata('id');
        //$this->db->set('who_updated', $fk_user);

       

        $this->db->update('cs_registro_material');




        $this->db->set('fk_material', $fk_material);
        $this->db->set('fk_comite', $accionConexion);
        $this->db->set('cantidad_distribuir', $cantidad_distribuir);

          $fecha_distribucion = $this->changeFormatDateSpEng($fecha_distribucion);


        $this->db->set('fecha_distribucion', $fecha_distribucion);



        $this->db->insert('cs_registro_material_distribucion');

      
      

        $status = true;
        $lastId = "";


        $datos = array(
            'status' => $status,
            'msg' => $lastId,
        );


        echo json_encode(array($datos));


    }


     





    public function changeFormatDateSpEng($data){

        $arrayDate = explode("/", $data);
        $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
        return($newData);
    }


}
