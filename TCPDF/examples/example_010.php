<?php
header('Content-Type: text/html; charset=utf-8');
 
//============================================================+
// File name   : example_010.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 010 for TCPDF class
//               Text on multiple columns
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

 


/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Text on multiple columns
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');


/**
 * Extend TCPDF to work with multiple columns
 */
 


// detalles de la conexion
$conn_string = "host=localhost port=5432 dbname=postgres user=ubuntu password=ubuntu123 options='--client_encoding=UTF8'";
 
// establecemos una conexion con el servidor postgresSQL
$dbconn = pg_connect($conn_string);

// ---------------------------------------------------------
// EXAMPLE
// ---------------------------------------------------------
// create new PDF document

         // $id_capacitacion =  8; //$this->input->post('id_capacitacion');


            $id_capacitacion = $_POST["id_capacitacion"];
       //   echo "id_capacitacion:" . $id_capacitacion . "<br>";

       

          $sql = "select * from cs_registro_capacitacion where id={$id_capacitacion}" ; 

           $result = pg_query($sql) or die('Error message: ' . pg_last_error());

 			$getDataCapacitacion = pg_fetch_assoc($result);

 		//	echo print_r($getDataCapacitacion);

 
 	// echo $sql;

 	// die();



 


              $nombre_capacitacion = $getDataCapacitacion['nombre_capacitacion'];
              $tematica = $getDataCapacitacion['fk_tematica'];
              $figura_capacitada = $getDataCapacitacion['fk_figura_capacitada'];
              $fk_comite =  $getDataCapacitacion['fk_accion_conexion'];

              $pgta_asamblea_beneficiarios = $getDataCapacitacion['pgta_asamblea_beneficiarios'];
              $pgta_equidad_genero = $getDataCapacitacion['pgta_equidad_genero'];
              $pgta_reglas_operacion = $getDataCapacitacion['pgta_reglas_operacion'];
              $pgta_reglas_operacion = $getDataCapacitacion['pgta_reglas_operacion'];
              $pgta_formatos_cs = $getDataCapacitacion['pgta_formatos_cs'];
              $pgta_disposicion_quejas = $getDataCapacitacion['pgta_disposicion_quejas'];
              $pgta_observaciones_comite = $getDataCapacitacion['pgta_observaciones_comite'];

 
              

                    //'entidad_federativa' => $row->entidad_federativa,
              $fecha_imparticion = transformDateToView($getDataCapacitacion['fecha_imparticion']);



              $id_comite = $fk_comite;

 $sql = " select comi.id, nombre_comite, program.ejercicio_fiscal, ejec.instancia_ejecutora, ejec.tipo_ejecutora, cs_prog.programa, comi.fecha_constitucion, proy.nombre_proyecto, comi.clave_registro, comi.qty_hombres, comi.qty_mujeres,  proy.id as id_obra, proy.estatus_proyecto,  proy.recurso_ejecutado_vigilado ,  proy.recurso_asignado_vigilar, ejec.instancia_ejecutora, muni.municipio, loca.localidad, (select localidad from cs_localidades as loca2 where comi.fk_localidad=loca2.id) as localidad_comite, proy.fk_program from cs_registro_comite as comi LEFT JOIN cs_registro_comite_obra as rel on (comi.id = rel.fk_comite)  JOIN cs_registro_proyecto as proy ON(rel.fk_obra = proy.id and proy.eliminado is null)  LEFT JOIN cs_registro_programa_recursos as program on (proy.fk_program = program.fk_programa_proyectos and program.eliminado=NULL) left join cs_programas cs_prog on proy.fk_program = cs_prog.id LEFT JOIN cs_instancia_ejecutora  as ejec ON(proy.fk_instancia_ejecutora = ejec.id) LEFT JOIN cs_municipios as muni ON(proy.fk_municipio =  muni.id) LEFT JOIN cs_localidades as loca ON(proy.fk_localidad = loca.id) where comi.eliminado is null and comi.id={$id_comite} ";

 echo $sql;

 $result2 = pg_query($sql) or die('Error message: ' . pg_last_error());

 $getDataComite = pg_fetch_array($result2);


 var_dump($getDataComite);

               //  $getDataComite = $this->ffinanciamiento_model->getInfoComite($fk_comite);



       
        $sql2 = "select * from cs_registro_comite_integrantes where fk_comite={$id_comite}";

  $result2 = pg_query($sql2) or die('Error message: ' . pg_last_error());

  //$listIntegrantes = pg_fetch_array($result2);


  /*var_dump($listIntegrantes);

  echo "primer:" . $listIntegrantes[0]['nombre_integrante'];*/


$counter = 0;

$listIntegrantes = array();

 while ($row = pg_fetch_array($result2)) {

  /*var_dump($row);
  echo $row['id'];
  echo $row['nombre_integrante'] . "<br>";*/

  $listIntegrantes[$counter]['id'] = $row['id'];
  $listIntegrantes[$counter]['nombre_integrante'] = $row['nombre_integrante'];
  $listIntegrantes[$counter]['appaterno_integrante'] = $row['appaterno_integrante'];
  $listIntegrantes[$counter]['apmaterno_integrante'] = $row['apmaterno_integrante'];
  $listIntegrantes[$counter]['cargo_integrante'] = $row['cargo_integrante'];
  $listIntegrantes[$counter]['telefono_integrante'] = $row['telefono_integrante'];
  $listIntegrantes[$counter]['calle_integrante'] = $row['calle_integrante'];
  $listIntegrantes[$counter]['cargo_integrante'] = $row['cargo_integrante'];
  $listIntegrantes[$counter]['numero_integrante'] = $row['numero_integrante'];
  $listIntegrantes[$counter]['colonia_integrante'] = $row['colonia_integrante'];


 

  $counter++;
 
}
 

 

        //  die();


        //  $this->load->library('Pdf');
          
          //$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
          $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

          

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Contraloria Social 2018');
$pdf->SetTitle('SICSEQ QRO');
 
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 17);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

 

// add a page
$pdf->AddPage('L');


// -- set new background ---

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
   $img_file = "/var/www/davinci2/assets/formats/capacitacion/capacitacion1.jpg";
  // $pdf->setJPEGQuality(75);

   // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)

$pdf->Image($img_file, 12, 0, 275, 210, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();


// Print a text
//$html = '<span style="color:red;text-align:center;font-weight:bold;font-size:80pt;">Draft</span>';
//$pdf->writeHTML($html, true, false, true, false, '');


/*
tcpdf.php:
public function Write($h, $txt, $link='', $fill=false, $align='', $ln=false, $stretch=0, $firstline=false, $firstblock=false, $maxh=0, $wadj=0, $margin='') {*/



/*$pdf->SetXY(58,80);

$txt = "3x1 PARA MIGRANTES";

$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);*/



//Programa
$pdf->SetXY(74,47);
 

$txtPrograma = $getDataComite['programa'];

// print a block of text using Write()
$pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



//Obra
$pdf->SetXY(190,45);
 

$pdf->SetFont('helvetica', '', 7);
//$txtObra = "EMPEDRADO AHOGADO EN MORTERO, EN CALLE NINO HEROES";
$nombreObra = "1.- AMPLIACIÓN DE SISTEMA DE ALCANTARILLADO SANITARIO (5TA ETAPA), PARA BENEFICIAR A 13 LOCALIDADES DE LA DELEGACIÓN MUNICIPAL DE SANTIAGO MEXQUITITLÁN, EN EL MUNICIPIO DE AMEALCO DE BONFIL";
//strtoupper($getDataComite['nombre_proyecto']);

//

// print a block of text using Write()
//$pdf->Write(0, $txtObra, '', 0, 'L', true, 0, false, false, 0);

/*$pdf->SetXY(190, 45);
$nombreObra = strtoupper($getDataComite['nombre_proyecto']); */
$separador = "";
if(strlen($nombreObra)>54){
  $separador = "_";
}

$nombreObraTmp = substr($nombreObra,0,66).$separador;

$nombreObraTmp2 = "";

$separador="";
$nombreObraTmp2 = substr($nombreObra,66,66);
if(strlen($nombreObraTmp2)>66){
  $separador="_";
  $nombreObraTmp2 = $nombreObraTmp2.$separador;
}
 

$nombreObraTmp4 = "";

$separador="";
$nombreObraTmp3 = substr($nombreObra,132,66);

if(strlen($nombreObraTmp2)>66){
  $separador="_";
  $nombreObraTmp3 = $nombreObraTmp3.$separador;
}
 

$nombreObraTmp4 = substr($nombreObra,194,66);
if(strlen($nombreObraTmp3)>66){
  $separador="_";
  $nombreObraTmp4 = $nombreObraTmp3.$separador;
}

 


$pdf->SetXY(176, 45);
$pdf->Write(0,$nombreObraTmp, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetXY(176, 48);
$pdf->Write(0, $nombreObraTmp2, '', 0, 'L', true, 0, false, false, 0);

$pdf->SetXY(176, 51);
$pdf->Write(0,$nombreObraTmp3, '', 0, 'L', true, 0, false, false, 0);

$pdf->SetXY(176, 54);
$pdf->Write(0, $nombreObraTmp4, '', 0, 'L', true, 0, false, false, 0);






//Municipio
$pdf->SetXY(74,60);
 
$pdf->SetFont('helvetica', '', 9);

$txtMunicipio = $getDataComite['municipio'];
// print a block of text using Write()
$pdf->Write(0, $txtMunicipio, '', 0, 'L', true, 0, false, false, 0);



//Localidad
$pdf->SetXY(189,60);
 
$txtLocalidad = strtoupper($getDataComite['localidad']);

// print a block of text using Write()
$pdf->Write(0, $txtLocalidad, '', 0, 'L', true, 0, false, false, 0);



//Dependencia Ejecutora
$pdf->SetXY(74,65);

$txtDependenciaEjecutora = $getDataComite['instancia_ejecutora'];

// print a block of text using Write()
$pdf->Write(0, $txtDependenciaEjecutora, '', 0, 'L', true, 0, false, false, 0);


//Instancia Normativa
$pdf->SetXY(189,65);
 
$txtNormativa = "SDUOP";

// print a block of text using Write()
$pdf->Write(0, $txtNormativa, '', 0, 'L', true, 0, false, false, 0);




//PRIMER INTEGRANTE
       $nombre = strtoupper($listIntegrantes[0]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[0]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[0]['apmaterno_integrante']);


    //NOMBRE
    $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(58, 95);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Cargo
    $pdf->SetXY(190, 98);
    $txtPrograma = strtoupper($listIntegrantes[0]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


      //Telefono
     $pdf->SetXY(156, 103);
    $txtPrograma = $listIntegrantes[0]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Direccion  
    $calle = strtoupper($listIntegrantes[0]['calle_integrante']);
    $num = $listIntegrantes[0]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[0]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    $pdf->SetXY(58, 120);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);











    //segundo INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(58, 112);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Cargo
    $pdf->SetXY(190, 115);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(58, 103);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(156, 120);
    $txtPrograma = $listIntegrantes[0]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);











    //tercer INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(58, 129);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Cargo
    $pdf->SetXY(190, 132);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(58, 137);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(156, 137);
    $txtPrograma = $listIntegrantes[1]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);






    //cuarto INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(58, 146);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Cargo
    $pdf->SetXY(190, 149);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'J', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(58, 154);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(156, 154);
    $txtPrograma = $listIntegrantes[1]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);










    //quinto INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(58, 163);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Cargo
    $pdf->SetXY(190, 166);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, 'VOCAL', '', 0, 'J', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(58, 171);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(156, 171);
    $txtPrograma = $listIntegrantes[1]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);





    //sexto INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(58, 180);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);

 

    //Cargo
    $pdf->SetXY(190, 183);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'J', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(58, 188);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(156, 188);
    $txtPrograma = $listIntegrantes[1]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);

//public function Write($h, $txt, $link='', $fill=false, $align='', $ln=false, $stretch=0, $firstline=false, $firstblock=false, $maxh=0, $wadj=0, $margin='')





// add a page
$pdf->AddPage('L');


// -- set new background ---

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
   $img_file = "/var/www/davinci2/assets/formats/capacitacion/capacitacion2.jpg";
  // $pdf->setJPEGQuality(75);

   // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)

$pdf->Image($img_file, 10, 0, 275, 210, '', '', '', false, 300, '', false, false, 0);
//$pdf->Image($img_file, 13, 0, 287, 210, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();


// Print a text
 

// ---------------------------------------------------------


//ASAMBLEA
    //si
  /* $pdf->SetXY(240, 99);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/


    //no
    $pdf->SetXY(258, 99);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



//EQUIDAD
    //si
 /*  $pdf->SetXY(240, 103);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/


    //no
    $pdf->SetXY(258, 103);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);





//FICHA INFORMATIVA
    //si
   $pdf->SetXY(240, 107);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //no
   /* $pdf->SetXY(258, 107);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/



 //REGLAS OPERACION
    //si
   $pdf->SetXY(240, 111);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //no
  /*  $pdf->SetXY(258, 111);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/



     //FORMATOS
    //si
  /* $pdf->SetXY(240, 116);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/


    //no
    $pdf->SetXY(258, 116);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



  //DISPOSICION QUEJAS
    //si
   $pdf->SetXY(240, 120);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //no
    /*$pdf->SetXY(258, 120);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/









 ob_end_clean();
$pdf->Output('example_007.pdf', 'I');
//============================================================+






     function transformDateToView($data){

        if($data == "0000-00-00"){
            return "";
        }



        $arrayDate = explode("-", $data);
        if(strlen($data)>6){
            $newData = $arrayDate[2] . "/" . $arrayDate[1] . "/"  . $arrayDate[0];
            return($newData);
        }
    }
