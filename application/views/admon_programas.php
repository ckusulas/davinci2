
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIQSEQ V2.0</title>

      <?php $this->view('template/header.php'); ?>   

</head>

<body>

          <div id="modalNew" class="modal fade modal-nuevo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <div id="subTitle"><h4 class="modal-title">Alta Nuevo Programa</h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form  action="#" id="form_programa" class="form-horizontal"   >


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Nombre del Programa</label>
                                    <div class="col-md-6">
                                        <input  name="nombrePrograma" id="nombrePrograma" type="text" class="form-control"  >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Recurso</label>
                                     <div class="col-sm-6">
                                        <select class="form-control" name="recurso" id="recurso">
                                             <option value="0">Seleccione.. </option>                                                             
                                                <?php
                                                foreach ($listRecourses as $row) {
                                                    $selected = "";

                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['nombre']) . "</option>\n";
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <input type="hidden" name="complexSelect" id="complexSelect">
                                <input type="hidden" name="areaSelect" id="areaSelect">
                                <input type="hidden" name="locationSelect" id="locationSelect">
                                <input type="hidden" name="capacitorSelect" id="capacitorSelect">
                                <input type="hidden" name="typeSelect" id="typeSelect">
                                <input type="hidden" name="categorySelect" id="categorySelect">
                                <input type="hidden" name="modalitySelect" id="modalitySelect">
                   

                            <div align="right">
                                <button type="button"  onClick="guardarPrograma()" class="btn btn-success waves-effect waves-light">Guardar</button>&nbsp;&nbsp;&nbsp;
                            </div>
                        </form>

                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



      

    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

        
          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
                    <!-- begin MAIN PAGE ROW -->
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                <tr>
                                    <td>
                                       <h1>
                                           
                                        </h1>
                                     </td>
                                     <td align="right">
                                         <!--<button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  ><i class="fa fa-floppy-o"></i> Nuevo</button> -->
                                          <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal"   onClick='activar_form()'><i class="fa fa-pencil"></i> Editar</button> 
                                          
                                            <button type="button" class="btn btn-success waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='saveElement()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                     </td>
                                </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Fuente de Financiamiento</i>
                                </li>
                                <li class="active">Registar F.Financiamiento</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                                </ol>
                                <input type="hidden" name="programSelected" id="programSelected">
                    
                            </div>
                        </form>                    
                        </div>
                    <!-- /.col-lg-12 -->
                    </div>
                    </div>






                <div class="row">



                            <div class="col-lg-12">

                      <div class="row">

                      <div class="col-lg-12">

                                <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Resumen de Recurso Asignado</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#ResumenRecurso"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="ResumenRecurso" class="panel-collapse collapse in">
                                            <div class="portlet-body">
                                            <form class="form-horizontal forma" id="form_resumenasignado" >

                             
 
                                         
<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>First name</th>
                <th>Last name</th>
                <th>Position</th>
                <th>Office</th>
                <th width="18%">Start date</th>
                <th>Salary</th>
            </tr>
        </thead>
    </table>
                                       
                                        <input type="hidden" name="totalFederalRA" id="totalFederalRA"> 
                                        <input type="hidden" name="totalEstatalRA" id="totalEstatalRA">
                                        <input type="hidden" name="totalMunicipalRA" id="totalMunicipalRA">
                                        <input type="hidden" name="totalOtrosRA" id="totalOtrosRA">
                                        <input type="hidden" name="totalRecursosRA" id="totalRecursosRA">

                                          
                                               
                                            </form>
                                        </div>
                                    </div>
                              
                                <!-- /.portlet -->
                            </div>
                         

                    </div>


                  </div>






                  

                </div>


            
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->
                        </div>
                    </div>




                            <!-- Inline Form Example -->


                    
                <!-- /.row -->
                <!-- end MAIN PAGE ROW -->
                
     

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.number.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
 
 
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?>     


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>



    <script type="text/javascript">

      var editor; // use a global for the submit and return data rendering in the examples
  


        function addRecourse(){
            var cadenaHTML;
            var contador;

            if($("#countResourceFF").val()<=2){

                contador = parseInt($("#countResourceFF").val()) + 1;

            }

             var label = "view_resource"+contador;
             
             $("#"+label).show("slow"); 
             
            $("#countResourceFF").val(contador);
        }


        function addRecourseEstatal(){
            var cadenaHTML;
            var contador;

            
            if($("#countResourceFFEstatal").val()<=2){
             
             contador = parseInt($("#countResourceFFEstatal").val()) + 1;
            }  

             var label = "view_resourceEstatal"+contador;
             

             $("#"+label).show("slow");


             
             
            $("#countResourceFFEstatal").val(contador);
        }

        function addRecourseMunicipal(){
            var cadenaHTML;
            var contador;


            if($("#countResourceFFMunicipal").val()<=2){
             contador = parseInt($("#countResourceFFMunicipal").val()) + 1;
            }

             var label = "view_resourceMunicipal"+contador;
      
             

             $("#"+label).show("slow");


             
             
            $("#countResourceFFMunicipal").val(contador);
        }



        function addRecourseOtros(){
            var cadenaHTML;
            var contador;

            if($("#countResourceFFMunicipal").val()<=2){
             contador = parseInt($("#countResourceFFOtros").val()) + 1;
            }
            
             var label = "view_resourceOtros"+contador;      
             

             $("#"+label).show("slow");

             
             
            $("#countResourceFFOtros").val(contador);
        }

 

        $(document).ready(function () {

      
         $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */

           // 
    
           <?php

            if($editData['onlyView']){
               

               echo "desactivar_form()";

            
            }

     
            ?>
   


                var resourceSelected = $("#recurso_sel option:selected").val();     
                 //  alert(resourceSelected);

                if(resourceSelected == 2){
                    $("#presupuesto_pef").prop("disabled", "true");
                    $("#label_pef").hide("slow");
                   
                    $("#instancia_arriba").html("Instancia Promotora:");
                    $("#instancia_abajo").html("Instancia Ejecutora:");
                    $("#modulo_ramo").hide("slow");

                }else{
                    $("#label_pef").show("slow");
                    $("#instancia_prom").html("Normativa ejecutora:");          
                    $("#instancia_arriba").html("Instancia Normativa:");
                    $("#instancia_abajo").html("Instancia Promotora:");
                    $("#modulo_ramo").show("slow");

                }
                
 


 
 

            $("#fechaFederal_1").keypress(function(event) {event.preventDefault();});
            $("#fechaFederal_2").keypress(function(event) {event.preventDefault();});
            $("#fechaFederal_3").keypress(function(event) {event.preventDefault();});

            $("#fechaEstatal_1").keypress(function(event) {event.preventDefault();});
            $("#fechaEstatal_2").keypress(function(event) {event.preventDefault();});
            $("#fechaEstatal_3").keypress(function(event) {event.preventDefault();});
          
            $("#fechaMunicipal_1").keypress(function(event) {event.preventDefault();});
            $("#fechaMunicipal_2").keypress(function(event) {event.preventDefault();});
            $("#fechaMunicipal_3").keypress(function(event) {event.preventDefault();});

            $("#fechaOtros_1").keypress(function(event) {event.preventDefault();});
            $("#fechaOtros_2").keypress(function(event) {event.preventDefault();});
            $("#fechaOtros_3").keypress(function(event) {event.preventDefault();});
          
          
          


 
           sumaMontoFederal();

             
        
            changeProgram("undefined");


editor = new $.fn.dataTable.Editor( {
        ajax: " ",
        table: "#example",
        fields: [ {
                label: "First name:",
                name: "first_name"
            }, {
                label: "Last name:",
                name: "last_name"
            }, {
                label: "Position:",
                name: "position"
            }, {
                label: "Office:",
                name: "office"
            }, {
                label: "Extension:",
                name: "extn"
            }, {
                label: "Start date:",
                name: "start_date",
                type: "datetime"
            }, {
                label: "Salary:",
                name: "salary"
            }
        ]
    } );
 
    $('#example').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this, {
            onBlur: 'submit'
        } );
    } );
 
    $('#example').DataTable( {
        dom: "Bfrtip",
        ajax: "../php/staff.php",
        columns: [
            {
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            },
            { data: "first_name" },
            { data: "last_name" },
            { data: "position" },
            { data: "office" },
            { data: "start_date" },
            { data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
        ],
        order: [ 1, 'asc' ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit",   editor: editor },
            { extend: "remove", editor: editor }
        ]
    } );
}






            $.datepicker.regional['es'] = {
                  closeText: 'Cerrar',
                  prevText: '<Ant',
                  nextText: 'Sig>',
                  currentText: 'Hoy',
                  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                  monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                  dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                  dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                  weekHeader: 'Sm',
                  dateFormat: 'dd/mm/yy',
                  firstDay: 1,
                  isRTL: false,
                  showMonthAfterYear: false,
                  yearSuffix: ''
                };

                $.datepicker.setDefaults($.datepicker.regional['es']);



            

          $( function() {
            $( "#fechaFederal_1" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaFederal_2" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaFederal_3" ).datepicker({ dateFormat: 'dd/mm/yy' });

            $( "#fechaEstatal_1" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaEstatal_2" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaEstatal_3" ).datepicker({ dateFormat: 'dd/mm/yy' });

            $( "#fechaMunicipal_1" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaMunicipal_2" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaMunicipal_3" ).datepicker({ dateFormat: 'dd/mm/yy' });
 
 

            $( "#fechaOtros_1" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaOtros_2" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaOtros_3" ).datepicker({ dateFormat: 'dd/mm/yy' });
          } );

           
         



            $("#catalogResources").change(function () {

                var resourceSelected = $("#catalogResources option:selected").val();     

                populate_programs(resourceSelected);

            });

            function populate_programs(fromResource) {
             
                var cantElement = 0;
                $('#catalogPrograms').empty();
                $('#catalogPrograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_programs') ?>/" + fromResource,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogPrograms').empty();
                        $('#catalogPrograms').append("<option>Selecciona Programa..</option>");


                        $.each(data, function (i, name) {
                            $('#catalogPrograms').append('<option value="' + data[i].id + '">' + data[i].programa + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   


      
 

            $("#catalogPrograms").change(function () {

                var programSelected = $("#catalogPrograms option:selected").val();   
                populate_subprograms(programSelected);

            });





            $( "#programaSeleccionado" ).change(function() {
 

               var resourceSelected = $("#recurso_sel option:selected").val();     
               
                if(resourceSelected == 2){
                    $("#presupuesto_pef").attr("disabled", "true");
                    $("#instancia_prom").html("Instancia Promotora:");
                }else{
                    $("#presupuesto_pef").removeAttr( "disabled" );
                     $("#instancia_prom").html("Normativa ejecutora:");
                }
                
                
});         
 


            function desactivar_form(){

                $("#form_ffinanciamiento :input").prop("disabled", true);
                $("#form_beneficios :input").prop("disabled", true);
                $("#form_convenio :input").prop("disabled", true);
                $("#form_ramo :input").prop("disabled", true);
                $("#form_resumenasignado :input").prop("disabled", true);
                $("#form_recursoOtros :input").prop("disabled", true);
                $("#form_recursoMunicipal :input").prop("disabled", true);
                $("#form_recursoEstatal :input").prop("disabled", true);
                $("#form_recursoFederal :input").prop("disabled", true);
        
    
            }

     

           //  $('#catalogSubprograms').removeAttr("disabled")

           function populate_subprograms(fromProgram) {
                var cantElement = 0;
                $('#catalogSubprograms').empty();
                $('#catalogSubprograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_subprograms') ?>/" + fromProgram,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogSubprograms').empty();
                        $('#catalogSubprograms').append("<option>Selecciona subprograma..</option>");
        

                        $.each(data, function (i, name) {
                          //  $('#catalogSubprograms').removeAttr("disabled")
                            $('#catalogSubprograms').append('<option value="' + data[i].id + '">' + data[i].subprograma + '</option>');
                            cantElement++;
                        });                         

                        if(cantElement==0){
                            $('#catalogSubprograms').attr('disabled', 'disabled');
                        }
                    }
                });
            }



            $('#vigila_federal').numeric(",");
            $('#vigila_estatal').numeric(",");
            $('#vigila_municipal').numeric(",");
            $('#vigila_otros').numeric(",");
            $('#total_acciones').numeric();
           

            $('#presupuesto_pef').number( true, 2 );
            $('#presupuesto_vigilar_cs').number( true, 2 );

            $('#montoFederal_1').number( true, 2 );
            $('#montoFederal_2').number( true, 2 );
            $('#montoFederal_3').number( true, 2 );

            $('#montoEstatal_1').number( true, 2 );
            $('#montoEstatal_2').number( true, 2 );
            $('#montoEstatal_3').number( true, 2 );

            $('#montoMunicipal_1').number( true, 2 );
            $('#montoMunicipal_2').number( true, 2 );
            $('#montoMunicipal_3').number( true, 2 );


            $('#montoOtros_1').number( true, 2 );
            $('#montoOtros_2').number( true, 2 );
            $('#montoOtros_3').number( true, 2 );

            $('#totalFederal').number( true, 2 );
            $('#total_recursoFederal').number( true, 2 );


            $('#totalEstatal').number( true, 2 );
            $('#total_recursoEstatal').number( true, 2 );

            $('#total_recursoMunicipal').number( true, 2 );
            $('#totalMunicipal').number( true, 2 );

            $('#total_recursoOtros').number( true, 2 );
            $('#totalOtros').number( true, 2 );





            $('#total_recursosasignados').number( true, 2 );





            
            

          }); 


      function activar_form(){              
               
                $("#ejercicio_fiscal").removeAttr("disabled");
                $("#presupuesto_pef").removeAttr("disabled");
                $("#presupuesto_vigilar_cs").removeAttr("disabled");
                $("#descripcionPoblacion").removeAttr("disabled");

                $("#form_beneficios :input").removeAttr("disabled");
                $("#form_convenio :input").removeAttr("disabled");
                $("#form_ramo :input").removeAttr("disabled");
           //     $("#form_resumenasignado :input").removeAttr("disabled");
                $("#form_recursoOtros :input").removeAttr("disabled");
                $("#form_recursoMunicipal :input").removeAttr("disabled");
                $("#form_recursoEstatal :input").removeAttr("disabled");
                $("#form_recursoFederal :input").removeAttr("disabled");
            
            } 


        function resetearForma(){

           
            $(".form-control").each(function() {
               $(this).val("");  
            });
        }


        function formatDecimal(input) {
            var val = '' + (+input.value);
            if (val) {
                val = val.split('\.');
                var out = val[0];
                while (out.length < 3) {
                    out = '0' + out;
                }
                if (val[1]) {
                    out = out + '.' + val[1]
                    if (out.length < 6) out = out + '0';
                } else {
                    out = out + '.00';
                }
                input.value = out;
            } else {
                input.value = '000.00';
            }
        }


    function addCommas(obj){

            x = obj.value;

            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            obj.value = parts.join(".");

            
    }   


        function permite(elEvento, permitidos) {
          // Variables que definen los caracteres permitidos
          var numeros = "0123456789";
          var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
          var numeros_caracteres = numeros + caracteres;
          var teclas_especiales = [8, 37, 39, 46];
          // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha
         
         
          // Seleccionar los caracteres a partir del parámetro de la función
          switch(permitidos) {
            case 'num':
              permitidos = numeros;
              break;
            case 'car':
              permitidos = caracteres;
              break;
            case 'num_car':
              permitidos = numeros_caracteres;
              break;
          }
         
          // Obtener la tecla pulsada 
          var evento = elEvento || window.event;
          var codigoCaracter = evento.charCode || evento.keyCode;
          var caracter = String.fromCharCode(codigoCaracter);
         
          // Comprobar si la tecla pulsada es alguna de las teclas especiales
          // (teclas de borrado y flechas horizontales)
          var tecla_especial = false;
          for(var i in teclas_especiales) {
            if(codigoCaracter == teclas_especiales[i]) {
              tecla_especial = true;
              break;
            }
          }
         
          // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
          // o si es una tecla especial
          return permitidos.indexOf(caracter) != -1 || tecla_especial;
        }


        function sumaVigilado(){

            var total = 0;

                $(".t_vigilado").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

            $("#total_vigilado").val(total);

        }


        function sumaMontoFederal(){



            var total = 0;

                $(".t_federal").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

                x = total;

                  $("#total_recursoFederal").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalFederal").val(total);
          
            sumaTotales();

        }


        function sumaMontoEstatal(){

             var total = 0;

             $(".t_estatal").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

                x = total;

                  $("#total_recursoEstatal").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalEstatal").val(total);
          
         

           
           

           sumaTotales();



        }


        

        function sumaMontoMunicipal(){

 
            var total = 0;

                $(".t_Municipal").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

                x = total;

                  $("#total_recursoMunicipal").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalMunicipal").val(total);
          
            sumaTotales();

        }


        function sumaMontoOtros(){


             var total = 0;

                $(".t_Otros").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
              
                });

                x = total;

                  $("#total_recursoOtros").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalOtros").val(total);
          
            sumaTotales();

 

        }


      function sumaTotales(){
 
            var total = 0;

            /*    $(".t_totales").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

            $("#total_recursosasignados").val(total);*/



                $(".t_totales").each(function() {


                   if (this.value!="") {
                        //total += parseFloat(this.value);
                        //alert(this.value);
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
                        //if(parseFloat(cantidadString.replace(',',''))!=='NaN'){
                      // alert("numeroasumar:" + parseFloat(cantidadString.replace(/,/g , "")));

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

          

                  $("#total_recursosasignados").val(total);

        
          
             

        }




        function sumaPeople(){

            var total = 0;

             $(".t_people").each(function() {


                   if (this.value!="") {
                        //total += parseFloat(this.value);
                        //alert(this.value);
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
                        //if(parseFloat(cantidadString.replace(',',''))!=='NaN'){
                      // alert("numeroasumar:" + parseFloat(cantidadString.replace(/,/g , "")));

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });


                x = total;

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");
          

                  $("#total_people").val(total);

              

        }

        





        function jsUcfirst(string) 
        {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }   


        function cleanElement(){

            swal({
              title: "Estas seguro de Crear nuevo Registro?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: false,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Listo!, Empezamos de nuevo la captura", {
                  icon: "success",
                });
                resetearForma();

              } else {
                swal("Lo dejamos como estaba..");
              }
            });
        }


         function deleteElement(){

            swal({
              title: "Estas seguro de querer Eliminar?",
              text: "Una vez eliminado, No podras recuperar. ",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Se ha borrado exitosamente.", {
                  icon: "success",
                });
              } else {
                swal("Ok. Lo dejamos como estaba..");
              }
            });
        }




        function saveElement(){

  
                var resourceSelected;
                var programSelected;
                var subProgramSelected;
                var ejercirseFiscal;
                var msgError = "";
                

                
                
                resourceSelected = $('#catalogResources option:selected').val();
                programSelected =  $('#catalogPrograms option:selected').val();
                subProgramSelected =  $('#catalogSubprograms option:selected').val();
                ejercirseFiscal = $('#ejercicio_fiscal').val();

    
              



                


    
                //Validar campos antes de mandar
    
                




                $("#resourceSelected").val(resourceSelected);
                $("#programSelected").val(programSelected);
                $("#subProgramSelected").val(subProgramSelected);
                $("#exercirseFiscal").val(ejercirseFiscal);


               

                $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarFFinanciamiento') ?>",
                    type: "POST",
                    data: $('#form_ffinanciamiento').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                       // idRegistro = message;
                        
 
                    }
                });

                 

                  var totalBeneficios;

                  totalBeneficios = $("#total_people").val();

                    //$("#idRegistro_1").val(idRegistro);
                    $("#totalBeneficios").val(totalBeneficios);
                     
        

                   

                      


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarBeneficios') ?>",
                    type: "POST",
                    data: $('#form_beneficios').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                        
                    }
                });




                 var totalBeneficios;

                  totalBeneficios = $("#total_people").val();
     

                var convenioSelect;
                var instanciaPromotora;
                

                
                
                convenioSelect = $('#catalogConvenio option:selected').val();
                instanciaPromotora = $('#cataloInstanciaPromotora option:selected').val();
                instanciaAbajo = $('#cataloInstanciaAbajo option:selected').val();

                $("#catalogConvenioSelected").val(convenioSelect);
                $("#instanciaPromotoraSelected").val(instanciaPromotora);
                $("#instanciaAbajoSelected").val(instanciaAbajo);

                      


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarConvenio') ?>",
                    type: "POST",
                    data: $('#form_convenio').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                       
                    }
                });

                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRamo') ?>",
                    type: "POST",
                    data: $('#form_ramo').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                       
                    }
                });



                var totalBeneficios;

                totalBeneficios = $("#total_people").val();
     


                //Federal
                var anioRecurso1_Select;
                var anioRecurso2_Select;
                var anioRecurso3_Select;
                 

                
                 
                
                anioRecurso1_Select = $('#perteneceAnioRecurso1 option:selected').val();
                anioRecurso2_Select = $('#perteneceAnioRecurso2 option:selected').val();
                anioRecurso3_Select = $('#perteneceAnioRecurso3 option:selected').val();
               
                $("#perteneceAnioRecurso1_selected").val(anioRecurso1_Select);
                $("#perteneceAnioRecurso2_selected").val(anioRecurso2_Select);
                $("#perteneceAnioRecurso3_selected").val(anioRecurso3_Select);
                
                      


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRecursoFederal') ?>",
                    type: "POST",
                    data: $('#form_recursoFederal').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });



               //Estatal
                var anioRecursoEstatal1_Select;
                var anioRecursoEstatal2_Select;
                var anioRecursoEstatal3_Select;
                 

                  
                anioRecursoEstatal1_Select = $('#perteneceAnioRecursoEstatal1 option:selected').val();
                anioRecursoEstatal2_Select = $('#perteneceAnioRecursoEstatal2 option:selected').val();
                anioRecursoEstatal3_Select = $('#perteneceAnioRecursoEstatal3 option:selected').val();
               
                $("#perteneceAnioRecursoEstatal_selected_1").val(anioRecursoEstatal1_Select);
                $("#perteneceAnioRecursoEstatal_selected_2").val(anioRecursoEstatal2_Select);
                $("#perteneceAnioRecursoEstatal_selected_3").val(anioRecursoEstatal3_Select);
                
                      


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRecursoEstatal') ?>",
                    type: "POST",
                    data: $('#form_recursoEstatal').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });



                   
                      


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRecursoMunicipal') ?>",
                    type: "POST",
                    data: $('#form_recursoMunicipal').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });


                 //Otros
                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRecursoOtros') ?>",
                    type: "POST",
                    data: $('#form_recursoOtros').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });






                //Resumen
                $("#totalFederalRA").val($("#total_recursoFederal").val());
                $("#totalMunicipalRA").val($("#total_recursoMunicipal").val());
                $("#totalEstatalRA").val($("#total_recursoEstatal").val());
                $("#totalOtrosRA").val($("#total_recursoOtros").val());
                $("#totalRecursosRA").val($("#total_recursosasignados").val());

    
                

                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarResumenAsignado') ?>",
                    type: "POST",
                    data: $('#form_resumenasignado').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });



                if(msgError!=""){
                     swal("Error!", msgError, "error");
                }else{
                    swal("Bien hecho!", "La Fuente de Financiamiento ha sido guardado.", "success");
                }










            }


         // forceNumeric() plug-in implementation
         jQuery.fn.forceNumeric = function () {

             return this.each(function () {
                 $(this).keydown(function (e) {
                     var key = e.which || e.keyCode;

                     if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                     // numbers   
                         key >= 48 && key <= 57 ||
                     // Numeric keypad
                         key >= 96 && key <= 105 ||
                     // comma, period and minus, . on keypad
                        key == 190 || key == 188 || key == 109 || key == 110 ||
                     // Backspace and Tab and Enter
                        key == 8 || key == 9 || key == 13 ||
                     // Home and End
                        key == 35 || key == 36 ||
                     // left and right arrows
                        key == 37 || key == 39 ||
                     // Del and Ins
                        key == 46 || key == 45)
                         return true;

                     return false;
                 });
             });
         }



         $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });



         function quitRecurso(elem){

             var noElem = elem.toString();

             contador = parseInt($("#countResourceFF").val()) - 1;
              $("#countResourceFF").val(contador);

             
              
              $("#fuenteFinanciamFederal_"+noElem).val('');
              $("#fechaFederal_"+noElem).val('');
              $("#montoFederal_"+noElem).val('');
            
            sumaMontoFederal();

            $("#view_resource"+noElem).hide('slow');
         }



         function quitRecursoEstatal(elem){

             var noElem = elem.toString();

             contador = parseInt($("#countResourceFFEstatal").val()) - 1;
              $("#countResourceFFEstatal").val(contador);

              
             
              $("#fuenteEstatal_"+noElem).val('');
              $("#fechaEstatal_"+noElem).val('');
              $("#montoEstatal_"+noElem).val('');
            
            sumaMontoEstatal();

            $("#view_resourceEstatal"+noElem).hide('slow');
         }

        function quitRecursoMunicipal(elem){

            var noElem = elem.toString();

             contador = parseInt($("#countResourceFFMunicipal").val()) - 1;
              $("#countResourceFFMunicipal").val(contador);

              $("#fuenteMunicipal_"+noElem).val('');
              $("#fechaMunicipal_"+noElem).val('');
              $("#montoMunicipal_"+noElem).val('');
            
            sumaMontoMunicipal();

            $("#view_resourceMunicipal"+noElem).hide('slow');
         }


        function quitRecursoOtros(elem){

             var noElem = elem.toString();

             contador = parseInt($("#countResourceFFOtros").val()) - 1;
              $("#countResourceFFOtros").val(contador);

         
 
             $("#fuenteFinanciamOtros_"+noElem).val('');
              $("#fechaOtros_"+noElem).val('');
              $("#montoOtros_"+noElem).val('');
            
            sumaMontoOtros();

            $("#view_resourceOtros"+noElem).hide('slow');
         }



         
         

 



        function changeProgram(checked){

           


            var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
           // $("#programSelected").

             if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");

                    return;
                }

              

               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                             recurso = obj.recurso;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").show("slow");

                      
                      if(recurso == 1){
                         $("#patcs_menu").hide();
                        $("#petcs_menu").show("slow");
                    }

                     if(recurso == 2){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow");
                    }

    
                    }
                });
 
            if(checked!="undefined"){
               
                swal({
                  title: "Estás seguro de querer cambiar Programa?",
                  text: "Nota: Se perderan los datos, antes debes guardar los datos.",
                  icon: "warning",
                  buttons: true,
                  dangerMode: false, 
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Listo!, Se cambio programa exitosamente", {
                      icon: "success",
                    });
                    location.reload(true);

                  } else {
                    swal("Lo dejamos como estaba..");
                  }
                });
            }



 
                     //location.reload(true);

               
        }





          function guardarPrograma(){


            $("#nombrePrograma").css({"background-color": "white"});
             $("#recurso").css({"background-color": "white"});

            var msgError = "";

            if( $("#nombrePrograma").val() == ""){
                msgError = "Debe existir nombre programa al menos, ";
                $("#nombrePrograma").css({"background-color": "yellow"});
            }

            
            if($("#recurso option:selected").val() == "Seleccione.."){
                 msgError+=" Debe existir un Recurso al menos, ";
                $("#recurso").css({"background-color": "yellow"});

            }



            

            if(msgError==""){

                     
                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/guardarNuevoPrograma') ?>",
                    type: "POST",
                    data: $('#form_programa').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = true;
                            titulo_programa = "";
                        });

                    if(status){

                         $('.modal-nuevo').modal("hide");

                                     swal("Bien hecho!", "El programa ha sido guardado. Ahora puedes usarlo.  Seleccionando en la Lista de Programas", "success");

                    }

    
                    }
                });





   
            }else{
                swal("Error!", msgError, "error");
            }

        }


        


 



    </script>



</body>

</html>
