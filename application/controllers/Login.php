<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


	public function __construct() {
        parent::__construct();

        $this->load->model('login_model');
        $this->load->model('ffinanciamiento_model');



        $this->load->helper('security');
    }

 
	public function index()
	{

		$this->load->view('login');

	}



  public function programaInscrito($user_id, $departamento){



      $programasInscrito = $this->ffinanciamiento_model->getListProgramasInscrito($user_id, $departamento);
      $programasInscritoEstatal = $this->ffinanciamiento_model->getListProgramasInscrito($user_id, $departamento,2019,2);
      $programasInscritoFederal = $this->ffinanciamiento_model->getListProgramasInscrito($user_id, $departamento,2019,1);


         $data = array(
       
                'programasInscrito' => $programasInscrito,
                'programasInscritoFederal' => $programasInscritoFederal,
                'programasInscritoEstatal' => $programasInscritoEstatal,
                      
                          
          );
   

        $this->session->set_userdata($data);
 
  }

	public function validate()
	{

	 
		
		$login = $this->input->post('uname');
        $password = $this->input->post('psw');


        $data = array(
            'username' => xss_clean($login),
            'password' => xss_clean($password),
        );


         $check_user = $this->login_model->login_success($data);

       	if($check_user){




          $this->programaInscrito($check_user->id, $check_user->fk_departamento);
       		    
         


       	 $data = array(
                'is_logued_in' => TRUE,
                'id' => $check_user->id,
                'pagina_inicio' => $check_user->page_startup,
                'nombre' => $check_user->nombre,
                'appaterno' => $check_user->apPaterno,              
                'photo' => $check_user->avatar,
                'departamento' => $check_user->fk_departamento,
                 
                'position' => $check_user->posicion                  
          );
    
 

        $this->session->set_userdata($data);
 
        //redirect(site_url()."/indicadores_generales/wind/".$check_user->page_startup);
        redirect(site_url()."/indicadores_generales/");



       	}else{
       		redirect(site_url()."/");

       	}

       


	}



	public function logout() {
       $data = array(
              /*  'id' => "",
                'pagina_inicio' => "",
                'nombre' => "",
                'appaterno' => "",         
                'photo' => "",
                'programasInscrito' => "",
                'departamento' => "",
                'ProgramasSelecc' => "todos",
                'RecursoSelecc' => "",
                'ProgramasSeleccTitulo' => "",*/
                'is_logued_in' => FALSE,
               
            );

          $departamento = $this->session->userdata('departamento');
    
        $departam_Admon = array(1,3,4);
     //   if(!in_array($departamento,$departam_Admon)) {

           $session_arrays = array("id","pagina_inicio","nombre","appaterno","photo","programasInscrito","departamento","ProgramasSelecc","departamento","programaSelecc","ProgramasSeleccTitulo","is_logued_in");

            $this->session->set_userdata($data);
            $this->session->unset_userdata($session_arrays);
            $this->session->sess_destroy();
       //S }
 
 
        
      /*  $data = array(
            'status' => TRUE,
        );
        echo json_encode($data);*/
         redirect('inicio');
    }
}
