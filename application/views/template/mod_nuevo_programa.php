        <div id="modalNew" class="modal fade modal-nuevo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <div id="subTitle2"><h4 class="modal-title">Alta Nuevo Programa</h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form   action="registro_proyecto" id="form_programa" class="form-horizontal"   >


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Nombre del Programa</label>
                                    <div class="col-md-6">
                                        <input  name="nombrePrograma" id="nombrePrograma" type="text" class="form-control"  >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Recurso</label>
                                     <div class="col-sm-6">
                                        <select class="form-control" name="recurso" id="recurso">
                                             <option>Seleccione.. </option>                                                             
                                                <?php
                                                foreach ($listRecourses as $row) {
                                                    $selected = "";

                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['nombre']) . "</option>\n";
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Esquema Normativo</label>
                                     <div class="col-sm-6">
                                        <select class="form-control" name="esquema_normativo" id="esquema_normativo">
                                             <option>Seleccione.. </option>                                                             
                                                <?php
                                                foreach ($listRecourses as $row) {
                                                    $selected = "";

                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['nombre']) . "</option>\n";
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                
                     
                   

                            <div align="right">
                                <button type="button"  onClick="guardarPrograma()" class="btn btn-success waves-effect waves-light">Guardar</button>&nbsp;&nbsp;&nbsp;
                            </div>
                        </form>

                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->