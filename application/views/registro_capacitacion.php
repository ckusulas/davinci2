
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>

       <?php $this->view('template/header.php'); ?>   
</head>

<body>

           <?php $this->view('template/mod_nuevo_programa'); ?>

    <div id="wrapper">

     

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            


                <!-- begin PAGE TITLE ROW -->
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                          
                                         </td>
                                        <td align="right"> 

                                            <button id="btnNuevo" type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='cleanElement()'><i class="fa fa-floppy-o"></i> Nuevo</button> 
                                            <?php
            
                                                if($editData['onlyView']){ ?>
                                      
                                          <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='editar()'><i class="fa fa-floppy-o"></i> Editar</button> 

                                             <? } ?>                                          

                                           

                                            <button id="btnGuardar" type="button" class="btn btn-success waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button> 

                                            <?php
                                             if($editData["metodo"]=="view" || $editData["metodo"]=="edit") {
                                            ?>
                                                  <button type="button" class="btn btn-info waves-effect w-md waves-light m-b-15" id="btnImprimir" data-toggle="modal"  onClick="imprimir();" ><i class="fa fa-floppy-o"></i> Imprimir</button>
                                            <?php
                                                }
                                            ?>
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Capacitación</i>
                                </li>
                                <li class="active">Registrar Capacitación</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
                
                
                 
                

                    <!-- Datos del Comite -->
                    <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>1. Datos de la Capacitación</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                         <div class="portlet-body">
                                            <form class="form-horizontal" id="form_datos_capacitacion" action="#">

 
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">*Nombre del Evento de Capacitación:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" id="nombreCapacitacion" name="nombreCapacitacion" value="<?php echo $editData['nombre_capacitacion']  ?>"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5  ">*Temática:</label>
                                                    <div class="col-sm-6">
                                                        <select name="tematica" class="form-control col-sm-6" id="tematica" onchange="preTematica(this)">
                                                            <option value="">Seleccione..</option>
                                                            <?php
                                                            foreach($listCapacitacionTematica as $row){

                                                                $select = "";
                                                                if($editData['tematica'] == $row['id']){
                                                                    $select = "selected";
                                                                }

                                                                echo "<option value='".$row['id']."' ". $select ." >".$row['campo']."</option>";

                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php
                                                    $style = "style='display:none'";
                                                    if($editData['tematica']=="9"){
                                                          $style = "style='display:block'";
                                                    }

                                                ?>

                                                <div id="view_tematica_otro" class="form-group" <?php echo $style ?>>
                                                    <label for="textInput" class="col-sm-5">Defina tematica:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" id="tematicaOtro" name="tematicaOtro" value="<?php echo $editData['tematica_otro']  ?>"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">*Figura Capacitada:</label>
                                                    <div class="col-sm-6">

                                                        <select name="figuraCapacitada" class="form-control"  id="figuraCapacitada" onChange="preFigura(this)">
                                                            <option value="">Seleccione..</option>
                                                            <?php
                                                            foreach($listFigCapacitada as $row){

                                                                $select = "";
                                                                if($editData['figura_capacitada'] == $row['id']){
                                                                    $select = "selected";
                                                                }

                                                                echo "<option value='".$row['id']."' ".$select." >".$row['campo']."</option>";

                                                            }
                                                            ?>
                                                        </select>
                                                        
                                                    </div>
                                                </div>

                                               <?php
                                                    $style = "style='display:none'";
                                                    if($editData['figura_capacitada']=="13" || $editData['figura_capacitada']=="14" ){
                                                          $style = "style='display:block'";
                                                    }

                                                ?>
                                                <div id="view_figura_otro" class="form-group"   <?php echo $style ?>>
                                                    <label for="textInput" class="col-sm-5">Defina figura:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" id="figuraCapacitadaOtro" name="figuraCapacitadaOtro" value="<?php echo $editData['figura_capacitada_otro']?>" onBlur="preGuardado()" >
                                                    </div>
                                                </div>
                                       

                                             

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">*Fecha de Impartición:</label>
                                                    <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-calendar objfechaImparticion"></i></span> 
                                                                <input type="text" class="form-control input-sm" id="fechaImparticion" name="fechaImparticion" value="<?php echo $editData['fecha_imparticion'] ?>"  >
                                                            </div>
                                                    </div>
                                                </div>

                                                <div class="form-group" style="display:none">
                                                    <label for="textInput" class="col-sm-5 ">Entidad Federativa:</label>
                                                    <div class="col-sm-6" >
                                                        <input type="text" class="form-control input-sm" id="entidadFederativa" value="Querétaro" disabled name="entidadFederativa"  >
                                                    </div>
                                                </div>



                                              
                                                <div class="form-group">
                                                    <label  class="col-sm-5">*Acción de Conexión:</label>
                                                    <div class="col-sm-6">
                                                        

                                                        <select id="listObras"    name="accionConexion[]" class="col-sm-12" onChange="preGuardado()"  style="text-overflow:ellipsis" >                                                                                                              
                                                            <?php 
/*
                                                             echo "<option value='' >Seleccione un Comitè..</option>";
                                                           
                                                            if(count($listComites)>0){ 
                                                            echo "<optgroup label='Comités'>";
     
                                                                foreach($listComites as $row){
                                                                    $selected = "";


                                                                    if($row['id'] == $editData['accion_conexion']){
                                                                        $selected = "selected";

                                                                    }

                                                                    $title = $row["nombre_comite"];
                                                                    $subtitle = substr($title, 0,112);

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."...</option>";
                                                                }

                                                                echo "</optgroup>";
                                                            }*/
                                                            ?>
 
                                                        </select>

                                                    </div>
                                                </div>

                                                 <div class="form-group" style="display:none">
                                                     <div class="col-sm-12">
                                                        <table border=0.5 width="100%">
                                                            <tr>
                                                                <td>No comite</td><td>NOMBRE COMITE</td><td>Obra</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Integrantes:</th>
                                                            </tr>
                                                            <tr>
                                                                <th>Nombre </th><th> ApellidoP </th><th> ApellidoM</th><th>Cargo</th><th>Asistio?</th>
                                                                
                                                            </tr>

                                                            <tr><td>Nombre</td><td>ApellidoP</td><td>ApellidoM</td><td>Cargo</td><td>Si / NO</td></tr>
                                                            <tr><td>Nombre</td><td>ApellidoP</td><td>ApellidoM</td><td>Cargo</td><td>Si / NO</td></tr>
                                                            <tr><td>Nombre</td><td>ApellidoP</td><td>ApellidoM</td><td>Cargo</td><td>Si / NO</td></tr>
                                                            <tr><td>Nombre</td><td>ApellidoP</td><td>ApellidoM</td><td>Cargo</td><td>Si / NO</td></tr>
                                                        </table>


                                                     </div>

                                                 </div>
                                                
                                         


                                                <input type="hidden" id="metodo" name="metodo" value="<?php echo $editData['metodo']?>">
                                                <input type="hidden" id="id_capacitacion" name="id_capacitacion" value="<?php echo $editData['id_capacitacion'] ?>" >




                                                         <?php
                                                         $band_exits_file = 0;
                                                        if(isset($editData['url_listaparticipantes'][0])) {
                                                     
                                                        $band_exits_file = 1;
                                                        }
                                                        ?>

                                                  <input type="hidden" id="archivoCargado" name="archivoCargado" value=<?=$band_exits_file?>>

 
                                              

                                                <br><br>
    
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->





                  

                </div>               
                <!-- fin Datos del Comite -->

 



          





                     <!-- Integrante del Comite -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>2. Beneficiarios Capacitados</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#integrantesComite"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="integrantesComite" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_beneficiariosCapacitados">



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">Mujeres:</label>
                                                    <div class="col-sm-4" ">
                                                         <div class="input-group">
                                                  <span class="input-group-addon"><i class="fa fa-female"></i> </span>
                                                        <input type="text" class="form-control input-sm t_people" style="text-align:right" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  maxlength="10"   id="beneficiarioMujeres" name="beneficiarioMujeres" value="<?php echo  ($editData['beneficiarios_mujeres_capacitados']) ?>" onblur="addCommas(this);sumaPeople()" >
                                                    </div>
                                                    </div>
                                                </div>


 

                                     


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">Hombres:</label>
                                                    <div class="col-sm-4" ">
                                                          <div class="input-group">
                                                  <span class="input-group-addon"><i class="fa fa-male"></i> </span>
                                                        <input type="text" class="form-control input-sm t_people" style="text-align:right"  oninput="this.value=this.value.replace(/[^0-9]/g,'');"  id="beneficiarioHombres" name="beneficiarioHombres"  maxlength="10" f value="<?php echo ($editData['beneficiarios_hombres_capacitados']) ?>"  onblur="addCommas(this);sumaPeople()" >
                                                    </div>
                                                </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">*Número de Participantes:</label>
                                                    <div class="col-sm-4" ">
                                                            <div class="input-group">
                                                  <span class="input-group-addon"><i class="fa fa-users"></i> </span>
                                                        <input type="text"  style="text-align:right"  oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control input-sm" id="numeroParticipantes" name="numeroParticipantes" value="<?php echo  ($editData['num_participantes']) ?>"  >
                                                    </div>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">*Lista de Participantes:</label>
                                                    <div class="col-sm-6" ">

                                                         <input type="file" name="file1" id="file1" onChange="cargarFile(this,1,'capacitacion','listaparticipantes')" >
                                                     
                                                        <div id="band1"></div>
                                                        <div id="url1"></div>


                                                         <?php
                                                         $band_exits_file = 0;
                                                        if(isset($editData['url_listaparticipantes'][0])) {
                                                         //  echo print_r($editData['url_listaparticipantes']);
                                                        ?>
                                                            <a target="_blank" href="<?=$editData['url_listaparticipantes'][0]['url']?>">Archivo cargado</a>
                                                        <?
                                                        $band_exits_file = 1;
                                                        }
                                                        ?>
                                                    </div>
                                                </div>

                                                
                                                
                                            
  
                                                <input type="hidden" name="id_capacitacion_beneficiarios" id="id_capacitacion_beneficiarios" value="<?php echo $editData['id_capacitacion'] ?>">
                                        
                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Documentos del Comite -->



                 <!-- Documentos del Comite -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>3. Preguntas para CS</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#documentosComite"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="documentosComite" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_integComiteCapacitados">



                                        
                                                <?php

                                                $style = "style='display:none'";
                                                if(12 == $editData['figura_capacitada']) {
                                                    $style = "";
                                                }

                                                ?>

                                                <div class="form-group" id="secon" <?=$style?>>
                                                    <label for="textInput" class="col-sm-6">La Secon participó en la Vigilancia de Integración?:</label>
                                                    <div class="col-sm-4" ">
                                                        <?php 
                                                            $selected = "";
                                                            if($editData['secon_participo']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        ?>

                                                       <label class="radio-inline">
                                                          <input type="radio" name="seconParticipo" id="seconParticipo" <?php echo $selected ?>  value="SI">SI
                                                        </label>

                                                        <?php 
                                                            $selected = "";
                                                            if($editData['secon_participo']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        ?>
                                                         <label class="radio-inline">
                                                          <input type="radio" name="seconParticipo" id="seconParticipo" <?php echo $selected ?> value="NO">NO
                                                        </label>
                                                </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-6">*En este acto: ¿Se constituyó el comitè de Contraloria Social?:</label>
                                                    <div class="col-sm-4" ">
                                                        <?php 
                                                            $selected = "";
                                                            if($editData['construyo_comite_cs']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        ?>

                                                       <label class="radio-inline">
                                                          <input type="radio" name="construyoComiteCS" id="construyoComiteCS" <?php echo $selected ?>  value="SI">SI
                                                        </label>

                                                        <?php 
                                                            $selected = "";
                                                            if($editData['construyo_comite_cs']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        ?>
                                                         <label class="radio-inline">
                                                          <input type="radio" name="construyoComiteCS" id="construyoComiteCS" <?php echo $selected ?> value="NO">NO
                                                        </label>
                                                </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-6">*Servidor Público que capacitó:</label>
                                                    <div class="col-sm-6" ">
                                                 
                                                        <input type="text"   class="form-control input-sm" id="servidorCapacito" name="servidorCapacito"   oninput="this.value=this.value.replace(/[^a-zA-Z/-\s]/g,'');" value="<?php echo $editData['servidor_publico_capacito'] ?>" >
                                                    </div>
                                                   
                                                </div>


                                         


                                            
                                                <input type="hidden" name="id_capacitacion_integrantes" id="id_capacitacion_integrantes" value="<?php echo $editData['id_capacitacion'] ?>">
                                        
                                               
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Documentos del Comite -->









                 <!-- Funcion del comité Estatal-->
                   <div class="col-lg-12"  id="modulo_funciones_estatal">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-blue">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>4. Anexo de Capacitacion en Contraloria Social</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#preguntasClave"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="preguntasClave" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_anexoCapacitacion">



 


                                                <div class="form-group">

                                                       <label  class="col-sm-8">1. ¿La constitución del comité se realizó mediante asamblea con los beneficiarios?:</label>
                                                    <div class="col-sm-3">


                                                        <?php 
                                                            $selected = "";
                                                            if($editData['asamblea_beneficiarios']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="SI" name="asamblea_beneficiarios" id="asamblea_beneficiarios" <?php echo $selected ?> >Si</label>

                                                              <?php 
                                                            $selected = "";
                                                            if($editData['asamblea_beneficiarios']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="asamblea_beneficiarios" id="asamblea_beneficiarios" <?php echo $selected ?>>No</label> 
                                                            
                                                    
                                                 
                                                    </div>
                                                 
                                                    
                                                </div>

                                                <div class="form-group">

                                                       <label  class="col-sm-8">2. ¿Durante la asamblea, se realizo invitación a los beneficiarios para que la integración del comité se lleve a cabo con equidad de genero?:</label>
                                                    <div class="col-sm-3">


                                                        <?php 
                                                            $selected = "";
                                                            if($editData['equidad_genero']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="SI" name="equidad_genero" id="equidad_genero" <?php echo $selected ?> >Si</label>

                                                              <?php 
                                                            $selected = "";
                                                            if($editData['equidad_genero']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="equidad_genero" id="equidad_genero" <?php echo $selected ?>>No</label> 
                                                            
                                                    
                                                 
                                                    </div>
                                                 
                                                    
                                                </div>
                                                
                                                <div class="form-group">

                                                       <label  class="col-sm-8">3. ¿Se entregó al comité la ficha informativa correspondiente?:</label>
                                                    <div class="col-sm-3">


                                                        <?php 
                                                            $selected = "";
                                                            if($editData['ficha_informativa']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="SI" name="ficha_informativa" id="ficha_informativa" <?php echo $selected ?> >Si</label>

                                                              <?php 
                                                            $selected = "";
                                                            if($editData['ficha_informativa']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="ficha_informativa" id="ficha_informativa" <?php echo $selected ?>>No</label> 
                                                            
                                                    
                                                 
                                                    </div>
                                                 
                                                    
                                                </div>
                                                <div class="form-group">

                                                       <label  class="col-sm-8">4. ¿Se dieron a conocer las reglas de operación del programa?:</label>
                                                    <div class="col-sm-3">


                                                        <?php 
                                                            $selected = "";
                                                            if($editData['reglas_operacion']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="SI" name="reglas_operacion" id="reglas_operacion" <?php echo $selected ?> >Si</label>

                                                              <?php 
                                                            $selected = "";
                                                            if($editData['reglas_operacion']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="reglas_operacion" id="reglas_operacion" <?php echo $selected ?>>No</label> 
                                                            
                                                    
                                                 
                                                    </div>
                                                 
                                                    
                                                </div>

                                                <div class="form-group">

                                                       <label  class="col-sm-8">5. ¿Se proporcionaron los formatos para uso del comité de contraloria social?:</label>
                                                    <div class="col-sm-3">


                                                        <?php 
                                                            $selected = "";
                                                            if($editData['formatos_cs']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="SI" name="formatos_cs"  id="formatos_cs" <?php echo $selected ?> >Si</label>

                                                              <?php 
                                                            $selected = "";
                                                            if($editData['formatos_cs']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="formatos_cs" id="formatos_cs" <?php echo $selected ?>>No</label> 
                                                            
                                                    
                                                 
                                                    </div>
                                                 
                                                    
                                                </div>

                                                
                                                 <div class="form-group">

                                                       <label  class="col-sm-8">6. ¿Se pusieron a disposición de los beneficiarios, mecanismos para la atención de Quejas y Denuncias?:</label>
                                                    <div class="col-sm-3">


                                                        <?php 
                                                            $selected = "";
                                                            if($editData['disposicion_quejas']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="SI" name="disposicion_quejas" id="disposicion_quejas" <?php echo $selected ?> >Si</label>

                                                              <?php 
                                                            $selected = "";
                                                            if($editData['disposicion_quejas']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="disposicion_quejas" id="disposicion_quejas" <?php echo $selected ?>>No</label> 
                                                            
                                                    
                                                 
                                                    </div>
                                                 
                                                    
                                                </div>

                                              
                                               <div class="form-group">
                                                    <label for="textInput" class="col-sm-12">Observaciones de la constitución del Comité:</label>
                                                    <div class="col-lg-6">
 
                                                          <textarea   class="form-control" rows="5" id="observaciones_comite"  name="observaciones_comite"><?=$editData['observaciones_comite']?> </textarea>
                                                       
                                                    </div>
                                                </div>  


                                               
                                            
                                             <input type="hidden" name="id_capacitacion_anexo" id="id_capacitacion_anexo">
                                               

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del comité -->



            
                
             

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script type="text/javascript" src="<?php echo asset_url();?>js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="<?php echo asset_url();?>css/bootstrap-multiselect.css" type="text/css"/>

      <link href="<?php echo asset_url();?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <script src="<?php echo asset_url();?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    
  

      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   
    


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">

        var onlyView = 0;
        var v_esquema;

         $(document).ready(function () {


         $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */

            $("#fechaImparticion").keypress(function(event) {event.preventDefault();});


              $('.objfechaImparticion').click(function() {
                    $("#fechaImparticion").focus();
            });  
 
              $('#fechaImparticion').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });

            <?php
            if($editData['onlyView']){
                echo "onlyView = 1;";

                echo "$('#btnGuardar').hide();";

            }

               if($editData['onlyView']){

            
            

                   

                  echo  '  $("#nombreCapacitacion").attr("disabled", true);';
                  echo  '  $("#tematica").attr("disabled", true);';
                  echo  '  $("#figuraCapacitada").attr("disabled", true);';
                  echo  '  $("#listObras").attr("disabled", true);';
                 
                  echo  '  $("#fechaImparticion").attr("disabled", true);';
                  echo  '  $("#municipio").attr("disabled", true);';
                  echo  '  $("#localidad").attr("disabled", true);';
                  echo  '  $("#beneficiarioMujeres").attr("disabled", true);';
                  echo  '  $("#beneficiarioHombres").attr("disabled", true);';
                  echo  '  $("#numeroParticipantes").attr("disabled", true);';
                  echo  '  $("input[name=seconParticipo]").attr("disabled", true);';
                  echo  '  $("input[name=construyoComiteCS]").attr("disabled", true);';
                  echo  '  $("#servidorCapacito").attr("disabled", true);';
                  echo  '  $("#observaciones_comite").attr("disabled", true);';
                        echo  '  $("input[name=asamblea_beneficiarios]").attr("disabled", true);';
                        echo  '  $("input[name=equidad_genero]").attr("disabled", true);';
                        echo  '  $("input[name=ficha_informativa]").attr("disabled", true);';
                        echo  '  $("input[name=reglas_operacion]").attr("disabled", true);';
                        echo  '  $("input[name=formatos_cs]").attr("disabled", true);';
                        echo  '  $("input[name=disposicion_quejas]").attr("disabled", true);';
                }
        
            ?>
                 


            procesarCambio();

            changeProgram("undefined");

       
              
           /*
                


                $('#upload').on('click', function () {
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'registro_comite/upload_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });
*/


          (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                monthsTitle: "Meses",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
            }(jQuery));



    

             $( function() {
            $( "#fechaImparticion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
 
            
          } );




 
       
             


            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();


            $( function() {
            $( "#fechaConstitucion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaInicioEjecucion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaUnicaProgramada" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaUnicaEjecucion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });       
            $( "#fechaFinalProgramada" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaFinalEjecucion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });

            $( "#fechaAsignaciónRecursoFederal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEjecucionRecursoFederal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            
            $( "#fechaAsignaciónRecursoEstatal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEjecucionRecursoEstatal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });

            $( "#fechaAsignaciónRecursoMunicipal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEjecucionRecursoMunicipal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });

            $( "#fechaAsignaciónRecursoOtros" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEjecucionRecursoOtros" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaAprobacionObra" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            
          } );



               $( function() {
            $( "#fechaConstitucion" ).datepicker({ dateFormat: 'dd/mm/yy' });
           
            
          } );



            

          });    



            function loadCatalogFunctions(){

            var resourceSelected = $("#recurso_sel option:selected").val();     
                 //  alert(resourceSelected);

               //  alert("esquema:" + v_esquema);
                
                /*$("#modulo_funciones_federal").show();
                if(resourceSelected == 2 || v_esquema == 1){ 
                    $("#modulo_funciones_estatal").fadeIn("slow");
                   
                }else{           
                     
                    
                     $("#modulo_funciones_estatal").fadeOut("slow");
                }*/
        }



         function sumaPeople(){

            var total = 0;

             $(".t_people").each(function() {


                   if (this.value!="") {
                        //total += parseFloat(this.value);
                        //alert(this.value);
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
                        //if(parseFloat(cantidadString.replace(',',''))!=='NaN'){
                      // alert("numeroasumar:" + parseFloat(cantidadString.replace(/,/g , "")));

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });


                x = total;

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");
          

                  $("#numeroParticipantes").val(total);

              

        }

         function addCommas(obj){

                //alert(obj.id);

                x = obj.value;

                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                obj.value = parts.join(".");

            
        }   


        function preGuardado(){



              $.ajax({
                    url: "<?php echo site_url('registro_capacitacion/guardarDatosCapacitacion') ?>",
                    type: "POST",
                    data: $('#form_datos_capacitacion').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });
                        

                      // alert("id:" + message);
                        

                        $("#metodo").val("edit");
                        
                        $("#id_capacitacion").val(message);
                        $("#id_capacitacion_integrantes").val(message);
                        $("#id_capacitacion_beneficiarios").val(message);
                        $("#id_capacitacion_anexo").val(message);
                      
                      

 
                    }
                });

        }



        function preTematica(campo){
            if(campo.value == 9){
                $("#view_tematica_otro").show("slow");
            }else{  
                    $("#view_tematica_otro").val("");                 
                    $("#view_tematica_otro").hide("slow");
            }
             
        }


        function preFigura(campo){
            if(campo.value == 13 || campo.value == 14 ){
                $("#view_figura_otro").show("slow");
            }else{  
                    $("#view_figura_otro").val("");                 
                    $("#view_figura_otro").hide("slow");
            }

            if(campo.value == 12 ){
                $("#secon").show("slow");
            }else{  
                         
                    $("#secon").hide("slow");
            }
             
        }



         function editar(){

            $("#btnNuevo").show("slow");
            $("#btnGuardar").show("slow");


            <?php

 
            
            if($editData['onlyView']){
               echo  '  $("#nombreCapacitacion").attr("disabled", false);';
                  echo  '  $("#tematica").attr("disabled", false);';
                  echo  '  $("#figuraCapacitada").attr("disabled", false);';
                  echo  '  $("#listObras").attr("disabled", false);';
                 
                  echo  '  $("#fechaImparticion").attr("disabled", false);';
                  echo  '  $("#municipio").attr("disabled", false);';
                  echo  '  $("#localidad").attr("disabled", false);';
                  echo  '  $("#beneficiarioMujeres").attr("disabled", false);';
                  echo  '  $("#beneficiarioHombres").attr("disabled", false);';
                  echo  '  $("#numeroParticipantes").attr("disabled", false);';
                     echo  '  $("input[name=seconParticipo]").attr("disabled", false);';
                  echo  '  $("input[name=construyoComiteCS]").attr("disabled", false);';
                  echo  '  $("#servidorCapacito").attr("disabled", false);';
                  echo  '  $("#servidorCapacito").attr("disabled", false);';
                  echo  '  $("input[name=asamblea_beneficiarios]").attr("disabled", false);';

                       echo  '  $("#observaciones_comite").attr("disabled", false);';
                        echo  '  $("input[name=asamblea_beneficiarios]").attr("disabled", false);';
                        echo  '  $("input[name=equidad_genero]").attr("disabled", false);';
                        echo  '  $("input[name=ficha_informativa]").attr("disabled", false);';
                        echo  '  $("input[name=reglas_operacion]").attr("disabled", false);';
                        echo  '  $("input[name=formatos_cs]").attr("disabled", false);';
                        echo  '  $("input[name=disposicion_quejas]").attr("disabled", false);';

                  
                    // echo  '$("#form_anexoCapacitacion :input").prop("disabled", true);';
                }
              ?>

              onlyView = 0;


        }




         function  cargarFile(elem,alt,seccion,objeto){
 


     
              var file_data = $('#file'+alt).prop('files')[0];
                        var form_data = new FormData();

                        form_data.set("seccion", "");
                        form_data.set("depto", "");
                        form_data.set("fk_depto", "");
                        form_data.set("programa", "");
                        form_data.set("objeto", "");
                        form_data.append('file', file_data);
                        form_data.append("seccion", seccion);
                        form_data.append("depto", '<?=$departamento?>');
                        form_data.append("fk_depto", '<?=$fk_departamento?>');
                        form_data.append("programa", '<?=$fk_programa?>');
                        form_data.append("objeto", objeto);
         


                $.ajax({
                                 url:'<?php echo base_url();?>index.php/upload/do_upload',
                                 type:"post",
                                 //data:new FormData(elem.form),
                                 data:form_data,
                                 processData:false,
                                 contentType:false,

                                 cache:false,
                                 async:false,
                                  success: function(data){
                                 
                                    
                             
                                      $("#band"+alt).html("Carga Exitosa...");
                                      $("#url"+alt).html("<a target='_blank' href='"+data+"'>Previsualizar</a>");

                                      if(data!=""){
                                        $("#archivoCargado").val(1);
                                      }

                               }
                             }); 

          }


        function guardar(){


                validaCamposCapacitacion();


                  if(msgError!=""){
                    swal("Error!", msgError, "error");
                    return;
                }

            preGuardado();



         

                




                    $.ajax({
                    url: "<?php echo site_url('registro_capacitacion/guardarBeneficiariosCapacitados') ?>",
                    type: "POST",
                    data: $('#form_beneficiariosCapacitados').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


                $.ajax({
                    url: "<?php echo site_url('registro_capacitacion/guardarintegComiteCapacitados') ?>",
                    type: "POST",
                    data: $('#form_integComiteCapacitados').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });




                $.ajax({
                    url: "<?php echo site_url('registro_capacitacion/guardaranexoCapacitacion') ?>",
                    type: "POST",
                    data: $('#form_anexoCapacitacion').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });



                

                    
        
 








            swal("Bien hecho!", "La Capacitación ha sido guardado.", "success");
        }


         var msgError = "";
        $("#msgError").val("");


         function validaCamposCapacitacion(){


            msgError = "";
                    $("#nombreCapacitacion").css({"background-color": "white"});  
                    $("#tematica").css({"background-color": "white"});        
                    $("#figuraCapacitada").css({"background-color": "white"});        
                    $("#fechaImparticion").css({"background-color": "white"});        
                    $("#listObras").css({"background-color": "white"});        
                    $("#beneficiarioMujeres").css({"background-color": "white"});        
                    $("#beneficiarioHombres").css({"background-color": "white"});     
                      $('input[name=servidorCapacito]').css({"background-color": "white"});      
                      $('input[name=seconParticipo]').css({"background-color": "white"});      
                    $("#servidorCapacito").css({"background-color": "white"});  
                    $("#equidad_genero").css({"background-color": "white"});  
                    $("#ficha_informativa").css({"background-color": "white"});  

                    if(v_esquema==1){
                         $('input[name=asamblea_beneficiarios]').css({"background-color": "white"});    
                    }


             

                if( $("#nombreCapacitacion").val() == ""){
                    msgError+= "- Debe tener Nombre del Evento de Capacitación, \n";
                    $("#nombreCapacitacion").css({"background-color": "yellow"});
                }


   
             
                if( $("#tematica").val() == ""){
                     msgError+= "- Debe seleccionar Tematica. \n";
                      $("#tematica").css({"background-color": "yellow"});
                    
                }

                if( $("#figuraCapacitada").val() == ""){
                     msgError+= "- Debe Figura Capacitada. \n";
                      $("#figuraCapacitada").css({"background-color": "yellow"});
                    
                }

              


                if( $("#fechaImparticion").val() == ""){
                    msgError+= "- Debe tener Fecha de Impartición, \n";
                    $("#fechaImparticion").css({"background-color": "yellow"});
                }



                 if( $("#listObras").val() == ""){
                     msgError+= "- Debe seleccionar al menos una Accion de Conexiòn. \n";
                      $("#listObras").css({"background-color": "yellow"});
                    
                }



                /*if( $("#beneficiarioMujeres").val() == ""){
                    msgError+= "- Debe poner cantidad de beneficiarios Mujeres, \n";
                    $("#beneficiarioMujeres").css({"background-color": "yellow"});
                }


                 if( $("#beneficiarioHombres").val() == ""){
                    msgError+= "- Debe poner cantidad de beneficiarios Hombres, \n";
                    $("#beneficiarioHombres").css({"background-color": "yellow"});
                }*/

 

                 if( $("#numeroParticipantes").val() == ""  || $("#numeroParticipantes").val() == 0 ){
                    msgError+= "- Debe poner alguna cantidad a Participantes, \n";
                    $("#numeroParticipantes").css({"background-color": "yellow"});
                }




                 if($("#archivoCargado").val()==0){
                            msgError+= "- Debe anexar adjunto para Lista de Participantes.  \n ";
                           // $("#ejercicio_fiscal").css({"background-color": "yellow"});
                } 
                


                 if ($("#figuraCapacitada option:selected").val() == 12) {
                        if( $('input[name=seconParticipo]:checked').val() == undefined){
                                msgError+= "- Debe contestar: La Secon participó en la Vigilancia de Integración?, \n";
                                $('input[name=seconParticipo]').css({"background-color": "yellow"});
                               
                        }
                 }

                   if( $('input[name=construyoComiteCS]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar Constitución del Comité de Contraloria Social,\n";
                    $('input[name=construyoComiteCS]').css({"background-color": "yellow"});
                }



                 if( $("#servidorCapacito").val() == ""){
                    msgError+= "- Debe poner el Servidor Público que capacitó, \n";
                    $("#servidorCapacito").css({"background-color": "yellow"});
                }




                 if(v_esquema==1){
                         $('input[name=asamblea_beneficiarios]').css({"background-color": "white"});  


                        if( $('input[name=asamblea_beneficiarios]:checked').val() == undefined){                       
                            msgError+= "- Debe contestar anexo pregunta 1, \n";
                            $("#asamblea_beneficiarios").css({"background-color": "yellow"});
                        }



 
                        if( $('input[name=equidad_genero]:checked').val() == undefined){                       
                            msgError+= "- Debe contestar anexo pregunta 2, \n";
                            $("#equidad_genero").css({"background-color": "yellow"});
                        }

                        
                        if( $('input[name=ficha_informativa]:checked').val() == undefined){                       
                            msgError+= "- Debe contestar anexo pregunta 3, \n";
                            $("#ficha_informativa").css({"background-color": "yellow"});
                        }

                         if( $('input[name=reglas_operacion]:checked').val() == undefined){                       
                            msgError+= "- Debe contestar anexo pregunta 4, \n";
                            $("#reglas_operacion").css({"background-color": "yellow"});
                        }



                         if( $('input[name=formatos_cs]:checked').val() == undefined){                       
                            msgError+= "- Debe contestar anexo pregunta 5, \n";
                            $("#formatos_cs").css({"background-color": "yellow"});
                        }



                         if( $('input[name=disposicion_quejas]:checked').val() == undefined){                       
                            msgError+= "- Debe contestar anexo pregunta 6, \n";
                            $("#disposicion_quejas").css({"background-color": "yellow"});
                        }

                        
 

                        
                        
  
                    }






                


                
               


        } 


         $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });



            function imprimir(){
       
                window.open('', 'TheWindow');
                document.getElementById('form_impresion').submit();
            }



            function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");


                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   

        function populate_comite(fromPrograma) {
             
                var cantElement = 0;
                $('#listObras').empty();
                $('#listObras').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_comite/ajax_list_comite') ?>/" + fromPrograma,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#listObras').empty();
                        $('#listObras').append("<option value=''>Selecciona Comité..</option>");



                        $.each(data, function (i, name) {
                             //console.log(name);

                            var selected = "";
                            var accion_conexion = "<?=$editData['accion_conexion']?>";
                            if(name.id === accion_conexion){
                                selected = "selected";
                            } 
                            
                            $('#listObras').append('<option value="' + name.id + '"'+selected+'>' + name.nombre_comite + '</option>');
                            cantElement++;
                        });



                         
                    }
                });
            }  




 






         function resetearForma(){
 
           
           location.href="<?php echo site_url();?>/registro_capacitacion";
            /*$(".form-control").each(function() {
               $(this).val("");  */
                
   
          
        }

        

        function cleanElement(){

            swal({
              title: "Estas seguro de Crear nuevo Registro?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: false,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Listo!, Empezamos de nuevo la captura", {
                  icon: "success",
                });
                resetearForma();

              } else {
                swal("Lo dejamos como estaba..");
              }
            });
        }



           function changeProgram(checked){


           
                if(checked!="undefined"){
               
                swal({
                  title: "Estás seguro de querer cambiar Programa?",
                  text: "",
                  icon: "warning",
                  buttons: true,
                  dangerMode: false, 
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Listo!, Se cambio programa exitosamente", {
                      icon: "success",
                    });
                    //location.reload(true);

            procesarCambio();


                  } else {
                    swal("Lo dejamos como estaba..");
                  }
                });
            }



               
        }


        function procesarCambio(){


             var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }



               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            recurso = obj.recurso;
                            esquema = obj.esquema;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").fadeIn("slow");

 

                   if(recurso == 1){
                         $("#patcs_menu").hide();
                        $("#petcs_menu").show("slow");
                             $("#documentos_menu").show("slow");

                         $("#seguimiento_menu").hide("slow");
                          //   $("#btnImprimir").hide();

                         $("#reuniones_menu").show("slow");

                           $("#informes_menu").show("slow");
                             $("#formatos_menu").hide("slow");

                    }

                    
                    v_esquema = 0;

                    if(esquema==1){
                        v_esquema = 1;
                    }
                     

                    if(recurso == 2 || v_esquema == 1){
                         $("#petcs_menu").hide();
                         $("#patcs_menu").show("slow");
                             $("#documentos_menu").show("slow");
                             
                         $("#seguimiento_menu").show("slow");
                          // $("#btnImprimir").show();
                                     $("#reuniones_menu").hide("slow");

                           $("#informes_menu").hide("slow");
                             $("#formatos_menu").show("slow");
                    }


                       loadCatalogFunctions();
    
                    }
                });

 
           populate_comite(id_programSelected);
        }





         function  cargarFile(elem,alt,seccion,objeto){
 


     
              var file_data = $('#file'+alt).prop('files')[0];
                        var form_data = new FormData();

                        form_data.set("seccion", "");
                        form_data.set("depto", "");
                        form_data.set("fk_depto", "");
                        form_data.set("programa", "");
                        form_data.set("objeto", "");
                        form_data.append('file', file_data);
                        form_data.append("seccion", seccion);
                        form_data.append("depto", '<?=$departamento?>');
                        form_data.append("fk_depto", '<?=$fk_departamento?>');
                        form_data.append("programa", '<?=$fk_programa?>');
                        form_data.append("objeto", objeto);
         


                $.ajax({
                                 url:'<?php echo base_url();?>index.php/upload/do_upload',
                                 type:"post",
                                 //data:new FormData(elem.form),
                                 data:form_data,
                                 processData:false,
                                 contentType:false,

                                 cache:false,
                                 async:false,
                                  success: function(data){
                                    
                                    
                                    console.log(data);
                                      $("#band"+alt).html("Carga Exitosa...");
                                      $("#url"+alt).html("<a target='_blank' href='"+data+"'>Previsualizar</a>");

                                      if(data!=""){
                                        $("#archivoCargado").val(1);
                                      }

                               }
                             }); 

          }






    </script>

</body>

</html>