
                
    <form id="form_listadoComites" action="registro_comite" method="post">

    <table id="example" class="display" class="col-lg-12">
        <thead>
            <tr >
                  <th>id</th>
                  <th>Programa</th>
                              <th>Nombre Comite</th>

                
                <th>Fecha Registro</th>
                <th >Apoyo,Obra o Servicio</th>
                  <th >Instancia Ejecutora</th>
               
                           
                <th>Municipio</th>                
                <th>Localidad</th>                
                <th>Estatus</th>
                <th>Total Integrantes</th>
                <th>Total a Vigilar</th>
                <th>Monto Vigilado</th>
                
                <th>Tipo Ejecutora</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>

            <?php

            $counter = 1;

            foreach ($listComites as $row){

              $localidad = $row["localidad"];
              if($row["localidad_comite"]!=""){ 
                $localidad = $row["localidad_comite"];
              }

            

                    echo "<tr id='tr_".$row["id"]."'><td>". $counter . "</td><td>". $row["programa"] . "</td><td>". $row["nombre_comite"] . "</td><td>". $row["fecha_constitucion"] . "</td><td><a href='#'   onClick='viewProy(".trim($row["id_obra"]).",".trim($row["fk_program"]).")'>". $row["nombre_proyecto"] . "</a></td><td>". $row["instancia_ejecutora"] . "</td><td>". $row["municipio"] . "</td><td>". $localidad . "</td><td>". $row["estatus_proyecto"] . "</td>  <td align='center'>". ($row["qty_hombres"]+$row["qty_mujeres"]) . "</td> <td>". number_format($row["recurso_asignado_vigilar"],2) . "</td> <td>". number_format($row["recurso_ejecutado_vigilado"],2) . "</td>  <td>". $row["tipo_ejecutora"] . "</td>";
                ?>

                 <td>

              <button type="button" class="btn btn-xs btn-warning waves-effect w-md waves-light m-b-15"  onClick='verComite(<?=$row['id']?>)' data-toggle="modal" title="ver" data-target=". "><i class="fa fa-file"></i></button>
                      
              <button type="button" class="btn btn-xs btn-primary waves-effect w-md waves-light m-b-15"  onClick='editComite(<?=$row['id']?>)' data-toggle="modal" title="editar" data-target=". "><i class="fa fa-pencil"></i></button>  
              
               <button type="button" class="btn btn-xs btn-danger waves-effect w-md waves-light m-b-15"  onClick='delProy(<?=$row['id']?>)' data-toggle="modal" title="borrar" data-target=". "><i class="fa fa-trash"></i></button></td></tr>

              
              
 


          

          <?php

            $counter++;
            
            }


            ?>
       
           
         
        </tbody>
       
    </table>


  
    <input type="hidden" name="id_comite" id="id_comite">
    <input type="hidden" name="method" id="method">
            <input type="hidden" name="id_proyecto" id="id_proyecto">
                          
                         <input type="hidden" name="id_ffinanciamiento" id="id_ffinanciamiento">
                         <input type="hidden" name="seleccPgma" id="seleccPgma">


    </form>


  

    <script type="text/javascript">


    $(document).ready(function () {     
     
 


        $('#example tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="'+title+'" />' );
        } );
 


        var tableProy = $('#example').DataTable( {
           language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
            dom: 'Bfrtip',
            scrollX: 'true',
            pagingType: 'full_numbers',
            bAutoWidth: false,


            "columnDefs": [
    { "width": "80px", "targets": 11 }
  ],
        
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
            bInfo: false,
            buttons: [
                
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                },
                
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF'
                }
            ],

        } );

 

               


           
 
            
            

          });    



       function editComite(id_comite)
        {

            $("#id_comite").val(id_comite);
            $("#method").val("edit");
           
            $("#form_listadoComites").submit();



        }



        function verComite(id_comite)
        {   

     

            $("#id_comite").val(id_comite);
            $("#method").val("view");


           
            $('#form_listadoComites').attr('action', 'registro_comite');
           
            $("#form_listadoComites").submit();



        }


        function viewProy(id_proyecto,id_programa)
        {

            $('#form_listadoComites').attr('action', '<?=site_url()?>/registro_proyecto');

            $("#id_proyecto").val(id_proyecto);
             
            $("#seleccPgma").val(id_programa);

            $("#method").val("view");
             
            $("#form_listadoComites").submit();




        }



    

     function delProy(id_comite){

            swal({
              title: "Estás seguro de querer borrar el Comite?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: false,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Listo!, Se ha borrado exitosamente", {
                  icon: "success",
                });
                eliminarComiteAjax(id_comite);

              } else {
                swal("Lo dejamos como estaba..");
              }
            });
        }


    function eliminarComiteAjax(id_comite){

         $("#id_comite").val(id_comite);

            $.ajax({
                    url: "<?php echo site_url('registro_comite/eliminarComite') ?>",
                    type: "POST",
                    data: $('#form_listadoComites').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                       if(message=="ok"){
                         $("#tr_"+id_comite).hide('slow');
                       }
                        
 
                    }
                });
    }

        


    </script>