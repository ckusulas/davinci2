<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport"  content="initial-scale=1,width=1.0">

    <meta name="description" content="">
    <meta name="author" content="">


    <title>SICSEQ V2.0</title>

       <?php $this->view('template/header.php'); ?>   

</head>

<body>

          <div id="modalNew" class="modal fade modal-nuevo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <div id="subTitle"><h4 class="modal-title">Alta Nuevo Programa</h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form   action="registro_proyecto" id="form_programa" class="form-horizontal"   >


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Nombre del Programa</label>
                                    <div class="col-md-6">
                                        <input  name="nombrePrograma" id="nombrePrograma" type="text" class="form-control"  >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Recurso</label>
                                     <div class="col-sm-6">
                                        <select class="form-control" name="recurso" id="recurso">
                                             <option>Seleccione.. </option>                                                             
                                                <?php
                                                foreach ($listRecourses as $row) {
                                                    $selected = "";

                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['nombre']) . "</option>\n";
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <input type="hidden" name="complexSelect" id="complexSelect">
                                <input type="hidden" name="areaSelect" id="areaSelect">
                                <input type="hidden" name="locationSelect" id="locationSelect">
                                <input type="hidden" name="capacitorSelect" id="capacitorSelect">
                                <input type="hidden" name="typeSelect" id="typeSelect">
                                <input type="hidden" name="categorySelect" id="categorySelect">
                                <input type="hidden" name="modalitySelect" id="modalitySelect">
                   

                            <div align="right">
                                <button type="button"  onClick="guardarPrograma()" class="btn btn-success waves-effect waves-light">Guardar</button>&nbsp;&nbsp;&nbsp;
                            </div>
                        </form>

                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


           

    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
 
                <!-- begin PAGE TITLE ROW -->
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_indicadores"  action="<?php echo site_url();?>/registro_proyecto" method="post">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                <tr>
                                    <td>
                                       <h1>
                                           
                                        </h1>
                                     </td>
                                     <td align="right">
                                         &nbsp;<input type="hidden" name="submenuproxim"> 
                                     </td>
                                </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Reporte General</i>
                                </li>
                                <li class="active">Principal</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>




                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                         <input type="hidden" name="id_proyecto" id="id_proyecto">
                         <input type="hidden" name="id_comite" id="id_comite">
                         <input type="hidden" name="id_ffinanciamiento" id="id_ffinanciamiento">
                         <input type="hidden" name="seleccPgma" id="seleccPgma">
                         <input type="hidden" name="method" id="method">
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                </div>

                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
     


                <div class="row">           
                
                    <!-- begin LEFT COLUMN -->
                    <div class="col-lg-12">

                        <div class="row">



                      

                            <!-- /.col-lg-12 (nested) -->
                            <!-- Dashboard de Obra/Apoyo y Servicios-->

                

                            <!-- /.col-lg-12 (nested) -->
                            <!-- Dashboard de Obra/Apoyo y Servicios-->





                 






                            <!-- Listado General -->

                               <div class="col-lg-12">

                                <div  class="portlet portlet-red">
                                    <div class="portlet-heading">


                                        <div class="portlet-title" id="subtitle-table">
                                            <h4>Reporte General - </h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample2"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                              
                                    <div id="basicFormExample2" class="panel-collapse collapse in">
                                         <div class="portlet-body">
                                             <div id="tablaGeneral"></div>  
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Listado General -->

                            <!-- Inline Form Example -->
                         

                    </div>
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- /.row -->
                <!-- end MAIN PAGE ROW -->
                
     

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
  
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
 
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Salir Notification Box -->
    <div id="logout">
        <div class="logout-message">
            <?php 
                $photo = $this->session->userdata('photo');
            ?>

            <img class="img-circle img-logout" width="150px" height="150px" src="<?php echo asset_url();?>img/<?=$photo?>" alt="">
            <h3>
                <i class="fa fa-sign-out text-green"></i> Listo para Salir?
            </h3>
            <p>Estas seguro, de salir de tu session actual?.</p>
            <ul class="list-inline">
                <li>
                    <a href="<?php echo site_url()?>" class="btn btn-green">
                        <strong>Salir</strong>
                    </a>
                </li>
                <li>
                    <button class="logout_close btn btn-green">Cancelar</button>
                </li>
            </ul>
        </div>
    </div>
    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
 
    
    
    
    




    <script type="text/javascript">



        $(document).ready(function () {

         

         $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */
 
          
           
     



            changeProgram();

 


    $('#example tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );


    var tableProy = $('#example').DataTable( {
        
        dom: 'Bfrtip',
        scrollX: 'true',
        pagingType: 'full_numbers',
        dom: 'T<"clear">lfrtip',
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        bInfo: false,
        buttons: [
            {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy'
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel'
            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV'
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF'
            }
        ],

    } );


     tableProy.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );


            $("#catalogResources").change(function () {

                var resourceSelected = $("#catalogResources option:selected").val();     

                populate_programs(resourceSelected);

            });

            function populate_programs(fromResource) {
                var cantElement = 0;
                $('#catalogPrograms').empty();
                $('#catalogPrograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_programs') ?>/" + fromResource,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogPrograms').empty();
                        $('#catalogPrograms').append("<option>Selecciona Programa..</option>");


                        $.each(data, function (i, name) {
                            $('#catalogPrograms').append('<option value="' + data[i].id + '">' + data[i].programa + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   


             $("#catalogPrograms").change(function () {

                var programSelected = $("#catalogPrograms option:selected").val();   

                populate_subprograms(programSelected);

            });


           function populate_subprograms(fromProgram) {
                var cantElement = 0;
                $('#catalogSubprograms').empty();
                $('#catalogSubprograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_subprograms') ?>/" + fromProgram,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogSubprograms').empty();
                        $('#catalogSubprograms').append("<option>Selecciona subprograma..</option>");
        

                        $.each(data, function (i, name) {
                            $('#catalogSubprograms').removeAttr("disabled")
                            $('#catalogSubprograms').append('<option value="' + data[i].id + '">' + data[i].subprograma + '</option>');
                            cantElement++;
                        });                         

                        if(cantElement==0){
                            $('#catalogSubprograms').attr('disabled', 'disabled');
                        }
                    }
                });
            }



            $('#vigila_federal').numeric(",");
            $('#vigila_estatal').numeric(",");
            $('#vigila_municipal').numeric(",");
            $('#vigila_otros').numeric(",");
            $('#total_acciones').numeric();
            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();
            
            

          });    


    
      
        


       

        function sumaVigilado(){

            var total = 0;

                $(".t_vigilado").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

            $("#total_vigilado").val(total);

        }



        function sumaPeople(){

            var total = 0;

                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

              

            $("#total_people").val(total);

        }

     


        function guardarPrograma(){


            $("#nombrePrograma").css({"background-color": "white"});
             $("#recurso").css({"background-color": "white"});

            var msgError = "";

            if( $("#nombrePrograma").val() == ""){
                msgError = "Debe existir nombre programa al menos, ";
                $("#nombrePrograma").css({"background-color": "yellow"});
            }

            
            if($("#recurso option:selected").val() == "Seleccione.."){
                 msgError+=" Debe existir un Recurso al menos, ";
                $("#recurso").css({"background-color": "yellow"});

            }



            

            if(msgError==""){

                     
                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/guardarNuevoPrograma') ?>",
                    type: "POST",
                    data: $('#form_programa').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = true;
                            titulo_programa = "";
                        });

                    if(status){

                         $('.modal-nuevo').modal("hide");

                                     swal("Bien hecho!", "El programa ha sido guardado. Ahora puedes usarlo.  Seleccionando en la Lista de Programas", "success");

                    }

    
                    }
                });





   
            }else{
                swal("Error!", msgError, "error");
            }

        }

        



        function jsUcfirst(string) 
        {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }


        function guardarFF(){

            swal("Bien hecho!", "La Fuente de Financiamiento ha sido guardado.", "success");

        }


        function detallesFF_Beneficiarios(){
            $('.modal-beneficiarios').modal("show");
        }


        function clickBeneficiarios(){

             var id_programSelected;



            id_programSelected = $("#programaSeleccionado option:selected").val();               
 

                   if(id_programSelected != "agregar"){
                        
                      $("#titleProgram").hide();
                      var idSel = $("#programaSeleccionado").val();

                       

                        $("#tablaGeneral").html('').hide('slow');

                        $("#subtitle-table").html("<div class='portlet-title' id='subtitle-table'><h4>Detalle de Beneficiarios <a href='#' onClick='clickPrincipal()'>[Regresar]</a></h4></div>");
                          



                             $.ajax({
                                        url: "<?php echo site_url('indicadores_generales/getTablaGeneralBeneficiarios') ?>/"+idSel,
                                        aSync: false,
                                        dataType: "html",
                                               success: function(data) {
                                                            $("#tablaGeneral").html(data).show("slow");
                                                        },
                                                
                                        });

                }

        }


        function clickPrincipal(){
                var id_programSelected;



            id_programSelected = $("#programaSeleccionado option:selected").val();               
 

                   if(id_programSelected != "agregar"){
                        
                      $("#titleProgram").hide();
                      var idSel = $("#programaSeleccionado").val();

                       

                      $("#tablaGeneral").html('').hide('slow');

                                        $("#subtitle-table").html("<div class='portlet-title' id='subtitle-table'><h4>Listados de Obras / Apoyos / Servicios 2018 - </h4></div>");
    
           

                    $.ajax({
                        url: "<?php echo site_url('indicadores_generales/getTablaGeneral') ?>/"+idSel,
                        aSync: false,
                        dataType: "html",
                               success: function(data) {
                                            $("#tablaGeneral").html(data).show("slow");
                                        },
                                
                          });


                    }

         }

      


        function changeProgram(){


            var id_programSelected;



            id_programSelected = $("#programaSeleccionado option:selected").val();               

                if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }

  

               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();
                $("#subtitulo2").css( "visibility", "hidden" );
                $("#subtitulo3").css( "visibility", "hidden" );

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_indicadores').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            recurso = obj.recurso;
                        });

                    $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");   

                    if(recurso == 1){
                         $("#patcs_menu").hide();
                        $("#petcs_menu").show("slow");
                         $("#seguimiento_menu").hide("slow");
                             $("#documentos_menu").show("slow");
                    }

                     if(recurso == 2){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow");
                         $("#seguimiento_menu").show("slow");
                             $("#documentos_menu").show("slow");
                    }
                  
                    //$("#titleProgram").show("slow");
 

    
                    }
                });


                //Mandar a llamar datos del Programa, a Excepciond de asignar Nuevo Programa.  
                if(id_programSelected != "agregar"){
                        
                      $("#titleProgram").hide();
                      var idSel = $("#programaSeleccionado").val();
                       
 

                



 

                        $.ajax({
                        url: "<?php echo site_url('reporte_general/getTablaGeneral') ?>/"+idSel,
                        aSync: false,
                        dataType: "html",
                               success: function(data) {
                                            $("#tablaGeneral").html(data).show("slow");
                                        },
                                
                        });


                         

                }



 
         
               
        }


        


 



    </script>



</body>

</html>
