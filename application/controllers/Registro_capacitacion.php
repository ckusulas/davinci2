<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_capacitacion extends CI_Controller {

    /**
     * Developed by Constantino Kusulas Vazquez   - 2018.
     */




    public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
        $this->load->model('catalogos_model');
        $this->load->library('session');

    }
 
    public function index()
    {   

        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }




        $metodo = $this->input->post('method');
        $id_capacitacion = $this->input->post('id_capacitacion');

       //echo "id_capacitacion:" . $id_capacitacion;

 
           
        $id_departamento = ""; //$this->input->post('departamento');
        $id_departamento = $this->session->userdata('departamento');


        $programaSelected = $this->session->userdata('ProgramasSelecc');
        
        //echo "programa_seleccionado:" . $programaSelected;

        $onlyView = "";
        if($metodo == "view"){
            $onlyView = 1;
        }


        //VERIFICANDO EXISTENCIA OBRA
        if($programaSelected!="todos"){
            $this->db->where('fk_programa', $programaSelected);
        }
        $this->db->where('id', $id_capacitacion);       
        $q = $this->db->get('cs_registro_capacitacion');

        


        /*
            id
            nombre_capacitacion
            tematica
            figura_capacitada
            entidad_federativa
            fecha_imparticion
            oficio_aprobacion
            esquema_normativo
            municipio
            localidad
            num_participantes
            lista_participantes
            beneficiarios_hombres_capacitados
            beneficiarios_mujeres_capacitados
            integrantes_hombres_capacitados
            integrantes_mujeres_capacitados
            fk_programa
        */



             $editData = array (
                  'id' => '',
                    'id_capacitacion' => '',
                    'metodo' => '',
                    
                    'nombre_capacitacion' => '',
                    'tematica' => '',
                    'figura_capacitada' => '',
                    'accion_conexion' => '',

                    'entidad_federativa' => '',
                    'fecha_imparticion' =>  '',
                    'oficio_aprobacion' => '',
                    
                    'esquema_normativo' => '',
                    'municipio' => '',
                    'localidad' => '',
                    'num_participantes' => '',
                    'lista_participantes' => '',
                    'beneficiarios_hombres_capacitados' => '',
                    'beneficiarios_mujeres_capacitados' => '',
                    'integrantes_mujeres_capacitados' => '',
                    'integrantes_hombres_capacitados' => '',

                    'list_localidades' => '',

                    'figura_capacitada_otro' => '',
                    'tematica_otro' => '',

                    'servidor_publico_capacito' => '',
                     'secon_participo' => '',
                     'onlyView' => '',

                    'asamblea_beneficiarios' => '',
                    'equidad_genero' => '',
                    'ficha_informativa' => '',
                    'reglas_operacion' => '',
                    'formatos_cs' => '',
                    'disposicion_quejas' => '',
                    'observaciones_comite' => '',

                    'construyo_comite_cs' => '',

            );




         if ($q->num_rows() > 0) {

            $metodo = "edit";


            foreach ($q->result() as $row) {

                    $list_localidades = array();

                    if($row->fk_municipio!=""){
                        $list_localidades = $this->ffinanciamiento_model->findLocalidades($row->fk_municipio);
                    }



                   $editData = array (                   
                    'id' => $row->id,
                    'id_capacitacion' => $id_capacitacion,                     
                    'metodo' => $metodo,
                    
                    'nombre_capacitacion' => $row->nombre_capacitacion,
                    'tematica' => $row->fk_tematica,
                    'figura_capacitada' => $row->fk_figura_capacitada,
                    'accion_conexion' => $row->fk_accion_conexion,

                    'entidad_federativa' => $row->entidad_federativa,
                    'fecha_imparticion' =>  $this->transformDateToView($row->fecha_imparticion),
                    'oficio_aprobacion' => $row->oficio_aprobacion,
                    
                    'esquema_normativo' => $row->esquema_normativo,
                    'municipio' => $row->fk_municipio,
                    'localidad' => $row->fk_localidad,
                    'num_participantes' => $row->num_participantes,
                    'lista_participantes' => $row->lista_participantes,
                    'beneficiarios_hombres_capacitados' => number_format($row->beneficiarios_hombres_capacitados),
                    'beneficiarios_mujeres_capacitados' => number_format($row->beneficiarios_mujeres_capacitados),
                    'integrantes_mujeres_capacitados' => number_format($row->integrantes_mujeres_capacitados),
                    'integrantes_hombres_capacitados' => number_format($row->integrantes_hombres_capacitados),

                    'list_localidades' => $list_localidades,

                    'figura_capacitada_otro' =>  $row->fk_figura_capacitada_otro,
                    'tematica_otro' => $row->fk_tematica_otro,

                    'servidor_publico_capacito' => $row->servidor_publico_capacito,
                    'secon_participo' => $row->secon_participo,
                    'onlyView' => $onlyView,

                    'asamblea_beneficiarios' => $row->pgta_asamblea_beneficiarios,
                    'equidad_genero' => $row->pgta_equidad_genero,
                    'ficha_informativa' => $row->pgta_ficha_informativa,
                    'reglas_operacion' =>  $row->pgta_reglas_operacion,
                    'formatos_cs' => $row->pgta_formatos_cs,
                    'disposicion_quejas' => $row->pgta_disposicion_quejas,
                    'observaciones_comite' => $row->pgta_observaciones_comite,

                      'construyo_comite_cs' => $row->construyo_comite_cs,
                      'url_listaparticipantes' => $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "capacitacion", "listaparticipantes"),
 
                    

                );

 

            }



        }







        $listMunicipios = $this->ffinanciamiento_model->getMunicipios();
        $listCapacitacionTematica = $this->catalogos_model->getListCapacitacionTematica(2018);
        $listFigCapacitada = $this->catalogos_model->getListCapacitacionFigCapacitada(2018);
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);

        $listComites = $this->ffinanciamiento_model->getListComites($programaSelected);

       // echo "contenido:" . print_r($listFigCapacitada);


        $data = array(     
            'listMunicipios' => $listMunicipios,
            'listFigCapacitada' => $listFigCapacitada,
            'listCapacitacionTematica' => $listCapacitacionTematica, 
            'listRecourses' => $listRecourses, 
            'editData' => $editData,    
            'listComites' => $listComites,
            'fk_programa' => $programaSelected,
            'departamento' => $this->session->userdata('name'),
            'fk_departamento' => $id_departamento,
        );


        $this->load->view('registro_capacitacion',$data);
    }




    public function changeFormatDateSpEng($data){

        $arrayDate = explode("/", $data);
        $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
        return($newData);
    }

    public function transformDateToView($data){

        if($data == "0000-00-00"){
            return "";
        }



        $arrayDate = explode("-", $data);
        if(strlen($data)>6){
            $newData = $arrayDate[2] . "/" . $arrayDate[1] . "/"  . $arrayDate[0];
            return($newData);
        }
    }






     public function guardarDatosCapacitacion(){

        $fk_program =  $this->session->userdata('ProgramasSelecc');
        $departamento =  $this->session->userdata('departamento');
         
     
        $nombreCapacitacion = $this->input->post('nombreCapacitacion');
        $tematica = $this->input->post('tematica')?:NULL;
        

        $figuraCapacitada = $this->input->post('figuraCapacitada')?:NULL;
        $accion_conexion = $this->input->post('accionConexion')?:NULL;

 

        if(count($accion_conexion>0)){
          $accion_conexion =  $accion_conexion[0];

        }

        //echo "resultado:" . $accion_conexion[0];

        $municipio = $this->input->post('municipio')?:NULL;
        /*if($municipio == ""){
            $municipio = NULL;
        }*/

        $localidad = $this->input->post('localidad')?:NULL;
    /**    if($localidad == ""){
            $localidad = NULL;
        }*/

        $figuraCapacitadaOtro = $this->input->post('figuraCapacitadaOtro')?:NULL;
       /* if($figuraCapacitadaOtro = ""){
            $figuraCapacitadaOtro = NULL;
        }*/

        $tematicaOtro = $this->input->post('tematicaOtro')?:NULL;
       /* if($tematicaOtro = ""){
            $tematicaOtro = NULL;
        }*/

        
        $entidadFederativa = "Querétaro";
        $fechaImparticion = $this->input->post('fechaImparticion');


        if($fechaImparticion!=""){
            $fechaImparticion = $this->changeFormatDateSpEng($fechaImparticion);
        }else{
            $fechaImparticion = NULL;
        }
 
        $id_capacitacion =  $this->input->post('id_capacitacion');  
        $metodo =  $this->input->post('metodo');  

/*
        echo "nombreCapacitacion: " . $nombreCapacitacion . "<br>";
        echo "tematica: " . $tematica . "<br>";
        echo "figuraCapacitada: " . $figuraCapacitada . "<br>";
        echo "entidadFederativa: " . $entidadFederativa . "<br>";
        echo "fechaImparticion: " . $fechaImparticion . "<br>";*/
 
        $this->db->set('fk_municipio', $municipio);
        $this->db->set('fk_localidad', $localidad);
        $this->db->set('nombre_capacitacion', $nombreCapacitacion);
        $this->db->set('fk_tematica', $tematica);        
        $this->db->set('fk_figura_capacitada', $figuraCapacitada);
        $this->db->set('fk_accion_conexion', $accion_conexion);
        $this->db->set('entidad_federativa', $entidadFederativa);
        $this->db->set('fecha_imparticion', $fechaImparticion);
        $this->db->set('fk_programa', $fk_program);
        $this->db->set('fk_figura_capacitada_otro', $figuraCapacitadaOtro);
        $this->db->set('fk_tematica_otro', $tematicaOtro);
        $this->db->set('fk_departamento', $departamento);



        
    
//echo "id_capacitacion:" . $id_capacitacion;

      //INSERT FF
        if($id_capacitacion=="" && $metodo!="edit"){
            //echo "insertando";
            $this->db->insert('cs_registro_capacitacion');
            $lastId = $this->db->insert_id();
        }
        else{
            //echo "actualizando";
            $metodo = "edit";
             $this->db->where('id', $id_capacitacion);      
             $this->db->update('cs_registro_capacitacion');
            $lastId = $id_capacitacion;
        }
 
   // echo $this->db->last_query();


       
            $status = true;
            $msg = $lastId;
             
      



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
        
 
    }



    function guardarIdentificacion(){
        
        
        $oficioAprobacion = $this->input->post('oficioAprobacion');
        $esquemaCS = $this->input->post('esquemaCS');

  /*      echo "oficioAaprobacion:" . $oficioAprobacion . "<br>";
        echo "esquemaCS:" . $esquemaCS . "<br>";
*/
        
        $lastId_capacitacion =  $this->input->post('id_capacitacion_identificacion');



        $this->db->set('oficio_aprobacion', $oficioAprobacion);     
        $this->db->set('esquema_normativo', $esquemaCS);     
        
        $this->db->where('id', $lastId_capacitacion);       
        $this->db->update('cs_registro_capacitacion');


 


    }


  


    function guardarBeneficiariosCapacitados(){

         $numeroParticipantes = $this->input->post('numeroParticipantes');
         $listaParticipantes  = $this->input->post('listaParticipantes');
         $beneficiarioHombres = $this->input->post('beneficiarioHombres');
         $beneficiarioMujeres = $this->input->post('beneficiarioMujeres');



         $lastId_capacitacion = $this->input->post('id_capacitacion_beneficiarios');


        $beneficiarioMujeres = str_replace(',','',$beneficiarioMujeres);  
        $beneficiarioHombres = str_replace(',','',$beneficiarioHombres);  
        $numeroParticipantes = str_replace(',','',$numeroParticipantes);  

     /*   echo "lastId:" . $lastId_capacitacion . "<br>";
       echo "numeroParticipantes:" . $numeroParticipantes . "<br>";
        echo "listaParticipantes:" . $listaParticipantes . "<br>";
        echo "beneficiarioHombres:" . $beneficiarioHombres . "<br>";
        echo "beneficiarioMujeres:" . $beneficiarioMujeres . "<br>";*/


        $this->db->set('num_participantes', $numeroParticipantes);     
        $this->db->set('lista_participantes', $listaParticipantes);     
        $this->db->set('beneficiarios_hombres_capacitados', $beneficiarioHombres);     
        $this->db->set('beneficiarios_mujeres_capacitados', $beneficiarioMujeres);     
        
        $this->db->where('id', $lastId_capacitacion);       
        $this->db->update('cs_registro_capacitacion');

        

        


    }

    


    function guardarintegComiteCapacitados(){

        $secon_participo  = $this->input->post('seconParticipo');
        $servidor_capacito  = $this->input->post('servidorCapacito'); 
        $construyoComiteCS  = $this->input->post('construyoComiteCS'); 

        $id_capacitacion  = $this->input->post('id_capacitacion_integrantes'); 
  

        $this->db->set('secon_participo', $secon_participo);     
        $this->db->set('servidor_publico_capacito', $servidor_capacito);     
        $this->db->set('construyo_comite_cs', $construyoComiteCS);     
     
        $this->db->where('id', $id_capacitacion);       
        $this->db->update('cs_registro_capacitacion');
        
        
      


    }


    function guardaranexoCapacitacion(){

        $asamblea_beneficiarios  = $this->input->post('asamblea_beneficiarios');
        $equidad_genero  = $this->input->post('equidad_genero'); 

        $ficha_informativa  = $this->input->post('ficha_informativa'); 
        $reglas_operacion  = $this->input->post('reglas_operacion'); 
        $formatos_cs  = $this->input->post('formatos_cs'); 
        $disposicion_quejas  = $this->input->post('disposicion_quejas'); 
        $observaciones_comite  = $this->input->post('observaciones_comite'); 
 

        $lastId_capacitacion = $this->input->post('id_capacitacion_anexo');



        $this->db->set('pgta_asamblea_beneficiarios', $asamblea_beneficiarios);     
        $this->db->set('pgta_ficha_informativa', $ficha_informativa);     
        $this->db->set('pgta_equidad_genero', $equidad_genero);     
        $this->db->set('pgta_ficha_informativa', $ficha_informativa);     
        $this->db->set('pgta_reglas_operacion', $reglas_operacion);     
        $this->db->set('pgta_formatos_cs', $formatos_cs);     
        $this->db->set('pgta_disposicion_quejas', $disposicion_quejas);     
        $this->db->set('pgta_observaciones_comite', $observaciones_comite);     
     
        $this->db->where('id', $lastId_capacitacion);       
        $this->db->update('cs_registro_capacitacion');
        
        
      


    }





    public function eliminarCapacitacion(){

        $id_capacitacion = $this->input->post('id_capacitacion');

          $this->db->set('eliminado', 1); 
          $this->db->where('id', $id_capacitacion);      
          $this->db->update('cs_registro_capacitacion');

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "ok";
        }

        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }





}