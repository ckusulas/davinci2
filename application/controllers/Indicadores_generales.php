<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indicadores_generales extends CI_Controller {

  /**
   * Developed by Constantino Kusulas Vazquez   - 2018.
   */


   	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
         $this->load->library('session');
 

       /* $profile = $this->session->userdata('is_logued_in');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }*/
    }




	public function index($puerta=0)
	{
		  if($this->session->userdata('is_logued_in')!==TRUE){
        redirect('inicio');
      }

        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        
        $departamento = $this->session->userdata('departamento');


    

        $fk_programa = $this->session->userdata('ProgramasSelecc');
        if($fk_programa == "agregar")
        {
          $fk_programa = "";
        }

 
        


        $listProyectos = $this->ffinanciamiento_model->getListProyectos($fk_programa, $departamento);
        $totalRecursoAsignado = $this->ffinanciamiento_model->getTotalRecursoAsignadoVigilar($fk_programa);
        $totalBeneficiados = $this->ffinanciamiento_model->getTotalBeneficiados($fk_programa);
        $qtyObras = $this->ffinanciamiento_model->getQtyBeneficio(1,$fk_programa);
        $qtyApoyo = $this->ffinanciamiento_model->getQtyBeneficio(2,$fk_programa);
        $qtyServicio = $this->ffinanciamiento_model->getQtyBeneficio(3,$fk_programa);

        //FF

        $subProgramSelected =  $this->session->userdata('SubProgramasSelecc');

       

        //VERIFICANDO EXISTENCIA PROGRAMA
 

            


    if($fk_programa=="")
        $fk_programa = 0;
 
        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor,
            'listProyectos' => $listProyectos,
            'totalRecursoAsignado' => $totalRecursoAsignado,        
            'totalBeneficiados' => $totalBeneficiados,           
            'qtyObras' => $qtyObras,           
            'qtyApoyos' => $qtyApoyo,           
            'qtyServicios' => $qtyServicio, 
            'fk_programa' => $fk_programa,
            'departamento' => $departamento,
            'fk_departamento' => $departamento,         
              
        );

       
        	
        //Departamentos de Administracion
        $departam_Admon = array(1,3,4);
        if(in_array($this->session->userdata('departamento'),$departam_Admon)) {                  
            $this->load->view('indicadores_generales_gerencia', $data);

        }else{          
            $this->load->view('indicadores_generales', $data);

        }
        
	}



  public function wind($parametro=0){
         

        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
         $totalRecursoAsignado = $this->ffinanciamiento_model->getTotalRecursoAsignadoVigilar();

        // echo "totalRecurso:" . $totalRecursoAsignado;
       
         die();

        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor,
            'totalRecursoAsignado' => $totalRecursoAsignado             
          
        );


        $departam_Admon = array(1,3,4);
        if(in_array($this->session->userdata('departamento'),$departam_Admon)) {
           // echo "admon";
       
            $this->load->view('indicadores_generales_gerencia', $data);
        }else{
           // echo "no es admon";
            $this->load->view('indicadores_generales', $data);
        }
        //}
        /*if($parametro == 3){
            $this->load->view('indicadores_generales_director', $data);
        }
        if($parametro == 2){
            $this->load->view('indicadores_generales_gerencia', $data);
        }
        if($parametro == 4){
            $this->load->view('indicadores_generales_dependencia', $data);
        }*/
    }




       public function ajax_validaExisteFFinanciamiento($fk_programa){

        $fk_departamento = $this->session->userdata('departamento');

        $existeFF = $this->ffinanciamiento_model->existFinanciamientoDepto($fk_programa, $fk_departamento);

        print_r($existeFF);

        
 

        $data = array(
          "status" => true,
          "existe_recursoFF" => $existeFF,
         // "fk_programa_recurso2" => 5
        );


       // $listComites = $this->ffinanciamiento_model->getListComites($fk_programa);
       
    

        echo json_encode($data);




   }


    public function seleccionPrograma(){

     
            $programaSeleccionado = $this->input->post('programaSeleccionado');
            $esquemaNormativo = "";

            if($programaSeleccionado!="todos"){
                $recursoSeleccionado = $this->ffinanciamiento_model->getRecurso($programaSeleccionado);//$this->input->post('recurso_sel');
                $this->db->where('id', $programaSeleccionado);

                    $query = $this->db->get('cs_programas');


                      foreach ($query->result() as $row) {
                          $nombre_programa = $row->programa;
                          $esquemaNormativo = $row->esquema_normativo;
                          $noCombinable = $row->no_combinable;
                          $cuadro_f = $row->cuadrof;
                          $cuadro_e = $row->cuadroe;
                          $cuadro_m = $row->cuadrom;
                          $cuadro_o = $row->cuadroo;
                      }
            }else{
            
                  $nombre_programa = "Todos los Programas";
            
            }


            $fondoEconomico="";

            if($esquemaNormativo!=1){
              $fondoEconomico = $nombre_programa;
            }


 //$fk_programa_recurso = $this->ffinanciamiento_model->getFinanciamientoDepto($programaSeleccionado, $this->session->userdata('departamento'));


            
        

            $data = array(
                'ProgramasSelecc' => $programaSeleccionado,
                'ProgramasSeleccTitulo' => $nombre_programa,
                'RecursoSelecc' => $recursoSeleccionado,
                'esquemaNormativo' => $esquemaNormativo,
                'fondoEconomico' =>  $fondoEconomico, 
                'noCombinable' =>  $noCombinable, 
                'cuadro_f' =>  $cuadro_f, 
                'cuadro_e' =>  $cuadro_e, 
                'cuadro_m' =>  $cuadro_m, 
                'cuadro_o' =>  $cuadro_o, 
                //'registroProgramaRecurso' =>  $fk_programa_recurso, 
        
            );

            $this->session->set_userdata($data);
        
           // sleep(0.5);
 
            $status = true;
            $msg = $nombre_programa;
        


        $datos = array(
            'status' => $status,
            'msg' => $msg,
            'recurso' => $recursoSeleccionado,
            'esquema' => $esquemaNormativo,
            'noCombinable' => $noCombinable,
            'cuadroF' =>  $cuadro_f, 
            'cuadroE' =>  $cuadro_e, 
            'cuadroM' =>  $cuadro_m, 
            'cuadroO' =>  $cuadro_o, 
        );


        echo json_encode(array($datos));
       
    }




  public function programaInscrito($user_id, $departamento){



      $programasInscrito = $this->ffinanciamiento_model->getListProgramasInscrito($user_id, $departamento);
      $programasInscritoEstatal = $this->ffinanciamiento_model->getListProgramasInscrito($user_id, $departamento,2018,2);
      $programasInscritoFederal = $this->ffinanciamiento_model->getListProgramasInscrito($user_id, $departamento,2018,1);


         $data = array(
       
                'programasInscrito' => $programasInscrito,
                'programasInscritoFederal' => $programasInscritoFederal,
                'programasInscritoEstatal' => $programasInscritoEstatal,
                               
                          
          );
   

        $this->session->set_userdata($data);
 
  }


    function guardarNuevoPrograma(){

          $nombrePrograma = $this->input->post('nombrePrograma');
          $recurso = $this->input->post('recurso');
          $esquema_post = $this->input->post('esquema_normativo');

          $departamento = $this->session->userdata('departamento');

       
          //Transporiendo Dato
          $esquema_n = 0;
          if($esquema_post == 2){
            $esquema_n = 1;
          }

 
        $this->db->set('programa', $nombrePrograma);
        $this->db->set('fk_recurso', $recurso);        
        $this->db->set('año', 2018);
        $this->db->set('esquema_normativo', $esquema_n);
        $this->db->set('status', 1);

        $fk_user = $this->session->userdata('id');
         
        $this->db->set('who_updated', $fk_user);
    
       

       
        //INSERT FF
        $this->db->insert('cs_programas');
        $lastId = $this->db->insert_id();



        $this->db->set('periodo' , '2018');
        $this->db->set('fk_programa', $lastId);
        $this->db->set('fk_user', $fk_user);
        $this->db->insert('cs_usuario_programa');


        $this->programaInscrito($fk_user,$departamento);

            $status = true;


        $datos = array(
            'status' => $status,
            'msg' => $lastId,
        );


        echo json_encode(array($datos));


    }


    public function populate_programas($findRecurso) {

          
        $listProgramas =   $this->session->userdata('programasInscrito'); //$this->ffinanciamiento_model->findProgramas($findRecurso);
        echo json_encode($listProgramas);
    }



    public function getDashboard($program = ""){

            
        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        

        if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');
          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }


        $departamento = $this->session->userdata('departamento');

        $listProyectos = $this->ffinanciamiento_model->getListProyectos($fk_programa, $departamento);
        $totalRecursoAsignado = $this->ffinanciamiento_model->getTotalRecursoAsignadoVigilar($fk_programa);
        $totalBeneficiados = $this->ffinanciamiento_model->getTotalBeneficiados($fk_programa);
        $qtyObras = $this->ffinanciamiento_model->getQtyBeneficio(1,$fk_programa);
        $qtyApoyo = $this->ffinanciamiento_model->getQtyBeneficio(2,$fk_programa);
        $qtyServicio = $this->ffinanciamiento_model->getQtyBeneficio(3,$fk_programa);
        $qtyComites = $this->ffinanciamiento_model->getQtyComites($fk_programa);

         $data = array();

        unset($data);
 
        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor,
            'listProyectos' => $listProyectos,
             'totalRecursoAsignado' => $totalRecursoAsignado,        
             'totalBeneficiados' => $totalBeneficiados,           
             'qtyObras' => $qtyObras,           
             'qtyApoyos' => $qtyApoyo,           
             'qtyServicios' => $qtyServicio,    
             'qtyComites' => $qtyComites,    
             'success' => true,       
             'status' => true,       
          
          
        );
 
       
          $this->load->view('template/dashboard', $data);
       
    }


     public function getDashboardFF($program = ""){

          
        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        

        if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');
          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }

        $listProyectos = $this->ffinanciamiento_model->getListProyectos($fk_programa, $departamento);
        $totalRecursoAsignado = $this->ffinanciamiento_model->getTotalRecursoAsignadoVigilar($fk_programa);
        $totalBeneficiados = $this->ffinanciamiento_model->getTotalBeneficiados($fk_programa);
        $qtyObras = $this->ffinanciamiento_model->getQtyBeneficio(1,$fk_programa);
        $qtyApoyo = $this->ffinanciamiento_model->getQtyBeneficio(2,$fk_programa);
        $qtyServicio = $this->ffinanciamiento_model->getQtyBeneficio(3,$fk_programa);
        $qtyComites = $this->ffinanciamiento_model->getQtyComites($fk_programa);

        $this->db->where('fk_programa_proyectos', $fk_programa);
        //$this->db->where('fk_suprograma', $subProgramSelected);       
        $q = $this->db->get('cs_registro_programa_recursos');

          $ffData = array(
                      'presupuesto_pef' => '',
                      'presupuesto_vigilar_cs' => '',
                       
                      'mujeres_beneficiadas' => '',
                      'hombres_beneficiados' => '',
                      'total_beneficiados' => '',


                      'convenio_suscrito' => '',
                      'resumen_total_asignado' => '',

                    );

                    
              if ($q->num_rows() > 0) {

               foreach ($q->result() as $row) {
                    $lastId = $row->id;

 

                      $ffData = array(
                        'presupuesto_pef' =>  number_format($row->presupuesto_pef),
                        'presupuesto_vigilar_cs' =>  number_format($row->presupuesto_vigilar_cf),
                         
                        'mujeres_beneficiadas' =>  number_format($row->mujeres_beneficiadas),
                        'hombres_beneficiados' =>   number_format($row->hombres_beneficiados),
                        'total_beneficiados' =>   number_format($row->total_beneficiados),
 
                        'convenio_suscrito' =>  $row->convenio_suscrito,
                        'resumen_total_asignado' =>  number_format($row->resumen_total_asignado),
 
                      
                      );
                }
              }

   

         $data = array();

        unset($data);
 
        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor,
            'listProyectos' => $listProyectos,
            'totalRecursoAsignado' => $totalRecursoAsignado,        
            'totalBeneficiados' => $totalBeneficiados,           
            'qtyObras' => $qtyObras,           
            'qtyApoyos' => $qtyApoyo,           
            'qtyServicios' => $qtyServicio,    
            'qtyComites' => $qtyComites,    
            'ffData' => $ffData,    
            'success' => true,       
            'status' => true,       
          
        );
 
       
          $this->load->view('template/dashboardFF', $data);
       
    }


    public function getTablaGeneral($program = ""){

     // sleep(0.7);
          
        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        

       
        if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');
          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }

         
        $is_gerencia = false;
        $departam_Admon = array(1,3,4);
        $departamento = $this->session->userdata('departamento');

        if(in_array($departamento, $departam_Admon)){
            $is_gerencia = true;
        } 


        $listProyectos = $this->ffinanciamiento_model->getListProyectos($fk_programa, $departamento);
        $totalRecursoAsignado = $this->ffinanciamiento_model->getTotalRecursoAsignadoVigilar($fk_programa);
        $totalBeneficiados = $this->ffinanciamiento_model->getTotalBeneficiados($fk_programa);
        $qtyObras = $this->ffinanciamiento_model->getQtyBeneficio(1,$fk_programa);
        $qtyApoyo = $this->ffinanciamiento_model->getQtyBeneficio(2,$fk_programa);
        $qtyServicio = $this->ffinanciamiento_model->getQtyBeneficio(3,$fk_programa);
        $qtyComites = $this->ffinanciamiento_model->getQtyComites($fk_programa);

        $data = array();

        unset($data);
 
        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor,
            'listProyectos' => $listProyectos,
            'totalRecursoAsignado' => $totalRecursoAsignado,        
            'totalBeneficiados' => $totalBeneficiados,           
            'qtyObras' => $qtyObras,           
            'qtyApoyos' => $qtyApoyo,           
            'qtyServicios' => $qtyServicio,    
            'qtyComites' => $qtyComites,    
            'is_gerencia' => $is_gerencia,
            'success' => true,       
            'status' => true,                
          
        );
 
       
          $this->load->view('template/tableGeneral_indicadores', $data);
       
    }





      public function getTablaGeneralBeneficiarios($program = ""){

     // sleep(0.7);
          
        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        
        $departamento = $this->session->userdata('departamento');

       
        if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');
          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }


        $listProyectos = $this->ffinanciamiento_model->getListProyectos($fk_programa, $departamento);
        $totalRecursoAsignado = $this->ffinanciamiento_model->getTotalRecursoAsignadoVigilar($fk_programa);
        $totalBeneficiados = $this->ffinanciamiento_model->getTotalBeneficiados($fk_programa);
        $qtyObras = $this->ffinanciamiento_model->getQtyBeneficio(1,$fk_programa);
        $qtyApoyo = $this->ffinanciamiento_model->getQtyBeneficio(2,$fk_programa);
        $qtyServicio = $this->ffinanciamiento_model->getQtyBeneficio(3,$fk_programa);
        $qtyComites = $this->ffinanciamiento_model->getQtyComites($fk_programa);
       // $infoGeneral = $this->ffinanciamiento_model->getTotalBeneficiadosFF($fk_programa);
        $totalBenefHombres = $this->ffinanciamiento_model->getTotalBeneficiadosHombres($fk_programa);
        $totalBenefMujeres = $this->ffinanciamiento_model->getTotalBeneficiadosMujeres($fk_programa);

   

        $data = array();

        unset($data);
 
        $data = array(
            
             'listQuestions' => $listQuestions, 
             'listRecourses' => $listRecourses,
             'listInstanceProm' => $listIntancePromotor,
             'listProyectos' => $listProyectos,
             'totalRecursoAsignado' => $totalRecursoAsignado,        
             'totalBeneficiados' => $totalBeneficiados,                   
             'totalHombres' => $totalBenefHombres,                   
             'totalMujeres' => $totalBenefMujeres,                   
             'qtyObras' => $qtyObras,           
             'qtyApoyos' => $qtyApoyo,           
             'qtyServicios' => $qtyServicio,    
             'qtyComites' => $qtyComites,    
             'success' => true,       
             'status' => true,       
          
          
        );
 
       
          $this->load->view('template/tableGeneral_indicadores_beneficiarios', $data);
       
    }



    


}
