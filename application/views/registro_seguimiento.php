<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>sicseqV2</title>

       <?php $this->view('template/header.php'); ?>   

       
</head>

<body>
                  <!--Modal para agregado de Nuevos Programas -->
  <?php $this->view('template/mod_nuevo_programa'); ?>
    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                           <h1>
                                               
                                            </h1>
                                         </td>
                                        <td align="right"> 
                                              
                                          <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='saveElement()'><i class="fa fa-floppy-o"></i> Editar</button> 
                                           <button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='deleteElement()'><i class="fa fa-floppy-o"></i> Nuevo</button> 

                                            <button type="button" class="btn btn-success waves-effect w-md waves-light m-b-15" id="btnGuardar" data-toggle="modal"  onClick='preGuardado()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Seguimiento</i>
                                </li>
                                <li class="active">Seguimiento a Comité de Contraloria Social</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->


                              <div class="row">
                

                 <!-- Funcion del Comite -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>1. Vinculación</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#funciones"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="funciones" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_cuestionario">

                                                             <div class="form-group">
                                                    <label  class="col-sm-5">*Accion de Conexión:</label>
                                                    <div class="col-sm-6">
                                                        

                                                        <select id="listObras"   id="vinculoComite" name="vinculoComite" class="col-sm-12" style="text-overflow:ellipsis" onChange="choiceVinculo(this.value)">                                                                                                              
                                                            <?php 
                                                           
                                                            if(count($listComites)>0){ 
                                                            echo "<optgroup label='Comités'>";
                                                             echo "<option title='seleccione' value='0' >Seleccione...</option>";
     
                                                                foreach($listComites as $row){
                                                                    $selected = "";


                                                                   if($row['id'] == $editData['fk_comite']){
                                                                        $selected = "selected";

                                                                    }

                                                                    $title = $row["nombre_comite"];
                                                                    $subtitle = substr($title, 0,112);

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."...</option>";
                                                                }

                                                                echo "</optgroup>";
                                                            }
                                                            ?>
 
                                                        </select>

                                                    </div>
                                                </div>
                                              

                                             
 


                                               
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                </div>









                  <div class="row">
                

                 <!-- Funcion del Comite -->
                 <div class="col-lg-12" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Cuestionario</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#funciones"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="funciones" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_cuestionario">

                                                     <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">De acuerdo con la Informacion proporcionada, Considera que la Localidad La comunidad o Las personas Beneficiadas Cumplen con Los requisitos para ser Beneficiarios</label>
                                                   
                                                        <label class="radio-inline"><input type="radio" value="SI" name="cumplenRequisitosBenefic">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="cumplenRequisitosBenefic">No</label>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right "> En la eleccion de integrantes de los comites Tiene la misma posibilidad de ser Electos H y M</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="igualdadHyM">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="igualdadHyM">No</label>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right "> ¿El Programa entregó los beneficios correcta y oportunamente, conforme a las reglas de operación u otras normas que lo regulen?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="ProgramaEntreOportunam">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="ProgramaEntreOportunam">No</label>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">Después de realizar la supervisión de la obra, apoyo o servicio ¿Consideran que cumple con lo que el Programa les informó que se les entregaría?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="proyectoCumplePrograma">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="proyectoCumplePrograma">No</label>
                                                </div>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">Detectaron que el Programa se utilizó con fines políticos, electorales, de lucro u otros distintos a su objetivo?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="ProgramaFinPolitico">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="ProgramaFinPolitico">No</label>
                                                </div>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">¿Recibieron quejas y denuncias sobre la aplicación u operación del Programa?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="recibieronQuejas">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="recibieronQuejas">No</label>
                                                </div>


                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">¿Entregaron las quejas y denuncias a la autoridad competente?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="entregaronQuejasAutoridad">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="entregaronQuejasAutoridad">No</label>
                                                </div>


                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">   ¿Recibieron respuesta de las quejas que entregaron a la autoridad competente?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="recibieronQuejaAutoridadCompetente">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="recibieronQuejaAutoridadCompetente">No</label>
                                                </div>





                                             
 


                                               
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del Comite -->

            </div>



                <div class="row">
                
                
                    











                

                    <!-- Datos del Comite -->
                    <div class="col-lg-8">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                             <h4>2. Cuestionario de Seguimiento a Comité</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_capturaSeguimiento" action="#">

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-8">1.- ¿Recibió capacitación en Contraloria Social?</label>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-10 col-md-offset-1">


                                                        <label class="radio-inline">
                                                          <?php 
                                                            $disable = "";
                                                            $selected = "";

                                                            if($editData['recibio_capacitacion']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        
                                                           

                                                            ?>
                                                          <input type="radio" name="optionRecibioCapacitacion" <?=$disable?>  id="optionRecibioCapacitacion" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                          <?php 
                                                            $selected = "";

                                                            if($editData['recibio_capacitacion']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        
                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionRecibioCapacitacion" <?=$disable?>  id="optionRecibioCapacitacion" <?php echo $selected ?> value="NO">No
                                                        </label>

                                                        <?php 
                                                            $selected = "";

                                                            if($editData['recibio_capacitacion']=="NIIDEA"){
                                                                $selected = "checked='checked'";
                                                            }



                                                           
                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionRecibioCapacitacion" <?=$disable?> id="optionRecibioCapacitacion" <?php echo $selected ?> value="NIIDEA">No Recuerda
                                                        </label>






                                                       
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>
 
                                              
                                              

   <div class="form-group">
                                                    <label for="textInput" class="col-sm-8">2.- ¿Conoce la obra, apoyo y/o servicio del cuál es beneficiario? </label>

                                                </div>


                                                <div class="form-group">  
                                                    <div class="col-sm-10 col-md-offset-1">


                                                        <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";

                                                              if($editData['conoce_obra']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }


                                                           

                                                            ?>
                                                          <input type="radio" name="optionConoceObra" <?=$disable?>  id="optionConoceObra" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                          <?php 
                                                            $selected = "";

                                                            if($editData['conoce_obra']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionConoceObra" <?=$disable?>  id="optionConoceObra" <?php echo $selected ?> value="NO">No
                                                        </label>
 






                                                       
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-8 col-md-offset-1" >En caso negativo mencionar el motivo y concluir con la entrevista:</label>
                                                 </div>
                                                   <div class="form-group">
                                                  <div class="col-sm-10 col-md-offset-1">

                                                        <textarea name="textNegativoConoceObra" id="textNegativoConoceObra" cols="90"><?=$editData['conoce_obra_negativo']?></textarea>
                                                  
 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>



                                                <div class="form-group">
                                                      <label for="textInput" class="col-sm-10">3.- ¿Durante el evento de capacitación recibió usted información sobre la obra, apoyo y/o servicio?</label>
                                                </div>



                                                 <div class="form-group">
                                                    <div class="col-sm-10 col-md-offset-1" ">
                                                        
                                                          <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";
                                                            
                                                            if($editData['conoce_info_obra']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }

                                                            

                                                            ?>
                                                          <input type="radio" name="optionInfoObra" <?=$disable?>  id="optionInfoObra" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                           <?php 
                                                            $selected = "";


                                                            if($editData['info_obra_tipo']=="VERBAL"){
                                                                $selected = "checked='checked'";
                                                            }

                                                            

                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionInfoObraSI" <?=$disable?>  id="optionInfoObra" <?php echo $selected ?> value="VERBAL">Verbal
                                                        </label>
                                                           <?php 
                                                            $selected = "";


                                                            if($editData['info_obra_tipo']=="ESCRITA"){
                                                                $selected = "checked='checked'";
                                                            }

                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionInfoObraSI" <?=$disable?>  id="optionInfoObraSI" <?php echo $selected ?> value="ESCRITA">Escrita
                                                        </label>

                                                          <?php 
                                                            $selected = "";

                                                             if($editData['conoce_info_obra']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionInfoObra" <?=$disable?>  id="optionInfoObra" <?php echo $selected ?> value="NO">No
                                                        </label>
 


                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>
                                       

   <div class="form-group">
                                                    <label for="textInput" class="col-sm-8 ">4.- ¿Sabe usted, cuál es el estado que guarda la obra, apoyo y/o servicio?</label>
                                                    
                                                </div>
                                                


                                                <div class="form-group ">
<div class="col-sm-10 col-md-offset-1">

                                                        <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";


                                                            if($editData['conoce_estado_beneficio']=="noINICIADA"){
                                                                $selected = "checked='checked'";
                                                            }

                                                           

                                                            ?>
                                                          <input type="radio" name="optionBeneficio" <?=$disable?>  id="optionBeneficio" <?php echo $selected ?> value="noINICIADA">No iniciada
                                                        </label>

                                                          <?php 
                                                            $selected = "";


                                                            if($editData['conoce_estado_beneficio']=="enProceso"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionBeneficio" <?=$disable?>  id="optionBeneficio" <?php echo $selected ?> value="enProceso">En proceso
                                                        </label>

                                                        <?php 
                                                            $selected = "";

                                                            if($editData['conoce_estado_beneficio']=="SUSPENDIDA"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        ?>

                                                        
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionBeneficio" <?=$disable?> id="optionBeneficio" <?php echo $selected ?> value="SUSPENDIDA">Suspendida
                                                        </label>


                                                        <?php 
                                                            $selected = "";

                                                            if($editData['conoce_estado_beneficio']=="TERMINADA"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        ?>

                                                          <label class="radio-inline">
                                                          <input type="radio" name="optionBeneficio" <?=$disable?> id="optionBeneficio" <?php echo $selected ?> value="TERMINADA">Terminada
                                                        </label>


                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-7 col-md-offset-1" >En caso suspendida o no iniciada mencionar el motivo: </label>
                                                </div>
                                                   

                                                <div class="form-group">
                                                    <div class="col-sm-10 col-md-offset-1">

                                                        <textarea  name="textNegativoEstatusBeneficio" id="textNegativoEstatusBeneficio" cols="90"><?php echo $editData['conoce_estado_beneficio_motivo']; ?></textarea>

                                                       
                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>



                                                  <div class="form-group">
                                                    <label for="textInput" class="col-sm-8">5.- Sabe usted el costo total de la obra, apoyo y/o servicio?</label>
                                                   
                                                </div>

                                                 <div class="form-group ">
                                                    <div class="col-sm-10 col-md-offset-1">

                                                        <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";

                                                             if($editData['conoce_costo_obra']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                           

                                                            ?>
                                                          <input type="radio" name="optionCostoObra" <?=$disable?>  id="optionCostoObra" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                          <?php 
                                                            $selected = "";
                                                             if($editData['conoce_costo_obra']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionCostoObra" <?=$disable?>  id="optionCostoObra" <?php echo $selected ?> value="NO">No
                                                        </label>

                                                             <?php 
                                                            $selected = "";
                                                             if($editData['conoce_costo_obra']=="NORECUERDA"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                           
                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionCostoObra" <?=$disable?> id="optionCostoObra" <?php echo $selected ?> value="NORECUERDA">No recuerda
                                                        </label>

                                                             <?php 
                                                            $selected = "";
                                                             if($editData['conoce_costo_obra']=="NA"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                           
                                                            ?>

                                                          <label class="radio-inline">
                                                          <input type="radio" name="optionCostoObra" <?=$disable?> id="optionCostoObra" <?php echo $selected ?> value="NA">No aplica
                                                        </label>


                                                    </div>
                                                </div>

                                                  <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-8">6.- ¿Sabe qué dependencia ejecuta la obra, apoyo y/o servicio?</label>

                                                </div>
                                                <div class="form-group">
                                                       <div class="col-sm-10 col-md-offset-1">


                                                        <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";
                                                            
                                                            if($editData['conoce_dependencia']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           

                                                            ?>
                                                          <input type="radio" name="optionDependencia" <?=$disable?>  id="optionDependencia" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                          <?php 
                                                            $selected = "";

                                                            if($editData['conoce_dependencia']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionDependencia" <?=$disable?>  id="optionDependencia" <?php echo $selected ?> value="NO">No
                                                        </label>

                                                             <?php 
                                                            $selected = "";

                                                            if($editData['conoce_dependencia']=="NORECUERDA"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionDependencia" <?=$disable?> id="optionDependencia" <?php echo $selected ?> value="NORECUERDA">No Recuerda
                                                        </label>






                                                       
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>






                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-8">7.- ¿Sabe a través de qué programa llegò obra, apoyo y/o servicio a su localidad?</label>
                                                </div>

                                                <div class="form-group">
                                                       <div class="col-sm-10 col-md-offset-1">


                                                        <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";

                                                            if($editData['conoce_programa']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           

                                                            ?>
                                                          <input type="radio" name="optionSabePrograma" <?=$disable?>  id="optionSabePrograma" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                          <?php 
                                                            $selected = "";

                                                            
                                                             if($editData['conoce_programa']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionSabePrograma" <?=$disable?>  id="optionSabePrograma" <?php echo $selected ?> value="NO">No
                                                        </label>

                                                             <?php 
                                                            $selected = "";


                                                             if($editData['conoce_programa']=="NORECUERDA"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionSabePrograma" <?=$disable?> id="optionSabePrograma" <?php echo $selected ?> value="NORECUERDA">No Recuerda
                                                        </label>






                                                       
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>




                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10">8.- Como integrante del comité, ¿Considera usted que los servidores públicos involucrados toman en cuenta a los beneficiarios para mejorar la ejecucion de la obra, apoyo, y/o servicio?</label>
                                                    </div>
                                                    <div class="form-group">
                                                       <div class="col-sm-10 col-md-offset-1">


                                                        <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";

                                                            if($editData['servidor_beneficia_benef']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           

                                                            ?>
                                                          <input type="radio" name="optionServidoresBanefic" <?=$disable?>  id="optionServidoresBanefic" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                          <?php 
                                                            $selected = "";


                                                            if($editData['servidor_beneficia_benef']=="NO"){
                                                                $selected = "checked='checked'";


                                                            }
                                                           

                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionServidoresBanefic" <?=$disable?>  id="optionServidoresBanefic" <?php echo $selected ?> value="NO">No
                                                        </label>

                                                             <?php 
                                                            $selected = "";

                                                             if($editData['servidor_beneficia_benef']=="NOSEPRESENTO"){
                                                                $selected = "checked='checked'";


                                                            }
                                                           
                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionServidoresBanefic" <?=$disable?> id="optionServidoresBanefic" <?php echo $selected ?> value="NOSEPRESENTO">No se ha presentado el servidor público
                                                        </label>






                                                       
                                                    </div>
                                                </div>


                                                 



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-6 col-md-offset-1">Explique los motivos de su respuesta:</label>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-10 col-md-offset-1">

                                                        <textarea id="textServidorPublicoBeneficiariosMejorar" name="textServidorPublicoBeneficiariosMejorar" cols="90"><?php echo $editData['servidor_beneficia_benef_motivo']?></textarea>
                                                  






                                                       
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>






                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10">9.- ¿Se cumplen los periodos de ejecución o atención (fechas y horarios) establecidos de la obra, apoyo y/o servicio?</label>

                                                </div>
                                                 <div class="form-group">
                                                       <div class="col-sm-10 col-md-offset-1">


                                                        <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";

                                                            

                                                               if($editData['conoce_periodo_cumplen']=="SI"){
                                                                $selected = "checked='checked'";

                                                                }

                                                            ?>
                                                          <input type="radio" name="optionPeriodoCumplen" <?=$disable?>  id="optionPeriodoCumplen" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                          <?php 
                                                            $selected = "";


                                                               if($editData['conoce_periodo_cumplen']=="NO"){
                                                                $selected = "checked='checked'";

                                                                }
                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionPeriodoCumplen" <?=$disable?>  id="optionPeriodoCumplen" <?php echo $selected ?> value="NO">No
                                                        </label>

                                                             <?php 
                                                            $selected = "";


                                                               if($editData['conoce_periodo_cumplen']=="NOSABE"){
                                                                $selected = "checked='checked'";

                                                                }
                                                           
                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionPeriodoCumplen" <?=$disable?> id="optionPeriodoCumplen" <?php echo $selected ?> value="NOSABE">No Sabe
                                                        </label>






                                                       
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-6 col-md-offset-1">Explique los motivos de su respuesta:</label>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-10 col-md-offset-1">

                                                        <textarea cols="90" name="textPeriodoCumplenMotivo"><?php echo $editData['conoce_periodo_cumplen_motivo']?></textarea>
                                                  






                                                       
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>





                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10">10.- ¿Ha visto publicada la información (meta, montos, beneficiarios, programa) de la obra, apoyo y/o servicio?</label>

                                                </div>
                                                 <div class="form-group">
                                                       <div class="col-sm-10 col-md-offset-1">


                                                        <label class="radio-inline">
                                                        <?php 
                                                            $selected = "";


                                                            if($editData['servidor_beneficia_benef']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                            

                                                         ?>
                                                          <input type="radio" name="optionInfoMeta" <?=$disable?>  id="optionInfoMeta" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                        <?php 
                                                            $selected = "";


                                                            if($editData['servidor_beneficia_benef']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                            

                                                         ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionInfoMeta" <?=$disable?>  id="optionInfoMeta" <?php echo $selected ?> value="NO">No
                                                        </label>

                                                        


                                                        <?php 
                                                            $selected = "";


                                                            if($editData['servidor_beneficia_benef']=="NORECUERDA"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                            

                                                         ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionInfoMeta" <?=$disable?> id="optionInfoMeta" <?php echo $selected ?> value="NORECUERDA">No recuerda
                                                        </label>
 

                                                       
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>

   <div class="form-group">
                                                    <label for="textInput" class="col-sm-10">11.- De las siguientes actividades de contraloria social, señale ¿Cuáles ha llevado usted a cabo?</label>

                                                </div>


                                                <div class="form-group">
                                                 <table  border=0 class="col-sm-10 col-md-offset-1">
<?php
$listActividadesCS_done = explode(",",$editData['list_actividades_contraloria']);


$checked = "";



 if(in_array(1,$listActividadesCS_done)){
    $checked = "checked";

 }

?>

<tr><td width="70%">- Vigilar la calidad de la obra, apoyo y/o servicio.</td><td width="10%"  align="center"><input type="checkbox" name="actividadesContraloria[]" value="1" <?=$checked?> ></td>   </tr>

<?php
 
$checked = "";

 if(in_array(2,$listActividadesCS_done)){
    $checked = "checked";

 }

?>
<tr><td width="70%">- Participar en reuniones con los demás integrantes del Comité de Contraloria Social.</td><td width="10%"  align="center"><input type="checkbox" name="actividadesContraloria[]" <?=$checked?> value="2"></td>   </tr>


<?php
 
$checked = "";

 if(in_array(3,$listActividadesCS_done)){
    $checked = "checked";

 }
 ?>

<tr><td width="70%">- Solicitar información relacionada con la operación de la obra, apoyo y/o servicio.</td><td width="10%"  align="center"><input type="checkbox" name="actividadesContraloria[]"  <?=$checked?> value="3"></td>   </tr>

<?php
 
$checked = "";

 if(in_array(4,$listActividadesCS_done)){
    $checked = "checked";

 }
 ?>

<tr><td width="70%">- Presentar sugerencias, quejas o denuncias y/o dar seguimiento a éstas.</td><td width="10%"  align="center"><input type="checkbox" name="actividadesContraloria[]" <?=$checked?> value="4"></td>   </tr>

<?php
 
$checked = "";

 if(in_array(5,$listActividadesCS_done)){
    $checked = "checked";

 }
 ?>

<tr><td width="70%">- Establecer comunicación con las autoridades de la obra, apoyo y/o servicio para informar sobre alguna problemática.</td><td width="10%"  align="center"><input type="checkbox" <?=$checked?> value="5" name="actividadesContraloria[]"></td>   </tr>

<?php
 
$checked = "";

 if(in_array(6,$listActividadesCS_done)){
    $checked = "checked";

 }
 ?>

<tr><td width="70%">- Presentar informes por escrito sobre las actividades de vigilancia a las autoridades y beneficiarios.</td><td width="10%"  align="center"><input type="checkbox" value="6" <?=$checked?> name="actividadesContraloria[]"></td>   </tr>

<?php
 
$checked = "";

 if(in_array(7,$listActividadesCS_done)){
    $checked = "checked";

 }
 ?>

<tr><td width="70%">- Hacer propuestas u observaciones a las autoriades involucradas con la obra, apoyo y/o servicio.</td><td width="10%" align="center"><input type="checkbox" value="7" <?=$checked?> name="actividadesContraloria[]"></td>   </tr>

                                                 </table>
                                                </div>
                                                   <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10">12.- En caso de haber solictado información o haber presentado alguna sugerencia queja o denuncia por favor indique lo siguiente:</label>

                                                </div>
                                                 <div class="form-group">
                                                 <table  border01 class=" col-md-offset-1">

<tr><td width="25%">&nbsp;</td><td width="10%" align="center">Verbal</td><td width="10%" align="center">Escrita</td><td width=180 align="center">Servidor Público o Dependencia ante la que se presentó:</td><td width="80" align="center"> Fecha de presentación</td><td width="80" align="center">¿Ya fue atendida?</td></tr>


<tr><td>Solicitud de Información</td><td align="center">
    <?php
 if($editData['solicitud_informacion_verbal']=="on"){
                    $checked = "checked='checked'";
    }
?>

    <input type="checkbox"  name="solicitudInformacionVerbal" <?php echo $checked?> ></td><td align="center">

 <?php
 if($editData['solicitud_informacion_escrita']=="on"){
                    $checked = "checked='checked'";
    }
?>


        <input type="checkbox" name="solicitudInformacionEscrita"  <?php echo $checked?>></td><td width=180><input type="text" name="servidorPublicoNombreSolicitudInfo" value="<?php echo $editData['solicitud_informacion_servidor_pub']?>"></td><td width="80"><input type="text" name="fechaSolicitudInformacion" value="<?php echo $editData['solicitud_informacion_fecha']?>"></td><td width="80" align="center">
<?php

 if($editData['solicitud_informacion_atendida']=="on"){
                    $checked = "checked='checked'";
    }
?>
    <input type="checkbox" name="solicitudInfoFueAtendida" <?php echo $checked?>></td></tr>

<?php
 if($editData['solicitud_informacion_escrita']=="on"){
                    $checked = "checked='checked'";
    }
?>

<tr><td>Sugerencias, quejas o denuncias</td><td align="center"><input type="checkbox" <?php echo $checked?> name="sugerenciaVerbal"></td><td align="center"S>

<?php
 if($editData['sugerencia_informacion_escrita']=="on"){
                    $checked = "checked='checked'";
    }
?>
<input type="checkbox" name="sugerenciaEscrita" <?php echo $checked?>></td><td width=180>

    <input type="text" name="servidorPublicoSugerencia" value="<?php echo $editData['sugerencia_informacion_servidor_pub']?>"></td><td width="80">
        <input type="text" name="fechaSugerencia"  value="<?php echo $editData['sugerencia_informacion_fecha']?>"></td><td width="80" align="center">

            <?php
 if($editData['sugerencia_informacion_atendida']=="on"){
                    $checked = "checked='checked'";
    }
?>

            <input type="checkbox" name="sugerenciaInfoFueAtendida" <?php echo $checked?>></td></tr>




                                                 </table>
                                                </div>

                                                   <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10">13.- Del siguiente cuadro señale la información que recibió sobre la obra, apoyo y/o servicio (aún cuando no la recuerde).</label>

                                                </div>


                                                <div class="form-group">
                                                 <table  border=0 class="col-sm-10 col-md-offset-1">
<?php
$listInformacion_recibio = explode(",",$editData['list_informacion_recibio']);


$checked = "";



 if(in_array(1,$listInformacion_recibio)){
    $checked = "checked";

 }

?>


<tr><td width="70%">- La finalidad del programa.</td><td width="10%" align="center"><input type="checkbox" <?=$checked?> name="informacionRecibio[] value="1"></td>   </tr>

<?php

$checked = "";

 if(in_array(2,$listInformacion_recibio)){
    $checked = "checked";

 }

?>
<tr> <td width="70%">- Los requisitos para ser beneficiario de la obra, apoyo o servicio.</td><td width="10%" align="center"><input type="checkbox" <?=$checked?>   name="informacionRecibio[]"  value="2" ></td>   </tr>

<?php

$checked = "";

 if(in_array(3,$listInformacion_recibio)){
    $checked = "checked";

 }

?>
<tr> <td width="70%">- La lista de las personas que recibirán el mismo beneficio.</td><td width="10%" align="center"><input type="checkbox" <?=$checked?>  name="informacionRecibio[]" value="3"></td>   </tr>
<?php

$checked = "";

 if(in_array(4,$listInformacion_recibio)){
    $checked = "checked";

 }

?>
<tr> <td width="70%">- El origen de inversión del apoyo o servicio (Federal, Estatal, Municipal).</td><td width="10%" align="center"><input type="checkbox" <?=$checked?>  name="informacionRecibio[]" value="4"></td>   </tr>

<?php

$checked = "";

 if(in_array(5,$listInformacion_recibio)){
    $checked = "checked";

 }

?>
<tr> <td width="70%">- La lista de materiales que se le entrega, si los hubiera.</td><td width="10%" align="center"><input type="checkbox" <?=$checked?>  name="informacionRecibio[]" value="5"></td>   </tr>

<?php

$checked = "";

 if(in_array(6,$listInformacion_recibio)){
    $checked = "checked";

 }

?>
<tr> <td width="70%">- El apoyo económico que recibe, si lo hubiera.</td><td width="10%" align="center"><input type="checkbox" <?=$checked?>  name="informacionRecibio[]" value="6"></td>   </tr> 

                                                 </table>
                                                </div>
                                                   <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>


         <div class="form-group">
                                                    <label for="textInput" class="col-sm-10">14.- ¿Del resultado de sus actividades de contraloría social, opina que la obra, apoyo y/o servicio se está realizando de acuerdo a como en su momento le fue informado? </label>
                                                   
                                                    <div class="col-sm-10 col-md-offset-1">


                                                        <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";


                                                            if($editData['obra_acorde_plan']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                           
                                                           

                                                            ?>
                                                          <input type="radio" name="optionObraAcordePlan" <?=$disable?>  id="optionObraAcordePlan" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                          <?php 
                                                            $selected = "";

                                                              if($editData['obra_acorde_plan']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionObraAcordePlan" <?=$disable?>  id="optionObraAcordePlan" <?php echo $selected ?> value="NO">No
                                                        </label>
  
                             
                                                    </div>
                                                </div>

                                              

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-6 col-md-offset-1" >En caso negativo mencionar el motivo:</label>
                                                   
                                                    <div class="col-sm-10 col-md-offset-1">

                                                        <textarea name="optionObraAcordePlanMotivo" id="optionObraAcordePlanMotivo" cols="90"><?php echo $editData['obra_acorde_plan_motivo'] ?></textarea>
                                                   
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>



   <div class="form-group">
                                                    <label for="textInput" class="col-sm-10">15.- ¿Del resultado de sus actividades de contraloría social,   usted como integrante del comité de contraloría social, opina que la obra, apoyo y/o servicio se está realizando a satisfacción de los beneficiarios? </label>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-sm-10 col-md-offset-1">


                                                        <label class="radio-inline">
                                                            <?php 
                                                            $disable = "";
                                                            $selected = "";


                                                            if($editData['satisfacion_beneficiarios']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            
                                                           

                                                            ?>
                                                          <input type="radio" name="optionSatisfBeneficiarios" <?=$disable?>  id="optionSatisfBeneficiarios" <?php echo $selected ?> value="SI">Si
                                                        </label>

                                                          <?php 
                                                            $selected = "";
                                                            if($editData['satisfacion_beneficiarios']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionSatisfBeneficiarios" <?=$disable?>  id="optionSatisfBeneficiarios" <?php echo $selected ?> value="NO">No
                                                        </label>
 

                                                       
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-6 col-md-offset-1" >En caso negativo mencionar cuáles:</label>
                                                </div>

                                                  <div class="form-group">  
                                                    <div class="col-sm-10 col-md-offset-1">

                                                        <textarea name="txtSatisfBeneficiarios" id="txtSatisfBeneficiarios" cols="90"><?php echo $editData['satisfacion_beneficiarios_motivo'] ?></textarea>
                                                   
                                                    </div>
                                                </div>


                                                  <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>


  <div class="form-group">
                                                    <label for="textInput" class="col-sm-10">16.- ¿En este momento, requiere de alguna información para mejorar sus actividades de contraloría social, o es su deseo manifestar o agregar algún comentario? </label>
                                                </div>

                                                 <div class="form-group">
                                                 
                                                   <div class="col-sm-10 col-md-offset-1">

                                                        <textarea cols="90" name="txtComentarioMejoraCS"><?php echo $editData['comentario_mejora_cs'] ?></textarea>
                                                   
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-6 col-md-offset-1" >Comentarios adicionales (por el auditor).</label>
                                                 </div>  

                                                    <div class="form-group">
                                                     <div class="col-sm-10 col-md-offset-1">

                                                        <textarea cols="90" name="txtComentarioMejoraCSAuditor"><?php echo $editData['comentario_mejora_cs_auditor'] ?></textarea>
                                                   
                                                    </div>
                                                </div>
  <div class="form-group">
                                                    <p>&nbsp;</p>
                                                </div>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-8 text-right" >Promocion(es) ciudadana(s) recabadas(s)</label> 
                                                    <input type="text" name="txtPromocionRecabada" id="txtPromocionRecabada" size="4" value="<?php echo $editData['promocion_recabada'] ?>">
                                                 </div>  
                                          


                                                 <input type="hidden" name="vinculoComite_hidden" id="vinculoComite_hidden">
                                                 <input type="hidden" name="id_seguimiento" id="id_seguimiento">
                         
                                                       
    <br> <br> <br> 
                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>               
                <!-- fin Datos del Comite -->

 

                 <!-- Funcion del Comite -->
            
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del Comite -->

                     <!-- Integrante del Comite -->

 
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>

    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
 

      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   
    


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">

         var v_esquema = 0;
         
         $(document).ready(function () {

               $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
               $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */


                    if($("#programaSeleccionado").val() == "todos"){
            //alert("Debe seleccionar un Programa, antes de guardar Registro.");
            swal("Advertencia!", "Debe seleccionar un Programa, antes de guardar Registro.", "warning");
            $("#btnGuardar").fadeOut("slow");
          }
 
                changeProgram();


                $('#upload').on('click', function () {
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'registro_comite/upload_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });
 


            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();
 
            

          });    







         function sumaPeople(){
            
            var total = 0;

                 
                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                   //     alert("value:" + this.value);
                        total += parseFloat(this.value);
                    }
                }); 


                //total = parseFloat($("#total_people_womans").val()) + parseFloat($("#total_people_mans").val());

            $("#total_personas").val(total);




        }

         function changeProgram(){


            var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }


                      if(id_programSelected!="todos"){
                  $("#btnGuardar").fadeIn("slow");
                }


               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            recurso = obj.recurso;
                            esquema = obj.esquema;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").fadeIn("slow");

                    if(recurso == 1){
                         $("#patcs_menu").hide();
                         $("#petcs_menu").show("slow");
                             $("#documentos_menu").show("slow");

                          $("#seguimiento_menu").hide("slow");

                              $("#reuniones_menu").show("slow");

                           $("#informes_menu").show("slow");
                             $("#formatos_menu").hide("slow");
                    }

                    
                    v_esquema = 0;

                    if(esquema==1){
                        v_esquema = 1;
                    }
                     

                    if(recurso == 2 || v_esquema == 1){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow");  
                            $("#documentos_menu").show("slow");
                        
                         $("#seguimiento_menu").show("slow");     

                          $("#reuniones_menu").hide("slow");

                           $("#informes_menu").hide("slow");
                             $("#formatos_menu").show("slow");
                    }

    
                    }
                });

 
         
               
        }



        function preGuardado(){

            var msgError = "";
             $("#optionRecibioCapacitacion").css({"background-color": "white"});
             $("#optionConoceObra").css({"background-color": "white"});
             $("#optionInfoObraSI").css({"background-color": "white"});
             $("#optionInfoObra").css({"background-color": "white"});
             $("#optionBeneficio").css({"background-color": "white"});
             $("#optionCostoObra").css({"background-color": "white"});
             $("#optionDependencia").css({"background-color": "white"});
             $("#optionSabePrograma").css({"background-color": "white"});
             $("#optionServidoresBanefic").css({"background-color": "white"});
             $("#optionPeriodoCumplen").css({"background-color": "white"});
             $("#optionInfoMeta").css({"background-color": "white"});

             $("#optionObraAcordePlan").css({"background-color": "white"});
             $("#optionSatisfBeneficiarios").css({"background-color": "white"});
             $("#txtPromocionRecabada").css({"background-color": "white"});
 
 
                if($("#vinculoComite_hidden").val()==""){
                     msgError+= "- Debe seleccionar al menos un Comité, \n ";
                    
              } 

               if( $('input[name=optionRecibioCapacitacion]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 1, \n";
                    $("#optionRecibioCapacitacion").css({"background-color": "yellow"});
                }

              if( $('input[name=optionConoceObra]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 2, \n";
                    $("#optionConoceObra").css({"background-color": "yellow"});
                }

                if( $('input[name=optionInfoObra]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 3, \n";
                    $("#optionInfoObra").css({"background-color": "yellow"});
                }else{

                    if( $('input[name=optionInfoObra]:checked').val() =="SI" && $('input[name=optionInfoObraSI]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 3, tipo, \n";
                    $("#optionInfoObraSI").css({"background-color": "yellow"});
                    }
                }
 
                 if( $('input[name=optionBeneficio]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 4, \n";
                    $("#optionBeneficio").css({"background-color": "yellow"});
                }

                 if( $('input[name=optionCostoObra]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 5, \n";
                    $("#optionCostoObra").css({"background-color": "yellow"});
                } 


                  if( $('input[name=optionDependencia]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 6,\n ";
                    $("#optionDependencia").css({"background-color": "yellow"});
                } 


                if( $('input[name=optionSabePrograma]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 7, \n";
                    $("#optionSabePrograma").css({"background-color": "yellow"});
                } 

                   if( $('input[name=optionServidoresBanefic]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 8, \n";
                    $("#optionServidoresBanefic").css({"background-color": "yellow"});
                } 

                 
                if( $('input[name=optionPeriodoCumplen]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 9, \n";
                    $("#optionPeriodoCumplen").css({"background-color": "yellow"});
                } 

                if( $('input[name=optionInfoMeta]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 10, \n";
                    $("#optionInfoMeta").css({"background-color": "yellow"});
                } 

                if( $('input[name=optionObraAcordePlan]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 14, \n";
                    $("#optionInfoMeta").css({"background-color": "yellow"});
                } 

                     
                if( $('input[name=optionSatisfBeneficiarios]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un opcion del Cuestionario 15, \n";
                    $("#optionInfoMeta").css({"background-color": "yellow"});
                } 

                if( $('#txtPromocionRecabada').val() == ""){
                    msgError+= "- Debe capturar la Promocion ciudadania recabada, \n ";
                    $("#txtPromocionRecabada").css({"background-color": "yellow"});
                } 
                
 
 
 

                



                
                if(msgError!=""){
                     swal("Error!", msgError, "error");
                     return;
                }else{

                   guardar();

                }
        }





        function guardar(){






            $.ajax({
                    url: "<?php echo site_url('registro_seguimiento/guardarSeguimiento') ?>",
                    type: "POST",
                    data: $('#form_capturaSeguimiento').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                            $("#id_seguimiento").val(message);

                         //   alert(message);


                        });

        
 
                    }
                });

           

               






            swal("Bien hecho!", "El Seguimiento ha sido guardado.", "success");
        }


         $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });



         function choiceVinculo(elem)
        {
           $("#vinculoComite_hidden").val(elem);
           

        }


            function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");


                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   







    </script>

</body>

</html>
