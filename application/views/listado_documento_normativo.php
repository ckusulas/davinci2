<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SICSEQ V2.0</title>

        <?php $this->view('template/header.php'); ?>   
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    </head>

    <body>

        <div id="wrapper">

            <!-- begin TOP NAVIGATION -->
            <nav class="navbar-top" role="navigation">

                <!-- begin BRAND HEADING -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                        <i class="fa fa-bars"></i> Menu
                    </button>
                    <div class="navbar-brand">
                        <a href="index.html">
                            <img src="<?php echo asset_url(); ?>img/sicseq-logo.png" class="img-responsive" alt="">
                        </a>
                    </div>
                </div>
                <!-- end BRAND HEADING -->



                <!--Modal para agregado de Nuevos Programas -->
                <?php $this->view('template/mod_nuevo_programa'); ?>

                <!-- begin TOP NAVIGATION -->
                <?php $this->view('template/nav_top_messages.php'); ?>       
                <!-- /.navbar-top -->
                <!-- end TOP NAVIGATION -->

                <!-- begin SIDE NAVIGATION -->
                <?php $this->view('template/nav_left_menus.php'); ?>       
                <!-- /.navbar-side -->
                <!-- end SIDE NAVIGATION -->

                <!-- begin MAIN PAGE CONTENT -->
                <div id="page-wrapper">

                    <div class="page-content">



                        <!-- begin PAGE TITLE ROW -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-title">
                                    <form id="form_navbar">
                                        <div id="titleProgram">  </div>
                                        <table border=0 width="100%">
                                            <tr>
                                                <td>
                                                    <h1>

                                                    </h1>
                                                </td>
                                                <td align="right"> 

                                              <!--
                                                    <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='saveElement()'><i class="fa fa-floppy-o"></i> Editar</button> 
                                                -->


                                                    <button type="button" class="btn btn-success waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                                </td>
                                            </tr>
                                        </table>
                                        <ol class="breadcrumb">

                                            <li><i class="fa fa-edit"></i>Documento Normativo</i>
                                            </li>
                                            <li class="active">Consultar Documentacion</li>

                                            <!--submenu Programas -->
                                            <?php $this->view('template/nav_submenu_programas.php'); ?>       



                                        </ol>

                                        <input type="hidden" name="programSelected" id="programSelected">


                                        </div>

                                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                                    </form>


                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                            <!-- end PAGE TITLE ROW -->

                            <!-- begin MAIN PAGE ROW -->
                            <div class="row">










                                <!-- Funcion del Comite -->
                                <div class="col-lg-12">

                                    <div class="row">

                                        <!-- Basic Form Example -->
                                        <div class="col-lg-6">

                                            <div class="portlet portlet-red">
                                                <div class="portlet-heading">
                                                    <div class="portlet-title">
                                                        <h4>Documentación Validada</h4>
                                                    </div>
                                                    <div class="portlet-widgets">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#funciones"><i class="fa fa-chevron-down"></i></a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div id="funciones" class="panel-collapse collapse in">
                                                    <div class="portlet-body">
                                                        <form class="form-horizontal" id="form_material_distribuir">






                                                            <div class="form-group">
                                                                <label for="textInput" class="col-sm-6  ">(*) Documento de Esquema de Contraloría Social:</label>
                                                                <div class="col-sm-3" >


                                                                    <input type="file" name="file" id="file1" onChange="cargarFile(this,1,'doc_normativo','esquema_cs')" >
                                                   
                                                                    <div id="band1"></div>
                                                                    <div id="url1"></div>
  



                                                                    <?php
                                                                    if (isset($url_esquema_cs[0])) {
                                                                        // echo print_r($editData['url_convenio']);
                                                                        ?>
                                                                        <a target="_blank" href="<?= trim($url_esquema_cs[0]['url']) ?>">Archivo cargado</a>
                                                                        <?
                                                                        }
                                                                        ?>

                                                                    </div>








                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="textInput" class="col-sm-6  ">(*) Documento de PATCS:</label>
                                                                    <div class="col-sm-3" >

                                                                        <input type="file" name="file" id="file2" onChange="cargarFile(this,2,'doc_normativo','patcs')" >
                                                       
                                                                        <div id="band2"></div>
                                                                        <div id="url2"></div>



                                                                    <?php
                                                                    if (isset($url_patcs[0])) {
                                                                        // echo print_r($editData['url_convenio']);
                                                                        ?>
                                                                        <a target="_blank" href="<?= trim($url_patcs[0]['url']) ?>">Archivo cargado</a>
                                                                        <?
                                                                        }
                                                                    ?>
      


                                                                    
                                                                        </div>




                                                                    </div>



                                                                    <div class="form-group">
                                                                        <label for="textInput" class="col-sm-6  ">(*) Documento de Oficio de Envío:</label>
                                                                        <div class="col-sm-3">


                                                                            <input type="file" name="file3" id="file3" onChange="cargarFile(this, 3, 'doc_normativo', 'oficio_envio')" >
                                                                            
                                                                            <div id="band3"></div>
                                                                            <div id="url3"></div>


                                                                            <?php
                                                                            if (isset($url_oficio_envio[0])) {
                                                                                // echo print_r($editData['url_convenio']);
                                                                                ?>
                                                                                <a target="_blank" href="<?= trim($url_oficio_envio[0]['url']) ?>">Archivo cargado</a>
                                                                                <?
                                                                                }
                                                                                ?>
                                                                            </div>




                                                                        </div>













                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.portlet -->
                                                    </div>
                                                    <!-- /.col-lg-12 (nested) -->
                                                    <!-- End Basic Form Example -->

                                                    <!-- Inline Form Example -->


                                                </div>
                                                <!-- /.col-lg-6 -->
                                                <!-- end LEFT COLUMN -->



                                            </div>
                                            <!-- fin Funciones del Comite -->




                                            <!-- Funcion del Comite -->
                                            <div class="col-lg-6">

                                                <div class="row">

                                                    <!-- Basic Form Example -->
                                                    <div class="col-lg-12">

                                                        <div class="portlet portlet-green">
                                                            <div class="portlet-heading">
                                                                <div class="portlet-title">
                                                                    <h4>(*) Documento de Guía Operativa:</h4>









                                                                </div>
                                                                <div class="portlet-widgets">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#funciones"><i class="fa fa-chevron-down"></i></a>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div id="funciones" class="panel-collapse collapse in">
                                                                <div class="portlet-body">
                                                                    <form class="form-horizontal" id="form_material_distribuir">






                                                                        <div class="form-group">
                                                                            <label for="textInput" class="col-sm-4  ">Ficha:</label>
                                                                            <div class="col-sm-3">
                                                                                <input type="file" name="file4" id="file4" onChange="cargarFile(this, 4, 'doc_normativo', 'ficha')">
                                                                              
                                                                                <div id="band4"></div>
                                                                                <div id="url4"></div>



                                                                                <?php
                                                                                if (isset($url_ficha[0])) {
                                                                                    // echo print_r($editData['url_convenio']);
                                                                                    ?>
                                                                                    <a target="_blank" href="<?= trim($url_ficha[0]['url']) ?>">Archivo cargado</a>
                                                                                    <?
                                                                                    }
                                                                                    ?>
                                                                                </div>




                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label for="textInput" class="col-sm-4  ">Cuaderno:</label>
                                                                                <div class="col-sm-3" >

                                                                                    <input type="file" name="file5" id="file5" onChange="cargarFile(this, 5, 'doc_normativo', 'cuaderno')">
                                                                               
                                                                                    <div id="band5"></div>
                                                                                    <div id="url5"></div>

                                                                                    <?php
                                                                                    if (isset($url_cuaderno[0])) {
                                                                                        // echo print_r($editData['url_convenio']);
                                                                                        ?>
                                                                                        <a target="_blank" href="<?= trim($url_cuaderno[0]['url']) ?>">Archivo cargado</a>
                                                                                        <?
                                                                                        }
                                                                                        ?>
                                                                                    </div>




                                                                                </div>



                                                                                <div class="form-group" style="display:none">
                                                                                    <label for="textInput" class="col-sm-4  ">Lin_PETCS:</label>
                                                                                    <div class="col-sm-3" >

                                                                                        <input type="file" name="file6" id="file6" onChange="cargarFile(this, 6, 'doc_normativo', 'petcs')">
                                                                                   
                                                                                        <div id="band6"></div>
                                                                                        <div id="url6"></div>

                                                                                        <?php
                                                                                        if (isset($url_petcs[0])) {
                                                                                            // echo print_r($editData['url_convenio']);
                                                                                            ?>
                                                                                            <a target="_blank" href="<?= trim($url_petcs[0]['url']) ?>">Archivo cargado</a>
                                                                                            <?
                                                                                            }
                                                                                            ?>
                                                                                        </div>




                                                                                    </div>



                                                                                    <div class="form-group">
                                                                                        <label for="textInput" class="col-sm-4  ">Guía:</label>
                                                                                        <div class="col-sm-3" >

                                                                                            <input type="file" name="file7" id="file7" onChange="cargarFile(this, 7, 'doc_normativo', 'oficio_envio')">
                                                                                           
                                                                                            <div id="band7"></div>
                                                                                            <div id="url7"></div>


                                                                                            <?php
                                                                                            if (isset($url_oficio_envio[0])) {
                                                                                                // echo print_r($editData['url_convenio']);
                                                                                                ?>
                                                                                                <a target="_blank" href="<?= trim($url_oficio_envio[0]['url']) ?>">Archivo cargado</a>
                                                                                                <?
                                                                                                }
                                                                                                ?>
                                                                                            </div>




                                                                                        </div>



                                                                                        <div class="form-group">
                                                                                            <label for="textInput" class="col-sm-4  ">Escrito:</label>
                                                                                            <div class="col-sm-3">

                                                                                                <input type="file" name="file8" onChange="cargarFile(this, 8, 'doc_normativo', 'escrito')"  id="file8">
                                                                                           
                                                                                                <div id="band8"></div>
                                                                                                <div id="url8"></div>
                                                                                                <?php
                                                                                                if (isset($url_escrito[0])) {
                                                                                                    // echo print_r($editData['url_convenio']);
                                                                                                    ?>
                                                                                                    <a target="_blank" href="<?= trim($url_escrito[0]['url']) ?>">Archivo cargado</a>
                                                                                                    <?
                                                                                                    }
                                                                                                    ?>
                                                                                                </div>




                                                                                            </div>



                                                                                            <div class="form-group">
                                                                                                <label for="textInput" class="col-sm-4  ">Informe Anual del Comité CS:</label>
                                                                                                <div class="col-sm-3" >

                                                                                                    <input type="file" name="file9" id="file9" onChange="cargarFile(this, 9, 'doc_normativo', 'informe_anual')">
                                                                                                   
                                                                                                    <div id="band9"></div>
                                                                                                    <div id="url9"></div>

                                                                                                    <?php
                                                                                                    if (isset($url_informe_anual[0])) {
                                                                                                        // echo print_r($editData['url_convenio']);
                                                                                                        ?>
                                                                                                        <a target="_blank" href="<?= trim($url_informe_anual[0]['url']) ?>">Archivo cargado</a>
                                                                                                        <?
                                                                                                        }
                                                                                                        ?>
                                                                                                    </div>




                                                                                                </div>










                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- /.portlet -->
                                                                            </div>
                                                                            <!-- /.col-lg-12 (nested) -->
                                                                            <!-- End Basic Form Example -->

                                                                            <!-- Inline Form Example -->


                                                                        </div>
                                                                        <!-- /.col-lg-6 -->
                                                                        <!-- end LEFT COLUMN -->



                                                                    </div>
                                                                    <!-- fin Funciones del Comite -->














                                                                </div>
                                                                <!-- /.page-content -->

                                                            </div>
                                                            <!-- /#page-wrapper -->
                                                            <!-- end MAIN PAGE CONTENT -->

                                                        </div>
                                                        <!-- /#wrapper -->

                                                        <!-- GLOBAL SCRIPTS -->
                                                        <script src="<?php echo asset_url(); ?>js/plugins/jquery/jquery.numeric.js"></script>
                                                        <script src="<?php echo asset_url(); ?>js/plugins/bootstrap/bootstrap.min.js"></script>

                                                        <script src="<?php echo asset_url(); ?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
                                                        <script src="<?php echo asset_url(); ?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
                                                        <script src="<?php echo asset_url(); ?>js/plugins/popupoverlay/defaults.js"></script>
                                                        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


                                                        <script type="text/javascript" src="<?php echo asset_url(); ?>js/bootstrap-multiselect.js"></script>
                                                        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap-multiselect.css" type="text/css"/>




                                                        <!-- Salir Notification Box -->
                                                        <?php $this->view('template/nav_footer_logout.php'); ?> 




                                                        <!-- /#Salir -->
                                                        <!-- Salir Notification jQuery -->
                                                        <script src="<?php echo asset_url(); ?>js/plugins/popupoverlay/logout.js"></script>
                                                        <!-- HISRC Retina Images -->
                                                        <script src="<?php echo asset_url(); ?>js/plugins/hisrc/hisrc.js"></script>

                                                        <!-- PAGE LEVEL PLUGIN SCRIPTS -->

                                                        <!-- THEME SCRIPTS -->
                                                        <script src="<?php echo asset_url(); ?>js/flex.js"></script>

                                                        <script language="javascript">

                                                                                                        $(document).ready(function () {


                                                                                                            $('body').css('zoom', '80%').fadeIn('slow'); /* Webkit browsers */
                                                                                                            $('body').css('zoom', '0.8').fadeIn('slow'); /* Other non-webkit browsers */



                                                                                                            changeProgram();
                                                                                                            $("#municipio").change(function () {

                                                                                                                var municipioSelected = $("#municipio option:selected").val();
                                                                                                                populate_localidad(municipioSelected);
                                                                                                            });
                                                                                                            function populate_localidad(fromMunicipio) {

                                                                                                                var cantElement = 0;
                                                                                                                $('#localidad').empty();
                                                                                                                $('#localidad').append("<option>Cargando ....</option>");
                                                                                                                $.ajax({
                                                                                                                    type: "GET",
                                                                                                                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                                                                                                                    contentType: "application/json;charset=utf-8",
                                                                                                                    dataType: 'json',
                                                                                                                    success: function (data) {
                                                                                                                        $('#localidad').empty();
                                                                                                                        $('#localidad').append("<option>Selecciona Localidad..</option>");
                                                                                                                        $.each(data, function (i, name) {
                                                                                                                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                                                                                                                            cantElement++;
                                                                                                                        });
                                                                                                                    }
                                                                                                                });
                                                                                                            }







                                                                                                            $('#cantidadProducida').numeric();
                                                                                                            $('#cantidadDistribuir').numeric();
                                                                                                        });
                                                                                                        function selectModulo(valor) {
                                                                                                            if (valor == "fecha") {
                                                                                                                $("#modulo_fecha").show("slow");
                                                                                                                $("#modulo_monto").hide("");
                                                                                                                $("#modulo_alcance").hide("");
                                                                                                            }

                                                                                                            if (valor == "monto") {
                                                                                                                $("#modulo_monto").show("slow");
                                                                                                                $("#modulo_fecha").hide("");
                                                                                                                $("#modulo_alcance").hide("");
                                                                                                            }

                                                                                                            if (valor == "alcance") {
                                                                                                                $("#modulo_alcance").show("slow");
                                                                                                                $("#modulo_monto").hide("");
                                                                                                                $("#modulo_fecha").hide("");
                                                                                                            }
                                                                                                        }





                                                        function changeProgram() {


                                                            var id_programSelected;
                                                            id_programSelected = $("#programaSeleccionado option:selected").val();
                                                            if (id_programSelected == "agregar") {
                                                                $('.modal-nuevo').modal("show");
                                                            }



                                                            var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                                                            $("#titleProgram").hide();
                                                            var res = programaSeleccionado.split("-");
                                                            $.ajax({
                                                                url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                                                                type: "POST",
                                                                data: $('#form_navbar').serialize(),
                                                                dataType: "JSON",
                                                                success: function (data)
                                                                {
                                                                    var status, message;
                                                                    $.each(data, function (index, obj) {
                                                                        status = obj.status;
                                                                        titulo_programa = obj.msg;
                                                                        recurso = obj.recurso;
                                                                        esquema = obj.esquema;
                                                                    });
                                                                    $("#titleProgram").html("<h1>" + titulo_programa + "</h1>").show("slow");
                                                                    if (recurso == 1) {
                                                                        $("#patcs_menu").hide();
                                                                        $("#petcs_menu").show("slow");
                                                                            $("#documentos_menu").show("slow");
                                                                        $("#seguimiento_menu").hide("slow");
                                                                        $("#reuniones_menu").show("slow");

                                                                           $("#informes_menu").show("slow");
                                                                             $("#formatos_menu").hide("slow");
                                                                    }


                                                                    v_esquema = 0;
                                                                    if (esquema == 1) {
                                                                        v_esquema = 1;
                                                                    }


                                                                    if (recurso == 2 || v_esquema == 1) {
                                                                        $("#petcs_menu").hide();
                                                                        $("#patcs_menu").show("slow");
                                                                            $("#documentos_menu").show("slow");
                                                                        $("#reuniones_menu").hide("slow");
                                                                        $("#seguimiento_menu").show("slow");
                                                                        // v_esquema = 0;

                                                                         $("#reuniones_menu").hide("slow");
                                                                           $("#informes_menu").hide("slow");
                                                                             $("#formatos_menu").show("slow");


                                                                    }
                                                                }

                                                            });
                                                        }


                                                        function sumaPeople() {

                                                            var total = 0;
                                                            $(".t_people").each(function () {
                                                                if (!isNaN(this.value) && this.value.length != 0) {
                                                                    //     alert("value:" + this.value);
                                                                    total += parseFloat(this.value);
                                                                }
                                                            });
                                                            //total = parseFloat($("#total_people_womans").val()) + parseFloat($("#total_people_mans").val());

                                                            $("#total_personas").val(total);
                                                        }


                                                        function guardar() {



                                                            $.ajax({
                                                                url: "<?php echo site_url('registro_materiales_difusion/guardarMaterialDifusion') ?>",
                                                                type: "POST",
                                                                data: $('#form_registrar_material_difusion').serialize(),
                                                                dataType: "JSON",
                                                                success: function (data)
                                                                {
                                                                    var status, message;
                                                                    $.each(data, function (index, obj) {
                                                                        status = obj.status;
                                                                        message = obj.msg;
                                                                    });
                                                                }
                                                            });
                                                            $.ajax({
                                                                url: "<?php echo site_url('registro_materiales_difusion/guardarMaterialDistribucion') ?>",
                                                                type: "POST",
                                                                data: $('#form_material_distribuir').serialize(),
                                                                dataType: "JSON",
                                                                success: function (data)
                                                                {
                                                                    var status, message;
                                                                    $.each(data, function (index, obj) {
                                                                        status = obj.status;
                                                                        message = obj.msg;
                                                                    });
                                                                }
                                                            });
                                                            swal("Bien hecho!", "El convenio modificador ha sido guardado.", "success");
                                                        }


                                                        $("#municipio").change(function () {

                                                            var municipioSelected = $("#municipio option:selected").val();
                                                            populate_localidad(municipioSelected);
                                                        });
                                                        function resetearForma() {


                                                            location.href = "<?php echo site_url(); ?>/registro_convenio_modificadoras";
                                                            /*$(".form-control").each(function() {
                                                             $(this).val("");  */



                                                        }
 


                                                        function populate_localidad(fromMunicipio) {

                                                            var cantElement = 0;
                                                            $('#localidad').empty();
                                                            $('#localidad').append("<option>Cargando ....</option>");
                                                            $.ajax({
                                                                type: "GET",
                                                                url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                                                                contentType: "application/json;charset=utf-8",
                                                                dataType: 'json',
                                                                success: function (data) {
                                                                    $('#localidad').empty();
                                                                    $('#localidad').append("<option>Selecciona Localidad..</option>");
                                                                    $.each(data, function (i, name) {
                                                                        $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                                                                        cantElement++;
                                                                    });
                                                                }
                                                            });
                                                        }






                                                        function  cargarFile(elem, alt, seccion, objeto) {




                                                            var file_data = $('#file' + alt).prop('files')[0];
                                                            var form_data = new FormData();
                                                            form_data.set("seccion", "");
                                                            form_data.set("depto", "");
                                                            form_data.set("fk_depto", "");
                                                            form_data.set("programa", "");
                                                            form_data.set("objeto", "");
                                                            form_data.append('file', file_data);
                                                            form_data.append("seccion", seccion);
                                                            form_data.append("depto", '<?= $departamento ?>');
                                                            form_data.append("fk_depto", '<?= $fk_departamento ?>');
                                                            form_data.append("programa", '<?= $fk_programa ?>');
                                                            form_data.append("objeto", objeto);
                                                            $.ajax({
                                                                url: '<?php echo base_url(); ?>index.php/upload/do_upload',
                            type: "post",
                            //data:new FormData(elem.form),
                            data: form_data,
                            processData: false,
                            contentType: false,
                            cache: false,
                            async: false,
                            success: function (data) {


                                console.log(data);
                                $("#band" + alt).html("Carga Exitosa...");
                                $("#url" + alt).html("<a target='_blank' href='" + data + "'>Previsualizar</a>");
                                if (data != "") {
                                    $("#archivoCargado").val(1);
                                }

                            }
                        });
                    }






                    </script>

                    </body>

                    </html>
