<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Upload files using Codeigniter and Ajax</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
</head>
<body>
    <div class="container">
        <div class="col-sm-4 col-md-offset-4">
        <h4>Upload files using Codeigniter and Ajax</h4>
            <form class="form-horizontal" id="submit">
                <div class="form-group">
                    <input type="text" name="title" class="form-control" placeholder="Title">
                </div>
                <div class="form-group">
                    <input type="file" name="file" id="file1">
                    <button class="btn btn-primary btn-sm" id="btn_upload" onClick="cargarFile(this,1)" type="button">Anexar</button>
                    <div id="band1"></div>
                </div>

                   <div class="form-group">
                    <input type="file" name="file" id="file2">
                      <button class="btn btn-primary btn-sm" id="btn_upload" onClick="cargarFile(this,2)" type="button">Anexar</button>
                    <div id="band2"></div>
                </div>
 
                 
            </form>   
        </div>
    </div>
<!--<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.2.1.js'?>"></script>-->
<!--<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>-->
<script src="<?php echo asset_url();?>js/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>


<script type="text/javascript">

     function  cargarFile(elem,alt){
 


 
          var file_data = $('#file'+alt).prop('files')[0];
                    var form_data = new FormData();

                    form_data.set("seccion", "");
                    form_data.set("depto", "");
                    form_data.set("programa", "");
                    form_data.set("objeto", "");
                    form_data.append('file', file_data);
                    form_data.append("seccion", 'comite');
                    form_data.append("depto", 'SEDESOQ');
                    form_data.append("programa", '16');
                    form_data.append("objeto", 'asamblea');
    $.ajax({
                     url:'<?php echo base_url();?>index.php/upload/do_upload',
                     type:"post",
                     //data:new FormData(elem.form),
                     data:form_data,
                     processData:false,
                     contentType:false,
                     cache:false,
                     async:false,
                      success: function(data){
                          //alert("Upload Image Successful.");
                          $("#band"+alt).html("Carga Exitosa...");
                   }
                 }); 

  }
     
</script>
</body>
</html>