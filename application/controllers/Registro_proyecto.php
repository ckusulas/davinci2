<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_proyecto extends CI_Controller {



	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
       
        $this->load->library('session');

    }
 
	public function index($id_proyecto="")
	{

        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
    }

      

        $metodo = $this->input->post('method');
        $id_proyecto = $this->input->post('id_proyecto');
        $programaSelected = $this->input->post('seleccPgma');
        


        
        //$fk_departamento = ""; //$this->input->post('departamento');
        $fk_departamento =  $this->session->userdata('departamento');

        if($programaSelected == "") {
            $programaSelected = $this->session->userdata('ProgramasSelecc');
        }

        
      
        $fk_programa_recurso = $this->ffinanciamiento_model->getProgramaRecurso($ejercicioFiscal="2019", $programaSelected, $fk_departamento);
        $presupuesto_vigilar_cf = 0; //$this->ffinanciamiento_model->getPresupuestoVigilar($id_programa_recurso);

      // echo "programa_recurso:" . $fk_programa_recurso;
       /* echo "presupuesto_vigilar_cf:" . $presupuesto_vigilar_cf; */
 
         
      
        //VERIFICANDO EXISTENCIA OBRA
        if($programaSelected!="todos"){
            $this->db->where('fk_program', $programaSelected);
        }
        //$this->db->where('fk_departamento', $fk_departamento);


        $this->db->where('id', $id_proyecto);       
        $q = $this->db->get('cs_registro_proyecto');

        $onlyView = "";
        if($metodo == "view"){
            $onlyView = 1;
        }

        if($metodo == "edit" || trim($metodo) == ""){
            $onlyView = 2;
        }

          $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
            
  
        $editData = array (
                'id' => '',
                'id_proyecto' => '',
                'metodo' => '',

                'nombre_proyecto' => '',               
                'fk_beneficio' => '',               
                'statusProyecto' => '',               
                'comentarios_proyecto' => '',

                'oficio_aprobacion_picaso' => '',
                'oficio_aprobacion_equivalente' => '',
                'fk_instancia_ejecutora' => '',
                'fk_instancia_primaria' =>'',
                'fk_instancia_secundaria' => '',

                'mujeres_beneficiadas' => '',
                'hombres_beneficiados' => '',
                'total_beneficiados' => '',

                'fecha_inicio_programada' => '',
                'fecha_inicio_ejecucion' => '',
                'fecha_unica_programada' => '',
                'fecha_unica_ejecucion' => '',
                'fecha_final_programada' => '',
                'fecha_final_ejecucion' => '',

                'fecha_aprobacion_obra' => '',

                'fk_municipio' => '',
                'fk_localidad' => '',
                'domicilio_conocido' => '',
                'calle' => '',
                'numero' => '',
                'colonia' => '',
                'cpostal' => '',
                'comentarios_calle' => '',
               

                'fecha_asignacion_recurso_federal' => '',
                'monto_recurso_asignado_vig_federal' => '',
                'fecha_ejecucion_recurso_federal' => '',
                'monto_recurso_asignado_ejec_federal' => '',

                'fecha_asignacion_recurso_estatal' => '',
                'monto_recurso_asignado_vig_estatal' => '',
                'fecha_ejecucion_recurso_estatal' => '',
                'monto_recurso_asignado_ejec_estatal' => '',


                'fecha_asignacion_recurso_municipal' => '',
                'monto_recurso_asignado_vig_municipal' => '',
                'fecha_ejecucion_recurso_municipal' => '',
                'monto_recurso_asignado_ejec_municipal' => '',
          
                'fecha_asignacion_recurso_otros' => '',
                'monto_recurso_asignado_vig_otros' => '',
                'fecha_ejecucion_recurso_otros' => '',
                'monto_recurso_asignado_ejec_otros' => '',

                'recurso_asignado_vigilar' => '',
                'recurso_ejecutado_vigilado' => '',
                'onlyView' => '',
                'seleccPgma' => '',
                'listLocalidades' => '',
                'responsable_llenado_nombre' => '',
                'responsable_llenado_cargo' => '',
                'monto_contratado' => '',
                'fecha_contrato' => '',
                'presupuesto_vigilar_cf' => $presupuesto_vigilar_cf,
                'fk_programa_recurso' => $fk_programa_recurso,


                'cuadro_f' => $this->session->userdata('cuadro_f'),
                'cuadro_e' => $this->session->userdata('cuadro_e'),
                'cuadro_m' => $this->session->userdata('cuadro_m'),
                'cuadro_o' => $this->session->userdata('cuadro_o'),
            
  

            );


        if ($q->num_rows() > 0) {

            $metodo = "edit";

            foreach ($q->result() as $row) {

                 $list_localidades = array();

                if($row->fk_municipio!=""){
                    $list_localidades = $this->ffinanciamiento_model->findLocalidades($row->fk_municipio);
                }


                $monto_contratado = ""; 
                if($row->monto_contratado!="")  {          
                    $monto_contratado = number_format($row->monto_contratado);
                }

                $editData = array (                   
                    'id' => $row->id,
                    'id_proyecto' => $id_proyecto,
                    'metodo' => $metodo,
                    
                    'nombre_proyecto' => $row->nombre_proyecto,
                    'fk_beneficio' => $row->fk_beneficio,
                    'statusProyecto' => $row->estatus_proyecto,
                    'comentarios_proyecto' => $row->comentarios_proyecto,
                    
                    'en_picaso' => $row->en_picaso,
                    'oficio_aprobacion_picaso' => $row->oficio_aprobacion_picaso,
                    'oficio_aprobacion_equivalente' => $row->oficio_aprobacion_equivalente,
                    'fk_instancia_ejecutora' => $row->fk_instancia_ejecutora,
                    'fk_instancia_primaria' => $row->fk_instancia_primaria,
                     
                    'mujeres_beneficiadas' => number_format($row->mujeres_beneficiadas),
                    'hombres_beneficiados' => number_format($row->hombres_beneficiados),
                    'total_beneficiados' => number_format($row->total_beneficiados),

                    'responsable_llenado_nombre' => $row->responsable_llenado_nombre,
                    'responsable_llenado_cargo' => $row->responsable_llenado_cargo,

                    'fecha_inicio_programada' => $this->transformDateToView($row->fecha_inicio_programada),
                    'fecha_inicio_ejecucion' => $this->transformDateToView($row->fecha_inicio_ejecucion),
                    'fecha_unica_programada' => $this->transformDateToView($row->fecha_unica_programada),
                    'fecha_unica_ejecucion' => $this->transformDateToView($row->fecha_unica_ejecucion),
                    'fecha_final_programada' => $this->transformDateToView($row->fecha_final_programada),
                    'fecha_final_ejecucion' =>  $this->transformDateToView($row->fecha_final_ejecucion),
                    'fecha_aprobacion_obra' =>  $this->transformDateToView($row->fecha_aprobacion_obra),

                    'fk_municipio' => $row->fk_municipio,
                    'fk_localidad' => $row->fk_localidad,
                    'domicilio_conocido' => $row->domicilio_conocido,
                    'calle' => $row->calle,
                    'numero' => $row->numero,
                    'colonia' => $row->colonia,
                    'cpostal' => $row->cpostal, 
                    'comentarios_calle' => $row->comentarios_calle,

                    'fecha_asignacion_recurso_federal' => $this->transformDateToView($row->fecha_asignacion_recurso_federal),
                    'monto_recurso_asignado_vig_federal' => number_format($row->monto_recurso_asignado_vig_federal,2),
                    'fecha_ejecucion_recurso_federal' => $this->transformDateToView($row->fecha_ejecucion_recurso_federal),
                    'monto_recurso_asignado_ejec_federal' => number_format($row->monto_recurso_asignado_ejec_federal,2),

                    'fecha_asignacion_recurso_estatal' => $this->transformDateToView($row->fecha_asignacion_recurso_estatal),
                    'monto_recurso_asignado_vig_estatal' => number_format($row->monto_recurso_asignado_vig_estatal,2),
                    'fecha_ejecucion_recurso_estatal' => $this->transformDateToView($row->fecha_ejecucion_recurso_estatal),
                    'monto_recurso_asignado_ejec_estatal' => number_format($row->monto_recurso_asignado_ejec_estatal,2),

                    'fecha_asignacion_recurso_municipal' => $this->transformDateToView($row->fecha_asignacion_recurso_municipal),
                    'monto_recurso_asignado_vig_municipal' => number_format($row->monto_recurso_asignado_vig_municipal,2),
                    'fecha_ejecucion_recurso_municipal' => $this->transformDateToView($row->fecha_ejecucion_recurso_municipal),
                    'monto_recurso_asignado_ejec_municipal' => number_format($row->monto_recurso_asignado_ejec_municipal,2),
              
                    'fecha_asignacion_recurso_otros' => $this->transformDateToView($row->fecha_asignacion_recurso_otros),
                    'monto_recurso_asignado_vig_otros' => number_format($row->monto_recurso_asignado_vig_otros,2),
                    'fecha_ejecucion_recurso_otros' => $this->transformDateToView($row->fecha_ejecucion_recurso_otros),
                    'monto_recurso_asignado_ejec_otros' => number_format($row->monto_recurso_asignado_ejec_otros,2),

                    'recurso_asignado_vigilar' =>  number_format($row->recurso_asignado_vigilar,2),
                    'recurso_ejecutado_vigilado' => number_format($row->recurso_ejecutado_vigilado,2),

                    'listLocalidades' => $list_localidades,
                    'onlyView' => $onlyView,
                    'seleccPgma' => $programaSelected,
                    'monto_contratado' => $monto_contratado,
                    'fecha_contrato' => $this->transformDateToView($row->fecha_contrato),
                    //'presupuesto_vigilar_cf' => $presupuesto_vigilar_cf,
                    'presupuesto_vigilar_cf' => $presupuesto_vigilar_cf,
                    'fk_programa_recurso' => $fk_programa_recurso,


                    'cuadro_f' => $this->session->userdata('cuadro_f'),
                    'cuadro_e' => $this->session->userdata('cuadro_e'),
                    'cuadro_m' => $this->session->userdata('cuadro_m'),
                    'cuadro_o' => $this->session->userdata('cuadro_o'),
        
 
      

                );

            

            }
        }

     //   $row->fk_municipio


        $listIntanceEjecutora = $this->ffinanciamiento_model->getListInstanciaEjecutora(2018);
        $listMunicipios = $this->ffinanciamiento_model->getMunicipios();


        $tituloPrograma = "";
        if($programaSelected!="todos"){
            $tituloPrograma =  $this->ffinanciamiento_model->getNamePrograma($programaSelected);
        }

        if($id_proyecto=="")
            $id_proyecto = 0;


        $listProyectoMetas = $this->ffinanciamiento_model->getProyectoMetas($id_proyecto);


        $data = array(     
            'listInstanceEjecutora' => $listIntanceEjecutora,
            'listMunicipios' => $listMunicipios,
            'tituloPrograma' => $tituloPrograma,
            'editData'       => $editData,
            'listRecourses'  => $listRecourses,
            'proyectoMetas'  => $listProyectoMetas,
           
        );

        $this->load->view('registro_proyecto', $data);

	}


    public function edit($id_proyecto){
          $this->index($id_proyecto);

    }


     public function populate_localidad($findResource) {
        
        $listLocalidad = $this->ffinanciamiento_model->findLocalidades($findResource);
        echo json_encode($listLocalidad);
    }




    public function existeComite(){


        $id_proyecto = $this->input->post('id_proyecto');
        $departamento = $this->input->post('fk_departamento');
 

        //primero verificar si existe el proyecto en Comite
    
        $this->db->where('accion_conexion', $id_proyecto);
        $this->db->where('fk_departamento', $departamento);       
        $this->db->where('eliminado', NULL);       

 

        $q = $this->db->get('cs_registro_comite');

        $existe_comite = 0;
        $msg = "";

        if ($q->num_rows() > 0) {
            $existe_comite = 1;            
            $msg = "existe";
        }

           $status = $existe_comite;
            
          

        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));


    }

    public function eliminarObra(){

        $id_proyecto = $this->input->post('id_proyecto');
        $departamento = $this->input->post('fk_departamento');
 

        //primero verificar si existe el proyecto en Comite
        $this->db->where('accion_conexion', $id_proyecto);
        $this->db->where('fk_departamento', $departamento);       

 

        $q = $this->db->get('cs_registro_comite');

        $existe_comite = 0;

        if ($q->num_rows() > 0) {
            $existe_comite = 1;            
        }



        //Aplicando el pseudoBorrado al Proyecto
         
              $this->db->set('eliminado', 1); 
              $this->db->where('id', $id_proyecto);      
              $this->db->update('cs_registro_proyecto');
       

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "ok";
        } 

        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }





    public function cancelarObra(){

        $id_proyecto = $this->input->post('id_proyecto');
        $departamento = $this->input->post('fk_departamento');
 

        //primero verificar si existe el proyecto en Comite
        $this->db->where('accion_conexion', $id_proyecto);
        $this->db->where('fk_departamento', $departamento);       

 

        $q = $this->db->get('cs_registro_comite');

        $existe_comite = 0;

        if ($q->num_rows() > 0) {
            $existe_comite = 1;            
        }



        //Aplicando el pseudoBorrado al Proyecto
         
              $this->db->set('estatus_proyecto', 'Cancelado'); 
              $this->db->where('id', $id_proyecto);      
              $this->db->update('cs_registro_proyecto');
       

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "ok";
        } 

        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }




     public function guardarDatosGeneralesObra(){
 
        //print_r($_POST);
        $nombreProyecto = trim($this->input->post('nombreProyecto'));
        $beneficio = $this->input->post('optionBeneficio');

        $statusProyectoSelected = $this->input->post('statusProyecto');
 
        $comentariosProyecto = trim($this->input->post('comentariosProyecto'));
        $instanciaEjecutora = trim($this->input->post('instanciaEjecutora_tmp'));
        $instanciaPromotora = trim($this->input->post('instanciaPromotora_tmp'));
        $metodo = $this->input->post('metodo');

        $id_proyecto  = trim($this->input->post('id_proyecto'));
        $aprobacion_en_picaso = $this->input->post('numeroPicaso');

       // echo "id_proyecto_antes:" . $id_proyecto;

        $id_proyecto_obra = $this->input->post('id_proyecto_fechaobra');
        // echo "id_proyecto_obra:" . $id_proyecto_obra;
 
        //Excepcion solo para el caso de View, al guardar no manda id_proyecto raro caso.
        if($id_proyecto==""){
           $id_proyecto =  $id_proyecto_obra;
        }

         //  echo "id_proyecto_dsps:" . $id_proyecto;


        $fk_program =  $this->session->userdata('ProgramasSelecc');
        $fk_subprogram =  $this->session->userdata('SubProgramasSelecc');
        $departamento =  $this->session->userdata('departamento');
         

        $ejercicioFiscal = "2018";        
 
        $fk_programa_recurso = "";
        //Buscando relacion de Programa con Recurso
 
       /* $this->db->where('fk_programa_proyectos', $fk_program);
        $this->db->where('fk_departamento', $departamento);       
       
        $q = $this->db->get('cs_registro_programa_recursos');

          if ($q->num_rows() > 0) {

               foreach ($q->result() as $row) {
                    $fk_programa_recurso = $row->id;                    

                }

        }*/

        $fk_programa_recurso = $this->ffinanciamiento_model->getProgramaRecurso(2019, $fk_program, $departamento);

    
      

        $this->db->set('fk_program', $fk_program);
        $this->db->set('fk_subprogram', $fk_subprogram);        
        $this->db->set('nombre_proyecto', $nombreProyecto);
        $this->db->set('fk_beneficio', $beneficio);
        $this->db->set('estatus_proyecto', $statusProyectoSelected);
        $this->db->set('comentarios_proyecto', $comentariosProyecto);
        
        
        $this->db->set('fk_programa_recurso', $fk_programa_recurso);

        $this->db->set('fk_departamento', $departamento);
        

     

       
        //INSERT FF
        //if($id_proyecto=="" && $metodo!="edit"){
        if($id_proyecto==""){
            //echo "insertando";
            $this->db->insert('cs_registro_proyecto');
            $lastId = $this->db->insert_id();
        }
        else{
            //echo "actualizando";

             $this->db->where('id', $id_proyecto);      
             $this->db->update('cs_registro_proyecto');
            $lastId = $id_proyecto;
        }


       

        $this->guardarDatosOficio($lastId);
        $this->guardarBeneficios($lastId);
        $this->guardarFechasProyecto($lastId);
        $this->guardarContrato($lastId);
        $this->guardarDomicilioProyecto($lastId);
        $this->guardarAsignaFederal($lastId);
        $this->guardarAsignaEstatal($lastId);
        $this->guardarAsignaMunicipal($lastId);
        $this->guardarAsignaOtros($lastId);
        $this->guardarRecursosVigila($lastId);
 
 

 
         $data = array(        
             'registro_proyecto' => $lastId,                               
          );
 

        $this->session->set_userdata($data);

          


        //if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId;
             
    /* } else {
            $status = false;
            $msg = $lastId;
        }*/



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
        
 
    }



    public function ajax_getDataSumaRecursosProy($fk_ffinanciamiento){

 
        $datosSumaRecursosProyecto = $this->ffinanciamiento_model->getSumaRecursosProyecto($fk_ffinanciamiento);
        

        echo json_encode($datosSumaRecursosProyecto);




   }



    public function guardarDatosOficio($lastId_proyecto){

        $aprobacion_en_picaso = $this->input->post('numeroPicaso');
        $en_picaso = $this->input->post('enPicaso');
        $oficio_aprobacion = $this->input->post('oficioAprobacion');
        $instancia_ejecutora = $this->input->post('instanciaEjecutora');
        $fecha_aprobacion_obra = $this->input->post('fechaAprobacionObra');
      
        $instancia_arriba = $this->input->post('cataloInstanciaPromotora');


        //$lastId_proyecto = trim($this->input->post('id_proyecto_picaso'));
 
       

        if($fecha_aprobacion_obra!=""){
            $fecha_aprobacion_obra = $this->changeFormatDateSpEng($fecha_aprobacion_obra);
        }else{
            $fecha_aprobacion_obra = NULL;
        }

     

        //$lastId_proyecto = $this->session->userdata('registro_proyecto');
       

/*
        echo "aprobacion_picaso:" . $aprobacion_en_picaso . "<br>";
        echo "oficio_aprobacion:" . $oficio_aprobacion . "<br>";
        echo "instancia_ejecutora:" . $instancia_ejecutora . "<br>";
        echo "lastId:" . $lastId_proyecto . "<br>";


*/
      //  echo "fecha aprobacion:" . $fecha_aprobacion_obra . "<br>";

        $this->db->set('en_picaso', $en_picaso);
        $this->db->set('oficio_aprobacion_picaso', $aprobacion_en_picaso);
        $this->db->set('oficio_aprobacion_equivalente', $oficio_aprobacion);        
        $this->db->set('fk_instancia_ejecutora', $instancia_ejecutora);     
        $this->db->set('fk_instancia_primaria', $instancia_arriba);     
          
        $this->db->set('fecha_aprobacion_obra', $fecha_aprobacion_obra);    
       
        $this->db->where('id', $lastId_proyecto);       
        $this->db->update('cs_registro_proyecto');


       /* if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_proyecto;
             
        } else {
            $status = false;
            $msg = $lastId_proyecto;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));*/


        if ($this->db->affected_rows() > 0) {
            return true;
             
        } else {
            return false;
        }


    }





    public function guardarBeneficios($lastId_proyecto){

 
       // $beneficiarioWomans = trim($this->input->post('mujeresBeneficiadas'));
     //   $beneficiarioMans = trim($this->input->post('hombresBeneficiados'));
        $totalBeneficios = trim($this->input->post('totalBeneficios'));
      
        $nombreResponsable = trim($this->input->post('nombreResponsable'));
        $cargoResponsable = trim($this->input->post('cargoResponsable'));


        $descripcionMeta = ($this->input->post('descripcion_meta'));
        $unidadMeta = ($this->input->post('unidad_meta'));
        $qtyMeta = ($this->input->post('qty_meta'));


       // echo "tenemos:" . print_r($descripcionMeta);

        $this->db->where('fk_proyecto', $lastId_proyecto);     
        $this->db->delete('cs_registro_proyecto_metas');
    
    $counter = 0;
    /*  foreach($descripcionMeta as $row){

            $qty = $unidad = $descripcion = "";

            if($row!=""){
                $descripcion = $row;
            }

            if(isset($qtyMeta[$counter])){
                $cantidad =  $qtyMeta[$counter];
            }

            if(isset($unidadMeta[$counter])){
                $unidad = $unidadMeta[$counter];
            }

            $counter++;

            $this->db->set('descripcion', $descripcion);
            $this->db->set('cantidad', $cantidad);
            $this->db->set('unidad', $unidad);
            

            $this->db->set('fk_proyecto', $lastId_proyecto);
      


            
            $this->db->insert('cs_registro_proyecto_metas');
        }*/
        //$lastId_proyecto = $this->session->userdata('registro_proyecto');
       
        //$lastId_proyecto = trim($this->input->post('id_proyecto_beneficio'));
 

      //  $beneficiarioWomans = str_replace(',','',$beneficiarioWomans);  
      //  $beneficiarioMans = str_replace(',','',$beneficiarioMans);  
        $totalBeneficios = str_replace(',','',$totalBeneficios);     

/*
        echo "beneficio_mujeres: $beneficiarioWomans <br>";
        echo "beneficio_hombres: $beneficiarioMans <br>";
        echo "total_beneficiados: $totalBeneficios <br>";
        
 */


     //   $this->db->set('mujeres_beneficiadas', intval($beneficiarioWomans));
     //   $this->db->set('hombres_beneficiados', intval($beneficiarioMans));
        $this->db->set('total_beneficiados', intval($totalBeneficios));     
        $this->db->set('responsable_llenado_nombre', $nombreResponsable);       
        $this->db->set('responsable_llenado_cargo', $cargoResponsable);       

        $this->db->where('id', intval($lastId_proyecto));
        $this->db->update("cs_registro_proyecto");


       // echo $this->db->last_query();



       /* if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_proyecto;
             
        } else {
            $status = false;
            $msg = $lastId_proyecto;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));*/


        if ($this->db->affected_rows() > 0) {
            return true;
             
        } else {
            return false;
        }


         

    }




      public function changeFormatDateSpEng($data){

        $arrayDate = explode("/", $data);
        $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
        return($newData);
    }



public function guardarContrato($lastId_proyecto){

 
        $fecha_contrato = trim($this->input->post('fecha_contrato'));        
        $monto_contratado = trim($this->input->post('monto_contratado'));


         $monto_contratado = str_replace(',','',$monto_contratado); 



        if($fecha_contrato!=""){
            $fecha_contrato = $this->changeFormatDateSpEng($fecha_contrato);
        }else{
            $fecha_contrato = NULL;
        }
 
   
 

        $this->db->set('fecha_contrato', $fecha_contrato);
        $this->db->set('monto_contratado', $monto_contratado);
       
    
        $this->db->where('id', $lastId_proyecto);
        $this->db->update("cs_registro_proyecto");

 

        if ($this->db->affected_rows() > 0) {
           return true;
             
        } else {
           return false;
        }

         

    }

     public function guardarFechasProyecto($lastId_proyecto){

 
        $fechaInicioProgramada = trim($this->input->post('fechaInicioProgramada'));        
        $fechaInicioEjecucion = trim($this->input->post('fechaInicioEjecucion'));
        $fechaUnicaProgramada = trim($this->input->post('fechaUnicaProgramada'));
        $fechaUnicaEjecucion = trim($this->input->post('fechaUnicaEjecucion'));
        $fechaFinalProgramada = trim($this->input->post('fechaFinalProgramada'));
        $fechaFinalEjecucion = trim($this->input->post('fechaFinalEjecucion'));

    /*    echo "fechaInicioProgramada:" . $fechaInicioProgramada . "<br>";
        echo "fechaInicioEjecucion:" . $fechaInicioEjecucion . "<br>";
        echo "fechaFinalProgramada:" . $fechaFinalProgramada . "<br>";
        echo "fechaFinalEjecucion:" . $fechaFinalEjecucion . "<br>";*/


        if($fechaInicioProgramada!=""){
            $fechaInicioProgramada = $this->changeFormatDateSpEng($fechaInicioProgramada);
        }else{
            $fechaInicioProgramada = NULL;
        }
 
        if($fechaInicioEjecucion!=""){
            $fechaInicioEjecucion = $this->changeFormatDateSpEng($fechaInicioEjecucion);
        }else{
            $fechaInicioEjecucion = NULL;
        }

        if($fechaUnicaProgramada!=""){
            $fechaUnicaProgramada = $this->changeFormatDateSpEng($fechaUnicaProgramada);
        }else{
            $fechaUnicaProgramada = NULL;
        }

        if($fechaUnicaEjecucion!=""){
            $fechaUnicaEjecucion = $this->changeFormatDateSpEng($fechaUnicaEjecucion);
        }else{
            $fechaUnicaEjecucion = NULL;
        }

        if($fechaFinalProgramada!=""){
            $fechaFinalProgramada = $this->changeFormatDateSpEng($fechaFinalProgramada);
        }else{
            $fechaFinalProgramada = NULL;
        }
 
       if($fechaFinalEjecucion!=""){
            $fechaFinalEjecucion = $this->changeFormatDateSpEng($fechaFinalEjecucion);
        }else{
              $fechaFinalEjecucion = NULL;
        }


       // $lastId_proyecto = trim($this->input->post('id_proyecto_fechaobra'));

        //$lastId_proyecto = $this->session->userdata('registro_proyecto');
        //$lastId_proyecto = trim($this->input->post('id_proyecto_fechaobra'));
        //echo "idObra:" . $lastId_proyecto;


    
 

        $this->db->set('fecha_inicio_programada', $fechaInicioProgramada);
        $this->db->set('fecha_inicio_ejecucion', $fechaInicioEjecucion);
        $this->db->set('fecha_unica_programada', $fechaUnicaProgramada);     
        $this->db->set('fecha_unica_ejecucion', $fechaUnicaEjecucion);     
        $this->db->set('fecha_final_programada', $fechaFinalProgramada);     
        $this->db->set('fecha_final_ejecucion', $fechaFinalEjecucion);     

        $date_now =  new DateTime();
        $date_final_obra = new DateTime($fechaFinalProgramada);

        if($date_now>$date_final_obra){
          $bandData = "Terminado";
        }else{
             $bandData = "Iniciado";
        }

        $this->db->set('estatus_proyecto', $bandData);

        $this->db->where('id', $lastId_proyecto);
        $this->db->update("cs_registro_proyecto");



       /* if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_proyecto;
             
        } else {
            $status = false;
            $msg = $lastId_proyecto;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));*/

        if ($this->db->affected_rows() > 0) {
           return true;
             
        } else {
           return false;
        }

         

    }


     public function ajax_getDataPicaso($no_obra){

//        $fk_departamento = $this->session->userdata('departamento');

      
       
        $this->db->where('num_obra', $no_obra);       
        $q = $this->db->get('cs_datos_picaso');


       // $listComites = $this->ffinanciamiento_model->getListComites($fk_programa);
       
        $datosPicaso = $q->result();

        echo json_encode($datosPicaso);




   }


   public function ajax_select_obras($fk_programa, $accion_conexion =0){

        $fk_departamento = $this->session->userdata('departamento');

        $listObras = $this->ffinanciamiento_model->getListProysBeneficios($fk_programa,1);
        $listApoyos = $this->ffinanciamiento_model->getListProysBeneficios($fk_programa,2);
        $listServicios = $this->ffinanciamiento_model->getListProysBeneficios($fk_programa,3);



 
        $data = array(     
            'listObras' => $listObras,
            'listApoyos' => $listApoyos,
            'listServicios' => $listServicios,
            'accion_conexion' => $accion_conexion,
           
        );


        $this->load->view('template/sub_select_comite', $data);


 




   }













    function guardarDomicilioProyecto($lastId_proyecto){

        
        //$lastId_proyecto = $this->session->userdata('registro_proyecto');
        //$lastId_proyecto =  trim($this->input->post('id_proyecto_domicilio'));
    
 


        $municipio = $this->input->post('municipio');        
        $localidad = $this->input->post('localidad2');
        $domicilioCortejado = $this->input->post('domicilioCortejado');
        $calle = $this->input->post('calle');
        $numero = $this->input->post('numero');
        $colonia = $this->input->post('colonia');
        $cpostal = $this->input->post('codigoPostal');
        $comentarios = $this->input->post('comentariosCalle');

        if($municipio == ""){
            $municipio = 0;
        }

        if($localidad == ""){
            $localidad = 0;
        }

/*
        echo "municipio:". $municipio . "<br>";
        echo "localidad:" . $localidad . "<br>";
        echo "domicilioCortejado:" . $domicilioCortejado . "<br>";
        echo "numero:" . $numero . "<calle>";
        echo "calle:" . $calle . "<br>";
        echo "cpostal:" . $cpostal . "<br>";*/



        $this->db->set('fk_municipio', $municipio);
        $this->db->set('fk_localidad', $localidad);
        $this->db->set('domicilio_conocido', $domicilioCortejado);     
        $this->db->set('calle', $calle);     
        $this->db->set('numero', $numero);     
        $this->db->set('colonia', $colonia);     
        $this->db->set('cpostal', $cpostal);     
        $this->db->set('comentarios_calle', trim($comentarios));     

        $this->db->where('id', $lastId_proyecto);
        $this->db->update("cs_registro_proyecto");


/*
        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_proyecto;
             
        } else {
            $status = false;
            $msg = $lastId_proyecto;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));*/

        if ($this->db->affected_rows() > 0) {
             return true;             
        } else {
            return false;
        }
         
     

    }


    


     function guardarAsignaFederal($lastId_proyecto){


       // print_r($_POST);

          // $lastId_proyecto = $this->session->userdata('registro_proyecto');
        //$lastId_proyecto = trim($this->input->post('id_proyecto_federal'));

    
 



        $fechaAsignacionRecursoFederal = trim($this->input->post('fechaAsignacionRecursoFederal'));        
        $montoRecursoAsignadoFederal   = trim($this->input->post('montoRecursoAsignadoFederal'));
        $fechaEjecucionRecursoFederal  = trim($this->input->post('fechaEjecucionRecursoFederal'));
        $montoRecursoEjecutadoFederal  = trim($this->input->post('montoRecursoEjecutadoFederal'));

       /* echo "fechaAsignacionRecursoFederal:" . $fechaAsignacionRecursoFederal;
        echo "fechaEjecucionRecursoFederal:" . $fechaEjecucionRecursoFederal;*/

        if($fechaAsignacionRecursoFederal == ""){
            $fechaAsignacionRecursoFederal = NULL;
        }

        if($fechaEjecucionRecursoFederal == ""){
            $fechaEjecucionRecursoFederal = NULL;
        }

        if($montoRecursoEjecutadoFederal == ""){
            $montoRecursoEjecutadoFederal = NULL;
        }

         if($montoRecursoAsignadoFederal == ""){
            $montoRecursoAsignadoFederal = NULL;
        }



/*
        echo "montoFederalAsignado:" . $montoRecursoAsignadoFederal . "<br>";
        echo "montoFederalEjecuado:" . $montoRecursoEjecutadoFederal;
      
        echo "fechaAsignacionRecursoFederal:". $fechaAsignacionRecursoFederal . "<br>";
        echo "montoRecursoAsignadoFederal:" . $montoRecursoAsignadoFederal . "<br>";
        echo "fechaEjecucionRecursoFederal:" . $fechaEjecucionRecursoFederal . "<br>";
        echo "montoRecursoEjecutadoFederal:" . $montoRecursoEjecutadoFederal . "<br>";*/

        if($fechaAsignacionRecursoFederal!=""){
            $fechaAsignacionRecursoFederal = $this->changeFormatDateSpEng($fechaAsignacionRecursoFederal);
        }else{
            $fechaAsignacionRecursoFederal = NULL;
        }
         
        if($fechaEjecucionRecursoFederal!=""){
            $fechaEjecucionRecursoFederal = $this->changeFormatDateSpEng($fechaEjecucionRecursoFederal);
        }else{
            $fechaEjecucionRecursoFederal = NULL;
        }


        $this->db->set('fecha_asignacion_recurso_federal', $fechaAsignacionRecursoFederal);
        $this->db->set('monto_recurso_asignado_vig_federal', $montoRecursoAsignadoFederal);
        $this->db->set('fecha_ejecucion_recurso_federal', $fechaEjecucionRecursoFederal);     
        $this->db->set('monto_recurso_asignado_ejec_federal', $montoRecursoEjecutadoFederal);     
          

        $this->db->where('id', $lastId_proyecto);
        $this->db->update("cs_registro_proyecto");

/*

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_proyecto;
             
        } else {
            $status = false;
            $msg = $lastId_proyecto;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));*/



        if ($this->db->affected_rows() > 0) {
            return true;
             
        } else {
            return false;
        }
         
     

    }




      function guardarAsignaEstatal($lastId_proyecto){

         
       // $lastId_proyecto = trim($this->input->post('id_proyecto_estatal'));
    
 


        $fechaAsignacionRecursoEstatal  = trim($this->input->post('fechaAsignacionRecursoEstatal'));        
        $montoRecursoAsignadoEstatal    = trim($this->input->post('montoRecursoAsignadoEstatal'));
        $fechaEjecucionRecursoEstatal   = trim($this->input->post('fechaEjecucionRecursoEstatal'));
        $montoRecursoEjecutadoEstatal   = trim($this->input->post('montoRecursoEjecutadoEstatal'));


        if($fechaAsignacionRecursoEstatal == ""){
            $fechaAsignacionRecursoEstatal = NULL;
        }

        if($montoRecursoAsignadoEstatal == ""){
            $montoRecursoAsignadoEstatal = NULL;
        }

        if($fechaEjecucionRecursoEstatal == ""){
            $fechaEjecucionRecursoEstatal = NULL;
        }

        if($montoRecursoEjecutadoEstatal == ""){
            $montoRecursoEjecutadoEstatal = NULL;
        }

      

      /*
        echo "fechaAsignacionRecursoEstatal:". $fechaAsignacionRecursoEstatal . "<br>";
        echo "montoRecursoAsignadoEstatal:" . $montoRecursoAsignadoEstatal . "<br>";
        echo "fechaEjecucionRecursoEstatal:" . $fechaEjecucionRecursoEstatal . "<br>";
        echo "montoRecursoEjecutadoEstatal:" . $montoRecursoEjecutadoEstatal . "<br>";*/

        if($fechaAsignacionRecursoEstatal!=""){
            $fechaAsignacionRecursoEstatal = $this->changeFormatDateSpEng($fechaAsignacionRecursoEstatal);
        }else{
            $fechaAsignacionRecursoEstatal = NULL;
        }
         
        
        if($fechaEjecucionRecursoEstatal!=""){
            $fechaEjecucionRecursoEstatal = $this->changeFormatDateSpEng($fechaEjecucionRecursoEstatal);
        }else{
             $fechaEjecucionRecursoEstatal = NULL;
        }


        $this->db->set('fecha_asignacion_recurso_estatal', $fechaAsignacionRecursoEstatal);
        $this->db->set('monto_recurso_asignado_vig_estatal', $montoRecursoAsignadoEstatal);
        $this->db->set('fecha_ejecucion_recurso_estatal', $fechaEjecucionRecursoEstatal);     
        $this->db->set('monto_recurso_asignado_ejec_estatal', $montoRecursoEjecutadoEstatal);     
          

        $this->db->where('id', $lastId_proyecto);
        $this->db->update("cs_registro_proyecto");



        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_proyecto;
             
        } else {
            $status = false;
            $msg = $lastId_proyecto;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        //echo json_encode(array($datos));
         
     

    }




      function guardarAsignaMunicipal($lastId_proyecto){


        //$lastId_proyecto = trim($this->input->post('id_proyecto_municipal'));

    
 

        $fechaAsignacionRecursoMunicipal = trim($this->input->post('fechaAsignacionRecursoMunicipal'));        
        $montoRecursoAsignadoMunicipal   = trim($this->input->post('montoRecursoAsignadoMunicipal'));
        $fechaEjecucionRecursoMunicipal  = trim($this->input->post('fechaEjecucionRecursoMunicipal'));
        $montoRecursoEjecutadoMunicipal  = trim($this->input->post('montoRecursoEjecutadoMunicipal'));


        if($fechaAsignacionRecursoMunicipal == ""){
            $fechaAsignacionRecursoMunicipal = NULL;
        }

        if($montoRecursoAsignadoMunicipal == ""){
            $montoRecursoAsignadoMunicipal = NULL;
        }

        if($fechaEjecucionRecursoMunicipal == ""){
            $fechaEjecucionRecursoMunicipal = NULL;
        }

        if($montoRecursoEjecutadoMunicipal == ""){
            $montoRecursoEjecutadoMunicipal = NULL;
        }

      
      
      /*  echo "fechaAsignacionRecursoMunicipal:". $fechaAsignacionRecursoMunicipal . "<br>";
        echo "montoRecursoAsignadoMunicipal:" . $montoRecursoAsignadoMunicipal . "<br>";
        echo "fechaEjecucionRecursoMunicipal:" . $fechaEjecucionRecursoMunicipal . "<br>";
        echo "montoRecursoEjecutadoMunicipal:" . $montoRecursoEjecutadoMunicipal . "<br>";*/

        if($fechaAsignacionRecursoMunicipal!=""){
            $fechaAsignacionRecursoMunicipal = $this->changeFormatDateSpEng($fechaAsignacionRecursoMunicipal);
        }else{
            $fechaAsignacionRecursoMunicipal = NULL;
        }
         
        if($fechaEjecucionRecursoMunicipal!=""){
            $fechaEjecucionRecursoMunicipal = $this->changeFormatDateSpEng($fechaEjecucionRecursoMunicipal);
        }else{
            $fechaEjecucionRecursoMunicipal = NULL;
        }


        $this->db->set('fecha_asignacion_recurso_municipal', $fechaAsignacionRecursoMunicipal);
        $this->db->set('monto_recurso_asignado_vig_municipal', $montoRecursoAsignadoMunicipal);
        $this->db->set('fecha_ejecucion_recurso_municipal', $fechaEjecucionRecursoMunicipal);     
        $this->db->set('monto_recurso_asignado_ejec_municipal', $montoRecursoEjecutadoMunicipal);     
          

        $this->db->where('id', $lastId_proyecto);
        $this->db->update("cs_registro_proyecto");



        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_proyecto;
             
        } else {
            $status = false;
            $msg = $lastId_proyecto;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


       // echo json_encode(array($datos));
         
     

    }



      function guardarAsignaOtros($lastId_proyecto){

 
        //$lastId_proyecto = trim($this->input->post('id_proyecto_otros'));
 

        $fechaAsignacionRecursoOtros = trim($this->input->post('fechaAsignacionRecursoOtros'));        
        $montoRecursoAsignadoOtros   = trim($this->input->post('montoRecursoAsignadoOtros'));
        $fechaEjecucionRecursoOtros  = trim($this->input->post('fechaEjecucionRecursoOtros'));
        $montoRecursoEjecutadoOtros  = trim($this->input->post('montoRecursoEjecutadoOtros'));


        if($fechaAsignacionRecursoOtros == ""){
            $fechaAsignacionRecursoOtros = NULL;
        }

        if($montoRecursoAsignadoOtros == ""){
            $montoRecursoAsignadoOtros = NULL;
        }

        if($fechaEjecucionRecursoOtros == ""){
            $fechaEjecucionRecursoOtros = NULL;
        }

        if($montoRecursoEjecutadoOtros == ""){
            $montoRecursoEjecutadoOtros = NULL;
        }

      
      

      /*
        echo "fechaAsignacionRecursoOtros:". $fechaAsignacionRecursoOtros . "<br>";
        echo "montoRecursoAsignadoOtros:" . $montoRecursoAsignadoOtros . "<br>";
        echo "fechaEjecucionRecursoOtros:" . $fechaEjecucionRecursoOtros . "<br>";
        echo "montoRecursoEjecutadoOtros:" . $montoRecursoEjecutadoOtros . "<br>";
*/


        if($fechaAsignacionRecursoOtros!=""){
            $fechaAsignacionRecursoOtros = $this->changeFormatDateSpEng($fechaAsignacionRecursoOtros);
        }else{
            $fechaAsignacionRecursoOtros = NULL;
        }
         
        if($fechaEjecucionRecursoOtros!=""){
            $fechaEjecucionRecursoOtros = $this->changeFormatDateSpEng($fechaEjecucionRecursoOtros);
        }else{
            $fechaEjecucionRecursoOtros = NULL;
        }


        $this->db->set('fecha_asignacion_recurso_otros', $fechaAsignacionRecursoOtros);
        $this->db->set('monto_recurso_asignado_vig_otros', $montoRecursoAsignadoOtros);
        $this->db->set('fecha_ejecucion_recurso_otros', $fechaEjecucionRecursoOtros);     
        $this->db->set('monto_recurso_asignado_ejec_otros', $montoRecursoEjecutadoOtros);     
          
        $this->db->where('id', $lastId_proyecto);
        $this->db->update("cs_registro_proyecto");



        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_proyecto;
             
        } else {
            $status = false;
            $msg = $lastId_proyecto;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        //echo json_encode(array($datos));
         
     

    }


        function guardarRecursosVigila($lastId_proyecto){

        //$lastId_proyecto = $this->session->userdata('registro_proyecto');
        //$lastId_proyecto = trim($this->input->post('id_proyecto_vigila'));

    
 

        $totalRecursoVigilar = $this->input->post('totalRecursoVigilar');        
        $totalRecursoVigilado = $this->input->post('totalRecursoVigilado');


         $totalRecursoVigilar = str_replace(',','',$totalRecursoVigilar); 
         $totalRecursoVigilado = str_replace(',','',$totalRecursoVigilado); 
        
        /*
        echo "totalRecursoVigilar:". $totalRecursoVigilar . "<br>";
        echo "totalRecursoVigilado:" . $totalRecursoVigilado . "<br>";*/

        if($totalRecursoVigilar == ""){
            $totalRecursoVigilar = NULL;
        }

        if($totalRecursoVigilado == ""){
             $totalRecursoVigilado = NULL;
        }
     
        


        $this->db->set('recurso_asignado_vigilar', $totalRecursoVigilar);
        $this->db->set('recurso_ejecutado_vigilado', $totalRecursoVigilado);
         
        $this->db->where('id', $lastId_proyecto);
        $this->db->update("cs_registro_proyecto");



        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_proyecto;
             
        } else {
            $status = false;
            $msg = $lastId_proyecto;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


       // echo json_encode(array($datos));
         
     

    }


    public function transformDateToView($data){

        if($data == "0000-00-00"){
            return "";
        }



        $arrayDate = explode("-", $data);
        if(strlen($data)>6){
            $newData = $arrayDate[2] . "/" . $arrayDate[1] . "/"  . $arrayDate[0];
            return($newData);
        }
    }







 

}


 