
                
    <form id="form_listadoSeguimientos" action="registro_seguimiento" method="post">

    <?php
 
     $programaSelected = $this->session->userdata('ProgramasSelecc');
    ?>

    <table id="table_seguimiento" class="display" class="col-lg-12">
        <thead>
            <tr >
                <th>Folio</th>
               
                <th>Comite</th>                      
                <th>Obra</th>                      
                <th>Municipio</th>
                <th>Localidad</th>
                <th>Ultimo Seguimiento</th>                
                <th>No. Seguimiento</th> 
                <th>Seguimientos</th> 

               
          
                   
              
               
                
               
                
               

                <th>Acciones</th>
            </tr>
        </thead>
               <tbody>

            <?php

            $counter = 1;



            foreach ($listSeguimientos as $row){
               

                echo "<tr id='tr_".$row["id_seguimiento"]."'><td>". $counter . "</td><td>".  $row["nombre_comite"] ."</td><td>". $row["nombre_proyecto"] . "</td><td>". $row["municipio"] . "</td><td>". $row["localidad"] . "</td><td>" . $row["fecha_seguimiento"] . "</td><td>2</td><td><a href='#' onClick='editSeguim(".$row['id_seguimiento'].")' border='0'><img width='25' title='seguim #1' height='25' src=".asset_url()."imagenes/File-02.jpg></a>,<img width='25' title='seguim #2' height='25' src=".asset_url()."imagenes/File-02.jpg></td>
                <td><img width='25' title='Agregar Nuevo Seguimiento' height='25' src=".asset_url()."imagenes/agregar.jpg>";
                ?>

                

             </td></tr>

                 


          

          <?php

            $counter++;
            
            }


            ?>
       
           
         
        </tbody>
       
    </table>


    <input type="hidden" name="id_seguimiento" id="id_seguimiento">
    <input type="hidden" name="fk_departamento" id="fk_departamento">
    <input type="hidden" name="method" id="method">
 



    </form>


  

    <script type="text/javascript">


    $(document).ready(function () {     
     
 


        $('#example tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="'+title+'" />' );
        } );
 


        var tableProy = $('#table_seguimiento').DataTable( {
             language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
            dom: 'Bfrtip',
            scrollX: 'true',
            pagingType: 'full_numbers',
            bAutoWidth: false,
        
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
            bInfo: false,
            buttons: [
                
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8 ]
                    }   
                },
                
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8 ]
                    }
                }
            ],

        } );


        /* tableProy.columns().every( function () {
            var that = this;
     
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );*/


               


           
 
            
            

          });    



   function editSeguim(id_seguimiento)
        {

            $("#id_seguimiento").val(id_seguimiento);
            $("#method").val("edit");
           
            $("#form_listadoSeguimientos").submit();



        }


     function viewSeguim(id_seguimiento)
        {

            $("#id_seguimiento").val(id_proyecto);
            $("#method").val("view");
           
            $("#form_listadoSeguimientos").submit();



        }



    

     function delSeguim(id_seguimiento,fk_departamento){


            $("#id_seguimiento").val(id_seguimiento);
            $("#fk_departamento").val(fk_departamento);

            $.ajax({
                    url: "<?php echo site_url('registro_proyecto/existeComite') ?>",
                    type: "POST",
                    data: $('#form_listadoSeguimientos').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                      if(status){
                           swal("Ups..", "No se puede borrar Obra, esta vinculado a un Comité", "error");
                       }else{
                           

                            swal({
                              title: "Estás seguro de querer borrar la obra?",
                              text: "",
                              icon: "warning",
                              buttons: true,
                              dangerMode: false,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                                swal("Listo!, Se ha borrado exitosamente", {
                                  icon: "success",
                                });
                                eliminarProyectoAjax(id_proyecto, fk_departamento);

                              } else {
                                swal("Lo dejamos como estaba..");
                              }
                            });
                       }
                        
 
                    }
                });


            return;

           
        }


    function eliminarProyectoAjax(id_proyecto, fk_departamento){

         $("#id_proyecto").val(id_proyecto);
         $("#fk_departamento").val(fk_departamento);

            $.ajax({
                    url: "<?php echo site_url('registro_proyecto/eliminarObra') ?>",
                    type: "POST",
                    data: $('#form_listadoSeguimientos').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                       if(message=="ok"){
                         $("#tr_"+id_proyecto).hide('slow');
                       }

                       if(message == "comite"){
                        alert("No se puede borrar Obra, si esta vinculado a un Comite.")
                       }
                        
 
                    }
                });
    }

        


    </script>