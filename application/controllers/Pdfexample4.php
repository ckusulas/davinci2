<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdfexample4 extends CI_Controller {
   


function __construct() { 
     parent::__construct();
       $this->load->model('ffinanciamiento_model');
} 


 


function index()
    {


          $id_capacitacion =  $this->input->post('id_capacitacion');

          echo "id_capacitacion:" . $id_capacitacion . "<br>";

          $this->db->where('id', $id_capacitacion);       
          $q = $this->db->get('cs_registro_capacitacion');



          foreach ($q->result() as $row) {
         /*        echo print_r($row);
          die();*/


              $nombre_capacitacion = $row->nombre_capacitacion;
              $tematica = $row->fk_tematica;
              $figura_capacitada = $row->fk_figura_capacitada;
              $fk_comite =  $row->fk_accion_conexion;

              $pgta_asamblea_beneficiarios = $row->pgta_asamblea_beneficiarios;
              $pgta_equidad_genero = $row->pgta_equidad_genero;
              $pgta_reglas_operacion = $row->pgta_reglas_operacion;
              $pgta_reglas_operacion = $row->pgta_reglas_operacion;
              $pgta_formatos_cs = $row->pgta_formatos_cs;
              $pgta_disposicion_quejas = $row->pgta_disposicion_quejas;
              $pgta_observaciones_comite = $row->pgta_observaciones_comite;

              

                    //'entidad_federativa' => $row->entidad_federativa,
              $fecha_imparticion = $this->transformDateToView($row->fecha_imparticion);

                 $getDataComite = $this->ffinanciamiento_model->getInfoComite($fk_comite);



          $listIntegrantes = $this->ffinanciamiento_model->getIntegrantesComite($fk_comite);
     // echo print_r($listIntegrantes);




          }

        //  die();


          $this->load->library('Pdf');
          
          //$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
          $pdf = new Pdf('L', PDF_UNIT, 'LETTER', true, 'UTF-8', false);

          

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Contraloria Social 2018');
$pdf->SetTitle('SICSEQ QRO');
 
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 17);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 9);

 

// add a page
$pdf->AddPage('L');


// -- set new background ---

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
   $img_file = asset_url()."/formats/capacitacion/capacitacion1.jpg";
  // $pdf->setJPEGQuality(75);

   // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)

$pdf->Image($img_file, 13, 0, 287, 210, '', '', '', false, 300, 0, false, false, false);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();


// Print a text
//$html = '<span style="color:red;text-align:center;font-weight:bold;font-size:80pt;">Draft</span>';
//$pdf->writeHTML($html, true, false, true, false, '');


/*
tcpdf.php:
public function Write($h, $txt, $link='', $fill=false, $align='', $ln=false, $stretch=0, $firstline=false, $firstblock=false, $maxh=0, $wadj=0, $margin='') {*/



/*$pdf->SetXY(60,80);

$txt = "3x1 PARA MIGRANTES";

$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);*/



//Programa
$pdf->SetXY(77,47);
 

$txtPrograma = $getDataComite[0]['programa'];

// print a block of text using Write()
$pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



//Obra
$pdf->SetXY(186,45);
 

$pdf->SetFont('helvetica', '', 7);
//$txtObra = "EMPEDRADO AHOGADO EN MORTERO, EN CALLE NINO HEROES";
$nombreObra = "1.- AMPLIACIÓN DE SISTEMA DE ALCANTARILLADO SANITARIO (5TA ETAPA), PARA BENEFICIAR A 13 LOCALIDADES DE LA DELEGACIÓN MUNICIPAL DE SANTIAGO MEXQUITITLÁN, EN EL MUNICIPIO DE AMEALCO DE BONFIL";
//strtoupper($getDataComite[0]['nombre_proyecto']);

//

// print a block of text using Write()
//$pdf->Write(0, $txtObra, '', 0, 'L', true, 0, false, false, 0);

/*$pdf->SetXY(186, 45);
$nombreObra = strtoupper($getDataComite[0]['nombre_proyecto']); */
$separador = "";
if(strlen($nombreObra)>54){
  $separador = "_";
}

$nombreObraTmp = substr($nombreObra,0,66).$separador;


$separador="";
$nombreObraTmp2 = substr($nombreObra,66,66);
if(strlen($nombreObraTmp2)>66){
  $separador="_";
  $nombreObraTmp2 = $nombreObraTmp2.$separador;
}



$separador="";
$nombreObraTmp3 = substr($nombreObra,132,66);
if(strlen($nombreObraTmp3)>66){
  $separador="_";
  $nombreObraTmp3 = $nombreObraTmp3.$separador;
}


$pdf->SetXY(186, 45);
$pdf->Write(0, $nombreObraTmp, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetXY(186, 48);
$pdf->Write(0, $nombreObraTmp2, '', 0, 'L', true, 0, false, false, 0);

$pdf->SetXY(186, 51);
$pdf->Write(0, $nombreObraTmp3, '', 0, 'L', true, 0, false, false, 0);

$pdf->SetXY(186, 54);
$pdf->Write(0, $nombreObraTmp4, '', 0, 'L', true, 0, false, false, 0);







//Municipio
$pdf->SetXY(77,60);
 
$pdf->SetFont('helvetica', '', 9);

$txtMunicipio = $getDataComite[0]['municipio'];
// print a block of text using Write()
$pdf->Write(0, $txtMunicipio, '', 0, 'L', true, 0, false, false, 0);



//Localidad
$pdf->SetXY(198,60);
 
$txtLocalidad = strtoupper($getDataComite[0]['localidad']);

// print a block of text using Write()
$pdf->Write(0, $txtLocalidad, '', 0, 'L', true, 0, false, false, 0);



//Dependencia Ejecutora
$pdf->SetXY(77,65);

$txtDependenciaEjecutora = $getDataComite[0]['instancia_ejecutora'];

// print a block of text using Write()
$pdf->Write(0, $txtDependenciaEjecutora, '', 0, 'L', true, 0, false, false, 0);


//Instancia Normativa
$pdf->SetXY(198,65);
 
$txtNormativa = "SDUOP";

// print a block of text using Write()
$pdf->Write(0, $txtNormativa, '', 0, 'L', true, 0, false, false, 0);




//PRIMER INTEGRANTE
       $nombre = strtoupper($listIntegrantes[0]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[0]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[0]['apmaterno_integrante']);


    //NOMBRE
    $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(62, 95);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //Cargo
    $pdf->SetXY(200, 98);
    $txtPrograma = strtoupper($listIntegrantes[0]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


      //Telefono
     $pdf->SetXY(164, 103);
    $txtPrograma = $listIntegrantes[0]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Direccion  
    $calle = strtoupper($listIntegrantes[0]['calle_integrante']);
    $num = $listIntegrantes[0]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[0]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    $pdf->SetXY(62, 120);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);











    //segundo INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(62, 112);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Cargo
    $pdf->SetXY(200, 115);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(62, 103);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(164, 120);
    $txtPrograma = $listIntegrantes[0]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);











    //tercer INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(62, 129);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Cargo
    $pdf->SetXY(200, 132);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(62, 137);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(164, 137);
    $txtPrograma = $listIntegrantes[1]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);






    //cuarto INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(62, 146);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Cargo
    $pdf->SetXY(200, 149);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'J', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(62, 154);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(164, 154);
    $txtPrograma = $listIntegrantes[1]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);










    //quinto INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(62, 163);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



    //Cargo
    $pdf->SetXY(200, 166);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, 'VOCAL', '', 0, 'J', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(62, 171);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(164, 171);
    $txtPrograma = $listIntegrantes[1]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);





    //sexto INTEGRANTE
       $nombre = strtoupper($listIntegrantes[1]['nombre_integrante']);
       $apPaterno = strtoupper($listIntegrantes[1]['appaterno_integrante']);
       $apMaterno =  strtoupper($listIntegrantes[1]['apmaterno_integrante']);

       $nombreCompleto = $nombre . " " . $apPaterno . " " . $apMaterno;

    $pdf->SetXY(62, 180);
    $txtPrograma = $nombreCompleto; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);

 

    //Cargo
    $pdf->SetXY(200, 183);
    $txtPrograma = strtoupper($listIntegrantes[1]['cargo_integrante']);
    $pdf->Write(0, $txtPrograma, '', 0, 'J', true, 0, false, false, 0);


     //Direccion    
    $calle = strtoupper($listIntegrantes[1]['calle_integrante']);
    $num = $listIntegrantes[1]['numero_integrante'];
    $colonia = strtoupper($listIntegrantes[1]['colonia_integrante']);

    $direccion = $calle . " " . $num . " Col." . $colonia;
    //$pdf->SetXY(31, 146);
    $pdf->SetXY(62, 188);
    $txtPrograma = $direccion;
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


       //Telefono
     $pdf->SetXY(164, 188);
    $txtPrograma = $listIntegrantes[1]['telefono_integrante'];
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);

//public function Write($h, $txt, $link='', $fill=false, $align='', $ln=false, $stretch=0, $firstline=false, $firstblock=false, $maxh=0, $wadj=0, $margin='')





// add a page
$pdf->AddPage('L');


// -- set new background ---

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
   $img_file = asset_url()."/formats/capacitacion/capacitacion2.jpg";
  // $pdf->setJPEGQuality(75);

   // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)

$pdf->Image($img_file, -5, 0, 289, 210, '', '', '', false, 300, '', false, false, 0);
//$pdf->Image($img_file, 13, 0, 287, 210, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();


// Print a text
 

// ---------------------------------------------------------


//ASAMBLEA
    //si
  /* $pdf->SetXY(240, 99);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/


    //no
    $pdf->SetXY(258, 99);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



//EQUIDAD
    //si
 /*  $pdf->SetXY(240, 103);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/


    //no
    $pdf->SetXY(258, 103);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);





//FICHA INFORMATIVA
    //si
   $pdf->SetXY(240, 107);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //no
   /* $pdf->SetXY(258, 107);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/



 //REGLAS OPERACION
    //si
   $pdf->SetXY(240, 111);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //no
  /*  $pdf->SetXY(258, 111);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/



     //FORMATOS
    //si
  /* $pdf->SetXY(240, 116);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/


    //no
    $pdf->SetXY(258, 116);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);



  //DISPOSICION QUEJAS
    //si
   $pdf->SetXY(240, 120);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);


    //no
    /*$pdf->SetXY(258, 120);
    $txtPrograma = "X"; 
    $pdf->Write(0, $txtPrograma, '', 0, 'L', true, 0, false, false, 0);*/









 ob_end_clean();
$pdf->Output('example_007.pdf', 'I');
    }




       public function transformDateToView($data){

        if($data == "0000-00-00"){
            return "";
        }



        $arrayDate = explode("-", $data);
        if(strlen($data)>6){
            $newData = $arrayDate[2] . "/" . $arrayDate[1] . "/"  . $arrayDate[0];
            return($newData);
        }
    }


}
?>
