<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listado_documento_normativo extends CI_Controller {

   /**
     * Developed by Constantino Kusulas Vazquez   - 2018.
     */


   	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
               $this->load->library('session');


       /* $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }*/
    }




	public function index()
	{  

        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }


    
         $programaSelected = $this->session->userdata('ProgramasSelecc');
                $id_departamento = $this->session->userdata('departamento'); 

                $departamento = $this->session->userdata('name');

                
                $data = array(     
               'fk_programa' => $programaSelected,
                'departamento' => $id_departamento,
                'fk_departamento' => $id_departamento,
 
                    'url_esquema_cs' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "doc_normativo", "esquema_cs"),
                    'url_patcs' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "doc_normativo", "patcs"),
                    'url_oficio_envio' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "doc_normativo", "oficio_envio"),
                    'url_ficha' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "doc_normativo", "ficha"),
                    'url_cuaderno' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "doc_normativo", "cuaderno"),
                    'url_petcs' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "doc_normativo", "petcs"),
                    'url_oficio_envio' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "doc_normativo", "oficio_envio"),
                    'url_escrito' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "doc_normativo", "escrito"),
                    'url_informe_anual' =>  $this->ffinanciamiento_model->getURL_adjunto($programaSelected, $id_departamento, "doc_normativo", "informe_anual"),

                 

        );




     


        $this->load->view('listado_documento_normativo', $data);
	}




	 

}
