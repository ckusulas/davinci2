    
                                                <div class="form-group">
                                                    <label  class="col-sm-4 text-right">Tipo de Beneficio:</label>
                                                     <div class="col-sm-6">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="1" checked>Obra
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="2">Apoyo
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="3" >Servicio
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label  class="col-sm-4 text-right">Estatus del Proyecto:</label>
                                                    <div class="col-sm-6">
                                                         <select class="form-control">
															<option>Seleccione...</option>
                                                            <option>Inicio</option>
                                                            <option>En Progreso</option>
                                                            <option>Finalizado</option>
                                                            <option>Cancelado</option>                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label  class="col-sm-4 text-right">Mujeres beneficiadas:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="textInput"   >
                                                    </div>
                                                </div>
											 
												<div class="form-group">
                                                    <label  class="col-sm-4 text-right">Hombres beneficiados:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="textInputDisabled" >
													</div>
                                                </div>
												
												<div class="form-group">
                                                    <label  class="col-sm-4 text-right">Total Beneficiados:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="textInputDisabled" disabled>
													</div>
                                                </div>
												
												<div class="form-group">
                                                    <label  class="col-sm-4 text-right">Municipio:</label>
                                                    <div class="col-sm-6">
                                                         <select class="form-control">
															<option>Seleccione...</option>
                                                            <option>Amealco de Bonfil</option>
															<option>Pinal de Amoles</option>
															<option>Arroyo Seco</option>
															<option>Cadereyta de Montes</option>
															<option>Colón</option>
															<option>Corregidora</option>
															<option>Ezequiel Montes</option>
															<option>Huimilpan</option>
															<option>Jalpan de Serra</option>
															<option>Landa de Matamoros</option>
															<option>El Marqués</option>
															<option>Pedro Escobedo</option>
															<option>Peñamiller</option>
															<option>Querétaro</option>
															<option>San Joaquín</option>
															<option>San Juan del Río</option>
															<option>Tequisquiapan</option>
															<option>Tolimán</option>
                                                        
                                                        </select>
                                                    </div>
                                                </div>
												
												<div class="form-group">
                                                    <label  class="col-sm-4 text-right">Localidad:</label>
                                                    <div class="col-sm-6">
                                                         <select class="form-control">
															<option>Seleccione...</option>																											
															<option>El Álamo Cuate</option>
															<option>La Joya</option>
															<option>Panales</option>
															<option>La Concepción</option>
															<option>La Palmita</option>
															<option>Tierra Adentro</option>
															<option>El Infiernillo</option>
															<option>La Zanja Grande</option>
															<option>Nuevo Álamos</option>
															<option>Los Benitos</option>
															<option>El Blanco</option>
															<option>Ejido Patria</option>
															<option>La Carbonera</option>
															<option>El Carrizal</option>
															<option>Las Cenizas</option>
															<option>El Coyote</option>
															<option>Esperanza</option>
															<option>El Fuenteño</option>
															<option>El Gallo</option>
															<option>El Leoncito</option>
															<option>México Lindo</option>
															<option>Nogales</option>
															<option>Purísima de Cubos (La Purísima)</option>
															<option>Peña Colorada</option>
															<option>La Pila</option>
															<option>El Poleo</option>
															<option>El Potrero</option>
															<option>Puerta de Enmedio</option>
															<option>El Mezote</option>
															<option>Puerto de San Antonio</option>
															<option>Los Quiotes (San José los Quiotes)</option>
															<option>Salitrera (Presa de la Soledad)</option>
															<option>San Francisco</option>
															<option>San Ildefonso</option>
															<option>Santa María Nativitas</option>
															<option>Santa Rosa de Lima (Santa Rosa Poblado)</option>
															<option>San Vicente el Alto (San Vicente)</option>
															<option>El Saucillo</option>
															<option>Los Trigos</option>
															<option>Urecho</option>
															<option>Vista Hermosa</option>
															<option>El Zamorano</option>
															<option>La Zorra</option>
															<option>Tanquecitos</option>
															<option>El Organal</option>
															<option>Palmas</option>
															<option>Tierra Dura</option>
															<option>Puerto del Coyote</option>
															<option>Ailitos</option>
															<option>Santa Rosa Finca</option>
															<option>Entronque Ajuchitlán</option>
															<option>Ejido Colón Fracción del Moral</option>
															<option>Rancho San Antonio (Colonia Álvaro Obregón)</option>
															<option>Nuevo Progreso</option>
															<option>Ojo de Agua</option>
															<option>La Quebradora</option>
															<option>Ampliación Galeras Oeste</option>
															<option>Barrio las Crucitas</option>
															<option>San Gabriel</option>
															<option>Colón</option>
															<option>Ajuchitlán</option>
															<option>Casas Viejas</option>
															<option>El Estanco</option>
															<option>Galeras</option>
															<option>El Lindero</option>
															<option>La Llosa (Rancho el Mezquite)</option>
															<option>Rancho la Montañesa</option>
															<option>San José la Peñuela (La Peñuela)</option>
															<option>Piedras Negras</option>
															<option>Rancho San Isidro</option>
															<option>San Martín</option>
															<option>Santa María de Guadalupe (Santa María del Mexicano)</option>
															<option>Rancho San Vicente (San Vicente el Bajo)</option>
															<option>Sauz Seco</option>
															<option>Rancho la Soledad</option>
															<option>Viborillas</option>
															<option>Peña Blanca</option>
															<option>Las Palmas</option>
															<option>El Salitrillo</option>
															<option>El Sauz</option>
															<option>Rancho Vista Hermosa</option>
															<option>San Jorge</option>
															<option>El Burral (Rancho Hermanos Flores)</option>
															<option>Rancho el Rocío</option>
															<option>Rancho San Fernando</option>
															<option>La Colmena</option>
															<option>Mesa de la Cruz</option>
															<option>El Terremote</option>
															<option>El Arte</option>
															<option>El Leoncito</option>
															<option>Rancho RAY</option>
															<option>El Cilguero (Pueblo Nuevo)</option>
															<option>Palo Alto</option>
															<option>Pueblo Nuevo</option>
															<option>El Nuevo Rumbo</option>
															<option>El Crucero</option>
															<option>La Ponderosa</option>
															<option>Granjas Unidas Tolimán</option>
															<option>Rancho San Antonio</option>
															<option>Cultivando Ideas</option>
															<option>Las Adjuntas</option>
															<option>Cerro del Pinal del Zamorano</option>
															<option>Laguna Seca</option>
															<option>El Terrero</option>
															<option>El Alfalfar [Granja]</option>
															<option>Rancho Galeras (Rancho los Olivos)</option>
															<option>El Cerrito de Don Félix</option>
															<option>Hontoria (Marquesado de Guadalupe) [Establo]</option>
															<option>Ejido el Muerto (El Infiernillo)</option>
															<option>4ta. Fracción Hontoria (Marquesado de Guadalupe)</option>
															<option>Amaral [Granja]</option>
															<option>El Milagro [Granja]</option>
															<option>La Luz [Granja]</option>
															<option>La Purísima [Granja]</option>
															<option>María del Carmen [Granja]</option>
															<option>Río [Granja]</option>
															<option>Feja [Maquiladora]</option>
															<option>La Hondonada</option>
															<option>Mal Paso</option>
															<option>El Nogalito</option>
															<option>Rancho los Laureles</option>
															<option>La Mora</option>
															<option>El Rancho</option>
															<option>Rancho El Cascabel</option>
															<option>Rancho el Jaguar</option>
															<option>Rancho San Francisco</option>
															<option>Rancho Guadalupe</option>
															<option>Rancho la Aurora</option>
															<option>Rancho la Mesita</option>
															<option>Rancho la Perla</option>
															<option>Rancho la Purísima</option>
															<option>Rancho los Arrayanes</option>
															<option>Rancho Milpillas</option>
															<option>Rancho Palo Seco</option>
															<option>Rancho San Felipe de Jesús</option>
															<option>Rancho San Judas</option>
															<option>Rancho San Martín</option>
															<option>Rancho Santa Sofía</option>
															<option>Familia Balderas</option>
															<option>Familia Nieves</option>
															<option>Familia Trejo</option>
															<option>Acceso a Urecho</option>
															<option>Familia Trejo Montes</option>
															<option>El Terrero</option>
															<option>Yerbabuena</option>
															<option>Zona Granjas Zorrillo</option>
															<option>Zona Salida a Tolimán (La Frontera)</option>
															<option>Conamegra [Mina]</option>
															<option>La Esperanza [Fraccionamiento]</option>
															<option>Granja Buenos Aires</option>
															<option>La Lomita</option>
															<option>Nuevo Rancho Guadalupe</option>
															<option>Rancho Florisol</option>
															<option>Rancho la Trinidad</option>
															<option>Rancho Los Cadetes</option>
															<option>Rancho San Carlos</option>
															<option>Rancho San Sebastián (Loma Buenos Aires)</option>
															<option>Alamitos (La Higuerita)</option>
															<option>Ninguno [Avícola la Peñuela]</option>
															<option>Ninguno [Distribuidora de Explosivos Oviedo]</option>
															<option>Rancho el Griego</option>
															<option>Ejido Purísima de Cubos</option>
															<option>Ejido San Ildefonso</option>
															<option>Ejido San Vicente el Alto Pozo Dos</option>
															<option>Familia Mandujano</option>
															<option>Familia Pérez</option>
															<option>Gazu</option>
															<option>Nuevo México [Granja]</option>
															<option>La Mina de Iris</option>
															<option>El Saucito</option>
															<option>Rancho el Conejo</option>
															<option>Rancho el Gavillero (Los Olivos)</option>
															<option>Rancho la Redonda</option>
															<option>Rancho los Dos Carnales</option>
															<option>Rancho los Guilles</option>
															<option>San José</option>
															<option>San Martín</option>
															<option>Familia Hurtado</option>
															<option>Acceso a Piedras Negras</option>
															<option>Ninguno [Blockera San Martín]</option>
															<option>Fracc. H. Ferrocarrileros</option>
															<option>Valvaco [Granja]</option>
															<option>José Apolinar Ramírez</option>
															<option>Juanita Godoy de Feregrino</option>
															<option>Kilómetro Cinco</option>
															<option>Rancho el Moral</option>
															<option>Rancho los Pericos</option>
															<option>Rancho San Agustín</option>
															<option>Familia Ríos</option>
															<option>Rancho Cactus</option>
															<option>Familia Dorantes Trejo (El Crucero)</option>
															<option>Ninguno [Carretera Querétaro Bernal Kilómetro 5.5]</option>
															<option>Mesa del Pino</option>
															<option>Rancho el Salitrillo</option>
															<option>Rancho los Celajes</option>
															<option>Sección Sur de Ajuchitlán</option>
															<option>La Enramada</option>
															<option>Rancho San José</option>
															<option>Campo deportivo Ajuchitlan</option>
															<option>Los Naranjos [Fraccionamiento]</option>
															<option>Rancho la Joya de los Quiotes</option>
															<option>Rancho Santa Martha</option>
															<option>Ampliación el Blanco</option>
															<option>La Nueva Esperanza</option>
															<option>El Puente (Familia Ávila Martínez)</option>
															<option>Familia Cabrera Arellano</option>
															<option>Familia Maldonado Pérez</option>
															<option>Familia Nicolás Bárcenas</option>
															<option>Familia Uribe Ugalde</option>
															<option>María Guadalupe Velázquez [Granja]</option>
															<option>Mesa del Rebaño</option>
															<option>Rancho Don Memo</option>
															<option>Rancho San Isidro (Luis Blanco Caballero)</option>
															<option>Antiguo Rastro de Pollos [Salida al Zamorano]</option>
															<option>Sección Sureste de Colón (Camino a la Pila)</option>
															<option>Sección Suroeste de Ajuchitlán</option>
															<option>Ninguno [Carretera a Tolimán Kilómetro 14]</option>
															<option>Familia Hernández Chindo</option>
															<option>La Ladera (Pedro Nicolás)</option>
															<option>Palmas</option>
															<option>Rancho el Toro Cachetón</option>
															<option>Rancho Purísima de Cubos</option>
															<option>Rancho San Ignacio [Centro Universitario SEICKOR]</option>
															<option>Ninguno</option>

                                                        </select>
                                                    </div>
                                                </div>
												<div class="form-group">
                                                    <label  class="col-sm-4 text-right">Comentarios:</label>
                                                    <div class="col-sm-6">
                                                         <textarea   class="form-control" rows="5" id="comment"></textarea>
													</div>
                                                </div>