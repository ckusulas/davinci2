
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>

       <?php $this->view('template/header.php'); ?>   
</head>

<body>

                  <!--Modal para agregado de Nuevos Programas -->
  <?php $this->view('template/mod_nuevo_programa'); ?>

    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                      <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                           <h1>
                                               
                                            </h1>
                                         </td>
                                        <td align="right"> 
                                             <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='resetearForma()'><i class="fa fa-floppy-o"></i> Limpiar</button> 
                                          <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='saveElement()'><i class="fa fa-floppy-o"></i> Editar</button> 
                                           <button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='deleteElement()'><i class="fa fa-floppy-o"></i> Nuevo</button> 

                                            <button type="button" class="btn btn-success waves-effect w-md waves-light m-b-15" id="btnGuardar" data-toggle="modal"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Minutas</i>
                                </li>
                                <li class="active">Registar Minutas</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
                <div class="row">



                
                
                 
                    <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>1. Datos de la Reunión</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#integrantesComite"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="integrantesComite" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_datosReunion">

 

                                       
 


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4  ">Lugar de Reunión:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="lugarReunion" name="lugarReunion"  >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4   ">Fecha de Reunión:</label>
                                                    <div class="col-sm-4" ">
                                                      <div class="input-group">   
                                                         <span class="input-group-addon"><i class="fa fa-calendar iconReunion"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaReunion" name="fechaReunion"  >
                                                    </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4  ">Municipio:</label>
                                                    <div class="col-sm-6" ">
                                                         <select class="form-control" id="municipio" name="municipio">
                                                            <option>Seleccione...</option>

                                                            

                                                            <?php
                                                                foreach ($listMunicipios as $row) {
                                                                    $selected = "";
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . strtoupper($row['municipio']) . "</option>\n";
                                                                }
                                                            ?>
                                                       
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-4  ">Motivo de la Reunión:</label>
                                                    <div class="col-sm-6" ">
                                                         <select class="form-control" id="municipio" name="municipio">
                                                            <option>Seleccione...</option>
                                                            <option>DARLE SEGUIMIENTO A LOS ASUNTOS ACORDADOS CON LOS BENEFICIARIOS</option>
                                                            <option>ENTREGA RECEPCIÓN DE LA OBRA, APOYO O SERVICIO</option>
                                                            <option>HACER RECOMENDACIONES PARA LAS ACTIVIDADES DE VIGILANCIA</option>
                                                            <option>OTRO</option>
                                                            <option>TRATAR ASUNTOS CON LOS BENEFICIARIOS</option>
                                                            <option>CONSTITUIR COMITES DE CONTRALORIA SOCIAL</option>
                                                            <option>ORIENTAR EN EL LLENADO DE INFORMES (ANTES CEDULAS)</option>
                                                            <option>RECOPILAR APARTADOS DE INFORMES Y/O INFORMES COMPLETOS (ANTES CÉDULAS E INFORMES ANUALES)</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">Se recibio alguna Queja o Denuncia:</label>
                                                    <div class="col-sm-6" ">
                                                        
                                                        <label class="radio-inline"><input type="radio" value="SI" name="recibioQueja">Si</label>
                                                        <label class="radio-inline"><input type="radio" value="NO" name="recibioQueja">No</label> 
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">Detalles de la Queja o Denuncia:</label>
                                                    <div class="col-sm-6" ">
                                                        <textarea   class="form-control" rows="4" id="detallesQueja" name="detallesQueja"></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4">Temas tratados en la Reunión:</label>
                                                    <div class="col-sm-6" ">
                                                        <textarea   class="form-control" rows="10" id="temasTratados" name="temasTratados"></textarea>
                                                    </div>
                                                </div>


                                               
                                                
                                                
                                                
                                            
  
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Documentos del Comite -->



                 <!-- Beneficiarios que asistieron -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>2. Minuta</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#funciones"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="funciones" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_beneficiario">
                                               
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">(*)Minuta:</label>
                                                    <div class="col-sm-6" ">
                                                       <input type="file" class=" input-sm" id="minutaFirmada" name="minutaFirmada"  >
                                                    </div>
                                                </div> 
                                                
                                                <input type="hidden" name="qtyBeneficiarios" id="qtyBeneficiarios" value=1>
                                                <input type="hidden" name="qtyIntegrantes" id="qtyIntegrantes" value=1>
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Beneficiarios que asistieron-->




                  <!-- Separador - Asignar Montos de la Obra ..-->
                 <div class="col-lg-12">

                        <div class="row">

                       
                            <div class="col-lg-12">

                                <div class="portlet portlet-blue">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4 class="text-center">Asistentes</h4> 
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#federal estatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                          
                                </div>
                                <!-- /.portlet -->
                            </div>
                            
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- Separador - Asignar montos de la obra.. -->

                    <!-- Datos del Comite -->
                    <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>3. Funcionarios que asistieron</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_funcionarios_asiste" action="#"> 

                                                  <div class="text-right">  <a href="#primero" onClick="agregarNuevoIntegrante()">[+] Agregar Funcionario</a></div><br>


                                                <div id="funcionario_1">
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-3 text-right ">FUNCIONARIO 1:</label>
                                                </div> 
                                          
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2   ">Instancia:</label>
                                                    <div class="col-sm-4">
                                                        <select id="instancia" name="instancia" >
                                                            <option>Seleccione...</option>
                                                            <option>Representación Federal</option>
                                                            <option>Ejecutora Estatal</option>
                                                            <option>Ejecutora Municipal</option>
                                                            <option>Ejecutora Otro</option>
                                                            <option>OEC</option>
                                                            <option>OIC</option>
                                                            <option>OTRO</option>

                                                        </select>
                                                    </div>


                                                    <label for="textInput" class="col-sm-1">Cargo:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control input-sm" id="cargo" name="cargo"  >
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                    </div>


                                                    <label for="textInput" class="col-sm-2">Minuta Firmada</label>
                                                    <div class="col-sm-3">

                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">No</label> 
                                                            

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Apellido Paterno:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Apellido Materno:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    
                                                </div>
                                       
                                                </div>
                                          









                                                <div id="funcionario_2">

                                                    <hr>


                                             <div class="text-right">  <a href="#primero" onClick="agregarNuevoIntegrante()">[+] Agregar Funcionario</a></div><br>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-3 text-right ">FUNCIONARIO 2:</label>
                                                </div> 
                                          
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2   ">Instancia:</label>
                                                    <div class="col-sm-4">
                                                        <select id="instancia" name="instancia" >
                                                            <option>Seleccione...</option>
                                                            <option>Representación Federal</option>
                                                            <option>Ejecutora Estatal</option>
                                                            <option>Ejecutora Municipal</option>
                                                            <option>Ejecutora Otro</option>
                                                            <option>OEC</option>
                                                            <option>OIC</option>
                                                            <option>OTRO</option>

                                                        </select>
                                                    </div>


                                                    <label for="textInput" class="col-sm-1">Cargo:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control input-sm" id="cargo" name="cargo"  >
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                    </div>


                                                    <label for="textInput" class="col-sm-2">Minuta Firmada</label>
                                                    <div class="col-sm-3">

                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">No</label> 
                                                            

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Apellido Paterno:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Apellido Materno:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    
                                                </div>
                                       





                                                </div>
 

                                           

 

    <br>















                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>               
                <!-- fin Datos del Comite -->


         



                 <!-- Beneficiarios que asistieron -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>4. Beneficiarios que asistieron</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#funciones"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="funciones" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_beneficiario">
                                                <div class="text-right">  <a href="#primero" onClick="agregarNuevoIntegrante()">[+] Agregar Beneficiario</a></div><br>


                                                <div id="funcionario_1">
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-3 text-right ">BENEFICIARIO 1:</label>
                                                    </div> 
                                              
                                           
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Nombre:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>


                                                        <label for="textInput" class="col-sm-2">Minuta Firmada</label>
                                                        <div class="col-sm-3">

                                                            <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">No</label> 
                                                                

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Apellido Paterno:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Apellido Materno:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        
                                                    </div>
                                       
                                                </div>





                                                <div id="funcionario_2">


                                                    <hr>


                                             <div class="text-right">  <a href="#primero" onClick="agregarNuevoIntegrante()">[+] Agregar Beneficiario</a></div><br>

                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-3 text-right ">BENEFICIARIO 2:</label>
                                                    </div> 
                                              
                                           
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Nombre:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>


                                                        <label for="textInput" class="col-sm-2">Minuta Firmada</label>
                                                        <div class="col-sm-3">

                                                            <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">No</label> 
                                                                

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Apellido Paterno:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Apellido Materno:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        
                                                    </div>
                                       
                                                </div>
                                          
                                                
                                                <input type="hidden" name="qtyBeneficiarios" id="qtyBeneficiarios" value=1>
                                                <input type="hidden" name="qtyIntegrantes" id="qtyIntegrantes" value=1>

                                                <br><br><br><br><br>
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Beneficiarios que asistieron-->





                 <!-- Funcion del Comite -->
                 <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-6">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>5. Comites que asistieron</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#preguntasClave"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="preguntasClave" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_integrantes">
                                                  <div class="text-right">  <a href="#primero" onClick="addIntegrante()">[+] Agregar Integrante</a></div><br>

                                            
                                                <div class="form-group">
                                                    <label  class="col-sm-3  col-sm-offset-1">Nombre Integrante 1:</label>
                                                    <div class="col-sm-6">
                                                       <input type="text" class="form-control input-sm" id="nombreIntegrante[]" name="nombreIntegrante[]"  >
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label  class="col-sm-3  col-sm-offset-1">Asistió:</label>
                                                    <div class="col-sm-6">
                                                         <label class="radio-inline"><input type="radio" value="SI" name="asistio1">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="asistio1">No</label>
                                                    </div>
                                                </div>

                                                <div id="tagIntegrante"></div>

                                                <hr>

                                                <div class="form-group">
                                                    <label  class="col-sm-3  col-sm-offset-1">Minuta Firmada:</label>
                                                    <input type="file" class=" input-sm" id="minutaFirmada" name="minutaFirmada"  >
                                                </div>

                                               
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-6 (nested) -->



                                 <!-- Basic Form Example -->
                            <div class="col-lg-6">

                                <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>6. Integrantes del Comité que Asistieron</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#preguntasClave"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="preguntasClave" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_integrantes">
                                                 

                                               

                                                <div id="funcionario_1">
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 1:</label>
                                                    </div> 
                                              
                                           
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Nombre:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>


                                                        <label for="textInput" class="col-sm-2">Asistió?</label>
                                                        <div class="col-sm-3">

                                                            <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">No</label> 
                                                                

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Apellido Paterno:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>


                                                        <label for="textInput" class="col-sm-2">Minuta Firmada</label>
                                                        <div class="col-sm-3">

                                                            <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">No</label> 
                                                                

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Apellido Materno:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        
                                                    </div>
                                       
                                                </div>




                                                      <div id="funcionario_2">
                                                        <hr>


                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 2:</label>
                                                    </div> 
                                              
                                           
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Nombre:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>


                                                        <label for="textInput" class="col-sm-2">Asistió?</label>
                                                        <div class="col-sm-3">

                                                            <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">No</label> 
                                                                

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Apellido Paterno:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>


                                                        <label for="textInput" class="col-sm-2">Minuta Firmada</label>
                                                        <div class="col-sm-3">

                                                            <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">Si</label>
                                                            <label class="radio-inline">
                                                                <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1">No</label> 
                                                                

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-2">Apellido Materno:</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control input-sm" id="responsable" name="responsable"  >
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        
                                                    </div>
                                       
                                                </div>
                                               
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                           
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del Comite -->

                     <!-- Integrante del Comite -->
               
 


            
                
             

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>

    <link href="<?php echo asset_url();?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <script src="<?php echo asset_url();?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  

      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   
    


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">


        var v_esquema = 0;

         $(document).ready(function () { 

          $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */


              if($("#programaSeleccionado").val() == "todos"){
            //alert("Debe seleccionar un Programa, antes de guardar Registro.");
            swal("Advertencia!", "Debe seleccionar un Programa, antes de guardar Registro.", "warning");
            $("#btnGuardar").fadeOut("slow");
          } 


          procesarCambio();

            changeProgram("undefined");

         
                $('#upload').on('click', function () {
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'registro_comite/upload_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });








           (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                monthsTitle: "Meses",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
            }(jQuery));



            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();


           


               $( function() {
            $( "#fechaReunion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
           
            
          } );



 //iconos
                $('.iconReunion').click(function() {
                    $("#fechaReunion").focus();
              });


            

          });    







         function sumaPeople(){
            
            var total = 0;

                 
                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                   //     alert("value:" + this.value);
                        total += parseFloat(this.value);
                    }
                }); 


                //total = parseFloat($("#total_people_womans").val()) + parseFloat($("#total_people_mans").val());

            $("#total_personas").val(total);




        }


        function changeProgram(checked){

              if(checked!="undefined"){
               
                swal({
                  title: "Estás seguro de querer cambiar Programa?",
                  text: "",
                  icon: "warning",
                  buttons: true,
                  dangerMode: false, 
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Listo!, Se cambio programa exitosamente", {
                      icon: "success",
                    });
                    //location.reload(true);

            procesarCambio();


                  } else {
                    swal("Lo dejamos como estaba..");
                  }
                });
            }
 
           
 
         
               
        }



        function procesarCambio(){
             var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }


                  if(id_programSelected!="todos"){
                  $("#btnGuardar").fadeIn("slow");
                }
              

               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            recurso = obj.recurso;
                            esquema = obj.esquema;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").fadeIn("slow");

                     if(recurso == 1){
                             $("#patcs_menu").hide();
                         $("#petcs_menu").show("slow");
                             $("#documentos_menu").show("slow");

                          $("#seguimiento_menu").hide("slow");
                     
                       

                             $("#reuniones_menu").show("slow");
                           $("#informes_menu").show("slow");
                             $("#formatos_menu").hide("slow");
                             $("#formatos_menu").hide("slow");
                    }

                    
                    v_esquema = 0;

                    if(esquema==1){
                        v_esquema = 1;
                    }
                     

                    if(recurso == 2 || v_esquema == 1){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow"); 
                            $("#documentos_menu").show("slow");
                        
                        $("#seguimiento_menu").show("slow");  
                       
                         $("#reuniones_menu").hide("slow");
                           $("#informes_menu").hide("slow");
                             $("#formatos_menu").show("slow");


                    }


    
                    }
                });

        }


        function guardar(){



                $.ajax({
                        url: "<?php echo site_url('registro_minutas/guardarFuncionariosAsiste') ?>",
                        type: "POST",
                        data: $('#form_funcionarios_asiste').serialize(),
                        dataType: "JSON",
                        success: function (data)
                        {
                            var status, message;
                            $.each(data, function (index, obj) {
                                status = obj.status;
                                message = obj.msg;
                            });

            
     
                        }
                });


                
                $.ajax({
                        url: "<?php echo site_url('registro_minutas/guardarDatosReunion') ?>",
                        type: "POST",
                        data: $('#form_datosReunion').serialize(),
                        dataType: "JSON",
                        success: function (data)
                        {
                            var status, message;
                            $.each(data, function (index, obj) {
                                status = obj.status;
                                message = obj.msg;
                            });

            
     
                        }
                });



                $.ajax({
                    url: "<?php echo site_url('registro_minutas/guardarBeneficiarios') ?>",
                    type: "POST",
                    data: $('#form_beneficiario').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


                $.ajax({
                    url: "<?php echo site_url('registro_minutas/guardarIntegrantes') ?>",
                    type: "POST",
                    data: $('#form_integrantes').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


               
 


            swal("Bien hecho!", "La  minuta ha sido guardada.", "success");
        }


         $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });


            function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");


                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   



    function addBeneficiario(){

        var cantidadBeneficiarios;
        cantidadBeneficiarios = $("#qtyBeneficiarios").val();

        cantidadBeneficiarios++;


        cad ="<div class='form-group'>"
        cad+="                                                 <label for='textInput' class='col-sm-4 text-right '>Nombre Beneficiario " + cantidadBeneficiarios + ":</label>";
        cad+="                                           <div class='col-sm-6'>";
        cad+= "                                         <input type='text' class='form-control input-sm' id='nombreBenefic[]' name='nombreBenefic[]'  >";
        cad+="                          </div>";
        cad+="   </div>";

                    



                    $("#tagBeneficiario").append(cad);

                    $("#qtyBeneficiarios").val(cantidadBeneficiarios);

    }



    function addIntegrante(){
     var cantidadIntegrantes;
        cantidadIntegrantes = $("#qtyIntegrantes").val();

        cantidadIntegrantes++;


              cad="      <div class='form-group'>";
              cad+="                                      <label  class='col-sm-3  col-sm-offset-1'>Nombre Integrante "+ cantidadIntegrantes +":</label>";
                          cad+="                          <div class='col-sm-6'>";
                                cad+="                       <input type='text' class='form-control input-sm' id='nombreIntegrante[]' name='nombreIntegrante[]'  >";
                                       cad+="             </div>";
                                       cad+="         </div>";
                                                
                                              cad+="  <div class='form-group'>";
                                                  cad+="  <label  class='col-sm-3  col-sm-offset-1'>Asistio:</label>";
                                                  cad+="  <div class='col-sm-6'>";
                                                     cad+="    <label class='radio-inline'><input type='radio' value='SI' name='asistio"+cantidadIntegrantes+"'>Si</label>";
                                                      cad+="  <label class='radio-inline'>";
                                                            cad+="<input type='radio' value='NO' name='asistio"+cantidadIntegrantes+"'>No</label>";
                                                   cad+=" </div>";
                                                cad+="</div>";



                    $("#tagIntegrante").append(cad);
                     $("#qtyIntegrantes").val(cantidadIntegrantes);
    }






    </script>

</body>

</html>
