
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>

       <?php $this->view('template/header.php'); ?>   
</head>

<body>

    <div id="wrapper">

                      <!--Modal para agregado de Nuevos Programas -->
  <?php $this->view('template/mod_nuevo_programa'); ?>

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                           <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                           <h1>
                                               
                                            </h1>
                                         </td>
                                        <td align="right"> 
                                              <!-- <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='resetearForma()'><i class="fa fa-floppy-o"></i> Limpiar</button> 
                                          <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='saveElement()'><i class="fa fa-floppy-o"></i> Editar</button> -->
                                           <button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='deleteElement()'><i class="fa fa-floppy-o"></i> Nuevo</button> 

                                            <button type="button" class="btn btn-success waves-effect w-md waves-light m-b-15" data-toggle="modal" id="btnGuardar"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Material</i>
                                </li>
                                <li class="active">Registar Material</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
                
                 

                    <!-- Datos del Comite -->
                    <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>1. Registrar Material</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_registrar_material_difusion" action="#">
                                                

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Tipo de Material:</label>
                                                    <div class="col-sm-3">
                                                        <select id="tipoMaterial" class="form-control" name="tipoMaterial">
                                                            <option value="">Seleccione..</option>
                                                            <option value="1">Difusión</option>
                                                            <option value="2">Capacitación</option>

                                                        </select>
                                                    </div>
                                                </div>
                   
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2 col-sm-2">Nombre del Material:</label>
                                                    <div class="col-sm-3">
                                                       <select class="form-control" id="catalogoMaterial" name="catalogoMaterial">
                                                            <option value="">Seleccione...</option>
                                                            <?php
                                                         
 
                                                               /* foreach ($editData['listLocalidades'] as $row) {
                                                                    $selected = "";

                                                                    if($editData['fk_localidad'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['localidad']) . "</option>\n";
                                                                }*/
                                                            
                                                            ?>

                                                        </select>
                                                    </div>

                                                    
                                                </div>
                                             
 <div class="form-group">
                                                 <label for="textInput" class="col-sm-2 ">Cantidad producida:</label>
                                                    <div class="col-sm-2" ">
                                                        <input type="text" class="form-control input-sm " id="cantidadProducida" name="cantidadProducida"  >
                                                    </div>
                                                    </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2  ">Ingrese Archivo del Material:</label>
                                                    <div class="col-sm-6">
                                                        <input type="file" class="input-sm" id="archivoMaterial" name="archivoMaterial"  >
                                                    </div>
                                                </div>

                                            
                                                 
                                               

                                       <br><br><br>

                                        

                                                <br>
                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>               
                <!-- fin Datos del Comite -->




                 <!-- Funcion del Comite -->
                
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del Comite -->






                  
 


            
                
             

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
 

      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   
    


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">


        var v_esquema = 0;

        var msgError = "";

         $(document).ready(function () {

          $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */

           if($("#programaSeleccionado").val() == "todos"){
            //alert("Debe seleccionar un Programa, antes de guardar Registro.");
            swal("Advertencia!", "Debe seleccionar un Programa, antes de guardar Registro.", "warning");
            $("#btnGuardar").fadeOut("slow");
          }


            procesarCambio();
            changeProgram("undefined");
            
                $('#upload').on('click', function () {
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'registro_comite/upload_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });



            $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });


           





             $.datepicker.regional['es'] = {
                  closeText: 'Cerrar',
                  prevText: '<Ant',
                  nextText: 'Sig>',
                  currentText: 'Hoy',
                  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                  monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                  dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                  dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                  weekHeader: 'Sm',
                  dateFormat: 'dd/mm/yy',
                  firstDay: 1,
                  isRTL: false,
                  showMonthAfterYear: false,
                  yearSuffix: ''
                };

                $.datepicker.setDefaults($.datepicker.regional['es']);



            $('#cantidadProducida').numeric();
            $('#cantidadDistribuir').numeric();
            

           


               $( function() {
            $( "#fechaDistribuicion" ).datepicker({ dateFormat: 'dd/mm/yy' });
           
            
          } );



            

          });    





        function changeProgram(checked){


             if(checked!="undefined"){
               
                swal({
                  title: "Estás seguro de querer cambiar Programa?",
                  text: "",
                  icon: "warning",
                  buttons: true,
                  dangerMode: false, 
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Listo!, Se cambio programa exitosamente", {
                      icon: "success",
                    });
                    //location.reload(true);

            procesarCambio();


                  } else {
                    swal("Lo dejamos como estaba..");
                  }
                });
            }
 
         
               
        }


        function procesarCambio(){
            var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }



              if(id_programSelected!="todos"){
                  $("#btnGuardar").fadeIn("slow");
                }


               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                               recurso = obj.recurso;
                            esquema = obj.esquema;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").fadeIn("slow");



                    if(recurso == 1){
                         $("#patcs_menu").hide();
                         $("#petcs_menu").show("slow");
                             $("#documentos_menu").show("slow");

                          $("#seguimiento_menu").hide("slow");

                           $("#reuniones_menu").show("slow");
                

                           $("#informes_menu").show("slow");
                             $("#formatos_menu").hide("slow");
                    }

                    
                    v_esquema = 0;

                    if(esquema==1){
                        v_esquema = 1;
                    }
                     

                    if(recurso == 2 || v_esquema == 1){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow");  
                            $("#documentos_menu").show("slow");
                        
                         $("#seguimiento_menu").show("slow");     

                          $("#reuniones_menu").hide("slow");
                

                           $("#informes_menu").hide("slow");
                             $("#formatos_menu").show("slow");
                    }


    
                    }
                });

        }

         function populate_material(tipoMaterial) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_materiales/populate_material') ?>/" + tipoMaterial,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogoMaterial').empty();
                        $('#catalogoMaterial').append("<option value=''>Seleccione Material..</option>");


                        $.each(data, function (i, name) {
                            $('#catalogoMaterial').append('<option value="' + data[i].id + '">' + data[i].campo + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
        }   


         function sumaPeople(){
            
            var total = 0;

                 
                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                   //     alert("value:" + this.value);
                        total += parseFloat(this.value);
                    }
                }); 


                //total = parseFloat($("#total_people_womans").val()) + parseFloat($("#total_people_mans").val());

            $("#total_personas").val(total);




        }




           function validaCamposMaterial(){


            msgError = "";
                    $("#cantidadProducida").css({"background-color": "white"});  
                    $("#catalogoMaterial").css({"background-color": "white"});        
                    $("#tipoMaterial").css({"background-color": "white"});        
                   

                


                 if ($("#tipoMaterial option:selected").val() == "") {
                       
                                msgError+= "- Debe seleccionar Tipo de Material, \n";
                                $("#tipoMaterial").css({"background-color": "yellow"});
                               
                        
                 }


                 if ($("#catalogoMaterial option:selected").val() == "") {
                       
                                msgError+= "- Debe seleccionar Nombre dle Material, \n";
                                $("#catalogoMaterial").css({"background-color": "yellow"});
                               
                        
                 }


             

                if( $("#cantidadProducida").val() == ""){
                    msgError+= "- Debe asignar la Cantidad Producida, \n";
                    $("#cantidadProducida").css({"background-color": "yellow"});
                }

 
 
               


        } 


        function guardar(){


            validaCamposMaterial();

      

             if(msgError!=""){
                    swal("Error!", msgError, "error");
                    return;
                }



            $.ajax({
                    url: "<?php echo site_url('registro_materiales/guardarMaterialDifusion') ?>",
                    type: "POST",
                    data: $('#form_registrar_material_difusion').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                }); 


            swal("Bien hecho!", "El Material ha sido guardado.", "success");
        }


         $("#tipoMaterial").change(function () {

                var tipoMaterialSelected = $("#tipoMaterial option:selected").val();     

                populate_material(tipoMaterialSelected);

            });


           






    </script>

</body>

</html>
