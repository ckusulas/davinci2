



<table id="example" class="display" class="col-lg-12" border=1 >
    <thead >
        <tr>
            <th colspan="5"  bgcolor="E1E1E1">
                REFERENCIA
            </th>

            <?php
            $width_obras_col = 10;
            if ($is_gerencia) {
                //add column Departamento
                $width_obras_col = 11;
            }
            ?>

            <th colspan="<?= $width_obras_col ?>"  bgcolor="E1E1E1">
                OBRA / APOYO / SERVICIO  <div id="cantidad_registros"><div>
            </th>
            <th colspan="4"   bgcolor="E1E1E1">
                COMITÉ
            </th>
            <th colspan="3"   bgcolor="E1E1E1">
                CAPACITACIÓN
            </th>
            <th colspan="3"   bgcolor="E1E1E1">
                MATERIAL
            </th>
             <th class="tdSeguimiento" colspan="2"   bgcolor="E1E1E1">
                SEGUIMIENTO
            </th>
            </tr>
            <tr>
                <th>Folio</th>
                <th>Ej.Fiscal</th>
                <th>Recurso</th>
                <th>Programa</th>
<?php
if ($is_gerencia) {
    ?>
                    <th>Departamento</th>
    <?php
}
?>

                <th>Tipo de Beneficio</th>         
                <th> Nombre de Proyecto </th>
                <th>Estatus Obra</th>

                <th>Inst.Ejecutora</th>


                <th>Municipio</th>
                <th>Localidad</th>

                <th>Monto Total Asignado</th>
                <th>Monto Total Ejecutado</th>

                <th>Total Beneficiados</th>
                <th>Fecha de Inicio Programada</th>
                <th>Fecha Final Programada</th>


                <th> Nombre del Comité </th>
                <th>Localidad del Comité</th>
                <th>Fecha de Registro</th>

              <!--  <th>Clave de Registro</th> -->

                <th>Integrantes del Comité</th>


                <th> Nombre Capacitación </th>
                <th>Figura Capacitada</th>
                <th>Fecha de Impartición</th>

                <th>Archivo de Material</th>


                <th>Cantidad Distribuida</th>

                <th>Fecha de Asignación</th>
                <th class="tdSeguimiento">No de Seguimiento</th>
                <th class="tdSeguimiento">Observaciones del Comité</th>

            </tr>
            </thead>
            <tbody>

<?php
$counter = 1;
$tabla = "";
foreach ($listProyectos as $row) {




    $bandDataStatus = "Iniciado";
    $date_now = new DateTime();
    $date_final_obra = new DateTime($row["fecha_final_programada"]);

    if ($date_now > $date_final_obra) {
        $bandDataStatus = "Terminado";
    }


    $this->db->set('estatus_proyecto', $bandDataStatus);
    $this->db->where('id', $row["id"]);
    $this->db->update('cs_registro_proyecto');


    $linkFFederalFirst = "<a href='#' class='textBox' onClick='viewFFederal(" . $row['fk_programa_recurso'] . ")'>";
    $linkFFederalSecond = "</a>";

    $tag_admin = "";
    if ($is_gerencia) {
        $tag_admin = "<td>" .$row["id_depto"] . ". " . $row["departamento"] . "</td>";
    }


    $tabla = "<tr>";
    $tabla.= "<td>" . $counter . "</td>";
    $tabla.= "<td>" . "2018" . "</td>";
    $tabla.= "<td>" . $row["recurso"] . "</td>";
    $tabla.= "<td>" . $row["programa"] . "</td>";
    $tabla.= $tag_admin;

    $linkObraFirst = "<a href='#' class='textBox' onClick='viewProy(" . $row['id'] . "," . $row['fk_program'] . ")'>";
    $linkObraSecond = "</a>";

    $linkComiteFirst = "<a href='#' class='textBox' onClick='viewComite(" . $row['fk_comite'] . "," . $row['id'] . "," . $row['fk_program'] . ")'>";
    $linkComiteSecond = "</a>";


    $linkCapacitacionFirst = "<a href='#' class='textBox' onClick='viewCapacitacion(" . $row['fk_capacitacion'] . "," . $row['id'] . "," . $row['fk_program'] . ")'>";
    $linkCapacitacionSecond = "</a>";

    $tabla.= "<td>" . $row["beneficio"] . "</td>";

    $tabla.= "<td><span>" . $linkObraFirst . $row["nombre_proyecto"] . $linkObraSecond . "</span></td>";

    $tabla.= "<td>" . $bandDataStatus . "</td>";
    $tabla.= "<td>" . $row["instancia_ejecutora"] . "</td>";



    $tabla.= "<td>" . $row["municipio"] . "</td>";
    $tabla.= "<td>" . $row["localidad"] . "</td>";
    $tabla.= "<td>$" . number_format($row["recurso_asignado_vigilar"]) . "</td>";
    $tabla.= "<td>$" . number_format($row["recurso_ejecutado_vigilado"]) . "</td>";
    $tabla.= "<td>" . number_format($row["total_beneficiados"]) . "</td>";
    $tabla.= "<td>" . $row["fecha_inicio_programada"] . "</td>";
    $tabla.= "<td>" . $row["fecha_final_programada"] . "</td>";



    $tabla.= "<td>" . $linkComiteFirst . $row["nombre_comite"] . $linkComiteSecond . "</td>";
    $tabla.= "<td>" . $row["localidad_comite"] . "</td>";
    $tabla.= "<td>" . $row["fecha_constitucion"] . "</td>";
    //  $tabla.= "<td>".$linkComiteFirst.$row["clave_registro"]."</td>";


    $tabla.= "<td>" . $row["totbeneficiarios"] . "</td>";

    $tabla.= "<td>" . $linkCapacitacionFirst . $row["nombre_capacitacion"] . $linkCapacitacionSecond . "</td>";
    $tabla.= "<td>" . $row["figura_capacitada"] . "</td>";
    $tabla.= "<td>" . $row["fecha_imparticion"] . "</td>";


    $tabla.= "<td>Archivo</td>";


    $tabla.= "<td>CantidadDistribuida</td>";

    $tabla.= "<td>FechaAsignación</td>";
    $tabla.= "<td class='tdSeguimiento'>#</td>";
    $tabla.= "<td class='tdSeguimiento'>Observacion...</td>";


    echo $tabla;
    ?>





                    </tr>








    <?php
    $counter++;
}
?>


<?php
$tag_filter = "";
if ($is_gerencia) {
    $tag_filter = "<th>Departamento</th>";
}
?>
            </tbody>

            </table>

            


            <script type="text/javascript">


                //solucion colores columnas https://jsfiddle.net/986capby/1/


                $(document).ready(function () {



                    $('#example').scroll(function() {
                        if ( $(".fixedHeader-floating").is(":visible") ) {
                            $(".fixedHeader-floating").scrollLeft( $(this).scrollLeft() );
                        }
                    });


                    $('#example tfoot th').each(function () {
                        var title = $(this).text();
                        $(this).html('<input type="text" placeholder="' + title + '" />');
                    });


                    var tableProy = $('#example').DataTable({
                        language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },

                        scrollY:        "600px",
                        scrollX:        false,
                        scrollCollapse: true,
                        paging:         false,
                        columnDefs: [
                            { width: 300, targets: 0 }
                        ],
  
                        //fixedColumns: true,
                                        
                         searchHighlight: true,
                        
                        fixedHeader: true,
                      //  FixedColumns: true,
                        //scrollX: true,
                       // lengthChange: false,
                        dom: 'T<"clear">lfrtip',
                        bAutoWidth: false,
                        //columnDefs: [{ "width": "200px", "targets": 10 }],
                        paging: false,
                        info: true,
                        buttons: [
                            {
                                extend: 'excelHtml5',
                                text: '<i class="fa fa-file-excel-o"></i>',
                                titleAttr: 'Excel',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                text: '<i class="fa fa-file-pdf-o"></i>',
                                titleAttr: 'PDF',
                                exportOptions: {
                                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                                }
                            }
                        ],
                    });

           


                     tableProy.on( 'draw', function () {
                        var body = $( table.table().body() );
                 
                        body.unhighlight();
                        body.highlight( table.search() );  
                    } );







                });


                function viewProy(id_proyecto, id_programa)
                {

                    $('#form_indicadores').attr('action', '<?= site_url() ?>/registro_proyecto');

                    $("#id_proyecto").val(id_proyecto);

                    $("#seleccPgma").val(id_programa);

                    $("#method").val("view");

                    $("#form_indicadores").submit();




                }



                function viewFFederal(id_federal)
                {

                    $('#form_indicadores').attr('action', '<?= site_url() ?>/registro_ffinanciamiento');

                    $("#id_ffinanciamiento").val(id_federal);

                    $("#method").val("view");

                    $("#form_indicadores").submit();




                }



                function viewComite(id_comite, id_proyecto, id_programa)
                {


                    $('#form_indicadores').attr('action', '<?= site_url() ?>/registro_comite');

                    $("#id_comite").val(id_comite);
                    $("#id_proyecto").val(id_proyecto);

                    $("#seleccPgma").val(id_programa);

                    $("#method").val("view");

                    $("#form_indicadores").submit();




                }



                function viewCapacitacion(id_capacitacion, id_proyecto, id_programa)
                {



                    $("#id_capacitacion").val(id_capacitacion);
                    $("#id_proyecto").val(id_proyecto);

                    $("#seleccPgma").val(id_programa);


                    $("#method").val("view");



                    $('#form_indicadores').attr('action', '<?= site_url() ?>/registro_capacitacion');

                    $("#form_indicadores").submit();



                }




            </script>