
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>

      
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
</head>

<body>

       <?php $this->view('template/header.php'); ?>   
       <?php $this->view('template/mod_nuevo_programa'); ?>
     <div id="wrapper">

       



    
        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                           <h1>
                                               
                                            </h1>
                                         </td>
                                        <td align="right"> 

                                         
            <!--

                                               <button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='cleanElement()'><i class="fa fa-floppy-o"></i> Nuevo</button> -->
                                            

                                          
                                             <?php
                                             if($editData["metodo"]=="view") {
                                            ?>

                                          <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='editar()'><i class="fa fa-floppy-o"></i> Editar</button>  
  <?
                                            }
                                            ?>
                                


                                                   <button type="button" id="btnGuardar" class="btn btn-success waves-effect w-md waves-light m-b-15" data-toggle="modal" id="btnGuardar"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button> 

                                                   <a href="#" target="_blank" class="btn btn-info waves-effect w-md waves-light m-b-15" data-toggle="modal" onClick="imprimirFormato()"><i class="fa fa-floppy-o"></i> Imprimir</a>
                                                 
                                         
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Comités</i>
                                </li>
                                <li class="active">Registar Comité</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>
                        <?php
                            $totalIntegrantes = intval($editData['qty_hombres']) + intval($editData['qty_mujeres']);

                        ?>

                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                            <input type="hidden" name="msgError" id="msgError">
                                  <input type="hidden" name="countResourceIntegrantes" id="countResourceIntegrantes" value="<?=$totalIntegrantes?>">
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

               

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
                
                
                 
                

                    <!-- Datos del comité -->
                    <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Datos del Comité</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_datos_comité" action="#">

 
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">Nombre del Comité:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" id="nombreComite" name="nombreComite" value="<?php echo $editData['nombre_comite']?>" onBlur="" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5  ">Fecha de Constitución:</label>
                                                    <div class="col-sm-4">
                                                           <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar"></i>
                                                    </span> 
                                                        <input type="text"  style="text-align:right" class="form-control input-sm" id="fechaConstitucion" name="fechaConstitucion"  value="<?php echo $editData['fecha_constitucion']?>"  >
                                                    </div></div>
                                                </div>
                                               <!-- <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">Clave del Registro:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-/#\s]+$/g,'');" id="claveRegistro" name="claveRegistro" value="<?php echo $editData['clave_registro']?>" >
                                                    </div>
                                                </div>-->
                                       

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">Accion de Conexión:</label>
                                                    <div class="col-sm-6" ">
                                                      
                                                        <select id="listObras" name="accionConexion[]" class="col-sm-5" style="text-overflow:ellipsis" onChange="populate_localidad(this.value)">
                                                       
                                                            
                                                            <?php 
                                                            if(strlen($editData['accion_conexion'])>0){
                                                                $listA = explode(",",$editData['accion_conexion']);
                                                            }


                                                                  




                                                            if(count($listObras)>0){ 
                                                            echo "<optgroup label='Obras'>";

                                                                
                                                               
                                                                foreach($listObras as $row){
                                                                    $selected = "";
                                                                    if(in_array($row['id'],$listA)){
                                                                        $selected = "selected";

                                                                    }

                                                                    $title = $row["nombre_proyecto"];
                                                                    $subtitle = substr($title, 0,112);

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."...</option>";
                                                                }

                                                                echo "</optgroup>";
                                                            }
                                                            ?>

                                                            <?php 
                                                            if(count($listApoyos)>0){ 
                                                            echo "<optgroup label='Apoyos'>";
                                                                 
                                                                foreach($listApoyos as $row){
                                                                    $selected = "";
                                                                   if(in_array($row['id'],$listA)){
                                                                        $selected = "selected";

                                                                    }

                                                                   
                                                                    $title = $row["nombre_proyecto"];
                                                                    $subtitle = substr($title, 0,112);

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."...</option>";
                                                                }

                                                                echo "</optgroup>";
                                                            }
                                                            ?>
                                                            
                                                            <?php 
                                                            if(count($listServicios)>0){ 
                                                            echo "<optgroup label='Servicios'>";
                                                                
                                                                foreach($listServicios as $row){
                                                                    $selected = "";
                                                                    if(in_array($row['id'],$listA)){
                                                                        $selected = "selected";

                                                                    }

                                                                   
                                                                    $title = $row["nombre_proyecto"];
                                                                    $subtitle = substr($title, 0,112);

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."...</option>";
                                                                }

                                                                echo "</optgroup>";
                                                            }
                                                            ?>
                                                        </select>

                                                    </div>
                                                </div>


                                                  <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">Localidad:</label>
                                                    <div class="col-sm-6">
                                                            <select class="form-control" id="localidad" name="localidad2">
                                                            <option value="">Seleccione...</option>
                                                            <?php
                                                         
                                                                if(isset($editData['listLocalidades']) && count($editData['listLocalidades'])>0) {
                                                                foreach ($editData['listLocalidades'] as $row) {
                                                                    $selected = "";

                                                                    if($editData['fk_localidad'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['localidad']) . "</option>\n";
                                                                }

                                                                }
                                                            
                                                            ?>

                                                        </select>
                                                    </div>
                                                </div>




                                                <input type="hidden" id="metodo" name="metodo" value="<?php echo $editData['metodo']?>">
                                                <input type="hidden" id="id_comite" name="id_comite" value="<?php echo $editData['id_comite'] ?>" >

                                                <br><br><br>
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>               
                <!-- fin Datos del comité -->




         










 <!-- Documentos del comité -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Documentos del comité</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#documentoscomité"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="documentoscomité" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_documentoscomite">



                                        


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">Servidor Público que Emite La Constancia:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="servidorPublicoNombre" name="servidorPublicoNombre" value="<?php echo $editData['servidor_publico_constancia'] ?>" >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">Cargo del Servidor Público:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="servidorPublicoCargo" name="servidorPublicoCargo" value="<?php echo $editData['cargo_servidor_publico'] ?>"  >


                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">Acta de Asamblea:</label>
                                                    <div class="col-sm-6" ">

                                                         <input type="file"   id="actaAsamblea" name="actaAsamblea"  >

                                                              
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">Constancia Firmada:</label>
                                                       <div class="col-sm-6">

                                                         <input type="file"   id="constanciaFirmada" name="constanciaFirmada"  >
  
                                                            
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label  class="col-sm-5 ">Escrito Libre Adjuntos:</label>
                                                    <div class="col-sm-6">
                                                         <input type="file"   id="escritoLibre" name="escritoLibre"  >
                                                         
                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <label  class="col-sm-5 ">Comentarios:</label>
                                                    <div class="col-sm-6">
                                                         <textarea   class="form-control" rows="3" id="comentarioComite" name="comentarioComite"><?php echo $editData['comentarios'] ?></textarea>
                                                    </div>
                                                </div>
                                                <br>

                                      
                                                    <input type="hidden" name="id_comite_documentos" id="id_comite_documentos" value="<?php echo $editData['id_comite'] ?>" >
                                                
                                        

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Documentos del comité -->


                 <!-- Hoverable Responsive Table -->
                         <div class="col-lg-12"  style="display:none" id="modulo_preguntas_funciones">
                        <div class="portlet portlet-blue">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Funciones</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <form id="form_funciones">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                
                                                <th>Funciones que se aplicaron en el comité (puede escoger mas de 1)</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php

                                            $counter = 1;
                                                      foreach($listComiteFunciones as $row) {
                                                                    $selected = "";

                                                            ?>
                                                            <tr> 

                                                                 <td style="vertical-align:middle">
                                                                    <?php



                                                                    $acentos = array("Ó", "Á", "Í", "Ú");
                                                                    $validos = array("ó", "a", "í", "ú");

                                                                    $funcion = ucwords(strtolower($row['campo']));
                                                                    $funcion = str_replace($acentos,$validos, $funcion);
                                                                    echo $counter.".- " . $funcion;

                                                                    $checked="";

                                                                    $datosA = explode(",",$editData['funciones_comite']);
                                                    
                                                                    if(in_array($row['id'],$datosA)){
                                                                        $checked = "checked";

                                                                    }

                                                                    ?>

                                                                </td>
                                                           

                                                                <td>
                                                                                                                                   

                                                               
                                                                        <label>
                                                                    <input type="checkbox" id="elem_<?php echo $row['id'] ?>" name="funcionesComite[]" value="<?php echo $row['id']?>" <?php echo $checked ?> />  </label>
                                                                        
                                                                </td>

                                                             
                                                               


                                                            </tr>
                                                            <?php
                                                            $counter++;


                                                            }
                                                            ?>
                                           
                                        </tbody>
                                    </table>
                                </div>

                                   <input type="hidden" name="id_comite_funciones" id="id_comite_funciones" value="<?php echo $editData['id_comite'] ?>" >
                             
                                </form>
                            </div>

                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-6 -->
                



                 <!-- Funcion del comité -->
                 <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Preguntas Clave</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#preguntasClave"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                            <div id="preguntasClave" class="panel-collapse collapse in">
                                            <div class="portlet-body">
                                            <form class="form-horizontal" id="form_preguntasClave">




                                                <div class="form-group">

                                                     <label  class="col-sm-8">1. Se proporciono resumen ejecutivo de obra apoyo o servicio?:</label>
                                                   
                                                     <div class="col-sm-3">

                                                          <?php 
                                                            $selected = "";
                                                            if($editData['proporciono_resumen']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>


                                                        <label class="radio-inline"><input type="radio" value="SI" name="proporcionoResumenEjecutivo" <?php echo $selected?> >Si</label>

                                                        <?php 
                                                            $selected = "";
                                                            if($editData['proporciono_resumen']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="NO" name="proporcionoResumenEjecutivo" <?php echo $selected ?>">No</label> 
                                                            
                                                    
                                                    </div>
                                                   
                                                </div>


 
 
                                      
 

                                                
                                                 <div class="form-group">

                                                       <label  class="col-sm-8">2. ¿Se pusieron a disposición de los beneficiarios, mecanismos para la atención de Quejas y Denuncias?:</label>
                                                    <div class="col-sm-3">


                                                        <?php 
                                                            $selected = "";
                                                            if($editData['disposicion_quejas']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="SI" name="disposicionQuejas" <?php echo $selected ?> >Si</label>

                                                              <?php 
                                                            $selected = "";
                                                            if($editData['disposicion_quejas']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="disposicionQuejas" <?php echo $selected ?>>No</label> 
                                                            
                                                    
                                                 
                                                    </div>
                                                 
                                                    
                                                </div>




                                               
                                            
                                             <input type="hidden" name="id_comite_preguntasclave" id="id_comite_preguntasclave">
                                               

                                                
                                               
                                            </form>
                                        </div>
                                    </div>



                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del comité -->









                     <!-- Division para Integrantes -->
                 <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4 class="text-center">
                                            Registrar datos de Integrantes del Comité
                                                </h4> 
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#federal estatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                          
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>









        <!-- Funcion del comité -->
                




                     <!-- Integrante del comité -->
                 <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Integrantes</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#integrantescomité"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="integrantescomité" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_integrante1">


 <div class="text-right"> <!-- <a href="#primero" onClick="addRecourse()">[+] Agregar nuevo Integrante</a> --></div>
                                                <div class="text-right">  <a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a></div><br>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 1:</label>
                                            </div> 

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante1" name="nombreIntegrante[]" value="<?php echo $editData['listIntegrantes'][0]['nombre_integrante'] ?>" >
                                                    </div>

                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante1" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][0]['cargo_integrante']=="Presidente"){
                                                                $selected = "selected";
                                                            }
                                                            ?>

                                                    <option value="Presidente" <?php echo $selected?>>Presidente</option>

                                                          <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][0]['cargo_integrante']=="Tesorero"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Tesorero" <?php echo $selected?>>Tesorero</option>

                                                        

                                                        <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][0]['cargo_integrante']=="Secretario"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Secretario" <?php echo $selected?>>Secretario</option>
                                                    
                                                         <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][0]['cargo_integrante']=="Vocal"){
                                                                $selected = "selected";
                                                            }
                                                            ?>



                                                    <option value="Vocal" <?php echo $selected?>>Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]" value="<?php echo $editData['listIntegrantes'][0]['appaterno_integrante'] ?>"   >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" ">
                                                       <label class="radio-inline">
                                                        <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][0]['firma_constancia_registro_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <input type="radio" value="SI" id="firmaRegistroConstancia1" name="firmaRegistroConstancia1" <?php echo $checked ?>>Si</label>

                                                         <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][0]['firma_constancia_registro_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia1" name="firmaRegistroConstancia1" <?php echo $checked ?>>No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]" value="<?php echo $editData['listIntegrantes'][0]['apmaterno_integrante'] ?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">

                                                          <?php
                                                            $checked = "";
                                                            $si_conocido = 0;

                                                            if($editData['listIntegrantes'][0]['domicilio_conocido_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                                $si_conocido = 1;
                                                            }

                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1"  <?php echo $checked ?>>Si</label>

                                                          <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][0]['domicilio_conocido_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1"  <?php echo $checked ?>>No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">


                                            <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][0]['sexo_integrante']=="Femenino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" name="sexoIntegrante1" <?php echo $checked ?>>Femenino</label>
                                                        <label class="radio-inline">
                                            

                                             <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][0]['sexo_integrante']=="Masculino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante1" name="sexoIntegrante1" <?php echo $checked ?>>Masculino</label> 
                                                            
                                                    </div>

                                                    <?php
                                                    $disabled = "";
                                                    if($si_conocido){
                                                        $disabled = "disabled";
                                                    }
                                                    ?>

                                                <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante1" name="calleIntegrante[]"      value="<?php echo $editData['listIntegrantes'][0]['calle_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]" value="<?php echo $editData['listIntegrantes'][0]['edad_integrante'] ?>"   >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante1" name="numeroIntegrante[]"  value="<?php echo $editData['listIntegrantes'][0]['numero_integrante'] ?>" <?=$disabled?>  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante1" name="coloniaIntegrante[]"   value="<?php echo $editData['listIntegrantes'][0]['colonia_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante1" name="codigoPostalIntegrante[]"  value="<?php echo $editData['listIntegrantes'][0]['cpostal_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Teléfono:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante1" name="telefonoIntegrante[]"  value="<?php echo $editData['listIntegrantes'][0]['telefono_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                    
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             


                                                
                                    <?php
                                    $display = "style='display:none'";
                                   
                                    if(trim($editData['listIntegrantes'][1]['nombre_integrante'])!="" ||                                    
                                    trim($editData['listIntegrantes'][1]['appaterno_integrante'])!=""){

                                        $display = "style='display:block'";
                                    }

                                    ?>

                                      <div id="view_integrante2" <?php echo $display ?> >   

                                            <hr>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 2:</label>
                                            </div> 
                                                <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(2)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante2" name="nombreIntegrante[]" value="<?php echo $editData['listIntegrantes'][1]['nombre_integrante'] ?>" >
                                                    </div>

                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante2" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][1]['cargo_integrante']=="Presidente"){
                                                                $selected = "selected";
                                                            }
                                                            ?>

                                                    <option value="Presidente" <?php echo $selected?>>Presidente</option>

                                                          <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][1]['cargo_integrante']=="Tesorero"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Tesorero" <?php echo $selected?>>Tesorero</option>

                                               

                                                        <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][1]['cargo_integrante']=="Secretario"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Secretario" <?php echo $selected?>>Secretario</option>
                                                 
                                                         <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][1]['cargo_integrante']=="Vocal"){
                                                                $selected = "selected";
                                                            }
                                                            ?>



                                                    <option value="Vocal" <?php echo $selected?>>Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]" value="<?php echo $editData['listIntegrantes'][1]['appaterno_integrante'] ?>"   >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" ">
                                                       <label class="radio-inline">
                                                        <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][1]['firma_constancia_registro_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <input type="radio" value="SI" id="firmaRegistroConstancia2" name="firmaRegistroConstancia2" <?php echo $checked ?>>Si</label>

                                                         <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][1]['firma_constancia_registro_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia2" name="firmaRegistroConstancia2" <?php echo $checked ?>>No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]" value="<?php echo $editData['listIntegrantes'][1]['apmaterno_integrante'] ?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">

                                                          <?php
                                                            $checked = "";
                                                            $si_conocido = 0;

                                                            if($editData['listIntegrantes'][1]['domicilio_conocido_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                                $si_conocido = 1;
                                                            }

                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante2" name="domicilioCortejadoIntegrante2"  <?php echo $checked ?>>Si</label>

                                                          <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][1]['domicilio_conocido_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante2" name="domicilioCortejadoIntegrante2"  <?php echo $checked ?>>No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">


                                            <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][1]['sexo_integrante']=="Femenino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" name="sexoIntegrante2" <?php echo $checked ?>>Femenino</label>
                                                        <label class="radio-inline">
                                            

                                             <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][1]['sexo_integrante']=="Masculino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante2" name="sexoIntegrante2" <?php echo $checked ?>>Masculino</label> 
                                                            
                                                    </div>
                                                    <?php
                                                    $disabled = "";
                                                    if($si_conocido){
                                                        $disabled = "disabled";
                                                    }
                                                    ?>
                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante2" name="calleIntegrante[]"      value="<?php echo $editData['listIntegrantes'][1]['calle_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]" value="<?php echo $editData['listIntegrantes'][1]['edad_integrante'] ?>"   >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante2" name="numeroIntegrante[]"  value="<?php echo $editData['listIntegrantes'][1]['numero_integrante'] ?>"  <?=$disabled?> >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante2" name="coloniaIntegrante[]"   value="<?php echo $editData['listIntegrantes'][1]['colonia_integrante'] ?>"  <?=$disabled?> >
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante2" name="codigoPostalIntegrante[]"  value="<?php echo $editData['listIntegrantes'][1]['cpostal_integrante'] ?>"  <?=$disabled?>>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Teléfono:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante2" name="telefonoIntegrante[]"  value="<?php echo $editData['listIntegrantes'][1]['telefono_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                    
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             


                                               
                                                
                                             

                                        </div>

                                    <?php
                                            $display = "style='display:none'";
                                       
                                        if(trim($editData['listIntegrantes'][2]['nombre_integrante'])!=="" ||                                     
                                        trim($editData['listIntegrantes'][2]['appaterno_integrante'])!==""){

                                            $display = "style='display:block'";
                                        }

                                      
                                    ?>



                                        <div id="view_integrante3" <?php echo $display ?> >   

                                            <hr>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 3:</label>
                                            </div> 

                                                <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(3)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante3" name="nombreIntegrante[]" value="<?php echo $editData['listIntegrantes'][2]['nombre_integrante'] ?>" >
                                                    </div>

                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante3" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][2]['cargo_integrante']=="Presidente"){
                                                                $selected = "selected";
                                                            }
                                                            ?>

                                                    <option value="Presidente" <?php echo $selected?>>Presidente</option>

                                                          <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][2]['cargo_integrante']=="Tesorero"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Tesorero" <?php echo $selected?>>Tesorero</option>
 

                                                        <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][2]['cargo_integrante']=="Secretario"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Secretario" <?php echo $selected?>>Secretario</option>
                                                           
                                                         <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][2]['cargo_integrante']=="Vocal"){
                                                                $selected = "selected";
                                                            }
                                                            ?>



                                                    <option value="Vocal" <?php echo $selected?>>Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]" value="<?php echo $editData['listIntegrantes'][2]['appaterno_integrante'] ?>"   >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" ">
                                                       <label class="radio-inline">
                                                        <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][2]['firma_constancia_registro_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <input type="radio" value="SI" id="firmaRegistroConstancia3" name="firmaRegistroConstancia3" <?php echo $checked ?>>Si</label>

                                                         <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][2]['firma_constancia_registro_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia3" name="firmaRegistroConstancia3" <?php echo $checked ?>>No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]" value="<?php echo $editData['listIntegrantes'][2]['apmaterno_integrante'] ?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">

                                                          <?php
                                                            $checked = "";
                                                            $si_conocido = 0;

                                                            if($editData['listIntegrantes'][2]['domicilio_conocido_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                                $si_conocido = 1;
                                                            }

                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante3" name="domicilioCortejadoIntegrante3"  <?php echo $checked ?>>Si</label>

                                                          <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][2]['domicilio_conocido_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante3" name="domicilioCortejadoIntegrante3"  <?php echo $checked ?>>No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">


                                            <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][2]['sexo_integrante']=="Femenino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" name="sexoIntegrante3" <?php echo $checked ?>>Femenino</label>
                                                        <label class="radio-inline">
                                            

                                             <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][2]['sexo_integrante']=="Masculino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante3" name="sexoIntegrante3" <?php echo $checked ?>>Masculino</label> 
                                                            
                                                    </div>


                                                  <?php
                                                    $disabled = "";
                                                    if($si_conocido){
                                                        $disabled = "disabled";
                                                    }
                                                    ?>
                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante3" name="calleIntegrante[]"      value="<?php echo $editData['listIntegrantes'][2]['calle_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]" value="<?php echo $editData['listIntegrantes'][2]['edad_integrante'] ?>"   >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante3" name="numeroIntegrante[]"  value="<?php echo $editData['listIntegrantes'][2]['numero_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante3" name="coloniaIntegrante[]"   value="<?php echo $editData['listIntegrantes'][2]['colonia_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante3" name="codigoPostalIntegrante[]"  value="<?php echo $editData['listIntegrantes'][2]['cpostal_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Teléfono:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante3" name="telefonoIntegrante[]"  value="<?php echo $editData['listIntegrantes'][2]['telefono_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                    
                                                </div>
                                                
                                                

                                                
                                                <br><br> 
                                                
                                             


                                               
                                                
                                             

                                        </div>

                                    


                                    <?php
                                            $display = "style='display:none'";
                                       
                                        if(trim($editData['listIntegrantes'][3]['nombre_integrante'])!=="" || trim($editData['listIntegrantes'][3]['appaterno_integrante'])!==""){

                                            $display = "style='display:block'";
                                        }

                                      
                                    ?>



                                        <div id="view_integrante4" <?php echo $display ?> >   

                                            <hr>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 4:</label>
                                            </div> 

                                              <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(4)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante4" name="nombreIntegrante[]" value="<?php echo $editData['listIntegrantes'][3]['nombre_integrante'] ?>" >
                                                    </div>

                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante4" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][3]['cargo_integrante']=="Presidente"){
                                                                $selected = "selected";
                                                            }
                                                            ?>

                                                    <option value="Presidente" <?php echo $selected?>>Presidente</option>

                                                          <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][3]['cargo_integrante']=="Tesorero"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Tesorero" <?php echo $selected?>>Tesorero</option>

                                              

                                                        <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][3]['cargo_integrante']=="Secretario"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Secretario" <?php echo $selected?>>Secretario</option>
                                                       
                                                         <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][3]['cargo_integrante']=="Vocal"){
                                                                $selected = "selected";
                                                            }
                                                            ?>



                                                    <option value="Vocal" <?php echo $selected?>>Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]" value="<?php echo $editData['listIntegrantes'][3]['appaterno_integrante'] ?>"   >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" ">
                                                       <label class="radio-inline">
                                                        <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][3]['firma_constancia_registro_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <input type="radio" value="SI" id="firmaRegistroConstancia4" name="firmaRegistroConstancia4" <?php echo $checked ?>>Si</label>

                                                         <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][3]['firma_constancia_registro_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia4" name="firmaRegistroConstancia4" <?php echo $checked ?>>No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]" value="<?php echo $editData['listIntegrantes'][3]['apmaterno_integrante'] ?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">

                                                          <?php
                                                            $checked = "";
                                                            $si_conocido = 0;

                                                            if($editData['listIntegrantes'][3]['domicilio_conocido_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                                $si_conocido = 1;
                                                            }

                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante4" name="domicilioCortejadoIntegrante4"  <?php echo $checked ?>>Si</label>

                                                          <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][3]['domicilio_conocido_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante4" name="domicilioCortejadoIntegrante4"  <?php echo $checked ?>>No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">


                                            <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][3]['sexo_integrante']=="Femenino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" name="sexoIntegrante4" <?php echo $checked ?>>Femenino</label>
                                                        <label class="radio-inline">
                                            

                                             <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][3]['sexo_integrante']=="Masculino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante4" name="sexoIntegrante4" <?php echo $checked ?>>Masculino</label> 
                                                            
                                                    </div>

                                                     <?php
                                                    $disabled = "";
                                                    if($si_conocido){
                                                        $disabled = "disabled";
                                                    }
                                                    ?>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante4" name="calleIntegrante[]"      value="<?php echo $editData['listIntegrantes'][3]['calle_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]" value="<?php echo $editData['listIntegrantes'][3]['edad_integrante'] ?>"   >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante4" name="numeroIntegrante[]"  value="<?php echo $editData['listIntegrantes'][3]['numero_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante4" name="coloniaIntegrante[]"   value="<?php echo $editData['listIntegrantes'][3]['colonia_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante4" name="codigoPostalIntegrante[]"  value="<?php echo $editData['listIntegrantes'][3]['cpostal_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Teléfono:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante4" name="telefonoIntegrante[]"  value="<?php echo $editData['listIntegrantes'][3]['telefono_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                    
                                                </div>
                                                
                                                

                                                
                                                <br><br> 
                                                
                                             


                                               
                                                
                                             

                                        </div>

                                                
 
                                          

                                    <?php
                                            $display = "style='display:none'";
                                       
                                        if(trim($editData['listIntegrantes'][4]['nombre_integrante'])!=="" || trim($editData['listIntegrantes'][4]['appaterno_integrante'])!==""){

                                            $display = "style='display:block'";
                                        }

                                      
                                    ?>



                                        <div id="view_integrante5" <?php echo $display ?> >   

                                            <hr>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 5:</label>
                                            </div> 

                                                <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(5)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante5" name="nombreIntegrante[]" value="<?php echo $editData['listIntegrantes'][4]['nombre_integrante'] ?>" >
                                                    </div>

                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante5" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][4]['cargo_integrante']=="Presidente"){
                                                                $selected = "selected";
                                                            }
                                                            ?>

                                                    <option value="Presidente" <?php echo $selected?>>Presidente</option>

                                                          <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][4]['cargo_integrante']=="Tesorero"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Tesorero" <?php echo $selected?>>Tesorero</option>

                                                     

                                                        <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][4]['cargo_integrante']=="Secretario"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Secretario" <?php echo $selected?>>Secretario</option>
                                                   
                                                         <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][4]['cargo_integrante']=="Vocal"){
                                                                $selected = "selected";
                                                            }
                                                            ?>



                                                    <option value="Vocal" <?php echo $selected?>>Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]" value="<?php echo $editData['listIntegrantes'][4]['appaterno_integrante'] ?>"   >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" ">
                                                       <label class="radio-inline">
                                                        <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][4]['firma_constancia_registro_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <input type="radio" value="SI" id="firmaRegistroConstancia5" name="firmaRegistroConstancia5" <?php echo $checked ?>>Si</label>

                                                         <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][4]['firma_constancia_registro_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia5" name="firmaRegistroConstancia5" <?php echo $checked ?>>No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]" value="<?php echo $editData['listIntegrantes'][4]['apmaterno_integrante'] ?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">

                                                          <?php
                                                            $checked = "";
                                                            $si_conocido = 0;

                                                            if($editData['listIntegrantes'][4]['domicilio_conocido_integrante']== "SI")
                                                            {
                                                                $si_conocido = 1;
                                                                $checked = "checked";
                                                            }

                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante5" name="domicilioCortejadoIntegrante5"  <?php echo $checked ?>>Si</label>

                                                          <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][4]['domicilio_conocido_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante5" name="domicilioCortejadoIntegrante5"  <?php echo $checked ?>>No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">


                                            <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][4]['sexo_integrante']=="Femenino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" name="sexoIntegrante5" <?php echo $checked ?>>Femenino</label>
                                                        <label class="radio-inline">
                                            

                                             <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][4]['sexo_integrante']=="Masculino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante4" name="sexoIntegrante5" <?php echo $checked ?>>Masculino</label> 
                                                            
                                                    </div>


                                                 <?php
                                                    $disabled = "";
                                                    if($si_conocido){
                                                        $disabled = "disabled";
                                                    }
                                                    ?>



                                                <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante5" name="calleIntegrante[]"      value="<?php echo $editData['listIntegrantes'][4]['calle_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]" value="<?php echo $editData['listIntegrantes'][4]['edad_integrante'] ?>"   >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante5" name="numeroIntegrante[]"  value="<?php echo $editData['listIntegrantes'][4]['numero_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante5" name="coloniaIntegrante[]"   value="<?php echo $editData['listIntegrantes'][4]['colonia_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante5" name="codigoPostalIntegrante[]"  value="<?php echo $editData['listIntegrantes'][4]['cpostal_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                </div>

                                              <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Teléfono:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante5" name="telefonoIntegrante[]"  value="<?php echo $editData['listIntegrantes'][4]['telefono_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                    
                                                </div>
                                                

                                                

                                                

                                                
                                                <br><br> 
                                                
                                             


                                               
                                                
                                             

                                        </div>




                                    <?php
                                            
                                        $display = "style='display:none'";
                                       
                                        if(trim($editData['listIntegrantes'][5]['nombre_integrante'])!=="" || trim($editData['listIntegrantes'][5]['appaterno_integrante'])!==""){

                                            $display = "style='display:block'";
                                        }

                                      
                                    ?>



                                        <div id="view_integrante6" <?php echo $display ?> >   

                                            <hr>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 6:</label>
                                            </div> 

                                               <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(6)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante6" name="nombreIntegrante[]" value="<?php echo $editData['listIntegrantes'][5]['nombre_integrante'] ?>" >
                                                    </div>

                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante6" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][5]['cargo_integrante']=="Presidente"){
                                                                $selected = "selected";
                                                            }
                                                            ?>

                                                    <option value="Presidente" <?php echo $selected?>>Presidente</option>

                                                          <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][5]['cargo_integrante']=="Tesorero"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Tesorero" <?php echo $selected?>>Tesorero</option>
 

                                                        <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][5]['cargo_integrante']=="Secretario"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Secretario" <?php echo $selected?>>Secretario</option>
                                              
                                                         <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][5]['cargo_integrante']=="Vocal"){
                                                                $selected = "selected";
                                                            }
                                                            ?>



                                                    <option value="Vocal" <?php echo $selected?>>Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]" value="<?php echo $editData['listIntegrantes'][5]['appaterno_integrante'] ?>"   >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" ">
                                                       <label class="radio-inline">
                                                        <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][5]['firma_constancia_registro_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <input type="radio" value="SI" id="firmaRegistroConstancia6" name="firmaRegistroConstancia6" <?php echo $checked ?>>Si</label>

                                                         <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][5]['firma_constancia_registro_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia6" name="firmaRegistroConstancia6" <?php echo $checked ?>>No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]" value="<?php echo $editData['listIntegrantes'][5]['apmaterno_integrante'] ?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">

                                                          <?php
                                                            $checked = "";
                                                            $si_conocido = 0;

                                                            if($editData['listIntegrantes'][5]['domicilio_conocido_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                                $si_conocido = 1;
                                                            }

                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante6" name="domicilioCortejadoIntegrante6"  <?php echo $checked ?>>Si</label>

                                                          <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][5]['domicilio_conocido_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante6" name="domicilioCortejadoIntegrante6"  <?php echo $checked ?>>No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">


                                            <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][5]['sexo_integrante']=="Femenino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" id="sexoIntegrante6" name="sexoIntegrante6" <?php echo $checked ?>>Femenino</label>
                                                        <label class="radio-inline">
                                            

                                             <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][5]['sexo_integrante']=="Masculino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante6" name="sexoIntegrante6" <?php echo $checked ?>>Masculino</label> 
                                                            
                                                    </div>


                                                       <?php
                                                    $disabled = "";
                                                    if($si_conocido){
                                                        $disabled = "disabled";
                                                    }
                                                    ?>


                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante6" name="calleIntegrante[]"      value="<?php echo $editData['listIntegrantes'][5]['calle_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]" value="<?php echo $editData['listIntegrantes'][5]['edad_integrante'] ?>"   >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante6" name="numeroIntegrante[]"  value="<?php echo $editData['listIntegrantes'][5]['numero_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante6" name="coloniaIntegrante[]"   value="<?php echo $editData['listIntegrantes'][5]['colonia_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante6" name="codigoPostalIntegrante[]"  value="<?php echo $editData['listIntegrantes'][5]['cpostal_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Teléfono:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante6" name="telefonoIntegrante[]"  value="<?php echo $editData['listIntegrantes'][5]['telefono_integrante'] ?>" <?=$disabled?> >
                                                    </div>
                                                    
                                                </div>
                                                
                                                

                                                
                                                <br><br> 
                                                
                                             


                                               
                                                
                                             

                                        </div>



                      
                                    <?php
                                            $display = "style='display:none'";
                                       
                                        if(trim($editData['listIntegrantes'][6]['nombre_integrante'])!=="" || trim($editData['listIntegrantes'][6]['appaterno_integrante'])!==""){

                                            $display = "style='display:block'";
                                        }

                                      
                                    ?>



                                        <div id="view_integrante7" <?php echo $display ?> >   

                                            <hr>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 7:</label>
                                            </div> 

                                               <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(7)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante7" name="nombreIntegrante[]" value="<?php echo $editData['listIntegrantes'][6]['nombre_integrante'] ?>" >
                                                    </div>

                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante7" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][6]['cargo_integrante']=="Presidente"){
                                                                $selected = "selected";
                                                            }
                                                            ?>

                                                    <option value="Presidente" <?php echo $selected?>>Presidente</option>

                                                          <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][6]['cargo_integrante']=="Tesorero"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Tesorero" <?php echo $selected?>>Tesorero</option>

                                           

                                                        <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][6]['cargo_integrante']=="Secretario"){
                                                                $selected = "selected";
                                                            }
                                                            ?>
                                                    <option value="Secretario" <?php echo $selected?>>Secretario</option>
                                                    
                                                         <?php
                                                                $selected = "";
                                                                if($editData['listIntegrantes'][6]['cargo_integrante']=="Vocal"){
                                                                $selected = "selected";
                                                            }
                                                            ?>



                                                    <option value="Vocal" <?php echo $selected?>>Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]" value="<?php echo $editData['listIntegrantes'][6]['appaterno_integrante'] ?>"   >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" ">
                                                       <label class="radio-inline">
                                                        <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][6]['firma_constancia_registro_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <input type="radio" value="SI" id="firmaRegistroConstancia7" name="firmaRegistroConstancia7" <?php echo $checked ?>>Si</label>

                                                         <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][6]['firma_constancia_registro_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia7" name="firmaRegistroConstancia7" <?php echo $checked ?>>No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]" value="<?php echo $editData['listIntegrantes'][6]['apmaterno_integrante'] ?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">

                                                          <?php
                                                            $checked = "";
                                                            $si_conocido = 0;

                                                            if($editData['listIntegrantes'][6]['domicilio_conocido_integrante']== "SI")
                                                            {
                                                                $checked = "checked";
                                                                $si_conocido = 1;
                                                            }

                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante7" name="domicilioCortejadoIntegrante7"  <?php echo $checked ?>>Si</label>

                                                          <?php
                                                            $checked = "";

                                                            if($editData['listIntegrantes'][6]['domicilio_conocido_integrante']== "NO")
                                                            {
                                                                $checked = "checked";
                                                            }

                                                        ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante7" name="domicilioCortejadoIntegrante7"  <?php echo $checked ?>>No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">


                                            <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][6]['sexo_integrante']=="Femenino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" id="sexoIntegrante7" name="sexoIntegrante7" <?php echo $checked ?>>Femenino</label>
                                                        <label class="radio-inline">
                                            

                                             <?php
                                                $checked = "";
                                                if($editData['listIntegrantes'][6]['sexo_integrante']=="Masculino")
                                                {
                                                    $checked="checked";

                                                }

                                            ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante7" name="sexoIntegrante7" <?php echo $checked ?>>Masculino</label> 
                                                            
                                                    </div>

                                                
                                                    <?php
                                                    $disabled = "";
                                                    if($si_conocido){
                                                        $disabled = "disabled";
                                                    }
                                                    ?>


                                                <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante7" name="calleIntegrante[]"      value="<?php echo $editData['listIntegrantes'][6]['calle_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]" value="<?php echo $editData['listIntegrantes'][6]['edad_integrante'] ?>"   >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante7" name="numeroIntegrante[]"  value="<?php echo $editData['listIntegrantes'][6]['numero_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante7" name="coloniaIntegrante[]"   value="<?php echo $editData['listIntegrantes'][6]['colonia_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante7" name="codigoPostalIntegrante[]"  value="<?php echo $editData['listIntegrantes'][6]['cpostal_integrante'] ?>" <?=$disabled?>>
                                                    </div>
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             


                                               
                                                
                                             

                                        </div>

                                        <!--
 
                                             <div id="view_integrante8" style='display:none' >   
                                               

                                            <hr>


                                               <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 8:</label>
                                            </div> 

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante[]" name="nombreIntegrante[]"  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante[]" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <option value="Presidente">Presidente</option>
                                                            <option value="Tesorero">Tesorero</option>
                                                            <option value="Tesorera">Tesorera</option>
                                                            <option value="Secretario">Secretario</option>
                                                            <option value="Secretaria">Secretaria</option>
                                                            <option value="Vocal">Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]"  >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" ">
                                                       <label class="radio-inline"><input type="radio" value="SI" id="firmaRegistroConstancia1" name="firmaRegistroConstancia8">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia1" name="firmaRegistroConstancia8">No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]"  >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante8" name="domicilioCortejadoIntegrante8">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante8" name="domicilioCortejadoIntegrante8">No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" name="sexoIntegrante8">Femenino</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="Masculino" id="sexoIntegrante1" name="sexoIntegrante8">Masculino</label> 
                                                            
                                                    </div>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante[]" name="calleIntegrante[]"    >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]"  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante[]" name="numeroIntegrante[]"  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante[]" name="coloniaIntegrante[]"  >
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante[]" name="codigoPostalIntegrante[]"  >
                                                    </div>
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             
                                            </div>

                                                
                                        

                                        <div id="view_integrante9" style='display:none' >   

                                               

                                            <hr>

                                               <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 9:</label>
                                            </div> 

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante[]" name="nombreIntegrante[]"  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante[]" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <option value="Presidente">Presidente</option>
                                                            <option value="Tesorero">Tesorero</option>
                                                            <option value="Tesorera">Tesorera</option>
                                                            <option value="Secretario">Secretario</option>
                                                            <option value="Secretaria">Secretaria</option>
                                                            <option value="Vocal">Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]"  >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" ">
                                                       <label class="radio-inline"><input type="radio" value="SI" id="firmaRegistroConstancia9" name="firmaRegistroConstancia9">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia9" name="firmaRegistroConstancia9">No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]"  >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante9" name="domicilioCortejadoIntegrante9">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante9" name="domicilioCortejadoIntegrante9">No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" name="sexoIntegrante9">Femenino</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="Masculino" id="sexoIntegrante1" name="sexoIntegrante9">Masculino</label> 
                                                            
                                                    </div>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante[]" name="calleIntegrante[]"    >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]"  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante[]" name="numeroIntegrante[]"  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante[]" name="coloniaIntegrante[]"  >
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante[]" name="codigoPostalIntegrante[]"  >
                                                    </div>
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             </div>

                                             <div id="view_integrante10" style='display:none' >     
                                               

                                            <hr>


                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 10:</label>
                                            </div> 

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Nombre del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante[]" name="nombreIntegrante[]"  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">Cargo del Integrante:</label>
                                                    <div class="col-sm-3" ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante[]" name="cargoIntegrante[]">
                                                            <option>Seleccione..</option>
                                                            <option value="Presidente">Presidente</option>
                                                            <option value="Tesorero">Tesorero</option>
                                                            <option value="Tesorera">Tesorera</option>
                                                            <option value="Secretario">Secretario</option>
                                                            <option value="Secretaria">Secretaria</option>
                                                            <option value="Vocal">Vocal</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Primer Apellido:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="apPIntegrante[]" name="apPIntegrante[]"  >
                                                    </div>

                                                    <label for="textInput" class="col-sm-2">Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3" >
                                                       <label class="radio-inline"><input type="radio" value="SI" id="firmaRegistroConstancia10" name="firmaRegistroConstancia10">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia10" name="firmaRegistroConstancia10">No</label> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm" id="apMIntegrante[]" name="apMIntegrante[]"  >
                                                    </div>
                                                     <label  class="col-sm-2 ">¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante10" name="domicilioCortejadoIntegrante10">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante10" name="domicilioCortejadoIntegrante10">No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Sexo:</label>
                                                       <div class="col-sm-3">
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" name="sexoIntegrante10">Femenino</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="Masculino" id="sexoIntegrante10" name="sexoIntegrante10">Masculino</label> 
                                                            
                                                    </div>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante[]" name="calleIntegrante[]"    >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Edad:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante[]" name="edadIntegrante[]"  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante[]" name="numeroIntegrante[]"  >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante[]" name="coloniaIntegrante[]"  >
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1" ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante[]" name="codigoPostalIntegrante[]"  >
                                                    </div>
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             


                                                
 

                                               

                                            <hr>

                                        </div>
                                        -->

                                       <input type="hidden" name="id_comite_integrantes" id="id_comite_integrantes" value="<?php echo $editData['id_comite'] ?>" >
                                               

                                       
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Documentos del comité -->

              








            
                
             

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>


 <link rel="stylesheet" href="recursos/temas/csq-uno/css/estilos.css">

<!-- FORMATO para COMITE -->
    <div class="panel-group" id="imprimir"  style="display:none" >

                                    <div class="cabeza">

                                        <div class="logo-formato">
                                        </div>

                                       <div class="logo-qro">
                                        </div>

                                        

                                        <div class="acta">
                                            <span>Acta de Integracion de Comité de Contraloría Social</span>
                                        </div>

                                    </div>

                                    <div class="panel panel-default">

                                        <div class="panel-heading">
                                            <h4 class="panel-title">1. Datos generales del Comité de Contraloría Social</h4>
                                        </div>

                                        <div class="panel-body">

                                            <div class="form-group">

                                                <div class="col-xs-3 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Municipio'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php

                                                            if(isset($editData['dataProy'][0]['municipio'])){
                                                              echo $editData['dataProy'][0]['municipio'];
                                                              }                                                           
                                             
                                                              ?>" class="form-control" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-3 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Localidad'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php
                                                        
                                                        if(isset($editData['dataProy'][0]['localidad'])){
                                                              echo $editData['dataProy'][0]['localidad']; 
                                                          }

                                                           ?>" class="form-control" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-4 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Programa'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php
                                                            if(isset($editData['dataProy'][0]['programa'])){
                                                                echo $editData['dataProy'][0]['programa']; 
                                                            }

                                                              ?>" class="form-control" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Fecha'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" id="printFechaConstitucion"  <input type="text" value="<?php
                                                            if(isset($editData['dataProy'][0]['fecha_constitucion'])){
                                                                echo $editData['fecha_constitucion']; 
                                                            }

                                                              ?>" class="form-control" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="panel panel-default">

                                        <div class="panel-heading">
                                            <h4 class="panel-title">2. Datos generales de la Obra y/o Apoyo</h4>
                                        </div>

                                        <div class="panel-body">

                                            <div class="form-group">

                                                <div class="col-xs-6 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Nombre de la Obra, Apoyo y/o Servicio'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php

                                                            if(isset($editData['dataProy'][0]['nombre_proyecto'])){
                                                                echo  $editData['dataProy'][0]['nombre_proyecto']; 
                                                                }
                                                            
                                                              ?>" class="form-control" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-3 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Monto de inversión'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php

                                                              if(isset($editData['dataProy'][0]['recurso_ejecutado_vigilado'])){
                                                                   echo  $editData['dataProy'][0]['recurso_ejecutado_vigilado']; 
                                                               }
                                                            
                                                            ?>" class="form-control" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-3 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Periodo de ejecución'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php
                                                            if (isset($formato)) {
                                                                if ($formato != NULL) {
                                                                    if ($formato->diferencia_int != NULL && $formato->diferencia_int != 0) {
                                                                        echo $formato->diferencia_int.' días';
                                                                    } else {
                                                                        echo '---';
                                                                    }
                                                                }
                                                            } ?>" class="form-control" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="panel panel-default">

                                        <div class="panel-heading">
                                            <h4 class="panel-title">3. Integrantes del Comité<br><small style="text-transform: none !important;">(Adjuntar la lista con nombre y firma de los integrantes asistentes a la constitución del Comité)</small></h4>
                                        </div>

                                        <div class="panel-body">

                                            <div class="form-group">

                                                <div class="col-xs-4 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Nombre completo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php
                                                            echo $editData['listIntegrantes'][0]['nombre_integrante'] . " " . $editData['listIntegrantes'][0]['appaterno_integrante'] . " " . $editData['listIntegrantes'][0]['apmaterno_integrante'];
                                                            ?>
                                                            " class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Domicilio'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php

                                                                if(isset($editData['listIntegrantes'][0]['calle_integrante'])){
                                                                      echo $editData['listIntegrantes'][0]['calle_integrante'];
                                                                }
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Teléfono'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][0]['telefono_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Cargo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][0]['cargo_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo firma">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Firma'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="" class="form-control" style="border: none !important;" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <hr class="hr" />
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-xs-4 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Nombre completo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php
                                                            echo $editData['listIntegrantes'][1]['nombre_integrante'] . " " . $editData['listIntegrantes'][1]['appaterno_integrante'] . " " . $editData['listIntegrantes'][1]['apmaterno_integrante'];
                                                            ?>
                                                            " class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Domicilio'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][1]['calle_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Teléfono'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][1]['telefono_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Cargo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                                  <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][1]['cargo_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo firma">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Firma'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="" class="form-control" style="border: none !important;" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <hr class="hr" />
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-xs-4 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Nombre completo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                            echo $editData['listIntegrantes'][2]['nombre_integrante'] . " " . $editData['listIntegrantes'][2]['appaterno_integrante'] . " " . $editData['listIntegrantes'][2]['apmaterno_integrante'];
                                                            ?>
                                                            " class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Domicilio'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][2]['calle_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Teléfono'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][2]['telefono_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Cargo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                                  <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][2]['cargo_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo firma">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Firma'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="" class="form-control" style="border: none !important;" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <hr class="hr" />
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-xs-4 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Nombre completo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                            echo $editData['listIntegrantes'][3]['nombre_integrante'] . " " . $editData['listIntegrantes'][3]['appaterno_integrante'] . " " . $editData['listIntegrantes'][3]['apmaterno_integrante'];
                                                            ?>
                                                            " class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Domicilio'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][3]['calle_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Teléfono'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][3]['telefono_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Cargo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                                   <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][3]['cargo_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo firma">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Firma'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="" class="form-control" style="border: none !important;" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <hr class="hr" />
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-xs-4 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Nombre completo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                              <input type="text" value="<?php
                                                            echo $editData['listIntegrantes'][4]['nombre_integrante'] . " " . $editData['listIntegrantes'][4]['appaterno_integrante'] . " " . $editData['listIntegrantes'][4]['apmaterno_integrante'];
                                                            ?>
                                                            " class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Domicilio'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][4]['calle_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Teléfono'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][4]['telefono_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Cargo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                                  <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][4]['cargo_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo firma">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Firma'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="" class="form-control" style="border: none !important;" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <hr class="hr" />
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div class="col-xs-4 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Nombre completo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                              <input type="text" value="<?php
                                                            echo $editData['listIntegrantes'][5]['nombre_integrante'] . " " . $editData['listIntegrantes'][5]['appaterno_integrante'] . " " . $editData['listIntegrantes'][5]['apmaterno_integrante'];
                                                            ?>
                                                            " class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Domicilio'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][5]['calle_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Teléfono'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                             <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][5]['telefono_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo borde-der">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Cargo'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                                   <input type="text" value="<?php
                                                                echo $editData['listIntegrantes'][5]['cargo_integrante'];
                                                            ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-2 columna">
                                                    <div class="row campo firma">
                                                        <div class="col-xs-12">
                                                            <?php echo form_label('Firma'); ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <input type="text" value="" class="form-control" style="border: none !important;" disabled />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-xs-12">

                                        <div class="col-xs-4 col-xs-offset-1 rubrica text-center">
                                            <div class="row">
                                                <hr class="hr" />
                                                <p><span class="rubrica-txt"><b>Nombre, Cargo y Firma del<br />Representante de la Dependencia Ejecutora</b></span></p>
                                            </div>
                                        </div>

                                        <div class="col-xs-4 col-xs-offset-2 rubrica text-center">
                                            <div class="row">
                                                <hr class="hr" />
                                                <p><span class="rubrica-txt"><b>Nombre, Cargo y Firma del<br />Representante de la Dependencia Promotora</b></span></p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-xs-12" style="padding-bottom: 20px;">

                                        <div class="col-xs-4 col-xs-offset-1 rubrica text-center">
                                            <div class="row">
                                                <hr class="hr" />
                                                <p><span class="rubrica-txt"><b>Testigo</b></span></p>
                                            </div>
                                        </div>

                                        <div class="col-xs-4 col-xs-offset-2 rubrica text-center">
                                            <div class="row">
                                                <hr class="hr" />
                                                <p><span class="rubrica-txt"><b>Testigo</b></span></p>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-xs-12 form-group"  >

                                        <p style="font-size:xx-small" align="justify">La Secretaría de la Contraloría del Poder Ejecutivo del Estado de Querétaro, a través del Departamento de Contraloría Social de la Dirección de Prevención y Evaluación, con domicilio en calle Dr. Leopoldo Río de la Loza número 12, Col. Centro Histórico, C.P. 76000, Ciudad de Santiago de Querétaro, Querétaro; utilizará sus datos personales recabados en el  Acta de Integración de Comité de Contraloría Social, serán para fines estadísticos y de difusión institucional que contemplan (fotografías, videos, grabaciones o datos escritos).Para mayor información acerca del tratamiento y de los derechos que puede hacer valer, usted puede acceder al aviso de privacidad a través de la página de la Secretaría de la Contraloría del Poder Ejecutivo del  Estado de Querétaro: http: www2.queretaro.gob.mx/contraloría.</p>

                                    </div>

                                </div>












<!-- FIN FORMATO para COMITE -->










    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>

    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
 

     <script type="text/javascript" src="<?php echo asset_url();?>js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="<?php echo asset_url();?>css/bootstrap-multiselect.css" type="text/css"/>


      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   
    


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">


        var onlyView = 0;
         var v_esquema = 0;

         var metodo = "<?=trim($editData["metodo"])?>";


         $(document).ready(function () {

                $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */

          //alert("x<?=$editData["metodo"]?>x");
         
         if(metodo == "view") {
            $("#btnGuardar").hide();   
           
         }
 
        
     

            if($("#programaSeleccionado").val() == "todos"){
            //alert("Debe seleccionar un Programa, antes de guardar Registro.");
            swal("Advertencia!", "Debe seleccionar un Programa, antes de guardar Registro.", "warning");
            $("#btnGuardar").fadeOut("slow");
          }

           $('#listObras').multiselect({
                       
                        buttonWidth: '300px',
                        maxHeight: 200,
                        enableCaseInsensitiveFiltering: true,

                    });


            <?php
            if($editData['onlyView']){
                echo "onlyView = 1;";
            }

          
 
            if($editData['onlyView']){
               

              echo  '$("#form_datos_comité :input").prop("disabled", true);';
              echo  '$("#form_preguntasClave :input").prop("disabled", true);';
              echo  '$("#form_fechas :input").prop("disabled", true);';
              echo  '  $("#form_beneficios :input").prop("disabled", true);';
              echo  '  $("#form_domicilio :input").prop("disabled", true);';
              echo  '  $("#form_data :input").prop("disabled", true);';
              echo  '  $("#form_funciones :input").prop("disabled", true);';
              echo  '  $("#form_integrante1 :input").prop("disabled", true);';
              echo  '  $("#form_documentoscomite :input").prop("disabled", true);';
            
            }
    ?>


            $("#fechaConstitucion").keypress(function(event) {event.preventDefault();});
 
 


                   changeProgram();
/*
                   $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante1]:checked').val() == "SI"){

                        $("#calleIntegrante1").prop( "disabled", true );
                        $("#numeroIntegrante1").prop( "disabled", true );
                        $("#coloniaIntegrante1").prop( "disabled", true );
                        $("#codigoPostalIntegrante1").prop( "disabled", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante1]:checked').val() == "NO"){

                        $("#calleIntegrante1").prop( "disabled", false );
                        $("#numeroIntegrante1").prop( "disabled", false );
                        $("#coloniaIntegrante1").prop( "disabled", false );
                        $("#codigoPostalIntegrante1").prop( "disabled", false );
                        
                    }

                });


              $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante2]:checked').val() == "SI"){

                        $("#calleIntegrante2").prop( "disabled", true );
                        $("#numeroIntegrante2").prop( "disabled", true );
                        $("#coloniaIntegrante2").prop( "disabled", true );
                        $("#codigoPostalIntegrante2").prop( "disabled", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante2]:checked').val() == "NO"){

                        $("#calleIntegrante2").prop( "disabled", false );
                        $("#numeroIntegrante2").prop( "disabled", false );
                        $("#coloniaIntegrante2").prop( "disabled", false );
                        $("#codigoPostalIntegrante2").prop( "disabled", false );
                        
                    }

                });


                 $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante3]:checked').val() == "SI"){

                        $("#calleIntegrante3").prop( "disabled", true );
                        $("#numeroIntegrante3").prop( "disabled", true );
                        $("#coloniaIntegrante3").prop( "disabled", true );
                        $("#codigoPostalIntegrante3").prop( "disabled", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante3]:checked').val() == "NO"){

                        $("#calleIntegrante3").prop( "disabled", false );
                        $("#numeroIntegrante3").prop( "disabled", false );
                        $("#coloniaIntegrante3").prop( "disabled", false );
                        $("#codigoPostalIntegrante3").prop( "disabled", false );
                        
                    }

                });





                 $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante4]:checked').val() == "SI"){

                        $("#calleIntegrante4").prop( "disabled", true );
                        $("#numeroIntegrante4").prop( "disabled", true );
                        $("#coloniaIntegrante4").prop( "disabled", true );
                        $("#codigoPostalIntegrante4").prop( "disabled", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante4]:checked').val() == "NO"){

                        $("#calleIntegrante4").prop( "disabled", false );
                        $("#numeroIntegrante4").prop( "disabled", false );
                        $("#coloniaIntegrante4").prop( "disabled", false );
                        $("#codigoPostalIntegrante4").prop( "disabled", false );
                        
                    }

                });



                    $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante5]:checked').val() == "SI"){

                        $("#calleIntegrante5").prop( "disabled", true );
                        $("#numeroIntegrante5").prop( "disabled", true );
                        $("#coloniaIntegrante5").prop( "disabled", true );
                        $("#codigoPostalIntegrante5").prop( "disabled", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante5]:checked').val() == "NO"){

                        $("#calleIntegrante5").prop( "disabled", false );
                        $("#numeroIntegrante5").prop( "disabled", false );
                        $("#coloniaIntegrante5").prop( "disabled", false );
                        $("#codigoPostalIntegrante5").prop( "disabled", false );
                        
                    }

                });


                       $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante6]:checked').val() == "SI"){

                        $("#calleIntegrante6").prop( "disabled", true );
                        $("#numeroIntegrante6").prop( "disabled", true );
                        $("#coloniaIntegrante6").prop( "disabled", true );
                        $("#codigoPostalIntegrante6").prop( "disabled", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante6]:checked').val() == "NO"){

                        $("#calleIntegrante6").prop( "disabled", false );
                        $("#numeroIntegrante6").prop( "disabled", false );
                        $("#coloniaIntegrante6").prop( "disabled", false );
                        $("#codigoPostalIntegrante6").prop( "disabled", false );
                        
                    }

                });


                           $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante7]:checked').val() == "SI"){

                        $("#calleIntegrante7").prop( "disabled", true );
                        $("#numeroIntegrante7").prop( "disabled", true );
                        $("#coloniaIntegrante7").prop( "disabled", true );
                        $("#codigoPostalIntegrante7").prop( "disabled", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante7]:checked').val() == "NO"){

                        $("#calleIntegrante7").prop( "disabled", false );
                        $("#numeroIntegrante7").prop( "disabled", false );
                        $("#coloniaIntegrante7").prop( "disabled", false );
                        $("#codigoPostalIntegrante7").prop( "disabled", false );
                        
                    }

                });

*/

                $('#upload').on('click', function () {
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'registro_comite/upload_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });








             $.datepicker.regional['es'] = {
                  closeText: 'Cerrar',
                  prevText: '<Ant',
                  nextText: 'Sig>',
                  currentText: 'Hoy',
                  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                  monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                  dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                  dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                  weekHeader: 'Sm',
                  dateFormat: 'dd/mm/yy',
                  firstDay: 1,
                  isRTL: false,
                  showMonthAfterYear: false,
                  yearSuffix: ''
                };

                $.datepicker.setDefaults($.datepicker.regional['es']);



            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();


           


               $( function() {
            $( "#fechaConstitucion" ).datepicker({ dateFormat: 'dd/mm/yy' });
           
            
          } );



            

          });    

    
 function loadCatalogFunctions(){

            var resourceSelected = $("#recurso_sel option:selected").val();     
                 //  alert(resourceSelected);
                
                $("#modulo_funciones_federal").show();
                if(resourceSelected == 2){ 
                    $("#modulo_funciones_estatal").show("slow");
                   // $("#modulo_funciones_federal").hide();
                    $("#modulo_preguntas_funciones").hide();

                }else{           
                     
                    //$("#modulo_funciones_federal").show("slow");
                    $("#modulo_preguntas_funciones").show("slow");
                     $("#modulo_funciones_estatal").hide();
                }
        }


        function imprimirFormato(){
           // alert("hola");
            <?php
            $url = base_url("index.php/pdfexample5");
            ?>
            //alert("<?=$url?>");
              location.target = "_blank";
            location.href ="<?=$url?>";

            return;

            var win = window.open("", "Formato de Acta CCS", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=800, height=600, top=100, left=100");
            var formato = document.getElementById('imprimir').innerHTML;
            win.document.write('<html><head><title>Formato de Acta</title><link rel="stylesheet" type="text/css" href="<?php echo asset_url().'css/bootstrap.css'; ?>"><link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>css/imprimir.css"><style>body{padding:25px;}</style></head><body>');
            win.document.write(formato);
            win.document.write('</body></html>');
            win.document.close();
            win.focus();
            setTimeout(function() {
                win.print();
                win.close();
            }, 100);

        }


        function updateFecha(valor){
            //alert("actualizando..");

            //$("#printFechaConstitucion").val("10/09/1985");
           // $("#printFechaConstitucion").val(valor);
        }


        function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_comite/populate_localidad_proyecto') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");

                     /*  $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });*/



                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
        }   






         function sumaPeople(){
            
            var total = 0;

                 
                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                   //     alert("value:" + this.value);
                        total += parseFloat(this.value);
                    }
                }); 


                //total = parseFloat($("#total_people_womans").val()) + parseFloat($("#total_people_mans").val());

            $("#total_personas").val(total);




        }

         function quitRecurso(elem){

             var noElem = elem.toString();

              var qtyIntegrantes = $("#countResourceIntegrantes").val();
              $("#countResourceIntegrantes").val(qtyIntegrantes-1)

                 var cad = "#nombreIntegrante"+noElem;
                   elem = cad.toString();
                


                   $(elem).val("");

                    var cad = "#cargoIntegrante"+noElem;
                    elem = cad.toString();
                    $(elem).val('0')
 

             $("#view_integrante"+noElem).hide('slow');
         }

 

         var msgError = "";
        $("#msgError").val("");



        function validaCamposComite(){


            msgError = "";
                    $("#nombreComite").css({"background-color": "white"});  
                    $("#fechaConstitucion").css({"background-color": "white"});            
 
          

             

                if( $("#nombreComite").val() == ""){
                    msgError+= "- Debe tener Nombre del Comite, \n";
                    $("#nombreComite").css({"background-color": "yellow"});
                }

                 if( $("#fechaConstitucion").val() == ""){
                    msgError+= "- Falta poner Fecha de Constitución:, \n";
                    $("#fechaConstitucion").css({"background-color": "yellow"});
                }

             
                if( $("#listObras option:selected").val() == undefined){
                     msgError+= "- Debe seleccionar al menos una Obra/Servicio/Apoyo. \n";
                    
                }
 

                
               


        } 


        function validaIntegrantes(){

            
            var qtyIntegrantes = $("#countResourceIntegrantes").val();
            

                $('input[name="nombreIntegrante[]"]').each(function(){
                      $(elem).css({"background-color": "white"});
                    });
          
 


                if(qtyIntegrantes>1){

                    var n, elem;
                for(i=1; i<=qtyIntegrantes; i++){
                    


                   //Nombre
                   var cad = "#nombreIntegrante"+i;
                   elem = cad.toString();
                


                   if($(elem).val()==""){
                     msgError+= "- Debe tener Nombre del Integrante "+i+", \n";
                      $(elem).css({"background-color": "yellow"});
                      $(elem).focus();

                   }


                   //Nombre
                   var cad = "#apPIntegrante"+i;
                   elem = cad.toString();
                


                   if($(elem).val()==""){
                     msgError+= "- Debe tener Apellido1 del Integrante "+i+", \n";
                      $(elem).css({"background-color": "yellow"});
                      $(elem).focus();

                   }


                   //Sexo
                    if( $('input[name=sexoIntegrante'+i+']:checked').val() == undefined){
                        msgError+= "- Debe seleccionar sexo del Integrante "+i+", \n";
                        $('input[name=sexoIntegrante'+i+']:checked').css({"background-color": "yellow"});
                        $('input[name=sexoIntegrante'+i+']:checked').focus();
                    }


                    //Cargo
                    var cad = "#cargoIntegrante"+i;
                    elem = cad.toString();
                    if($(elem).val()=="Seleccione.."){
                        $(elem).css({"background-color": "yellow"});
                         $(elem).focus();
                           msgError+= "- Debe seleccionar cargo del Integrante "+i+", \n";


                    }


                }

            }else{
                 msgError+= "- Debe tener al menos 2 Integrantes en el Comite \n";
            }

               
                
                                                
        }


        function preGuardado(){
            
            var dataForms = $("#form_datos_comité").serialize() + "&" + $("#form_documentoscomite").serialize() + "&" + $("#form_funciones").serialize() + "&" + $("#form_preguntasClave").serialize() + "&" + $("#form_integrante1").serialize();

            $.ajax({
                    url: "<?php echo site_url('registro_comite/guardarDatosDelcomite') ?>",
                    type: "POST",
                    data: dataForms,
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                        $("#id_comite").val(message);

                        $("#id_comite_documentos").val(message);
                        $("#id_comite_preguntasclave").val(message);
                        $("#id_comite_funciones").val(message);
                        $("#id_comite_integrantes").val(message);

        
 
                    }
                });
        }


         function editar(){

            $("#btnGuardar").show("slow");

            <?php
                
            if($editData['onlyView']){

                  echo  '$("#form_datos_comité :input").prop("disabled", false);';
                  echo  '$("#form_preguntasClave :input").prop("disabled", false);';
                  echo  '$("#form_fechas :input").prop("disabled", false);';
                  echo  '  $("#form_beneficios :input").prop("disabled", false);';
                  echo  '  $("#form_domicilio :input").prop("disabled", false);';
                  echo  '  $("#form_data :input").prop("disabled", false);';
                  echo  '  $("#form_funciones :input").prop("disabled", false);';
                  echo  '  $("#form_integrante1 :input").prop("disabled", false);';
                  echo  '  $("#form_documentoscomite :input").prop("disabled", false);';
                }

              ?>

              onlyView = 0;


        }


        function guardar(){
            

              if(onlyView==1){
                return;
               } 

                validaCamposComite();
                validaIntegrantes();



                 if(msgError!=""){
                    swal("Error!", msgError, "error");
                    return;
                }

 

                preGuardado();


                 /* $.ajax({
                    url: "<?php echo site_url('registro_comite/guardarFunciones') ?>",
                    type: "POST",
                    data: $('#form_funciones').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });



                  $.ajax({
                    url: "<?php echo site_url('registro_comite/guardarPreguntasClave') ?>",
                    type: "POST",
                    data: $('#form_preguntasClave').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


                    $.ajax({
                    url: "<?php echo site_url('registro_comite/guardarIntegrante1') ?>",
                    type: "POST",
                    data: $('#form_integrante1').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


                $.ajax({
                    url: "<?php echo site_url('registro_comite/guardarDocumentosComite') ?>",
                    type: "POST",
                    data: $('#form_documentoscomite').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });*/

          swal("Bien hecho!", "El comité ha sido guardado.", "success");
          
        }


         function loadCatalogFunctions(){

            var resourceSelected = $("#recurso_sel option:selected").val();     
                 
                $("#modulo_funciones_federal").show();
                if(resourceSelected == 2 || v_esquema == 1){ 
                    $("#modulo_funciones_estatal").show("slow");
                    $("#modulo_preguntas_funciones").hide();

                }else{           
                     
                    //$("#modulo_funciones_federal").show("slow");
                    $("#modulo_preguntas_funciones").show("slow");
                    $("#modulo_funciones_estatal").hide();
                }
        }



        function changeProgram(){


            var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }



               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            esquema = obj.esquema;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").fadeIn("slow");

                 //  $("#esquema").val(esquema);


                    if(recurso == 1){
                         $("#patcs_menu").hide();
                         $("#petcs_menu").show("slow");
                          $("#seguimiento_menu").hide("slow");
                    }

                    
                    v_esquema = 0;

                    if(esquema==1){
                        v_esquema = 1;
                    }
                     

                    if(recurso == 2 || v_esquema == 1){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow");    
                         $("#seguimiento_menu").show("slow");   
                    }

                      loadCatalogFunctions();

                
                    }
                });

  
               
        }



        function agregarNuevoIntegrante(){
 
            var cadenaHTML;
            var contador;


            if($("#countResourceIntegrantes").val()<7){
                 contador = parseInt($("#countResourceIntegrantes").val()) + 1;
     
                 var label = "view_integrante"+contador;
                 
                 $("#"+label).show("slow"); 

                  
                 
                $("#countResourceIntegrantes").val(contador);
            }

        }

               function resetearForma(){
 
           
           location.href="<?php echo site_url();?>/registro_comite";
            
   
          
        }

          function cleanElement(){

            swal({
              title: "Estas seguro de Crear nuevo Registro?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: false,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Listo!, Empezamos de nuevo la captura", {
                  icon: "success",
                });
                resetearForma();

              } else {
                swal("Lo dejamos como estaba..");
              }
            });
        }








    </script>

</body>

</html>
