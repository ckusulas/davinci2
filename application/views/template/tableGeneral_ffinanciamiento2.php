  <form id="form_listadoFF" action="registro_ffinanciamiento" method="post">


 <table id="example2" class="display" style="width:100%" border=1>
        <thead>
            <tr>
              <th colspan=2 bgcolor="E1E1E1">
                Referencia
              </th>
              <th colspan=2 bgcolor="E1E1E1">
                Federal
              </th>
               <th colspan=2 bgcolor="E1E1E1">
                Estatal
              </th>
               <th colspan=2 bgcolor="E1E1E1">
                Municipal
              </th>
               <th colspan=2 bgcolor="E1E1E1">
                Otros
              </th>
               <th colspan=4 bgcolor="E1E1E1">
                TOTALES
              </th>
            </tr>
            <tr>
                <th>Id</th>
                
                <th>Programa</th>
                <th>Fondo</th>
                <th>Inversion</th>
                <th>Fondo</th>
                <th>Inversion</th>
                <th>Fondo</th>
                <th>Inversion</th>
                <th>Fondo</th>
                <th>Inversion</th>
                
              
                <th>TOTAL</th>
                <th>Obras</th>
                <th>Comites</th>
                 
            </tr>
        </thead>
        <tbody> 

            
            <?php
            foreach ($listFFinanciamiento as $row){


              $hombresYmujeres = "";
              if($recurso==1){

                $hombresYmujeres = "<td>" . number_format($row["mujeres_beneficiadas"]) ."</td> <td>" . number_format($row["hombres_beneficiados"]) ."</td>";
              }

              $band_recursto = "Estatal";
              if($recurso == 1){
                $band_recursto = "Federal";
              }




//print_r($ffinanciamiento_model);
             
      
       $qtyObras = $ffinanciamiento_model->getQtyBeneficio(1,$row["programa_id"]);
        $qtyApoyo = $ffinanciamiento_model->getQtyBeneficio(2,$row["programa_id"]);
        $qtyServicio = $ffinanciamiento_model->getQtyBeneficio(3,$row["programa_id"]);

        $totalComites = $this->ffinanciamiento_model->getQtyComites($row["programa_id"]);

        $totalObras = $qtyObras + $qtyApoyo + $qtyServicio;

        

        
                echo "<tr id='tr_".$row["id"]."'><td>". $row["id"] . "</td><td>". $row["programa"] . "</td><td>". $row["federal_recurso_fuente_financiamiento_recurso1"] . "</td><td> $" . number_format($row["resumen_total_federal"]) ."</td><td>". $row["estatal_recurso_fuente_financiamiento_recurso1"] . "</td><td> $" . number_format($row["resumen_total_estatal"]) ."</td><td>". $row["fondo"] . "</td><td>" . number_format($row["resumen_total_municipal"]) ."</td><td>". $row["otros_recurso_fuente_financiamiento_recurso1"] . "</td><td> $". number_format($row["resumen_total_otros"]) . "</td><td>" . number_format($row["resumen_total_asignado"]) ."</td><td>" . $totalObras ."</td><td>" . $totalComites ."</td>";

                ?>

                </tr>

              <?php
                
            }
            ?>


            
         
        </tbody>
      
    </table>

    <input type="hidden" name="id_ffinanciamiento" id="id_ffinanciamiento">
    <input type="hidden" name="method" id="method">
 


</form>


 <script type="text/javascript">

        $(document).ready(function () {

       
     
 

     var tableProy = $('#example2').DataTable( {
       language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
        dom: 'Bfrtip',
        scrollX: 'true',
        pagingType: 'full_numbers',
        bAutoWidth: false,
    
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        bInfo: false,
        buttons: [
            
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                  exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14]
                    }
            },
            
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                  exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14]
                    }
            }
        ],

    } );



     
     
  });    

        function viewFF(id_ffinanciamiento)
        {

            $("#id_ffinanciamiento").val(id_ffinanciamiento);
            $("#method").val("view");
           
            $("#form_listadoFF").submit();



        }


        function editFF(id_ffinanciamiento)
        {

            $("#id_ffinanciamiento").val(id_ffinanciamiento);
            $("#method").val("edit");
           
            $("#form_listadoFF").submit();



        }


        function delFF(id_ffinanciamiento, fk_departamento)
        {

         $("#id_ffinanciamiento").val(id_ffinanciamiento);
         $("#fk_departamento").val(fk_departamento);

         swal({
                              title: "Estás seguro de querer borrar la Fuente de Financiamiento?",
                              text: "",
                              icon: "warning",
                              buttons: true,
                              dangerMode: false,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                                swal("Listo!, Se ha borrado exitosamente", {
                                  icon: "success",
                                });
                                eliminarFFinanciamiento(id_ffinanciamiento, fk_departamento);

                              } else {
                                swal("Lo dejamos como estaba..");
                              }
                            });
        }

 


    function eliminarFFinanciamiento(id_ffinanciamiento, fk_departamento){

         

            $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/eliminarFFinanciamiento') ?>",
                    type: "POST",
                    data: $('#form_listadoFF').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                       if(message=="ok"){
                         $("#tr_"+id_ffinanciamiento).hide('slow');
                       }

                       
 
                    }
                });
    }





    </script>