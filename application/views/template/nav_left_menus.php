 <nav class="navbar-side" role="navigation">
            <div class="navbar-collapse sidebar-collapse collapse">
                <ul id="side" class="nav navbar-nav side-nav">
                    <!-- begin SIDE NAV USER PANEL -->
                    <li class="side-user hidden-xs">

                        <?php
                        $photo = $this->session->userdata('photo');
                        $nombre = strtolower($this->session->userdata('nombre'));
 
                        $posicion = $this->session->userdata('position');

                        
                        if($posicion=="Ejecutora"){
                            $nombre = strtoupper($nombre);
                        }else{
                            $nombre = ucwords($nombre);
                        }
                        
                        $appaterno = ucwords(strtolower($this->session->userdata('appaterno')));
                      

                      
                        
                        ?>
                        <img class="img-circle" width="150px" height="150px" src="<?php echo asset_url();?>img/<?=$photo?>" alt="">
                        <p class="welcome">
                            <i class="fa fa-key"></i> Ejecutor
                        </p>
                        <p class="name tooltip-sidebar-logout">

                            <?=$nombre?>
                            <span class="last-name"><?=$appaterno?></span> <a style="color: inherit" class="Salir_open" href="#logout" data-toggle="tooltip" data-placement="top" title="logout"><i class="fa fa-sign-out"></i></a>
                        </p>
                        <div class="clearfix"></div>
                    </li>
                    <!-- end SIDE NAV USER PANEL -->
                    <!-- begin SIDE NAV SEARCH -->
     
					 
					
                    <!-- end SIDE NAV SEARCH -->


                    <li>
                        <a href="<?php echo site_url();?>/indicadores_generales">
                            <i class="fa fa-dashboard"></i> Indicadores Generales
                        </a>
                    </li>
        
					
				   <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#financiamiento">
                            <i class="fa fa-money"></i> Fuente de Financiamiento <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="financiamiento">
                            <li>
                                <a href="#" onClick="registrarFinanciamiento()" id="registrarFfinanciamiento">
                                    <i class="fa fa-angle-double-right"></i> Registrar F.Financiamiento.
                                </a>
                            </li>
                            <li>
                                <a href="#" onClick="listadoFFinanciamiento()">
                                    <i class="fa fa-angle-double-right"></i> Listado F.Financiamiento.
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo site_url(); ?>/listado_ffinanciamiento2" >
                                    <i class="fa fa-angle-double-right"></i> Resumen F. Financiamiento
                                </a>
                            </li>
                         
                            
                           
                        </ul>

                    </li>
 

                   <li class="panel" id="documentos_menu" style="display:none">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#d_normativo">
                            <i class="fa fa-money"></i> Documentos Normativos <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="d_normativo">
                            <li>
                                <a href="<?php echo site_url();?>/listado_documento_normativo">
                                    <i class="fa fa-angle-double-right"></i> Consultar Documentacion
                                </a>
                            </li>
                            
                         
                            
                           
                        </ul>

                    </li>



                   <li class="panel" id="petcs_menu" style="display:none">
                    
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#petcs">
                            <i class="fa fa-money"></i>  PETCS  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="petcs">
                            <li>
                                <a href="<?php echo site_url();?>/registro_petcs"> 
                             
                                    <i class="fa fa-angle-double-right"></i> Elaborar PETCS
                                </a>
                            </li>
                              
                        </ul>

                    </li>

                     <li class="panel" id="patcs_menu" style="display:none">
                    
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#patcs">
                            <i class="fa fa-money"></i>  PATCS  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="patcs">
                            <li>
                                 <a href="<?php echo site_url();?>/registro_patcs"> 
                              
                                    <i class="fa fa-angle-double-right"></i> Elaborar PATCS
                                </a>
                            </li>
                              
                        </ul>

                    </li>




                 
             
                    <!-- Dropdown Proyectos de Obras-->
                    <li class="panel">
                        <a href="javascript:;" id="navObra" data-parent="#side" data-toggle="collapse" class="accordion-toggle" onClick="validarFFinanciamiento(this)" data-target="#forms">
                            <i class="fa fa-clipboard"></i> Obra / Apoyo / Servicio <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="forms">
                            <li>

                                
                                <a href="#" onClick="registrarProyecto()" >
                                    <i class="fa fa-angle-double-right"></i> Registrar Obra/Apoyo/Servicio
                                </a>
                            </li>
                            <li>
                                <a href="#" onClick="listadoProyecto()" >
                                    <i class="fa fa-angle-double-right"></i> Listado de Obra/Apoyo/Servicios
                                </a>
                            </li>
                             
                           
                        </ul>
                    </li>


                     <!-- dropdown convenio modificadora -->
                   <!-- <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#modificadora">
                            <i class="fa fa-users"></i> Convenio Modificadora <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="modificadora">
                           <li>
                                <a href="<?php echo site_url();?>/registro_convenio_modificadoras">
                                    <i class="fa fa-angle-double-right"></i> Registro Convenio de Modificadoras
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url();?>/listado_convenio_modificadoras">
                                    <i class="fa fa-angle-double-right"></i> Listado de Convenio de Modficadoras
                                </a>
                            </li>
                           
                        </ul>
                    </li>-->
                    <!-- end comite DROPDOWN -->

                    <!-- dropdown COMITES -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" onClick="validarFFinanciamiento(this)" class="accordion-toggle" data-target="#comites">
                            <i class="fa fa-users"></i> Comités <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="comites">
                            <li>

                                <a href="#" onClick="registrarComite()" >
                     
                                    <i class="fa fa-angle-double-right"></i> Registro Nuevo Comité
                                </a>
                            </li>
                            <li>
                                 <a href="#" onClick="listadoComites()" >
                                    <i class="fa fa-angle-double-right"></i> Listado de Comités
                                </a>
                            </li>
                           
                        </ul>
                    </li>
                    <!-- end comite DROPDOWN -->


             



                    <!-- dropdown Capacitacion -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle"  onClick="validarFFinanciamiento(this)" data-target="#capacitacion">
                            <i class="fa fa-book"></i> Capacitación <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="capacitacion">
                            <li>
                                  <a href="#" onClick="registrarCapacitacion()" >
                                    <i class="fa fa-angle-double-right"></i> Registro de Capacitación
                                </a>
                            </li>
                            <li>
                              <a href="#" onClick="listadoCapacitacion()" >
                                    <i class="fa fa-angle-double-right"></i> Listado de Capacitacion
                                </a>
                            </li>
 
                                   
                           
                           
                        </ul>
                    </li>
                    <!-- end comite DROPDOWN -->

                     <!-- dropdown Materiales -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle"  onClick="validarFFinanciamiento(this)" data-target="#materiales">
                            <i class="fa fa-book"></i> Materiales <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="materiales">
                            <li>
                                   <a href="#" onClick="registrarMaterial()" >
                                    <i class="fa fa-angle-double-right"></i> Registro de Material 
                                </a>
                            </li>
                              
                            <li>
                               <a href="<?php echo site_url();?>/listado_materiales">
                                    <i class="fa fa-angle-double-right"></i> Listado de Materiales
                                </a>
                            </li>

                              <li role="separator" class="divider"></li>
                            <li>
                               <a href="<?php echo site_url();?>/listado_distribucion">
                                    <i class="fa fa-angle-double-right"></i> Distribucion de Material
                                </a>
                            </li>
                           
                           
                        </ul>
                    </li>
 

                    <!-- dropdown Seguimiento a Comite -->
                    <li class="panel" id="seguimiento_menu" style="display:none">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle"  onClick="validarFFinanciamiento(this)" data-target="#seguimiento">
                            <i class="fa fa-users"></i> Seguimiento a Comité<i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="seguimiento">
                            <li>

                                <a href="#" onClick="registrarSeguimiento()" >
                     
                                    <i class="fa fa-angle-double-right"></i> Registro Seguimiento a Comité
                                </a>
                            </li>
                            <li>
                                <a href="#" onClick="listadoSeguimiento()" >
                                    <i class="fa fa-angle-double-right"></i> Listado de Seguimientos a Comités
                                </a>
                            </li>

                            <li>
                                <a href="<?=base_url()?>upload/plantillas/capacitacion_ccs.pdf">
                                    <i class="fa fa-angle-double-right"></i> Formato Seguimiento a Comité de CS
                                </a>
                            </li>
                           
                        </ul>
                    </li>
                    <!-- end comite DROPDOWN -->
                    

                    

                    <li class="panel" id="reuniones_menu" style="display:none">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle"  onClick="validarFFinanciamiento(this)" data-target="#minutas">
                            <i class="fa fa-edit"></i> Registro de Reuniones <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="minutas">
                            <li>
                                 <a href="#" onClick="registrarReunion()" >
                                    <i class="fa fa-angle-double-right"></i> Registro de Reunion
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-double-right"></i> Listado de Reuniones
                                </a>
                            </li>
                           
                           
                        </ul>
                    </li>



                    <li class="panel" id="informes_menu" style="display:none">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle"  onClick="validarFFinanciamiento(this)" onClick="validarFFinanciamiento(this)" data-target="#informes">
                            <i class="fa fa-bullhorn"></i> Informes <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="informes">
                            <li>
                                  <a href="#" onClick="registrarInforme()" >                     
                                    <i class="fa fa-angle-double-right"></i> Registro de Informes
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-angle-double-right"></i> Listado de Informes
                                </a>
                            </li>
                           
                           
                        </ul>
                    </li>


                    <li class="panel" id="formatos_menu" style="display:none">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#formatos">
                            <i class="fa fa-bullhorn"></i> Formatos <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="formatos">

                              <li>
                                <a href="<?=base_url()?>upload/plantillas/acta_integracion_cs.pdf"  target="_blank">
                                    <i class="fa fa-angle-double-right"></i> Acta de Integración de Comité de CS
                                </a>
                            </li>   
      
                            <li>
                                <a href="<?=base_url()?>upload/plantillas/capacitacion_ccs.pdf" target="_blank">
                                    <i class="fa fa-angle-double-right"></i> Capacitacion en CS a Contralores Sociales
                                </a>
                            </li>     

                             <li>
                                <a href="<?=base_url()?>upload/plantillas/seguimiento_comite_csocail.pdf"  target="_blank">
                                    <i class="fa fa-angle-double-right"></i> Seguimiento a Comité de CS
                                </a>
                            </li>    

                         
                              

                                                      
                                                  
                            
                        </ul>
                    </li>



                    <li class="panel" id="reportes_menu" style="display:none">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#Reportes">
                            <i class="fa fa-bullhorn"></i> Reportes <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="Reportes">
                            <li>
                                <a href="<?php echo site_url();?>/reporte_general">
                                    <i class="fa fa-angle-double-right"></i> Reporte General
                                </a>
                            </li>
                           
                           
                           
                        </ul>
                    </li>


                    <!-- begin CALENDAR LINK -->
                  <!--  <li>
                        <a href="#">
                            <i class="fa fa-calendar"></i> Calendario
                        </a>
                    </li>-->

                <!--  <li>
                       <a href="<?php echo site_url();?>/admon_programas">
                            <i class="fa fa-calendar"></i> Agregar Nuevo Programa
                        </a>
                    </li>-->
        
                
          
                  
                    <!-- end PAGES DROPDOWN -->
                </ul>
                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>








        <script language="javascript">




    /*function existeFFinanciamiento(){

        <?php
        $fk_programa_recurso = $this->session->userdata('registroProgramaRecurso');
        ?>
        var fk_programa_recurso = "<?=$fk_programa_recurso?>";

        if(fk_programa_recurso==""){               
            return false;
        }else{
            return true;
        }
    }*/


     function validarExistenciaFFinanciamiento(fk_programa){

          //  var existeFFinanciamiento = true;

         var existeFFinanciamiento = false;
         var count = 0;
         var existeFF = false;

    
                    $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('indicadores_generales/ajax_validaExisteFFinanciamiento') ?>/" + fk_programa,
                            contentType: "application/json;charset=utf-8",
                            dataType: 'json',
                            success: function (data) {

                         console.log(data);

                         var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            existeFF = obj.existe_recursoFF;
                            // recurso = obj.recurso;
                        });


                            

                           
    
                                    
                            }
                   

                        });

                          return existeFF; 
 

        }



function test(){
  return true;
};



   


    function validarFFinanciamiento(elem){

alert("proxim..");
       // alert("proxim. validar FF exists");
      //  return;
        
    /*
 
     var  fk_programa = $("#programaSeleccionado option:selected").val();

 

resultado = validarExistenciaFFinanciamiento(fk_programa);

//console.log("resultado2:"+ validarExistenciaFFinanciamiento(fk_programa));
console.log("resultado4:"+ test());

 alert("resultado:"+resultado);
 
       if(resultado==true){

            $($(elem).data("target")).show();
        
        }else{
            alert("Debes capturar al menos una Fuente de Financiamiento...");
            $($(elem).data("target")).hide();
        }  */
    }


        </script>