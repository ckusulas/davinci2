 <!-- begin DASHBOARD CIRCLE TILES -->
                <div class="row">
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading dark-blue">
                                    <i class="fa fa-users fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-blue">
                                <div class="circle-tile-description text-faded">
                                   <br> Usuarios Beneficiados<br>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <center><?php echo number_format($totalBeneficiados) ?></center>
                                     <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                 
                                </div>
                                <a href="#" onClick="clickBeneficiarios()" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading red">
                                    <i class="fa fa-money fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content red">
                                <div class="circle-tile-description text-faded">
                                    <br>
                                    Presupuesto Asignado  <br>
                                </div>
                                <div class="circle-tile-number text-faded">
                                   <center> $<?php echo number_format($totalRecursoAsignado) ?></center>
                                    <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                </div>
                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading orange">
                                    <i class="fa fa-cubes fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content orange">
                                <div class="circle-tile-description text-faded">
                                    <br>Obras<br>
                                </div>
                                <div class="circle-tile-number text-faded">
                                  <center>  <?=$qtyObras?></center>
                                   <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                </div>
                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                     <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-handshake-o fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content purple">
                                <div class="circle-tile-description text-faded">
                                   <br>Apoyos<br>
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <center><?=$qtyApoyos?></center>
                                     <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                </div>
                                <a href="#" class="circle-tile-footer">Mas Detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-tint fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                    <br>Servicios<br>
                                </div>
                                <div class="circle-tile-number text-faded">
                                      <center><?=$qtyServicios?></center>
                                    <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                </div>
                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading green">
                                    <i class="fa fa-book fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content green">
                                <div class="circle-tile-description text-faded">
                                    <br>Comités 
                                </div>
                                <div class="circle-tile-number text-faded">
                                     <center><?=$qtyComites?></center>
                                    <span id="sparklineB"><canvas style="display: inline-block; width: 24px; height: 24px; vertical-align: top;" width="24" height="24"></canvas></span>
                                </div>
                                <a href="#" class="circle-tile-footer">Mas detalles <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>

       
                   
               
               
                </div>
                <!-- end DASHBOARD CIRCLE TILES -->