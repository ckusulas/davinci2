;(function () {

	'use strict';

	// Detectar iOS (iPad/iPhone)
	var isiPad = function(){
		return (navigator.platform.indexOf("iPad") != -1);
	};

	var isiPhone = function(){
	    return (
			(navigator.platform.indexOf("iPhone") != -1) ||
			(navigator.platform.indexOf("iPod") != -1)
	    );
	};

	// Slider de página principal
	var owlCrouselFeatureSlide = function() {

		var owl = $('.owl-carousel');

		owl.on('initialized.owl.carousel change.owl.carousel',function(elem){
			var current = elem.item.index;
			$(elem.target).find(".owl-item").eq(current).find(".to-animate").removeClass('fadeInUp animated');
			$(elem.target).find(".owl-item").eq(current).find(".to-animate-2").removeClass('fadeInUp animated');

		});
		owl.on('initialized.owl.carousel changed.owl.carousel',function(elem){
			setTimeout(function(){
				var current = elem.item.index;
				$(elem.target).find(".owl-item").eq(current).find(".to-animate").addClass('fadeInUp animated');
			}, 1200);
			setTimeout(function(){
				var current = elem.item.index;
				$(elem.target).find(".owl-item").eq(current).find(".to-animate-2").addClass('fadeInUp animated');
			}, 1300);
     	});
		owl.owlCarousel({
			items: 1,
		    loop: true,
		    margin: 0,
		    responsiveClass: true,
		    nav: true,
		    dots: true,
		    autoHeight: true,
		    smartSpeed: 1200,
		    autoplay: false,
			autoplayTimeout: 5000,
			autoplayHoverPause: false,
		    navText: [
		      "<i class='icon-arrow-left2 owl-direction'></i>",
		      "<i class='icon-arrow-right2 owl-direction'></i>"
	     	]
		});

	};

	// Animación waypoint/animate
	var contentWayPoint = function() {

		$('.animate-box').waypoint( function( direction ) {

			if( direction === 'down' && !$(this).hasClass('animated') ) {

				$(this.element).addClass('fadeInUp animated');

			}

		} , { offset: '75%' } );

	};


	// Menu
	var burgerMenu = function() {

		$('body').on('click', '.js-contraloria-nav-toggle', function(event){

			if ( $('#navbar').is(':visible') ) {
				$(this).removeClass('active');
			} else {
				$(this).addClass('active');
			}

			event.preventDefault();

		});

	};


	// Scroll
	var windowScroll = function() {
		var lastScrollTop = 0;

		$(window).scroll(function(event){

		   	var header = $('#contraloria-header'),
				scrlTop = $(this).scrollTop();

			if ( scrlTop > 72 && scrlTop <= 2000 ) {
				header.addClass('navbar-fixed-top');
				//$('.sub-header').addClass('menu-fijo');
			} else if ( scrlTop <= 72) {
				if ( header.hasClass('navbar-fixed-top') ) {
					header.addClass('navbar-fixed-top');
					//$('.sub-header').removeClass('menu-fijo');
					header.removeClass('navbar-fixed-top');
				}
			}

		});
	};


	// Acerca de
	var aboutAnimate = function() {

		if ( $('#nosotros').length > 0 ) {
			$('#nosotros .to-animate').each(function( k ) {

				var el = $(this);

				setTimeout ( function () {
					el.addClass('fadeInUp animated');
				},  k * 200, 'easeInOutExpo' );

			});
		}

	};
	var aboutWayPoint = function() {

		if ( $('#nosotros').length > 0 ) {
			$('#nosotros').waypoint( function( direction ) {

				if( direction === 'down' && !$(this).hasClass('animated') ) {


					setTimeout(aboutAnimate, 200);


					$(this.element).addClass('animated');

				}
			} , { offset: '95%' } );
		}

	};


	// Programas
	var featuresAnimate = function() {

		if ( $('#contraloria-features').length > 0 ) {
			$('#contraloria-features .to-animate').each(function( k ) {

				var el = $(this);

				setTimeout ( function () {
					el.addClass('fadeInUp animated');
				},  k * 200, 'easeInOutExpo' );

			});
		}

	};
	var featuresWayPoint = function() {

		if ( $('#contraloria-features').length > 0 ) {
			$('#contraloria-features').waypoint( function( direction ) {

				if( direction === 'down' && !$(this).hasClass('animated') ) {


					setTimeout(function(){
						$('.animate-features-1').addClass('animated fadeIn');
					}, 100);
					setTimeout(function(){
						$('.animate-features-2').addClass('animated fadeIn');
					}, 200);
					setTimeout(featuresAnimate, 500);
					setTimeout(function(){
						$('.animate-features-3').addClass('animated fadeInUp');
					}, 1400);


					$(this.element).addClass('animated');

				}
			} , { offset: '95%' } );
		}

	};


	// Contenido
	var contactoAnimate = function() {

		if ( $('#contraloria-contacto').length > 0 ) {
			$('#contraloria-contacto .to-animate').each(function( k ) {

				var el = $(this);

				setTimeout ( function () {
					el.addClass('fadeInUp animated');
				},  k * 200, 'easeInOutExpo' );

			});
		}

	};
	var contactoWayPoint = function() {

		if ( $('#contraloria-contacto').length > 0 ) {
			$('#contraloria-contacto').waypoint( function( direction ) {

					setTimeout(function(){
						$('.animate-contacto-1').addClass('animated fadeIn');
					}, 200);
					setTimeout(function(){
						$('.animate-contacto-2').addClass('animated fadeIn');
					}, 300);
					setTimeout(contactoAnimate, 700);


					$(this.element).addClass('animated');


			} , { offset: '95%' } );
		}

	};

	// Lanzar scripts
	$(function(){

		burgerMenu();
		owlCrouselFeatureSlide();
		windowScroll();
		aboutWayPoint();
		featuresWayPoint();
		contactoWayPoint();

	});


}());


	// Función para scroll animado
	function scrollToDown() {

		$('.flecha-abajo').on('click',function(){
			$('html, body').animate({
			scrollTop: $('#contraloria-features').offset().top -30
			}, 1000);
			return false;
		});

		$('#arriba').on('click',function(){
			$("html, body").animate({ scrollTop: 0 }, 1000);
			return false;
		});

		$('.panel-collapse').on('shown.bs.collapse', function (e) {
		    var $panel = $(this).closest('.panel');
		    $('html,body').animate({
		        scrollTop: $panel.offset().top -100
		    }, 500);
		});

	}
	scrollToDown();


	$(function() {

		$('#campos1').hide();
		if ($('#difusion').val() == 'Sí') {
			$('#campos1').show();
		}
		else {
			$('#campos1').hide();
		}

		$('#campos2').hide();
		if ($('#integracion').val() == 'Sí') {
			$('#campos2').show();
		}
		else {
			$('#campos2').hide();
		}

		$('#campos3').hide();
		if ($('select#seguimiento').val() == 'Sí') {
			$('#campos3').show();
		}
		else {
			$('#campos3').hide();
		}

		$('#campos4').hide();
		if ($('#resultado_acciones').val() == 'Sí') {
			$('#campos4').show();
		}
		else {
			$('#campos4').hide();
		}

		$('#campos5').hide();
		if ($('#figura').val() == 'Comité') {
			$('#campos5').show();
		}
		else if ($('#figura').val() == 'Beneficiario') {
			$('#campos5').hide();
		}

		$('#campos6').hide();
		if ($('#compromiso').val() == 'Sí') {
			$('#campos6').show();
		}
		else {
			$('#campos6').hide();
		}

		$('#campos7').hide();
		if ($('#promociones').val() == 'Sí') {
			$('#campos7').show();
		}
		else {
			$('#campos7').hide();
		}

		$('#campos8').hide();
		if ($('#recursos').val() == 'Federal') {
			$('#campos8').show();
		}
		else {
			$('#campos8').hide();
		}

		$('#linea-seg').hide();

		$('#seguimiento_estatal').hide();
		$('#seguimiento_federal').hide();
		if ($('#esquema_normativo').val() == 'Estatal') {
			$('#seguimiento_estatal').show();
			$('#seguimiento_federal').hide();
			$('#headingFour a').html('Seguimiento <small>— Esquema normativo Estatal</small>');
		}
		else if ($('#esquema_normativo').val() == 'Federal') {
			$('#seguimiento_estatal').hide();
			$('#seguimiento_federal').show();
			$('#headingFour a').html('Seguimiento <small>— Esquema normativo Federal</small>');
		}

		$('#difusion').change(function(){
			if($('#difusion').val() == 'Sí') {
				$('#campos1').fadeIn();
			} else {
				$('#campos1').hide();
			}
		});
		$('#integracion').change(function(){
			if($('#integracion').val() == 'Sí') {
				$('#campos2').fadeIn();
			} else {
				$('#campos2').hide();
			}
		});
		$('select#seguimiento').change(function(){
			if($('select#seguimiento').val() == 'Sí') {
				$('#campos3').fadeIn();
			} else {
				$('#campos3').hide();
				$('#campos4').hide();
				$('#resultado_acciones').val('');
			}
		});
		$('#resultado_acciones').change(function(){
			if($('#resultado_acciones').val() == 'Sí') {
				$('#campos4').fadeIn();
			} else {
				$('#campos4').hide();
			}
		});
		$('#figura').change(function(){
			if($('#figura').val() == 'Comité') {
				$('#campos5').fadeIn();
			} else {
				$('#campos5').hide();
			}
		});
		$('#compromiso').change(function(){
			if($('#compromiso').val() == 'Sí') {
				$('#campos6').fadeIn();
			} else {
				$('#campos6').hide();
			}
		});
		$('#promociones').change(function(){
			if($('#promociones').val() == 'Sí') {
				$('#campos7').fadeIn();
			} else {
				$('#campos7').hide();
			}
		});
		$('#recursos').change(function(){
			if($('#recursos').val() == 'Federal') {
				$('#campos8').fadeIn();
			} else {
				$('#campos8').hide();
			}
		});
		$('#accion').change(function(){
			if($('#accion').val() == 'Obra') {
				$('#campos2').fadeIn();
				$('#integracion').val('Sí');
			} else {
				$('#campos2').hide();
				$('.integracion-ad #integracion').val('');
				$('.integracion-us #integracion').val('');
			}
		});
		$('#esquema_normativo').change(function(){
			if ($('#esquema_normativo').val() == 'Federal') {
				$('#seguimiento_federal').fadeIn();
				$('#seguimiento_estatal').hide();
				$('#linea-seg').fadeIn();
				$('#headingFour a').html('Seguimiento <small>— Esquema normativo Federal</small>');
			} else if ($('#esquema_normativo').val() == 'Estatal') {
				$('#seguimiento_estatal').fadeIn();
				$('#seguimiento_federal').hide();
				$('#linea-seg').fadeIn();
				$('select#seguimiento').val('No');
				$('#headingFour a').html('Seguimiento <small>— Esquema normativo Estatal</small>');
			} else {
				$('#seguimiento_estatal').hide();
				$('#seguimiento_federal').hide();
				$('#linea-seg').hide();
				$('#headingFour a').html('Seguimiento');
			}
		});

		$(".fecha").dateDropper();

	});


	(function() {

	    $('#submit, .siguiente, .titulo-enlace, input[type="submit" i]').attr('disabled', 'disabled');
		$('#siguiente-uno, #siguiente-dos, #siguiente-tres, #siguiente-cuatro, #siguiente-cinco').attr('disabled', 'disabled');
		$('#filtrar').removeAttr('disabled');

		$('form #collapseOne .validar').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseOne .validar').each(function() {
	            if ($(this).val() == '') {
	                empty = true;
	            }
	        });

	        if (empty) {
	            $('#siguiente-uno, #headingTwo .titulo-enlace').attr('disabled', 'disabled');
				$('#submit').attr('disabled', 'disabled');
				$('#collapseOne input[type="submit" i]').removeAttr('disabled');
				$('#collapseOne .mensaje').fadeIn();
	        } else {
	            $('#siguiente-uno, #headingTwo .titulo-enlace').removeAttr('disabled');
				$('#collapseOne input[type="submit" i]').removeAttr('disabled');
				$('#submit').removeAttr('disabled');
				$('.siguiente.two').removeAttr('disabled');
				$('#collapseOne .mensaje').fadeOut();
	        }

	    });

		$('form #collapseTwo .validar').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseTwo .validar').each(function() {
	            if ($(this).val() == '') {
	                empty = true;
	            }
	        });

	        if (empty) {
	            $('#siguiente-dos, #headingThree .titulo-enlace').attr('disabled', 'disabled');
				$('#submit').attr('disabled', 'disabled');
				$('#collapseTwo input[type="submit" i]').removeAttr('disabled');
				$('#collapseTwo .mensaje').fadeIn();
	        } else {
	            $('#siguiente-dos, #headingThree .titulo-enlace').removeAttr('disabled');
				$('#collapseTwo input[type="submit" i]').removeAttr('disabled');
				$('.siguiente.three').removeAttr('disabled');
				$('#submit').removeAttr('disabled');
				$('#collapseTwo .mensaje').fadeOut();
	        }

	    });

		$('form #collapseThree .validar').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseThree .validar').each(function() {
	            if ($(this).val() == '') {
	                empty = true;
	            }
	        });

	        if (empty) {
	            $('#siguiente-tres, #headingFour .titulo-enlace').attr('disabled', 'disabled');
				$('#collapseThree input[type="submit" i]').removeAttr('disabled');
				$('#collapseThree .mensaje').fadeIn();
	        } else {
	            $('#siguiente-tres, #headingFour .titulo-enlace').removeAttr('disabled');
				$('#collapseThree input[type="submit" i]').removeAttr('disabled');
				$('.siguiente.four').removeAttr('disabled');
				$('#submit').removeAttr('disabled');
				$('#collapseThree .mensaje').fadeOut();
	        }

	    });

		$('form #collapseFour .validar').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseFour .validar').each(function() {
	            if ($(this).val() == '') {
	                empty = true;
	            }
	        });

	        if (empty) {
	            $('#siguiente-cuatro, #headingFive .titulo-enlace').attr('disabled', 'disabled');
				$('#collapseFour input[type="submit" i]').removeAttr('disabled');
				$('#collapseFour .mensaje').fadeIn();
	        } else {
	            $('#siguiente-cuatro, #headingFive .titulo-enlace').removeAttr('disabled');
				$('#collapseFour input[type="submit" i]').removeAttr('disabled');
				$('.siguiente.five').removeAttr('disabled');
				$('#submit').removeAttr('disabled');
				$('#collapseFour .mensaje').fadeOut();
	        }

	    });

		$('form #collapseFive .validar').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseFive .validar').each(function() {
	            if ($(this).val() == '') {
	                empty = true;
	            }
	        });

	        if (empty) {
	            $('#collapseFive input[type="submit" i]').removeAttr('disabled');
				$('#collapseFive .mensaje').fadeIn();
	        } else {
	            $('#collapseFive input[type="submit" i]').removeAttr('disabled'); $('#submit').removeAttr('disabled');
				$('#collapseFive .mensaje').fadeOut();
	        }

	    });

		// Desactivar guardar folio al cambiar cualquier campo
		$('form #collapseOne .form-control').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseOne .form-control').each(function() {
				if ($(this).is(":visible")) {
		            if ($(this).val() == '') {
		                empty = true;
		            }
				}
	        });

	        if (empty) {
				$('#submit').attr('disabled', 'disabled');
				$(".mensaje span").hide().html('<em>Debe rellenar todos los campos para poder accerder al siguiente bloque.</em>').fadeIn('slow');
	        } else {
				$('#submit').removeAttr('disabled');
	        }

	    });

		$('form #collapseTwo .form-control').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseTwo .form-control').each(function() {
				if ($(this).is(":visible")) {
		            if ($(this).val() == '') {
		                empty = true;
		            }
				}
	        });

	        if (empty) {
				$('#submit').attr('disabled', 'disabled');
				$(".mensaje span").hide().html('<em>Debe rellenar todos los campos para poder accerder al siguiente bloque.</em>').fadeIn('slow');
	        } else {
				$('#submit').removeAttr('disabled');
	        }

	    });

		$('form #collapseThree .form-control').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseThree .form-control').each(function() {
				if ($(this).is(":visible")) {
		            if ($(this).val() == '') {
		                empty = true;
		            }
				}
	        });

	        if (empty) {
				$('#submit').attr('disabled', 'disabled');
				$(".mensaje span").hide().html('<em>Debe rellenar todos los campos para poder accerder al siguiente bloque.</em>').fadeIn('slow');
	        } else {
				$('#submit').removeAttr('disabled');
	        }

	    });

		$('form #collapseFour .form-control').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseFour .form-control').each(function() {
				if ($(this).is(":visible")) {
		            if ($(this).val() == '') {
		                empty = true;
		            }
				}
	        });

	        if (empty) {
				$('#submit').attr('disabled', 'disabled');
				$(".mensaje span").hide().html('<em>Debe rellenar todos los campos para poder accerder al siguiente bloque.</em>').fadeIn('slow');
	        } else {
				$('#submit').removeAttr('disabled');
	        }

	    });

		$('form #collapseFive .form-control').bind("change keyup", function(event) {

	        var empty = false;
	        $('form #collapseFive .form-control').each(function() {
				if ($(this).is(":visible")) {
		            if ($(this).val() == '') {
		                empty = true;
		            }
				}
	        });

	        if (empty) {
				$('#submit').attr('disabled', 'disabled');
				$(".mensaje span").hide().html('<em>Debe rellenar todos los campos para poder guardar y cerrar el folio.</em>').fadeIn('slow');
	        } else {
				$('#submit').removeAttr('disabled');
	        }

	    });

	})()

	$('form #collapseOne .form-control').bind("change keyup", function(event) {
		$('#collapseOne input[type="submit" i]').removeAttr('disabled');
	});
	$('form #collapseTwo .form-control').bind("change keyup", function(event) {
		$('#collapseTwo input[type="submit" i]').removeAttr('disabled');
	});
	$('form #collapseThree .form-control').bind("change keyup", function(event) {
		$('#collapseThree input[type="submit" i]').removeAttr('disabled');
	});
	$('form #collapseFour .form-control').bind("change keyup", function(event) {
		$('#collapseFour input[type="submit" i]').removeAttr('disabled');
	});
	$('form #collapseFive .form-control').bind("change keyup", function(event) {
		$('#collapseFive input[type="submit" i]').removeAttr('disabled');
	});

	window.onload = function() {

		if ($('#collapseOne.in').length > 0) {
			$('.siguiente.one, #headingOne .titulo-enlace').removeAttr('disabled');
			$('.siguiente.one').addClass('cap');
		}
		if ($('#collapseTwo.in').length > 0) {
			$('.siguiente.one, .siguiente.two, #headingOne .titulo-enlace, #headingTwo .titulo-enlace').removeAttr('disabled');
			$('.siguiente.two').addClass('cap');
		}
		if ($('#collapseThree.in').length > 0) {
			$('.siguiente.one, .siguiente.two, .siguiente.three, #headingOne .titulo-enlace, #headingTwo .titulo-enlace, #headingThree .titulo-enlace').removeAttr('disabled');
			$('.siguiente.three').addClass('cap');
		}
		if ($('#collapseFour.in').length > 0) {
			$('.siguiente.one, .siguiente.two, .siguiente.three, .siguiente.four, #headingOne .titulo-enlace, #headingTwo .titulo-enlace, #headingThree .titulo-enlace, #headingFour .titulo-enlace').removeAttr('disabled');
			$('.siguiente.four').addClass('cap');
		}
		if ($('#collapseFive.in').length > 0) {
			$('.siguiente.one, .siguiente.two, .siguiente.three, .siguiente.four, .siguiente.five, #headingOne .titulo-enlace, #headingTwo .titulo-enlace, #headingThree .titulo-enlace, #headingFour .titulo-enlace, #headingFive .titulo-enlace').removeAttr('disabled');
			$('.siguiente.five').addClass('cap');
		}

		var tres = true;
		$('form #collapseThree .validar').each(function() {
			if ($(this).val() != '') {
				tres = true;
			} else {
				tres = false;
			}
		});
		if (tres) {
			$('#collapseThree .siguiente, #siguiente-tres, #headingFour .titulo-enlace, .siguiente.four').removeAttr('disabled');
			$('#collapseThree .mensaje').fadeOut();
			$('#collapseThree input[type="submit" i]').attr('disabled', 'disabled');
			$('#collapseThree').removeClass('in');
			$('#collapseFour').addClass('in');
			$('.panel.tres').addClass('amarillo');
		}

		var cuatro = true;
		$('form #collapseFour .validar').each(function() {
			if ($(this).val() != '') {
				cuatro = true;
			} else {
				cuatro = false;
			}
		});
		if (cuatro) {
			$('#collapseFour .siguiente, #siguiente-cuatro, #headingFive .titulo-enlace, .siguiente.five').removeAttr('disabled');
			$('#collapseFour .mensaje').fadeOut();
			$('#collapseFour input[type="submit" i]').attr('disabled', 'disabled');
			$('#collapseFour').removeClass('in');
			$('#collapseFive').addClass('in');
			$('.panel.cuatro').addClass('amarillo');
		}

		var cinco = true;
		$('form #collapseFive .validar').each(function() {
			if ($(this).val() != '') {
				cinco = true;
			} else {
				cinco = false;
			}
		});
		if (cinco) {
			$('#collapseFive .mensaje').fadeOut();
			$('#collapseFour input[type="submit" i]').attr('disabled', 'disabled');
			$('#collapseFive').removeClass('in');
			//$('#collapseTwo').addClass('in');
			$('.panel.cinco').addClass('amarillo');
		}

		// Ocultar modulos sin registros en la vista de folio
		var integracion = false;
		var capacitacion = false;
		var seguimiento = false;
		var promocion = false;
		var msj = '<div class="col-xs-12 columna"><div class="row campo"><span style="font-size:13px;">Aun no se capturan registros de este módulo.</div></div>';

		$('body#registro #integracion input').each(function() {
			if ($(this).val() != '---') { integracion = true; }
		});
		if (integracion) {
			//
		} else {
			$('body#registro #integracion .panel-body').html(msj);
		}

		$('body#registro #capacitacion input').each(function() {
			if ($(this).val() != '---') { capacitacion = true; }
		});
		if (capacitacion) {
			//
		} else {
			$('body#registro #capacitacion .panel-body').html(msj);
		}

		$('body#registro #seguimiento input').each(function() {
			if ($(this).val() != '---') { seguimiento = true; }
		});
		if (seguimiento) {
			//
		} else {
			$('body#registro #seguimiento .panel-body').html(msj);
		}

		$('body#registro #promocion input').each(function() {
			if ($(this).val() != '---') { promocion = true; }
		});
		if (promocion) {
			//
		} else {
			$('body#registro #promocion .panel-body').html(msj);
		}

	}

	$('#headingOne .titulo-enlace, #anterior-dos').on('click', function(event) {
		$('.siguiente').removeClass('cap');
		$('.siguiente.one').addClass('cap');
	});
	$('#siguiente-uno, #headingTwo .titulo-enlace, #anterior-tres').on('click', function(event) {
		$('.siguiente').removeClass('cap');
		$('.siguiente.two').addClass('cap');
	});
	$('#siguiente-dos, #headingThree .titulo-enlace, #anterior-cuatro').on('click', function(event) {
		$('.siguiente').removeClass('cap');
		$('.siguiente.three').addClass('cap');
	});
	$('#siguiente-tres, #headingFour .titulo-enlace, #anterior-cinco').on('click', function(event) {
		$('.siguiente').removeClass('cap');
		$('.siguiente.four').addClass('cap');
	});
	$('#siguiente-cuatro, #headingFive .titulo-enlace').on('click', function(event) {
		$('.siguiente').removeClass('cap');
		$('.siguiente.five').addClass('cap');
	});


	// Mostrar valores en combo select dependiente
	$(document).ready(function() {
		$("#municipio").change(function() {
			$("#municipio option:selected").each(function() {
				municipio = $('#municipio').val();
				$.post("captura/get_localidades", {
					municipio : municipio
				}, function(data) {
					$("#localidad").html(data);
				});
			});
		})
	});

	$(document).ready(function() {
		$("#recursos").change(function() {
			$("#subprograma").html('<option value="">Seleccionar</option>');;
			$("#recursos option:selected").each(function() {
				recursos = $('#recursos').val();
				$.post("captura/get_programas", {
					recursos : recursos
				}, function(data) {
					$("#programa").html(data);
				});
			});
		})
	});

	$(document).ready(function() {
		$("#programa").change(function() {
			$("#programa option:selected").each(function() {
				programa = $('#programa').val();
				$.post("captura/get_subprogramas", {
					programa : programa
				}, function(data) {
					$("#subprograma").html(data);
				});
			});
		})
	});


	// Habilitar multiselect
	$('#medio').multiselect({
	    checkboxName: function(option) {
	        return 'medio[]';
	    }
	});
	$('#tipo').multiselect({
	    checkboxName: function(option) {
	        return 'tipo[]';
	    }
	});

	// Sumar inputs
	$(document).ready(function(){

		$("#beneficiarios_h,#beneficiarios_m").change(function(){

			var beneficiarios = 0;
			beneficiarios = Number($("#beneficiarios_h").val());
			beneficiarios += Number($("#beneficiarios_m").val());
			$("#beneficiarios").val(beneficiarios);

		});

		$("#miembros_h,#miembros_m").change(function(){

			var miembros = 0;
			miembros = Number($("#miembros_h").val());
			miembros += Number($("#miembros_m").val());
			$("#miembros").val(miembros);

		});

		$("#integrantes_h,#integrantes_m").change(function(){

			var integrantes = 0;
			integrantes = Number($("#integrantes_h").val());
			integrantes += Number($("#integrantes_m").val());
			$("#integrantes_totales").val(integrantes);

			var porcentaje_h = 0;
			var integrantes_h = $("#integrantes_h").val();
			var total = $("#integrantes_totales").val();
			porcentaje_h = integrantes_h * 100 / total;
			decimal_h = porcentaje_h.toFixed(2);
			$("#porcentaje_h").val(decimal_h);

			var porcentaje_m = 0;
			var integrantes_m = $("#integrantes_m").val();
			var total = $("#integrantes_totales").val();
			porcentaje_m = integrantes_m * 100 / total;
			decimal_m = porcentaje_m.toFixed(2);
			$("#porcentaje_m").val(decimal_m);

		});

	});

	// Mostrar/Ocultar filtro
	$('#consulta #contenedor-filtro').hide();

	$(document).ready(function() {

		$('#filtro').click(function() {

			$(this).toggleClass("clic");
			$('#contenedor-filtro').slideToggle( "slow", function() {
		    	// Animación
		  	});

		});

	});

	//Sumar fechas
	$(document).ready(function() {

		$('#fecha_inicio,#fecha_capacitacion').change(function() {

			var date1 = $("#fecha_inicio").val();
			var date2 = $("#fecha_capacitacion").val();

			var fecha1 = moment(date1);
			var fecha2 = moment(date2);

			diferencia = fecha2.diff(fecha1, 'days'), ' dias de diferencia';

			$("#diferencia_cap").val(diferencia);

		});

		$('#fecha_inicio,#fecha_constitucion').change(function() {

			var date3 = $("#fecha_inicio").val();
			var date4 = $("#fecha_constitucion").val();

			var fecha3 = moment(date3);
			var fecha4 = moment(date4);

			diferencia = fecha4.diff(fecha3, 'days'), ' dias de diferencia';

			$("#diferencia_int").val(diferencia);

		});

	});

	// Selección Inicio
	$('.menu-sel a').on('click', function() {
		$('.menu-sel a').removeClass('sel');
		$(this).addClass('sel');
		$('.menu-sel a').find('.fa').remove();
		$('.menu-sel a').prepend('<i class="fa fa-file-text-o" aria-hidden="true"></i>');
		$(this).find('.fa').remove();
		$(this).prepend('<i class="fa fa-file-text" aria-hidden="true"></i>');
		$('#cont-ini panel-heading').fadeOut();
		$('#cont-ini panel-body').fadeOut();
		$('#cont-ini panel-footer').fadeOut();
		nombre = $(this).attr('id');
		$.post("sistema/get_"+nombre, function(data) {
			$("#cont-ini").hide().html(data).fadeIn('slow');
		});
	});

	// Selección captura
	$('.menu-cap a').on('click', function() {
		$('.menu-cap a').removeClass('cap');
		$(this).addClass('cap');
		$('.menu-cap a').find('.fa').remove();
		$('.menu-cap a').prepend('<i class="fa fa-file-text-o" aria-hidden="true"></i>');
		$(this).find('.fa').remove();
		$(this).prepend('<i class="fa fa-file-text" aria-hidden="true"></i>');
	});

	// Guardar formulario parcialmente (por sección)
	$('#collapseOne #guardar-registro').click(function() {
		id = $("#collapseOne .mensaje span").attr('id');
		if(id == null ) {
			$.ajax({
	            type: 'POST',
	            url: 'captura/guardar_registro_ajax',
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseOne .mensaje").html("<em>Guardando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
					folio = $("#collapseOne .mensaje span").attr('id');
					$('#registro').attr('action','captura/actualizar_registro/'+folio);
	            }
	        })
			$('#folio-captura').attr('id','folio-guardado');
			$('#folio').attr('id','folio-asignado');

	        return false;
		} else {
			$.ajax({
	            type: 'POST',
	            url: 'captura/actualizar_registro_ajax/'+id,
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseOne .mensaje").html("<em>Actualizando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
	            }
	        })
	        return false;
		}
	});
	$('#collapseTwo #guardar-registro').click(function() {
		id = $("#collapseOne .mensaje span").attr('id');
		if(id == null ) {
			$.ajax({
	            type: 'POST',
	            url: 'captura/guardar_registro_ajax',
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseTwo .mensaje").html("<em>Guardando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
					folio = $("#collapseOne .mensaje span").attr('id');
					$('#registro').attr('action','captura/actualizar_registro/'+folio);
	            }
	        })
			$('#folio-captura').attr('id','folio-guardado');
			$('#folio').attr('id','folio-asignado');

	        return false;
		} else {
			$.ajax({
	            type: 'POST',
	            url: 'captura/actualizar_registro_ajax/'+id,
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseTwo .mensaje").html("<em>Actualizando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
	            }
	        })
	        return false;
		}
	});
	$('#collapseThree #guardar-registro').click(function() {
		id = $("#collapseOne .mensaje span").attr('id');
		if(id == null ) {
			$.ajax({
	            type: 'POST',
	            url: 'captura/guardar_registro_ajax',
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseThree .mensaje").html("<em>Guardando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
					folio = $("#collapseOne .mensaje span").attr('id');
					$('#registro').attr('action','captura/actualizar_registro/'+folio);
	            }
	        })
			$('#folio-captura').attr('id','folio-guardado');
			$('#folio').attr('id','folio-asignado');

	        return false;
		} else {
			$.ajax({
	            type: 'POST',
	            url: 'captura/actualizar_registro_ajax/'+id,
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseThree .mensaje").html("<em>Actualizando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
	            }
	        })
	        return false;
		}
	});
	$('#collapseFour #guardar-registro').click(function() {
		id = $("#collapseOne .mensaje span").attr('id');
		if(id == null ) {
			$.ajax({
	            type: 'POST',
	            url: 'captura/guardar_registro_ajax',
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseFour .mensaje").html("<em>Guardando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
					folio = $("#collapseOne .mensaje span").attr('id');
					$('#registro').attr('action','captura/actualizar_registro/'+folio);
	            }
	        })
			$('#folio-captura').attr('id','folio-guardado');
			$('#folio').attr('id','folio-asignado');

	        return false;
		} else {
			$.ajax({
	            type: 'POST',
	            url: 'captura/actualizar_registro_ajax/'+id,
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseFour .mensaje").html("<em>Actualizando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
	            }
	        })
	        return false;
		}
	});
	$('#collapseFive #guardar-registro').click(function() {
		id = $("#collapseOne .mensaje span").attr('id');
		if(id == null ) {
			$.ajax({
	            type: 'POST',
	            url: 'captura/guardar_registro_ajax',
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseFive .mensaje").html("<em>Guardando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
					folio = $("#collapseOne .mensaje span").attr('id');
					$('#registro').attr('action','captura/actualizar_registro/'+folio);
	            }
	        })
			$('#folio-captura').attr('id','folio-guardado');
			$('#folio').attr('id','folio-asignado');

	        return false;
		} else {
			$.ajax({
	            type: 'POST',
	            url: 'captura/actualizar_registro_ajax/'+id,
	            data: $('#registro').serialize(),
				beforeSend: function() {
					$("#collapseFive .mensaje").html("<em>Actualizando, espere por favor...</em>");
				},
	            success: function(data) {
	                $(".mensaje").hide().html(data).fadeIn('slow');
	            }
	        })
	        return false;
		}
	});


	// Habilitar tooltips
	$(document).ready(function(){
    	$('[data-toggle="tooltip"]').tooltip();
	});

	// Mantener fijo el menú del formulario de captura
	$(function() {

		if ($("#sidebar").length > 0) {

	        var offset = $("#sidebar").offset();
	        var topPadding = 100;

	        $(window).scroll(function() {

	            if ($("#sidebar").height() < $(window).height() && $(window).scrollTop() >= 170) {
	                $("#sidebar").stop().animate({
	                    top: $(window).scrollTop() - offset.top +topPadding
	                });
	            } else {
	                $("#sidebar").stop().animate({
	                    top: 0
	                });
	            }

	        });

		}

	});


	$(document).ready(function(){

		var identificador = document.body.id;

		if (identificador == 'captura') {

			var countdown = 30 * 60 * 1000;
			var activityTimeout = setTimeout(inActive, countdown);

			function inActive(){

				$(document.body).removeClass('active');
				$(document.body).addClass('inactive');
				swal({
					title: "¿Deseas continuar?",
					text: "Han pasado 30 minutos de inactividad, ¿Deseas continuar realizando el registro?",
					type: 'warning',
					showCancelButton: true,
					cancelButtonColor: '#4cb58b',
					confirmButtonColor: '#ff0000',
					confirmButtonText: 'Cerrar registro',
					cancelButtonText: 'Continuar',
					focusConfirm: false,
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function(){
					window.location.href="consulta";
				});

			}

			function resetActive(){

				$(document.body).removeClass('inactive');
				$(document.body).addClass('active');
				clearTimeout(activityTimeout);
				activityTimeout = setTimeout(inActive, countdown);

			}

			$(document).mousemove(function(event){
				resetActive();
			});

			window.onbeforeunload = function(e) {
				return true;
			}

	        $("#submit").on("click", function() {
				window.onbeforeunload = function(){ null }
	        });

		} else if (identificador == 'seguimiento') {

			var countdown = 30 * 60 * 1000;
			var activityTimeout = setTimeout(inActive, countdown);

			function inActive(){

				$(document.body).removeClass('active');
				$(document.body).addClass('inactive');
				swal({
					title: "¿Deseas continuar?",
					text: "Han pasado 30 minutos de inactividad, ¿Deseas continuar realizando el registro?",
					type: 'warning',
					showCancelButton: true,
					cancelButtonColor: '#4cb58b',
					confirmButtonColor: '#ff0000',
					confirmButtonText: 'Cerrar registro',
					cancelButtonText: 'Continuar',
					focusConfirm: false,
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function(){
					window.location.href="consulta";
				});

			}

			function resetActive(){

				$(document.body).removeClass('inactive');
				$(document.body).addClass('active');
				clearTimeout(activityTimeout);
				activityTimeout = setTimeout(inActive, countdown);

			}

			$(document).mousemove(function(event){
				resetActive();
			});

			window.onbeforeunload = function(e) {
				return true;
			}

	        $("#submit").on("click", function() {
				window.onbeforeunload = function(){ null }
	        });

		}

	});

	// Grabar en el DOM valores de inputs para impresión
	$('form#formato input').bind("change", function(e) {
		var valor = $(this).val();
		$(this).attr('value',valor);
	});
