
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>

       <?php $this->view('template/header.php'); ?>   
</head>

<body  >
              <!--Modal para agregado de Nuevos Programas -->
  <?php $this->view('template/mod_nuevo_programa'); ?>

    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
           

                <!-- begin PAGE TITLE ROW -->
              <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                <tr>
                                    <td>
                                       <h1>
                                           
                                        </h1>
                                     </td>
                                     <td align="right">  


                                        <button id="btnNuevo" type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal-dialog"  onClick='cleanElement()'><i class="fa fa-floppy-o"></i> Nuevo</button>   
                                        

                                        <?php
                                            if($editData['onlyView'] == 1){

                                        ?>

                                          <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='editar()'><i class="fa fa-floppy-o"></i> Editar</button>

                                        <?php
                                            }
                                        ?>

                                                                                

                                            <button type="button"   id="btnGuardar" class="btn btn-success waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                     </td>
                                </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Obras / Apoyos / Servicios</i>
                                </li>
                                <li class="active">Registar Obras /Apoyos /Servicios </li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                            <input type="hidden" name="v_recurso" id="v_recurso">
                            <input type="hidden" name="v_fecha_asignacion_federal" id="v_fecha_asignacion_federal">
                            <input type="hidden" name="v_fecha_asignacion_estatal" id="v_fecha_asignacion_estatal">
                             

                        </div>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
                
                               <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Totales</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#totales"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="totales" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_vigila">




                                              
                                                <table border=0  width="100%">
                                                    <tr>
                                                        <td>
                                                    <label  class="col-sm-6 text-right">Total de Recurso Asignado (A Vigilar):</label>
                                                    <div class="col-sm-4">
                                                         <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control" id="totalRecursoAsignadoVigilar" name="totalRecursoAsignadoVigilar" value="<?php echo $editData['recurso_asignado_vigilar'] ?>"  disabled></div>
                                                    </div>
                                                    </td>
                                                    <td>
                                                
                                                    <label  class="col-sm-6 text-right">Total de Recurso Ejecutado (Vigilado):</label>
                                                    <div class="col-sm-4">
                                                         <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control" id="totalRecursoEjecutadoVigilado" name="totalRecursoEjecutadoVigilado" value="<?php echo $editData['recurso_ejecutado_vigilado'] ?>" disabled ></div>
                                                    </div>
                                                    </td>
                                                </tr>
                                                </table>


                                                <input type="hidden" id="totalRecursoVigilar" name="totalRecursoVigilar">
                                                <input type="hidden" id="totalRecursoVigilado" name="totalRecursoVigilado">
                                                <input type="hidden" id="id_proyecto_vigila" name="id_proyecto_vigila" value="<?php echo $editData['id_proyecto'] ?>" >
                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                 
                     <!-- Instancia del Proyecto -->

                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>1. Instancia de Obra / Apoyo / Servicio</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#instanciaObra"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="instanciaObra" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_oficio">
 

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5  ">*Oficio de Aprobación en Picaso?:</label>
                                                    <div class="col-sm-6">
                                                       <select class="form-control" id="enPicaso" name="enPicaso" onChange="selectPicaso(this.value)">
                                                            <option value="0">Seleccione...</option>
                                                            <?php 
                                                            $selected = "";
                                                            if($editData['en_picaso']=="Si"){
                                                                $selected = "selected";
                                                            }

                                                            ?>
                                                            <option value="Si" <?php echo $selected?>>Si</option>
                                                            <?php 
                                                            $selected = "";
                                                            if($editData['en_picaso']=="No"){
                                                                $selected = "selected";
                                                            }

                                                            ?>
                                                            <option value="No" <?php echo $selected?>>No</option>
                                                                                                                       
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php

                                                $style="style='display:none'";

                                                if($editData['oficio_aprobacion_picaso']!="" && $editData['en_picaso']=="Si" ){
                                                    $style="";
                                                }
                                               


                                                ?>
                                               <div class="form-group" id="no_picaso" <?=$style?>>
                                                    <label for="textInput" class="col-sm-5  ">*Número Obra en Picaso:</label>
                                                    <div class="col-sm-4" >
                                                        <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9/-\s]/g,'');" class="form-control input-sm"  id="numeroPicaso" name="numeroPicaso"  maxlength="40" value="<?php echo $editData['oficio_aprobacion_picaso'] ?>" > 
                                                    </div>
                                                      <div class="col-sm-2" >
                                                        <input type="button" value="precargar..." onClick="precargar_picaso()">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5  ">*Oficio de Aprobación o Equivalente:</label>
                                                    <div class="col-sm-6" >
                                                        <input type="text"  class="form-control input-sm caja"   id="oficioAprobacion" name="oficioAprobacion"  maxlength="40" value="<?php echo $editData['oficio_aprobacion_equivalente'] ?>" >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label  class="col-sm-5 ">*Fecha Aprobación de la Obra:</label>
                                                    <div class="col-sm-4">
                                                          <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar aprobacionobra"></i>
                                                    </span> 
                                                        <input type="text"  class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-#\s]+$/g,'');" style="text-align:right" id="fechaAprobacionObra" name="fechaAprobacionObra" value="<?php echo $editData['fecha_aprobacion_obra'] ?>" >
                                                    </div></div>
                                                </div>
 


                                                <div class="form-group" id ="tag_ejecutora">
                                                    <label  class="col-sm-5  ">*Instancia Ejecutora:</label>
                                                    <div class="col-sm-4">
                                                         <select class="form-control" id="instanciaEjecutora" name="instanciaEjecutora">
                                                            <option value="0">Seleccione...</option>
                                                                   <?php
                                                                foreach ($listInstanceEjecutora as $row) {
                                                                    $selected = "";
                                                                    if($editData['fk_instancia_ejecutora'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['instancia_ejecutora']) . "</option>\n";
                                                                }
                                                                ?>
                                                       
                                                        
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group" id ="tag_normativa">
                                                        <label for="textInput" class="col-sm-5"><div id="instancia_arriba">Instancia Normativa:</div></label>
                                                        <div class="col-sm-4">

                                                                <select id="cataloInstanciaPromotora" name="cataloInstanciaPromotora" class="form-control">
                                                                        <option value="">Seleccione.. </option>                                                             
                                                                        <?php
                                                                        foreach ($listInstanceEjecutora as $row) {
                                                                            $selected = "";
                                                                            if($editData['fk_instancia_primaria']==$row['id']){
                                                                                $selected = "selected";
                                                                            }
         
                                                                            echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['instancia_ejecutora']) . "</option>\n";
                                                                        }
                                                                        ?>
                                                                </select>
                                                            
                                                        </div>
                  
                                                </div>  
 
                                                   
 
                                            

                            
                                                <input type="hidden" name="instanciaEjecutora_selected" id="instanciaEjecutora_selected">
                                                 <input type="hidden" id="id_proyecto_picaso" name="id_proyecto_picaso" value="<?php echo $editData['id_proyecto'] ?>" >

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Instancia de la Obra -->

                    <!-- begin LEFT COLUMN -->
                    <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-blue">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>2. Datos Generales de Obra / Apoyo / Servicio</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_datosobra">

                                              


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">Nombre del Programa Presupuestario:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" id="nombreProgramaPresupuestario" name="nombreProgramaPresupuestario" value="<?php echo $tituloPrograma?>" disabled >
                                                    </div>
                                                </div>



                                              <?php
                                                $disable="";
                                                if($editData["onlyView"] == 2){
                                                    $disable = "readonly='readonly'";
                                                }else {
                                                    $disable = "";
                                                }

                                                ?>

                                                <div class="form-group">
                                                    <label  class="col-sm-5">*Tipo de Beneficio:</label>
                                                     <div class="col-sm-6">

                                                        <label class="radio-inline">
                                                            <?php 
                                                            $selected = "";
                                                            if($editData['fk_beneficio']==1){
                                                                $selected = "checked='checked'";
                                                            }

                                                            ?>
                                                          <input type="radio" name="optionBeneficio" <?=$disable?>  id="optionBeneficio" <?php echo $selected ?> value="1">Obra
                                                        </label>

                                                          <?php 
                                                            $selected = "";
                                                            if($editData['fk_beneficio']==2){
                                                                $selected = "checked='checked'";
                                                            }

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionBeneficio" <?=$disable?>  id="optionBeneficio" <?php echo $selected ?> value="2">Apoyo
                                                        </label>

                                                             <?php 
                                                            $selected = "";
                                                            if($editData['fk_beneficio']==3){
                                                                $selected = "checked='checked'";
                                                            }

                                                            ?>
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionBeneficio" <?=$disable?> id="optionBeneficio" <?php echo $selected ?> value="3">Servicio
                                                        </label>

                                             
                                                    </div>
                                                </div>

                                                
                                            
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">*Nombre Obra / Apoyo / Servicio:</label>
                                                    <div class="col-sm-6">

                                                          <textarea  <?=$disable?>  class="form-control" rows="7" id="nombreProyecto" onBlur="" name="nombreProyecto"><?php echo $editData['nombre_proyecto'] ?></textarea>
                                                       
                                                    </div>
                                                </div>

                                                <?php
                                                $disable2 = "disabled" ;
                                                if($editData["onlyView"] == 2){
                                                    $disable2 = "";
                                                } 

                                                ?>


                                             
                                                <div class="form-group">
                                                    <label  class="col-sm-5">Estatus del Proyecto:</label>
                                                    <div class="col-sm-6">
                                                        <?php
                                                          $disabled="";

                                                           $disabled="disabled";
                                                          //if($editData['statusProyecto']=="Cancelado")
                                                           
                                                        ?>
                                                         <select class="form-control" id="statusProyecto"   <?=$disabled?> name="statusProyecto">
                                                            <option>Seleccione...</option>
                                                            <?php
                                                                $selected = "";
                                                                if($editData['statusProyecto']=="Iniciado" || $editData['statusProyecto']=="")
                                                                $selected = "selected";
                                                            ?>
                                                            <option <?php echo $selected?> value="Iniciado">Iniciado</option>

                                                            <?php
                                                                $selected = "";
                                                                if($editData['statusProyecto']=="Terminado")
                                                                $selected = "selected";
                                                            ?>
                                                            <option <?php echo $selected?> value="Terminado">Terminado</option>      


                                                            <?php
                                                             if($editData['statusProyecto']=="Cancelado") {
                                                                $selected = "";
                                                                $mostrar="style='display:none'";
                                                                if($editData['statusProyecto']=="Cancelado")
                                                                $selected = "selected";
                                                                $mostrar="";
                                                            ?>
                                                            <option <?php echo $selected?> value="Cancelado" <?=$mostrar?> >Cancelado</option>      
                                                            <?php
                                                            }
                                                            ?>
                                                                                                              
                                                        </select>
                                                    </div>
                                                </div>


                                               <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">Comentarios:</label>
                                                    <div class="col-sm-6">

                                                          <textarea   class="form-control" rows="4" id="comentariosProyecto"  name="comentariosProyecto"><?php echo trim($editData['comentarios_proyecto']) ?></textarea>
                                                       
                                                    </div>
                                                </div>

                                                

 
                                                
                                               

                                                <br> 
                                                <br>
                                                <br>
                                                <br>
                                   

                                              


                                                <input type="hidden" id="instanciaEjecutora_tmp" name="instanciaEjecutora_tmp">
                                                <input type="hidden" id="instanciaPromotora_tmp" name="instanciaPromotora_tmp">
                                                <input type="hidden" id="statusProyectoSelected" name="statusProyectoSelected">
                                                <input type="hidden" id="metodo" name="metodo" value="<?php echo $editData['metodo']?>">
                                                <input type="hidden" id="id_proyecto" name="id_proyecto" value="<?php echo $editData['id_proyecto'] ?>" >
 
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- /.row -->
                <!-- end MAIN PAGE ROW -->



            



                 <!-- Instancia de Cancelacion -->

                 <div class="col-lg-6" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Cancelacion de Obra</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#instanciaObra"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="instanciaObra" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_cancelacion">
 

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5  ">Deseas Cancelar La Obra?:</label>
                                                    <div class="col-sm-6">
                                                       <select class="form-control" id="enPicaso" name="enPicaso" onChange="selCancel(this.value)">
                                                            <option value="0">Seleccione...</option>
                                                            <?php 
                                                            $selected = "";
                                                            if($editData['en_picaso']=="Si"){
                                                                $selected = "selected";
                                                            }

                                                            ?>
                                                            <option value="Si" <?php echo $selected?>>Si</option>
                                                            <?php 
                                                            $selected = "";
                                                            if($editData['en_picaso']=="No"){
                                                                $selected = "selected";
                                                            }

                                                            ?>
                                                            <option value="No" <?php echo $selected?>>No</option>
                                                                                                                       
                                                        </select>
                                                    </div>
                                                </div>
                                                <div id="cuadro_cancelacion" style="display:none">
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-5  ">Explique el Motivo de Cancelación:</label>
                                                        <div class="col-sm-6">
                                                           <textarea id="motivo_cancelacion"  class="form-control" rows="3" ></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-11  text-right ">
                                                            <button type="button"   id="btnCancelar" class="btn   btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick="cancelObra('Si')"><i class="fa fa-floppy-o"></i> Cancelar</button>  
                                                        </div>
                                                    </div>
                                                </div>
                                               

                            
                                                <input type="hidden" name="instanciaEjecutora_selected" id="instanciaEjecutora_selected">
                                                 <input type="hidden" id="id_proyecto_picaso" name="id_proyecto_picaso" value="<?php echo $editData['id_proyecto'] ?>" >

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Instancia de Cancelacion -->




               
                           <!-- Division para recursos -->
                 <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#federal estatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                          
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>

                <!-- fin Beneficiarios de la Obra -->
                <!--Fecha de la Obra-->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green" >
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                          <h4>3. Fecha de Obra / Apoyo / Servicio</h4> 
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#fechaObra"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="fechaObra" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_fechas">



                                                <div class="form-group">
                                                    <label  class="col-sm-5 ">*Fecha de Inicio Programada: <a href="#fecha_obra#" id="tag_fecha_inicio_programada" title="Fecha de Inicio pactada en el Contrato de la Obra, Apoyo, o Servicio.".">?</a></label>
                                                    <div class="col-sm-4">
                                                          <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar inicioprogramada"></i>
                                                    </span> 
                                                        <input type="text"  class="form-control"  style="text-align:right" id="fechaInicioProgramada" name="fechaInicioProgramada" value="<?php echo $editData['fecha_inicio_programada']?>" >
                                                    </div></div>
                                                </div>

                                              

                                       


                                                <div class="form-group">
                                                    <label  class="col-sm-5 ">*Fecha Final Programada:<a href="#fecha_obra#" id="tag_fecha_inicio_programada" title="Fecha Final pactada en el Contrato de la Obra, Apoyo, o Servicio.".">?</a></label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar finalprogramada calendarTino"></i>
                                                    </span> 
                                                        <input type="text"  class="form-control" style="text-align:right" id="fechaFinalProgramada" name="fechaFinalProgramada" onChange="automatizarFecha()" value="<?php echo $editData['fecha_final_programada']?>" >
                                                    </div></div>
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-5 ">Fecha de Inicio de Ejecución: <a href="#fecha_obra#" id="tag_fecha_inicio_programada" title="Fecha Real de Inicio de la Obra, Apoyo, o Servicio.">?</a></label>
                                                    <div class="col-sm-4">
                                                          <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar inicioejecucion"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" style="text-align:right" id="fechaInicioEjecucion" name="fechaInicioEjecucion" value="<?php echo $editData['fecha_inicio_ejecucion']?>"   >
                                                    </div>
                                                    </div>
                                                </div>

                                           


                                                <div class="form-group">
                                                    <label  class="col-sm-5 ">Fecha Final de Ejecución: <a href="#fecha_obra#" id="tag_fecha_inicio_programada" title="Fecha Real de Termino de la Obra, Apoyo, o Servicio.">?</a></label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-calendar finalejecucion"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" style="text-align:right" id="fechaFinalEjecucion" name="fechaFinalEjecucion" value="<?php echo $editData['fecha_final_ejecucion']?>"    >
                                                    </div></div>
                                                </div>


 

                                           
                                                

                                              <?php
                                                    $disable = "";
                                                  if($editData['fk_beneficio']==1){
                                                    $disable = "style='display:none'";
                                                  }

                                                ?>

                                                <div class="form-group unicaprogramada_div" <?=$disable?>>
                                                    <label  class="col-sm-5 ">Fecha Única Programada:</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar unicaprogramada calendarTino"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" style="text-align:right" id="fechaUnicaProgramada" name="fechaUnicaProgramada" value="<?php echo $editData['fecha_unica_programada']?>" <?=$disable?>>
                                                    </div></div>
                                                </div>

                                                <?php
                                                    $disable = "";
                                                  if($editData['fk_beneficio']==1){
                                                     $disable = "style='display:none'";
                                                  }

                                                ?>

                                                <div class="form-group unicaejecucion_div" <?=$disable?>>
                                                    <label  class="col-sm-5 ">Fecha Única Ejecución:</label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-calendar unicaejecucion calendarTino"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" style="text-align:right" id="fechaUnicaEjecucion" name="fechaUnicaEjecucion"  value="<?php echo $editData['fecha_unica_ejecucion']?>" <?=$disable?> >
                                                    </div>
                                                    </div>
                                                </div>


                                                <input type="hidden" id="id_proyecto_fechaobra" name="id_proyecto_fechaobra" value="<?php echo $editData['id_proyecto'] ?>" >
                                    
<br><br><br> 
                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!--fin Fecha de la Obra>




                

                <!-- Domicilio de la Obra -->

                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>4. Domicilio de Obra / Apoyo / Servicio</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#domicilioObra"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="domicilioObra" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_domicilio">




                                                <div class="form-group">
                                                    <label  class="col-sm-2 ">*Municipio:</label>
                                                    <div class="col-sm-4">
                                                         <select class="form-control" id="municipio" name="municipio">
                                                            <option value="">Seleccione...</option>

                                                            

                                                            <?php
                                                                foreach ($listMunicipios as $row) {
                                                                    $selected = "";

                                                                    if($editData['fk_municipio'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . strtoupper($row['municipio']) . "</option>\n";
                                                                }
                                                            ?>
                                                       
                                                        </select>
                                                    </div>
                                             
                                                    <label  class="col-sm-2 ">*Localidad:</label>
                                                    <div class="col-sm-4">
                                                         <select class="form-control" id="localidad" name="localidad2">
                                                            <option value="">Seleccione...</option>
                                                            <?php
                                                         
                                                                if(isset($editData['listLocalidades']) && count($editData['listLocalidades'])>0) {
                                                                foreach ($editData['listLocalidades'] as $row) {
                                                                    $selected = "";

                                                                    if($editData['fk_localidad'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . strtoupper($row['localidad']) . "</option>\n";
                                                                }

                                                                }
                                                            
                                                            ?>

                                                        </select>
                                                    </div>
                                                </div>


                                                 <div class="form-group">
                                                    <label  class="col-sm-3 ">*¿Domicilio Conocido?:</label>
                                                    <div class="col-sm-6">

                                                        <?php 
                                                            $selected = "";
                                                            if($editData['domicilio_conocido']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejado" name="domicilioCortejado" <?=$selected?> >Si</label>

                                                        <label class="radio-inline">
                                                        <?php 
                                                            $selected = "";
                                                            if($editData['domicilio_conocido']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                        ?>

                                                        <input type="radio" value="NO" id="domicilioCortejado" name="domicilioCortejado" <?=$selected?>>No</label> 
                                                            
                                                    </div>
                                                </div>


                                                        <?php 
                                                            $disable = "";
                                                            if($editData['domicilio_conocido']=="SI"){
                                                                $disable = "disabled";
                                                            }
                                                        ?>


                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-2 ">Calle:</label>
                                                    <div class="col-sm-10" ">
                                                          
                                                            <input type="text" class="form-control input-sm" value="<?php echo trim($editData['calle']) ?>" id="calle" name="calle" <?=$disable?> >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Número:</label>
                                                    <div class="col-sm-2" ">
                                                        <input type="text" class="form-control input-sm" <?=$disable?> oninput="this.value=this.value.replace(/[^a-zA-Z0-9-#\s]+$/g,'');" id="numero" name="numero"  maxlength="5" value="<?php echo trim($editData['numero']) ?>" >
                                                    </div>
                                                
                                                    <label for="textInput" class="col-sm-2">Colonia:</label>
                                                    <div class="col-sm-2" ">
                                                        <input type="text" class="form-control input-sm" <?=$disable?>  oninput="this.value=this.value.replace(/[^a-zA-Z0-9\s]+$/g,'');" id="colonia" name="colonia" maxlength="30"  value="<?php echo trim($editData['colonia']) ?>"  >
                                                    </div>
                                                
                                                    <label for="textInput" class="col-sm-2 ">C.Postal:</label>
                                                    <div class="col-sm-2" ">
                                                        <input type="text" class="form-control input-sm" <?=$disable?> oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="6" id="codigoPostal" name="codigoPostal" value="<?php echo trim($editData['cpostal']) ?>" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">Comentarios:</label>
                                                    <div class="col-sm-10">

                                                          <textarea   class="form-control" rows="3" id="comentariosCalle"  name="comentariosCalle"><?php echo trim($editData['comentarios_calle']) ?></textarea>
                                                       
                                                    </div>
                                                </div>

                                              

                                             <input type="hidden" id="id_proyecto_domicilio" name="id_proyecto_domicilio" value="<?php echo $editData['id_proyecto'] ?>" >
                                               

                                                <br><br> <br>


                                                  
                                              
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>


                <!--Contrato-->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-blue" >
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                          <h4>5. Contrato</h4> 
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#fechaObra"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="fechaObra" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_contrato">

                                       
                                                 <div class="form-group">
                                                    <label  class="col-sm-5 ">Fecha de Contrato: <a href="#fecha_obra#" id="tag_fecha_inicio_programada" title="Fecha de Contrato.".">?</a></label>
                                                    <div class="col-sm-4">
                                                          <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar fechacontratoclass"></i>
                                                    </span> 
                                                        <input type="text"  class="form-control"  style="text-align:right" id="fecha_contrato" name="fecha_contrato" value="<?php echo $editData['fecha_contrato']?>" >
                                                    </div></div>
                                                </div>


 

                                                 <div class="form-group">
                                                    <label  class="col-sm-5  ">Monto Contratado: <a href="#asignar_montos#" id="tag_fecha_inicio_programada"  title="Monto Contratado..">?</a></label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control "   type="number" maxlength="15" id="monto_contratado" name="monto_contratado" onblur="addCommas(this);"  style="text-align:right" value="<?php echo $editData['monto_contratado']?>" >
                                                    </div></div>
                                                </div>

 
                                        


                                               
                                    

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!--fin Contrato>



                  <!-- Beneficiarios de la Obra -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12"  >

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                          <a name="fecha_obra">    <h4>6. Beneficiarios</h4></a>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#beneficiarios"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="beneficiarios" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_beneficios">

<!--
                                                <div class="form-group"  style="display:block">
                                                    <label  class="col-sm-2">Metas contratadas:</label>

                                                  
                                                </div>

                                                <div class="form-group" style="display:block">
                                                   
                                                    <div class="col-sm-8">
                                                    <table border=0 width="100%">
                                                        <tr>
                                                            <th> Descripción </th><th>Cantidad</th><th>Unidad</th>
                                                        </tr>
                                                        <tr>
                                                            <td>

                                                                <?php
                                                                $descripcion1="";
                                                                if(isset($proyectoMetas[0]['descripcion'])){
                                                                    $descripcion1 = $proyectoMetas[0]['descripcion'];
                                                                }
                                                                   
                                                              
                                                                ?>
                                                        
      
                                                                <input type="text" id="descripcion_meta1" name="descripcion_meta[]" class="col-sm-10" value="<?php echo $descripcion1?>">
                                                              
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $cantidad1="";
                                                                if(isset($proyectoMetas[0]['cantidad'])){
                                                                    $cantidad1 = $proyectoMetas[0]['cantidad'];
                                                                }
                                                                   
                                                              
                                                                ?>
                                                                <input type="text" id="qty_meta1" name="qty_meta[]"  value="<?php echo $cantidad1?>">
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $unidad1="";
                                                                if(isset($proyectoMetas[0]['unidad'])){
                                                                    $unidad1 = $proyectoMetas[0]['unidad'];
                                                                }
                                                                   
                                                              
                                                                ?>

                                                                <input type="text" id="unidad_meta1" name="unidad_meta[]"  value="<?php echo $unidad1?>">
                                                            </td>
                                                        </tr>
                                                       <tr>

                                                        <?php
                                                                $descripcion2="";
                                                                if(isset($proyectoMetas[1]['descripcion'])){
                                                                    $descripcion2 = $proyectoMetas[1]['descripcion'];
                                                                }
                                                                   
                                                              
                                                                ?>
                                                            <td>
                                                                <input type="text" id="descripcion_meta2" name="descripcion_meta[]" class="col-sm-10"  value="<?=$descripcion2?>">
                                                            </td>

                                                              <?php
                                                                $cantidad2="";
                                                                if(isset($proyectoMetas[1]['cantidad'])){
                                                                    $cantidad2 = $proyectoMetas[1]['cantidad'];
                                                                }
                                                                   
                                                              
                                                                ?>
                                                            <td>
                                                                <input type="text" id="qty_meta2" name="qty_meta[]" value="<?=$cantidad2?>">
                                                            </td>

                                                              <?php
                                                                $unidad2="";
                                                                if(isset($proyectoMetas[1]['unidad'])){
                                                                    $unidad2 = $proyectoMetas[1]['unidad'];
                                                                }
                                                                   
                                                              
                                                                ?>
                                                            <td>
                                                                <input type="text" id="unidad_meta2" name="unidad_meta[]"  value="<?=$unidad2?>">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>

                                                            <?php
                                                                $descripcion3="";
                                                                if(isset($proyectoMetas[2]['descripcion'])){
                                                                    $descripcion3 = $proyectoMetas[2]['descripcion'];
                                                                }
                                                                   
                                                              
                                                            ?>



                                                                <input type="text" id="descripcion_meta3" name="descripcion_meta[]" class="col-sm-10" value="<?php echo $descripcion3?>">
                                                            </td>
                                                            <td>


                                                            <?php
                                                                $cantidad3="";
                                                                if(isset($proyectoMetas[2]['cantidad'])){
                                                                    $cantidad3 = $proyectoMetas[2]['cantidad'];
                                                                } 
                                                            ?>
 
                                                                <input type="text" id="qty_meta3" name="qty_meta[]" value="<?php echo $cantidad3?>">
                                                            </td>
                                                            <td>

                                                            <?php
                                                                $unidad3="";
                                                                if(isset($proyectoMetas[2]['unidad'])){
                                                                    $unidad3 = $proyectoMetas[2]['unidad'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="unidad_meta3" name="unidad_meta[]" value="<?php echo $unidad3 ?>">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                             <?php
                                                                $descripcion4="";
                                                                if(isset($proyectoMetas[3]['descripcion'])){
                                                                    $descripcion4 = $proyectoMetas[3]['descripcion'];
                                                                } 
                                                            ?>

                                                                <input type="text" id="descripcion_meta4" name="descripcion_meta[]" class="col-sm-10" value="<?php echo $descripcion4 ?>">
                                                            </td>
                                                            <td>
                                                            <?php
                                                                $cantidad4="";
                                                                if(isset($proyectoMetas[3]['cantidad'])){
                                                                    $cantidad4 = $proyectoMetas[3]['cantidad'];
                                                                } 
                                                            ?>

                                                                <input type="text" id="qty_meta4" name="qty_meta[]" value="<?php echo $cantidad4 ?>">
                                                            </td>
                                                            <td>
                                                           <?php
                                                                $unidad4="";
                                                                if(isset($proyectoMetas[3]['unidad'])){
                                                                    $unidad4 = $proyectoMetas[3]['unidad'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="unidad_meta4" name="unidad_meta[]" value="<?php echo $unidad4?>">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                              <?php
                                                                $descripcion5="";
                                                                if(isset($proyectoMetas[4]['descripcion'])){
                                                                    $descripcion5 = $proyectoMetas[4]['descripcion'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="descripcion_meta5" name="descripcion_meta[]" class="col-sm-10" value="<?php echo $descripcion5?>">
                                                            </td>
                                                            <td>
                                                            <?php
                                                                $cantidad5="";
                                                                if(isset($proyectoMetas[4]['cantidad'])){
                                                                    $cantidad5 = $proyectoMetas[4]['cantidad'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="qty_meta5" name="qty_meta[]"  value="<?php echo $cantidad5?>">
                                                            </td>
                                                            <td>
                                                           <?php
                                                                $unidad5="";
                                                                if(isset($proyectoMetas[4]['unidad'])){
                                                                    $unidad5 = $proyectoMetas[4]['unidad'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="unidad_meta5" name="unidad_meta[]" value="<?php echo $unidad5?>">
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            <?php
                                                                $descripcion6="";
                                                                if(isset($proyectoMetas[5]['descripcion'])){
                                                                    $descripcion6 = $proyectoMetas[5]['descripcion'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="descripcion_meta6" name="descripcion_meta[]" class="col-sm-10" value="<?php echo $descripcion6?>">
                                                            </td>
                                                            <td>
                                                             <?php
                                                                $cantidad6="";
                                                                if(isset($proyectoMetas[5]['cantidad'])){
                                                                    $cantidad6 = $proyectoMetas[5]['cantidad'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="qty_meta5" name="qty_meta[]" value="<?php echo $cantidad6?>">
                                                            </td>
                                                            <td>
                                                            <?php
                                                                $unidad6="";
                                                                if(isset($proyectoMetas[5]['unidad'])){
                                                                    $unidad6 = $proyectoMetas[5]['unidad'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="unidad_meta5" name="unidad_meta[]" value="<?php echo $unidad6?>">
                                                            </td>
                                                        </tr>
                                                             <tr>
                                                            <td>
                                                                <?php
                                                                $descripcion7="";
                                                                if(isset($proyectoMetas[6]['descripcion'])){
                                                                    $descripcion7 = $proyectoMetas[6]['descripcion'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="descripcion_meta5" name="descripcion_meta[]" class="col-sm-10" value="<?php echo $descripcion7?>">
                                                            </td>
                                                            <td>
                                                            <?php
                                                                $cantidad7="";
                                                                if(isset($proyectoMetas[6]['cantidad'])){
                                                                    $cantidad7 = $proyectoMetas[6]['cantidad'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="qty_meta5" name="qty_meta[]" value="<?php echo $cantidad7?>">
                                                            </td>
                                                            <td>
                                                             <?php
                                                                $unidad7="";
                                                                if(isset($proyectoMetas[6]['unidad'])){
                                                                    $unidad7 = $proyectoMetas[6]['unidad'];
                                                                } 
                                                            ?>
                                                                <input type="text" id="unidad_meta5" name="unidad_meta[]" value="<?php echo $unidad7?>">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    </div>
                                                </div>
                                            -->


                                                <div class="form-group">
                                                    <label  class="col-sm-4">Mujeres Beneficiadas:</label>
                                                    <div class="col-sm-6">
                                                         <div class="input-group">
                                                  <span class="input-group-addon"><i class="fa fa-female"></i> </span>
                                                        <input type="text" class="form-control t_people" id="mujeresBeneficiadas" name="mujeresBeneficiadas" 
                                                        value="<?php echo $editData['mujeres_beneficiadas'] ?>" style="text-align:right" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  onblur="addCommas(this);sumaPeople()" class="t_people"    >
                                                    </div>
                                                    </div>
                                                </div>
                                             
                                                <div class="form-group">
                                                    <label  class="col-sm-4">Hombres Beneficiados:</label>
                                                    <div class="col-sm-6">
                                                         <div class="input-group">
                                                     <span class="input-group-addon"><i class="fa fa-male"></i> </span>
                                                        <input type="text" class="form-control t_people" id="hombresBeneficiados" name="hombresBeneficiados"  value="<?php echo $editData['hombres_beneficiados'] ?>"  style="text-align:right" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  onblur="addCommas(this);sumaPeople()" class="t_people"   >
                                                    </div></div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label  class="col-sm-4">*Total Beneficiarios:</label>

                                                       <div class="col-sm-6">
                                                              <div class="input-group">
                                                     <span class="input-group-addon"><i class="fa fa-users"></i> </span>
                                                        <input type="text"  oninput="this.value=this.value.replace(/[^0-9]/g,'');"  onblur="addCommas(this);"  style="text-align:right" maxlength="10" class="form-control"  value="<?php echo $editData['total_beneficiados'] ?>" id="totalBeneficiados" name="totalBeneficiados"  >
                                                    </div>
                                                    </div>
                                               
                                                 </div>
                                                
 



                                            
                                                <input type="hidden" name="totalBeneficios" id="totalBeneficios">
                                                <input type="hidden" id="id_proyecto_beneficio" name="id_proyecto_beneficio" value="<?php echo $editData['id_proyecto'] ?>" >
                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
















                <!-- fin Domicilio de la Obra -->

               <!-- Separador - Asignar Montos de la Obra ..-->
                 <div class="col-lg-12">

                        <div class="row">

                       
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                             <a name="asignar_montos"><h4 class="text-center">6. Asignar Montos de la Obra / Apoyo / Servicio</h4> </a>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#federal estatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                          
                                </div>
                                <!-- /.portlet -->
                            </div>
                            
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- Separador - Asignar montos de la obra.. -->



                  <!-- Monto Federal de la Obra -->
                 <div class="col-lg-6" id="divFederal" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-blue">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Federal</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#federal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="federal" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_asignaFederal">




                                                <div class="form-group">
                                                    <label  class="col-sm-6">Fecha de Asignación del Recurso: <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Fecha Oficial en que fue Asignado el Recurso para la Obra, Apoyo o Servico.">?</a></label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar asignacionrecursofederal calendarTino"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" id="fechaAsignacionRecursoFederal" name="fechaAsignacionRecursoFederal" value="<?php echo $editData['fecha_asignacion_recurso_federal']?>" style="text-align:right">
                                                    </div></div>
                                                </div>

                                                <div class="form-group">
                                                    <label  class="col-sm-6  ">Monto del Recurso Asignado (A Vigilar): <a href="#asignar_montos#" id="tag_fecha_inicio_programada"  title="Monto del Recurso Oficial Asignado para la Obra, Apoyo o Servico.">?</a></label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control t_vigilar"    onblur="sumaRecursoaVigilar();validarREAsignadoFederal(this)" type="number" maxlength="15" id="montoRecursoAsignadoFederal" name="montoRecursoAsignadoFederal" style="text-align:right" value="<?php echo $editData['monto_recurso_asignado_vig_federal']?>" >
                                                    </div></div>
                                                </div>

                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Fecha de Ejecución del Recurso: <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Fecha Real de Termino de la Obra, Apoyo o Servicio.">?</a></label>
                                                    <div class="col-sm-4">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar ejecucionrecursofederal calendarTino"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" id="fechaEjecucionRecursoFederal" name="fechaEjecucionRecursoFederal" style="text-align:right" value="<?php echo $editData['fecha_ejecucion_recurso_federal']?>">
                                                    </div></div>
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Monto del Recurso Ejecutado (Vigilado): <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Monto Final de la Obra, Apoyo o Servicio.">?</a></label>
                                                    <div class="col-sm-4">
                                                         <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control t_vigilado"  onblur="sumaRecursoaVigilado();validarREVigiladoFederal(this);" type="number" maxlength="15" id="montoRecursoEjecutadoFederal" name="montoRecursoEjecutadoFederal" style="text-align:right"  value="<?php echo $editData['monto_recurso_asignado_ejec_federal']?>" >
                                                    </div></div>
                                                </div>

                                                <input type="hidden" class="total_recursoAsignado" name="montoRecursoAsignadoFederal_hidden" id="montoRecursoAsignadoFederal_hidden" >


                                                   <input type="hidden" id="id_proyecto_federal" name="id_proyecto_federal" value="<?php echo $editData['id_proyecto'] ?>" >

                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin - Monto Federal de la Obra-->




                  <!-- Monto Estatal de la Obra -->
                 <div class="col-lg-6" id="divEstatal" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Estatal</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#estatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="estatal" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_asignaEstatal">




                                             <div class="form-group">
                                                    <label  class="col-sm-6 ">Fecha de Asignación del Recurso: <a href="#asignar_montos##" id="tag_fecha_inicio_programada" title="Fecha Oficial en que fue Asignado el Recurso para la Obra, Apoyo o Servico.">?</a></label>
                                                    <div class="col-sm-4">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar asignacionrecursoestatal calendarTino"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" id="fechaAsignacionRecursoEstatal" name="fechaAsignacionRecursoEstatal" style="text-align:right"  value="<?php echo $editData['fecha_asignacion_recurso_estatal']?>"  >
                                                    </div></div>
                                                </div>

                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Monto del Recurso Asignado (A Vigilar): <a href="#asignar_montos#" id="tag_fecha_inicio_programada" type="number" title="Monto del Recurso Oficial Asignado para la Obra, Apoyo o Servico.">?</a></label>
                                                    <div class="col-sm-4">
                                                         <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control t_vigilar"  onblur="sumaRecursoaVigilar();validarREAsignadoEstatal(this)" id="montoRecursoAsignadoEstatal" name="montoRecursoAsignadoEstatal" maxlength="15" style="text-align:right" value="<?php echo $editData['monto_recurso_asignado_vig_estatal']?>" >
                                                    </div></div>
                                                </div>

                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Fecha de Ejecución del Recurso: <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Fecha Real de Termino de la Obra, Apoyo o Servicio.">?</a></label>
                                                    <div class="col-sm-4">
                                                          <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar ejecurcionrecursoestatal "></i>
                                                    </span> 
                                                        <input type="text" class="form-control" id="fechaEjecucionRecursoEstatal" name="fechaEjecucionRecursoEstatal" style="text-align:right" value="<?php echo $editData['fecha_ejecucion_recurso_estatal']?>">
                                                    </div></div>
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Monto del Recurso Ejecutado (Vigilado): <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Monto Final de la Obra, Apoyo o Servicio.">?</a></label>
                                                    <div class="col-sm-4">
                                                         <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control t_vigilado"  onblur="sumaRecursoaVigilado();validarREVigiladoEstatal(this)" type="number" maxlength="15" id="montoRecursoEjecutadoEstatal" name="montoRecursoEjecutadoEstatal" style="text-align:right" value="<?php echo $editData['monto_recurso_asignado_ejec_estatal']?>">
                                                    </div></div>
                                                </div>

                                               
                                              <input type="hidden" id="id_proyecto_estatal" name="id_proyecto_estatal" value="<?php echo $editData['id_proyecto'] ?>" >

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin - Monto Estatal de la Obra-->





                  <!-- Monto Municipal de la Obra -->
                 <div class="col-lg-6" id="divMunicipal" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Municipal</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#municipal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="municipal" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_asignaMunicipal">




                                                <div class="form-group">
                                                     <label  class="col-sm-6 ">Fecha de Asignación del Recurso: <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Fecha Oficial en que fue Asignado el Recurso para la Obra, Apoyo o Servico.">?</a></label>
                                                    <div class="col-sm-4">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar asignacionrecursomunicipal"></i>
                                                    </span>                                            

                                                          <input type="text" class="form-control" id="fechaAsignacionRecursoMunicipal" name="fechaAsignacionRecursoMunicipal" style="text-align:right"  value="<?php echo $editData['fecha_asignacion_recurso_municipal']?>" >
                                                    </div></div>
                                                </div>

                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Monto del Recurso Asignado (A Vigilar): <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Monto del Recurso Oficial Asignado para la Obra, Apoyo o Servico.">?</a></label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control t_vigilar"  onblur="sumaRecursoaVigilar();validarREAsignadoMunicipal(this)" id="montoRecursoAsignadoMunicipal" name="montoRecursoAsignadoMunicipal" type="number" maxlength="15"  style="text-align:right" value="<?php echo $editData['monto_recurso_asignado_vig_municipal']?>">
                                                    </div></div>
                                                </div>

                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Fecha de Ejecución del Recurso: <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Fecha Real de Termino de la Obra, Apoyo o Servicio.">?</a></label>
                                                    <div class="col-sm-4">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar ejecucionrecursomunicipal"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" id="fechaEjecucionRecursoMunicipal" name="fechaEjecucionRecursoMunicipal" style="text-align:right"  value="<?php echo $editData['fecha_ejecucion_recurso_municipal']?>" >
                                                    </div></div>
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Monto del Recurso Ejecutado (Vigilado): <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Monto Final de la Obra, Apoyo o Servicio.">?</a></label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control t_vigilado"  onblur="sumaRecursoaVigilado();validarREVigiladoMunicipal(this)" id="montoRecursoEjecutadoMunicipal" name="montoRecursoEjecutadoMunicipal" type="number" maxlength="15"  style="text-align:right"  value="<?php echo $editData['monto_recurso_asignado_ejec_municipal']?>">
                                                    </div></div>
                                                </div>

        
                                              <input type="hidden" id="id_proyecto_municipal" name="id_proyecto_municipal" value="<?php echo $editData['id_proyecto'] ?>" >
                                                                             
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin - Monto Municipal de la Obra-->




                  <!-- Monto Otros de la Obra -->
                 <div class="col-lg-6" id="divOtros" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Otros</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#otros"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="otros" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_asignaOtros">




                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Fecha de Asignación del Recurso: <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Fecha Oficial en que fue Asignado el Recurso para la Obra, Apoyo o Servico.">?</a></label>
                                                    <div class="col-sm-4">
                                                           <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar asignacionrecursootros"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" id="fechaAsignacionRecursoOtros" name="fechaAsignacionRecursoOtros" style="text-align:right" value="<?php echo $editData['fecha_asignacion_recurso_otros']?>">
                                                    </div></div>
                                                </div>

                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Monto del Recurso Asignado (A Vigilar): <a href="#asignar_montos#" id="tag_fecha_inicio_programada"   title="Monto del Recurso Oficial Asignado para la Obra, Apoyo o Servico.">?</a></label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control t_vigilar"  onblur="sumaRecursoaVigilar();validarREAsignadoOtros(this)" id="montoRecursoAsignadoOtros" name="montoRecursoAsignadoOtros" type="number" maxlength="15" style="text-align:right" value="<?php echo $editData['monto_recurso_asignado_vig_otros']?>">
                                                    </div></div>
                                                </div>

                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Fecha de Ejecución del Recurso: <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Fecha Real iniciado en la Obra.">?</a></label>
                                                    <div class="col-sm-4">
                                                           <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar ejecucionrecursootros"></i>
                                                    </span> 
                                                        <input type="text" class="form-control" id="fechaEjecucionRecursoOtros" name="fechaEjecucionRecursoOtros" style="text-align:right" value="<?php echo $editData['fecha_ejecucion_recurso_otros']?>">
                                                    </div></div>
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-6 ">Monto del Recurso Ejecutado (Vigilado): <a href="#asignar_montos#" id="tag_fecha_inicio_programada" title="Monto Final de la Obra, Apoyo o Servicio.">?</a></label>
                                                    <div class="col-sm-4">
                                                        <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control t_vigilado"  onblur="sumaRecursoaVigilado();validarREVigiladoOtros(this)" id="montoRecursoEjecutadoOtros" name="montoRecursoEjecutadoOtros"  type="number" maxlength="15"  style="text-align:right" value="<?php echo $editData['monto_recurso_asignado_ejec_otros']?>">
                                                    </div></div>
                                                </div>

                                                   
        
                                              <input type="hidden" id="id_proyecto_otros" name="id_proyecto_otros" value="<?php echo $editData['id_proyecto'] ?>" >
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin - Monto Otros de la Obra-->



                    <!-- Monto Totales de la Obra -->
  
                <!-- fin - Monto Totales de la Obra-->

<div class="col-lg-12" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4 class="text-center">CONVENIO MODIFICATORIO</h4> 
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#federal estatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                          
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>



                 <div class="col-lg-6" style="display:none" >

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                           <A name="modificadora"> <h4>Convenio Modificatorio</h4> </A>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#modificadora"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="modificadora" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_material_distribuir">
                                                


                                                

                                              
                                                <div class="text-right">  <a href="#modificadora" onClick="addModificadora()">[+] Agregar Nueva Modificadora</a></div>
                                            

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   ">Modificadora 1:</label>
                                            </div>
                                               
                                                   
                                              

                                                
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Documento:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento">
                                                            <option value="">Seleccione..</option>
                                                            <option value="1">Convenio</option>
                                                            <option value="2">Contrato</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                                          <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Número Documento:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="numDocumento" name="numDocumento"  >
                                                    </div>



                                             
                                                    <label for="textInput" class="col-sm-3  ">Fecha Firma:</label>
                                                    <div class="col-sm-3 ">

                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar firma1"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaFirma1" name="fechaFirma1"  >
                                                    </div>
                                                    </div>
                                              
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-3 ">Adjuntar convenio:</label>
                                                    <div class="col-sm-6">
                                                        <input type="file">
                                                    </div>
                                                </div> 

                                                      <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Modificación:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento" onChange="selectModulo1(this.value)">
                                                            <option value="">Seleccione..</option>
                                                            <option value="fecha">Fecha</option>
                                                            <option value="monto">Monto</option>
                                                            <option value="alcance">Alcance</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                     
                                              <div class="form-group" style="display:none" id="modulo_fecha1">
                                                    <label for="textInput" class="col-sm-3  ">Fecha Inicio:</label>
                                                    <div class="col-sm-3" ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar inicio1"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaInicio1" name="fechaInicio1"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Fecha Modificada:</label>
                                                    <div class="col-sm-3 ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar modificada1"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaModificada1" name="fechaModificada"  >
                                                    </div>
                                                    </div>
                                                </div>


                                          
                                      
                                     
                                           
                                                <div class="form-group" style="display:none" id="modulo_monto1">
                                                    <label for="textInput" class="col-sm-3  ">Monto Original:</label>
                                                    <div class="col-sm-3" ">
                                                           <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoOriginal" name="montoOriginal"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Monto Modificado:</label>
                                                    <div class="col-sm-3" ">
                                                            <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoModificado" name="montoModificado"  ></div>
                                                    </div>
                                                </div>

                                                <div style="display:none" id="modulo_alcance1">
                                               <div class="form-group" >
                                                    <label for="textInput" class="col-sm-3  ">Alcance Original:</label>
                                                    <div class="col-sm-9" ">
                                                          <textarea   class="form-control" rows="3" id="alcanceOriginal" name="alcanceOriginal">
                                                              
                                                          </textarea>
                                                    </div>

                                                    
                                                </div>

                                                   <div class="form-group">
                                                   

                                                    <label for="textInput" class="col-sm-3  ">Alcance Modificado:</label>
                                                    <div class="col-sm-9" ">
                                                         <textarea   class="form-control" rows="3" id="alcanceModificado" name="alcanceModificado">
                                                               
                                                          </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                                 
                                                 


                                            <div id="view_modificadora2" style="display:none">
                                            <hr>



  
                                                 <div class="text-right">  <a href="#otros" onClick="quitModficadora(2)">[-] Eliminar recurso Modificatorio</a></div>

                                            

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   ">Modificadora 2:</label>
                                            </div>
                                               
                                                   
                                              

                                                
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Documento:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento">
                                                            <option value="">Seleccione..</option>
                                                            <option value="1">Convenio</option>
                                                            <option value="2">Contrato</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                                          <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Número Documento:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="numDocumento" name="numDocumento"  >
                                                    </div>



                                             
                                                    <label for="textInput" class="col-sm-3  ">Fecha Firma:</label>
                                                    <div class="col-sm-3 ">

                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar firma2"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaFirma2" name="fechaFirma"  >
                                                    </div>
                                                    </div>
                                              
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-3 ">Adjuntar convenio:</label>
                                                    <div class="col-sm-6">
                                                        <input type="file">
                                                    </div>
                                                </div> 

                                                      <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Modificación:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento" onChange="selectModulo2(this.value)">
                                                            <option value="">Seleccione..</option>
                                                            <option value="fecha">Fecha</option>
                                                            <option value="monto">Monto</option>
                                                            <option value="alcance">Alcance</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                     
                                              <div class="form-group" style="display:none" id="modulo_fecha2">
                                                    <label for="textInput" class="col-sm-3  ">Fecha Inicio:</label>
                                                    <div class="col-sm-3" ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar inicio2"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaInicio2" name="fechaInicio2"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Fecha Modificada:</label>
                                                    <div class="col-sm-3 ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar modificada2"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaModificada2" name="fechaModificada2"  >
                                                    </div>
                                                    </div>
                                                </div>


                                          
                                      
                                     
                                           
                                                <div class="form-group" style="display:none" id="modulo_monto2">
                                                    <label for="textInput" class="col-sm-3  ">Monto Original:</label>
                                                    <div class="col-sm-3" ">
                                                           <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoOriginal" name="montoOriginal"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Monto Modificado:</label>
                                                    <div class="col-sm-3" ">
                                                            <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoModificado" name="montoModificado"  ></div>
                                                    </div>
                                                </div>

                                                <div style="display:none" id="modulo_alcance2">
                                               <div class="form-group" >
                                                    <label for="textInput" class="col-sm-3  ">Alcance Original:</label>
                                                    <div class="col-sm-9" ">
                                                          <textarea   class="form-control" rows="3" id="alcanceOriginal" name="alcanceOriginal">
                                                              
                                                          </textarea>
                                                    </div>

                                                    
                                                </div>

                                                   <div class="form-group">
                                                   

                                                    <label for="textInput" class="col-sm-3  ">Alcance Modificado:</label>
                                                    <div class="col-sm-9" ">
                                                         <textarea   class="form-control" rows="3" id="alcanceModificado" name="alcanceModificado">
                                                               
                                                          </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                                 





























                                            </div>





                                              <div id="view_modificadora3" style="display:none">
                                            <hr>



  
                                                 <div class="text-right">  <a href="#otros" onClick="quitModificadora(3)">[-] Eliminar recurso Modficiatorio</a></div>

                                            

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   ">Modificadora 3:</label>
                                            </div>
                                               
                                                   
                                              

                                                
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Documento:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento">
                                                            <option value="">Seleccione..</option>
                                                            <option value="1">Convenio</option>
                                                            <option value="2">Contrato</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                                          <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Número Documento:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="numDocumento" name="numDocumento"  >
                                                    </div>



                                             
                                                    <label for="textInput" class="col-sm-3  ">Fecha Firma:</label>
                                                    <div class="col-sm-3 ">

                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar firma3"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaFirma3" name="fechaFirma3"  >
                                                    </div>
                                                    </div>
                                              
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-3 ">Adjuntar convenio:</label>
                                                    <div class="col-sm-6">
                                                        <input type="file">
                                                    </div>
                                                </div> 

                                                      <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Modificación:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento" onChange="selectModulo3(this.value)">
                                                            <option value="">Seleccione..</option>
                                                            <option value="fecha">Fecha</option>
                                                            <option value="monto">Monto</option>
                                                            <option value="alcance">Alcance</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                     
                                              <div class="form-group" style="display:none" id="modulo_fecha3">
                                                    <label for="textInput" class="col-sm-3  ">Fecha Inicio:</label>
                                                    <div class="col-sm-3" ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar inicio3"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaInicio3" name="fechaInicio3"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Fecha Modificada:</label>
                                                    <div class="col-sm-3 ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar modificada3"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaModificada3" name="fechaModificada3"  >
                                                    </div>
                                                    </div>
                                                </div>


                                          
                                      
                                     
                                           
                                                <div class="form-group" style="display:none" id="modulo_monto3">
                                                    <label for="textInput" class="col-sm-3  ">Monto Original:</label>
                                                    <div class="col-sm-3" ">
                                                           <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoOriginal" name="montoOriginal"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Monto Modificado:</label>
                                                    <div class="col-sm-3" ">
                                                            <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoModificado" name="montoModificado"  ></div>
                                                    </div>
                                                </div>

                                                <div style="display:none" id="modulo_alcance3">
                                               <div class="form-group" >
                                                    <label for="textInput" class="col-sm-3  ">Alcance Original:</label>
                                                    <div class="col-sm-9" ">
                                                          <textarea   class="form-control" rows="3" id="alcanceOriginal" name="alcanceOriginal">
                                                              
                                                          </textarea>
                                                    </div>

                                                    
                                                </div>

                                                   <div class="form-group">
                                                   

                                                    <label for="textInput" class="col-sm-3  ">Alcance Modificado:</label>
                                                    <div class="col-sm-9" ">
                                                         <textarea   class="form-control" rows="3" id="alcanceModificado" name="alcanceModificado">
                                                               
                                                          </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                                 





























                                            </div>

                
             

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
     <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.number.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link href="<?php echo asset_url();?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <script src="<?php echo asset_url();?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    

    <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">

          var onlyView = 0;
          var v_esquema = 0;




          //Ayudas
             $( function() {
                $( document ).tooltip();
              } );
 
         $(document).ready(function () {

            //var v_recurso = "";
            var v_fecha_asignacion = "";

        

            window.onbeforeunload = function () {
                return "Do you really want to close?";
            };


        precargar_ffinanciamiento();
    
 
            automatizarFecha();


            if($("#numeroPicaso").val()!=""){

                deshabilitarCamposPicaso();
            }


        $("#form_beneficios").submit(function() {
            //search($("#totalBeneficiados").get(0));
            //totalBeneficiados
            return false;
        });
                
         
                     



            

 
         $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */


            if($("#programaSeleccionado").val() == "todos"){
            //alert("Debe seleccionar un Programa, antes de guardar Registro.");
            swal("Advertencia!", "Debe seleccionar un Programa, antes de guardar Registro.", "warning");
            $("#btnGuardar").fadeOut("slow");
          }


        
           <?php
       
            if($editData['onlyView'] == 1){
                ?>
                var onlyView = 1;
                $('#btnGuardar').hide();
            

       
            <?php
            
              echo  '$("#form_datosobra :input").prop("disabled", true);';
              echo  '$("#form_oficio :input").prop("disabled", true);';
              echo  '$("#form_beneficios :input").prop("disabled", true);';
              echo  '$("#form_contrato :input").prop("disabled", true);';
           
              echo  '  $("#form_fechas :input").prop("disabled", true);';
              echo  '  $("#form_domicilio :input").prop("disabled", true);';
            
              echo  '  $("#form_asignaFederal :input").prop("disabled", true);';
              echo  '  $("#form_asignaMunicipal :input").prop("disabled", true);';
              echo  '  $("#form_asignaEstatal :input").prop("disabled", true);';
              echo  '  $("#form_asignaOtros :input").prop("disabled", true);';
            }



            /* if($editData['onlyView'] == 2){
               

              //echo  '$("#form_datosobra :input").prop("disabled", true);';
              //echo  '  $("#form_fechas :input").prop("disabled", true);';


               
              echo  '  $("#form_beneficios :input").prop("disabled", true);';
              echo  '  $("#form_oficio :input").prop("disabled", true);';
              echo  '  $("#form_domicilio :input").prop("disabled", true);';
              echo  '  $("#form_asignaFederal :input").prop("disabled", true);';
              echo  '  $("#form_asignaMunicipal :input").prop("disabled", true);';
              echo  '  $("#form_asignaEstatal :input").prop("disabled", true);';
              echo  '  $("#form_asignaOtros :input").prop("disabled", true);';
            }*/
    ?>  




    
 
         $("#fechaInicioProgramada").keypress(function(event) {event.preventDefault();});
         $("#fechaInicioEjecucion").keypress(function(event) {event.preventDefault();});
         $("#fechaUnicaProgramada").keypress(function(event) {event.preventDefault();});
         $("#fechaUnicaEjecucion").keypress(function(event) {event.preventDefault();});
         $("#fechaFinalProgramada").keypress(function(event) {event.preventDefault();});
         $("#fechaFinalEjecucion").keypress(function(event) {event.preventDefault();});
         $("#fechaEjecucionRecursoFederal").keypress(function(event) {event.preventDefault();});
         $("#fechaEjecucionRecursoEstatal").keypress(function(event) {event.preventDefault();});
         $("#fechaEjecucionRecursoMunicipal").keypress(function(event) {event.preventDefault();});
         $("#fechaEjecucionRecursoOtros").keypress(function(event) {event.preventDefault();});
         $("#fecha_contrato").keypress(function(event) {event.preventDefault();});

         $("#fechaAsignacionRecursoMunicipal").keypress(function(event) {event.preventDefault();});
         $("#fechaAsignacionRecursoFederal").keypress(function(event) {event.preventDefault();});
         $("#fechaAsignacionRecursoEstatal").keypress(function(event) {event.preventDefault();});
         $("#fechaAsignacionRecursoOtros").keypress(function(event) {event.preventDefault();});

         $("#fechaAprobacionObra").keypress(function(event) {event.preventDefault();});



          procesarCambio();

              changeProgram("undefined");


              $('#recurso_sel').on('change', function() {
             
                    cambiarInstanciaRecurso(this.value);



                });




           
                //onLoad page
                cambiarInstanciaRecurso($("#recurso_sel option:selected").val());
                
                

            

               $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejado]:checked').val() == "SI"){

                        $("#calle").prop( "disabled", true );
                        $("#numero").prop( "disabled", true );
                        $("#colonia").prop( "disabled", true );
                        $("#codigoPostal").prop( "disabled", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejado]:checked').val() == "NO"){

                        $("#calle").prop( "disabled", false );
                        $("#numero").prop( "disabled", false );
                        $("#colonia").prop( "disabled", false );
                        $("#codigoPostal").prop( "disabled", false );
                        
                    }

                });

 
               $('input:radio').change(function() {
                    if($('input:radio[name=optionBeneficio]:checked').val() == "1"){


                        $(".unicaprogramada_div").fadeOut("slow");
                        $(".unicaejecucion_div").fadeOut("slow");
                  
                        
                    }


                    if($('input:radio[name=optionBeneficio]:checked').val() != "1"){

                         $(".unicaprogramada_div").fadeIn("slow");
                        $(".unicaejecucion_div").fadeIn("slow");
                        
                    }

                }); 


               
       

        

           (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                monthsTitle: "Meses",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
            }(jQuery));
             
 


            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();

            $('#mujeresBeneficiadas').numeric();
            $('#hombresBeneficiados').numeric();

            $('#numero').numeric();

        

             $('#totalRecursoAsignadoVigilar').number( true, 2 );
             $('#totalRecursoEjecutadoVigilado').number( true, 2 );

             $('#total_people_womans').number( true, 2 );
             $('#total_people_mans').number( true, 2 );

             $('#total_people').number( true, 2 );
             $('#monto_contratado').number( true, 2 );

             

        /*$('#sandbox-container input').datepicker({
    format: "dd/mm/yyyy",
    language: "es"
});*/        
                                               
// "option", "showAnim", $( this ).val()

 
             $( function() {
            $( "#fechaInicioProgramada" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaInicioEjecucion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaUnicaProgramada" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaUnicaEjecucion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });       
            $( "#fechaFinalProgramada" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaFinalEjecucion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" }); 

            $( "#fechaAsignacionRecursoFederal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEjecucionRecursoFederal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            
            $( "#fechaAsignacionRecursoEstatal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEjecucionRecursoEstatal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });

            $( "#fechaAsignacionRecursoMunicipal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEjecucionRecursoMunicipal" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });

            $( "#fechaAsignacionRecursoOtros" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEjecucionRecursoOtros" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaAprobacionObra" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });

            $( "#fecha_contrato").datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            
          } );

             //iconos
              $('.aprobacionobra').click(function() {
                    $("#fechaAprobacionObra").focus();
              });

                $('#fechaAprobacionObra').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });

              $('.inicioprogramada').click(function() {
                    $("#fechaInicioProgramada").focus();
              });

               $('.fechacontratoclass').click(function() {
                    $("#fecha_contrato").focus();
              });

 
         


              $('.inicioejecucion').click(function() {
                    $("#fechaInicioEjecucion").focus();
              });

             $('#fechaInicioEjecucion').on('changeDate', function(ev){
                $(this).datepicker('hide');


                    var startDate = $('#fechaInicioEjecucion').val();
                    var endDate =  $('#fechaFinalEjecucion').val();

                    var dateParts = startDate.split("/");
                    var dateObjectInicial = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  


                    var dateParts = endDate.split("/");
                    var dateObjectFinal = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  


                         
                    if (dateObjectInicial > dateObjectFinal) {
                        alert("Fecha Final Ejecucion debe ser mayor a la Fecha Inicial Ejecucion..");
                        $('#fechaFinalEjecucion').val('');
                    }



            });

              $('#fechaContrato').on('changeDate', function(ev){
                $(this).datepicker('hide');
                   });


              $('.unicaprogramada').click(function() {
                    $("#fechaUnicaProgramada").focus();
              });
 

                  $('#fechaUnicaProgramada').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });


                  
                  $('#fechaUnicaEjecucion').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });

                     $('#fecha_contrato').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });


             
                  

                


            $('.unicaejecucion').click(function() {
                    $("#fechaUnicaEjecucion").focus();
            });   


            $('.finalprogramada').click(function() {
                    $("#fechaFinalProgramada").focus();
            }); 


            $('.finalejecucion').click(function() {
                    $("#fechaFinalEjecucion").focus();
            });                  

                
            $('.asignacionrecursofederal').click(function() {
                    $("#fechaAsignacionRecursoFederal").focus();
            });  
                  


            $('#fechaInicioProgramada').on('changeDate', function(ev){
                $(this).datepicker('hide');

                  var startDate = $('#fechaInicioProgramada').val();
                        var endDate =  $('#fechaFinalProgramada').val();
                        var oficioAprobacion = $("#fechaAprobacionObra").val();

                        var dateParts = startDate.split("/");
                        var dateObjectInicial = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  


                        var dateParts = endDate.split("/");
                        var dateObjectFinal = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  

                        var dateParts = oficioAprobacion.split("/");
                        var dateObjectAprobacionObra = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]-1);  


                         
                        if (dateObjectInicial > dateObjectFinal && $("#statusProyecto").val()=="Iniciado") {
                            alert("Fecha Final Programada debe ser mayor a la Fecha Inicial Programada..");
                            $('#fechaFinalProgramada').val('');
                        }

                        
                        if (dateObjectInicial <= dateObjectAprobacionObra && $("#statusProyecto").val()=="Iniciado") {
                            alert("Fecha de Inicio Programada debe ser mayor o igual a la Fecha Aprobacion de la Obra..");
                            $('#fechaInicioProgramada').val('');
                        }



        
        });

 


            

            $('#fechaAprobacionObra').on('changeDate', function(ev){
                $(this).datepicker('hide');


                //alert($("#fechaAprobacionObra").val());

               /* alert("recurso:"+$("#v_recurso").val());

                alert("fecha_federal:"+$("#v_fecha_asignacion_federal").val());*/

                var oficioAprobacion = $('#fechaAprobacionObra').val();

                var recurso = $("#v_recurso").val();
                var fecha_federal = $("#v_fecha_asignacion_federal").val();
                var fecha_estatal = $("#v_fecha_asignacion_estatal").val();
                var fecha_asignadaFF = "";

                if(recurso == 1){
                    fecha_asignadaFF = fecha_federal;
                }else{
                    fecha_asignadaFF = fecha_estatal;
                }

                        var dateParts = oficioAprobacion.split("/");
                        var dateObjectAprobacionObra = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]-1);  

                        var dateParts = fecha_asignadaFF.split("-");
                        var dateObjectFechaAsignadaFF = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]-1); 



                     if (dateObjectFechaAsignadaFF > dateObjectAprobacionObra) {
                        alert("Fecha Aprobacion de la Obra debe ser mayor o igual a la Fecha Asignacion de FFinanciamiento("+dateParts[2]+"/"+dateParts[1]+"/"+dateParts[0]+")");
                         
                    }

             /*
                        var oficioAprobacion = $("#fechaAprobacionObra").val();
                        var fechaContrato = $("#fecha_contrato").val();
                         var oficioAprobacion = $("#fechaAprobacionObra").val();
 
                        var dateParts = oficioAprobacion.split("/");
                        var dateObjectAprobacionObra = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]-1);  

                        var dateParts = fechaContrato.split("/");
                        var dateObjectFechaContrato = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]-1);  

                          
 


                     if (dateObjectFechaContrato < dateObjectAprobacionObra  && $("#statusProyecto").val()=="Iniciado") {
                        alert("Fecha Contrato debe ser mayor o igual a la Fecha Aprobacion de la Obra..");
                        $('#fecha_contrato').val('');
                    }*/




        
        });


               $('#fecha_contrato').on('changeDate', function(ev){
                $(this).datepicker('hide');

             
                        var oficioAprobacion = $("#fechaAprobacionObra").val();
                        var fechaContrato = $("#fecha_contrato").val();
                         var oficioAprobacion = $("#fechaAprobacionObra").val();
 
                        var dateParts = oficioAprobacion.split("/");
                        var dateObjectAprobacionObra = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]-1);  

                        var dateParts = fechaContrato.split("/");
                        var dateObjectFechaContrato = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]-1);  

                          
 


                     if (dateObjectFechaContrato < dateObjectAprobacionObra  && $("#statusProyecto").val()=="Iniciado") {
                        alert("Fecha Contrato debe ser mayor o igual a la Fecha Aprobacion de la Obra..");
                        $('#fecha_contrato').val('');
                    }




        
        });

            
        $('#fechaFinalProgramada').on('changeDate', function(ev){
            
            $(this).datepicker('hide');

              
                          var startDate = $('#fechaInicioProgramada').val();
                            var endDate =  $('#fechaFinalProgramada').val();
                               var oficioAprobacion = $("#fechaAprobacionObra").val();

                         

                            var dateParts = startDate.split("/");
                            var dateObjectInicial = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  


                            var dateParts = endDate.split("/");
                            var dateObjectFinal = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  

                                var dateParts = oficioAprobacion.split("/");
                        var dateObjectAprobacionObra = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  

 
                             
            if (dateObjectInicial > dateObjectFinal) {
                alert("Fecha Final Programada debe ser mayor a la Fecha Inicial Programada..");
                $('#fechaFinalProgramada').val('');
            }

            if (dateObjectFinal < dateObjectAprobacionObra  ) {
                alert("Fecha Final Programada debe ser mayor o igual a la Fecha Aprobacion de la Obra..");
                $('#fechaFinalProgramada').val('');
            }




        });




            $('#fechaInicioEjecucion').on('changeDate', function(ev){
                    

  

                    var startDate = $('#fechaInicioEjecucion').val();
                    var inicioProgramadaDate = $('#fechaInicioProgramada').val();
                    var oficioAprobacion = $("#fechaAprobacionObra").val();


                
                    var dateParts = startDate.split("/");
                    var dateObjectInicial = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  


                  
                    var dateParts = inicioProgramadaDate.split("/");
                    var dateObjectInicioProgramada = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]-1);  


                    var dateParts = oficioAprobacion.split("/");
                    var dateObjectAprobacionObra = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  

                         
                    if (dateObjectInicial <= dateObjectInicioProgramada) {
                        alert("Fecha Inicio de Ejecucion debe ser mayor a la Fecha de Inicio Programada..");
                        $('#fechaInicioEjecucion').val('');
                    }


                    if (dateObjectInicial < dateObjectAprobacionObra  ) {
                        alert("Fecha Inicio de Ejecución debe ser mayor o igual a la Fecha Aprobacion de la Obra..");
                        $('#fechaInicioEjecucion').val('');
                    }





            });
          

                  

          
            $('#fechaFinalEjecucion').on('changeDate', function(ev){
                    $(this).datepicker('hide');





                    var startDate = $('#fechaInicioEjecucion').val();
                    var endDate =  $('#fechaFinalEjecucion').val();
                    var oficioAprobacion = $("#fechaAprobacionObra").val(); 
                    var inicioProgramadaDate = $('#fechaInicioProgramada').val();
                 

                      
                    var dateParts = startDate.split("/");
                    var dateObjectInicial = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  


                    var dateParts = endDate.split("/");
                    var dateObjectFinal = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  

               
                    var dateParts = oficioAprobacion.split("/");
                    var dateObjectAprobacionObra = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  


                    var dateParts = inicioProgramadaDate.split("/");
                    var dateObjectInicioProgramada = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]-1);  

 



                         
                    if (dateObjectInicial > dateObjectFinal) {
                        alert("Fecha Final Ejecucion debe ser mayor a la Fecha Inicial Ejecucion..");
                        $('#fechaFinalEjecucion').val('');
                    }

                 
                    if (dateObjectFinal <= dateObjectInicioProgramada) {
                         alert("Fecha Final Ejecucion debe ser mayor a la Fecha de Inicio Programada..");
                        $('#fechaFinalEjecucion').val('');
                    }


                   
                    if (dateObjectFinal < dateObjectAprobacionObra  ) {
                        alert("Fecha Final de Ejecución debe ser mayor o igual a la Fecha Aprobacion de la Obra..");
                        $('#fechaFinalEjecucion').val('');
                    }








            });


            $('#fechaAsignacionRecursoFederal').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });



                 




              $('.ejecucionrecursofederal').click(function() {
                    $("#fechaEjecucionRecursoFederal").focus();
              }); 


             $('#fechaEjecucionRecursoFederal').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });





                $('.asignacionrecursoestatal').click(function() {
                    $("#fechaAsignacionRecursoEstatal").focus();
              }); 

                   $('#fechaAsignacionRecursoEstatal').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });



                $('.ejecurcionrecursoestatal').click(function() {
                    $("#fechaEjecucionRecursoEstatal").focus();
              }); 

                


                   $('#fechaEjecucionRecursoEstatal').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });



                $('.asignacionrecursomunicipal').click(function() {
                    $("#fechaAsignacionRecursoMunicipal").focus();
              }); 

                     $('#fechaAsignacionRecursoMunicipal').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });


                


                $('.ejecucionrecursomunicipal').click(function() {
                    $("#fechaEjecucionRecursoMunicipal").focus();
              }); 



                          $('#fechaEjecucionRecursoMunicipal').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });


                


               $('.asignacionrecursootros').click(function() {
                    $("#fechaAsignacionRecursoOtros").focus();
              }); 

               


                          $('#fechaAsignacionRecursoOtros').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });



                $('.ejecucionrecursootros').click(function() {
                    $("#fechaEjecucionRecursoOtros").focus();
              }); 

                              $('#fechaEjecucionRecursoOtros').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });


              
                
                
                  
                



                

            
              $('#montoRecursoAsignadoFederal').number( true, 2 );
              $('#montoRecursoEjecutadoFederal').number( true, 2 );

               $('#montoRecursoAsignadoEstatal').number( true, 2 );
              $('#montoRecursoEjecutadoEstatal').number( true, 2 );

              $('#montoRecursoAsignadoMunicipal').number( true, 2 );
              $('#montoRecursoEjecutadoMunicipal').number( true, 2 );

              $('#montoRecursoAsignadoOtros').number( true, 2 );
              $('#montoRecursoEjecutadoOtros').number( true, 2 );



            
           

  

            

          });    




       

            

 



            $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });


             function resetearForma(){
 
           
           location.href="<?php echo site_url();?>/registro_proyecto";
            /*$(".form-control").each(function() {
               $(this).val("");  */
                
   
          
        }


        function selCancel(choice)
        {
            if(choice=="Si"){
                $("#cuadro_cancelacion").show("slow");
            }

            if(choice=="No"){
                $("#cuadro_cancelacion").hide("slow");
            }

        }


        function deshabilitar(){


            <?php
            
              echo  '$("#form_datosobra :input").prop("disabled", true);';
              echo  '$("#form_oficio :input").prop("disabled", true);';
              echo  '$("#form_beneficios :input").prop("disabled", true);';
              echo  '$("#form_cancelacion :input").prop("disabled", true);';
              echo  '$("#form_contrato :input").prop("disabled", true);';
           
              echo  '  $("#form_fechas :input").prop("disabled", true);';
              echo  '  $("#form_domicilio :input").prop("disabled", true);';
            
              echo  '  $("#form_asignaFederal :input").prop("disabled", true);';
              echo  '  $("#form_asignaMunicipal :input").prop("disabled", true);';
              echo  '  $("#form_asignaEstatal :input").prop("disabled", true);';
              echo  '  $("#form_asignaOtros :input").prop("disabled", true);';
            ?>

        }



        function cancelObra(choice){

            //alert(choice)

            if($("#motivo_cancelacion").val()=="")
                alert("Si deseas Cancelar, debes dejar texto del Motivo de Cancelacion..")
            else{ 
           if(choice=="Si")
              swal({
                              title: "Estás seguro de querer cancelar la Obra?",
                              text: "",
                              icon: "warning",
                              buttons: true,
                              dangerMode: false,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                                swal("Listo!, Se ha Cancelado la obra Exitosamente", {
                                  icon: "success",
                                });
                               // eliminarProyectoAjax(id_proyecto, fk_departamento);
                                deshabilitar();
                              } else {
                                swal("Lo dejamos como estaba..");
                              }
                            });
            }

        }



        function selectPicaso(elem){

            if(elem == "No")
            {
                $("#no_picaso").hide('slow');
            }

            if(elem == "Si")
            {
                $("#no_picaso").show('slow');
            }

           
        }



        function cambiarInstanciaRecurso(resourceSelected){


  

                      if(resourceSelected == 2 || v_esquema == 1){




                        $("#presupuesto_pef").prop("disabled", "true");
                        $("#instancia_arriba").html("Instancia Promotora:");
                        v_esquema = 1;

                    }else{
                

                      
                        $("#tag_normativa").show();
                        $("#instancia_arriba").html("Instancia Normativa:");
               


                    } 

                        
                        

        }

        

       function cleanElement(){

            swal({
              title: "Estas seguro de Crear nuevo Registro?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: false,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Listo!, Empezamos de nuevo la captura", {
                  icon: "success",
                });
                resetearForma();

              } else {
                swal("Lo dejamos como estaba..");
              }
            });
        }





        

        function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");

 
                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad.toUpperCase() + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
        }   



     



         function precargar_picaso() {
             
                var cantElement = 0;
                var numPicaso = $("#numeroPicaso").val();


                if(numPicaso==""){
                    alert("Debes poner un numero de Obra ejemplo 2018-000##");
                  
                }else{
           /*     $('#listObras').empty();
                $('#listObras').append("<option>Cargando ....</option>");*/
                        $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('registro_proyecto/ajax_getDataPicaso') ?>/" + numPicaso,
                            contentType: "application/json;charset=utf-8",
                            dataType: 'json',
                            success: function (data) {

                               // console.log(data);
                                /*$('#listObras').empty();
                                $('#listObras').append("<option>Selecciona Comité..</option>");
        */

                                 $.each(data, function (i, name) {
                                    console.log(name);

                                   /* var selected = "";
                                    var accion_conexion = "<?=$editData['accion_conexion']?>";
                                    if(name.id === accion_conexion){
                                        selected = "selected";
                                    } */

                                    $("#nombreProyecto").val(name.nombre_obra);
                                    $("#montoRecursoAsignadoFederal").val(name.total_aprobado);
                                    $("#montoRecursoEjecutadoFederal").val(name.total_ejercido);
                                    $("#fechaAprobacionObra").val(name.fecha_primer_aprobacion);
                                    $("#montoRecursoAsignadoFederal").val(name.total_aprobado);
                                    $("#montoRecursoEjecutadoFederal").val(name.total_ejercido);


                                  
                                   deshabilitarCamposPicaso();
                                    
                                   // $('#listObras').append('<option value="' + name.id + '"'+selected+'>' + name.nombre_comite + '</option>');
                                    cantElement++;



                                });
 

                                 


                                 sumaRecursoaVigilar();



                                 
                            }
                        });
                }
            }  





         function precargar_ffinanciamiento() {

         //   alert("precargando Financiamiento...");
             
                var cantElement = 0;
                var fk_ffinanciamiento = "<?=$editData['fk_programa_recurso']?>";

               // alert(fk_ffinanciamiento);

                if(fk_ffinanciamiento==""){
                    alert("Debes capturar una Fuente de Financiamiento, antes de Capturar una Obra...");
                    location.href = "indicadores_generales";
                  
                }else{
        


                $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('registro_ffinanciamiento/ajax_getDataFFinanciamiento') ?>/" + fk_ffinanciamiento,
                            contentType: "application/json;charset=utf-8",
                            dataType: 'json',
                            success: function (data) {

                               // console.log(data);
                                /*$('#listObras').empty();
                                $('#listObras').append("<option>Selecciona Comité..</option>");
        */

                                 $.each(data, function (i, name) {
                                    console.log(name);


 
                                   /* var selected = "";
                                    var accion_conexion = "<?=$editData['accion_conexion']?>";
                                    if(name.id === accion_conexion){
                                        selected = "selected";
                                    } */

                                    //alert("valor:" + name.federal_recurso_monto_asignado_recurso1);
                                    if(name.resumen_total_federal == "" || name.resumen_total_federal == null || name.resumen_total_federal == 0){
                                        $("#form_asignaFederal :input").prop("disabled", true);
                                    }


                                      if(name.resumen_total_estatal == "" || name.resumen_total_estatal == null || name.resumen_total_estatal == 0){
                                        $("#form_asignaEstatal :input").prop("disabled", true);
                                    }


                                     if(name.resumen_total_municipal == "" || name.resumen_total_municipal == null || name.resumen_total_municipal == 0){
                                        $("#form_asignaMunicipal :input").prop("disabled", true);
                                    }



                                      if(name.resumen_total_otros == "" || name.resumen_total_otros == null || name.resumen_total_otros == 0){
                                        $("#form_asignaOtros :input").prop("disabled", true);
                                    }

                                    $("#v_fecha_asignacion_federal").val(name.federal_recurso_fecha_asignacion_recurso1);
                                    $("#v_fecha_asignacion_estatal").val(name.estatal_recurso_fecha_asignacion_recurso1);

                                       

                                    /*$("#nombreProyecto").val(name.nombre_obra);
                                    $("#montoRecursoAsignadoFederal").val(name.total_aprobado);
                                    $("#montoRecursoEjecutadoFederal").val(name.total_ejercido);
                                    $("#fechaAprobacionObra").val(name.fecha_primer_aprobacion);
                                    $("#montoRecursoAsignadoFederal").val(name.total_aprobado);
                                    $("#montoRecursoEjecutadoFederal").val(name.total_ejercido);


                                  
                                   deshabilitarCamposPicaso();*/
                                    
                                   // $('#listObras').append('<option value="' + name.id + '"'+selected+'>' + name.nombre_comite + '</option>');
                                    cantElement++;



                                });
 

                                 


                                // sumaRecursoaVigilar();



                                 
                            }
                        });
                }
            }  










        function deshabilitarCamposPicaso(){

              $("#nombreProyecto").prop("readOnly", true);
                                    $("#montoRecursoAsignadoFederal").prop("readOnly", true);
                                    $("#montoRecursoEjecutadoFederal").prop("readOnly", true);
                                    $("#fechaAprobacionObra").prop("readOnly", true);
                                    $("#montoRecursoAsignadoFederal").prop("readOnly", true);
                                    $("#montoRecursoEjecutadoFederal").prop("readOnly", true);

        }

        function sumaPeople(){

            var total = 0;

             $(".t_people").each(function() {


                   if (this.value!="") {

                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
                   
                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
     
                });


                x = total;

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");
          

                  $("#totalBeneficiados").val(total);

              

        }


        

        function preGuardado(){



                var totalBeneficios;
                var municipioSelected;
                var localidadSelected;
                var vigilar;
                var vigilado;
 


                $("#instanciaEjecutora_tmp").val($("#instanciaEjecutora").val());
                $("#instanciaPromotora_tmp").val($("#cataloInstanciaPromotora").val());
                totalBeneficios = $("#totalBeneficiados").val();
                $("#totalBeneficios").val(totalBeneficios);


                //DOMICILIO DEL PROYECTO


                   municipioSelected = $("#municipio").val();
                   $("#municipioSelected").val(municipioSelected);

                   localidadSelected = $("#localidad").val();
                    $("#localidadSelected").val(localidadSelected);



                 vigilar = $("#totalRecursoAsignadoVigilar").val();
                 $("#totalRecursoVigilar").val(vigilar);

                 vigilado = $("#totalRecursoEjecutadoVigilado").val();
                 $("#totalRecursoVigilado").val(vigilado);

 

 
             var dataForms = $("#form_datosobra").serialize() + "&" + $("#form_oficio").serialize() + "&" + $("#form_beneficios").serialize() + "&" + $("#form_fechas").serialize() + "&" + $("#form_domicilio").serialize() + "&" +  $("#form_asignaFederal").serialize() + "&" +  $("#form_asignaEstatal").serialize() + "&" +  $("#form_asignaMunicipal").serialize() + "&" +  $("#form_asignaOtros").serialize() + "&" +  $("#form_vigila").serialize()  + "&" +  $("#form_contrato").serialize();

            
            
                   $.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarDatosGeneralesObra') ?>",
                    type: "POST",
                    data: dataForms,
                    dataType: "JSON",
                    success: function (data)
                    {
                        
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                            //console.log("message:" + obj.msg);
                            $("#id_proyecto").val(message);

                           

                         /*   
                            $("#id_proyecto_otros").val(message);
                            $("#id_proyecto_municipal").val(message);
                            $("#id_proyecto_estatal").val(message);
                            $("#id_proyecto_federal").val(message);

                            $("#id_proyecto_fechaobra").val(message);
                            $("#id_proyecto_domicilio").val(message);
                            $("#id_proyecto_picaso").val(message);
                            $("#id_proyecto_beneficio").val(message);

                            $("#id_proyecto_vigila").val(message);  

                        */

                        });
                       
                      
                        
 
                    }
                });
        }


        


        function validarREAsignadoOtros(elem){

            var valorAsignado = elem.value;
            var montoRecursoEjecutadoOtros= $("#montoRecursoEjecutadoOtros").val();

            $(elem).css({"background-color": "white"});
            $("#montoRecursoEjecutadoOtros").css({"background-color": "white"});
             
            if (valorAsignado < montoRecursoEjecutadoOtros && valorAsignado!="" && montoRecursoEjecutadoOtros!="") {
                            alert("Monto de Recurso Asignado Otros (A Vigilar), No puede ser menor al Monto del Recurso Ejecutado Otros (Vigilado)...");
                             $(elem).css({"background-color": "yellow"});
            }
        }


        function validarREVigiladoOtros(elem){

            var valorVigilado = elem.value;
            var montoRecursoAsignadoOtros = $("#montoRecursoAsignadoOtros").val();

            $(elem).css({"background-color": "white"});
            $("#montoRecursoAsignadoOtros").css({"background-color": "white"});
             
            if (valorVigilado > montoRecursoAsignadoOtros && valorVigilado!="" && montoRecursoAsignadoOtros!="") {

                            alert("Monto del Recurso Ejecutado Otros (Vigilado), No puede ser mayor al Monto Del Recurso Asignado Otros a (Vigilar)...");
                             $(elem).css({"background-color": "yellow"});
            }
        }

        function validarREAsignadoMunicipal(elem){

            var valorAsignado = elem.value;
            var recursoEjecutadoMunicipal= $("#montoRecursoEjecutadoMunicipal").val();

            $(elem).css({"background-color": "white"});
            $("#montoRecursoEjecutadoMunicipal").css({"background-color": "white"});
             
            if (valorAsignado < recursoEjecutadoMunicipal && valorAsignado!="" && recursoEjecutadoMunicipal!="") {
                            alert("Monto de Recurso Asignado Municipal(A Vigilar), No puede ser menor al Monto del Recurso Ejecutado Municipal (Vigilado)...");
                             $(elem).css({"background-color": "yellow"});
            }
        }


        function validarREVigiladoMunicipal(elem){

            var valorVigilado = elem.value;
            var montoRecursoAsignadoMunicipal= $("#montoRecursoAsignadoMunicipal").val();

            $(elem).css({"background-color": "white"});
            $("#montoRecursoAsignadoMunicipal").css({"background-color": "white"});
             
            if (valorVigilado > montoRecursoAsignadoMunicipal && valorVigilado!="" && montoRecursoAsignadoMunicipal!="") {
                            alert("Monto del Recurso Ejecutado Municipal (Vigilado), No puede ser mayor al Monto Del Recurso Asignado Municipal a (Vigilar)...");
                             $(elem).css({"background-color": "yellow"});
            }
        }



       function validarREAsignadoEstatal(elem){

            var valorAsignado = elem.value;
            var recursoEjecutadoEstatal= $("#montoRecursoEjecutadoEstatal").val();

            $(elem).css({"background-color": "white"});
            $("#montoRecursoEjecutadoEstatal").css({"background-color": "white"});
             
            if (valorAsignado < recursoEjecutadoEstatal && valorAsignado!="" && recursoEjecutadoEstatal!="") {
                            alert("Monto de Recurso Asignado Estatal(A Vigilar), No puede ser menor al Monto del Recurso Ejecutado Estatal (Vigilado)...");
                             $(elem).css({"background-color": "yellow"});
            }
        }


        function validarREVigiladoEstatal(elem){

            var valorVigilado = elem.value;
            var recursoAsignadoEstatal= $("#montoRecursoAsignadoEstatal").val();

            $(elem).css({"background-color": "white"});
            $("#montoRecursoAsignadoEstatal").css({"background-color": "white"});
             
            if (valorVigilado > recursoAsignadoEstatal && valorVigilado!="" && recursoAsignadoEstatal!="") {
                            alert("Monto del Recurso Ejecutado Estatal (Vigilado), No puede ser mayor al Monto Del Recurso Asignado Estatal a (Vigilar)...");
                             $(elem).css({"background-color": "yellow"});
            }
        }


        

        function validarREAsignadoFederal(elem){

            var valorAsignado = elem.value;
            var recursoEjecutadoFederal= $("#montoRecursoEjecutadoFederal").val();

            $(elem).css({"background-color": "white"});
            $("#montoRecursoEjecutadoFederal").css({"background-color": "white"});
             
            if (valorAsignado < recursoEjecutadoFederal && valorAsignado!="" && recursoEjecutadoFederal!="") {
                            alert("Monto de Recurso Asignado Federal(A Vigilar), No puede ser menor al Monto del Recurso Ejecutado Federal (Vigilado)...");
                             $(elem).css({"background-color": "yellow"});
            }
        }


        function validarREVigiladoFederal(elem){

            var valorVigilado = elem.value;
            var recursoAsignadoFederal= $("#montoRecursoAsignadoFederal").val();

            $(elem).css({"background-color": "white"});
            $("#montoRecursoAsignadoFederal").css({"background-color": "white"});
             
            if (valorVigilado > recursoAsignadoFederal && valorVigilado!="" && recursoAsignadoFederal!="") {
                            alert("Monto del Recurso Ejecutado Federal (Vigilado), No puede ser mayor al Monto Del Recurso Asignado Federal a (Vigilar)...");
                             $(elem).css({"background-color": "yellow"});
            }
        }

        function editar(){

            //$("#btnNuevo").show("slow");
            $("#btnGuardar").show("slow");


           //  $("#comentariosProyecto").prop("readOnly", false);
           //   alert("hola");
            <?php
            
            if($editData['onlyView'] == 1){


                  echo  '$("#form_datosobra :input").prop("disabled", false);';
              echo  '$("#form_oficio :input").prop("disabled", false);';
              echo  '$("#form_beneficios :input").prop("disabled", false);';
              echo  '$("#form_contrato :input").prop("disabled", false);';
           
              echo  '  $("#form_fechas :input").prop("disabled", false);';
              echo  '  $("#form_domicilio :input").prop("disabled", false);';
            
              echo  '  $("#form_asignaFederal :input").prop("disabled", false);';
              echo  '  $("#form_asignaMunicipal :input").prop("disabled", false);';
              echo  '  $("#form_asignaEstatal :input").prop("disabled", false);';
              echo  '  $("#form_asignaOtros :input").prop("disabled", false);';

                  
      
              /*   echo  '  $("#nombreProyecto").prop("readOnly", false);';
             
                  echo  '  $("#comentariosProyecto").prop("readOnly", false);';
                  echo  '  $("#form_fechas :input").prop("readOnly", false);';

                  echo  '  $("#form_beneficios :input").prop("readOnly", false);';
                  echo  '  $("#form_oficio :input").prop("readOnly", false);';
                  echo  '  $("#form_domicilio :input").prop("readOnly", false);';
                  echo  '  $("#form_asignaFederal :input").prop("readOnly", false);';
                  echo  '  $("#form_asignaMunicipal :input").prop("readOnly", false);';
                  echo  '  $("#form_asignaEstatal :input").prop("readOnly", false);';
                  echo  '  $("#form_asignaOtros :input").prop("readOnly", false);'; 
                    

                     echo  '$("#enPicaso").removeAttr("disabled");';
                     echo  '$("#instanciaEjecutora").removeAttr("disabled");';
                     echo  '$("#cataloInstanciaPromotora").removeAttr("disabled");';*/
 



                }
              ?>

              onlyView = 0;


        }


        function guardar(){

           if(onlyView==1){
            return;
           }

            var statusProyecto;
            statusProyecto = $("#statusProyecto").val();
            $("#statusProyectoSelected").val(statusProyecto);



             

                 
            //Validacion
 
                    $("#nombreProgramaPresupuestario").css({"background-color": "white"});
              
 
                    $("#nombreProyecto").css({"background-color": "white"});

                     $("#instanciaEjecutora").css({"background-color": "white"});
                     $("#cataloInstanciaPromotora").css({"background-color": "white"});
                     $("#totalBeneficiados").css({"background-color": "white"});
                     $("#municipio").css({"background-color": "white"});
                     $("#localidad").css({"background-color": "white"});
                     $("#optionBeneficio").css({"background-color": "white"});
                     $("#totalBeneficiados").css({"background-color": "white"});
                     $("#enPicaso").css({"background-color": "white"});
                     $("#fechaInicioProgramada").css({"background-color": "white"});
                     $("#fechaFinalProgramada").css({"background-color": "white"});
                       $("#numeroPicaso").css({"background-color": "white"});
                       $("#oficioAprobacion").css({"background-color": "white"});
                       $("#fechaAprobacionObra").css({"background-color": "white"});
                       $("#calle").css({"background-color": "white"});
                       $("#numero").css({"background-color": "white"});
                       $("#colonia").css({"background-color": "white"});
                       $("#codigoPostal").css({"background-color": "white"});
                        $("#totalRecursoAsignadoVigilar").css({"background-color": "white"});
                         $("#monto_contratado").css({"background-color": "white"});
         

                var msgError = "";


             

                if( $("#nombreProgramaPresupuestario").val() == ""){
                    msgError+= "- Debe seleccionar al menos un Programa Presupuestario, \n";
                    $("#nombreProgramaPresupuestario").css({"background-color": "yellow"});
                }

                if( $('input[name=optionBeneficio]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar al menos un Tipo de Beneficio,\n";
                    $("#optionBeneficio").css({"background-color": "yellow"});
                }




              




                if( $("#nombreProyecto").val() == ""){
                    msgError+= "- Debe tener Nombre Obra / Apoyo / Servicio,  \n ";
                    $("#nombreProyecto").css({"background-color": "yellow"});
                }

            
                  if( $("#oficioAprobacion").val() == ""){
                    msgError+= "- Debe tener Oficio de Aprobacion o Equivalente  \n ";
                    $("#oficioAprobacion").css({"background-color": "yellow"});
                }

                  if( $("#fechaAprobacionObra").val() == ""){
                    msgError+= "- Debe tener Fecha Aprobacion de la Obra  \n ";
                    $("#fechaAprobacionObra").css({"background-color": "yellow"});
                }


              /*    if( $("#totalBeneficiados").val() == ""){
                    msgError+= "- Debe tener Total de Beneficiarios  \n ";
                    $("#totalBeneficiados").css({"background-color": "yellow"});
                }*/

                   if( $("#fechaInicioProgramada").val() == ""){
                    msgError+= "- Debe tener Fecha de Inicio Programada  \n ";
                    $("#fechaInicioProgramada").css({"background-color": "yellow"});
                }

                   if( $("#fechaFinalProgramada").val() == ""){
                    msgError+= "- Debe tener Fecha de Final Programada:  \n ";
                    $("#fechaFinalProgramada").css({"background-color": "yellow"});
                }


              


                  


                if($("#instanciaEjecutora option:selected").val()==0){
                    msgError+= "- Debe seleccionar Instancia Ejecutora,  \n ";
                    $("#instanciaEjecutora").css({"background-color": "yellow"});
                }


                if($("#enPicaso option:selected").val()==0){
                    msgError+= "- Debe seleccionar Oficio de Aprobación en Picaso,  \n ";
                    $("#enPicaso").css({"background-color": "yellow"});
                }


                 if($("#enPicaso option:selected").val()=="Si"){

                    
                    if($("#numeroPicaso").val() == ""){

                    msgError+= "- Debe poner Numero Obra en Picaso,  \n ";
                    $("#numeroPicaso").css({"background-color": "yellow"});
                    }
                }






                

                var titulo_promotora = "Normativa";
                if(v_esquema){
                    titulo_promotora = "Promotora";
                }
                  if($("#cataloInstanciaPromotora option:selected").val()==0){
                    msgError+= "- Debe seleccionar Instancia " + titulo_promotora +  ",  \n ";
                    $("#cataloInstanciaPromotora").css({"background-color": "yellow"});
                }

 

                 if($("#totalBeneficiados").val() == "" || $("#totalBeneficiados").val() == 0){
                    msgError+= "- Debe poner Total Beneficiarios  \n ";
                    $("#totalBeneficiados").css({"background-color": "yellow"});
                }



                if($("#municipio option:selected").val()==""){
                    msgError+= "- Debe seleccionar Municipio,  \n ";
                    $("#municipio").css({"background-color": "yellow"});
                }



                 if($("#localidad option:selected").val()=="Selecciona Localidad.."){
                    msgError+= "- Debe seleccionar Localidad,  \n ";
                    $("#localidad").css({"background-color": "yellow"});
                }


                 if( $('input[name=domicilioCortejado]:checked').val() == undefined){
                    msgError+= "- Debe seleccionar opcion de Domicilio Conocido,\n";
                    $("#domicilioCortejado").css({"background-color": "yellow"});
                }


                if( $('input[name=domicilioCortejado]:checked').val() == "NO"){

                    if($("#calle").val() == "") {
                        msgError+= "- Debe anotar la Calle,\n";
                        $("#calle").css({"background-color": "yellow"});
                    }

                    if($("#numero").val() == "") {
                        msgError+= "- Debe anotar número de la Calle,\n";
                        $("#numero").css({"background-color": "yellow"});
                    }

                     if($("#colonia").val() == "") {
                        msgError+= "- Debe anotar la Colonia,\n";
                        $("#colonia").css({"background-color": "yellow"});
                    }

                    if($("#codigoPostal").val() == "") {
                        msgError+= "- Debe anotar el Código Postal,\n";
                        $("#codigoPostal").css({"background-color": "yellow"});
                    }






                }




                if(parseFloat($("#totalRecursoAsignadoVigilar").val())> parseFloat($("#monto_contratado").val()) && $("#monto_contratado").val()!="" && $("#totalRecursoAsignadoVigilar").val()!="")
                {
                     msgError+= "- El Monto del Total de Recurso Asignado (A Vigilar) No puede ser Mayor al Monto Contratado  \n ";
                    $("#monto_contratado").css({"background-color": "yellow"});
                }
                    

                 if( $("#totalRecursoAsignadoVigilar").val() == "" || $("#totalRecursoAsignadoVigilar").val() == 0){
                    msgError+= "- Debe asignar el Monto de Recurso Asignado en alguno de los Recursos:  \n ";
                    $("#totalRecursoAsignadoVigilar").css({"background-color": "yellow"});
                }


                alert("recurso:"+v_recurso);


            
            /* var presupuestoVigilar = parseFloat(<?=$editData['presupuesto_vigilar_cf']?>);
             
             îf(presupuestoVigilar < parseFloat(30000)){

                      msgError+= "- La suma de Total de Recurso Asignado (A Vigilar) debe ser menor al Presupuesto a Vigilar por la CS de FFinanciamiento  \n ";
             } 



              if( $("#totalRecursoAsignadoVigilar").val() == "" || $("#totalRecursoAsignadoVigilar").val() == 0){
                        msgError+= "- Debe asignar el Monto de Recurso Asignado en alguno de los Recursos:  \n ";
                        $("#totalRecursoAsignadoVigilar").css({"background-color": "yellow"});
                    }*/


              //      else{
                    


                    

                    
              //  }

                
                                


                
                if(msgError!=""){
                     swal("Error!", msgError, "error");
                     return;
                }else{

                   preGuardado();

                }


                //Si todo okm, entonces mandamos los datos al Servidor.
                var status, message;

 

         

                //$("#folio").val(message);


                var picasoSelected;
              
                  


                /*  $.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarDatosOficio') ?>",
                    type: "POST",
                    data: $('#form_oficio').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });*/



            //BENEFICIOS
              
 

           /* $.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarBeneficios') ?>",
                    type: "POST",
                    data: $('#form_beneficios').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });*/


 

            //FECHAS DEL PROYECTO

           /*  $.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarFechasProyecto') ?>",
                    type: "POST",
                    data: $('#form_fechas').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                }); */




               
/*
              $.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarDomicilioProyecto') ?>",
                    type: "POST",
                    data: $('#form_domicilio').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });
*/

              
 

              /*$.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarAsignaFederal') ?>",
                    type: "POST",
                    data: $('#form_asignaFederal').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                }); */


               /* $.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarAsignaEstatal') ?>",
                    type: "POST",
                    data: $('#form_asignaEstatal').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });*/

/*
                  $.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarAsignaMunicipal') ?>",
                    type: "POST",
                    data: $('#form_asignaMunicipal').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


                 $.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarAsignaOtros') ?>",
                    type: "POST",
                    data: $('#form_asignaOtros').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });
*/

                

/*
                 $.ajax({
                    url: "<?php echo site_url('registro_proyecto/guardarRecursosVigila') ?>",
                    type: "POST",
                    data: $('#form_vigila').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });*/


 




            swal("Bien hecho!", "El proyecto ha sido guardado.", "success");
        }



        function sumaRecursoaVigilar(){


              var total = 0;

                $(".t_vigilar").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

                x = total;

                  //$("#total_recursoFederal").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalRecursoAsignadoVigilar").val(total);
          
           
            
           

        }



          function sumaRecursoaVigilado(){

                 var total = 0;

                $(".t_vigilado").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

                x = total;

                  //$("#total_recursoFederal").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalRecursoEjecutadoVigilado").val(total);
          



           
            
           

        }





        function changeProgram(checked){



           

       
               // $("#recurso_sel").val(recurso).change();



            if(checked!="undefined"){
               
                swal({
                  title: "Estás seguro de querer cambiar Programa?",
                  text: "",
                  icon: "warning",
                  buttons: true,
                  dangerMode: false, 
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Listo!, Se cambio programa exitosamente", {
                      icon: "success",
                    });
                    //location.reload(true);

        procesarCambio();


                  } else {
                    swal("Lo dejamos como estaba..");
                  }
                });
            }


             
 


           // populate_list_obras(id_programSelected);
         
               
        }




        function procesarCambio(){

               var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();

              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }
         
               // validarExistenciaFFinanciamiento(id_programSelected);

               //l();
 
              

               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            recurso = obj.recurso;
                            esquema = obj.esquema;
                            cuadro_f = obj.cuadroF;
                            cuadro_e = obj.cuadroE;
                            cuadro_m = obj.cuadroM;
                            cuadro_o = obj.cuadroO;
                        });


                      $("#v_recurso").val(recurso);
                        
         
                      $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                      // $("#titleProgram").fadeIn("slow");
                      $("#nombreProgramaPresupuestariol").val(titulo_programa);
 

                    if(cuadro_f == 1){
                        $("#divFederal").show("slow");
                      }else{
                        $("#divFederal").hide("slow");
                      }


                    if(cuadro_e == 1){
                        $("#divEstatal").show("slow");
                      }else{
                        $("#divEstatal").hide("slow");
                      }

                    if(cuadro_m == 1){
                        $("#divMunicipal").show("slow");
                      }else{
                        $("#divMunicipal").hide("slow");
                      }

                    if(cuadro_o == 1){
                        $("#divOtros").show("slow");
                      }else{
                        $("#divOtros").hide("slow");
                      }

                      

                    if(recurso == 1){

                        $("#patcs_menu").hide();
                        $("#petcs_menu").show("slow");
                        $("#documentos_menu").show("slow");

                        $("#seguimiento_menu").hide("slow");
                        $("#reuniones_menu").show("slow");

                        $("#informes_menu").show("slow");
                        $("#formatos_menu").hide("slow");

                    }

                    
                    v_esquema = 0;

                    if(esquema==1){
                      v_esquema = 1;
                    }
                     

                     if(recurso == 2 || v_esquema == 1){

                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow");
                        $("#documentos_menu").show("slow");

                        $("#reuniones_menu").hide("slow");
                        $("#seguimiento_menu").show("slow");
                       // v_esquema = 0;

                        $("#informes_menu").hide("slow");
                        $("#formatos_menu").show("slow");

                        
                    }


                    //iNSTANCIA
                    cambiarInstanciaRecurso($("#recurso_sel option:selected").val());
                   
                    }


                  

                });


                /*  if(!existeFFinanciamiento())
                    alert("no debe estar aqui");
                else
                    alert("esta bien que estes aqui");*/
        }



         function addCommas(obj){

                //alert(obj.id);

                x = obj.value;

                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                obj.value = parts.join(".");

            
        }   


 
 
function automatizarFecha(){
    
    //alert(today);
        var today = new Date();
        var endDate =  $('#fechaFinalProgramada').val();
        var dateParts = endDate.split("/");
        var dateObjectFinal = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);  

                         
        if (today >= dateObjectFinal) {
           $("#statusProyecto").val('Terminado');
           $('#statusProyecto').prop('disabled', 'disabled');
            
        }
        

        if (today < dateObjectFinal) {
           $("#statusProyecto").val('Iniciado');
           $('#statusProyecto').prop('disabled', 'disabled');
            
        }

    
}

        




    </script>

</body>

</html>
