        <div id="modalMaterial" class="modal fade modal-material" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <div id="subTitle2"><h4 class="modal-title">Distribuir Materiales de Difusion</h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form    id="form_distribucion"  name="form_distribucion" class="form-horizontal"   >


                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Nombre del Material:</label>
                                           <div class="col-sm-6">
                                     <input type="text" class="col-md-10 material_nombre" id="nombre_material2">
                                    </div>
                                </div>

                                  <div class="form-group">
                                    <label class="col-sm-4 control-label">Disponible en Inventario:</label>
                                           <div class="col-sm-3">
                                     <input type="text" class="col-md-8 material_nombre" disabled id="cantidad_inventario">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Accion de Conexión:</label>
                                    <div class="col-md-6">
                                          <select  id="accionConexion" name="accionConexion"  style="text-overflow:ellipsis">                                                                                                              
                                                            <?php 
                                                           
                                                            if(count($listComites)>0){ 
                                                            echo "<optgroup label='Comités'>";

                                                                   echo "<option  value=0>Seleccione...</option>";
     
                                                                foreach($listComites as $row){
                                                                    $selected = "";


                                                                  /*  if($row['id'] == $editData['accion_conexion']){
                                                                        $selected = "selected";

                                                                    }*/

                                                                    $title = $row["nombre_comite"];
                                                                    $subtitle = substr($title, 0,112);

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."...</option>";
                                                                }

                                                                echo "</optgroup>";
                                                            }
                                                            ?>
 
                                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Cantidad a Distribuir:</label>
                                           <div class="col-sm-3">
                                     <input type="text" class="col-md-8" id="cantidad_distribuir" name="cantidad_distribuir">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Fecha de Distribución:</label>
                                     <div class="col-sm-4">
                                            <input type="text"  class="form-control" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-#\s]+$/g,'');" style="text-align:right" id="fecha_distribucion" name="fecha_distribucion"   >
                                    </div>
                                </div>
                                
                             <!--   <input type="hidden" name="complexSelect" id="complexSelect">
                                <input type="hidden" name="areaSelect" id="areaSelect">
                                <input type="hidden" name="locationSelect" id="locationSelect">
                                <input type="hidden" name="capacitorSelect" id="capacitorSelect">
                                <input type="hidden" name="typeSelect" id="typeSelect">
                                <input type="hidden" name="categorySelect" id="categorySelect">
                                <input type="hidden" name="modalitySelect" id="modalitySelect">-->
                                



                            <div align="right">
                                <button type="button"  onClick="guardarDistribucion()" class="btn btn-success waves-effect waves-light">Guardar</button>&nbsp;&nbsp;&nbsp;
                            </div>

                            <input type="hidden" name="fk_material" id="fk_material">

                        </form>

                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->