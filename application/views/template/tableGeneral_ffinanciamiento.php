  <form id="form_listadoFF" action="registro_ffinanciamiento" method="post">


 <table id="example2" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Departamento</th>
                <th>Programa</th>
                <th>Recurso</th>
                <th>Ejercicio Fiscal</th>
                <th>Presup.Autor. PEF</th>
                <th>Presup. Vigilar</th>
                <?php
                if($recurso==1){
                ?>
            
                  <th>Mujeres Beneficiadas</th>
                  <th>Hombres Beneficiados</th>
                <?php
                }
                ?>
                <th>Total Población Beneficiada</th>
                <th>Convenio Suscrito</th>
                <th>Total Federal</th>
                <th>Total Estatal</th>
                <th>Total Municipal</th>
                <th>Total Otros</th>
                <th>Total Recurso Asignado</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody> 

            
            <?php
            foreach ($listFFinanciamiento as $row){


              $hombresYmujeres = "";
              if($recurso==1){

                $hombresYmujeres = "<td>" . number_format($row["mujeres_beneficiadas"]) ."</td> <td>" . number_format($row["hombres_beneficiados"]) ."</td>";
              }

              $band_recursto = "Estatal";
              if($recurso == 1){
                $band_recursto = "Federal";
              }

                echo "<tr id='tr_".$row["id"]."'><td>". $row["id"] . "</td><td>". $row["departamento"] . "</td><td>". $row["programa"] . "</td><td>". $band_recursto . "</td><td>". $row["ejercicio_fiscal"] . "</td><td>$". number_format($row["presupuesto_pef"]) . "</td><td>$" .  number_format($row["presupuesto_vigilar_cf"]) . "</td>".$hombresYmujeres."<td>" . number_format($row["total_beneficiados"]) ."</td><td>" . $row["convenio_suscrito"] ."</td><td>" . number_format($row["resumen_total_federal"]) ."</td><td>" . number_format($row["resumen_total_estatal"]) ."</td><td>" . number_format($row["resumen_total_municipal"]) ."</td><td>" . number_format($row["resumen_total_otros"]) ."</td><td>" . number_format($row["resumen_total_asignado"]) ."</td>";

                ?>

                <td>
       


             <button type="button" class="btn btn-xs btn-warning waves-effect w-md waves-light m-b-15"  onClick='viewFF(<?=$row['id']?>)' data-toggle="modal" title="Ver" data-target=". "><i class="fa fa-file"></i></button>
                      
              <button type="button" class="btn btn-xs btn-primary waves-effect w-md waves-light m-b-15"  onClick='editFF(<?=$row['id']?>)' data-toggle="modal" title="Editar" data-target=". "><i class="fa fa-pencil"></i></button>  
              
               <button type="button" class="btn btn-xs btn-danger waves-effect w-md waves-light m-b-15"  onClick='delFF(<?=$row['id']?>)' data-toggle="modal" title="Borrar" data-target=". "><i class="fa fa-trash"></i></button></td></tr>


          </td></tr>

              <?php
                
            }
            ?>


            
         
        </tbody>
      
    </table>

    <input type="hidden" name="id_ffinanciamiento" id="id_ffinanciamiento">
    <input type="hidden" name="method" id="method">
 


</form>


 <script type="text/javascript">

        $(document).ready(function () {

       
     
 

     var tableProy = $('#example2').DataTable( {
       language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
        dom: 'Bfrtip',
        scrollX: 'true',
        pagingType: 'full_numbers',
        bAutoWidth: false,
    
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        bInfo: false,
        buttons: [
            
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                  exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14]
                    }
            },
            
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                  exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14]
                    }
            }
        ],

    } );



     
     
  });    

        function viewFF(id_ffinanciamiento)
        {

            $("#id_ffinanciamiento").val(id_ffinanciamiento);
            $("#method").val("view");
           
            $("#form_listadoFF").submit();



        }


        function editFF(id_ffinanciamiento)
        {

            $("#id_ffinanciamiento").val(id_ffinanciamiento);
            $("#method").val("edit");
           
            $("#form_listadoFF").submit();



        }


        function delFF(id_ffinanciamiento, fk_departamento)
        {

         $("#id_ffinanciamiento").val(id_ffinanciamiento);
         $("#fk_departamento").val(fk_departamento);

         swal({
                              title: "Estás seguro de querer borrar la Fuente de Financiamiento?",
                              text: "",
                              icon: "warning",
                              buttons: true,
                              dangerMode: false,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                                swal("Listo!, Se ha borrado exitosamente", {
                                  icon: "success",
                                });
                                eliminarFFinanciamiento(id_ffinanciamiento, fk_departamento);

                              } else {
                                swal("Lo dejamos como estaba..");
                              }
                            });
        }

 


    function eliminarFFinanciamiento(id_ffinanciamiento, fk_departamento){

         

            $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/eliminarFFinanciamiento') ?>",
                    type: "POST",
                    data: $('#form_listadoFF').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });


                       if(message=="ok"){
                         $("#tr_"+id_ffinanciamiento).hide('slow');
                       }

                       
 
                    }
                });
    }





    </script>