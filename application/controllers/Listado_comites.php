<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listado_comites extends CI_Controller {

	 /**
   * Developed by Constantino Kusulas Vazquez   - 2018.
   */


   	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');


       /* $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }*/
    }




	public function index()
	{  
        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }

        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);

        


        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor
          
        );


        $this->load->view('listado_comites', $data);
	}




	

	   public function getTablaGeneralComites($program = ""){

     // sleep(0.7);
          
        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        

       
        if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');
          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }


        $listComites = $this->ffinanciamiento_model->getListComites($fk_programa);
       
        $data = array();

        unset($data);
 
        $data = array(
            
         
            'listComites' => $listComites,           
            'success' => true,       
            'status' => true,       
          
          
        );
 
         
          $this->load->view('template/tableGeneral_comites', $data);
       
    }



 



}
