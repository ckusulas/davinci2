l<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_seguimiento extends CI_Controller {



	public function __construct() {
        parent::__construct();
        $this->load->model('informes_model');
         $this->load->model('ffinanciamiento_model');
        $this->load->library('session');

    }
 
	public function index()
	{	

        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      } 


        $metodo = $this->input->post('method');
        $id_seguimiento = $this->input->post('id_seguimiento');
        $programaSelected = $this->input->post('seleccPgma');

         $id_departamento = ""; //$this->input->post('departamento');

        if($programaSelected == "") {
            $programaSelected = $this->session->userdata('ProgramasSelecc');
        }

        //VERIFICANDO EXISTENCIA SEGUIMIENTO
        /*if($programaSelected!="todos"){
            $this->db->where('fk_program', $programaSelected);
        }*/
       
        //echo "id_seguimiento:" . $id_seguimiento;

        $this->db->where('id', 6);       
        $q = $this->db->get('cs_registro_seguimiento');

       // echo $this->db->last_query();


        $onlyView = "";
        if($metodo == "view"){
            $onlyView = 1;
        }

        if($metodo == "edit"){
            $onlyView = 2;
        }
      

		$listInformes = $this->informes_model->getListInformes();

        $listComites = $this->ffinanciamiento_model->getListComites($programaSelected);



         $editData = array (
            'id' => '',  
            'fk_comite' => '',  
            'metodo' => '',
              
            'recibio_capacitacion' => '',  
            'conoce_obra'  => '',
            'conoce_obra_negativo' => '',
            'conoce_info_obra' => '',
            'info_obra_tipo' => '',
            'conoce_estado_beneficio' => '',
            'conoce_estado_beneficio_motivo' => '',
            'conoce_costo_obra' => '',
            'conoce_dependencia' => '',
            'conoce_programa' => '',
            'servidor_beneficia_benef' => '',
            'servidor_beneficia_benef_motivo' => '',
            'conoce_periodo_cumplen' => '',
            'conoce_periodo_cumplen_motivo' => '',
            'conoce_info_meta' => '',
            'list_actividades_contraloria' => '',

            'solicitud_informacion_verbal' => '',
            'solicitud_informacion_escrita' => '',
            'solicitud_informacion_servidor_pub' => '',
            'solicitud_informacion_fecha' => '',
            'solicitud_informacion_atendida' => '',

            'sugerencia_informacion_verbal' => '',
            'sugerencia_informacion_escrita' => '',
            'sugerencia_informacion_servidor_pub' => '',
            'sugerencia_informacion_fecha' => '',
            'sugerencia_informacion_atendida' => '',

            'list_informacion_recibio' => '',

            'obra_acorde_plan' => '',
            'obra_acorde_plan_motivo' => '',

            'satisfacion_beneficiarios' => '',
            'satisfacion_beneficiarios_motivo' => '',
            'comentario_mejora_cs' => '',
            'comentario_mejora_cs_auditor' => '',
            'promocion_recabada' => '',
             
            'fk_program' => '',
            'fk_subprogram' => '',
            'fk_departamento' => '',
            'updated' => '',
            'eliminado' => '',
             

              


 

         

            );


           if ($q->num_rows() > 0) {

            $metodo = "edit";

            foreach ($q->result() as $row) {

                 $list_localidades = array();

            /*    if($row->fk_municipio!=""){
                    $list_localidades = $this->ffinanciamiento_model->findLocalidades($row->fk_municipio);
                }*/


                $editData = array (                   
                    'id' => $row->id,
                  
                    'fk_comite' => $row->fk_comite,
                    'metodo' => $metodo,
                      
                    'recibio_capacitacion' => $row->recibio_capacitacion,
                    'conoce_obra'  => $row->conoce_obra,
                    'conoce_obra_negativo' => $row->conoce_obra_negativo,
                    'conoce_info_obra' => $row->conoce_info_obra,
                    'info_obra_tipo' => $row->info_obra_tipo,
                    'conoce_estado_beneficio' => $row->conoce_estado_beneficio,
                    'conoce_estado_beneficio_motivo' => $row->conoce_estado_beneficio_motivo,
                    'conoce_costo_obra' => $row->conoce_costo_obra,
                    'conoce_dependencia' => $row->conoce_dependencia,
                    'conoce_programa' =>  $row->conoce_programa,
                    'servidor_beneficia_benef' => $row->servidor_beneficia_benef,
                    'servidor_beneficia_benef_motivo' =>  $row->servidor_beneficia_benef_motivo,
                    'conoce_periodo_cumplen' =>  $row->conoce_periodo_cumplen,
                    'conoce_periodo_cumplen_motivo' =>  $row->conoce_periodo_cumplen_motivo,
                    'conoce_info_meta' => $row->conoce_info_meta,
                    'list_actividades_contraloria' => $row->list_actividades_contraloria,

                    'solicitud_informacion_verbal' => $row->solicitud_informacion_verbal,
                    'solicitud_informacion_escrita' => $row->solicitud_informacion_escrita,
                    'solicitud_informacion_servidor_pub' => $row->solicitud_informacion_servidor_pub,
                    'solicitud_informacion_fecha' =>  $row->solicitud_informacion_fecha,
                    'solicitud_informacion_atendida' =>  $row->solicitud_informacion_atendida,

                    'sugerencia_informacion_verbal' => $row->sugerencia_informacion_verbal,
                    'sugerencia_informacion_escrita' => $row->sugerencia_informacion_escrita,
                    'sugerencia_informacion_servidor_pub' => $row->sugerencia_informacion_servidor_pub,
                    'sugerencia_informacion_fecha' => $row->sugerencia_informacion_fecha,
                    'sugerencia_informacion_atendida' => $row->sugerencia_informacion_atendida,

                    'list_informacion_recibio' => $row->list_informacion_recibio,

                    'obra_acorde_plan' => $row->obra_acorde_plan,
                    'obra_acorde_plan_motivo' => $row->obra_acorde_plan_motivo,

                    'satisfacion_beneficiarios' =>  $row->satisfacion_beneficiarios,
                    'satisfacion_beneficiarios_motivo' => $row->satisfacion_beneficiarios_motivo,
                    'comentario_mejora_cs' => $row->comentario_mejora_cs,
                    'comentario_mejora_cs_auditor' =>  $row->comentario_mejora_cs_auditor,
                    'promocion_recabada' => $row->promocion_recabada,
                     
                    'fk_program' => '',
                    'fk_subprogram' => '',
                    'fk_departamento' => '',
                    'updated' => '',
                    'eliminado' => '',
 
      

                );

            

            }
        }












        
        // "informes:" . print_r($listInformes);
        $data = array(     
          
            'listInformes' => $listInformes,
            'listComites' => $listComites,
            'editData' => $editData

        );

       // print_r($editData);

		$this->load->view('registro_seguimiento',$data);
	}

    


    public function changeFormatDateSpEng($data){

        $arrayDate = explode("/", $data);
        $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
        return($newData);
    }
 


    function guardarSeguimiento(){


         $accion_conexion = $this->input->post('vinculoComite_hidden')?:NULL;

          $id_seguimiento  = trim($this->input->post('id_seguimiento'));

      //    echo "id_seguimiento:" . $id_seguimiento . "<br>";
         
        $optionRecibioCapacitacion = $this->input->post('optionRecibioCapacitacion');
        $optionConoceObra = $this->input->post('optionConoceObra');
        $textNegativoConoceObra = $this->input->post('textNegativoConoceObra');
        $optionInfoObra = $this->input->post('optionInfoObra');
        $optionInfoObraSITipo = $this->input->post('optionInfoObraSI');
        $optionBeneficio = $this->input->post('optionBeneficio');
        $optionBeneficioMotivo = $this->input->post('textNegativoEstatusBeneficio');
        $optionCostoObra = $this->input->post('optionCostoObra');
        $optionDependencia = $this->input->post('optionDependencia');
        $optionSabePrograma = $this->input->post('optionSabePrograma');
        $optionServidoresBanefic = $this->input->post('optionServidoresBanefic');
        $textServidorPublicoBeneficiariosMejorar = $this->input->post('textServidorPublicoBeneficiariosMejorar');
        $optionPeriodoCumplen = $this->input->post('optionPeriodoCumplen');
        $optionPeriodoCumplenMotivo = $this->input->post('textPeriodoCumplenMotivo');

        $optionInfoMeta = $this->input->post('optionInfoMeta');

        $actividadesContraloria = $this->input->post('actividadesContraloria');

        $solicitudInformacionVerbal = $this->input->post('solicitudInformacionVerbal');
        $solicitudInformacionEscrita = $this->input->post('solicitudInformacionEscrita');
        $servidorPublicoNombreSolicitudInfo = $this->input->post('servidorPublicoNombreSolicitudInfo');
        $fechaSolicitudInformacion = $this->input->post('fechaSolicitudInformacion');
        $solicitudInfoFueAtendida = $this->input->post('solicitudInfoFueAtendida');
        
        $sugerenciaVerbal = $this->input->post('sugerenciaVerbal');
        $sugerenciaEscrita = $this->input->post('sugerenciaEscrita');
        $servidorPublicoSugerencia = $this->input->post('servidorPublicoSugerencia');
        $fechaSugerencia = $this->input->post('fechaSugerencia');
        $sugerenciaInfoFueAtendida = $this->input->post('sugerenciaInfoFueAtendida');

        
        $informacionRecibio = $this->input->post('informacionRecibio');


        $optionObraAcordePlan = $this->input->post('optionObraAcordePlan');
        $optionObraAcordePlanMotivo = $this->input->post('optionObraAcordePlanMotivo');
        $optionSatisfBeneficiarios = $this->input->post('optionSatisfBeneficiarios');
        $txtSatisfBeneficiarios = $this->input->post('txtSatisfBeneficiarios');
        $txtComentarioMejoraCS = $this->input->post('txtComentarioMejoraCS');
        $txtComentarioMejoraCSAuditor = $this->input->post('txtComentarioMejoraCSAuditor');       
        $txtPromocionRecabada = $this->input->post('txtPromocionRecabada');
 


        if(count($actividadesContraloria)>0){
            $actividadesContraloria = implode(",", $actividadesContraloria);
        }else{
            $actividadesContraloria = NULL;
        }


          if(count($informacionRecibio)>0){
            $informacionRecibio = implode(",", $informacionRecibio);
        }else{
            $informacionRecibio = NULL;
        }

   /*     

        echo $optionRecibioCapacitacion . "<br>";
        echo $optionConoceObra  . "<br>";
        echo $textNegativoConoceObra . "<br>";
        echo $optionInfoObra . "<br>";
        echo $optionInfoObraSITipo . "<br>";
        echo $optionCostoObra . "<br>";
        echo $optionSabePrograma . "<br>";
        echo $textServidorPublicoBeneficiariosMejorar . "<br>";
        echo $optionInfoMeta . "<br>";
        echo  "actividades:".$actividadesContraloria . "<br>";
        echo  "solicitudInformacionVerbal:".$solicitudInformacionVerbal . "<br>";
        echo  "solicitudInformacionEscrita:".$solicitudInformacionEscrita . "<br>";
        echo  "informacionRecibio:".$informacionRecibio . "<br>";
        echo  "optionObraAcordePlan:".$optionObraAcordePlan . "<br>";
        echo  "optionObraAcordePlanMotivo:".$optionObraAcordePlanMotivo . "<br>";
        echo  "optionSatisfBeneficiarios:".$optionSatisfBeneficiarios . "<br>";
        echo  "txtSatisfBeneficiarios:".$txtSatisfBeneficiarios . "<br>";
        echo  "txtComentarioMejoraCS:".$txtComentarioMejoraCS . "<br>";
        echo  "txtComentarioMejoraCSAuditor:".$txtComentarioMejoraCSAuditor . "<br>";
        echo  "txtPromocionRecabada:".$txtPromocionRecabada . "<br>";

    */

 
  

        $this->db->set('fk_comite', $accion_conexion);
        $this->db->set('recibio_capacitacion', $optionRecibioCapacitacion);        
        $this->db->set('conoce_obra', $optionConoceObra);
        $this->db->set('conoce_obra_negativo', $textNegativoConoceObra);
        $this->db->set('conoce_info_obra', $optionInfoObra);
        $this->db->set('info_obra_tipo', $optionInfoObraSITipo);
        $this->db->set('conoce_costo_obra', $optionCostoObra);
        $this->db->set('conoce_estado_beneficio', $optionBeneficio);
        $this->db->set('conoce_estado_beneficio_motivo', $optionBeneficioMotivo);
        $this->db->set('conoce_dependencia', $optionDependencia);
        $this->db->set('conoce_programa', $optionSabePrograma);
        $this->db->set('servidor_beneficia_benef', $optionServidoresBanefic);
        $this->db->set('servidor_beneficia_benef_motivo', $textServidorPublicoBeneficiariosMejorar);
        $this->db->set('conoce_periodo_cumplen', $optionPeriodoCumplen);
        $this->db->set('conoce_periodo_cumplen_motivo', $optionPeriodoCumplenMotivo);
        $this->db->set('conoce_info_meta', $optionInfoMeta);


        $this->db->set('list_actividades_contraloria', $actividadesContraloria);


        $this->db->set('solicitud_informacion_verbal', $solicitudInformacionVerbal);
        $this->db->set('solicitud_informacion_escrita', $solicitudInformacionEscrita);
        $this->db->set('solicitud_informacion_servidor_pub', $servidorPublicoNombreSolicitudInfo);
        $this->db->set('solicitud_informacion_fecha', $fechaSolicitudInformacion);
        $this->db->set('solicitud_informacion_atendida', $solicitudInfoFueAtendida);

 
        $this->db->set('sugerencia_informacion_verbal', $sugerenciaVerbal);
        $this->db->set('sugerencia_informacion_escrita', $sugerenciaEscrita);
        $this->db->set('sugerencia_informacion_servidor_pub', $servidorPublicoSugerencia);
        $this->db->set('sugerencia_informacion_fecha', $fechaSugerencia);
        $this->db->set('sugerencia_informacion_atendida', $sugerenciaInfoFueAtendida);

        $this->db->set('list_informacion_recibio', $informacionRecibio);

        $this->db->set('obra_acorde_plan', $optionObraAcordePlan);
        $this->db->set('obra_acorde_plan_motivo', $optionObraAcordePlanMotivo);

        $this->db->set('satisfacion_beneficiarios', $txtSatisfBeneficiarios);
        $this->db->set('satisfacion_beneficiarios_motivo', $txtComentarioMejoraCS);
        $this->db->set('comentario_mejora_cs', $txtComentarioMejoraCS);
        $this->db->set('comentario_mejora_cs_auditor', $txtComentarioMejoraCSAuditor);
        $this->db->set('promocion_recabada', $txtPromocionRecabada);


        $this->db->set('fk_program', 1);
        $this->db->set('fk_subprogram', 1);
        $this->db->set('fk_departamento', 1);
        

      //  echo "id_seguimiento:" . $id_seguimiento . "<br>";


         if($id_seguimiento==""){
            //echo "insert";
    //   echo "insertando";
            $this->db->insert('cs_registro_seguimiento');
            $lastId = $this->db->insert_id();
        }
        else{
         //   echo "actualizando";

             $this->db->where('id', $id_seguimiento);      
             $this->db->update('cs_registro_seguimiento');
            $lastId = $id_seguimiento;
        }

      

           $data = array(        
             'registro_informe' => $lastId,                               
          );
 

        $this->session->set_userdata($data);

   
                                                
/*
        echo "nombreComite:" . $nombreComite . "<br>";
        echo "informe:" . $informe . "<br>";
        echo "apartadoInforme:" . $apartadoInforme . "<br>";
        echo "numero:" . $numero . "<br>";
        echo "claveComite:" . $claveComite . "<br>";
        echo "nombreComite:" . $nombreComite . "<br>";
        echo "fechaCaptura:" . $fechaCaptura . "<br>";
        echo "numeroRegistroCCS:" . $numeroRegistroCCS . "<br>";
        echo "nombreProyecto:" . $nombreProyecto . "<br>";
        echo "fechaLlenado:" . $fechaLlenado . "<br>";
        echo "periodoEjecucion:" . $periodoEjecucion . "<br>";
        echo "entidadFederativa:" . $entidadFederativa . "<br>";
        echo "claveMunicipio:" . $claveMunicipio . "<br>";
        echo "claveLocalidad:" . $claveLocalidad . "<br>";*/

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId;
             
        } else {
            $status = false;
            $msg = $lastId;
        }



        

            $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

        


    }




    






                                             



    function guardarCuestionario(){
         
        $cumplenRequisitosBenefic = $this->input->post('cumplenRequisitosBenefic');
        $igualdadHyM = $this->input->post('igualdadHyM');
        $ProgramaEntreOportunam = $this->input->post('ProgramaEntreOportunam');
        $proyectoCumplePrograma = $this->input->post('proyectoCumplePrograma');
        $ProgramaFinPolitico = $this->input->post('ProgramaFinPolitico');
        $recibieronQuejas = $this->input->post('recibieronQuejas');
        $entregaronQuejasAutoridad = $this->input->post('entregaronQuejasAutoridad');
        $recibieronQuejaAutoridadCompetente = $this->input->post('recibieronQuejaAutoridadCompetente');
 
        $lastId_informe = $this->session->userdata('registro_informe');

                                         
                                                 
                                                

        echo "cumplenRequisitosBenefic:" . $cumplenRequisitosBenefic . "<br>";
        echo "igualdadHyM:" . $igualdadHyM . "<br>";
        echo "ProgramaEntreOportunam:" . $ProgramaEntreOportunam . "<br>";
        echo "proyectoCumplePrograma:" . $proyectoCumplePrograma . "<br>";
        echo "ProgramaFinPolitico:" . $ProgramaFinPolitico . "<br>";
        echo "recibieronQuejas:" . $recibieronQuejas . "<br>";
        echo "entregaronQuejasAutoridad:" . $entregaronQuejasAutoridad . "<br>";
        echo "recibieronQuejaAutoridadCompetente:" . $recibieronQuejaAutoridadCompetente . "<br>";
       

        $this->db->set('cumplenRequisitosBenefic', $cumplenRequisitosBenefic);
        $this->db->set('igualdadHyM', $igualdadHyM);        
        $this->db->set('ProgramaEntreOportunam', $ProgramaEntreOportunam);     
        $this->db->set('proyectoCumplePrograma', $proyectoCumplePrograma);     
        $this->db->set('ProgramaFinPolitico', $ProgramaFinPolitico);     
        $this->db->set('recibieronQuejas', $recibieronQuejas);     
        $this->db->set('entregaronQuejasAutoridad', $entregaronQuejasAutoridad);     
        $this->db->set('recibieronQuejaAutoridadCompetente', $recibieronQuejaAutoridadCompetente);     
 
        $this->db->where('id', $lastId_informe);       
        $this->db->update('cs_registro_informe');


              

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId;
             
        } else {
            $status = false;
            $msg = $lastId;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));


    }


    


     function guardarCuestionario2(){
         
        $csInformacionConocen = $this->input->post('csInformacionConocen');
        $csActividades = $this->input->post('csActividades');
        $csUtilidad = $this->input->post('csUtilidad');

        $list_csInformacionConocen = ""; 
        $list_csActividades = "";
        $list_csUtilidad = "";

        $lastId_informe = $this->session->userdata('registro_informe');



        if(!empty($csInformacionConocen)){           

             $list_csInformacionConocen = implode(",", $csInformacionConocen);
            echo "list_csInformacionConocen:" . $list_csInformacionConocen;

        }

        

        if(!empty($csActividades)){
        
            $list_csActividades = implode(",", $csActividades);
            echo "list_csInformacionConocen:" . $list_csInformacionConocen;

            
        }
 

        if(!empty($csUtilidad)){
            $list_csUtilidad = implode(",", $csUtilidad);
         
              echo "list_csUtilidad:" . $list_csUtilidad;
        }


        $this->db->set('csInformacionConocen', $list_csInformacionConocen);     
        $this->db->set('csActividades', $list_csActividades);     
        $this->db->set('csUtilidad', $list_csUtilidad);     
 
        $this->db->where('id', $lastId_informe);       
        $this->db->update('cs_registro_informe');
 

         
        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastId_informe;
             
        } else {
            $status = false;
            $msg = $lastId_informe;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

           
   


    }



}
