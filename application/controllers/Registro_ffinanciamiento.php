<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_ffinanciamiento extends CI_Controller {

 


   	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
        $this->load->library('session');
 
    }




	public function index()
	{

        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }

        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        $listFondoEstatal = $this->ffinanciamiento_model->getListFondoEstatal(2018);



        $departamento =  $this->session->userdata('departamento');

        //echo "departamento:" . $departamento;

        $metodo = $this->input->post('method');
        $id_ffinanciamiento = $this->input->post('id_ffinanciamiento');



        $programSelected = $this->input->post('seleccPgma');
        if($programSelected == "") {
            $programSelected =  $this->session->userdata('ProgramasSelecc');
        }
        
        $subProgramSelected =  $this->session->userdata('SubProgramasSelecc');

       // echo "programa:" . $programSelected;

       

        //VERIFICANDO EXISTENCIA PROGRAMA
        if($programSelected!="todos"){
            $this->db->where('fk_programa_proyectos', $programSelected);
        }
        //$this->db->where('fk_suprograma', $subProgramSelected);    
       // echo "fuenteFF:" . $id_ffinanciamiento;

        if($id_ffinanciamiento!="") {
            $this->db->where('id', $id_ffinanciamiento);  
        }   else{ 

            $this->db->where('fk_departamento', $departamento);  
            
        }
       $this->db->where('eliminado',NULL); 

        $q = $this->db->get('cs_registro_programa_recursos');


       $onlyView = "";
        if($metodo == "view"){
            $onlyView = 1;
        }

         $editData = array(

                        'id_ffinanciamiento' => '',
                        'ejercicio_fiscal' => '',
                        'presupuesto_pef' => '',
                        'presupuesto_vigilar_cs' => '',
                        'descripcion_poblacion_objetivo' =>  '',
                        'mujeres_beneficiadas' => '',
                        'hombres_beneficiados' => '',
                        'total_beneficiados' =>  '',


                        //FEDERAL

                        'fechaFederal_1' =>  '',
                        'anioRecurso_1' =>  '',
                        'montoFuente_1' =>  '',
                        'fuenteFinanciamiento_1' =>  '',

                        'fechaFederal_2' => '',
                        'anioRecurso_2' =>  '',
                        'montoFuente_2' =>  '',
                        'fuenteFinanciamiento_2' => '',

                        'fechaFederal_3' =>  '',
                        'anioRecurso_3' =>  '',
                        'montoFuente_3' =>  '',
                        'fuenteFinanciamiento_3' =>  '',


                        //ESTATAL
                        'fechaEstatal_1' => '',
                        'perteneceAnioRecursoEstatal1' => '',
                        'fuenteEstatal_1' => '',
                        'montoEstatal_1' => '',

                        'fechaEstatal_2' =>  '',
                        'perteneceAnioRecursoEstatal2' => '',
                        'fuenteEstatal_2' => '',
                        'montoEstatal_2' => '',
                        'fechaEstatal_3' =>  '',
                        'perteneceAnioRecursoEstatal3' => '',
                        'fuenteEstatal_3' => '',
                        'montoEstatal_3' => '',

                        //Municipal
                        'fechaMunicipal_1' =>  '',
                        'perteneceAnioRecursoMunicipal1' => '',
                        'fuenteMunicipal_1' => '',
                        'montoMunicipal_1' => '',

                        'fechaMunicipal_2' =>  '',
                        'perteneceAnioRecursoMunicipal2' => '',
                        'fuenteMunicipal_2' => '',
                        'montoMunicipal_2' => '',

                        'fechaMunicipal_3' =>  '',
                        'perteneceAnioRecursoMunicipal3' => '',
                        'fuenteMunicipal_3' => '',
                        'montoMunicipal_3' => '',


                        //Otros
                        'fechaOtros_1' =>  '',
                        'perteneceAnioRecursoOtros1' => '',
                        'fuenteOtros_1' => '',
                        'montoOtros_1' => '',

                        'fechaOtros_2' =>  '',
                        'perteneceAnioRecursoOtros2' => '',
                        'fuenteOtros_2' => '',
                        'montoOtros_2' => '',

                        'fechaOtros_3' =>  '',
                        'perteneceAnioRecursoOtros3' => '',
                        'fuenteOtros_3' => '',
                        'montoOtros_3' => '',






                        'convenio_suscrito' =>  '',
                        'instancia_primaria' =>  '',
                        'instancia_secundaria' =>  '',

                        'ramo' =>  '',
                        'nombre_ramo' =>  '',


                        'resumen_total_federal' => '',
                        'resumen_total_estatal' =>  '',
                        'resumen_total_municipal' =>  '',
                        'resumen_total_otros' =>  '',
                        'resumen_total_asignado' =>  '',
                        'onlyView' => $onlyView,
                        'metodo' => 'NEW',
                         'url_convenio' => '',






                      
                      );
                    

         if ($q->num_rows() > 0) {

            if(strtoupper($metodo)=="EDIT"){
                $onlyView = 2;
            }

               foreach ($q->result() as $row) {
                    $lastId = $row->id;



                        //Monto Federal
                        $montoF1 = $row->federal_recurso_monto_asignado_recurso1;
                        if($montoF1 == 0){
                            $montoF1 = "";

                        }

                          $montoF2 = $row->federal_recurso_monto_asignado_recurso2;
                        if($montoF2== 0){
                            $montoF2 = "";

                        }

                          $montoF3 = $row->federal_recurso_monto_asignado_recurso3;
                        if($montoF3 == 0){
                            $montoF3 = "";

                        }



                        //Monto Estatal
                        $montoE1 = $row->estatal_recurso_monto_asignado_recurso1;
                        if($montoE1 == 0){
                            $montoE1 = "";

                        }

                        $montoE2 = $row->estatal_recurso_monto_asignado_recurso2;
                        if($montoE2== 0){
                            $montoE2 = "";

                        }

                        $montoE3 = $row->estatal_recurso_monto_asignado_recurso3;
                        if($montoE3 == 0){
                            $montoE3 = "";

                        }


                        //Monto Municipal
                        $montoM1 = $row->municipal_recurso_monto_asignado_recurso1;
                        if(intval($montoM1) == 0){
                            $montoM1 = "";

                        }

                        $montoM2 = $row->municipal_recurso_monto_asignado_recurso2;
                        if(intval($montoM2) == 0){
                            $montoM2 = "";

                        }

                        $montoM3 = $row->municipal_recurso_monto_asignado_recurso3;
                        if(intval($montoM3) == 0){
                            $montoM3 = "";

                        }


                        //Monto Otros
                        $montoO1 = $row->otros_recurso_monto_asignado_recurso1;
                        if(intval($montoO1) == 0){
                            $montoO1 = "";

                        }

                        $montoO2 = $row->otros_recurso_monto_asignado_recurso2;
                        if(intval($montoO2) == 0){
                            $montoO2 = "";

                        }

                        $montoO3 = $row->otros_recurso_monto_asignado_recurso3;
                        if(intval($montoO3) == 0){
                            $montoO3 = "";

                        }




                      $editData = array(
                        'id_ffinanciamiento' => $lastId,
                        'ejercicio_fiscal' => $row->ejercicio_fiscal,
                        'presupuesto_pef' =>  number_format($row->presupuesto_pef,2),
                        'presupuesto_vigilar_cs' =>  number_format($row->presupuesto_vigilar_cf,2),
                        'descripcion_poblacion_objetivo' =>  $row->descripcion_poblacion_objetivo,
                        'mujeres_beneficiadas' =>  number_format($row->mujeres_beneficiadas),
                        'hombres_beneficiados' =>   number_format($row->hombres_beneficiados),
                        'total_beneficiados' =>   number_format($row->total_beneficiados),


                        //FEDERAL

                        'fechaFederal_1' =>  $this->transformDateToView($row->federal_recurso_fecha_asignacion_recurso1),
                        'anioRecurso_1' =>  $row->federal_recurso_anio_recurso1,
                        'montoFuente_1' =>  $montoF1,
                        'fuenteFinanciamiento_1' =>  $row->federal_recurso_fuente_financiamiento_recurso1,

                        'fechaFederal_2' => $this->transformDateToView($row->federal_recurso_fecha_asignacion_recurso2),
                        'anioRecurso_2' =>  $row->federal_recurso_anio_recurso2,
                        'montoFuente_2' =>  $montoF2,
                        'fuenteFinanciamiento_2' =>  $row->federal_recurso_fuente_financiamiento_recurso2,

                        'fechaFederal_3' =>  $this->transformDateToView($row->federal_recurso_fecha_asignacion_recurso3),
                        'anioRecurso_3' =>  $row->federal_recurso_anio_recurso3,
                        'montoFuente_3' =>  $montoF3,
                        'fuenteFinanciamiento_3' =>  $row->federal_recurso_fuente_financiamiento_recurso3,


                        //ESTATAL
                        'fechaEstatal_1' =>  $this->transformDateToView($row->estatal_recurso_fecha_asignacion_recurso1),
                        'perteneceAnioRecursoEstatal1' => $row->estatal_recurso_anio_recurso1,
                        'fuenteEstatal_1' => $row->estatal_recurso_fuente_financiamiento_recurso1,
                        'montoEstatal_1' => $montoE1,

                        'fechaEstatal_2' =>  $this->transformDateToView($row->estatal_recurso_fecha_asignacion_recurso2),
                        'perteneceAnioRecursoEstatal2' => $row->estatal_recurso_anio_recurso2,
                        'fuenteEstatal_2' => $row->estatal_recurso_fuente_financiamiento_recurso2,
                        'montoEstatal_2' => $montoE2,

                        'fechaEstatal_3' =>  $this->transformDateToView($row->estatal_recurso_fecha_asignacion_recurso3),
                        'perteneceAnioRecursoEstatal3' => $row->estatal_recurso_anio_recurso3,
                        'fuenteEstatal_3' => $row->estatal_recurso_fuente_financiamiento_recurso3,
                        'montoEstatal_3' => $montoE3,


                        //Municipal
                        'fechaMunicipal_1' =>  $this->transformDateToView($row->municipal_recurso_fecha_asignacion_recurso1),
                        'perteneceAnioRecursoMunicipal1' => $row->municipal_recurso_anio_recurso1,
                        'fuenteMunicipal_1' => $row->municipal_recurso_fuente_financiamiento_recurso1,
                        'montoMunicipal_1' => $montoM1,

                        'fechaMunicipal_2' =>  $this->transformDateToView($row->municipal_recurso_fecha_asignacion_recurso2),
                        'perteneceAnioRecursoMunicipal2' => $row->municipal_recurso_anio_recurso2,
                        'fuenteMunicipal_2' => $row->municipal_recurso_fuente_financiamiento_recurso2,
                        'montoMunicipal_2' => $montoM2,

                        'fechaMunicipal_3' =>  $this->transformDateToView($row->municipal_recurso_fecha_asignacion_recurso3),
                        'perteneceAnioRecursoMunicipal3' => $row->municipal_recurso_anio_recurso3,
                        'fuenteMunicipal_3' => $row->municipal_recurso_fuente_financiamiento_recurso3,
                        'montoMunicipal_3' => $montoM3,


                        //Otros
                        'fechaOtros_1' =>  $this->transformDateToView($row->otros_recurso_fecha_asignacion_recurso1),
                        'perteneceAnioRecursoOtros1' => $row->otros_recurso_anio_recurso1,
                        'fuenteOtros_1' => $row->otros_recurso_fuente_financiamiento_recurso1,
                        'montoOtros_1' => $montoO1,

                        'fechaOtros_2' =>  $this->transformDateToView($row->otros_recurso_fecha_asignacion_recurso2),
                        'perteneceAnioRecursoOtros2' => $row->otros_recurso_anio_recurso2,
                        'fuenteOtros_2' => $row->otros_recurso_fuente_financiamiento_recurso2,
                        'montoOtros_2' => $montoO2,

                        'fechaOtros_3' =>  $this->transformDateToView($row->otros_recurso_fecha_asignacion_recurso3),
                        'perteneceAnioRecursoOtros3' => $row->otros_recurso_anio_recurso3,
                        'fuenteOtros_3' => $row->otros_recurso_fuente_financiamiento_recurso3,
                        'montoOtros_3' => $montoO3,






                        'convenio_suscrito' =>  $row->convenio_suscrito,
                        'instancia_primaria' =>  $row->instancia_primaria,
                        'instancia_secundaria' =>  $row->instancia_secundaria,

                        'ramo' =>  $row->ramo,
                        'nombre_ramo' =>  $row->nombre_ramo,


                        'resumen_total_federal' =>  $row->resumen_total_federal,
                        'resumen_total_estatal' =>  $row->resumen_total_estatal,
                        'resumen_total_municipal' =>  $row->resumen_total_municipal,
                        'resumen_total_otros' =>  $row->resumen_total_otros,
                        'resumen_total_asignado' =>  $row->resumen_total_asignado,
                        'onlyView' => $onlyView,
                        'metodo' => 'EDIT',

                        'url_convenio' =>  $this->ffinanciamiento_model->getURL_adjunto($programSelected, $departamento, "ffinanciamiento", "convenio"),


 
                      );

 



  
                    
 
                }//foreach
        }//if num rows



       // echo print_r($editData);
        

        $listRamo = $this->ffinanciamiento_model->getListRamos();

        
       // echo print_r($listRamo);

       // echo "nombreDEpto:" . $this->session->userdata('name');

        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor,
            'listFondoEstatal' => $listFondoEstatal,
            'editData' => $editData,
            'listRamo' => $listRamo,
            'fk_programa' => $programSelected,
            'departamento' => $this->session->userdata('name'),
            'fk_departamento' => $departamento,
          
        );


        $this->load->view('registro_ffinanciamiento', $data);
	}


  //  public function ajax_getDataSumaRecursos($fk_ffinanciamiento)
    //select sum(monto_recurso_asignado_vig_federal) as sum_monto_vig_federal, sum(monto_recurso_asignado_vig_estatal) as sum_monto_vig_estatal, sum(monto_recurso_asignado_vig_municipal) as sum_monto_vig_municipal, sum(monto_recurso_asignado_vig_otros) as sum_monto_vig_otros from cs_registro_proyecto where fk_programa_recurso=86

    public function ajax_getDataFFinanciamiento($fk_ffinanciamiento){

//        $fk_departamento = $this->session->userdata('departamento');

        
       
        //$this->db->where('id', $fk_ffinanciamiento);       
        //$q = $this->db->get('cs_registro_programa_recursos');


       // $listComites = $this->ffinanciamiento_model->getListComites($fk_programa);
       
        //$datosFFinanciamiento = $q->result();

        $datosFFinanciamiento = $this->ffinanciamiento_model->getDataFFinanciamiento($fk_ffinanciamiento);
        

        echo json_encode($datosFFinanciamiento);




   }







    public function eliminarFFinanciamiento(){

        $id_ffinanciamiento = $this->input->post('id_ffinanciamiento');
        $departamento = $this->input->post('fk_departamento');
 

       


        //Aplicando el borrado fisico al Proyecto         
             // $this->db->set('eliminado', 1); 
              $this->db->where('id', $id_ffinanciamiento);      
              $this->db->delete('cs_registro_programa_recursos');
       

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "ok";
        } 

        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }





    public function populate_programs($findResource) {
        
        $listPrograms = $this->ffinanciamiento_model->getListPrograms($findResource);
        echo json_encode($listPrograms);
    }




    public function populate_subprograms($findProgram) {

        $listSuprograms = $this->ffinanciamiento_model->getListSubprograms($findProgram);
        echo json_encode($listSuprograms);

    }



    public function changeFormatDateSpEng($data){

        if(strlen($data)>6){
            $arrayDate = explode("/", $data);
            $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
            return($newData);
        }  
    }

    public function transformDateToView($data){

        if($data == "0000-00-00"){
            return "";
        }



        $arrayDate = explode("-", $data);
        if(strlen($data)>6){
            $newData = $arrayDate[2] . "/" . $arrayDate[1] . "/"  . $arrayDate[0];
            return($newData);
        }
    }


    public function guardarFFinanciamiento(){


        //id_ffinanciamiento_recursoFederal

        //echo print_r($_POST);
        
        $programSelected =  $this->session->userdata('ProgramasSelecc');
        $subProgramSelected =  $this->session->userdata('SubProgramasSelecc');
        $departamento =  $this->session->userdata('departamento');

        $catalogResources = $this->input->post('resourceSelected');
        $exercirseFiscal = $this->input->post('ejercicio_fiscal');
        $presupuestoPEF = $this->input->post('presupuesto_pef');
        $presupuestoVigilarCS = $this->input->post('presupuesto_vigilar_cs');
        $descripcionPoblacion = $this->input->post('descripcionPoblacion');

        $metodo = $this->input->post('metodo');


        $presupuestoPEF = floatval(str_replace(',', '', $presupuestoPEF));
        $presupuestoVigilarCS = floatval(str_replace(',', '', $presupuestoVigilarCS));
 

        //VERIFICANDO EXISTENCIA PROGRAMA
        $this->db->where('fk_programa_proyectos', $programSelected);
        $this->db->where('fk_departamento', $departamento);       
        //$this->db->where('eliminado', NULL);       
        $this->db->order_by("id", "desc");
        $this->db->limit(1);
       
        $q = $this->db->get('cs_registro_programa_recursos');

    

        $this->db->set('fk_resource', $catalogResources);
        $this->db->set('fk_programa_proyectos', $programSelected);
        $this->db->set('fk_suprograma', $subProgramSelected);
        $this->db->set('ejercicio_fiscal', $exercirseFiscal);
        $this->db->set('presupuesto_pef', $presupuestoPEF);
        $this->db->set('presupuesto_vigilar_cf', $presupuestoVigilarCS);
        $this->db->set('descripcion_poblacion_objetivo', $descripcionPoblacion);
        $this->db->set('fk_departamento', $departamento);

 
       

        //$this->db->insert("cs_registro_programa_recursos");
        if($metodo=="NEW"){

           // echo "1";
            $this->db->set('eliminado', 1);
        }else{
           // echo "NULL";
               $this->db->set('eliminado', NULL);
        }


       // echo "methodo:" . $metodo;
         



         if ($q->num_rows() > 0) {

               foreach ($q->result() as $row) {
                    $lastId = $row->id;
                   // echo "lastId:" .$lastId . "<br>";


                }

                

                //UPDATE FF
                $this->db->where('id', $lastId);
                $this->db->where('fk_programa_proyectos', $programSelected);
                $this->db->where('fk_departamento', $departamento);
                $this->db->update('cs_registro_programa_recursos');

            
            } else {
             
      

                //INSERT FF
                $this->db->insert('cs_registro_programa_recursos');
                $lastId = $this->db->insert_id();

                 //echo "lastId:" .$lastId . "<br>";

                
            }
 


 
         $data = array(
        
                'ProgramasSelecc' => $programSelected,
                'SubProgramasSelecc' => $subProgramSelected,
                'registro_p' => $lastId
                
          );


        $this->session->set_userdata($data);

          


        $status = true;
        $msg = $lastId;
             
      



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


       
        echo json_encode($datos);
        
 
    }



     public function guardarBeneficios(){

 
        $beneficiarioWomans = $this->input->post('total_people_womans');
        $beneficiarioMans = $this->input->post('total_people_mans');
        $totalBeneficios = $this->input->post('totalBeneficios');
        $lastID =  $this->input->post('id_ffinanciamiento_beneficios');
  
        // echo print_r($_POST);
        $beneficiarioWomans = str_replace(',','',$beneficiarioWomans);  
        $beneficiarioMans = str_replace(',','',$beneficiarioMans);  
        $totalBeneficios = str_replace(',','',$totalBeneficios);  

       

        if($beneficiarioWomans==""){
            $beneficiarioWomans = 0;
        }

        if($beneficiarioMans==""){
            $beneficiarioMans = 0;
        }

        if($totalBeneficios==""){
            $totalBeneficios = 0;
        }




        $this->db->set('mujeres_beneficiadas', $beneficiarioWomans);
        $this->db->set('hombres_beneficiados', $beneficiarioMans);
        $this->db->set('total_beneficiados', $totalBeneficios);   
          $this->db->set('eliminado', NULL);  

        $this->db->where('id', $lastID);
        $this->db->update("cs_registro_programa_recursos");
        


        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = $lastID;
        } else {
            $status = false;
            $msg = $lastID;
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

    }



     public function guardarConvenio(){

 
        $catalogConvenioSelected = $this->input->post('catalogConvenio');
         $this->db->set('convenio_suscrito', $catalogConvenioSelected); 

         // echo print_r($_POST);

        $instanciaPromotoraSelected = $this->input->post('instanciaPromotoraSelected')?:NULL;
            $this->db->set('instancia_primaria', $instanciaPromotoraSelected); 

         $instanciaAbajoSelected = $this->input->post('instanciaAbajoSelected')?:NULL;
         $this->db->set('instancia_secundaria', $instanciaAbajoSelected); 
     

     
  
        $lastID =  $this->input->post('id_ffinanciamiento_convenio');
    
        $entidadFederativa = "Querétaro";

       /* echo "instanciaPrimaria:" . $instanciaPromotoraSelected . "<br>";    
        echo "instanciaSecundaria:" . $instanciaAbajoSelected . "<br>"; */   
 

               
    
          
           
        $this->db->set('entidad_federativa', $entidadFederativa); 
         $this->db->set('eliminado', NULL);      

        $this->db->where('id', $lastID);
        $this->db->update("cs_registro_programa_recursos");


        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

 

 

    }

    


    public function guardarRamo(){

         // echo print_r($_POST);
        
        $lastID =  $this->input->post('id_ffinanciamiento_ramo');
 
        $ramo = $this->input->post('ramo');
        $nombre_ramo = $this->input->post('nombre_ramo');


      //  echo "ramo:" . $ramo;
        
        if($ramo == ""){
            $ramo = NULL;
        }

        if($nombre_ramo == ""){
            $nombre_ramo = NULL;
        }
    
 

        $this->db->set('ramo', $ramo);
        $this->db->set('nombre_ramo', $nombre_ramo);     
         $this->db->set('eliminado', NULL);  
       
        $this->db->where('id', $lastID);
        $this->db->update("cs_registro_programa_recursos");


        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

 

 

    }




     public function  guardarRecursoFederal(){





 
        $fechaFederal_1 = trim($this->input->post('fechaFederal_1'));
        $anioRecurso_1 = $this->input->post('perteneceAnioRecurso1_selected');     
        $fuenteFinanciamiento_1 = $this->input->post('fuenteFinanciamFederal_1');
        $montoFuente_1 = $this->input->post('montoFederal_1');

        if($montoFuente_1==""){
            $montoFuente_1 = NULL;
        }

        if($fechaFederal_1!=""){
            $fechaFederal_1 = $this->changeFormatDateSpEng($fechaFederal_1);
        }else{
            $fechaFederal_1 = NULL;
        }


        $fechaFederal_2 = trim($this->input->post('fechaFederal_2'));
        $anioRecurso_2 = $this->input->post('anioRecurso_2');
        $fuenteFinanciamiento_2 = $this->input->post('fuenteFinanciamFederal_2');
        $montoFuente_2 = $this->input->post('montoFederal_2');

        if($montoFuente_2==""){
            $montoFuente_2 = NULL;
        }

        if($fechaFederal_2!=""){
            $fechaFederal_2 = $this->changeFormatDateSpEng($fechaFederal_2);
        }else{
            $fechaFederal_2 = NULL;
        }



        $fechaFederal_3 = trim($this->input->post('fechaFederal_3'));
        $anioRecurso_3 = $this->input->post('anioRecurso_3');
        $fuenteFinanciamiento_3 = $this->input->post('fuenteFinanciamFederal_3');
        $montoFuente_3 = $this->input->post('montoFederal_3');

        if($fechaFederal_3!=""){
            $fechaFederal_3 = $this->changeFormatDateSpEng($fechaFederal_3);
        }else{
            $fechaFederal_3 = NULL;
        }

        if($montoFuente_3==""){
            $montoFuente_3 = NULL;
        }
        


        $lastID =  $this->input->post('id_ffinanciamiento_recursoFederal');




 


        $this->db->set('federal_recurso_fecha_asignacion_recurso1', $fechaFederal_1);
        $this->db->set('federal_recurso_anio_recurso1', $anioRecurso_1);
        $this->db->set('federal_recurso_monto_asignado_recurso1', $montoFuente_1);
        $this->db->set('federal_recurso_fuente_financiamiento_recurso1', $fuenteFinanciamiento_1);     



        $this->db->set('federal_recurso_fecha_asignacion_recurso2', $fechaFederal_2);
        $this->db->set('federal_recurso_anio_recurso2', $anioRecurso_2);
        $this->db->set('federal_recurso_monto_asignado_recurso2', $montoFuente_2);
        $this->db->set('federal_recurso_fuente_financiamiento_recurso2', $fuenteFinanciamiento_2);    


        $this->db->set('federal_recurso_fecha_asignacion_recurso3', $fechaFederal_3);
        $this->db->set('federal_recurso_anio_recurso3', $anioRecurso_3);
        $this->db->set('federal_recurso_monto_asignado_recurso3', $montoFuente_3);
        $this->db->set('federal_recurso_fuente_financiamiento_recurso3', $fuenteFinanciamiento_3);      
          

        $this->db->where('id', $lastID);
        $this->db->update("cs_registro_programa_recursos");

      //  echo $this->db->last_query();


        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

 
 

    }


    

     public function  guardarResumenAsignado(){


      $totalFederal = $this->input->post('totalFederalRA');
      $totalEstatal = $this->input->post('totalEstatalRA');
      $totalMunicipal = $this->input->post('totalMunicipalRA');
      $totalOtros = $this->input->post('totalOtrosRA');
      $totalRecursos = $this->input->post('totalRecursosRA');

        $totalFederal = str_replace(',','',$totalFederal);  
        $totalEstatal = str_replace(',','',$totalEstatal);  
        $totalMunicipal = str_replace(',','',$totalMunicipal);  
        $totalOtros = str_replace(',','',$totalOtros);  
        $totalRecursos = str_replace(',','',$totalRecursos);  


        if($totalFederal == ""){
            $totalFederal = NULL;
        }

        if($totalEstatal == ""){
            $totalEstatal = NULL;
        }

         if($totalMunicipal == ""){
            $totalMunicipal = NULL;
        }


        if($totalOtros == ""){
            $totalOtros = NULL;
        }

        if($totalRecursos == ""){
            $totalRecursos = NULL;
        }

       

        $lastID =  $this->input->post('id_ffinanciamiento_resumenA');




        $this->db->set('resumen_total_federal', $totalFederal);
        $this->db->set('resumen_total_estatal', $totalEstatal);
        $this->db->set('resumen_total_municipal', $totalMunicipal);
        $this->db->set('resumen_total_otros', $totalOtros);      
        $this->db->set('resumen_total_asignado', $totalRecursos);      
          

        $this->db->where('id', $lastID);
        $this->db->update("cs_registro_programa_recursos");




     }




      public function  guardarRecursoEstatal(){



        $fechaEstatal_1 = trim($this->input->post('fechaEstatal_1'));
        $anioRecurso_1 = $this->input->post('perteneceAnioRecursoEstatal1');
        $fuenteFinanciamiento_1 = $this->input->post('fuenteEstatal_1');
        $montoFuente_1 = $this->input->post('montoEstatal_1');
        if($montoFuente_1 == ""){
            $montoFuente_1 = NULL;
        }
        if($fechaEstatal_1!=""){
            $fechaEstatal_1 = $this->changeFormatDateSpEng($fechaEstatal_1);
        }else{
            $fechaEstatal_1 = NULL;
        }


        $fechaEstatal_2 = trim($this->input->post('fechaEstatal_2'));
        $anioRecurso_2 = $this->input->post('perteneceAnioRecursoEstatal2');
        $fuenteFinanciamiento_2 = $this->input->post('fuenteEstatal_2');
        $montoFuente_2 = $this->input->post('montoEstatal_2');
        if($montoFuente_2 == ""){
            $montoFuente_2 = NULL;
        }
        if($fechaEstatal_2!=""){
            $fechaEstatal_2 = $this->changeFormatDateSpEng($fechaEstatal_2);
        }else{
            $fechaEstatal_2 = NULL;
        }


        $fechaEstatal_3 = trim($this->input->post('fechaEstatal_3'));
        $anioRecurso_3 = $this->input->post('perteneceAnioRecursoEstatal3');
        $fuenteFinanciamiento_3 = $this->input->post('fuenteEstatal_3');
        $montoFuente_3 = $this->input->post('montoEstatal_3');

        if($montoFuente_3 == ""){
            $montoFuente_3 = NULL;
        }
        if($fechaEstatal_3!=""){
            $fechaEstatal_3 = $this->changeFormatDateSpEng($fechaEstatal_3);
        }else{
            $fechaEstatal_3 = NULL;
        }


        $lastID =  $this->input->post('id_ffinanciamiento_recursoEstatal');

/*

        echo "lastId:" . $lastID . "<br>";
        
        echo "fechaEstatal_1:" . $fechaEstatal_1 . "<br>";
        echo "anioRecurso_1:" . $anioRecurso_1 . "<br>";
        echo "fuenteFinanciamiento_1:" . $fuenteFinanciamiento_1 . "<br>";
        echo "montoFuente_1:" . $montoFuente_1 . "<br>";


        echo "fechaEstatal_2:" . $fechaEstatal_2 . "<br>";
        echo "anioRecurso_2:" . $anioRecurso_2 . "<br>";
        echo "fuenteFinanciamiento_1:" . $fuenteFinanciamiento_2 . "<br>";
        echo "montoFuente_1:" . $montoFuente_2 . "<br>";


        echo "fechaEstatal_3:" . $fechaEstatal_3 . "<br>";
        echo "anioRecurso_3:" . $anioRecurso_3 . "<br>";
        echo "fuenteFinanciamiento_3:" . $fuenteFinanciamiento_3 . "<br>";
        echo "montoFuente_3:" . $montoFuente_3 . "<br>"; */
 

        $this->db->set('estatal_recurso_fecha_asignacion_recurso1', $fechaEstatal_1);
        $this->db->set('estatal_recurso_anio_recurso1', $anioRecurso_1);
        $this->db->set('estatal_recurso_monto_asignado_recurso1', $montoFuente_1);
        $this->db->set('estatal_recurso_fuente_financiamiento_recurso1', $fuenteFinanciamiento_1);     



        $this->db->set('estatal_recurso_fecha_asignacion_recurso2', $fechaEstatal_2);
        $this->db->set('estatal_recurso_anio_recurso2', $anioRecurso_2);
        $this->db->set('estatal_recurso_monto_asignado_recurso2', $montoFuente_2);
        $this->db->set('estatal_recurso_fuente_financiamiento_recurso2', $fuenteFinanciamiento_2);    


        $this->db->set('estatal_recurso_fecha_asignacion_recurso3', $fechaEstatal_3);
        $this->db->set('estatal_recurso_anio_recurso3', $anioRecurso_3);
        $this->db->set('estatal_recurso_monto_asignado_recurso3', $montoFuente_3);
        $this->db->set('estatal_recurso_fuente_financiamiento_recurso3', $fuenteFinanciamiento_3);      
          

        $this->db->where('id', $lastID);
        $this->db->update("cs_registro_programa_recursos");



        

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

 
 

    }





     public function  guardarRecursoMunicipal(){

       /// echo print_r($_POST);
 

 
        $fechaMunicipal_1 = trim($this->input->post('fechaMunicipal_1'));
        $anioRecurso_1 = $this->input->post('perteneceAnioRecursoMunicipal1');
        $fuenteFinanciamiento_1 = $this->input->post('fuenteMunicipal_1');
        $montoFuente_1 = $this->input->post('montoMunicipal_1');

        if($montoFuente_1 ==""){
            $montoFuente_1 = NULL;
        }

        if($fechaMunicipal_1!=""){
            $fechaMunicipal_1 = $this->changeFormatDateSpEng($fechaMunicipal_1);
        }else{
            $fechaMunicipal_1 = NULL;
        }


        $fechaMunicipal_2 = trim($this->input->post('fechaMunicipal_2'));
        $anioRecurso_2 = $this->input->post('perteneceAnioRecursoMunicipal2');
        $fuenteFinanciamiento_2 = $this->input->post('fuenteMunicipal_2');
        $montoFuente_2 = $this->input->post('montoMunicipal_2');

        if($montoFuente_2 == ""){
            $montoFuente_2 = NULL;
        }

        if($fechaMunicipal_2!=""){
            $fechaMunicipal_2 = $this->changeFormatDateSpEng($fechaMunicipal_2);
        }else{
            $fechaMunicipal_2 = NULL;
        }


        $fechaMunicipal_3 = trim($this->input->post('fechaMunicipal_3'));
        $anioRecurso_3 = $this->input->post('perteneceAnioRecursoMunicipal3');
        $fuenteFinanciamiento_3 = $this->input->post('fuenteMunicipal_3');
        $montoFuente_3 = $this->input->post('montoMunicipal_3');
        if($montoFuente_3 == ""){
            $montoFuente_3 = NULL;
        }
        if($fechaMunicipal_3!=""){
            $fechaMunicipal_3 = $this->changeFormatDateSpEng($fechaMunicipal_3);
        }else{
            $fechaMunicipal_3 = NULL;
        }


        $lastID =  $this->input->post('id_ffinanciamiento_recursoMunicipal');



        /*
        echo "lastId:" . $lastID . "<br>";
        
        echo "fechaMunicipal_1:" . $fechaMunicipal_1 . "<br>";
        echo "anioRecurso_1:" . $anioRecurso_1 . "<br>";
        echo "fuenteFinanciamiento_1:" . $fuenteFinanciamiento_1 . "<br>";
        echo "montoFuente_1:" . $montoFuente_1 . "<br>";


        echo "fechaMunicipal_2:" . $fechaMunicipal_2 . "<br>";
        echo "anioRecurso_2:" . $anioRecurso_2 . "<br>";
        echo "fuenteFinanciamiento_1:" . $fuenteFinanciamiento_2 . "<br>";
        echo "montoFuente_2:" . $montoFuente_2 . "<br>";


        echo "fechaMunicipal_3:" . $fechaMunicipal_3 . "<br>";
        echo "anioRecurso_3:" . $anioRecurso_3 . "<br>";
        echo "fuenteFinanciamiento_3:" . $fuenteFinanciamiento_3 . "<br>";
        echo "montoFuente_3:" . $montoFuente_3 . "<br>";   */

 

        $this->db->set('municipal_recurso_fecha_asignacion_recurso1', $fechaMunicipal_1);
        $this->db->set('municipal_recurso_anio_recurso1', $anioRecurso_1);
        $this->db->set('municipal_recurso_monto_asignado_recurso1', $montoFuente_1);
        $this->db->set('municipal_recurso_fuente_financiamiento_recurso1', $fuenteFinanciamiento_1);     



        $this->db->set('municipal_recurso_fecha_asignacion_recurso2', $fechaMunicipal_2);
        $this->db->set('municipal_recurso_anio_recurso2', $anioRecurso_2);
        $this->db->set('municipal_recurso_monto_asignado_recurso2', $montoFuente_2);
        $this->db->set('municipal_recurso_fuente_financiamiento_recurso2', $fuenteFinanciamiento_2);    


        $this->db->set('municipal_recurso_fecha_asignacion_recurso3', $fechaMunicipal_3);
        $this->db->set('municipal_recurso_anio_recurso3', $anioRecurso_3);
        $this->db->set('municipal_recurso_monto_asignado_recurso3', $montoFuente_3);
        $this->db->set('municipal_recurso_fuente_financiamiento_recurso3', $fuenteFinanciamiento_3);      
           
          

        $this->db->where('id', $lastID);
        $this->db->update("cs_registro_programa_recursos");


 

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

 
 

    }



     public function  guardarRecursoOtros(){


        $fechaOtros_1 = trim($this->input->post('fechaOtros_1'));
        $anioRecurso_1 = $this->input->post('perteneceAnioRecursoOtros1');
        $fuenteFinanciamiento_1 = $this->input->post('fuenteFinanciamOtros_1');
        $montoFuente_1 = $this->input->post('montoOtros_1');

        if($montoFuente_1 == ""){
            $montoFuente_1 = NULL;
        }

        if($fechaOtros_1!=""){
            $fechaOtros_1 = $this->changeFormatDateSpEng($fechaOtros_1);
        }else{
            $fechaOtros_1 = NULL;
        }


        $fechaOtros_2 = trim($this->input->post('fechaOtros_2'));
        $anioRecurso_2 = $this->input->post('perteneceAnioRecursoOtros2');
        $fuenteFinanciamiento_2 = $this->input->post('fuenteFinanciamOtros_2');
        $montoFuente_2 = $this->input->post('montoOtros_2');

        if($montoFuente_2 == ""){
            $montoFuente_2 = NULL;
        }

        if($fechaOtros_2!=""){
            $fechaOtros_2 = $this->changeFormatDateSpEng($fechaOtros_2);
        }else{
            $fechaOtros_2 = NULL;
        }


        $fechaOtros_3 = trim($this->input->post('fechaOtros_3'));
        $anioRecurso_3 = $this->input->post('perteneceAnioRecursoOtros3');
        $fuenteFinanciamiento_3 = $this->input->post('fuenteFinanciamOtros_3');
        $montoFuente_3 = $this->input->post('montoOtros_3');

        if($montoFuente_3 == ""){
            $montoFuente_3 = NULL;
        }

        if($fechaOtros_3!=""){
            $fechaOtros_3 = $this->changeFormatDateSpEng($fechaOtros_3);
        }else{
            $fechaOtros_3 = NULL;
        }


        $lastID =  $this->input->post('id_ffinanciamiento_recursoOtros');

/*
 
        echo "lastId:" . $lastID . "<br>";
        
        echo "fechaOtros_1:" . $fechaOtros_1 . "<br>";
        echo "anioRecurso_1:" . $anioRecurso_1 . "<br>";
        echo "fuenteFinanciamiento_1:" . $fuenteFinanciamiento_1 . "<br>";
        echo "montoFuente_1:" . $montoFuente_1 . "<br>";


        echo "fechaOtros_2:" . $fechaOtros_2 . "<br>";
        echo "anioRecurso_2:" . $anioRecurso_2 . "<br>";
        echo "fuenteFinanciamiento_1:" . $fuenteFinanciamiento_2 . "<br>";
        echo "montoFuente_2:" . $montoFuente_2 . "<br>";


        echo "fechaOtros_3:" . $fechaOtros_3 . "<br>";
        echo "anioRecurso_3:" . $anioRecurso_3 . "<br>";
        echo "fuenteFinanciamiento_3:" . $fuenteFinanciamiento_3 . "<br>";
        echo "montoFuente_3:" . $montoFuente_3 . "<br>";  */



/*
otros_recurso_fecha_asignacion_recurso1
otros_recurso_monto_asignado_recurso1


 
resumen_total_otros */



 

        $this->db->set('otros_recurso_fecha_asignacion_recurso1', $fechaOtros_1);
        $this->db->set('otros_recurso_monto_asignado_recurso1', $montoFuente_1);
        $this->db->set('otros_recurso_fuente_financiamiento_recurso1', $fuenteFinanciamiento_1);
        $this->db->set('otros_recurso_anio_recurso1', $anioRecurso_1);     

        $this->db->set('otros_recurso_fecha_asignacion_recurso2', $fechaOtros_2);
        $this->db->set('otros_recurso_monto_asignado_recurso2', $montoFuente_2);
        $this->db->set('otros_recurso_fuente_financiamiento_recurso2', $fuenteFinanciamiento_2);
        $this->db->set('otros_recurso_anio_recurso2', $anioRecurso_2); 

        $this->db->set('otros_recurso_fecha_asignacion_recurso3', $fechaOtros_3);
        $this->db->set('otros_recurso_monto_asignado_recurso3', $montoFuente_3);
        $this->db->set('otros_recurso_fuente_financiamiento_recurso3', $fuenteFinanciamiento_3);
        $this->db->set('otros_recurso_anio_recurso3', $anioRecurso_3); 
           
          

        $this->db->where('id', $lastID);
        $this->db->update("cs_registro_programa_recursos"); 



  

        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));

 
 

    }


   






}
