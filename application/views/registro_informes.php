
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>sicseqV2</title>

       <?php $this->view('template/header.php'); ?>   

       
</head>

<body>
                  <!--Modal para agregado de Nuevos Programas -->
  <?php $this->view('template/mod_nuevo_programa'); ?>
    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                           <h1>
                                               
                                            </h1>
                                         </td>
                                        <td align="right"> 
                                             <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='resetearForma()'><i class="fa fa-floppy-o"></i> Limpiar</button> 
                                          <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='saveElement()'><i class="fa fa-floppy-o"></i> Editar</button> 
                                           <button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='deleteElement()'><i class="fa fa-floppy-o"></i> Nuevo</button> 

                                            <button type="button" class="btn btn-success waves-effect w-md waves-light m-b-15" id="btnGuardar" data-toggle="modal"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Informes</i>
                                </li>
                                <li class="active">Registro de Informes</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
                
                
                 
                

                    <!-- Datos del Comite -->
                    <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Captura de Informe</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_capturaInforme" action="#">

 
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Nombre del Comité:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" id="nombreComite" name="nombreComite"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Informe (Antes Cédula):</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" id="informe" name="informe"  >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Apartado de Informe (Antes Cédula):</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="apartadoInforme" name="apartadoInforme"  >
                                                    </div>
                                                </div>
                                       

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">No:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="numero" name="numero"  >
                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Clave del Comité:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="claveComite" name="claveComite"  >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Nombre del Comité:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="nombreComite2" name="nombreComite2"  >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Fecha de Captura:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="fechaCaptura" name="fechaCaptura"  >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Numero de Registro del CCS:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="numeroRegistroCCS" name="numeroRegistroCCS"  >
                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Nombre de Obra/Apoyo/Servicio:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="nombreProyecto" name="nombreProyecto"  >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Fecha de Llenado:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="fechaLlenado" name="fechaLlenado"  >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Periodo de La ejecucion o Entrega del Beneficio:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="periodoEjecucion" name="periodoEjecucion"  >
                                                    </div>
                                                </div>
 
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Clave de la Entidad Federativa:</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="entidadFederativa" name="entidadFederativa"  >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Clave del Municipio</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="claveMunicipio" name="claveMunicipio"  >
                                                    </div>
                                                </div>

                                               <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-right ">Clave de la Localidad</label>
                                                    <div class="col-sm-6" ">
                                                        <input type="text" class="form-control input-sm" id="claveLocalidad" name="claveLocalidad"  >
                                                    </div>
                                                </div>



                                              





 

    <br> <br> <br> 
                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>               
                <!-- fin Datos del Comite -->




                 <!-- Funcion del Comite -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Cuestionario</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#funciones"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="funciones" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_cuestionario">

                                                     <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">De acuerdo con la Informacion proporcionada, Considera que la Localidad La comunidad o Las personas Beneficiadas Cumplen con Los requisitos para ser Beneficiarios</label>
                                                   
                                                        <label class="radio-inline"><input type="radio" value="SI" name="cumplenRequisitosBenefic">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="cumplenRequisitosBenefic">No</label>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right "> En la eleccion de integrantes de los comites Tiene la misma posibilidad de ser Electos H y M</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="igualdadHyM">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="igualdadHyM">No</label>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right "> ¿El Programa entregó los beneficios correcta y oportunamente, conforme a las reglas de operación u otras normas que lo regulen?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="ProgramaEntreOportunam">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="ProgramaEntreOportunam">No</label>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">Después de realizar la supervisión de la obra, apoyo o servicio ¿Consideran que cumple con lo que el Programa les informó que se les entregaría?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="proyectoCumplePrograma">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="proyectoCumplePrograma">No</label>
                                                </div>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">Detectaron que el Programa se utilizó con fines políticos, electorales, de lucro u otros distintos a su objetivo?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="ProgramaFinPolitico">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="ProgramaFinPolitico">No</label>
                                                </div>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">¿Recibieron quejas y denuncias sobre la aplicación u operación del Programa?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="recibieronQuejas">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="recibieronQuejas">No</label>
                                                </div>


                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">¿Entregaron las quejas y denuncias a la autoridad competente?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="entregaronQuejasAutoridad">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="entregaronQuejasAutoridad">No</label>
                                                </div>


                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-10 text-right ">   ¿Recibieron respuesta de las quejas que entregaron a la autoridad competente?</label>
                                                    
                                                        <label class="radio-inline"><input type="radio" value="SI" name="recibieronQuejaAutoridadCompetente">Si</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="recibieronQuejaAutoridadCompetente">No</label>
                                                </div>

                                             
 


                                               
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del Comite -->





                 <!-- Funcion del Comite -->
            
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del Comite -->

                     <!-- Integrante del Comite -->


                <div>
     <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Cuestionario 2</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#preguntasClave"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="preguntasClave" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_cuestionario2">




                                             <div class="form-group">
                                                    <label for="textInput" class="col-sm-6 text-right "> La información que conocen se refiere a:</label>
                                                     <div class="col-sm-6">
                                                         <select class="form-control selectpicker" multiple='multiple'   title="Pueden elegir mas de una opción..." id="csInformacionConocen[]" name="csInformacionConocen[]">  
                                                            <option>Seleccione...</option>


                                                            <?php
                                                            
                                                                foreach ($listInformes as $row) {
                                                                    $selected = "";

                                                                    if($row['seccion'] == "informacion_conocen"){
 
                                                                        echo "<option value='" . $row['id'] . "' $selected>" . ($row['catalogo']) . ".</option>\n";
                                                                    }
                                                                }
                                                            ?>

                                                          
                                                       
                                                        </select>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-6 text-right "> ¿Qué actividades de Contraloría Social realizaron como comité? </label>
                                                     <div class="col-sm-6">
                                                         <select class="form-control selectpicker" multiple  title="Pueden elegir mas de una opción..."  id="csActividades[]" name="csActividades[]">
                                                            <option>Seleccione...</option>


                                                            <?php
                                                            
                                                                foreach ($listInformes as $row) {
                                                                    $selected = "";

                                                                    if($row['seccion'] == "actividades"){
 
                                                                        echo "<option value='" . $row['id'] . "' $selected>" . ($row['catalogo']) . ".</option>\n";
                                                                    }
                                                                }
                                                            ?>

                                                          
                                                       
                                                        </select>
                                                    </div>

                                                </div>


                                                      <div class="form-group">
                                                    <label for="textInput" class="col-sm-6 text-right "> De los resultados de seguimiento, supervisión y vigilancia ¿Para qué les sirvió participar en actividades de Contraloría
                                                    Social? (pueden elegir más de una opción)</label>
                                                     <div class="col-sm-6">
                                                         <select class="form-control selectpicker" multiple id="csUtilidad" title="Pueden elegir mas de una opción..." id="csUtilidad[]" name="csUtilidad[]">
                                                            <option>Seleccione...</option>
                                                             <?php
                                                            
                                                                foreach ($listInformes as $row) {
                                                                    $selected = "";

                                                                    if($row['seccion'] == "utilidad_cs"){
 
                                                                        echo "<option value='" . $row['id'] . "' $selected>" . ($row['catalogo']) . ".</option>\n";
                                                                    }
                                                                }
                                                            ?>


                                                          
                                                       
                                                        </select>
                                                    </div>

                                                </div>


                                               
                                            

                                                <input type="hidden" name="csInformacionConocenHidden" id="csInformacionConocenHidden">
                                                <input type="hidden" name="csActividadesHidden" id="csActividadesHidden">
                                                <input type="hidden" name="csUtilidadHidden" id="csUtilidadHidden">
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->

                </div>
              
               

            
                
             

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>

    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
 

      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   
    


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">

         var v_esquema = 0;
         
         $(document).ready(function () {

               $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
               $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */


                    if($("#programaSeleccionado").val() == "todos"){
            //alert("Debe seleccionar un Programa, antes de guardar Registro.");
            swal("Advertencia!", "Debe seleccionar un Programa, antes de guardar Registro.", "warning");
            $("#btnGuardar").fadeOut("slow");
          }


         
                changeProgram();


                $('#upload').on('click', function () {
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'registro_comite/upload_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });








             $.datepicker.regional['es'] = {
                  closeText: 'Cerrar',
                  prevText: '<Ant',
                  nextText: 'Sig>',
                  currentText: 'Hoy',
                  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                  monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                  dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                  dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                  weekHeader: 'Sm',
                  dateFormat: 'dd/mm/yy',
                  firstDay: 1,
                  isRTL: false,
                  showMonthAfterYear: false,
                  yearSuffix: ''
                };

                $.datepicker.setDefaults($.datepicker.regional['es']);



            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();


           


               $( function() {
            $( "#fechaCaptura" ).datepicker({ dateFormat: 'dd/mm/yy' });
            $( "#fechaLlenado" ).datepicker({ dateFormat: 'dd/mm/yy' });
           
            
          } );



            

          });    







         function sumaPeople(){
            
            var total = 0;

                 
                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                   //     alert("value:" + this.value);
                        total += parseFloat(this.value);
                    }
                }); 


                //total = parseFloat($("#total_people_womans").val()) + parseFloat($("#total_people_mans").val());

            $("#total_personas").val(total);




        }

         function changeProgram(){


            var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }


                      if(id_programSelected!="todos"){
                  $("#btnGuardar").fadeIn("slow");
                }


               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            recurso = obj.recurso;
                            esquema = obj.esquema;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").fadeIn("slow");

                    if(recurso == 1){
                         $("#patcs_menu").hide();
                         $("#petcs_menu").show("slow");
                             $("#documentos_menu").show("slow");

                           $("#seguimiento_menu").hide("slow");
                           $("#reuniones_menu").show("slow");
                

                           $("#informes_menu").show("slow");
                             $("#formatos_menu").hide("slow");
                    }

                    
                    v_esquema = 0;

                    if(esquema==1){
                        v_esquema = 1;
                    }
                     

                    if(recurso == 2 || v_esquema == 1){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow");    
                            $("#documentos_menu").show("slow");   


                          $("#seguimiento_menu").show("slow");
                           $("#reuniones_menu").hide("slow");
                

                           $("#informes_menu").hide("slow");
                             $("#formatos_menu").show("slow");
                    }

    
                    }
                });

 
         
               
        }



        function guardar(){



            $.ajax({
                    url: "<?php echo site_url('registro_informes/guardarCapturaInforme') ?>",
                    type: "POST",
                    data: $('#form_capturaInforme').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


                  $.ajax({
                    url: "<?php echo site_url('registro_informes/guardarCuestionario') ?>",
                    type: "POST",
                    data: $('#form_cuestionario').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });






                  $("#csInformacionConocenHidden").val($("#csInformacionConocen option:selected").text());
                  $("#csActividadesHidden").val($("#csActividades option:selected").text());
                  $("#csUtilidadHidden").val($("#csUtilidad option:selected").text());


                  $.ajax({
                    url: "<?php echo site_url('registro_informes/guardarCuestionario2') ?>",
                    type: "POST",
                    data: $('#form_cuestionario2').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

        
 
                    }
                });


               

                    
        
 








            swal("Bien hecho!", "El informe ha sido guardado.", "success");
        }


         $("#municipio").change(function () {

                var municipioSelected = $("#municipio option:selected").val();     

                populate_localidad(municipioSelected);

            });


            function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_proyecto/populate_localidad') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option>Selecciona Localidad..</option>");


                        $.each(data, function (i, name) {
                            $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   







    </script>

</body>

</html>
