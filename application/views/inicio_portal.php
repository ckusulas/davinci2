<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" style="" lang="es"><!--<![endif]--><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">


		<!--  Metadados -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta http-equiv="Cache-Control" content="max-age=0, no-cache, no-store, private">

		<title>Contraloría Social Querétaro</title>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Nuevo registro">
		<meta name="keywords" content="contraloría social">
		<meta name="author" content="">
		<meta name="robots" content="noindex">
		<meta name="googlebot" content="noindex">
 

		<!-- favicon (.ico / .png / .gif) -->
		<link rel="shortcut icon" href="http://csq.ikonlab.mx/favicon.ico">

		<!-- Google Fonts-->
		<link href="<?php echo asset_url();?>recursos/css.css" rel="stylesheet">

		<!--  Estilos base -->
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/icomoon.css">
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/simple-line-icons.css">
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/font-awesome.css">
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/bootstrap.css">
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/bootstrap-multiselect.css">
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/animate.css">
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/keyframes.css">
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/owl.css">
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/owl_002.css">
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/datedropper.css">

        <!--  Estilos del tema -->
        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/estilos.css">


        <style>
			body {font-family: Arial, Helvetica, sans-serif;}

			/* Full-width input fields */
			input[type=text], input[type=password] {
			    width: 28%;
			    padding: 12px 20px;
			    margin: 8px 0;
			    display: inline-block;
			    border: 1px solid #ccc;
			    box-sizing: border-box;
			}

			/* Set a style for all buttons */
			button {
			    background-color: #4CAF50;
			    color: white;
			    padding: 14px 20px;
			    margin: 8px 0;
			    border: none;
			    cursor: pointer;
			    width: 28%;
			}

			button:hover {
			    opacity: 0.8;
			}

			/* Extra styles for the cancel button */
			.cancelbtn {
			    width: auto;
			    padding: 10px 18px;
			    background-color: #f44336;
			}

			/* Center the image and position the close button */
			.imgcontainer {
			    text-align: center;
			    margin: 24px 0 12px 0;
			    position: relative;
			}

			img.avatar {
			    width: 30%;
			    border-radius: 50%;
			}

			.container {
			    padding: 16px;
			}

			span.psw {
			    float: right;
			    padding-top: 16px;
			}

			/* The Modal (background) */
			.modal {
			    display: none; /* Hidden by default */
			    position: fixed; /* Stay in place */
			    z-index: 1; /* Sit on top */
			    left: 0;
			    top: 0;
			    width: 100%; /* Full width */
			    height: 100%; /* Full height */
			    overflow: auto; /* Enable scroll if needed */
			    background-color: rgb(0,0,0); /* Fallback color */
			    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
			    padding-top: 100px;
			}

			/* Modal Content/Box */
			.modal-content {
			    background-color: #fefefe;
			    margin: 5% auto 25% auto; /* 5% from the top, 15% from the bottom and centered */
			    border: 1px solid #888;
			    width: 350px; /* Could be more or less, depending on screen size */
			}

			/* The Close Button (x) */
			.close {
			    position: absolute;
			    right: 25px;
			    top: 0;
			    color: #000;
			    font-size: 35px;
			    font-weight: bold;
			}

			.close:hover,
			.close:focus {
			    color: red;
			    cursor: pointer;
			}

			/* Add Zoom Animation */
			.animate {
			    -webkit-animation: animatezoom 0.6s;
			    animation: animatezoom 0.6s
			}

			@-webkit-keyframes animatezoom {
			    from {-webkit-transform: scale(0)} 
			    to {-webkit-transform: scale(1)}
			}
			    
			@keyframes animatezoom {
			    from {transform: scale(0)} 
			    to {transform: scale(1)}
			}

			/* Change styles for span and cancel button on extra small screens */
			@media screen and (max-width: 300px) {
			    span.psw {
			       display: block;
			       float: none;
			    }
			    .cancelbtn {
			       width: 100%;
			    }
			}
		</style>

    	<!-- Modernizr JS -->
    	<script src="<?php echo asset_url();?>recursos/modernizr-2.js"></script>

    	<!--[if lt IE 9]>
    	<script src="http://csq.ikonlab.mx/recursos/default/js/respond.min.js"></script><![endif]-->

        <link rel="stylesheet" href="<?php echo asset_url();?>recursos/sweetalert.css">
        <script src="<?php echo asset_url();?>recursos/sweetalert.js"></script>

	</head>

    <body id="">

        <!-- Modals -->

        <!-- formulario de acceso -->
        <div id="loginx" class="modalbg login">

            <form action="#" class="col-md-6 dialog form-horizontal" method="post" accept-charset="utf-8">

                <h3 class="text-center">Acceso<br><small style="text-transform:uppercase;font-size:11px;font-weight:600;">Sistema Informático de Contraloría Social de Estado de Querétaro</small></h3>
                <hr>
                <a href="#close" title="Close" class="close"><i class="icon-close icons"></i></a>
                <div class="form-group">
                    <label for="matricula" class="col-sm-3 control-label">Matrícula</label>
                    <div class="col-sm-9">
                        <input name="matricula" class="form-control" id="matricula" placeholder="Nombre de usuario" required="" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label for="clave" class="col-sm-3 control-label">Clave</label>
                    <div class="col-sm-9">
                        <input name="password" class="form-control" id="password" placeholder="Ingresa tu contraseña" required="" type="password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9">
                        <input name="" class="form-control" id="" value="" required="" type="hidden">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-small btn-primary" style="margin-bottom: 15px;"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp; Acceder</button>
                    </div>
                </div>

            </form>
        </div>

        <div id="id01" class="modal">
  
			  <form class="modal-content animate" action="<?=site_url()?>/login/validate" method="post">
			    <div class="imgcontainer">
			      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
			      <img src="<?php echo asset_url();?>/img/img_avatar2.png" width="70px" height="100px" alt="Avatar" class="avatar">
			    </div>

			    <div class="container">
			      <label for="uname"><b>Usuario</b></label>
			      <br>
			      <input type="text" class="form-control input-sm"  placeholder="Ingrese su usuario" name="uname" required>
			      <br>
			      <label for="psw"><b>Contraseña</b></label>
			      <br>
			      <input type="password" placeholder="Ingrese su constraseña" name="psw" required>	
			      <br>		        
			      <button type="submit">Login</button>
			     <!-- <label>
			        <input type="checkbox" checked="checked" name="remember"> Remember me
			      </label>-->
			    </div>

			    <!-- <div class="container" style="background-color:#f1f1f1">
			      <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancelar</button>
			     <span class="psw">Forgot <a href="#">password?</a></span>
			    </div>-->
			  </form>
		</div>

        <!-- Top -->        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="texto text-right">
                            <span>
                                Lunes, 9 de Abril de 2018                            </span>
                                                    </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="linea clearfix"></div>

        <!-- Encabezado -->
        <header role="banner" id="contraloria-header" class="">
            <div class="border-header">
                <div class="container">
                    <nav class="navbar navbar-default">
                                            <div class="navbar-header">
                        <a href="#" class="js-contraloria-nav-toggle contraloria-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                        <a class="navbar-brand" href="#"></a>
                        <!-- <a class="navbar-nombre" href="#" data-nav-section="arriba">Contraloría Social</a> -->
                    </div>

                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                        
                            <!-- <li><a href="http://csq.ikonlab.mx/#"><span>Acerca de<br /><i class="icon-arrow-down ico-menu"></i></span></a></li>
                            <li><a href="http://csq.ikonlab.mx/#"><span>Participación<br /><i class="icon-arrow-down ico-menu"></i></span></a></li> -->
                            <li><a href="#"><span>Sistema<br><i class="icon-layers ico-menu"></i></span></a></li>
                            <!-- <li><a href="http://csq.ikonlab.mx/#" ><span>Noticias<br /><i class="icon-arrow-down ico-menu"></i></span></a></li>
                            <li><a href="http://csq.ikonlab.mx/#" ><span>Conocer más<br /><i class="icon-arrow-down ico-menu"></i></span></a></li>
                            <li><a href="http://csq.ikonlab.mx/#" ><span>Contacto<br /><i class="icon-arrow-down ico-menu"></i></span></a></li> -->
                        </ul>
                    </div>
                    </nav>
                </div>
            </div>
                     </header>

        <!-- Contenido -->
		<div id="contraloria-contacto" data-section="arriba" class="animated">
			<div class="container">
				<div class="row">
					<div class="col-md-12 section-heading text-center">
						<h2 class="single-animate animate-contacto-1 animated fadeIn"><font color="#61a7c6">SICSEQ 2 Plus</font></h2>
						<div class="row">
							<div class="col-md-8 col-md-offset-2 subtext single-animate animate-contacto-2 animated fadeIn">
								<h3>Bienvenido al<br><small style="text-transform:uppercase;">Sistema Informático de Contraloría Social de Estado de Querétaro</small></h3>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<div class="contraloria-contacto-item to-animate fadeInUp animated">
							<div class="col-sm-6 col-sm-offset-3 text-center">
								<h2>Identificación requerida</h2>
								<p>Por favor, proporcione su matrícula de identificación y clave
 de acceso para poder ingresar al sistema y realizar las capturas o
consultas necesarias:</p>
								<p><a class="btn btn-primary"  onclick="document.getElementById('id01').style.display='block'" >Mostrar ventana de acceso</a></p> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- / Contenido -->

    	<!-- Info -->
        <div id="contraloria-contacto" class="padding-top fondo-gris">
            <div class="container">
            </div>
        </div>

    	<!-- Pié de página -->
		<footer id="footer" role="contentinfo">
			<div class="container">
				<div class="row row-bottom-padded-sm">
					<div class="col-md-12">
						<p class="copyright text-center">
							<span class="logo-gob"></span><br>
							<strong class="azul">
								Poder Ejecutivo del Estado de Querétaro							</strong>
						</p>
						<p class="copyright text-center">
							<small>
								Secretaría de la Contraloría								<br>
								Dr. Leopoldo Río de la Loza No. 12 Centro Histórico, C.P. 76000 Querétaro, Qro.								<br>
								Tel: (442) 238 5000 ext. 5014, 5586							</small>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
		                <p><a id="arriba" href="#top"><i class="icon-arrow-up icons abajo"></i></a></p>
					</div>
				</div>
			</div>
		</footer>

    	<!-- Scripts -->
	<!-- jQuery -->
	<script src="<?php echo asset_url();?>recursos/jquery_003.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo asset_url();?>recursos/jquery_002.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo asset_url();?>recursos/bootstrap.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo asset_url();?>recursos/bootstrap-multiselect.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo asset_url();?>recursos/jquery.js"></script>
	<!-- Owl Carousel -->
	<script src="<?php echo asset_url();?>recursos/owl.js"></script>
	<!-- Datedropper.JS -->
	<script src="<?php echo asset_url();?>recursos/datedropper.js"></script>
	<!-- Moment.JS -->
	<script src="<?php echo asset_url();?>recursos/moment.js"></script>

	<!-- Scripts personalizados -->
	<script src="<?php echo asset_url();?>recursos/main.js"></script>

    <!-- Añadir clase activa a elemento menú de página acutal -->
    <script>

				$(document).ready(function(){
			$('.navbar-nav a[href="http://csq.ikonlab.mx/acceso"]').addClass('active');
			$('.menu-sistema a[href="http://csq.ikonlab.mx/acceso"]').addClass('active');
		});

	</script>




</body></html>