<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_materiales_capacitacion extends CI_Controller {



	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
        $this->load->library('session');

    }
 
	public function index()
	{	

    if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }

		 $listMunicipios = $this->ffinanciamiento_model->getMunicipios();

        $data = array(     
          
            'listMunicipios' => $listMunicipios
        );

		$this->load->view('registro_materiales_capacitacion',$data);
	}



        public function guardarMaterialCapacitacion(){

        $nombreMaterial         = $this->input->post('nombreMaterial');
        $archivoMaterial               = $this->input->post('archivoMaterial');
        $cantidadProducida       = $this->input->post('cantidadProducida');
        
        $entidadFederativa      = "Querétaro";
     

 

        echo "nombreMaterial: " . $nombreMaterial . "<br>";
        echo "archivoMaterial: " . $archivoMaterial . "<br>";
        echo "cantidadProducida: " . $cantidadProducida . "<br>";
        echo "entidadFederativa: " . $entidadFederativa . "<br>";
 

        die();

    }

     public function changeFormatDateSpEng($data){

        $arrayDate = explode("/", $data);
        $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
        return($newData);
    }


    public function guardarMaterialDistribucion(){

        $programa         = $this->input->post('programa');
          $entidadFederativa      = "Querétaro";
        $nombreMaterial               = $this->input->post('nombreMaterial');
        $tipoMaterial       = $this->input->post('tipoMaterial');
        $municipio       = $this->input->post('municipio');
        $localidad       = $this->input->post('localidad');
        $cantidadDistribuir       = $this->input->post('cantidadDistribuir');
        $fechaDistribuicion       = $this->input->post('fechaDistribuicion');

         if($fechaDistribuicion!="")
           $fechaDistribuicion = $this->changeFormatDateSpEng($fechaDistribuicion);



          echo "programa: " . $programa . "<br>";
          echo "nombreMaterial: " . $nombreMaterial . "<br>";
          echo "tipoMaterial: " . $tipoMaterial . "<br>";
          echo "municipio: " . $municipio . "<br>";
          echo "localidad: " . $localidad . "<br>";
          echo "cantidadDistribuir: " . $cantidadDistribuir . "<br>";
          echo "fechaDistribuicion: " . $fechaDistribuicion . "<br>";

    }

}
