 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     if ( ! function_exists('getCatalog()'))
     {

       function get_user_full_name($userId) {

			    //the database functions can not be called from within the helper
			    //so we have to explicitly load the functions we need in to an object
			    //that I will call ci. then we use that to access the regular stuff.
			    $ci=& get_instance();
			    $ci->load->database();

			    //select the required fields from the database
			    $ci->db->select('firstName, lastName');

			    //tell the db class the criteria
			    $ci->db->where('userId', $userId);

			    //supply the table name and get the data
			    $row = $ci->db->get('user')->row();

			    //get the full name by concatinating the first and last names
			    $fullName = $row->firstName . " " . $row->lastName;

			    // return the full name;
			    return $fullName;
			    

			}
     }
