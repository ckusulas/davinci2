



<table id="example" class="display" class="col-lg-12" >
    <thead >
        <tr>
            <th colspan="3"  bgcolor="E1E1E1">
                REFERENCIA
            </th>
            <th colspan="15"  bgcolor="E1E1E1">
                OBRA / APOYO / SERVICIO  <div id="cantidad_registros"><div>
            </th>
            <th colspan="9"   bgcolor="E1E1E1">
                COMITÉ
            </th>
            <th colspan="5"   bgcolor="E1E1E1">
                CAPACITACION
            </th>
            <th colspan="6"   bgcolor="E1E1E1">
                MATERIAL
            </th>
            </tr>
            <tr>
                <th>Folio</th>

                <th>Programa</th>
                <?php
                if ($is_gerencia) {
                    ?>

                    <th>Departamento</th>
    <?php
}
?>
                <th>Tipo de Beneficio</th>                    
                <th>Nombre de Obra/Servicio/Apoyo</th>
                <th>Inst.Ejecutora</th>
                <th>Estatus del Proyecto</th> 
                <th>Recurso</th>
                <th>Entidad Federativa</th>
                <th>Municipio</th>
                <th>Localidad</th>

                <th>Monto Total Asignado</th>
                <th>Monto Total Ejecutado</th>
                <th>Hombres</th>
                <th>Mujeres</th>
                <th>Total</th>
                <th>Fecha de Inicio Programada</th>
                <th>Fecha Final Programada</th>
                <th>Nombre del Comité</th>
                <th>Fecha de Registro</th>
                <th>Ej.Fiscal</th>

<!--  <th>Clave de Registro</th>-->
                <th>Hombres</th>
                <th>Mujeres</th>
                <th>Total</th>
                <th>Estatus</th>
                <th>Denominacion Ejecutora</th>
                <th>Tipo Ejecutora</th>
                <th>Tematica</th>
                <th>Figura Capacitada</th>
                <th>Fecha de Imparticion</th>
                <th>Numero de Participantes</th>
                <th>Archivo de Material</th>
                <th>Nombre del Material</th>
                <th>Tipo de Material</th>
                <th>Cantidad Producida</th>
                <th>Cantidad Distribuida</th>

                <th>Fecha de Asignación</th>

                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>

<?php
$counter = 1;
foreach ($listProyectos as $row) {

    $tag_admin = "";
    if ($is_gerencia) {
        $tag_admin = "<td>" . $row["departamento"] . "</td>";
    }

    //  $extension = "<td>".$row["clave_registro"]."</td>";
    $extension.= "<td>" . $row["masculino"] . "</td>";
    $extension.= "<td>" . $row["femenino"] . "</td>";
    $extension.= "<td>" . $row["totbeneficiarios"] . "</td>";
    $extension.= "<td>Abierto</td>";
    $extension.= "<td>" . $row["denominacion"] . "</td>";
    $extension.= "<td>Ejecutora Estatal</td>";
    $extension.= "<td>Tematica C.</td>";
    $extension.= "<td>Figura C.</td>";
    $extension.= "<td>Fecha Imparticion</td>";
    $extension.= "<td>#Particpantes</td>";
    $extension.= "<td>Archivo</td>";
    $extension.= "<td>Nombre de Material</td>";
    $extension.= "<td>Tipo de Material</td>";
    $extension.= "<td>CantidadProducida</td>";
    $extension.= "<td>CantidadDistribuida</td>";

    $extension.= "<td>FechaAsignación</td>";


    echo "<tr><td>" . $counter . "</td><td>" . $row["programa"] . "</td>" . $tag_admin . "<td>" . $row["beneficio"] . "</td><td>" . $row["nombre_proyecto"] . "</td><td>" . $row["instancia_ejecutora"] . "</td><td>Iniciado</td><td>" . $row["recurso"] . "</td><td>Queretaro</td><td>" . $row["municipio"] . "</td><td>" . $row["localidad"] . "</td><td>$" . number_format($row["recurso_asignado_vigilar"]) . "</td><td>$" . number_format($row["recurso_ejecutado_vigilado"]) . "</td><td>" . number_format($row["hombres_beneficiados"]) . "</td><td>" . number_format($row["mujeres_beneficiadas"]) . "</td><td>" . number_format($row["total_beneficiados"]) . " </td><td>" . $row["fecha_inicio_programada"] . "</td><td>" . $row["fecha_final_programada"] . "</td><td>" . $row["nombre_comite"] . "</td><td>" . $row["fecha_constitucion"] . "</td><td>2018</td>" . $extension;
    ?>

                <td>
                    <button type="button" class="btn btn-xs btn-primary waves-effect w-md waves-light m-b-15"  onClick='viewProy(<?= $row['id'] ?>)' data-toggle="modal" data-target=". "><i class="fa fa-pencil"></i> Detalles</button>



                </td>




                </tr>








    <?php
    $counter++;
}
?>


            <?php
            $tag_filter = "";
            if ($is_gerencia) {
                $tag_filter = "<th>Departamento</th>";
            }
            ?>
            </tbody>
            <tfoot>

                <tr>
                    <th>Folio</th>
                    <th>Programa</th>
<?php
echo $tag_filter;
?>



                    <th>Tipo de Beneficio</th>                    
                    <th>Nombre de Obra/Servicio/Apoyo</th>
                    <th>Inst.Ejecutora</th>
                    <th>Estatus del Proyecto</th>
                    <th>Recurso</th>
                    <th>Entidad Federativa</th>
                    <th>Municipio</th>
                    <th>Localidad</th>
                    <td><strong>$<?php echo number_format($totalRecursoAsignado) ?></strong></td>
                    <th>Recurso Ejecutado</th>
                    <th>Hombres</th>
                    <th>Mujeres</th>
                    <td><strong> <?php echo number_format($totalBeneficiados) ?></strong></td>
                    <th>Fecha Inicio Programada</th>
                    <th>Fecha Final Programada</th>
                    <th>Nombre del Comité</th>
                    <th>Fecha de Registro</th>
                    <th>Ej.Fiscal</th>

<!-- <th>Clave de Registro</th>-->
                    <th>Hombres</th>
                    <th>Mujeres</th>
                    <th>Total</th>
                    <th>Estatus</th>
                    <th>Denominacion Ejecutora</th>
                    <th>Tipo Ejecutora</th>
                    <th>Tematica</th>
                    <th>Figura Capacitada</th>
                    <th>Fecha de Imparticion</th>
                    <th>Numero de Participantes</th>
                    <th>Archivo de Material</th>
                    <th>Nombre del Material</th>
                    <th>Tipo de Material</th>
                    <th>Cantidad Producida</th>
                    <th>Cantidad Distribuida</th>

                    <th>Fecha de Asignación</th>

                    <th>Acciones</th>
                </tr>
            </tfoot>
            </table>








            <script type="text/javascript">

                $(document).ready(function () {






                    $('#example tfoot th').each(function () {
                        var title = $(this).text();
                        $(this).html('<input type="text" placeholder="' + title + '" />');
                    });


                    var tableProy = $('#example').DataTable({
                         language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
                        dom: 'Bfrtip',
                        "scrollX": true,
                        pagingType: 'full_numbers',
                        dom: 'T<"clear">lfrtip',
                                bAutoWidth: false,
                        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                        info: true


                    });

                    //obtener total registros
                    //alert(tableProy.page.info().recordsTotal);


                    tableProy.columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that
                                        .search(this.value)
                                        .draw();

                                // $("#cantidad_registros").html(tableProy.page.info().recordsTotal);

                            }
                        });
                    });










                });


                function viewProy(id_proyecto)
                {


                    $("#id_proyecto").val(id_proyecto);

                    $("#method").val("view");

                    $("#form_indicadores").submit();




                }


            </script>