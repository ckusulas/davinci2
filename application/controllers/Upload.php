<?php
class Upload extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('upload_model');
    }
 
    function index(){
        $this->load->view('upload_view');
            $this->load->library('session');
    }
    

    //departamento/Programa/seccion
 
    function do_upload(){ 

        $path_initial = "./upload/adjuntos/";


        //fk departamento
        //fk user
        //fk programa
       
    

       // echo "seccion:" . 
        $seccion = $this->input->post('seccion');
       // echo "depto:" . 
        $depto =  $this->session->userdata('nombre');
      //  echo "programa:" . 
        $programa = $this->input->post('programa');
        $fk_departamento = $this->input->post('fk_depto');
    
     //   echo "<br>";

        $postUrl = $depto . "/" . $programa . "/" . $seccion . "/";
        $preUrl = base_url() . "upload/adjuntos/";

           $saveTo = $path_initial . $postUrl;
        $objeto = $this->input->post('objeto');

         if (!file_exists($saveTo)) {
                        mkdir($saveTo, 0777, true);
                    }  

                   // echo "saveTO:" . $saveTo;

        $config['upload_path']=  $saveTo; //"./assets/images";
        $config['allowed_types']='gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx';
       //$config['encrypt_name'] = TRUE;


        //$this->input->post('seleccPgma');
         
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());
            
          /*  echo base_url() . "<br>";
            echo print_r($data);*/
            $title= "titulo";// $this->input->post('title');
            $image= $data['upload_data']['file_name']; 
            $url = $data['upload_data']['file_path']; 
            $ext = $data['upload_data']['file_ext']; 
            $tipo = $data['upload_data']['file_type']; 
            $fk_programa = $programa;
            $fk_departamento = $fk_departamento;

       

           // print_r($data);
           // $url= $data['upload_data']['url']; 
            /*
               [file_name] => f83fc4a72337a3c541bb833c07c8683c.pdf
            [file_type] => application/pdf
            [file_path] => /var/www/davinci2/assets/images/
            [full_path] => /var/www/davinci2/assets/images/f83fc4a72337a3c541bb833c07c8683c.pdf
            [raw_name] => f83fc4a72337a3c541bb833c07c8683c
            [orig_name] => example_007.pdf
            [client_name] => example_007.pdf
            [file_ext] => .pdf
            [file_size] => 539.79




            */


            $status = true;

            if($image!="")
            {

                $status = false;
            }

      

            $url = "";
            if($image!=""){
                $url = $preUrl . $postUrl . $image;
            }
            
            $result= $this->upload_model->save_upload($title,$image,$url, $ext, $tipo, $seccion, $objeto, $fk_programa, $fk_departamento);
            $msg = $url;

              $datos = array(
            'status' => $status,
            'saveTo' => $msg,
        );
            echo $msg;//json_encode($datos);
        }
 
     }



    function delete_atachment(){

        $seccion = $this->input->post('seccion');
        $fk_programa = $this->input->post('programa');
        $fk_departamento = $this->input->post('fk_depto');    
        $objeto = $this->input->post('objeto');

        $result = $this->upload_model->delete_atachment($fk_programa, $fk_departamento, $seccion, $objeto);
 

        json_encode($result);
    }


//===================================================================================================================


  function upload_files() {

        $idCourseRecord = $this->input->post("IDCourseTraining");
        $idAtachmentTMP = $this->input->post('idAtachmentTMP');



        if ($idCourseRecord == "") {

            if ($idAtachmentTMP != "" && $idAtachmentTMP != "undefined") {

                $idCourseRecord = $idAtachmentTMP;
            } else {

                $idCourseRecord = mt_rand(80000000, 90000000);
            }
        }
        //echo "valor:" . $idCourseRecord;

        $saveTo = 'upload/atachment/' . $idCourseRecord . "/";

        if (isset($_FILES['files']) && !empty($_FILES['files'])) {
            $no_files = count($_FILES["files"]['name']);
            for ($i = 0; $i < $no_files; $i++) {
                if ($_FILES["files"]["error"][$i] > 0) {
                    echo "Error: " . $_FILES["files"]["error"][$i] . "<br>";
                } else {

                    if (!file_exists($saveTo)) {
                        mkdir($saveTo, 0777, true);
                    }


                    if (file_exists($saveTo . $_FILES["files"]["name"][$i])) {

                        echo 'El archivo ya existia: <b>' . $_FILES["files"]["name"][$i] . "</b>";
                    } else {
                        move_uploaded_file($_FILES["files"]["tmp_name"][$i], $saveTo . $_FILES["files"]["name"][$i]);
                        echo 'Archivo exitosamente guardado...';


                        //get Extension
                        $name = $_FILES["files"]["name"][$i];
                        $temp = explode(".", $name);
                        $ext = "";
                        if (count($temp) > 1) {
                            $ext = $temp[1];
                        }

                        $typeExt = "";
                        switch (strtolower($ext)) {
                            case "pdf":
                                $typeExt = "<i class='fa fa-file-pdf-o' aria-hidden='true'></i>";
                                break;

                            case "doc":
                            case "docx":
                                $typeExt = "<i class='fa fa-file-word-o' aria-hidden='true'></i>";
                                break;

                            case "xls":
                            case "csv":
                            case "xlsx":
                                $typeExt = "<i class='fa fa-table' aria-hidden='true'></i>";
                                break;

                            case "zip":
                            case "rar":
                                $typeExt = "<i class='fa fa-file-archive-o' aria-hidden='true'></i>";
                                break;

                            case "avi":
                            case "mp4":
                                $typeExt = "<i class='fa fa-file-video-o' aria-hidden='true'></i>";
                                break;

                            case "jpg":
                            case "bmp":
                            case "png":
                                $typeExt = "<i class='fa fa-file-image-o' aria-hidden='true'></i>";
                                break;



                            default:
                                $typeExt = "<i class='fa fa-file-o' aria-hidden='true'></i>";
                                break;
                        }

                        //Registrando log
                        $this->db->set('action', "Carga de Adjunto");
                        $this->db->set('fk_user', $this->session->userdata('id'));
                        $this->db->insert('Imports');

                        $fk_imports = $this->db->insert_id();

                        $this->db->set('fk_import', $fk_imports);
                        $this->db->set('fk_trainingRecords', $idCourseRecord);
                        $this->db->set('fk_trainingRecords', $idCourseRecord);
                        $this->db->set('url', $saveTo);
                        $this->db->set('extension', strtolower($ext));
                        $this->db->set('typeExtHTML', $typeExt);
                        $this->db->set('name', $_FILES['files']['name'][$i]);
                        $this->db->insert('Atachments');
                    }
                    $qtyAtachment = 0;
                    $this->db->where('fk_trainingRecords', $idCourseRecord);
                    $this->db->where('active', 1);
                    $this->db->order_by('id', 'DESC');
                    $query = $this->db->get('Atachments');

                    $qtyAtachment = $query->num_rows();
                    echo '<input type="hidden" id="idTrainingTMP" name="idTrainingTMP" value="' . $idCourseRecord . '">';
                    echo "<br>";

                    foreach ($query->result_array() as $row) {

                        echo "<div class='atachment' id='atachment_" . $row['id'] . "'>";
                        echo $row['typeExtHTML'] . "&nbsp;<a href='" . base_url() . $row['url'] . $row['name'] . "'>" . $row['name'] . "</a>&nbsp;<a href='#' onClick='deleteAtachment(" . $row['id'] . "," . $idCourseRecord . ")' border='0'><img width='14px' height='14px' src='../assets/images/delete-icon.png'></a></div>";
                    }


                    echo '<input type="hidden"  name="qtyAtachment" id="qtyAtachment" value="' . $qtyAtachment . '">';

                    $this->db->set('qtyAtachments', $qtyAtachment);
                    $this->db->where('id', $idCourseRecord);
                    $this->db->update('TrainingRecords');

                    $this->db->set('qtyAtachments', $qtyAtachment);
                    $this->db->where('cloneFrom', $idCourseRecord);
                    $this->db->update('TrainingRecords');
                }
            }
        } else {
            echo 'Porfavor, seleccione al menos un archivo.';
        }
    }
















     
}


