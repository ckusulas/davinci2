
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>

      <?php $this->view('template/header.php'); ?>   

</head>

<body>
 
  <!--Modal para agregado de Nuevos Programas. -->
  <?php $this->view('template/mod_nuevo_programa'); ?>

   



      

    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

        
          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
                    <!-- begin MAIN PAGE ROW -->
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                            
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                <tr>
                                    <td>
                                       <h1>
                                           
                                        </h1>
                                     </td>
                                     <td align="right">
                                         <!--<button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  ><i class="fa fa-floppy-o"></i> Nuevo</button> -->
                                         <?php

                                          $display = "";


                                    if($editData['metodo']=="EDIT" && $editData['onlyView']==""){
                                            ?>
                                          <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal"   onClick='editar()'><i class="fa fa-pencil"></i> Editar</button> 
                                          <?

                                          if($editData['onlyView']==""){
                                                $display = "display:none";
                                             }
                                      }
                                  
                                       
                                     if($editData['metodo']=="EDIT" && $editData['onlyView']=="1"){
                                            ?>
                                          <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal"   onClick='editar()'><i class="fa fa-pencil"></i> Editar</button> 
                                          <?

                                          if($editData['onlyView']=="1"){
                                                $display = "display:none";
                                             }
                                      }
                                      ?>
                                      
                                            <button type="button"  class="btn btn-success waves-effect w-md waves-light m-b-15" id="btnGuardar"  style="<?=$display?>" data-toggle="modal"  onClick='saveElement()'><i class="fa fa-floppy-o"></i> Guardar</button> 
                                       
                                     </td>
                                </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Fuente de Financiamiento</i>
                                </li>
                                <li class="active">Registar F.Financiamiento</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                                </ol>
                                <input type="hidden" name="programSelected" id="programSelected">
                    
                            </div>
                        </form>                    
                        </div>
                    <!-- /.col-lg-12 -->
                    </div>
                    </div>






                <div class="row">



                            <div class="col-lg-12">

                      <div class="row">

                      <div class="col-lg-12">

                                <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Resumen de Recurso Asignado</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#ResumenRecurso"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="ResumenRecurso" class="panel-collapse collapse in">
                                            <div class="portlet-body">
                                            <form class="form-horizontal forma" id="form_resumenasignado" >

                                 
                                <table border=0  width="100%">
                                    <tr>
                                        <td>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-5"> Total del Recurso Federal: </label>
                                                
                                                <div class="col-sm-4">               
                                                 <div class="input-group">
                                                        <span class="input-group-addon">$</span>                         
                                                <input type="text" id="total_recursoFederal" style="text-align:right"  name="total_recursoFederal" value="<?php echo $editData['resumen_total_federal'] ?>"  class="form-control input-sm t_totales"  disabled >                                        
                                                                                    </div>
                                                </div>
                                            </div>
                                        </td>

                                        <td>
                                               <div class="form-group">
                                                <label for="textInput" class="col-sm-5"> Total del Recurso Municipal: </label>
                                                
                                                <div class="col-sm-4">   
                                                 <div class="input-group">
                                                        <span class="input-group-addon">$</span>                                     
                                                <input type="text" id="total_recursoMunicipal" style="text-align:right" name="total_recursoMunicipal" value="<?php echo $editData['resumen_total_municipal'] ?>" class="form-control input-sm t_totales"  disabled >                                        
                                                       </div>                         
                                                </div>
                                            </div>



                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                           <div class="form-group">
                                                <label for="textInput" class="col-sm-5"> Total del Recurso Estatal: </label>
                                                
                                                <div class="col-sm-4">    
                                                 <div class="input-group">
                                                        <span class="input-group-addon">$</span>                                    
                                                <input type="text" id="total_recursoEstatal" style="text-align:right" value="<?php echo $editData['resumen_total_estatal'] ?>" name="total_recursoEstatal" class="form-control input-sm t_totales"  disabled >          </div>                              
                                                                                
                                                </div>
                                            </div>

                                            </td>
                                            <td>
                                                      <div class="form-group">
                                                        <label for="textInput" class="col-sm-5"> Total del Recurso Otros: </label>
                                                        
                                                        <div class="col-sm-4">      
                                                         <div class="input-group">
                                                                <span class="input-group-addon">$</span>                                  
                                                        <input type="text" id="total_recursoOtros"  style="text-align:right" value="<?php echo $editData['resumen_total_otros'] ?>" name="total_recursoOtros" value="<?php echo $editData['resumen_total_otros'] ?>" class="form-control input-sm t_totales"  disabled >              </div>                          
                                                                                        
                                                        </div>
                                                    </div>
                                            </td>


                                    </tr>

                                    <tr>     

                                       <td colspan=2> 

                                         

                                    <hr>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;                                                                        
                                </td>
                                <td>

                                              <div class="form-group">
                                                <label for="textInput" class="col-sm-5">*TOTAL de los Recursos Asignados</label>
                                                
                                                <div class="col-sm-4">   
                                                 <div class="input-group">
                                                        <span class="input-group-addon">$</span>                                     
                                                <input type="text" id="total_recursosasignados" style="text-align:right" name="total_recursosasignados" value="<?php echo $editData['resumen_total_asignado'] ?>" class="form-control input-sm "  disabled >                                        
                                                         </div>                       
                                                </div>
                                            </div>
                                            
                                        </td>
                                    </tr>
                                </table>
 
                                         

                                       
                                        <input type="hidden" name="totalFederalRA" id="totalFederalRA"> 
                                        <input type="hidden" name="totalEstatalRA" id="totalEstatalRA">
                                        <input type="hidden" name="totalMunicipalRA" id="totalMunicipalRA">
                                        <input type="hidden" name="totalOtrosRA" id="totalOtrosRA">
                                        <input type="hidden" name="totalRecursosRA" id="totalRecursosRA">
                                        <input type="hidden" name="id_ffinanciamiento_resumenA" id="id_ffinanciamiento_resumenA" value="<?=$editData['id_ffinanciamiento']?>">

                                          
                                               
                                            </form>
                                        </div>
                                    </div>
                              
                                <!-- /.portlet -->
                            </div>
                         

                    </div>


                  </div>






                  

                </div>


                <!-- encabezado de captura -->

                      <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4 class="text-center">CAPTURA DATOS DE FUENTE DE FINANCIAMIENTO</h4> 
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#federal estatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                          
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>








                      <!-- begin LEFT COLUMN -->
                    <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <?php

                                //extract($editData);


                                ?>

                                <div class="portlet portlet-blue">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>1. Datos Generales</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#datosPrograma"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="datosPrograma" class="panel-collapse collapse in">
                                            <div class="portlet-body">
                                            <form id="form_ffinanciamiento" class="form-horizontal forma">

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-5">Origen del recurso:</label>
                                                <div class="col-sm-6">
                                          

                                                        <select id="catalogResources" name="catalogResources" class="form-control" disabled>
                                                                <option value="0">Seleccione.. </option>                                                             
                                                                <?php

                                                                 $id_recurso = $this->session->userdata('RecursoSelecc');

                                                                foreach ($listRecourses as $row) {
                                                                    $selected = "";

                                                                    if($row['id'] == $id_recurso){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected >" . ucwords($row['nombre']) . "</option>\n";
                                                                }
                                                                ?>
                                                        </select>
                                        
                                                                                
                                                </div>
                                            </div>


                                     


                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-5">Nombre del Programa Presupuestario:</label>
                                                <div class="col-sm-6">
                                          

                                                <select  id="catalogPrograms"  class="form-control" disabled>
                                                       <option><?=$this->session->userdata('ProgramasSeleccTitulo')?></option>
                                                </select>
                                        

                                                                                
                                                </div>
                                            </div>

                                            <div class="form-group" style="display:none">
                                                <label for="textInput" class="col-sm-5">Sub-programa:</label>
                                                <div class="col-sm-6">
                                          

                                                     <select  id="catalogSubprograms"  disabled class="form-control">
                                                           <option>Seleccione SubPrograma</option>
                                                     </select>
                                        
                                                                                
                                                </div>
                                            </div>




                                           <div class="form-group">
                                                <label for="textInput" class="col-sm-5">*Ejercicio Fiscal:</label>
                                                <div class="col-sm-6">
 

                                                    <select  id="ejercicio_fiscal"  name="ejercicio_fiscal" id="ejercicio_fiscal" onChange="preGuardado()" class="form-control">
                                                           <option value="">Seleccione Año Fiscal..</option>

                                                            <?php
                                                               $sel = "";
                                                               if($editData["ejercicio_fiscal"]=="2017")
                                                                $sel = "selected";
                                                            ?>
                                                           <option value="2017" <?=$sel?>>2017</option>

                                                            <?php
                                                               $sel = "";
                                                               if($editData["ejercicio_fiscal"]=="2018")
                                                                $sel = "selected";
                                                           ?>
                                                           <option value="2018"  <?=$sel?>>2018</option>

                                                            <?php
                                                                $sel = "";
                                                               if($editData["ejercicio_fiscal"]=="2019")
                                                                $sel = "selected";
                                                            ?>
                                                           <option value="2019"  <?=$sel?>>2019</option>

                                                            <?php
                                                                $sel = "";
                                                               if($editData["ejercicio_fiscal"]=="2020")
                                                                $sel = "selected";
                                                            ?>
                                                           <option value="2020"  <?=$sel?>>2020</option>
                                                        
                                                     </select>
                                          
                                                                                
                                                </div>
                                            </div>





                                            
                                            <div class="form-group" id="label_pef">
                                                <label for="textInput" class="col-sm-5">*Presupuesto Autorizado en el PEF:</label>
                                                
                                                <div class="col-sm-6 text-left">                                        
                                                    
                                                    <div class="input-group">
                                                        <span class="input-group-addon">$</span>
                                                        <input type="text" maxlength="17" style="text-align:right" id="presupuesto_pef" value="<?php echo $editData['presupuesto_pef'] ?>"   name="presupuesto_pef" class="form-control" onBlur="addCommas(this)"  >
                                                        
                                                </div>                                      
                                                                                
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-5">*Presupuesto a Vigilar por la CS:</label>
                                                
                                                <div class="col-sm-6 text-left">  
                                                 <div class="input-group">
                                                        <span class="input-group-addon">$</span>                                      
                                                    <input type="text" style="text-align:right"  id="presupuesto_vigilar_cs"  maxlength="17" name="presupuesto_vigilar_cs" value="<?php echo $editData['presupuesto_vigilar_cs'] ?>" onBlur="addCommas(this);validarPresupuestoVigilar()"   class="form-control" >        </div>                                        
                                                                                
                                                </div>
                                            </div>

 
                                            <div class="form-group">
                                                <label  class="col-sm-5  ">*Descripción de la Población Objetivo:</label>
                                                <div class="col-sm-6">
                                                     <textarea   class="form-control" rows="10" id="descripcionPoblacion"  onChange="preGuardado()"   name="descripcionPoblacion"><?php echo $editData['descripcion_poblacion_objetivo'] ?></textarea>
                                                </div>
                                            </div>


                                      
                                            <br><br>



                                             <input type="hidden" name="resourceSelected" id="resourceSelected">
                                             <input type="hidden" name="programSelected" id="programSelected">
                                             <input type="hidden" name="subProgramSelected" id="subProgramSelected">
                                             <input type="hidden" name="exercirseFiscal" id="exercirseFiscal">

                                             <input type="hidden" name="countResourceFF" id="countResourceFF" value=1>
                                             <input type="hidden" name="countModificadora" id="countModificadora" value=1>
                                             <input type="hidden" name="countResourceFFEstatal" id="countResourceFFEstatal" value=1>
                                             <input type="hidden" name="countResourceFFMunicipal" id="countResourceFFMunicipal" value=1>
                                             <input type="hidden" name="countResourceFFOtros" id="countResourceFFOtros" value=1>

                                             <input type="hidden" name="id_ffinanciamiento" id="id_ffinanciamiento" value="<?=$editData['id_ffinanciamiento']?>" >
                                             <input type="hidden" name="metodo" id="metodo" value="<?=$editData['metodo']?>">

                                            </form>

                                            </div>
                                    </div>
                                       
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                
                <!-- Datos extra para instancias participantes-->

                 <div class="col-lg-6">

                        <div class="row">

                      <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
 
                                            <h4>2. Convenio</h4>
 
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#ExtraCS"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="ExtraCS" class="panel-collapse collapse in">
                                            <div class="portlet-body">
                                            <form id="form_convenio" class="form-horizontal forma">

                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-5 ">*¿Aplica Convenio?: </label>
                                                        <div class="col-sm-4">
                                                  

                                                         <select  id="catalogConvenio" name="catalogConvenio"  class="form-control" onChange="activarAdjuntoConvenio(this.value)">
 
                                                               <option value="0">Suscrito?..</option>
                                                               
                                                               <?php 
                                                               $selected = "";
                                                               if($editData['convenio_suscrito'] == "SI") 
                                                                $selected = "selected";
                                                                ?>
                                                               
                                                               <option value="SI" <?php echo $selected?>>Si</option>
                                                              
                                                               <?php 
                                                               $selected = "";
                                                               if($editData['convenio_suscrito'] == "NO") 
                                                                $selected = "selected";
                                                                ?>
                                                               
                                                               <option value="NO" <?php echo $selected?>>No</option>
                                                         </select>
                                                
                                                                                        
                                                        </div>
                                                    </div>
                                                    <?php
                                                        $visual = "style='display:none'";

                                                      if($editData['convenio_suscrito'] == "SI") 
                                                            $visual = "";   
                                                    ?>

                                                    <div class="form-group" id="viewAdjuntoConvenio" <?=$visual?>>
                                                        <label for="textInput" class="col-sm-5 ">*Adjuntar Convenio:</label>
                                                        <div class="col-sm-4">
                                                  
 
                                                        <input type="file" name="file" id="file1" onChange="cargarFile(this,1,'ffinanciamiento','convenio')" >
                                                        
                                               
                                                        <div id="band1"></div>
                                                        <div id="url1"></div>
                                                        <?php
                                                        $existe = 0;
                                                        if(isset($editData['url_convenio'][0])) {
                                                           // echo print_r($editData['url_convenio']);
                                                          $existe = 1;
                                                        ?>
                                                            <a target="_blank" href="<?=$editData['url_convenio'][0]['url']?>">Visualizar Adjunto</a>
                                                        <?
                                                        }
                                                        ?>
                                                        


                                                                                        
                                                        </div>
                                                    </div>



                                          

                                                


                                                    <input type="hidden" id="catalogConvenioSelected" name="catalogConvenioSelected">
                                                    <input type="hidden" id="instanciaPromotoraSelected" name="instanciaPromotoraSelected">
                                                    <input type="hidden" id="instanciaAbajoSelected" name="instanciaAbajoSelected">
                                                    <input type="hidden" id="archivoCargado" name="archivoCargado" value=<?=$existe?>>

                                                    <input type="hidden" id="id_ffinanciamiento_convenio" name="id_ffinanciamiento_convenio" value="<?=$editData['id_ffinanciamiento']?>">
                                               
                                            </form>
                                        </div>
                                    </div>
                              
                                <!-- /.portlet -->
                            </div>
                         

                    </div>


                  </div>






                  

                </div>

                    <!-- Poblacion beneficiada-->
                    <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>3. Población Beneficiada</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#beneficiosPrograma"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                <div id="beneficiosPrograma" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                        <form  id="form_beneficios" class="form-horizontal forma" >
                                            
                                            

                                           <div class="form-group" id="benefic_mujeres" style="display:none">
                                                <label for="textInput" class="col-sm-5  ">Total Mujeres Beneficiadas:</label>
                                                
                                                <div class="col-sm-4">  
                                                    <div class="input-group">
                                                  <span class="input-group-addon"><i class="fa fa-female"></i> </span>                                     
                                                <input type="text" id="total_people_womans" name="total_people_womans" class="form-control t_people"  maxlength="10" style="text-align:right" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  value="<?php echo $editData['mujeres_beneficiadas'] ?>" onblur="addCommas(this);sumaPeople()">

                                                </div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="benefic_hombres" style="display:none">
                                                <label for="textInput" class="col-sm-5   ">Total Hombres Beneficiados:</label>
                                                
                                                <div class="col-sm-4">   
                                                   <div class="input-group">
                                                  <span class="input-group-addon"><i class="fa fa-male"></i> </span>                                     
                                                <input type="text" id="total_people_mans" maxlength="10" name="total_people_mans"  value="<?php echo $editData['hombres_beneficiados'] ?>" style="text-align:right" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  class="form-control t_people"  onblur="addCommas(this); sumaPeople()"   >
                                                            </div>                 
                                                </div>
                                            </div>

                                            
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-5  ">*Total de Población Beneficiada:</label>
                                                
                                                <div class="col-sm-4"> 
                                                  <div class="input-group">
                                                  <span class="input-group-addon"><i class="fa fa-users"></i> </span>                                       
                                                <input type="text"  id="total_people" style="text-align:right;background-color:transparent;"  value="<?php echo $editData['total_beneficiados'] ?>" oninput="this.value=this.value.replace(/[^0-9]/g,'');"  class="form-control" onblur="addCommas(this)">                                        
                                                            </div>                    
                                                </div>
                                            </div>
                                            
                                            <input type="hidden" name="totalBeneficios" id="totalBeneficios">
                                            <input type="hidden" name="idRegistro_1" id="idRegistro_1" value="12">
                                            
                                            <input type="hidden" name="id_ffinanciamiento_beneficios" id="id_ffinanciamiento_beneficios"   value="<?=$editData['id_ffinanciamiento']?>">
       
                                        </form>

                                        </div>
                                </div>
                                       
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!--Fin poblacion beneficiada -->


              

              <!-- Ramo -->
              <div class="col-lg-6" style="display:none" id="modulo_ramo">

                    <div class="row">

                      <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>4. Ramo</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#ramo"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="ramo" class="panel-collapse collapse in">
                                            <div class="portlet-body">
                                            <form id="form_ramo" class="form-horizontal forma">

                                             
 
                                                    <div class="form-group">
                                                        <label for="textInput" class="col-sm-1">Ramo:<?=$editData['ramo']?></label>

                                                        <div class="col-sm-8">
                                                            
                                                        <select id="list_ramo"     name="ramo" class="col-sm-5" style="text-overflow:ellipsis">
                                                       
                                                            
                                                            <?php 
                                                       


                                                                  




                                                            
                                                           echo "<optgroup label='Ramos'>";


                                                                 echo "<option title='' value='' {$selected}>Seleccione Ramo..</option>";
                                                                
                                                               
                                                                foreach($listRamo as $row){
                                                                    $selected = "";
                                                                    if($row['id'] == $editData['ramo']){
                                                                        $selected = "selected";

                                                                    }


                                                                    $subtitle = $row['id'] ." .- ".$row['ramo'];
                                                            

                                                                    echo "<option title='{$title}' value='".$row["id"]."' {$selected}>".$subtitle."</option>";
                                                                }

                                                               echo "</optgroup>";
                                                            
                                                          ?>
                                                        </select>
                                                        </div>
                                                    </div>

                                                    

 

                                                


                                                    <input type="hidden" id="catalogConvenioSelected" name="catalogConvenioSelected">
                                                    <input type="hidden" id="instanciaPromotoraSelected" name="instanciaPromotoraSelected">

                                                    <input type="hidden" id="id_ffinanciamiento_ramo" name="id_ffinanciamiento_ramo" value="<?=$editData['id_ffinanciamiento']?>" >
                                               
                                            </form>
                                        </div>
                                    </div>
                              
                                <!-- /.portlet -->
                            </div>
                         

                    </div>


                  </div>

 
                  

                </div>
                <!-- Fin Ramo -->
                  

             

                <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4 class="text-center">5. ASIGNACIÓN DE LAS FUENTES DE FINANCIAMIENTO</h4> 
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#federal estatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                          
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                 


                    <!-- asignación Fuente Financiam Federal -->
                    <div class="col-lg-6" id="divFederal" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-blue" >
                                    <div class="portlet-heading"  >
                                        <div class="portlet-title" >
                                           <A name="primero"> <h4>RECURSO FEDERAL</h4> </A>
                                        </div>
                                        <div class="portlet-widgets" >
                                            <a data-toggle="collapse" data-parent="#accordion"  href="#navFinanciamiento"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="navFinanciamiento" class="panel-collapse">
                                             <div class="portlet-body">
                                            <form class="form-horizontal forma" id="form_recursoFederal">


                                            <div class="text-right">  <a href="#primero" onClick="addRecourse()">[+] Agregar nuevo recurso</a></div>
                                            

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   ">Recurso 1:</label>
                                            </div>
                                            <?php
                                            $fecha = $editData['fechaFederal_1'];
                                             if($fecha === "00/00/0000"){
                                                $fecha = "";
                                            }
                                            ?>


                                            <div class="form-group">
                                                <label for="textInput"  value="<?php echo $fecha ?>" class="col-sm-4   "><h5> Fecha asignación: </h5></label>
                                                
                                                 <div class="col-sm-4">  
                                                       <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar federal_1"></i>
                                                    </span>                    
                                                <input type="text" id="fechaFederal_1" style="text-align:right" name="fechaFederal_1" value="<?php echo $editData['fechaFederal_1'] ?>" class="form-control input-sm "    >                                        
                                                                                
                                                </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5> Año al que pertenece:</h5> </label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecurso1" class="form-control">
                                                    
                                                    <?php   

                                            $fecha =$editData['fechaFederal_1'];
                                            if($fecha == "00/00/0000"){
                                                $fecha = "";
                                            }


                                                    $select = "";
                                                    if($editData['anioRecurso_1']=="2017"){
                                                        $select="selected";
                                                    }

                                                     ?>
                                                    <option <?php echo $select ?>>2017</option>
                                                    <?php
                                                    $select="";
                                                    if($editData['anioRecurso_1']=="2018" || $editData['anioRecurso_1']=="" ){
                                                        $select="selected";
                                                    }

                                                     ?>

                                                    <option <?php echo $select ?>>2018</option>

                                                     <?php
                                                     $select="";
                                                    if($editData['anioRecurso_1']=="2019" ){
                                                        $select="selected";
                                                    }

                                                     ?>
                                                    <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   "><h5> Fuente de Financiamiento:</h5> </label>
                                                
                                                <div class="col-sm-4">                                        
                                                <input type="text" id="fuenteFinanciamFederal_1" value="<?php echo $this->session->userdata('fondoEconomico')?>" name="fuenteFinanciamFederal_1" class="form-control input-sm"    >                                        
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">                                                
                                                <label for="textInput" class="col-sm-4 "><h5> Monto de la Fuente: </h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                      <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                <input type="text" maxlength="15"  style="text-align:right"  id="montoFederal_1" name="montoFederal_1" class="form-control input-sm t_federal"  value="<?php echo $editData["montoFuente_1"] ?>" onblur="sumaMontoFederal()"   >              </div>                          
                                                                                
                                                </div>
                                            </div>


                             
                                            <?php  

                                           $fecha =$editData['fechaFederal_2'];
                                            if($fecha == "00/00/0000"){
                                                $fecha = "";
                                            }
                                            $style = "style='display:none'";
                                            if(trim($fecha) !="" || trim($editData['montoFuente_2'])!="" || trim($editData['fuenteFinanciamiento_2'])!="")
                                                $style = "style='display:block'";
                                            ?>
                                            
                                            <div id="view_resource2" <?php echo $style ?>>
                                            <hr>

                                            <div class="text-right">  <a href="#primero" onClick="quitRecurso(2)">[-] Eliminar recurso</a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4  ">Recurso 2:</label>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4  "><h5>Fecha asignación:</h5></label>
                                                    
                                                     <div class="col-sm-4"> 
                                                                        <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar federal_2"></i>                    </span>
                                                    <input type="text" id="fechaFederal_2" style="text-align:right" name="fechaFederal_2" value="<?php echo $editData['fechaFederal_2']  ?>" class="form-control input-sm "    >                                

                                                    </div>                     
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 "><h5>Año al que pertenece:</h5></label>
                                                    
                                                    <div class="col-sm-4">                                        
                                                    <select id="anioRecurso_2" name="anioRecurso_2" class="form-control">
                                                          <?php   
                                                    $select = "";
                                                    if($editData['anioRecurso_2']=="2017"){
                                                        $select="selected";
                                                    }

                                                     ?>
                                                    <option <?php echo $select ?>>2017</option>
                                                    <?php
                                                    $select="";
                                                    if($editData['anioRecurso_2']=="2018"){
                                                        $select="selected";
                                                    }

                                                     ?>

                                                    <option <?php echo $select ?>>2018</option>

                                                     <?php
                                                     $select="";
                                                    if($editData['anioRecurso_2']=="2019" ){
                                                        $select="selected";
                                                    }

                                                     ?>
                                                    <option <?php echo $select ?>>2019</option>
                                                    </select>

                                                                                    
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4"><h5>Fuente de Financiamiento:</h5></label>
                                                    
                                                    <div class="col-sm-4">                                        
                                                    <input type="text" id="fuenteFinanciamFederal_2" value="<?php echo $editData['fuenteFinanciamiento_2'] ?>"  name="fuenteFinanciamFederal_2" class="form-control input-sm"    >                                        
                                                                                    
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label for="textInput"    class="col-sm-4"><h5>Monto de la Fuente:</h5></label>
                                                    
                                                    <div class="col-sm-4">     
                                                      <div class="input-group">
                                                                  <span class="input-group-addon">$</span>                                   
                                                    <input type="text" style="text-align:right" id="montoFederal_2" name="montoFederal_2" maxlength="15"  class="form-control input-sm t_federal" value="<?php echo $editData["montoFuente_2"] ?>"  onblur="sumaMontoFederal()"   >                                        
                                                                                    </div>
                                                    </div>
                                                </div>

                                            </div>



                             
                                            <?php  

                                            $fecha = $editData['fechaFederal_3'];
                                            if($fecha == "00/00/0000"){
                                                $fecha = "";
                                            }

                                            $style = "style='display:none'";
                                            if(trim($fecha)!="" || trim($editData['montoFuente_3'])!="" || trim($editData['fuenteFinanciamiento_3'])!="")
                                                $style = "style='display:block'";
                                            ?>



                                            <div id="view_resource3" <?php echo $style ?>>
                                            <hr>
                                                      <div class="text-right">  <a href="#primero" onClick="quitRecurso(3)">[-] Eliminar recurso</a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 text-left ">Recurso 3:</label>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4 "><h5>Fecha asignación:</h5></label>
                                                    
                                                     <div class="col-sm-4">   
     <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar federal_3"></i>                    </span>
                                                    <input type="text" id="fechaFederal_3" name="fechaFederal_3" class="form-control input-sm " style="text-align:right"   value="<?php echo $editData['fechaFederal_3']  ?>"  >                                        
                                                                   </div>                 
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-4   "><h5>Año al que pertenece:</h5></label>
                                                    
                                                    <div class="col-sm-4">                                        
                                                    <select id="anioRecurso_3" name="anioRecurso_3" class="form-control">
                                                         <?php   
                                                                $select = "";
                                                                if($editData['anioRecurso_3']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['anioRecurso_3']=="2018" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['anioRecurso_3']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                    </select>

                                                                                    
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-4  "><h5>Fuente de Financiamiento:</h5></label>
                                              
                                                    
                                                    <div class="col-sm-4">

                                                    <input type="text" id="fuenteFinanciamFederal_3" name="fuenteFinanciamFederal_3" class="form-control input-sm"   value="<?php echo $editData['fuenteFinanciamiento_3'] ?>" >                      
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label for="textInput"  class="col-sm-4 "><h5>Monto de la Fuente:</h5></label>
                                                    
                                                    <div class="col-sm-4"> 
                                                      <div class="input-group">
                                                                  <span class="input-group-addon">$</span>                                       
                                                    <input type="text" id="montoFederal_3"  style="text-align:right" name="montoFederal_3" maxlength="15" class="form-control input-sm t_federal"  onblur="sumaMontoFederal();" value="<?php echo $editData["montoFuente_3"] ?>"  >                                        
                                                                                    
                                                                                    </div>
                                                    </div>
                                                </div>

                                            </div>



















                                 

                                            <hr >
                                              <div class="form-group">
                                                <label for="textInput" class="col-sm-4   "> Total del Recurso Federal:</label>
                                                
                                                <div class="col-sm-4">   
                                                  <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                                        
                                     
                                                <input type="text"   style="text-align:right"   id="totalFederal" disabled class="form-control input-sm"    >   </div>                                        
                                                                                
                                                </div>
                                            </div>
                                      
                                      
                                            <input type="hidden" name="perteneceAnioRecurso1_selected" id="perteneceAnioRecurso1_selected">
                                            <input type="hidden" name="perteneceAnioRecurso2_selected" id="perteneceAnioRecurso2_selected">
                                            <input type="hidden" name="perteneceAnioRecurso3_selected" id="perteneceAnioRecurso3_selected">
                                          

                                            <input type="hidden" name="id_ffinanciamiento_recursoFederal" id="id_ffinanciamiento_recursoFederal" value="<?=$editData['id_ffinanciamiento']?>">
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                </div>
                <!--asignación fuente FF -->





                <!--Datos extra para CS -->
               












                    <!-- Fuente estatal -->

                       <div class="col-lg-6" id="divEstatal" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                          <A name="estatal">  <h4>RECURSO ESTATAL</h4> </A>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#navEstatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="navEstatal" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal forma" id="form_recursoEstatal">

                                                                         
                                           <div class="text-right">  <a href="#estatal" onClick="addRecourseEstatal()">[+] Agregar nuevo recurso</a></div>


                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 ">Recurso 1:</label>
                                            </div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   "><h5>Fecha asignación:</h5></label>
                                                
                                                 <div class="col-sm-4">  
                                                  <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar estatal_1"></i>                    </span>                                      
                                                <input type="text" id="fechaEstatal_1"  style="text-align:right" value="<?php echo $editData['fechaEstatal_1'] ?>" name="fechaEstatal_1" class="form-control input-sm "    >                                        
                                                </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4  "><h5>Año al que pertenece:</h5></label>
                                                 
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecursoEstatal1" name="perteneceAnioRecursoEstatal1" class="form-control">
                                                     <?php   
                                                                $select = "";
                                                                if($editData['perteneceAnioRecursoEstatal1']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['perteneceAnioRecursoEstatal1']=="2018" || $editData['perteneceAnioRecursoEstatal1']=="" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['perteneceAnioRecursoEstatal1']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Fuente de Financiamiento:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                
                                                <input type="text" id="fuenteMunicipal_1"  name="fuenteEstatal_1"  class="form-control input-sm"   value="<?php echo $editData['fuenteEstatal_1'] ?> "   >                                        
                                                                                
                                                </div>                                    
                                              
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Monto de la Fuente:</h5></label>
                                                
                                                <div class="col-sm-4">   
                                                    <div class="input-group">
                                                                  <span class="input-group-addon">$</span>                                     
                                                <input type="text" id="montoEstatal_1"  style="text-align:right" name="montoEstatal_1" maxlength="15" class="form-control input-sm t_estatal" value="<?php echo $editData['montoEstatal_1'] ?>"  onblur="sumaMontoEstatal();"   >                                        
                                                                                </div>
                                                </div>
                                            </div>

                                    <?php
                                     $style = "style='display:none'";
                                        if(trim($editData['fechaEstatal_2'])!="" || trim($editData['montoEstatal_2'])!="" || trim($editData['fuenteEstatal_2'])!=""){
                                            $style = "style='display:block'";

                                        }
                                    ?>



                                    <div id="view_resourceEstatal2" <?php echo $style ?> >

                                    <hr>
                                          <div class="text-right">  <a href="#estatal" onClick="quitRecursoEstatal(2)">[-] Eliminar recurso</a></div>

                                            
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4  ">Recurso 2:</label>
                                            </div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   "><h5>Fecha asignación:</h5></label>
                                                
                                                 <div class="col-sm-4">    
                                                    <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar estatal_2"></i>                    </span>                                    
                                                <input type="text" id="fechaEstatal_2" style="text-align:right" value="<?php echo $editData['fechaEstatal_2'] ?>" name="fechaEstatal_2" class="form-control input-sm "    >                                        
                                                                                </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Año al que pertenece:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecursoEstatal2" name="perteneceAnioRecursoEstatal2" class="form-control">
                                                    <?php   
                                                                $select = "";
                                                                if($editData['perteneceAnioRecursoEstatal2']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['perteneceAnioRecursoEstatal2']=="2018" || $editData['perteneceAnioRecursoEstatal2']=="" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['perteneceAnioRecursoEstatal2']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4  "><h5>Fuente de Financiamiento:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select class="form-control" id="instanciaEjecutora" name="fuenteEstatal_2">
                                                            <option value="0">Seleccione...</option>
                                                                   <?php
                                                                foreach ($listFondoEstatal as $row) {
                                                                    $selected = "";
                                                                    if($editData['fuenteEstatal_2'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['fondo']) . "</option>\n";
                                                                }
                                                                ?>
                                                       
                                                        
                                                  </select>                                                
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Monto de la Fuente:</h5></label>
                                                
                                                <div class="col-sm-4">   

                                                        <div class="input-group">
                                                                  <span class="input-group-addon">$</span>

                                                <input type="text" id="montoEstatal_2"  style="text-align:right" name="montoEstatal_2"  maxlength="15" class="form-control input-sm t_estatal" value="<?php echo $editData["montoEstatal_2"] ?>"  onblur="sumaMontoEstatal();"  >                                       
                                                                                </div>
                                                </div>
                                            </div>
                                    </div>


                                    <?php
                                     $style = "style='display:none'";
                                        if(trim($editData['fechaEstatal_3'])!="" || trim($editData['montoEstatal_3'])!="" || trim($editData['fuenteEstatal_3'])!=""){
                                            $style = "style='display:block'";

                                        }
                                    ?>



                                    <div id="view_resourceEstatal3" <?php echo $style ?> >
                                        <hr>
                                            <div class="text-right">  <a href="#estatal" onClick="quitRecursoEstatal(3)">[-] Eliminar recurso</a></div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 ">Recurso 3:</label>
                                            </div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Fecha asignación:</h5></label>
                                                
                                                 <div class="col-sm-4"> 
                                                     <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar estatal_3"></i>                    </span>                                      

                                                <input type="text" id="fechaEstatal_3"  style="text-align:right" name="fechaEstatal_3" value="<?php echo $editData['fechaEstatal_3'] ?>" class="form-control input-sm "    >                                        
                                                  </div>                             
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Año al que pertenece:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecursoEstatal3" name="perteneceAnioRecursoEstatal3" class="form-control">
                                                     <?php   
                                                                $select = "";
                                                                if($editData['perteneceAnioRecursoEstatal3']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['perteneceAnioRecursoEstatal3']=="2018" || $editData['perteneceAnioRecursoEstatal3']=="" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['perteneceAnioRecursoEstatal3']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Fuente de Financiamiento:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                
                                                   <select class="form-control" id="instanciaEjecutora" name="fuenteEstatal_3">
                                                            <option value="0">Seleccione...</option>
                                                                   <?php
                                                                foreach ($listFondoEstatal as $row) {
                                                                    $selected = "";
                                                                    if($editData['fuenteEstatal_3'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['fondo']) . "</option>\n";
                                                                }
                                                                ?>
                                                       
                                                        
                                                  </select>        

                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Monto de la Fuente:</h5></label>
                                                
                                                <div class="col-sm-4">        
                                                  <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                
                                                <input type="text" id="montoEstatal_3"  style="text-align:right" name="montoEstatal_3"  maxlength="15" class="form-control input-sm t_estatal"   value="<?php echo $editData["montoEstatal_3"] ?>"   onblur="sumaMontoEstatal();"     >                                    
                                                    </div>                                                                                
                                                </div>
                                            </div>
                                    </div>

                                            <hr >
                                              <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "> Total del Recurso Estatal:</label>
                                                
                                                <div class="col-sm-4">  
                                                  <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                                        
                                      
                                                <input type="text" id="totalEstatal"  style="text-align:right"  value="<?php echo $editData["resumen_total_estatal"] ?>"  disabled class="form-control input-sm"    >                                        
                    </div>                                                                                
                                                </div>
                                            </div>
                                           
                                         


                                            <input type="hidden" name="perteneceAnioRecursoEstatal_selected_1" id="perteneceAnioRecursoEstatal_selected_1">
                                            <input type="hidden" name="perteneceAnioRecursoEstatal_selected_2" id="perteneceAnioRecursoEstatal_selected_2">
                                            <input type="hidden" name="perteneceAnioRecursoEstatal_selected_3" id="perteneceAnioRecursoEstatal_selected_3">
                                            <input type="hidden" name="id_ffinanciamiento_recursoEstatal" id="id_ffinanciamiento_recursoEstatal" value="<?=$editData['id_ffinanciamiento']?>">
                                      
 


                                          
                                               
                                            </form>
                                   
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         </div>

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->
                </div>
                   <!-- fin - Fuente estatal -->





            

                    <!-- Fuente Municipal -->

                      <div class="col-lg-6" id="divMunicipal" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                           <A name="municipal">   <h4>RECURSO MUNICIPAL</h4></A>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#navMunicipal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="navMunicipal" class="panel-collapse">
                                             <div class="portlet-body">
                                            <form class="form-horizontal forma" id="form_recursoMunicipal">


                                            <div class="text-right">  <a href="#municipal" onClick="addRecourseMunicipal()">[+] Agregar nuevo recurso</a></div>
                                            

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 ">Recurso 1:</label>
                                            </div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Fecha asignación:</h5></label>
                                                
                                                 <div class="col-sm-4"> 
                                                 <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar municipal_1"></i>                    </span>                                       
                                                <input type="text" id="fechaMunicipal_1" style="text-align:right" name="fechaMunicipal_1"" class="form-control input-sm " value="<?php echo $editData['fechaMunicipal_1'] ?> "    >                                        
                                                </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Año al que pertenece:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecursoMunicipal1" name="perteneceAnioRecursoMunicipal1" class="form-control">
                                                    <?php   
                                                                $select = "";
                                                                if($editData['perteneceAnioRecursoMunicipal1']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['perteneceAnioRecursoMunicipal1']=="2018" || $editData['perteneceAnioRecursoEstatal3']=="" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['perteneceAnioRecursoMunicipal1']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4  "><h5>Fuente de Financiamiento:</h5></label>
                                                
                                                <div class="col-sm-4">       

                                                    <select class="form-control" id="fuenteMunicipal_1" name="fuenteMunicipal_1">
                                                            <option value="0">Seleccione...</option>
                                                                   <?php
                                                                foreach ($listFondoEstatal as $row) {
                                                                    $selected = "";
                                                                    if($editData['fuenteMunicipal_1'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['fondo']) . "</option>\n";
                                                                }
                                                                ?>
                                                       
                                                        
                                                  </select>     




                                               
                                            </div>
                                          </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Monto de la Fuente:</h5></label>
                                                
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                                        
                                                <input type="text" id="montoMunicipal_1"  name="montoMunicipal_1"  style="text-align:right"  maxlength="15" class="form-control input-sm t_Municipal"  value="<?php echo $editData['montoMunicipal_1'] ?> "  onblur="sumaMontoMunicipal();"   >                                        
                                </div>                                                
                                                </div>
                                            </div>



                                    <?php

                                     $style = "style='display:none'";
                                        if(trim($editData['fechaMunicipal_2'])!="" || trim($editData['montoMunicipal_2'])!="" || trim($editData['fuenteMunicipal_2'])!=""){
                                            $style = "style='display:block'";

                                        }
                                    ?>



                                     <div id="view_resourceMunicipal2" <?php echo $style?>>

                                    <hr>

                                                    
                                           <div class="text-right">  <a href="#municipal" onClick="quitRecursoMunicipal(2)">[-] Eliminar recurso</a></div>

                                                    
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 ">Recurso 2:</label>
                                            </div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Fecha asignación:</h5></label>
                                                
                                                 <div class="col-sm-4">  
                                                   <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar municipal_2"></i>                    </span>                                                                             
                                                <input type="text" id="fechaMunicipal_2" name="fechaMunicipal_2" style="text-align:right" class="form-control input-sm " value="<?php echo $editData['fechaMunicipal_2'] ?> "    >          </div>                              
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Año al que pertenece:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecursoMunicipal2" name="perteneceAnioRecursoMunicipal2" class="form-control">
                                                    <?php   
                                                                $select = "";
                                                                if($editData['perteneceAnioRecursoMunicipal2']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['perteneceAnioRecursoMunicipal2']=="2018" || $editData['perteneceAnioRecursoEstatal3']=="" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['perteneceAnioRecursoMunicipal2']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Fuente de Financiamiento:</h5></label>
                                                
                                                <div class="col-sm-4">        


                                                  <select class="form-control" id="fuenteMunicipal_2" name="fuenteMunicipal_2">
                                                            <option value="0">Seleccione...</option>
                                                                   <?php
                                                                foreach ($listFondoEstatal as $row) {
                                                                    $selected = "";
                                                                    if($editData['fuenteMunicipal_2'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['fondo']) . "</option>\n";
                                                                }
                                                                ?>
                                                       
                                                        
                                                  </select>                                     
                                                                                       
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Monto de la Fuente:</h5></label>
                                                
                                                <div class="col-sm-4"> 
                                                     <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                                        

                                                <input type="text" id="montoMunicipal_2" name="montoMunicipal_2"  style="text-align:right"  maxlength="15" class="form-control input-sm t_Municipal"  onblur="sumaMontoMunicipal();" value="<?php echo $editData['montoMunicipal_2'] ?> "  >                                       
                                                                        
                                                                        </div>        
                                                </div>
                                            </div>
                                    </div>



                                    <?php
                                     $style = "style='display:none'";
                                        if(trim($editData['fechaMunicipal_3'])!="" || trim($editData['montoMunicipal_3'])!="" || trim($editData['fuenteMunicipal_3'])!=""){
                                            $style = "style='display:block'";

                                        }
                                    ?>


                                     <div id="view_resourceMunicipal3" <?php echo $style ?>>

                                        <hr>
                                             <div class="text-right">  <a href="#municipal" onClick="quitRecursoMunicipal(3)">[-] Eliminar recurso</a></div>


                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4">Recurso 3:</label>
                                            </div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Fecha asignación:</h5></label>
                                                
                                                 <div class="col-sm-4">     
                                                   <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar municipal_3"></i>                    </span>                                                                                          
                                                <input type="text" id="fechaMunicipal_3" name="fechaMunicipal_3" style="text-align:right" class="form-control input-sm "     value="<?php echo $editData['fechaMunicipal_3'] ?> "   >                                        </div>
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Año al que pertenece:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecursoMunicipal3" name="perteneceAnioRecursoMunicipal3" class="form-control">
                                                     <?php   
                                                                $select = "";
                                                                if($editData['perteneceAnioRecursoMunicipal3']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['perteneceAnioRecursoMunicipal3']=="2018" || $editData['perteneceAnioRecursoEstatal3']=="" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['perteneceAnioRecursoMunicipal3']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Fuente de Financiamiento:</h5></label>
                                                
                                                <div class="col-sm-4">        
                                                  <select class="form-control" id="fuenteMunicipal_3" name="fuenteMunicipal_3">
                                                            <option value="0">Seleccione...</option>
                                                                   <?php
                                                                foreach ($listFondoEstatal as $row) {
                                                                    $selected = "";
                                                                    if($editData['fuenteMunicipal_3'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['fondo']) . "</option>\n";
                                                                }
                                                                ?>
                                                       
                                                        
                                                  </select>         
                                       
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Monto de la Fuente:</h5></label>
                                                
                                                <div class="col-sm-4">     
                                                  <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                                        
                                   
                                                <input type="text" id="montoMunicipal_3" name="montoMunicipal_3" style="text-align:right"  maxlength="15" class="form-control input-sm t_Municipal"   onblur="sumaMontoMunicipal();"     value="<?php echo $editData['montoMunicipal_3'] ?> "  ></div>                                                                 
                                                                                
                                                </div>
                                            </div>
                                    </div>

                                            <hr >
                                              <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "> Total del Recurso Municipal:</label>
                                                
                                                <div class="col-sm-4">   
                                                  <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                                        
                                     
                                                <input type="text" id="totalMunicipal"  style="text-align:right" disabled class="form-control input-sm"   value="<?php echo $editData["resumen_total_municipal"] ?>"  ></div>                                        
                                                                                
                                                </div>
                                            </div>
                                         

                                            <input type="hidden" name="id_ffinanciamiento_recursoMunicipal" id="id_ffinanciamiento_recursoMunicipal" value="<?=$editData['id_ffinanciamiento']?>">
                                      
 
                                            
                                            
                                            

                                          
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                </div>
                <!-- Fuente Municipal-->


                    <!-- otros -->


                    <div class="col-lg-6" id="divOtro" style="display:none">

                        <div class="row">

                      <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                           <A name="otros">   <h4>OTRO RECURSO</h4> </A>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#navOtros"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="navOtros" class="panel-collapse">
                                             <div class="portlet-body">
                                            <form class="form-horizontal forma" id="form_recursoOtros">


                                             
                                            <div class="text-right">  <a href="#otros" onClick="addRecourseOtros()">[+] Agregar nuevo recurso</a></div>
                                           

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4  ">Recurso 1:</label>
                                            </div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Fecha asignación:</h5></label>
                                                
                                                 <div class="col-sm-4">  
                                                   <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar otros_1"></i>                    </span>                                      
                                                <input type="text" id="fechaOtros_1" name="fechaOtros_1"  style="text-align:right" class="form-control input-sm "  value="<?php echo $editData['fechaOtros_1']?>" >                                        
                                                                                </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Año al que pertenece:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecursoOtros1" name="perteneceAnioRecursoOtros1" class="form-control">
                                                    <?php   
                                                                $select = "";
                                                                if($editData['perteneceAnioRecursoOtros1']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['perteneceAnioRecursoOtros1']=="2018" || $editData['perteneceAnioRecursoEstatal3']=="" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['perteneceAnioRecursoOtros1']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Fuente de Financiamiento:</h5></label>
                                                
                                                <div class="col-sm-4">    

                                                    <select class="form-control" id="fuenteMunicipal_3" name="fuenteMunicipal_3">
                                                            <option value="0">Seleccione...</option>
                                                                   <?php
                                                                foreach ($listFondoEstatal as $row) {
                                                                    $selected = "";
                                                                    if($editData['fuenteMunicipal_3'] == $row['id']){
                                                                        $selected = "selected";
                                                                    }
 
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . ucwords($row['fondo']) . "</option>\n";
                                                                }
                                                                ?>
                                                       
                                                        
                                                  </select>                                             
                                                                                      
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Monto de la Fuente:</h5></label>
                                                
                                                <div class="col-sm-4">     
                                                    <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                                                 
                                                <input type="text" id="montoOtros_1" name="montoOtros_1"  style="text-align:right"   maxlength="15" class="form-control input-sm t_Otros"  value="<?php echo $editData['montoOtros_1']?>" onblur="sumaMontoOtros();"   ></div>                                        
                                                                                
                                                </div>
                                            </div>


                                  <?php
                                     $style = "style='display:none'";
                                        if($editData['fechaOtros_2']!="" || $editData['montoOtros_2']!="" || $editData['fuenteOtros_2']!=""){
                                            $style = "style='display:block'";

                                        }
                                    ?>

                                 <div id="view_resourceOtros2" <?php echo $style?> >

                                    <hr>

                                      <div class="text-right">  <a href="#otros" onClick="quitRecursoOtros(2)">[-] Eliminar recurso</a></div>


                                            
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 ">Recurso 2:</label>
                                            </div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Fecha asignación:</h5></label>
                                                
                                                 <div class="col-sm-4">    
                                                   <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar otros_2"></i>                    </span>                                    
                                                <input type="text" id="fechaOtros_2" name="fechaOtros_2"  style="text-align:right" class="form-control input-sm "  value="<?php echo $editData['fechaOtros_2']?>"   >
                                                </div>                                        
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Año al que pertenece:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecursoOtros2" name="perteneceAnioRecursoOtros2" class="form-control">
                                                    <?php   
                                                                $select = "";
                                                                if($editData['perteneceAnioRecursoOtros2']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['perteneceAnioRecursoOtros2']=="2018" || $editData['perteneceAnioRecursoEstatal3']=="" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['perteneceAnioRecursoOtros2']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 "><h5>Fuente de Financiamiento:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <input type="text" id="fuenteFinanciamOtros_2" name="fuenteFinanciamOtros_2"  value="<?php echo $editData['fuenteOtros_2']?>" class="form-control input-sm"    >                                        
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Monto de la Fuente:</h5></label>
                                                
                                                <div class="col-sm-4">  
                                                    <div class="input-group">
                                                                  <span class="input-group-addon">$</span>                                      
                                                <input type="text" id="montoOtros_2" name="montoOtros_2" style="text-align:right"  maxlength="15"  class="form-control input-sm t_Otros" value="<?php echo $editData['montoOtros_2']?>"  onblur="sumaMontoOtros();"  >                                       </div>
                                                                                
                                                </div>
                                            </div>

                                </div>

                                 <?php
                                     $style = "style='display:none'";
                                        if($editData['fechaOtros_3']!="" || $editData['montoOtros_3']!="" || $editData['fuenteOtros_3']!=""){
                                            $style = "style='display:block'";

                                        }
                                    ?>

                                <div id="view_resourceOtros3" <?php echo  $style ?>>

                                        <hr>

                                          <div class="text-right">  <a href="#otros" onClick="quitRecursoOtros(3)">[-] Eliminar recurso</a></div>


                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4 ">Recurso 3:</label>
                                            </div>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Fecha asignación:</h5></label>
                                                
                                                 <div class="col-sm-4">  
                                                  <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar otros_3"></i>                    </span>                                      
                                                <input type="text" id="fechaOtros_3" name="fechaOtros_3" value="<?php echo $editData['fechaOtros_3']?>" style="text-align:right" class="form-control input-sm "    >                                        
                                                            </div>                    
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Año al que pertenece:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <select id="perteneceAnioRecursoOtros3" name="perteneceAnioRecursoOtros3" class="form-control">
                                                     <?php   
                                                                $select = "";
                                                                if($editData['perteneceAnioRecursoOtros3']=="2017"){
                                                                    $select="selected";
                                                                }

                                                                 ?>
                                                                <option <?php echo $select ?>>2017</option>
                                                                <?php
                                                                $select="";
                                                                if($editData['perteneceAnioRecursoOtros3']=="2018" || $editData['perteneceAnioRecursoEstatal3']=="" ){
                                                                    $select="selected";
                                                                }

                                                                 ?>

                                                                <option <?php echo $select ?>>2018</option>

                                                                 <?php
                                                                 $select="";
                                                                if($editData['perteneceAnioRecursoOtros3']=="2019" ){
                                                                    $select="selected";
                                                                }

                                                        ?>
                                                                <option <?php echo $select ?>>2019</option>
                                                </select>

                                                                                
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Fuente de Financiamiento:</h5></label>
                                                
                                                <div class="col-sm-4">                                        
                                                <input type="text" id="fuenteFinanciamOtros_3" name="fuenteFinanciamOtros_3"  class="form-control input-sm"   value="<?php echo $editData['fuenteOtros_3']?>"   >                                        
                                                                                
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label for="textInput" class="col-sm-4"><h5>Monto de la Fuente:</h5></label>
                                                
                                                <div class="col-sm-4">     
                                                  <div class="input-group">
                                                                  <span class="input-group-addon">$</span>                                   
                                                <input type="text" id="montoOtros_3" name="montoOtros_3"    style="text-align:right"  maxlength="15" class="form-control input-sm t_Otros"  value="<?php echo $editData['montoOtros_3']?>"   onblur="sumaMontoOtros();"     >                                    </div>
                                                                                
                                                </div>
                                            </div>
                                </div>

                                            <hr >
                                              <div class="form-group">
                                                <label for="textInput" class="col-sm-4"> Total del Recurso Otros:</label>
                                                
                                                <div class="col-sm-4"> 
                                                   <div class="input-group">
                                                                  <span class="input-group-addon">$</span>                                                                          
                                                <input type="text" id="totalOtros" name="totalOtros"  style="text-align:right" disabled class="form-control input-sm"  value="<?php echo $editData["resumen_total_otros"] ?>"    >                                        
                                                                                </div>
                                                </div>
                                            </div>
                                        

                                      
                                  <input type="hidden" name="id_ffinanciamiento_recursoOtros" id="id_ffinanciamiento_recursoOtros" value="<?=$editData['id_ffinanciamiento']?>">


                                          
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->
                        </div>
                    </div>




                            <!-- Inline Form Example -->


                <div class="col-lg-12"  style="display:none" >

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4 class="text-center">CONVENIO MODIFICATORIO</h4> 
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#federal estatal"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                          
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>



                 <div class="col-lg-6" style="display:none">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                           <A name="modificadora"> <h4>Convenio Modificatorio</h4> </A>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#modificadora"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="modificadora" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_material_distribuir">
                                                


                                                

                                              
                                                <div class="text-right">  <a href="#modificadora" onClick="addModificadora()">[+] Agregar Nueva Modificadora</a></div>
                                            

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   ">Modificadora 1:</label>
                                            </div>
                                               
                                                   
                                              

                                                
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Documento:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento">
                                                            <option value="">Seleccione..</option>
                                                            <option value="1">Convenio</option>
                                                            <option value="2">Contrato</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                                          <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Número Documento:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="numDocumento" name="numDocumento"  >
                                                    </div>



                                             
                                                    <label for="textInput" class="col-sm-3  ">Fecha Firma:</label>
                                                    <div class="col-sm-3 ">

                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar firma1"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaFirma1" name="fechaFirma1"  >
                                                    </div>
                                                    </div>
                                              
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-3 ">Adjuntar convenio:</label>
                                                    <div class="col-sm-6">
                                                        <input type="file">
                                                    </div>
                                                </div> 

                                                      <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Modificación:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento" onChange="selectModulo1(this.value)">
                                                            <option value="">Seleccione..</option>
                                                            <option value="fecha">Fecha</option>
                                                            <option value="monto">Monto</option>
                                                            <option value="alcance">Alcance</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                     
                                              <div class="form-group" style="display:none" id="modulo_fecha1">
                                                    <label for="textInput" class="col-sm-3  ">Fecha Inicio:</label>
                                                    <div class="col-sm-3" ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar inicio1"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaInicio1" name="fechaInicio1"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Fecha Modificada:</label>
                                                    <div class="col-sm-3 ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar modificada1"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaModificada1" name="fechaModificada"  >
                                                    </div>
                                                    </div>
                                                </div>


                                          
                                      
                                     
                                           
                                                <div class="form-group" style="display:none" id="modulo_monto1">
                                                    <label for="textInput" class="col-sm-3  ">Monto Original:</label>
                                                    <div class="col-sm-3" ">
                                                           <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoOriginal" name="montoOriginal"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Monto Modificado:</label>
                                                    <div class="col-sm-3" ">
                                                            <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoModificado" name="montoModificado"  ></div>
                                                    </div>
                                                </div>

                                                <div style="display:none" id="modulo_alcance1">
                                               <div class="form-group" >
                                                    <label for="textInput" class="col-sm-3  ">Alcance Original:</label>
                                                    <div class="col-sm-9" ">
                                                          <textarea   class="form-control" rows="3" id="alcanceOriginal" name="alcanceOriginal">
                                                              
                                                          </textarea>
                                                    </div>

                                                    
                                                </div>

                                                   <div class="form-group">
                                                   

                                                    <label for="textInput" class="col-sm-3  ">Alcance Modificado:</label>
                                                    <div class="col-sm-9" ">
                                                         <textarea   class="form-control" rows="3" id="alcanceModificado" name="alcanceModificado">
                                                               
                                                          </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                                 
                                                 


                                            <div id="view_modificadora2" style="display:none">
                                            <hr>



  
                                                 <div class="text-right">  <a href="#otros" onClick="quitModficadora(2)">[-] Eliminar recurso Modificatorio</a></div>

                                            

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   ">Modificadora 2:</label>
                                            </div>
                                               
                                                   
                                              

                                                
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Documento:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento">
                                                            <option value="">Seleccione..</option>
                                                            <option value="1">Convenio</option>
                                                            <option value="2">Contrato</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                                          <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Número Documento:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="numDocumento" name="numDocumento"  >
                                                    </div>



                                             
                                                    <label for="textInput" class="col-sm-3  ">Fecha Firma:</label>
                                                    <div class="col-sm-3 ">

                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar firma2"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaFirma2" name="fechaFirma"  >
                                                    </div>
                                                    </div>
                                              
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-3 ">Adjuntar convenio:</label>
                                                    <div class="col-sm-6">
                                                        <input type="file">
                                                    </div>
                                                </div> 

                                                      <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Modificación:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento" onChange="selectModulo2(this.value)">
                                                            <option value="">Seleccione..</option>
                                                            <option value="fecha">Fecha</option>
                                                            <option value="monto">Monto</option>
                                                            <option value="alcance">Alcance</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                     
                                              <div class="form-group" style="display:none" id="modulo_fecha2">
                                                    <label for="textInput" class="col-sm-3  ">Fecha Inicio:</label>
                                                    <div class="col-sm-3" ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar inicio2"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaInicio2" name="fechaInicio2"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Fecha Modificada:</label>
                                                    <div class="col-sm-3 ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar modificada2"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaModificada2" name="fechaModificada2"  >
                                                    </div>
                                                    </div>
                                                </div>


                                          
                                      
                                     
                                           
                                                <div class="form-group" style="display:none" id="modulo_monto2">
                                                    <label for="textInput" class="col-sm-3  ">Monto Original:</label>
                                                    <div class="col-sm-3" ">
                                                           <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoOriginal" name="montoOriginal"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Monto Modificado:</label>
                                                    <div class="col-sm-3" ">
                                                            <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoModificado" name="montoModificado"  ></div>
                                                    </div>
                                                </div>

                                                <div style="display:none" id="modulo_alcance2">
                                               <div class="form-group" >
                                                    <label for="textInput" class="col-sm-3  ">Alcance Original:</label>
                                                    <div class="col-sm-9" ">
                                                          <textarea   class="form-control" rows="3" id="alcanceOriginal" name="alcanceOriginal">
                                                              
                                                          </textarea>
                                                    </div>

                                                    
                                                </div>

                                                   <div class="form-group">
                                                   

                                                    <label for="textInput" class="col-sm-3  ">Alcance Modificado:</label>
                                                    <div class="col-sm-9" ">
                                                         <textarea   class="form-control" rows="3" id="alcanceModificado" name="alcanceModificado">
                                                               
                                                          </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                                 





























                                            </div>





                                              <div id="view_modificadora3" style="display:none">
                                            <hr>



  
                                                 <div class="text-right">  <a href="#otros" onClick="quitModificadora(3)">[-] Eliminar recurso Modficiatorio</a></div>

                                            

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-4   ">Modificadora 3:</label>
                                            </div>
                                               
                                                   
                                              

                                                
                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Documento:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento">
                                                            <option value="">Seleccione..</option>
                                                            <option value="1">Convenio</option>
                                                            <option value="2">Contrato</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                                          <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Número Documento:</label>
                                                    <div class="col-sm-3" ">
                                                        <input type="text" class="form-control input-sm" id="numDocumento" name="numDocumento"  >
                                                    </div>



                                             
                                                    <label for="textInput" class="col-sm-3  ">Fecha Firma:</label>
                                                    <div class="col-sm-3 ">

                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar firma3"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaFirma3" name="fechaFirma3"  >
                                                    </div>
                                                    </div>
                                              
                                                </div>


                                                <div class="form-group">
                                                    <label  class="col-sm-3 ">Adjuntar convenio:</label>
                                                    <div class="col-sm-6">
                                                        <input type="file">
                                                    </div>
                                                </div> 

                                                      <div class="form-group">
                                                    <label for="textInput" class="col-sm-3  ">Modificación:</label>
                                                    <div class="col-sm-3" ">
                                                   
                                                        <select id="tipoDocumento" class="form-control" name="tipoDocumento" onChange="selectModulo3(this.value)">
                                                            <option value="">Seleccione..</option>
                                                            <option value="fecha">Fecha</option>
                                                            <option value="monto">Monto</option>
                                                            <option value="alcance">Alcance</option>
                                                        </select>
                                                    </div>

                                                   
                                                </div>


                                     
                                              <div class="form-group" style="display:none" id="modulo_fecha3">
                                                    <label for="textInput" class="col-sm-3  ">Fecha Inicio:</label>
                                                    <div class="col-sm-3" ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar inicio3"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaInicio3" name="fechaInicio3"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Fecha Modificada:</label>
                                                    <div class="col-sm-3 ">
                                                         <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar modificada3"></i>
                                                    </span> 
                                                        <input type="text" class="form-control input-sm" id="fechaModificada3" name="fechaModificada3"  >
                                                    </div>
                                                    </div>
                                                </div>


                                          
                                      
                                     
                                           
                                                <div class="form-group" style="display:none" id="modulo_monto3">
                                                    <label for="textInput" class="col-sm-3  ">Monto Original:</label>
                                                    <div class="col-sm-3" ">
                                                           <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoOriginal" name="montoOriginal"  >
                                                    </div>
                                                    </div>

                                                    <label for="textInput" class="col-sm-3  ">Monto Modificado:</label>
                                                    <div class="col-sm-3" ">
                                                            <div class="input-group">
                                                                  <span class="input-group-addon">$</span>
                                                        <input type="text" class="form-control input-sm"  style="text-align:right" id="montoModificado" name="montoModificado"  ></div>
                                                    </div>
                                                </div>

                                                <div style="display:none" id="modulo_alcance3">
                                               <div class="form-group" >
                                                    <label for="textInput" class="col-sm-3  ">Alcance Original:</label>
                                                    <div class="col-sm-9" ">
                                                          <textarea   class="form-control" rows="3" id="alcanceOriginal" name="alcanceOriginal">
                                                              
                                                          </textarea>
                                                    </div>

                                                    
                                                </div>

                                                   <div class="form-group">
                                                   

                                                    <label for="textInput" class="col-sm-3  ">Alcance Modificado:</label>
                                                    <div class="col-sm-9" ">
                                                         <textarea   class="form-control" rows="3" id="alcanceModificado" name="alcanceModificado">
                                                               
                                                          </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                                 





























                                            </div>
 
 


                                               
                                            

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    
                <!-- /.row -->
                <!-- end MAIN PAGE ROW -->
                
     

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.number.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
 
 
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script type="text/javascript" src="<?php echo asset_url();?>js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="<?php echo asset_url();?>css/bootstrap-multiselect.css" type="text/css"/>
    
    <link href="<?php echo asset_url();?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <script src="<?php echo asset_url();?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    



    
    <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?>     


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>



    <script type="text/javascript">

        var v_esquema = 0;

        var es_federal = 0;

        function addRecourse(){
            var cadenaHTML;
            var contador = 0;

            if($("#countResourceFF").val()<=2){

                contador = parseInt($("#countResourceFF").val()) + 1;

            }

             var label = "view_resource"+contador;
             
             $("#"+label).show("slow"); 
             
            $("#countResourceFF").val(contador);
        }



         function selectModulo1(valor){
            if(valor == "fecha"){
                $("#modulo_fecha1").show("slow");
                 $("#modulo_monto1").hide("");
                  $("#modulo_alcance1").hide("");

            }

             if(valor == "monto"){
                $("#modulo_monto1").show("slow");
                 $("#modulo_fecha1").hide("");
                  $("#modulo_alcance1").hide("");
            }

             if(valor == "alcance"){
                $("#modulo_alcance1").show("slow");
                 $("#modulo_monto1").hide("");
                  $("#modulo_fecha1").hide("");

            }
         }

           function selectModulo2(valor){
            if(valor == "fecha"){
                $("#modulo_fecha2").show("slow");
                 $("#modulo_monto2").hide("");
                  $("#modulo_alcance2").hide("");

            }

             if(valor == "monto"){
                $("#modulo_monto2").show("slow");
                 $("#modulo_fecha2").hide("");
                  $("#modulo_alcance2").hide("");
            }

             if(valor == "alcance"){
                $("#modulo_alcance2").show("slow");
                 $("#modulo_monto2").hide("");
                  $("#modulo_fecha2").hide("");

            }
         }


        function selectModulo3(valor){
            if(valor == "fecha"){
                $("#modulo_fecha3").show("slow");
                 $("#modulo_monto3").hide("");
                  $("#modulo_alcance3").hide("");

            }

             if(valor == "monto"){
                $("#modulo_monto3").show("slow");
                 $("#modulo_fecha3").hide("");
                  $("#modulo_alcance3").hide("");
            }

             if(valor == "alcance"){
                $("#modulo_alcance3").show("slow");
                 $("#modulo_monto3").hide("");
                  $("#modulo_fecha3").hide("");

            }
         }

         function addModificadora(){
            var cadenaHTML;
            var contador;

            if($("#countModificadora").val()<=2){

                contador = parseInt($("#countModificadora").val()) + 1;

            }

             var label = "view_modificadora"+contador;
             
             $("#"+label).show("slow"); 
             
            $("#countModificadora").val(contador);
        }
        


        function addRecourseEstatal(){
            var cadenaHTML;
            var contador = 0;

            
            if($("#countResourceFFEstatal").val()<=2){
             
             contador = parseInt($("#countResourceFFEstatal").val()) + 1;
            }  

             var label = "view_resourceEstatal"+contador;
             

             $("#"+label).show("slow");


             
             
            $("#countResourceFFEstatal").val(contador);
        }

        function addRecourseMunicipal(){
            var cadenaHTML;
            var contador = 0;

            if($("#countResourceFFMunicipal").val()<=2){
             contador = parseInt($("#countResourceFFMunicipal").val()) + 1;
            }

             var label = "view_resourceMunicipal"+contador;
      
             

             $("#"+label).show("slow");


             
             
            $("#countResourceFFMunicipal").val(contador);
        }



        function addRecourseOtros(){
            var cadenaHTML;
            var contador = 0;

            if($("#countResourceFFOtros").val()<=2){
             contador = parseInt($("#countResourceFFOtros").val()) + 1;
            }
             var label = "view_resourceOtros"+contador;      
             

             $("#"+label).show("slow");

             
             
            $("#countResourceFFOtros").val(contador);
        }



        $(document).ready(function () {

      

         $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */


        //  preGuardado();

               $('#list_ramo').multiselect({
                       
                        buttonWidth: '550px',
                        //maxHeight: 200,
                        enableCaseInsensitiveFiltering: true,

                    });

        
          <?php 
          if($editData['metodo']=="EDIT" && $editData['onlyView']==1 ){

            echo "deshabilitar()";
          }               
          elseif($editData['metodo']=="EDIT" && $editData['onlyView']==""){

            echo "deshabilitar()";
          } 
          ?>
          

          if($("#programaSeleccionado").val() == "todos"){
            //alert("Debe seleccionar un Programa, antes de guardar Registro.");
            swal("Advertencia!", "Debe seleccionar un Programa, antes de guardar Registro.", "warning");
            $("#btnGuardar").fadeOut("slow");
          }

           // 
    
           <?php

           /* if($editData['onlyView']){
               

               echo "deshabilitar()";

            
            }*/

     
            ?>
   


                
 


 
 

            $("#fechaFederal_1").keypress(function(event) {event.preventDefault();});
            $("#fechaFederal_2").keypress(function(event) {event.preventDefault();});
            $("#fechaFederal_3").keypress(function(event) {event.preventDefault();});

            $("#fechaEstatal_1").keypress(function(event) {event.preventDefault();});
            $("#fechaEstatal_2").keypress(function(event) {event.preventDefault();});
            $("#fechaEstatal_3").keypress(function(event) {event.preventDefault();});
          
            $("#fechaMunicipal_1").keypress(function(event) {event.preventDefault();});
            $("#fechaMunicipal_2").keypress(function(event) {event.preventDefault();});
            $("#fechaMunicipal_3").keypress(function(event) {event.preventDefault();});

            $("#fechaOtros_1").keypress(function(event) {event.preventDefault();});
            $("#fechaOtros_2").keypress(function(event) {event.preventDefault();});
            $("#fechaOtros_3").keypress(function(event) {event.preventDefault();});
          
          
          


 
           sumaMontoFederal();

             
        
            changeProgram("undefined");

         
           (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                monthsTitle: "Meses",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
            }(jQuery));
             



            

          $( function() {
            $( "#fechaFederal_1" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaFederal_2" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaFederal_3" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });

            $( "#fechaEstatal_1" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEstatal_2" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaEstatal_3" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });

            $( "#fechaMunicipal_1" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaMunicipal_2" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaMunicipal_3" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
 
 

            $( "#fechaOtros_1" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaOtros_2" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaOtros_3" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });



            $( "#fechaInicio1" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaModificada1" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaFirma1" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });


            $( "#fechaInicio2" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaModificada2" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaFirma2" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
           

           $( "#fechaInicio3" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaModificada3" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
            $( "#fechaFirma3" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
           
           
          } );




            //iconos
                $('.federal_1').click(function() {
                    $("#fechaFederal_1").focus();
              });



              $('#fechaFederal_1').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });


                 $('.federal_2').click(function() {
                    $("#fechaFederal_2").focus();
              });

                    $('#fechaFederal_2').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });


                        $('.federal_3').click(function() {
                    $("#fechaFederal_3").focus();
              });


                                    $('#fechaFederal_3').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });






              $('.estatal_1').click(function() {
                    $("#fechaEstatal_1").focus();
              });

                                       $('#fechaEstatal_1').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });




              $('.estatal_2').click(function() {
                    $("#fechaEstatal_2").focus();
              });

                                            $('#fechaEstatal_2').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });




                $('.estatal_3').click(function() {
                    $("#fechaEstatal_3").focus();
              });

                                                $('#fechaEstatal_3').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });




             $('.municipal_1').click(function() {
                    $("#fechaMunicipal_1").focus();
              });

             $('.municipal_2').click(function() {
                    $("#fechaMunicipal_2").focus();
              });


            $('#fechaMunicipal_1').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });

               $('#fechaMunicipal_2').on('changeDate', function(ev){
                    $(this).datepicker('hide');
            });

            

              $('.municipal_3').click(function() {
                    $("#fechaMunicipal_3").focus();
              });



              $('#fechaMunicipal_3').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });

            


              $('.otros_1').click(function() {
                    $("#fechaOtros_1").focus();
              });


              $('#fechaOtros_1').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });





                $('.otros_2').click(function() {
                    $("#fechaOtros_2").focus();
              });


              $('#fechaOtros_2').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });



                  $('.otros_3').click(function() {
                    $("#fechaOtros_3").focus();
              });


                        $('#fechaOtros_3').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });





                  $('.firma1').click(function() {
                        $("#fechaFirma1").focus();
                  });

                




                $('#fechaFirma1').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });


                $('#fechaFirma2').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });




                $('#fechaFirma3').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });







              $('.firma2').click(function() {
                    $("#fechaFirma2").focus();
              });

              $('.firma3').click(function() {
                    $("#fechaFirma3").focus();
              });


                     $('.inicio1').click(function() {
                    $("#fechaInicio1").focus();
              });

                   $('.modificada1').click(function() {
                    $("#fechaModificada1").focus();
              });





                $('#fechaInicio1').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });



                $('#fechaModificada1').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });



               $('.inicio2').click(function() {
                    $("#fechaInicio2").focus();
              });



                   $('#fechaInicio2').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });



                $('#fechaModificada2').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });



                   $('.modificada2').click(function() {
                    $("#fechaModificada2").focus();
              });
              


                     $('.inicio3').click(function() {
                    $("#fechaInicio3").focus();
              });


                   $('.modificada3').click(function() {
                    $("#fechaModificada3").focus();
              });


                 $('#fechaInicio3').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });



                $('#fechaModificada3').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });

                   






           
         



            $("#catalogResources").change(function () {

                var resourceSelected = $("#catalogResources option:selected").val();     

                populate_programs(resourceSelected);

            });

            function populate_programs(fromResource) {
             
                var cantElement = 0;
                $('#catalogPrograms').empty();
                $('#catalogPrograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_programs') ?>/" + fromResource,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogPrograms').empty();
                        $('#catalogPrograms').append("<option>Selecciona Programa..</option>");


                        $.each(data, function (i, name) {
                            $('#catalogPrograms').append('<option value="' + data[i].id + '">' + data[i].programa + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   


      
 

            $("#catalogPrograms").change(function () {

                var programSelected = $("#catalogPrograms option:selected").val();   
                populate_subprograms(programSelected);

            });





            $( "#programaSeleccionado" ).change(function() {
 

               var resourceSelected = $("#recurso_sel option:selected").val();     
               
                if(resourceSelected == 2){
                    $("#presupuesto_pef").attr("disabled", "true");
                    $("#instancia_prom").html("Instancia Promotora:");
                }else{
                    $("#presupuesto_pef").removeAttr( "disabled" );
                     $("#instancia_prom").html("Normativa ejecutora:");
                }
                
                
});         
 

 

 
     

           //  $('#catalogSubprograms').removeAttr("disabled")

           function populate_subprograms(fromProgram) {
                var cantElement = 0;
                $('#catalogSubprograms').empty();
                $('#catalogSubprograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_subprograms') ?>/" + fromProgram,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogSubprograms').empty();
                        $('#catalogSubprograms').append("<option>Selecciona subprograma..</option>");
        

                        $.each(data, function (i, name) {
                          //  $('#catalogSubprograms').removeAttr("disabled")
                            $('#catalogSubprograms').append('<option value="' + data[i].id + '">' + data[i].subprograma + '</option>');
                            cantElement++;
                        });                         

                        if(cantElement==0){
                            $('#catalogSubprograms').attr('disabled', 'disabled');
                        }
                    }
                });
            }



            $('#vigila_federal').numeric(",");
            $('#vigila_estatal').numeric(",");
            $('#vigila_municipal').numeric(",");
            $('#vigila_otros').numeric(",");
            $('#total_acciones').numeric();
           

            $('#presupuesto_pef').number( true, 2 );
            $('#presupuesto_vigilar_cs').number( true, 2 );

            $('#montoFederal_1').number( true, 2 );
            $('#montoFederal_2').number( true, 2 );
            $('#montoFederal_3').number( true, 2 );

            $('#montoEstatal_1').number( true, 2 );
            $('#montoEstatal_2').number( true, 2 );
            $('#montoEstatal_3').number( true, 2 );

            $('#montoMunicipal_1').number( true, 2 );
            $('#montoMunicipal_2').number( true, 2 );
            $('#montoMunicipal_3').number( true, 2 );


            $('#montoOtros_1').number( true, 2 );
            $('#montoOtros_2').number( true, 2 );
            $('#montoOtros_3').number( true, 2 );

            $('#totalFederal').number( true, 2 );
            $('#total_recursoFederal').number( true, 2 );


            $('#totalEstatal').number( true, 2 );
            $('#total_recursoEstatal').number( true, 2 );

            $('#total_recursoMunicipal').number( true, 2 );
            $('#totalMunicipal').number( true, 2 );

            $('#total_recursoOtros').number( true, 2 );
            $('#totalOtros').number( true, 2 );





            $('#total_recursosasignados').number( true, 2 );





            
            

          }); 
        

        function activarAdjuntoConvenio(elem){

            if(elem=="SI"){

                $("#viewAdjuntoConvenio").show("slow");
            }else{
                $("#viewAdjuntoConvenio").hide("slow");

                eliminarFile(elem,alt,seccion,objeto);
            }
        }

 


        function resetearForma(){

           
            $(".form-control").each(function() {
               $(this).val("");  
            });
        }


        function formatDecimal(input) {
            var val = '' + (+input.value);
            if (val) {
                val = val.split('\.');
                var out = val[0];
                while (out.length < 3) {
                    out = '0' + out;
                }
                if (val[1]) {
                    out = out + '.' + val[1]
                    if (out.length < 6) out = out + '0';
                } else {
                    out = out + '.00';
                }
                input.value = out;
            } else {
                input.value = '000.00';
            }
        }


    function addCommas(obj){

            x = obj.value;

            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            obj.value = parts.join(".");

            
    }   


        function permite(elEvento, permitidos) {
          // Variables que definen los caracteres permitidos
          var numeros = "0123456789";
          var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
          var numeros_caracteres = numeros + caracteres;
          var teclas_especiales = [8, 37, 39, 46];
          // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha
         
         
          // Seleccionar los caracteres a partir del parámetro de la función
          switch(permitidos) {
            case 'num':
              permitidos = numeros;
              break;
            case 'car':
              permitidos = caracteres;
              break;
            case 'num_car':
              permitidos = numeros_caracteres;
              break;
          }
         
          // Obtener la tecla pulsada 
          var evento = elEvento || window.event;
          var codigoCaracter = evento.charCode || evento.keyCode;
          var caracter = String.fromCharCode(codigoCaracter);
         
          // Comprobar si la tecla pulsada es alguna de las teclas especiales
          // (teclas de borrado y flechas horizontales)
          var tecla_especial = false;
          for(var i in teclas_especiales) {
            if(codigoCaracter == teclas_especiales[i]) {
              tecla_especial = true;
              break;
            }
          }
         
          // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
          // o si es una tecla especial
          return permitidos.indexOf(caracter) != -1 || tecla_especial;
        }


        function sumaVigilado(){

            var total = 0;

                $(".t_vigilado").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

            $("#total_vigilado").val(total);

        }


        function sumaMontoFederal(){



            var total = 0;

                $(".t_federal").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

                x = total;

                  $("#total_recursoFederal").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalFederal").val(total);
          
            sumaTotales();

        }


        function sumaMontoEstatal(){

             var total = 0;

             $(".t_estatal").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

                x = total;

                  $("#total_recursoEstatal").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalEstatal").val(total);
          
         

           
           

           sumaTotales();



        }


        

        function sumaMontoMunicipal(){

 
            var total = 0;

                $(".t_Municipal").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

                x = total;

                  $("#total_recursoMunicipal").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalMunicipal").val(total);
          
            sumaTotales();

        }


        function sumaMontoOtros(){


             var total = 0;

                $(".t_Otros").each(function() {


                   if (this.value!="") {
            
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
        

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
              
                });

                x = total;

                  $("#total_recursoOtros").val(total);

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");



            $("#totalOtros").val(total);
          
            sumaTotales();

 

        }


      function sumaTotales(){
 
            var total = 0;

            /*    $(".t_totales").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

            $("#total_recursosasignados").val(total);*/



                $(".t_totales").each(function() {


                   if (this.value!="") {
                        //total += parseFloat(this.value);
                        //alert(this.value);
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
                        //if(parseFloat(cantidadString.replace(',',''))!=='NaN'){
                      // alert("numeroasumar:" + parseFloat(cantidadString.replace(/,/g , "")));

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });

          

                  $("#total_recursosasignados").val(total);

        
          
             

        }




        function sumaPeople(){

            var total = 0;

             $(".t_people").each(function() {


                   if (this.value!="") {
                        //total += parseFloat(this.value);
                        //alert(this.value);
                        var cantidad = this.value;
                        var cantidadString = cantidad.toString();
                        //if(parseFloat(cantidadString.replace(',',''))!=='NaN'){
                      // alert("numeroasumar:" + parseFloat(cantidadString.replace(/,/g , "")));

                        total += parseFloat(cantidadString.replace(/,/g , ""));
                    }
               // }
                });


                x = total;

               var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = parts.join(".");
          

                  $("#total_people").val(total);

              

        }

        





        function jsUcfirst(string) 
        {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }   


        function cleanElement(){

            swal({
              title: "Estas seguro de Crear nuevo Registro?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: false,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Listo!, Empezamos de nuevo la captura", {
                  icon: "success",
                });
                resetearForma();

              } else {
                swal("Lo dejamos como estaba..");
              }
            });
        }


         function deleteElement(){

            swal({
              title: "Estas seguro de querer Eliminar?",
              text: "Una vez eliminado, No podras recuperar. ",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Se ha borrado exitosamente.", {
                  icon: "success",
                });
              } else {
                swal("Ok. Lo dejamos como estaba..");
              }
            });
        }


        var msgError = ""

         function validaCamposFfinanciamiento(){


            msgError = "";

                    $("#instanciaEjecutora").css({"background-color": "white"});
             
                    $("#ejercicio_fiscal").css({"background-color": "white"});  

                           $("#presupuesto_vigilar_cs").css({"background-color": "white"});
                           $("#descripcionPoblacion").css({"background-color": "white"});
                           $("#presupuesto_pef").css({"background-color": "white"});
                           $("#total_people").css({"background-color": "white"});
                           $("#catalogConvenio").css({"background-color": "white"});
                           $("#total_recursosasignados").css({"background-color": "white"});



                if($("#ejercicio_fiscal option:selected").val()==0){
                    msgError+= "- Debe seleccionar Ejercicio Fiscal.  \n ";
                    $("#ejercicio_fiscal").css({"background-color": "yellow"});
                }


                    if($("#catalogConvenio option:selected").val()=="SI"){

                        if($("#archivoCargado").val()==0){
                            msgError+= "- Debe cargar un Adjunto para el Convenio.  \n ";
                           // $("#ejercicio_fiscal").css({"background-color": "yellow"});
                        } 
                }
               

                if( $("#presupuesto_pef").val() == "" && es_federal==1){
                    msgError+= "- Presupuesto Autorizado en el PEF. \n";
                    $("#presupuesto_pef").css({"background-color": "yellow"});
                }


                 if( $("#presupuesto_vigilar_cs").val() == ""){
                    msgError+= "- Presupuesto a Vigilar por la CS. \n";
                    $("#presupuesto_vigilar_cs").css({"background-color": "yellow"});
                }

              

                if(es_federal==1 && (parseFloat($("#presupuesto_pef").val()) < parseFloat($("#presupuesto_vigilar_cs").val()) )){

                   msgError+= "- Presupuesto a Vigilar por la CS no puede ser mayor a Presupuesto Autorizado en el PEF. \n";
                     $("#presupuesto_pef").css({"background-color": "yellow"});
                      $("#presupuesto_vigilar_cs").css({"background-color": "yellow"});
                }

                
                
                  if( $("#descripcionPoblacion").val() == ""){
                    msgError+= "- Descripcion de la Población Objetivo. \n";
                    $("#descripcionPoblacion").css({"background-color": "yellow"});
                }

                  if($("#catalogConvenio option:selected").val()==0){
                    msgError+= "- Debe seleccionar Convenio Suscrito. \n";
                    $("#catalogConvenio").css({"background-color": "yellow"});
                }

                

                if( $("#total_people").val() == "" || $("#total_people").val() == 0 ){
                    msgError+= "- Debe tener el Total de Población Beneficiada. \n";
                    $("#total_people").css({"background-color": "yellow"});

                }


               if( $("#total_recursosasignados").val() == "" || $("#total_recursosasignados").val() == 0 || $("#total_recursosasignados").val() == "0.00"){
                    msgError+= "- Debe tener minimo un Total de Recurso. \n";
                    $("#total_recursosasignados").css({"background-color": "yellow"});

                }


            

              
/*

                var formInvalid = false;
                var algunValor = 0;
                  $('#form_recursoFederal input').each(function() {
                    if ($(this).val() === '') {
                      formInvalid = true;
                    }
                    if ($(this).val() !== '') {
                      algunValor = 1;
                    }
                  });

                  if(algunValor == 1){
                  if (formInvalid){

                     $('#form_recursoFederal input').each(function() {
                     $(this).css({"background-color": "yellow"});
                   
                  
                  });

                    alert('One or Two fields are empty. Please fill up all fields');
                    }
                  }
                */
          


                
                //alert($('input:checkbox:checked').length;
                //alert($('input[name="funcionesComite[]"]:checked').length);

                
               


        } 



        function preGuardado(){



                var resourceSelected;
                var programSelected;
                var subProgramSelected;
                var ejercirseFiscal; 


                resourceSelected = $('#catalogResources option:selected').val();
                programSelected =  $('#catalogPrograms option:selected').val();
                subProgramSelected =  $('#catalogSubprograms option:selected').val();
                ejercirseFiscal = $('#ejercicio_fiscal').val();


    
                //Validar campos antes de mandar 
                $("#resourceSelected").val(resourceSelected);
                $("#programSelected").val(programSelected);
                $("#subProgramSelected").val(subProgramSelected);
                $("#exercirseFiscal").val(ejercirseFiscal);


               

                $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarFFinanciamiento') ?>",
                    type: "POST",
                    data: $('#form_ffinanciamiento').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                       // var status, message;
                       // $.each(data, function (index, obj) {
                            status = data.status;
                            message = data.msg;
 

                 
                        
                        $("#id_ffinanciamiento").val(message);
                        $("#id_ffinanciamiento_beneficios").val(message);
                        $("#id_ffinanciamiento_convenio").val(message);
                        $("#id_ffinanciamiento_ramo").val(message);
                       
                        $("#id_ffinanciamiento_recursoFederal").val(message);
                        $("#id_ffinanciamiento_recursoEstatal").val(message);
                        $("#id_ffinanciamiento_recursoMunicipal").val(message);
                        $("#id_ffinanciamiento_recursoOtros").val(message);
                        $("#id_ffinanciamiento_resumenA").val(message);
                        
 
                    }
                });


        }



        function saveElement(){ 



                validaCamposFfinanciamiento();


                
                 if(msgError!=""){
                    swal("Error!", msgError, "error");
                    return;
                }
                
               // alert($("#id_ffinanciamiento_convenio").val());


                preGuardado();
                 
                      //   $("#metodo").val("edit");
 

                  var totalBeneficios;

                  totalBeneficios = $("#total_people").val();

                    //$("#idRegistro_1").val(idRegistro);
                    $("#totalBeneficios").val(totalBeneficios);
                     
         


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarBeneficios') ?>",
                    type: "POST",
                    data: $('#form_beneficios').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                        
                    }
                });




                var totalBeneficios;

                totalBeneficios = $("#total_people").val();
     

                var convenioSelect;
                var instanciaPromotora;

                

                
                
                convenioSelect = $('#catalogConvenio option:selected').val();
                instanciaPromotora = $('#cataloInstanciaPromotora option:selected').val();
                instanciaAbajo = $('#cataloInstanciaAbajo option:selected').val();

                $("#catalogConvenioSelected").val(convenioSelect);
                $("#instanciaPromotoraSelected").val(instanciaPromotora);
                $("#instanciaAbajoSelected").val(instanciaAbajo);

                      


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarConvenio') ?>",
                    type: "POST",
                    data: $('#form_convenio').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                       
                    }
                });

                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRamo') ?>",
                    type: "POST",
                    data: $('#form_ramo').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                       
                    }
                });



                var totalBeneficios;

                totalBeneficios = $("#total_people").val();
     


                //Federal
                var anioRecurso1_Select;
                var anioRecurso2_Select;
                var anioRecurso3_Select;
                 

                
                 
                
                anioRecurso1_Select = $('#perteneceAnioRecurso1 option:selected').val();
                anioRecurso2_Select = $('#perteneceAnioRecurso2 option:selected').val();
                anioRecurso3_Select = $('#perteneceAnioRecurso3 option:selected').val();
               
                $("#perteneceAnioRecurso1_selected").val(anioRecurso1_Select);
                $("#perteneceAnioRecurso2_selected").val(anioRecurso2_Select);
                $("#perteneceAnioRecurso3_selected").val(anioRecurso3_Select);
                
                      


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRecursoFederal') ?>",
                    type: "POST",
                    data: $('#form_recursoFederal').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });



               //Estatal
                var anioRecursoEstatal1_Select;
                var anioRecursoEstatal2_Select;
                var anioRecursoEstatal3_Select;
                 

                  
                anioRecursoEstatal1_Select = $('#perteneceAnioRecursoEstatal1 option:selected').val();
                anioRecursoEstatal2_Select = $('#perteneceAnioRecursoEstatal2 option:selected').val();
                anioRecursoEstatal3_Select = $('#perteneceAnioRecursoEstatal3 option:selected').val();
               
                $("#perteneceAnioRecursoEstatal_selected_1").val(anioRecursoEstatal1_Select);
                $("#perteneceAnioRecursoEstatal_selected_2").val(anioRecursoEstatal2_Select);
                $("#perteneceAnioRecursoEstatal_selected_3").val(anioRecursoEstatal3_Select);
                
                      


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRecursoEstatal') ?>",
                    type: "POST",
                    data: $('#form_recursoEstatal').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });



                   
                      


                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRecursoMunicipal') ?>",
                    type: "POST",
                    data: $('#form_recursoMunicipal').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });


                 //Otros
                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarRecursoOtros') ?>",
                    type: "POST",
                    data: $('#form_recursoOtros').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });






                //Resumen
                $("#totalFederalRA").val($("#total_recursoFederal").val());
                $("#totalMunicipalRA").val($("#total_recursoMunicipal").val());
                $("#totalEstatalRA").val($("#total_recursoEstatal").val());
                $("#totalOtrosRA").val($("#total_recursoOtros").val());
                $("#totalRecursosRA").val($("#total_recursosasignados").val());

    
                

                 $.ajax({
                    url: "<?php echo site_url('registro_ffinanciamiento/guardarResumenAsignado') ?>",
                    type: "POST",
                    data: $('#form_resumenasignado').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                     
                    }
                });



                if(msgError!=""){
                     swal("Error!", msgError, "error");
                }else{
                    swal("Bien hecho!", "La Fuente de Financiamiento ha sido guardado.", "success");
                }










            }


         // forceNumeric() plug-in implementation
         jQuery.fn.forceNumeric = function () {

             return this.each(function () {
                 $(this).keydown(function (e) {
                     var key = e.which || e.keyCode;

                     if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                     // numbers   
                         key >= 48 && key <= 57 ||
                     // Numeric keypad
                         key >= 96 && key <= 105 ||
                     // comma, period and minus, . on keypad
                        key == 190 || key == 188 || key == 109 || key == 110 ||
                     // Backspace and Tab and Enter
                        key == 8 || key == 9 || key == 13 ||
                     // Home and End
                        key == 35 || key == 36 ||
                     // left and right arrows
                        key == 37 || key == 39 ||
                     // Del and Ins
                        key == 46 || key == 45)
                         return true;

                     return false;
                 });
             });
         }



         $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });



         function quitRecurso(elem){

             var noElem = elem.toString();

             contador = parseInt($("#countResourceFF").val()) - 1;
              $("#countResourceFF").val(contador);

             
              
              $("#fuenteFinanciamFederal_"+noElem).val('');
              $("#fechaFederal_"+noElem).val('');
              $("#montoFederal_"+noElem).val('');
            
            sumaMontoFederal();

            $("#view_resource"+noElem).hide('slow');
         }



        function quitModificadora(elem){

             var noElem = elem.toString();

             contador = parseInt($("#countModificadora").val()) - 1;
              $("#countModificadora").val(contador);

             /*
              
              $("#fuenteFinanciamFederal_"+noElem).val('');
              $("#fechaFederal_"+noElem).val('');
              $("#montoFederal_"+noElem).val('');*/
            
           // sumaMontoFederal();

            $("#view_modificadora"+noElem).hide('slow');
         }




         function quitRecursoEstatal(elem){

             var noElem = elem.toString();

             contador = parseInt($("#countResourceFFEstatal").val()) - 1;
              $("#countResourceFFEstatal").val(contador);

              
             
              $("#fuenteEstatal_"+noElem).val('');
              $("#fechaEstatal_"+noElem).val('');
              $("#montoEstatal_"+noElem).val('');
            
            sumaMontoEstatal();

            $("#view_resourceEstatal"+noElem).hide('slow');
         }

        function quitRecursoMunicipal(elem){

            var noElem = elem.toString();

             contador = parseInt($("#countResourceFFMunicipal").val()) - 1;
              $("#countResourceFFMunicipal").val(contador);

              $("#fuenteMunicipal_"+noElem).val('');
              $("#fechaMunicipal_"+noElem).val('');
              $("#montoMunicipal_"+noElem).val('');
            
            sumaMontoMunicipal();

            $("#view_resourceMunicipal"+noElem).hide('slow');
         }


        function quitRecursoOtros(elem){

             var noElem = elem.toString();

             contador = parseInt($("#countResourceFFOtros").val()) - 1;
              $("#countResourceFFOtros").val(contador);

         
 
             $("#fuenteFinanciamOtros_"+noElem).val('');
              $("#fechaOtros_"+noElem).val('');
              $("#montoOtros_"+noElem).val('');
            
            sumaMontoOtros();

            $("#view_resourceOtros"+noElem).hide('slow');
         }





      function deshabilitar(){

             <?php
            
                  echo  '$("#form_ffinanciamiento :input").prop("disabled", true);';
                  echo  '$("#catalogConvenio :input").prop("disabled", true);';
                  echo  '$("#form_convenio :input").prop("disabled", true);';
                  echo  '$("#form_beneficios :input").prop("disabled", true);';
                  echo  '$("#form_ramo :input").prop("disabled", true);';
                  echo  '$("#form_recursoFederal :input").prop("disabled", true);';
                  echo  '$("#form_recursoEstatal :input").prop("disabled", true);';
                  echo  '$("#form_recursoMunicipal :input").prop("disabled", true);';
                  echo  '$("#form_recursoOtros :input").prop("disabled", true);';
                 /* echo  '$("#form_oficio :input").prop("disabled", true);';
                  echo  '$("#form_beneficios :input").prop("disabled", true);';
               
                  echo  '  $("#form_fechas :input").prop("disabled", true);';
                  echo  '  $("#form_domicilio :input").prop("disabled", true);';
                
                  echo  '  $("#form_asignaFederal :input").prop("disabled", true);';
                  echo  '  $("#form_asignaMunicipal :input").prop("disabled", true);';
                  echo  '  $("#form_asignaEstatal :input").prop("disabled", true);';
                  echo  '  $("#form_asignaOtros :input").prop("disabled", true);';*/
             

            ?>
    }






      function editar(){

        $("#btnGuardar").show("slow");

             <?php
            
                //  echo  '$("#form_ffinanciamiento :input").prop("disabled", false);';
                  echo  '$("#ejercicio_fiscal").prop("disabled", false);';
                  echo  '$("#presupuesto_pef").prop("disabled", false);';
                  echo  '$("#presupuesto_vigilar_cs").prop("disabled", false);';
                  echo  '$("#descripcionPoblacion").prop("disabled", false);';

                  
                  
                  
                  
                  

                  echo  '$("#catalogConvenio :input").prop("disabled", false);';
                  echo  '$("#form_convenio :input").prop("disabled", false);';
                  echo  '$("#form_beneficios :input").prop("disabled", false);';
                  echo  '$("#form_ramo :input").prop("disabled", false);';
                  echo  '$("#form_recursoFederal :input").prop("disabled", false);';
                  echo  '$("#form_recursoEstatal :input").prop("disabled", false);';
                  echo  '$("#form_recursoMunicipal :input").prop("disabled", false);';
                  echo  '$("#form_recursoOtros :input").prop("disabled", false);';
                 /* echo  '$("#form_oficio :input").prop("disabled", true);';
                  echo  '$("#form_beneficios :input").prop("disabled", true);';
               
                  echo  '  $("#form_fechas :input").prop("disabled", true);';
                  echo  '  $("#form_domicilio :input").prop("disabled", true);';
                
                  echo  '  $("#form_asignaFederal :input").prop("disabled", true);';
                  echo  '  $("#form_asignaMunicipal :input").prop("disabled", true);';
                  echo  '  $("#form_asignaEstatal :input").prop("disabled", true);';
                  echo  '  $("#form_asignaOtros :input").prop("disabled", true);';*/
             

            ?>
    }
         


         function  cargarFile(elem,alt,seccion,objeto){
 


     
              var file_data = $('#file'+alt).prop('files')[0];
                        var form_data = new FormData();

                        form_data.set("seccion", "");
                        form_data.set("depto", "");
                        form_data.set("fk_depto", "");
                        form_data.set("programa", "");
                        form_data.set("objeto", "");
                        form_data.append('file', file_data);
                        form_data.append("seccion", seccion);
                        form_data.append("depto", '<?=$departamento?>');
                        form_data.append("fk_depto", '<?=$fk_departamento?>');
                        form_data.append("programa", '<?=$fk_programa?>');
                        form_data.append("objeto", objeto);
         


                $.ajax({
                                 url:'<?php echo base_url();?>index.php/upload/do_upload',
                                 type:"post",
                                 //data:new FormData(elem.form),
                                 data:form_data,
                                 processData:false,
                                 contentType:false,

                                 cache:false,
                                 async:false,
                                  success: function(data){
                                    
                                    
                                    console.log(data);
                                    if(data!=""){
                                      $("#band"+alt).html("Carga Exitosa...");
                                      $("#url"+alt).html("<a target='_blank' href='"+data+"'>Previsualizar</a>");

                                      if(data!=""){
                                        $("#archivoCargado").val(1);
                                      }

                                      }

                               }
                             }); 

          }



        function validarPresupuestoVigilar(){

            var presupuestoVigilar = $("#presupuesto_vigilar_cs").val();
            var totalRecursosasignados = $("#total_recursosasignados").val();

           
             $("#total_recursosasignados").css({"background-color": "white"});
             $("#presupuesto_vigilar_cs").css({"background-color": "white"});
             
            if (presupuestoVigilar > totalRecursosasignados) {
                            alert("Presupuesto a Vigilar por la CS no puede ser mayor al TOTAL de los Recursos Asignados (Federal, Estatal, Municipal y Otros).");
                                $("#presupuesto_vigilar_cs").css({"background-color": "yellow"});
                                $("#total_recursosasignados").css({"background-color": "yellow"});
            }
        }



/*
         function  eliminarFile(elem,alt,seccion,objeto){
 


     
       
                        var form_data = new FormData();

                        form_data.set("seccion", "");
                  
                        form_data.set("fk_depto", "");
                        form_data.set("programa", "");
                        form_data.set("objeto", "");
                
                        form_data.append("seccion", seccion);
                      
                        form_data.append("fk_depto", '<?=$fk_departamento?>');
                        form_data.append("programa", '<?=$fk_programa?>');
                        form_data.append("objeto", objeto);
         


                $.ajax({
                                 url:'<?php echo base_url();?>index.php/upload/delete_atachment',
                                 type:"post",
                                 //data:new FormData(elem.form),
                                 data:form_data,
                                 processData:false,
                                 contentType:false,

                                 cache:false,
                                 async:false,
                                  success: function(data){
                                    
                                    
                                    console.log(data);
                               

                               }
                             }); 

          }*/
 



        function changeProgram(checked){

           

 


            var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
           // $("#programSelected").

             if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");

                    return;
                }



                if(id_programSelected!="todos"){
                  //$("#btnGuardar").fadeIn("slow");
                }
              

               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                     
                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {

                     
                        var status, message;
                        $.each(data, function (index, obj) {
                        //  console.log(obj);
                            status = obj.status;
                            titulo_programa = obj.msg;
                             recurso = obj.recurso;
                             esquema = obj.esquema;
                             noCombinable = obj.noCombinable;
                             cuadroF = obj.cuadroF;
                             cuadroE = obj.cuadroE;
                             cuadroM = obj.cuadroM;
                             cuadroO = obj.cuadroO;

                        });

                    $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                  




                    es_federal = 0;
                    if(recurso == 1){

                            $("#label_pef").show("slow");
              
                   
                    $("#modulo_ramo").show("slow");

                         $("#patcs_menu").hide();
                         $("#petcs_menu").show("slow");
                             $("#documentos_menu").show("slow"); 
                             $( "#navFinanciamiento" ).removeClass( "collapse down" ).addClass( "collapse in" );
                              $( "#navEstatal" ).removeClass( "collapse in" ).addClass( "collapse down" );
                              $( "#navMunicipal" ).removeClass( "collapse in" ).addClass( "collapse down" );
                              $( "#navOtros" ).removeClass( "collapse in" ).addClass( "collapse down" );

                          $("#seguimiento_menu").hide("slow");
                           $("#reuniones_menu").show("slow");
                          es_federal = 1;

                          $("#benefic_hombres").show("slow");
                          $("#benefic_mujeres").show("slow");
                          if(esquema!=1){
                             $("#total_people").attr("disabled", "true");
                          }


                           $("#informes_menu").show("slow");
                             $("#formatos_menu").hide("slow");


                    }


                    
                    v_esquema = 0;
                    band = 1;

                    if(esquema==1){
                             v_esquema = 1;
                     //        band = 0;
                    }
                     
                    

                    /*
                     if(noCombinable)
                     {
                       $("#divFederal").hide();
                       $("#divMunicipal").hide();
                       $("#divOtro").hide();
                     }
                      
                     if(noCombinable==0) {
                       $("#divFederal").show();
                       $("#divMunicipal").show();
                       $("#divOtro").show();
                      
                     }*/

                    // alert("F:"+cuadroF);
  //$("#divEstatal").hide();
 

                   if(cuadroF==1){
                      $("#divFederal").show();
                    } else{
                       $("#divFederal").hide();
                    }
                    
                    if(cuadroE==1){
                     
                      $("#divEstatal").show();
                    }  else{
                      $("#divEstatal").hide();
                    }

                   //  alert("M:"+cuadroM);
                    if(cuadroM==1){
                      $("#divMunicipal").show();
                    } else{
                      $("#divMunicipal").hide();
                    }

 
                    if(cuadroO==1){
                      $("#divOtro").show();
                    } else{
                      $("#divOtro").hide();
                    }



                    if(recurso == 2 || v_esquema == 1){
                  // if(recurso == 2 ){

                      es_federal = 0;
                   // if(v_esquema!=1){
                       $("#presupuesto_pef").prop("disabled", "true");
                    $("#label_pef").hide("slow");
                //  }
                   
                   
                  if(v_esquema!=1){
                      $("#modulo_ramo").hide("slow");
                       $("#benefic_hombres").hide("slow");
                          $("#benefic_mujeres").hide("slow");

                  }



                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow");
                            $("#documentos_menu").show("slow");

                        $("#reuniones_menu").hide("slow");
                        $("#seguimiento_menu").show("slow");

                              if(band){

                              if(v_esquema==1){
                            
                             $( "#navFinanciamiento" ).removeClass( "collapse in" ).addClass( "collapse in" );

                              $( "#navEstatal" ).removeClass( "collapse down" ).addClass( "collapse down" );
                            }else{
                       
                                 $( "#navFinanciamiento" ).removeClass( "collapse in" ).addClass( "collapse down" );

                              $( "#navEstatal" ).removeClass( "collapse down" ).addClass( "collapse in" );
                            }
                              $( "#navMunicipal" ).removeClass( "collapse in" ).addClass( "collapse down" );
                              $( "#navOtros" ).removeClass( "collapse in" ).addClass( "collapse down" );
                            }


                        v_esquema = 0;

                          

                           $("#informes_menu").hide("slow");
                             $("#formatos_menu").show("slow");

                             
                            // $("#fFinanciamiento").data-toggle();



                var resourceSelected = $("#recurso_sel option:selected").val();     
                 //  alert(resourceSelected);

                


                        
                    }

    
                    }
                });
 
            if(checked!="undefined"){
               
                swal({
                  title: "Estás seguro de querer cambiar Programa?",
                  text: "Nota: Se perderan los datos, antes debes guardar los datos.",
                  icon: "warning",
                  buttons: true,
                  dangerMode: false, 
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Listo!, Se cambio programa exitosamente", {
                      icon: "success",
                    });
                    location.reload(true);

                  } else {
                    swal("Lo dejamos como estaba..");
                  }
                });
            }



 
                     //location.reload(true);

               
        }


             


 
        


 



    </script>



</body>

</html>
