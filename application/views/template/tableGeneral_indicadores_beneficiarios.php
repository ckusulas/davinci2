



<table id="example" class="display" class="col-lg-12">
    <thead>
        <tr>
            <th>Folio</th>
            <th>Beneficiados</th>
            <th>Hombres</th>
            <th>Mujeres</th>
            <th>Recurso</th>
            <th>Tipo</th>  
            <th>Titulo</th>
            <th>Ejecutora</th>



            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $counter = 1;
        foreach ($listProyectos as $row) {

            echo "<tr><td>" . $counter . "</td><td align=''>" . $row["total_beneficiados"] . "</td><td align=''>" . $row["hombres_beneficiados"] . "</td><td align=''>" . $row["mujeres_beneficiadas"] . "</td><td>" . $row["recurso"] . "</td><td >" . $row["beneficio"] . "</td><td>" . $row["nombre_proyecto"] . "</td><td>" . $row["instancia_ejecutora"] . "</td>";
            ?>

        <td>
            <button type="button" class="btn btn-xs btn-primary waves-effect w-md waves-light m-b-15"  onClick='viewProy(<?= $row['id'] ?>)' data-toggle="modal" data-target=". "><i class="fa fa-pencil"></i> Detalles</button>
        </td>




    </tr>








    <?php
    $counter++;
}
?>



</tbody>
<tfoot>
    <tr>
        <td></td>
        <td><strong> <?php echo number_format($totalBeneficiados) ?></strong></td>
        <td><strong> <?php echo number_format($totalHombres) ?></strong></td>
        <td><strong> <?php echo number_format($totalMujeres) ?></strong></td>              
        <th>Recurso</th>
        <td></td>
        <td></td>

        <td></td>
        <td>Acciones</td>
    </tr>
</tfoot>
</table>








<script type="text/javascript">

    $(document).ready(function () {






        $('#example tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="' + title + '" />');
        });


        var tableProy = $('#example').DataTable({
             language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            }
                        },
            dom: 'Bfrtip',
            scrollX: 'true',
            pagingType: 'full_numbers',
            dom: 'T<"clear">lfrtip',
                    bAutoWidth: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
            bInfo: false,
            buttons: [
                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy'
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                },
                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV'
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF'
                }
            ],
        });


        tableProy.columns().every(function () {
            var that = this;

            $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that
                            .search(this.value)
                            .draw();
                }
            });
        });










    });


    function viewProy(id_proyecto)
    {


        $("#id_proyecto").val(id_proyecto);

        $("#method").val("view");

        $("#form_indicadores").submit();




    }


</script>