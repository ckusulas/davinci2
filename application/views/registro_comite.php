
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SICSEQ V2.0</title>

       <?php $this->view('template/header.php'); ?>   
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
</head>

<body>


    <?php $this->view('template/header.php'); ?>  
    <?php $this->view('template/mod_nuevo_programa'); ?>

    <div id="wrapper">

       <!--Modal para agregado de Nuevos Programas -->
      

 

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                           <h1>
                                               
                                            </h1>
                                         </td>
                                        <td align="right"> 

                                           <!-- <button type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='cleanElement()'><i class="fa fa-floppy-o"></i> Nuevo</button> -->
                                            

                                            <button id="btnNuevo" type="button" class="btn btn-danger waves-effect w-md waves-light m-b-15" data-toggle="modal-dialog"  onClick='cleanElement()'><i class="fa fa-floppy-o"></i> Nuevo</button>


                                           <?php
                                           $style="display:none";
                                             if($editData["metodo"]!="view" ) {

                                                $style = "";
                                            ?>

                                               <?php
                                                }
                                            ?>
                                                 <button  type="button" style="<?=$style?>" class="btn btn-success waves-effect w-md waves-light m-b-15" id="btnGuardar" data-toggle="modal"  onClick='guardar()'><i class="fa fa-floppy-o"></i> Guardar</button>   
                                          

                                            <?php
                                             if($editData["metodo"]=="view") {
                                            ?>

                                              <button type="button" class="btn btn-warning waves-effect w-md waves-light m-b-15" data-toggle="modal"  onClick='editar()'><i class="fa fa-floppy-o"></i> Editar</button>  
                                            <?
                                            }
                                            ?>

                                            <?php
                                            $style="display:none";
                                             if($editData["metodo"]=="view" || $editData["metodo"]=="edit" || $editData['onlyView'] ) {

                                                 $style = "";
                                            ?>

                                            <?php
                                                }
                                            ?>
                                                  <button type="button"n style="<?=$style?>" class="btn btn-info waves-effect w-md waves-light m-b-15" id="btnImprimir" data-toggle="modal"  onClick="imprimir_Formato();" ><i class="fa fa-floppy-o"></i> Imprimir</button>
                                        


                                           
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                <li><i class="fa fa-edit"></i>Comités</i>
                                </li>
                                <li class="active">Registar Comité</li>
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                              <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                              <input type="hidden" name="msgError" id="msgError">
                        <?php
                            $totalIntegrantes = intval($editData['qty_hombres']) + intval($editData['qty_mujeres']);
                            if($totalIntegrantes == 0 || $totalIntegrantes ==""){
                                $totalIntegrantes = 1;
                            }

                        ?>

                              <input type="hidden" name="x" id="countResourceIntegrantes" value="<?=$totalIntegrantes?>">
                              <input type="hidden" name="recurso_hidden" id="recurso_hidden" >
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

               

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
                
                
                 
                

                    <!-- Datos del comité -->
                    <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>1. Datos del Comité</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_datos_comité" action="#">

 
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">*Nombre del Comité:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" id="nombreComite" name="nombreComite" value="<?php echo $editData['nombre_comite']?>" maxlength="100" onBlur="" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">*Fecha de Constitución:</label>
                                                    <div class="col-sm-4">
                                                           <div class="input-group">
                                                                   <span class="input-group-addon"><i class="fa fa-calendar constitucionIcon"></i>
                                                    </span> 
                                                        <input type="text"  style="text-align:right" class="form-control input-sm" id="fechaConstitucion" name="fechaConstitucion" value="<?php echo $editData['fecha_constitucion']?>"  >
                                                    </div></div>
                                                </div>
                                                <!--<div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">Clave del Registro:</label>
                                                    <div class="col-sm-6 ">
                                                        <input type="text" class="form-control input-sm" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-/#\s]+$/g,'');" id="claveRegistro" name="claveRegistro" value="<?php echo $editData['clave_registro']?>" >
                                                    </div>
                                                </div>-->
                                       

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">*Accion de Conexión:</label>
                                                    <div class="col-sm-6 ">
                                                        
                                                        <!-- multiple="multiple" -->
                                                        <select id="listObras2"    name="accionConexion[]" class="col-sm-5" style="text-overflow:ellipsis" onChange="populate_localidad(this.value)">
                                                       
                                                            
                                                        </select>

                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">*Localidad:</label>
                                                    <div class="col-sm-6">
                                                           <select class="form-control" id="localidad" name="localidad2">
                                                            <option value="0">Seleccione Localidad..</option>
                                                            

                                                        </select>
                                                    </div>
                                                </div>


                                                <input type="hidden" id="metodo" name="metodo" value="<?php echo $editData['metodo']?>">
                                                <input type="hidden" id="nombreIntegrante_tmp" name="nombreIntegrante_tmp" value="">
                                                <input type="hidden" id="msgError" name="msgError">
                                                <input type="hidden" id="id_comite" name="id_comite" value="<?php echo $editData['id_comite'] ?>" >

                                                <br><br><br>
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>               
                <!-- fin Datos del comité -->




         










 <!-- Documentos del comité -->
                 <div class="col-lg-6">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-red">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>2. Documentos del comité</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#documentoscomité"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="documentoscomité" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_documentoscomite">
 
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5 ">*Servidor Público que Emite La Constancia:</label>
                                                    <div class="col-sm-6 ">
                                                        <input type="text" class="form-control input-sm" id="servidorPublicoNombre"  oninput="this.value=this.value.replace(/[^a-zA-Z/-\s]/g,'');" name="servidorPublicoNombre" maxlength="50"   value="<?php echo $editData['servidor_publico_constancia'] ?>" >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">*Cargo del Servidor Público:</label>
                                                    <div class="col-sm-6 ">
                                                        <input type="text" maxlength="50" oninput="this.value=this.value.replace(/[^a-zA-Z/-\s]/g,'');" class="form-control input-sm" id="servidorPublicoCargo" name="servidorPublicoCargo" value="<?php echo $editData['cargo_servidor_publico'] ?>"  >


                                                    </div>
                                                </div>

                                              <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">*Cargo del Servidor Público:</label>
                                                    <div class="col-sm-6 ">
                                                        <input type="text" maxlength="50" oninput="this.value=this.value.replace(/[^a-zA-Z/-\s]/g,'');" class="form-control input-sm" id="servidorPublicoCargo" name="servidorPublicoCargo" value="<?php echo $editData['cargo_servidor_publico'] ?>"  >


                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-5">*Acta de Asamblea:</label>
                                                    <div class="col-sm-6 ">

                                                      <!--   <input type="file"   id="actaAsamblea" name="actaAsamblea"  >-->
                                                  

                                                        <input type="file" name="file" id="file1" onChange="cargarFile(this,1,'comite','actaasamblea')" >
                                                       
                                                        <div id="band1"></div>
                                                        <div id="url1"></div>

                                                         <?php
                                                        if(isset($editData['url_actasamblea'][0])) {
                                                           // echo print_r($editData['url_convenio']);
                                                        ?>
                                                            <a target="_blank" href="<?=$editData['url_actasamblea'][0]['url']?>">Archivo cargado</a>
                                                        <?
                                                        }
                                                        ?>


                                                              
                                                    </div>
                                                </div>

                                                <div class="form-group" id="divConstanciaFirmada">
                                                    <label for="textInput" class="col-sm-5 ">Constancia Firmada (Anexo de Guía Operativa):</label>
                                                       <div class="col-sm-6">

                                                        <!-- <input type="file"   id="constanciaFirmada" name="constanciaFirmada"  >-->

                                                          <input type="file" name="file2" id="file2" onChange="cargarFile(this,2,'comite','constancia')" >
                                                     
                                                        <div id="band2"></div>
                                                        <div id="url2"></div>

                                                        <?php
                                                        if(isset($editData['url_constancia'][0])) {
                                                           // echo print_r($editData['url_convenio']);
                                                        ?>
                                                            <a target="_blank" href="<?=$editData['url_constancia'][0]['url']?>">Archivo cargado</a>
                                                        <?
                                                        }
                                                        ?>

  
                                                            
                                                    </div>
                                                </div>



                                                <div class="form-group" id="divEscritoLibre">
                                                    <label  class="col-sm-5 ">Escrito Libre:</label>
                                                    <div class="col-sm-6">
                                                        <!-- <input type="file"   id="escritoLibre" name="escritoLibre"  >-->

                                                          <input type="file" name="file3" id="file3" onChange="cargarFile(this,3,'comite','escritolibre')" >
                                                
                                                        <div id="band3"></div>
                                                        <div id="url3"></div>

                                                        <?


                                                        if(isset($editData['url_escritolibre'][0])) {
                                                           // echo print_r($editData['url_convenio']);
                                                        ?>
                                                            <a target="_blank" href="<?=$editData['url_escritolibre'][0]['url']?>">Archivo cargado</a>
                                                        <?
                                                        }
                                                        ?>
                                                         
                                                    </div>
                                                </div>

                                                 <div class="form-group">
                                                    <label  class="col-sm-5 ">Comentarios:</label>
                                                    <div class="col-sm-6">
                                                         <textarea   class="form-control" rows="3" id="comentarioComite" name="comentarioComite"><?php echo $editData['comentarios'] ?></textarea>
                                                    </div>
                                                </div>
                                                <br>

                                      
                                            
                                                <input type="hidden" name="id_comite_documentos" id="id_comite_documentos">
                                                <input type="hidden" name="file_asamblea" id="file_asamblea">
                                                <input type="hidden" name="file_constancia" id="file_constancia">
                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Documentos del comité -->


                 <!-- Hoverable Responsive Table -->
                    <div class="col-lg-12"  style="display:none" id="modulo_preguntas_funciones">
                        <div class="portlet portlet-blue">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Funciones</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <form id="form_funciones">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                
                                                <th>*Funciones que se aplicarón en el comité (puede escoger mas de uno)</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php

                                            $counter = 1;
                                                      foreach($listComiteFunciones as $row) {
                                                                    $selected = "";

                                                            ?>
                                                            <tr> 

                                                                 <td style="vertical-align:middle">
                                                                    <?php



                                                                    $acentos = array("Ó", "Á", "Í", "Ú");
                                                                    $validos = array("ó", "a", "í", "ú");

                                                                    $funcion = ucwords(strtolower($row['campo']));
                                                                    $funcion = str_replace($acentos,$validos, $funcion);
                                                                    echo $counter.".- " . $funcion;

                                                                    $checked="";

                                                                    $datosA = explode(",",$editData['funciones_comite']);
                                                    
                                                                    if(in_array($row['id'],$datosA)){
                                                                        $checked = "checked";

                                                                    }

                                                                    ?>

                                                                </td>
                                                           

                                                                <td>
                                                                                                                                   

                                                               
                                                                        <label>
                                                                    <input type="checkbox" id="elem_<?php echo $row['id'] ?>" name="funcionesComite[]" value="<?php echo $row['id']?>" <?php echo $checked ?> />  </label>
                                                                        
                                                                </td>

                                                             
                                                               


                                                            </tr>
                                                            <?php
                                                            $counter++;


                                                            }
                                                            ?>
                                           
                                        </tbody>
                                    </table>
                                </div>

                                 <input type="hidden" name="id_comite_funciones" id="id_comite_funciones">
                                               
                                </form>
                            </div>

                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-6 -->
                



                  <!-- Funcion del comité Federal-->
                 <div class="col-lg-12"  style="display:none" id="modulo_funciones_federal">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-orange">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>3. Preguntas Clave</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#preguntasClave"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="preguntasClave" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_preguntasClave">




                                                <div class="form-group">

                                                     <label  class="col-sm-8">*1. Se proporcionó resumen ejecutivo de obra apoyo o servicio?:</label>
                                                   
                                                     <div class="col-sm-3">

                                                          <?php 
                                                            $selected = "";
                                                            if($editData['proporciono_resumen']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>


                                                        <label class="radio-inline"><input type="radio" value="SI" name="proporcionoResumenEjecutivo" <?php echo $selected?> >Si</label>

                                                        <?php 
                                                            $selected = "";
                                                            if($editData['proporciono_resumen']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="NO" name="proporcionoResumenEjecutivo" <?php echo $selected ?>">No</label> 
                                                            
                                                    
                                                    </div>
                                                   
                                                </div>


 
 
                                      
 

                                                
                                                 <div class="form-group">

                                                       <label  class="col-sm-8">*2. ¿Se pusieron a disposición de los beneficiarios, mecanismos para la atención de Quejas y Denuncias?:</label>
                                                    <div class="col-sm-3">


                                                        <?php 
                                                            $selected = "";
                                                            if($editData['disposicion_quejas']=="SI"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline"><input type="radio" value="SI" name="disposicionQuejas" <?php echo $selected ?> >Si</label>

                                                              <?php 
                                                            $selected = "";
                                                            if($editData['disposicion_quejas']=="NO"){
                                                                $selected = "checked='checked'";
                                                            }
                                                            ?>

                                                        <label class="radio-inline">
                                                            <input type="radio" value="NO" name="disposicionQuejas" <?php echo $selected ?>>No</label> 
                                                            
                                                    
                                                 
                                                    </div>
                                                 
                                                    
                                                </div>




                                               
                                            
                                             <input type="hidden" name="id_comite_preguntasclave" id="id_comite_preguntasclave">
                                               

                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Funciones del comité Federal-->






 


                     <!-- Integrante del comité -->
                 <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-purple">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>4. Registrar datos de Integrantes del Comité</h4>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#integrantescomité"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="integrantescomité" class="panel-collapse collapse in">
                                             <div class="portlet-body">
                                            <form class="form-horizontal" id="form_integrante1">

 
                                               <div class="text-right">  <a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a></div><br>

                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 1:</label>
                                            </div> 

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Nombre del Integrante:</label>
                                                    <div class="col-sm-3 ">

                                                        <?php

                                                            $nombre1="";
                                                            if(isset($listIntegrantes[0]['nombre_integrante'])){
                                                                $nombre1 = $listIntegrantes[0]['nombre_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        
                                                        <input type="text" maxlength="30" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');"  class="form-control input-sm" id="nombreIntegrante1" name="nombreIntegrante[]" tabindex="1"  value="<?=$nombre1?>">
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">*Cargo del Integrante:</label>
                                                    <div class="col-sm-3 ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante1" name="cargoIntegrante[]" tabindex="7" >
                                                        <?php

                                                            $cargo1="";
                                                            if(isset($listIntegrantes[0]['cargo_integrante'])){
                                                                $cargo1 = $listIntegrantes[0]['cargo_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                            <option>Seleccione..</option>
                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Presidente"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>

                                                            <option <?=$selected?> value="Presidente">PRESIDENTE</option>

                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Secretario"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option value="Secretario" <?=$selected?> >SECRETARIO</option> 

                                                           <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Tesorero"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>


                                                            <option value="Tesorero" <?=$selected?> >TESORERO</option>    


                                                           <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Vocal"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>                      
                                                            <option value="Vocal" <?=$selected?> >VOCAL</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">

                                                    <?php

                                                            $apellido1="";
                                                            if(isset($listIntegrantes[0]['appaterno_integrante'])){
                                                                $apellido1 = $listIntegrantes[0]['appaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <label for="textInput" class="col-sm-2">*Primer Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" maxlength="15"  oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');"  class="form-control input-sm" id="apPIntegrante1" name="apPIntegrante[]" tabindex="2" value="<?=$apellido1?>">
                                                    </div>
<div class="firma_constancia_class">
                                                    <label for="textInput" class="col-sm-2">*Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3 ">


                                                    <?php

                                                            $checked="";
                                                            $firmaconstancia1 = "";
                                                            if(isset($listIntegrantes[0]['firma_constancia_registro_integrante'])){
                                                                $firmaconstancia1 = $listIntegrantes[0]['firma_constancia_registro_integrante'];
                                                            }
                                                            if("SI"==$firmaconstancia1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                    ?>
                                                    
                                                       <label class="radio-inline"><input type="radio" value="SI" id="firmaRegistroConstancia1" name="firmaRegistroConstancia1" tabindex="8" <?=$checked?>>Si</label>
                                                        <label class="radio-inline">


                                                    <?php

                                                            $checked="";
                                                            if(isset($listIntegrantes[0]['firma_constancia_registro_integrante'])){
                                                                $firmaconstancia1 = $listIntegrantes[0]['firma_constancia_registro_integrante'];
                                                            }
                                                            if("NO"==$firmaconstancia1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia1" name="firmaRegistroConstancia1" <?=$checked?>>No</label> 
                                                    </div>
                                                </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Segundo Apellido:</label>

                                                       <?php

                                                            $apellido2="";
                                                            if(isset($listIntegrantes[0]['apmaterno_integrante'])){
                                                                $apellido2 = $listIntegrantes[0]['apmaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-3" >
                                                        <input type="text" maxlength="15" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');"  class="form-control input-sm" id="apMIntegrante1" name="apMIntegrante[]"  tabindex="3" value="<?=$apellido2?>">
                                                    </div>
                                                     <label  class="col-sm-2 ">*¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">

                                                         <?php

                                                            $checked="";
                                                            $domicilioConocido1 = "";
                                                            if(isset($listIntegrantes[0]['domicilio_conocido_integrante'])){
                                                                $domicilioConocido1 = $listIntegrantes[0]['domicilio_conocido_integrante'];
                                                            }
                                                            if("SI"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante1" name="domicilioCortejadoIntegrante1" <?=$checked?> tabindex="9">Si</label>
                                                        <label class="radio-inline">

                                                        <?php

                                                            $checked="";
                                                        
                                                            if("NO"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante1" <?=$checked?> name="domicilioCortejadoIntegrante1">No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Sexo:</label>
                                                       <div class="col-sm-3">


                                                        <?php

                                                            $checked="";
                                                            $sexo1 = "";
                                                            if(isset($listIntegrantes[0]['sexo_integrante'])){
                                                                $sexo1 = $listIntegrantes[0]['sexo_integrante'];
                                                            }
                                                            if("Femenino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>

                                                        
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" id="sexoIntegrante1"  name="sexoIntegrante1" tabindex="4" <?=$checked?>>Femenino</label>
                                                        <label class="radio-inline">

                                                        <?php
                                                        $checked = "";
                                                    
                                                            if("Masculino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>

                                                            <input type="radio" value="Masculino" id="sexoIntegrante1" name="sexoIntegrante1" <?=$checked?>>Masculino</label> 
                                                            
                                                    </div>

                                                        <?php

                                                            $calle1="";
                                                            if(isset($listIntegrantes[0]['calle_integrante'])){
                                                                $calle1 = $listIntegrantes[0]['calle_integrante'];
                                                            }
                                                                   
                                                        ?>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6 ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante1" name="calleIntegrante[]"  tabindex="10" value="<?=$calle1?>">
                                                    </div>
                                                </div>


                                                <div class="form-group" >
                                                      <?php

                                                            $edad1="";
                                                            if(isset($listIntegrantes[0]['edad_integrante'])){
                                                                $edad1 = $listIntegrantes[0]['edad_integrante'];
                                                            }
                                                                   
                                                        ?>


                                                    <label for="textInput" class="col-sm-2">*Edad:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante1"  maxlength="3" name="edadIntegrante[]" tabindex="5" value="<?=$edad1?>" >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                        <?php

                                                            $numero1="";
                                                            if(isset($listIntegrantes[0]['numero_integrante'])){
                                                                $numero1 = $listIntegrantes[0]['numero_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante1" maxlength="6" name="numeroIntegrante[]" tabindex="11" value="<?=$numero1?>">
                                                    </div>

                                                      <?php

                                                            $colonia1="";
                                                            if(isset($listIntegrantes[0]['colonia_integrante'])){
                                                                $colonia1 = $listIntegrantes[0]['colonia_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante1" maxlength="30"  name="coloniaIntegrante[]" tabindex="12" value="<?=$colonia1?>">
                                                    </div>

                                                   
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>

                                                       <?php

                                                            $cpostal1="";
                                                            if(isset($listIntegrantes[0]['cpostal_integrante'])){
                                                                $cpostal1 = $listIntegrantes[0]['cpostal_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante1" maxlength="5" name="codigoPostalIntegrante[]" oninput="this.value=this.value.replace(/[^7]+[^0-9-#\-S]+$/g,'');"  tabindex="6" value="<?=$cpostal1?>" >
                                                    </div>
                                                </div>

                                                   <div class="form-group">

                                                     <?php

                                                            $telefono1="";
                                                            if(isset($listIntegrantes[0]['telefono_integrante'])){
                                                                $telefono1 = $listIntegrantes[0]['telefono_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <label for="textInput" class="col-sm-2">Teléfono:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante1" maxlength="10" name="telefonoIntegrante[]"  oninput="this.value=this.value.replace(/[^0-9-#\-S]+$/g,'');"  tabindex="6" value="<?php echo $telefono1?>">
                                                    </div>
                                                    
                                                </div>
                          

                                                
                                                <br><br> 
                                                
                                             

                                      <?php
                                      $nombre2 = "";
                                        $display = "style='display:none'";
                                         if(isset($listIntegrantes[1]['nombre_integrante'])){
                                                                $nombre2 = $listIntegrantes[1]['nombre_integrante'];
                                                                if(trim($nombre2)!=""){
                                                                    $display =  "";
                                                                }
                                        }
                                                                   
                                      ?>
                                                
 

                                      <div id="view_integrante2" <?=$display?> >   

                                            <hr>


                                            <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 2:</label>
                                            </div> 
                                                <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(2)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>

                                                <div class="form-group">
                                             
                                                
                                                
                                                    <label for="textInput" class="col-sm-2">*Nombre del Integrante:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" maxlength="25"   class="form-control input-sm" id="nombreIntegrante2" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" value="<?=$nombre2?>" name="nombreIntegrante[]" tabindex="14" >
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">*Cargo del Integrante:</label>
                                                       <?php

                                                            $cargo1="";
                                                            if(isset($listIntegrantes[1]['cargo_integrante'])){
                                                                $cargo1 = $listIntegrantes[1]['cargo_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-3 ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante2" name="cargoIntegrante[]" tabindex="20">
                                                            <option>Seleccione..</option>
                                                               <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Presidente"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option  <?=$selected?> value="Presidente">PRESIDENTE</option>
                                                            
                                                              <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Secretario"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option  <?=$selected?> value="Secretario">SECRETARIO</option>
                                                            
                                                              <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Tesorero"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?> value="Tesorero">TESORERO</option>
                                                            
                                                             <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Vocal"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>                   
                                                           <option <?=$selected?> value="Vocal">VOCAL</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                     <?php

                                                            $apellido1="";
                                                            if(isset($listIntegrantes[1]['appaterno_integrante'])){
                                                                $apellido1 = $listIntegrantes[1]['appaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                
                                                    <label for="textInput" class="col-sm-2">*Primer Apellido:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" maxlength="15" class="form-control input-sm" id="apPIntegrante2" name="apPIntegrante[]" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" tabindex="15" value="<?=$apellido1?>">
                                                    </div>
                                                    
                                                    
                                                    <?php

                                                            $checked="";
                                                            $firmaconstancia2 = "";
                                                            if(isset($listIntegrantes[1]['firma_constancia_registro_integrante'])){
                                                                $firmaconstancia2 = $listIntegrantes[1]['firma_constancia_registro_integrante'];
                                                            }
                                                            if("SI"==$firmaconstancia2){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                     
                                                    <label for="textInput" class="col-sm-2 firma_constancia_class">*Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3"  class="firma_constancia_class">
                                                       <label class="radio-inline firma_constancia_class"><input type="radio" value="SI" id="firmaRegistroConstancia2" name="firmaRegistroConstancia2" tabindex="21" <?=$checked?>>Si</label>
                                                       
                                                    <?php
                                                    
                                                             $checked="";
                                                            if("NO"==$firmaconstancia2){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 <label class="radio-inline firma_constancia_class">
                                                            <input type="radio" class="firma_constancia_class" value="NO" id="firmaRegistroConstancia2" <?=$checked?>>No</label> 
                                                    </div>
                                                </div>
                                                 

                                                <div class="form-group">
                                                  <?php

                                                            $apellido2="";
                                                            if(isset($listIntegrantes[1]['apmaterno_integrante'])){
                                                                $apellido2 = $listIntegrantes[1]['apmaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                
                                                    <label for="textInput" class="col-sm-2">*Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" maxlength="15"   class="form-control input-sm" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" id="apMIntegrante2" name="apMIntegrante[]" tabindex="16" value="<?=$apellido2?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">*¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">
                                                    
                                                         <?php

                                                            $checked="";
                                                             $firmaconstancia1 = "";
                                                            if(isset($listIntegrantes[1]['domicilio_conocido_integrante'])){
                                                                $firmaconstancia1 = $listIntegrantes[1]['domicilio_conocido_integrante'];
                                                            }
                                                            if("SI"==$firmaconstancia1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante2" name="domicilioCortejadoIntegrante2" tabindex="22" <?=$checked?>>Si</label>
                                                        <label class="radio-inline">
                                                        <?php
                                                                
                                                                $checked="";
                                                                if("NO"==$firmaconstancia1){
                                                                $checked = "checked";
                                                            }
                                                                  
                                                        ?>
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante2" name="domicilioCortejadoIntegrante2" <?=$checked?>>No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Sexo:</label>
                                                       <div class="col-sm-3">
                                                       
                                                            <?php

                                                            $checked="";
                                                              $sexo1 = "";
                                                            if(isset($listIntegrantes[1]['sexo_integrante'])){
                                                                $sexo1 = $listIntegrantes[1]['sexo_integrante'];
                                                            }
                                                            if("Femenino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" name="sexoIntegrante2" tabindex="17" <?=$checked?>>Femenino</label>
                                                        <label class="radio-inline">
                                                        
                                                            <?php

                                                            $checked="";
                                              
                                                            if("Masculino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante2" name="sexoIntegrante2" <?=$checked?>>Masculino</label> 
                                                            
                                                    </div>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                           <?php

                                                            $calle1="";
                                                            if(isset($listIntegrantes[1]['calle_integrante'])){
                                                                $calle1 = $listIntegrantes[1]['calle_integrante'];
                                                            }
                                                                   
                                                        ?>

                                                    <div class="col-sm-6"  >
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante2" name="calleIntegrante[]" tabindex="23" value="<?=$calle1?>"   >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Edad:</label>
                                                    <?php

                                                            $edad1="";
                                                            if(isset($listIntegrantes[1]['edad_integrante'])){
                                                                $edad1 = $listIntegrantes[1]['edad_integrante'];
                                                            }
                                                                   
                                                        ?>

                                                    <div class="col-sm-3"  >
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante2" maxlength="3" name="edadIntegrante[]" tabindex="18"  value="<?=$edad1?>">
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                      <?php

                                                            $numero1="";
                                                            if(isset($listIntegrantes[1]['numero_integrante'])){
                                                                $numero1 = $listIntegrantes[1]['numero_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante2" maxlength="6" name="numeroIntegrante[]" tabindex="24"   value="<?=$numero1?>">
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                     
                                                            <?php

                                                            $colonia1="";
                                                            if(isset($listIntegrantes[1]['colonia_integrante'])){
                                                                $colonia1 = $listIntegrantes[1]['colonia_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante2" maxlength="30" name="coloniaIntegrante[]"  tabindex="25"  value="<?=$colonia1?>">
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1 ">
                                                       <?php

                                                            $cpostal1="";
                                                            if(isset($listIntegrantes[1]['cpostal_integrante'])){
                                                                $cpostal1 = $listIntegrantes[1]['cpostal_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante2" maxlength="5" name="codigoPostalIntegrante[]" tabindex="26"  value="<?=$cpostal1?>">
                                                    </div>
                                                </div>

                                                  <div class="form-group">
                                                           <?php

                                                            $telefono1="";
                                                            if(isset($listIntegrantes[1]['telefono_integrante'])){
                                                                $telefono1 = $listIntegrantes[1]['telefono_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <label for="textInput" class="col-sm-2">Telefono:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante2" oninput="this.value=this.value.replace(/[^0-9-#\-S]+$/g,'');"  maxlength="10" name="telefonoIntegrante[]"  tabindex="18" value="<?=$telefono1?>">
                                                    </div>
                                                    
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             

                                        </div>
                                        
                                        
                                          <?php
                                      $nombre2 = "";
                                        $display = "style='display:none'";
                                         if(isset($listIntegrantes[2]['nombre_integrante'])){
                                                                $nombre2 = $listIntegrantes[2]['nombre_integrante'];
                                                                if(trim($nombre2)!=""){
                                                                    $display =  "";
                                                                }
                                        }
                                                                   
                                      ?>



                                       <div id="view_integrante3" <?=$display?> >   
                                                
 

                                               

                                            <hr>


                                               <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 3:</label>
                                            </div> 
                                              <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(3)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Nombre del Integrante:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" maxlength="25"   class="form-control input-sm" id="nombreIntegrante3" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" value="<?=$nombre2?>" name="nombreIntegrante[]"  tabindex="27">
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">*Cargo del Integrante:</label>
                                                     
                                                        <?php

                                                            $cargo1="";
                                                            if(isset($listIntegrantes[2]['cargo_integrante'])){
                                                                $cargo1 = $listIntegrantes[2]['cargo_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-3 ">
                                                          <select class="form-control col-sm-6" id="cargoIntegrante3" name="cargoIntegrante[]" tabindex="33">
                                                            <option>Seleccione..</option>
                                                              <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Presidente"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?> value="Presidente">PRESIDENTE</option>
                                                            
                                                             <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Secretario"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option  <?=$selected?>  value="Secretario">SECRETARIO</option>
                                                            
                                                             
                                                              <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Tesorero"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?>  value="Tesorero">TESORERO</option>    

                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Vocal"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>                                                              
                                                            <option <?=$selected?> value="Vocal">VOCAL</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Primer Apellido:</label>
                                                    
                                                    <?php

                                                            $apellido1="";
                                                            if(isset($listIntegrantes[2]['appaterno_integrante'])){
                                                                $apellido1 = $listIntegrantes[2]['appaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    
                                                    <div class="col-sm-3" >
                                                        <input type="text" maxlength="15"   class="form-control input-sm" id="apPIntegrante3" name="apPIntegrante[]" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');"  tabindex="28" value="<?=$apellido1?>">
                                                    </div>
<div class="firma_constancia_class">
                                                    <label for="textInput" class="col-sm-2">*Firma de Constancia Registro:</label>
                                                            <?php

                                                            $checked="";
                                                            $firmaconstancia3="";
                                                            if(isset($listIntegrantes[2]['firma_constancia_registro_integrante'])){
                                                                $firmaconstancia3 = $listIntegrantes[2]['firma_constancia_registro_integrante'];
                                                            }
                                                            if("SI"==$firmaconstancia3){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                    
                                                    <div class="col-sm-3 ">
                                                       <label class="radio-inline"><input type="radio" value="SI" <?=$checked?> id="firmaRegistroConstancia3" name="firmaRegistroConstancia3" tabindex="34">Si</label>
                                                        <label class="radio-inline">
                                                        
                                                              <?php
                                                    
                                                             $checked="";
                                                            if("NO"==$firmaconstancia3){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia3" name="firmaRegistroConstancia3" <?=$checked?>>No</label> 
                                                    </div>
                                                </div>
                                                </div>

                                                <div class="form-group">                                                  
                                                
                                                <?php

                                                            $apellido2="";
                                                            if(isset($listIntegrantes[2]['apmaterno_integrante'])){
                                                                $apellido2 = $listIntegrantes[2]['apmaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                
                                                    <label for="textInput" class="col-sm-2">*Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" maxlength="15"    class="form-control input-sm" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" id="apMIntegrante3" name="apMIntegrante[]"  tabindex="29"  value="<?=$apellido2?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">*¿Domicilio Conocido del Integrante?:</label>
                                                       <?php

                                                            $checked="";
                                                            $domicilioConocido1 = "";
                                                            if(isset($listIntegrantes[2]['domicilio_conocido_integrante'])){
                                                                $domicilioConocido1 = $listIntegrantes[2]['domicilio_conocido_integrante'];
                                                            }
                                                            if("SI"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-3">
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante3" <?=$checked?>  name="domicilioCortejadoIntegrante3" tabindex="35">Si</label>
                                                        <label class="radio-inline">
                                                                                                               
                                                       <?php

                                                            $checked="";
                                                        
                                                            if("NO"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                        
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante3" <?=$checked?>  name="domicilioCortejadoIntegrante3">No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Sexo:</label>
                                                       
                                                            <?php

                                                            $checked="";
                                                                  $sexo1 = "";
                                                            if(isset($listIntegrantes[2]['sexo_integrante'])){
                                                                $sexo1 = $listIntegrantes[2]['sexo_integrante'];
                                                            }
                                                            if("Femenino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                       <div class="col-sm-3">
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" id="sexoIntegrante3" name="sexoIntegrante3" tabindex="30"  <?=$checked?>>Femenino</label>
                                                        <label class="radio-inline">
                                                        
                                                       <?php

                                                            $checked="";
                                            
                                                            if("Masculino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                        
                                                            <input type="radio" value="Masculino" id="sexoIntegrante3" name="sexoIntegrante3"  <?=$checked?>>Masculino</label> 
                                                            
                                                    </div>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                     <?php

                                                            $calle1="";
                                                            if(isset($listIntegrantes[2]['calle_integrante'])){
                                                                $calle1 = $listIntegrantes[2]['calle_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante3" name="calleIntegrante[]" tabindex="36"  value="<?=$calle1?>"  >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Edad:</label>
                                                          <?php

                                                            $edad1="";
                                                            if(isset($listIntegrantes[2]['edad_integrante'])){
                                                                $edad1 = $listIntegrantes[2]['edad_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante3" maxlength="3" name="edadIntegrante[]"  tabindex="31" value="<?=$edad1?>">
                                                    </div>
                                                         <?php

                                                            $numero1="";
                                                            if(isset($listIntegrantes[2]['numero_integrante'])){
                                                                $numero1 = $listIntegrantes[2]['numero_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante3" maxlength="6"  name="numeroIntegrante[]" tabindex="37"  value="<?=$numero1?>" >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                     <?php

                                                            $colonia1="";
                                                            if(isset($listIntegrantes[2]['colonia_integrante'])){
                                                                $colonia1 = $listIntegrantes[2]['colonia_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante3" maxlength="30" name="coloniaIntegrante[]" tabindex="38" value="<?=$colonia1?>">
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                      <?php

                                                            $cpostal1="";
                                                            if(isset($listIntegrantes[2]['cpostal_integrante'])){
                                                                $cpostal1 = $listIntegrantes[2]['cpostal_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1" >
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante3" maxlength="5" name="codigoPostalIntegrante[]" tabindex="39"  value="<?=$cpostal1?>">
                                                    </div>
                                                </div>
                                                

                                                <div class="form-group"> 
                                                
                                                <?php

                                                            $telefono1="";
                                                            if(isset($listIntegrantes[2]['telefono_integrante'])){
                                                                $telefono1 = $listIntegrantes[2]['telefono_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                
                                                    <label for="textInput" class="col-sm-2">Telefono:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm" oninput="this.value=this.value.replace(/[^0-9-#\-S]+$/g,'');"  id="telefonoIntegrante3" maxlength="10" name="telefonoIntegrante[]"  tabindex="32" value="<?=$telefono1?>">
                                                    </div>
                                                    
                                                </div>

                                                
                                                <br><br> 
                                                
                                             


                                            </div>      
                                            
                                              
                                     <?php
                                      $nombre2 = "";
                                        $display = "style='display:none'";
                                         if(isset($listIntegrantes[3]['nombre_integrante'])){
                                                                $nombre2 = $listIntegrantes[3]['nombre_integrante'];
                                                                if(trim($nombre2)!=""){
                                                                    $display =  "";
                                                                }
                                        }
                                                                   
                                      ?>
 
                                        <div id="view_integrante4" <?=$display?> >   
                                               

                                            <hr>
 
                                               <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 4:</label>
                                            </div> 
                                             <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(4)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Nombre del Integrante:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" maxlength="25"   class="form-control input-sm" id="nombreIntegrante4" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" name="nombreIntegrante[]"  tabindex="40" value="<?=$nombre2?>">
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">*Cargo del Integrante:</label>
                                                    <div class="col-sm-3 ">
                                                      <?php

                                                            $cargo1="";
                                                            if(isset($listIntegrantes[3]['cargo_integrante'])){
                                                                $cargo1 = $listIntegrantes[3]['cargo_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                          <select class="form-control col-sm-6" id="cargoIntegrante4" name="cargoIntegrante[]" tabindex="46">
                                                            <option>Seleccione..</option>
                                                                <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Presidente"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?>   value="Presidente">PRESIDENTE</option>
                                                            
                                                                 <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Secretario"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?> value="Secretario">SECRETARIO</option>
                                                            
                                                                 <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Tesorero"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option   <?=$selected?> value="Tesorero">TESORERO</option>
                                                            
                                                                <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Vocal"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>     
                                                            <option <?=$selected?> value="Vocal">VOCAL</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Primer Apellido:</label>
                                                    <div class="col-sm-3">
                                                    
                                                    <?php

                                                            $apellido1="";
                                                            if(isset($listIntegrantes[3]['appaterno_integrante'])){
                                                                $apellido1 = $listIntegrantes[3]['appaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        <input type="text" maxlength="15"  class="form-control input-sm" id="apPIntegrante4" name="apPIntegrante[]" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" tabindex="41" value="<?=$apellido1?>">
                                                    </div>
<div class="firma_constancia_class">
                                                    <label for="textInput" class="col-sm-2">*Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3 ">
                                                    
                                                       <?php

                                                            $checked="";
                                                            $firmaconstancia4 = "";
                                                            if(isset($listIntegrantes[3]['firma_constancia_registro_integrante'])){
                                                                $firmaconstancia4 = $listIntegrantes[3]['firma_constancia_registro_integrante'];
                                                            }
                                                            if("SI"==$firmaconstancia4){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                        
                                                       <label class="radio-inline"><input type="radio" value="SI"  <?=$checked?> id="firmaRegistroConstancia4" name="firmaRegistroConstancia4" tabindex="47">Si</label>
                                                        <label class="radio-inline">
                                                            <?php
                                                    
                                                             $checked="";
                                                            if("NO"==$firmaconstancia4){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia4" <?=$checked?> name="firmaRegistroConstancia4">No</label> 
                                                    </div>
                                                </div>
                                            </div>

                                                <div class="form-group">
                                                       <?php

                                                            $apellido2="";
                                                            if(isset($listIntegrantes[3]['apmaterno_integrante'])){
                                                                $apellido2 = $listIntegrantes[3]['apmaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <label for="textInput" class="col-sm-2">*Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" maxlength="15"   class="form-control input-sm" id="apMIntegrante4" name="apMIntegrante[]" tabindex="42" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');"  value="<?=$apellido2?>">
                                                    </div>
                                                     <label  class="col-sm-2 ">*¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">
                                                    
                                                       <?php

                                                            $checked="";
                                                            $domicilioConocido1 = "";
                                                            if(isset($listIntegrantes[3]['domicilio_conocido_integrante'])){
                                                                $domicilioConocido1 = $listIntegrantes[3]['domicilio_conocido_integrante'];
                                                            }
                                                            if("SI"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante4"  <?=$checked?> name="domicilioCortejadoIntegrante4" tabindex="48">Si</label>
                                                        <label class="radio-inline">
                                                        
                                                        <?php

                                                            $checked="";
                                                        
                                                            if("NO"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante4" <?=$checked?> name="domicilioCortejadoIntegrante4">No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Sexo:</label>
                                                       <div class="col-sm-3">
                                                       
                                                        <?php

                                                            $checked="";
                                                              $sexo1 = "";
                                                            if(isset($listIntegrantes[3]['sexo_integrante'])){
                                                                $sexo1 = $listIntegrantes[3]['sexo_integrante'];
                                                            }
                                                            if("Femenino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" id="sexoIntegrante4" <?=$checked?> name="sexoIntegrante4" tabindex="43">Femenino</label>
                                                        <label class="radio-inline">
                                                        
                                                        <?php

                                                            $checked="";
                                          
                                                            if("Masculino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante4" <?=$checked?> name="sexoIntegrante4">Masculino</label> 
                                                            
                                                    </div>
                                                    
                                                    
                                                      <?php

                                                            $calle1="";
                                                            if(isset($listIntegrantes[3]['calle_integrante'])){
                                                                $calle1 = $listIntegrantes[3]['calle_integrante'];
                                                            }
                                                                   
                                                        ?>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6" >
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante4" name="calleIntegrante[]"  tabindex="49" value="<?=$calle1?>" >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Edad:</label>
                                                         <?php

                                                            $edad1="";
                                                            if(isset($listIntegrantes[3]['edad_integrante'])){
                                                                $edad1 = $listIntegrantes[3]['edad_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante4" maxlength="3" name="edadIntegrante[]" tabindex="44" value="<?=$edad1?>" >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                           <?php

                                                            $numero1="";
                                                            if(isset($listIntegrantes[3]['numero_integrante'])){
                                                                $numero1 = $listIntegrantes[3]['numero_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante4" maxlength="6"  name="numeroIntegrante[]" tabindex="50"  value="<?=$numero1?>" >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                     
                                                       <?php

                                                            $colonia1="";
                                                            if(isset($listIntegrantes[3]['colonia_integrante'])){
                                                                $colonia1 = $listIntegrantes[3]['colonia_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante4" maxlength="30"  name="coloniaIntegrante[]" tabindex="51" value="<?=$colonia1?>" >
                                                    </div>
                                                    
                                                        <?php

                                                            $cpostal1="";
                                                            if(isset($listIntegrantes[3]['cpostal_integrante'])){
                                                                $cpostal1 = $listIntegrantes[3]['cpostal_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante4" maxlength="5" name="codigoPostalIntegrante[]"  tabindex="52"  value="<?=$cpostal1?>">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                
                                                   <?php

                                                            $telefono1="";
                                                            if(isset($listIntegrantes[3]['telefono_integrante'])){
                                                                $telefono1 = $listIntegrantes[3]['telefono_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <label for="textInput" class="col-sm-2">Telefono:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm " oninput="this.value=this.value.replace(/[^0-9-#\-S]+$/g,'');"  id="telefonoIntegrante4"  maxlength="10" name="telefonoIntegrante[]"  tabindex="45" value="<?=$telefono1?>">
                                                    </div>
                                                    
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             
                                            </div>

                                                
                                                
                                     <?php
                                          $nombre2 = "";
                                            $display = "style='display:none'";
                                             if(isset($listIntegrantes[4]['nombre_integrante'])){
                                                                    $nombre2 = $listIntegrantes[4]['nombre_integrante'];
                                                                    if(trim($nombre2)!=""){
                                                                        $display =  "";
                                                                    }
                                            }
                                                                   
                                      ?>
 
                                            <div id="view_integrante5"  <?=$display?>>   
                                               

                                            <hr>

                                               <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 5:</label>
                                            </div> 
                                                <div class="text-right"> <a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(5)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Nombre del Integrante:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" maxlength="25"   class="form-control input-sm" id="nombreIntegrante5" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" value="<?=$nombre2?>" name="nombreIntegrante[]" tabindex="53" >
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">*Cargo del Integrante:</label>
                                                    <div class="col-sm-3 ">
                                                        <select class="form-control col-sm-6" id="cargoIntegrante5" name="cargoIntegrante[]" tabindex="59">
                                                        <?php

                                                            $cargo1="";
                                                            if(isset($listIntegrantes[4]['cargo_integrante'])){
                                                                $cargo1 = $listIntegrantes[4]['cargo_integrante'];
                                                            }
                                                                   
                                                        ?>

                                                            <option>Seleccione..</option>
                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Presidente"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?> value="Presidente">PRESIDENTE</option>
                                                            
                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Secretario"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>

                                                           <option  <?=$selected?>  value="Secretario">SECRETARIO</option>
                                                                 
                                                             
                                                              <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Tesorero"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>

                                                            <option <?=$selected?>  value="Tesorero">TESORERO</option>
                                                            
                                                            
                                                            
                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Vocal"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>   
                                                            
                                                            
                                                            <option <?=$selected?> value="Vocal">VOCAL</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                
                                                    
                                                    <?php

                                                            $apellido1="";
                                                            if(isset($listIntegrantes[4]['appaterno_integrante'])){
                                                                $apellido1 = $listIntegrantes[4]['appaterno_integrante'];
                                                            }
                                                                   
                                                    ?>
                                                    
                                                    <label for="textInput" class="col-sm-2">*Primer Apellido:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" maxlength="15"   class="form-control input-sm" id="apPIntegrante5" name="apPIntegrante[]" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" tabindex="54"  value="<?=$apellido1?>">
                                                    </div>
                                                    
                                                    <?php

                                                            $checked="";
                                                            $firmaconstancia5 = "";
                                                            if(isset($listIntegrantes[4]['firma_constancia_registro_integrante'])){
                                                                $firmaconstancia5 = $listIntegrantes[4]['firma_constancia_registro_integrante'];
                                                            }
                                                            if("SI"==$firmaconstancia5){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
<div class="firma_constancia_class">
                                                    <label for="textInput" class="col-sm-2">*Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3 ">
                                                       <label class="radio-inline"><input type="radio" value="SI"  <?=$checked?> id="firmaRegistroConstancia5" name="firmaRegistroConstancia5" tabindex="60">Si</label>
                                                        <label class="radio-inline">
                                                        
                                                          <?php
                                                    
                                                             $checked="";
                                                            if("NO"==$firmaconstancia5){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia5" <?=$checked?> name="firmaRegistroConstancia5">No</label> 
                                                    </div>
                                                </div>
                                                </div>

                                                <div class="form-group">
                                                
                                                           <?php

                                                            $apellido2="";
                                                            if(isset($listIntegrantes[4]['apmaterno_integrante'])){
                                                                $apellido2 = $listIntegrantes[4]['apmaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        
                                                    <label for="textInput" class="col-sm-2">*Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text"  maxlength="15"   class="form-control input-sm" id="apMIntegrante5" name="apMIntegrante[]" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" tabindex="55" value="<?=$apellido2?>" >
                                                    </div>
                                                     <label  class="col-sm-2 ">*¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">
                                                      <?php

                                                            $checked="";
                                                            $domicilioConocido1 = "";
                                                            if(isset($listIntegrantes[4]['domicilio_conocido_integrante'])){
                                                                $domicilioConocido1 = $listIntegrantes[4]['domicilio_conocido_integrante'];
                                                            }
                                                            if("SI"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" <?=$checked?> id="domicilioCortejadoIntegrante5" name="domicilioCortejadoIntegrante5" tabindex="61">Si</label>
                                                        <label class="radio-inline">
                                                        
                                                        <?php

                                                            $checked="";
                                                        
                                                            if("NO"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                        
                                                        
                                                        
                                                        
                                                            <input type="radio" value="NO"  <?=$checked?>  id="domicilioCortejadoIntegrante5" name="domicilioCortejadoIntegrante5">No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Sexo:</label>
                                                       <div class="col-sm-3">
                                                       
                                                         <?php

                                                            $checked="";
                                                              $sexo1 = "";
                                                            if(isset($listIntegrantes[2]['sexo_integrante'])){
                                                                $sexo1 = $listIntegrantes[2]['sexo_integrante'];
                                                            }
                                                            if("Femenino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" <?=$checked?> id="sexoIntegrante5"  name="sexoIntegrante5" tabindex="56">Femenino</label>
                                                        <label class="radio-inline">
                                                             <?php

                                                            $checked="";
                                          
                                                            if("Masculino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante5" name="sexoIntegrante5" <?=$checked?>>Masculino</label> 
                                                            
                                                    </div>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6 ">
                                                    
                                                        <?php

                                                            $calle1="";
                                                            if(isset($listIntegrantes[4]['calle_integrante'])){
                                                                $calle1 = $listIntegrantes[4]['calle_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante5" name="calleIntegrante[]"  tabindex="62" value="<?=$calle1?>"   >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Edad:</label>
                                                    
                                                     <?php

                                                            $edad1="";
                                                            if(isset($listIntegrantes[4]['edad_integrante'])){
                                                                $edad1 = $listIntegrantes[4]['edad_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante5" maxlength="3" name="edadIntegrante[]" tabindex="57" value="<?=$edad1?>">
                                                    </div>
                                                    
                                                    <?php

                                                            $numero1="";
                                                            if(isset($listIntegrantes[4]['numero_integrante'])){
                                                                $numero1 = $listIntegrantes[4]['numero_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante5" maxlength="6"  name="numeroIntegrante[]" tabindex="63" value="<?=$numero1?>"  >
                                                    </div>
                                                    
                                                    
                                                <?php

                                                            $colonia1="";
                                                            if(isset($listIntegrantes[4]['colonia_integrante'])){
                                                                $colonia1 = $listIntegrantes[4]['colonia_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante5" maxlength="30"  name="coloniaIntegrante[]" tabindex="64"  value="<?=$colonia1?>">
                                                    </div>
                                                    
                                                    <?php

                                                            $cpostal1="";
                                                            if(isset($listIntegrantes[4]['cpostal_integrante'])){
                                                                $cpostal1 = $listIntegrantes[4]['cpostal_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante5" maxlength="5" name="codigoPostalIntegrante[]" tabindex="65" value="<?=$cpostal1?>">
                                                    </div>
                                                </div>

                                                
                                                       <?php

                                                            $telefono1="";
                                                            if(isset($listIntegrantes[4]['telefono_integrante'])){
                                                                $telefono1 = $listIntegrantes[4]['telefono_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Telefono:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante5" oninput="this.value=this.value.replace(/[^0-9-#\-S]+$/g,'');"   maxlength="10" name="telefonoIntegrante[]"  tabindex="58" value="<?=$telefono1?>">
                                                    </div>
                                                    
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             
                                            </div>
                                            
                                            
                                            
                                            
                                                      
                                     <?php
                                      $nombre2 = "";
                                        $display = "style='display:none'";
                                         if(isset($listIntegrantes[5]['nombre_integrante'])){
                                                                $nombre2 = $listIntegrantes[5]['nombre_integrante'];
                                                                if(trim($nombre2)!=""){
                                                                    $display =  "";
                                                                }
                                        }
                                                                   
                                      ?>

                                            <div id="view_integrante6"  <?=$display?> >   

                                               
 

                                               

                                            <hr>

                                               <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 6:</label>
                                                </div> 
                                                <div class="text-right"> <a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(6)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Nombre del Integrante:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" maxlength="25"   class="form-control input-sm" id="nombreIntegrante6" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" name="nombreIntegrante[]" tabindex="66" value="<?=$nombre2?>">
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">*Cargo del Integrante:</label>
                                                    <div class="col-sm-3 ">
                                                    
                                                        <?php

                                                            $cargo1="";
                                                            if(isset($listIntegrantes[5]['cargo_integrante'])){
                                                                $cargo1 = $listIntegrantes[5]['cargo_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                          <select class="form-control col-sm-6" id="cargoIntegrante6" name="cargoIntegrante[]" tabindex="72">
                                                            <option>Seleccione..</option>
                                                            
                                                             <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Presidente"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?> value="Presidente">PRESIDENTE</option>
                                                            
                                                            
                                                                
                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Secretario"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?> value="Secretario">SECRETARIO</option>
                                                            
                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Tesorero"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?>  value="Tesorero">TESORERO</option>
                                                            
                                                            
                                                            
                                                             <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Vocal"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>     
                                                            <option  <?=$selected?>  value="Vocal">VOCAL</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Primer Apellido:</label>
                                                    <div class="col-sm-3 ">
                                                    
                                                        <?php

                                                            $apellido1="";
                                                            if(isset($listIntegrantes[5]['appaterno_integrante'])){
                                                                $apellido1 = $listIntegrantes[5]['appaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        <input type="text"  maxlength="15"  class="form-control input-sm" id="apPIntegrante6" name="apPIntegrante[]"  oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" tabindex="67" value="<?=$apellido1?>" >
                                                    </div>
                                                    
                                                    
                                                       <?php

                                                            $checked="";
                                                            $firmaconstancia6 = "";
                                                            if(isset($listIntegrantes[5]['firma_constancia_registro_integrante'])){
                                                                $firmaconstancia6 = $listIntegrantes[5]['firma_constancia_registro_integrante'];
                                                            }
                                                            if("SI"==$firmaconstancia6){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>

                                                        <div class="firma_constancia_class">
                                                    <label for="textInput" class="col-sm-2">*Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3 ">
                                                       <label class="radio-inline"><input type="radio" value="SI"   <?=$checked?> id="firmaRegistroConstancia6" name="firmaRegistroConstancia6" tabindex="73">Si</label>
                                                        <label class="radio-inline">
                                                        
                                                          <?php
                                                    
                                                             $checked="";
                                                            if("NO"==$firmaconstancia6){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia6" <?=$checked?> name="firmaRegistroConstancia6">No</label> 
                                                    </div>
                                                </div>
                                                </div>

                                                <div class="form-group">
                                                
                                                    <?php

                                                            $apellido2="";
                                                            if(isset($listIntegrantes[5]['apmaterno_integrante'])){
                                                                $apellido2 = $listIntegrantes[5]['apmaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <label for="textInput" class="col-sm-2">*Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" maxlength="15"  class="form-control input-sm" id="apMIntegrante6" name="apMIntegrante[]" tabindex="68" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" value="<?=$apellido2?>">
                                                    </div>
                                                     <label  class="col-sm-2 ">*¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">
                                                    
                                                    <?php

                                                            $checked="";
                                                            $domicilioConocido1 = "";
                                                            if(isset($listIntegrantes[5]['domicilio_conocido_integrante'])){
                                                                $domicilioConocido1 = $listIntegrantes[5]['domicilio_conocido_integrante'];
                                                            }
                                                            if("SI"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante6" <?=$checked?> name="domicilioCortejadoIntegrante6" tabindex="74">Si</label>
                                                        <label class="radio-inline">
                                                        
                                                        <?php

                                                            $checked="";
                                                        
                                                            if("NO"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante6" name="domicilioCortejadoIntegrante6">No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Sexo:</label>
                                                       <div class="col-sm-3">
                                                       
                                                          
                                                        <?php

                                                            $checked="";
                                                              $sexo1 = "";
                                                            if(isset($listIntegrantes[5]['sexo_integrante'])){
                                                                $sexo1 = $listIntegrantes[5]['sexo_integrante'];
                                                            }
                                                            if("Femenino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" id="sexoIntegrante6"  <?=$checked?> name="sexoIntegrante6" tabindex="69">Femenino</label>
                                                        <label class="radio-inline">
                                                        
                                                    <?php

                                                            $checked="";
                                          
                                                            if("Masculino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante6" <?=$checked?> name="sexoIntegrante6" >Masculino</label> 
                                                            
                                                    </div>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                                                                        
                                                      <?php

                                                            $calle1="";
                                                            if(isset($listIntegrantes[5]['calle_integrante'])){
                                                                $calle1 = $listIntegrantes[5]['calle_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-6 ">
                                                        <input type="text" class="form-control input-sm" id="calleIntegrante6" name="calleIntegrante[]"  tabindex="75"   value="<?=$calle1?>" >
                                                    </div>
                                                </div>
                                                
                                                     <?php

                                                            $edad1="";
                                                            if(isset($listIntegrantes[5]['edad_integrante'])){
                                                                $edad1 = $listIntegrantes[5]['edad_integrante'];
                                                            }
                                                                   
                                                        ?>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Edad:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante6" maxlength="3" name="edadIntegrante[]" tabindex="70" value="<?=$edad1?>" >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                     
                                                    <?php

                                                            $numero1="";
                                                            if(isset($listIntegrantes[5]['numero_integrante'])){
                                                                $numero1 = $listIntegrantes[5]['numero_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante6" maxlength="6"  name="numeroIntegrante[]" tabindex="76" value="<?=$numero1?>" >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                     
                                                        <?php

                                                            $colonia1="";
                                                            if(isset($listIntegrantes[5]['colonia_integrante'])){
                                                                $colonia1 = $listIntegrantes[5]['colonia_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante6" maxlength="30"  name="coloniaIntegrante[]" tabindex="77"  value="<?=$colonia1?>" >
                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                       <?php

                                                            $cpostal1="";
                                                            if(isset($listIntegrantes[5]['cpostal_integrante'])){
                                                                $cpostal1 = $listIntegrantes[5]['cpostal_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante6" maxlength="5" name="codigoPostalIntegrante[]" tabindex="78"  value="<?=$cpostal1?>">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                   <?php

                                                            $telefono1="";
                                                            if(isset($listIntegrantes[5]['telefono_integrante'])){
                                                                $telefono1 = $listIntegrantes[5]['telefono_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <label for="textInput" class="col-sm-2">Telefono:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm " id="telefonoIntegrante6" oninput="this.value=this.value.replace(/[^0-9-#\-S]+$/g,'');"  maxlength="10" name="telefonoIntegrante[]"  tabindex="71" value="<?=$telefono1?>">
                                                    </div>
                                                    
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             
                                            </div>

                                        
                                        
                                        
                                <?php
                                      $nombre2 = "";
                                        $display = "style='display:none'";
                                         if(isset($listIntegrantes[6]['nombre_integrante'])){
                                                                $nombre2 = $listIntegrantes[6]['nombre_integrante'];
                                                                if(trim($nombre2)!=""){
                                                                    $display =  "";
                                                                }
                                        }
                                                                   
                                      ?>


                                            <div id="view_integrante7"  <?=$display?> >     

            

                                            <hr>

                                               <div class="form-group">
                                                <label for="textInput" class="col-sm-3 text-right ">INTEGRANTE 7:</label>
                                                </div>  
                                                 <div class="text-right"><a href="#primero" onClick="agregarNuevoIntegrante()"><img width="50" height="50" src="<?php echo asset_url()?>img/addPerson.jpg"></a>  <a href="#primero" onClick="quitRecurso(7)"><img width="50" height="50" src="<?php echo asset_url()?>img/quitPerson.jpg"></a></div>

                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Nombre del Integrante:</label>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm" id="nombreIntegrante7" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" name="nombreIntegrante[]" maxlength="25"   tabindex="79" value="<?=$nombre2?>">
                                                    </div>
                                                     <label for="textInput" class="col-sm-2">*Cargo del Integrante:</label>
                                                    <div class="col-sm-3" >
                                                    
                                                      <?php

                                                            $cargo1="";
                                                            if(isset($listIntegrantes[6]['cargo_integrante'])){
                                                                $cargo1 = $listIntegrantes[6]['cargo_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                          <select class="form-control col-sm-6" id="cargoIntegrante7" name="cargoIntegrante[]" tabindex="85">
                                                            <option>Seleccione..</option>
                                                            
                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Presidente"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?> value="Presidente">PRESIDENTE</option>
                                                            
                                                              <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Secretario"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option  <?=$selected?>  value="Secretario">SECRETARIO</option>
                                                            
                                                            <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Tesorero"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>
                                                            <option <?=$selected?> value="Tesorero">TESORERO</option>
                                                             
                                                             
                                                                 <?php  
                                                                $selected = "";
                                                                if($cargo1 == "Vocal"){
                                                                    $selected = "selected";
                                                                }
                                                            ?>     
                                                            <option <?=$selected?> value="Vocal">VOCAL</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Primer Apellido:</label>
                                                    <div class="col-sm-3 ">
                                                    <?php

                                                            $apellido1="";
                                                            if(isset($listIntegrantes[6]['appaterno_integrante'])){
                                                                $apellido1 = $listIntegrantes[6]['appaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        <input type="text" maxlength="15"   class="form-control input-sm" id="apPIntegrante7" name="apPIntegrante[]" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" tabindex="80" value="<?=$apellido1?>"> 
                                                    </div>
<div class="firma_constancia_class">
                                                    <label for="textInput" class="col-sm-2">*Firma de Constancia Registro:</label>
                                                    <div class="col-sm-3 ">
                                                    
                                                        
                                                       <?php

                                                            $checked="";
                                                            $firmaconstancia7 = "";
                                                            if(isset($listIntegrantes[6]['firma_constancia_registro_integrante'])){
                                                                $firmaconstancia7 = $listIntegrantes[6]['firma_constancia_registro_integrante'];
                                                            }
                                                            if("SI"==$firmaconstancia7){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                       <label class="radio-inline"><input type="radio" value="SI" id="firmaRegistroConstancia7" name="firmaRegistroConstancia7" tabindex="86" <?=$checked?>>Si</label>
                                                        <label class="radio-inline">
                                                        
                                                         <?php
                                                    
                                                             $checked="";
                                                            if("NO"==$firmaconstancia7){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="firmaRegistroConstancia7" <?=$checked?>  name="firmaRegistroConstancia7">No</label> 
                                                    </div>
                                                </div>
                                            </div>

                                                <div class="form-group">
                                                
                                                 <?php

                                                            $apellido2="";
                                                            if(isset($listIntegrantes[6]['apmaterno_integrante'])){
                                                                $apellido2 = $listIntegrantes[6]['apmaterno_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <label for="textInput" class="col-sm-2">*Segundo Apellido:</label>
                                                    <div class="col-sm-3" >
                                                        <input type="text" maxlength="15"   class="form-control input-sm" id="apMIntegrante7" value="<?=$apellido2?>" oninput="this.value=this.value.replace(/[^a-zA-Z-#\s]+$/g,'');" name="apMIntegrante[]" tabindex="81" >
                                                    </div>
                                                     <label  class="col-sm-2 ">*¿Domicilio Conocido del Integrante?:</label>
                                                    <div class="col-sm-3">
                                                    
                                                       <?php

                                                            $checked="";
                                                            $domicilioConocido1 = "";
                                                            if(isset($listIntegrantes[6]['domicilio_conocido_integrante'])){
                                                                $domicilioConocido1 = $listIntegrantes[6]['domicilio_conocido_integrante'];
                                                            }
                                                            if("SI"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="SI" id="domicilioCortejadoIntegrante7" <?=$checked?> name="domicilioCortejadoIntegrante7" tabindex="87">Si</label>
                                                        <label class="radio-inline">
                                                        
                                                            <?php

                                                            $checked="";
                                                        
                                                            if("NO"==$domicilioConocido1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="NO" id="domicilioCortejadoIntegrante7"   <?=$checked?> name="domicilioCortejadoIntegrante7">No</label> 
                                                            
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Sexo:</label>
                                                       <div class="col-sm-3">

                                                          <?php

                                                            $checked="";
                                                              $sexo1 = "";
                                                            if(isset($listIntegrantes[6]['sexo_integrante'])){
                                                                $sexo1 = $listIntegrantes[6]['sexo_integrante'];
                                                            }
                                                            if("Femenino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
 
                                                        <label class="radio-inline"><input type="radio" value="Femenino" <?=$checked?>id="sexoIntegrante7" name="sexoIntegrante7" tabindex="82">Femenino</label>
                                                        <label class="radio-inline">

                                                       <?php

                                                            $checked="";

                                          
                                                            if("Masculino"==$sexo1){
                                                                $checked = "checked";
                                                            }
                                                                   
                                                        ?>
                                                            <input type="radio" value="Masculino" id="sexoIntegrante7" name="sexoIntegrante7" <?=$checked?>>Masculino</label> 
                                                            
                                                    </div>

                                                      <label for="textInput" class="col-sm-2">Calle:</label>
                                                    <div class="col-sm-6 ">
                                                       <?php

                                                            $calle1="";
                                                            if(isset($listIntegrantes[6]['calle_integrante'])){
                                                                $calle1 = $listIntegrantes[6]['calle_integrante'];
                                                            }
                                                                   
                                                        ?>                                                        

                                                        <input type="text" class="form-control input-sm" id="calleIntegrante7" name="calleIntegrante[]"  tabindex="88"  value="<?=$calle1?>" >
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">*Edad:</label>
                                                       <?php

                                                            $edad1="";
                                                            if(isset($listIntegrantes[6]['edad_integrante'])){
                                                                $edad1 = $listIntegrantes[6]['edad_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-3 ">
                                                        <input type="text" class="form-control input-sm " id="edadIntegrante7" maxlength="3" name="edadIntegrante[]" tabindex="83" value="<?=$edad1?>" >
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Número:</label>
                                                          <?php

                                                            $numero1="";
                                                            if(isset($listIntegrantes[6]['numero_integrante'])){
                                                                $numero1 = $listIntegrantes[6]['numero_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="numeroIntegrante7" maxlength="6"  name="numeroIntegrante[]"  tabindex="89" value="<?=$numero1?>">
                                                    </div>
                                                     <label for="textInput" class="col-sm-1">Colonia:</label>
                                                    <div class="col-sm-1 ">
                                                        <?php

                                                            $colonia1="";
                                                            if(isset($listIntegrantes[6]['colonia_integrante'])){
                                                                $colonia1 = $listIntegrantes[6]['colonia_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        <input type="text" class="form-control input-sm" id="coloniaIntegrante7" maxlength="30"  name="coloniaIntegrante[]" tabindex="90" value="<?=$colonia1?>">

                                                    </div>
                                                   <label for="textInput" class="col-sm-1">C.Postal:</label>
                                                     <?php

                                                            $cpostal1="";
                                                            if(isset($listIntegrantes[6]['cpostal_integrante'])){
                                                                $cpostal1 = $listIntegrantes[6]['cpostal_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                    <div class="col-sm-1 ">
                                                        <input type="text" class="form-control input-sm" id="codigoPostalIntegrante7" maxlength="5" name="codigoPostalIntegrante[]"  tabindex="91" value="<?=$cpostal1?>">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="textInput" class="col-sm-2">Telefono:</label>
                                                    <div class="col-sm-3 ">
                                                           <?php

                                                            $telefono1="";
                                                            if(isset($listIntegrantes[6]['telefono_integrante'])){
                                                                $telefono1 = $listIntegrantes[6]['telefono_integrante'];
                                                            }
                                                                   
                                                        ?>
                                                        <input type="text" class="form-control input-sm" id="telefonoIntegrante7" oninput="this.value=this.value.replace(/[^0-9-#\-S]+$/g,'');"   maxlength="10" name="telefonoIntegrante[]"  tabindex="84" value="<?=$telefono1?>">
                                                    </div>
                                                    
                                                </div>
                                                

                                                
                                                <br><br> 
                                                
                                             </div>


                                           


 
                                            

                                                  <input type="hidden" name="id_comite_integrantes" id="id_comite_integrantes">
                                               

                                       
                                                
                                               
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                        




                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- fin Documentos del comité -->

              
             

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>







<form id="TheForm" method="post" action="<?php base_url()?>/davinci2/TCPDF/examples/formato_comite.php" target="TheWindow">
<input type="hidden" name="id_comite" value="<?=$editData['id_comite']?>" />

</form>
















    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>

    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
 
    <script type="text/javascript" src="<?php echo asset_url();?>js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="<?php echo asset_url();?>css/bootstrap-multiselect.css" type="text/css"/>

    
    <link href="<?php echo asset_url();?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <script src="<?php echo asset_url();?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   
    


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>

    <script language="javascript">

        var v_esquema = 0;
        var v_recurso = 0;


         $(document).ready(function () {


        

            //populate_localidad($("#listObras2 option:selected").val());



                      <?php
       
            if($editData['onlyView'] == 1){

                 echo "deshabilitar();";
                ?>
                var onlyView = 1;
                $('#btnGuardar').hide();

            <?php
                }
            ?>


        


           // alert($("#countResourceIntegrantes").val());

            
          /*

          $("#cargoIntegrante1").change(function(){
            alert($('#cargoIntegrante1').val());

            if($('#cargoIntegrante1').val()=="Presidente"){
                $('#cargoIntegrante2 option[value=Presidente]').attr('disabled', 'disabled').hide();
                 $('#cargoIntegrante3 option[value=Presidente]').attr('disabled', 'disabled').hide();
                  $('#cargoIntegrante4 option[value=Presidente]').attr('disabled', 'disabled').hide();
                  $('#cargoIntegrante5 option[value=Presidente]').attr('disabled', 'disabled').hide();
                  $('#cargoIntegrante6 option[value=Presidente]').attr('disabled', 'disabled').hide();
                  $('#cargoIntegrante7 option[value=Presidente]').attr('disabled', 'disabled').hide();
            }

             if($('#cargoIntegrante1').val()=="Secretario"){
                $('#cargoIntegrante2 option[value=Presidente]').attr('disabled', 'Secretario').show();
                 $('#cargoIntegrante3 option[value=Secretario]').attr('disabled', 'Secretario').hide();
                  $('#cargoIntegrante4 option[value=Secretario]').attr('disabled', 'Secretario').hide();
                  $('#cargoIntegrante5 option[value=Secretario]').attr('disabled', 'Secretario').hide();
                  $('#cargoIntegrante6 option[value=Secretario]').attr('disabled', 'Secretario').hide();
                  $('#cargoIntegrante7 option[value=Secretario]').attr('disabled', 'Secretario').hide();
            }


              if($('#cargoIntegrante1').val()=="Tesorero"){
                $('#cargoIntegrante2 option[value=Tesorero]').attr('disabled', 'Tesorero').hide();
                 $('#cargoIntegrante3 option[value=Tesorero]').attr('disabled', 'Tesorero').hide();
                  $('#cargoIntegrante4 option[value=Tesorero]').attr('disabled', 'Tesorero').hide();
                  $('#cargoIntegrante5 option[value=Tesorero]').attr('disabled', 'Tesorero').hide();
                  $('#cargoIntegrante6 option[value=Tesorero]').attr('disabled', 'Tesorero').hide();
                  $('#cargoIntegrante7 option[value=Tesorero]').attr('disabled', 'Tesorero').hide();
            }


              if($('#cargoIntegrante2').val()=="Presidente"){
                $('#cargoIntegrante1 option[value=Presidente]').attr('disabled', 'disabled').hide();
                 $('#cargoIntegrante3 option[value=Presidente]').attr('disabled', 'disabled').hide();
                  $('#cargoIntegrante4 option[value=Presidente]').attr('disabled', 'disabled').hide();
                  $('#cargoIntegrante5 option[value=Presidente]').attr('disabled', 'disabled').hide();
                  $('#cargoIntegrante6 option[value=Presidente]').attr('disabled', 'disabled').hide();
                  $('#cargoIntegrante7 option[value=Presidente]').attr('disabled', 'disabled').hide();
            }

             if($('#cargoIntegrante2').val()=="Secretario"){
                $('#cargoIntegrante1 option[value=Presidente]').attr('disabled', 'Presidente').show();
                 $('#cargoIntegrante3 option[value=Secretario]').attr('disabled', 'Secretario').hide();
                  $('#cargoIntegrante4 option[value=Secretario]').attr('disabled', 'Secretario').hide();
                  $('#cargoIntegrante5 option[value=Secretario]').attr('disabled', 'Secretario').hide();
                  $('#cargoIntegrante6 option[value=Secretario]').attr('disabled', 'Secretario').hide();
                  $('#cargoIntegrante7 option[value=Secretario]').attr('disabled', 'Secretario').hide();
            }*/

 
 


            


            /*var selected_option = $('#fnivel').val();
            if(selected_option == '2'){
                $("#fnivel2").removeClass("hide");
                $("#fnivel2").attr('pk','1');
            }
                if(selected_option != '2'){
                $("#fnivel2").addClass("hide");
                $("#fnivel2").removeAttr('pk');
            }
  });*/
 // });


         $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */


            if($("#programaSeleccionado").val() == "todos"){
            //alert("Debe seleccionar un Programa, antes de guardar Registro.");
            swal("Advertencia!", "Debe seleccionar un Programa, antes de guardar Registro.", "warning");
            $("#btnGuardar").fadeOut("slow");
          }



          loadCatalogFunctions();

        






            $("#fechaConstitucion").keypress(function(event) {event.preventDefault();});
 
 
             $('#fechaConstitucion').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });




                procesarCambio();
                changeProgram("undefined");
 



                $('#upload').on('click', function () {
                    var file_data = $('#file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    $.ajax({
                        url: 'registro_comite/upload_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                            $('#msg').html(response); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });







        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                monthsTitle: "Meses",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
            }(jQuery));



            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();

            $('#edadIntegrante1').numeric();
            $('#edadIntegrante2').numeric();
            $('#edadIntegrante3').numeric();
            $('#edadIntegrante4').numeric();
            $('#edadIntegrante5').numeric();
            $('#edadIntegrante6').numeric();
            $('#edadIntegrante7').numeric();

            $('#numeroIntegrante1').numeric();
            $('#numeroIntegrante2').numeric();
            $('#numeroIntegrante3').numeric();
            $('#numeroIntegrante4').numeric();
            $('#numeroIntegrante5').numeric();
            $('#numeroIntegrante6').numeric();
            $('#numeroIntegrante7').numeric();

            $('#codigoPostalIntegrante1').numeric();
            $('#codigoPostalIntegrante2').numeric();
            $('#codigoPostalIntegrante3').numeric();
            $('#codigoPostalIntegrante4').numeric();
            $('#codigoPostalIntegrante5').numeric();
            $('#codigoPostalIntegrante6').numeric();
            $('#codigoPostalIntegrante7').numeric();
             
            



           


               $( function() {
            $( "#fechaConstitucion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
           
            
          } );


                //iconos
                $('.constitucionIcon').click(function() {
                    $("#fechaConstitucion").focus();
              });



               

                $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante1]:checked').val() == "SI"){

                        $("#calleIntegrante1").prop( "readOnly", true );
                        $("#numeroIntegrante1").prop( "readOnly", true );
                      //  $("#colonia").prop( "disabled", true );
                        $("#codigoPostalIntegrante1").prop( "readOnly", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante1]:checked').val() == "NO"){

                        $("#calleIntegrante1").prop( "readOnly", false );
                        $("#numeroIntegrante1").prop( "readOnly", false );
                        //$("#colonia").prop( "disabled", false );
                        $("#codigoPostalIntegrante1").prop( "readOnly", false );
                        
                    }

                });




                $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante2]:checked').val() == "SI"){

                        $("#calleIntegrante2").prop( "readOnly", true );
                        $("#numeroIntegrante2").prop( "readOnly", true );
                      //  $("#colonia").prop( "disabled", true );
                        $("#codigoPostalIntegrante2").prop( "readOnly", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante2]:checked').val() == "NO"){

                        $("#calleIntegrante2").prop( "readOnly", false );
                        $("#numeroIntegrante2").prop( "readOnly", false );
                        //$("#colonia").prop( "disabled", false );
                        $("#codigoPostalIntegrante2").prop( "readOnly", false );
                        
                    }

                });


                $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante3]:checked').val() == "SI"){

                        $("#calleIntegrante3").prop( "readOnly", true );
                        $("#numeroIntegrante3").prop( "readOnly", true );
                      //  $("#colonia").prop( "disabled", true );
                        $("#codigoPostalIntegrante3").prop( "readOnly", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante3]:checked').val() == "NO"){

                        $("#calleIntegrante3").prop( "readOnly", false );
                        $("#numeroIntegrante3").prop( "readOnly", false );
                        //$("#colonia").prop( "disabled", false );
                        $("#codigoPostalIntegrante3").prop( "readOnly", false );
                        
                    }

                });




                $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante4]:checked').val() == "SI"){

                        $("#calleIntegrante4").prop( "readOnly", true );
                        $("#numeroIntegrante4").prop( "readOnly", true );
                      //  $("#colonia").prop( "disabled", true );
                        $("#codigoPostalIntegrante4").prop( "readOnly", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante4]:checked').val() == "NO"){

                        $("#calleIntegrante4").prop( "readOnly", false );
                        $("#numeroIntegrante4").prop( "readOnly", false );
                        //$("#colonia").prop( "disabled", false );
                        $("#codigoPostalIntegrante4").prop( "readOnly", false );
                        
                    }

                });




                $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante5]:checked').val() == "SI"){

                        $("#calleIntegrante5").prop( "readOnly", true );
                        $("#numeroIntegrante5").prop( "readOnly", true );
                      //  $("#colonia").prop( "disabled", true );
                        $("#codigoPostalIntegrante5").prop( "readOnly", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante5]:checked').val() == "NO"){

                        $("#calleIntegrante5").prop( "readOnly", false );
                        $("#numeroIntegrante5").prop( "readOnly", false );
                        //$("#colonia").prop( "disabled", false );
                        $("#codigoPostalIntegrante5").prop( "readOnly", false );
                        
                    }

                });



                $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante6]:checked').val() == "SI"){

                        $("#calleIntegrante6").prop( "readOnly", true );
                        $("#numeroIntegrante6").prop( "readOnly", true );
                      //  $("#colonia").prop( "disabled", true );
                        $("#codigoPostalIntegrante6").prop( "readOnly", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante6]:checked').val() == "NO"){

                        $("#calleIntegrante6").prop( "readOnly", false );
                        $("#numeroIntegrante6").prop( "readOnly", false );
                        //$("#colonia").prop( "disabled", false );
                        $("#codigoPostalIntegrante6").prop( "readOnly", false );
                        
                    }

                });



                $('input:radio').change(function() {
                    if($('input:radio[name=domicilioCortejadoIntegrante7]:checked').val() == "SI"){

                        $("#calleIntegrante7").prop( "readOnly", true );
                        $("#numeroIntegrante7").prop( "readOnly", true );
                      //  $("#colonia").prop( "disabled", true );
                        $("#codigoPostalIntegrante7").prop( "readOnly", true );
                        
                    }


                    if($('input:radio[name=domicilioCortejadoIntegrante7]:checked').val() == "NO"){

                        $("#calleIntegrante7").prop( "readOnly", false );
                        $("#numeroIntegrante7").prop( "readOnly", false );
                        //$("#colonia").prop( "disabled", false );
                        $("#codigoPostalIntegrante7").prop( "readOnly", false );
                        
                    }

                });



            

          });   




        function deshabilitar(){


            <?php
            
              echo  '$("#form_datos_comité :input").prop("disabled", true);';
        

              echo  '$("#form_funciones :input").prop("disabled", true);';
              echo  '$("#form_documentoscomite :input").prop("disabled", true);';
              echo  '$("#form_preguntasClave :input").prop("disabled", true);';
              echo  '$("#form_integrante1 :input").prop("disabled", true);';
           
            ?>

        }


        function editar(){


            $("#btnGuardar").show("slow");

            <?php
                
            if($editData['onlyView']){

                echo  '$("#form_datos_comité :input").prop("disabled", false);';
              echo  '$("#form_funciones :input").prop("disabled", false);';
              echo  '$("#form_documentoscomite :input").prop("disabled", false);';
              echo  '$("#form_preguntasClave :input").prop("disabled", false);';
              echo  '$("#form_integrante1 :input").prop("disabled", false);';
                }

              ?>

              onlyView = 0;


        }
 








        
    function imprimir_Formato(){
       // alert("hola");

     //  window.open( 'pdfexample5', '_blank' ); 

       window.open('', 'TheWindow');
document.getElementById('TheForm').submit();
    }

  
   function populate_localidad(fromMunicipio) {
             
                var cantElement = 0;
                $('#localidad').empty();
                $('#localidad').append("<option>Cargando ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_comite/populate_localidad_proyecto') ?>/" + fromMunicipio,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#localidad').empty();
                        $('#localidad').append("<option value='0'>Selecciona Localidad..</option>");

                     /*  $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });*/


                        var selected = "";





                        $.each(data, function (i, name) {

                            <?php
                            $fk_localidad="0";
                            if(isset($editData['fk_localidad'])){
                                $fk_localidad = $editData['fk_localidad'];
                            }

                            ?>

                            
                        if(data[i].id == <?=$fk_localidad?>){
                            $('#localidad').append('<option selected value="' + data[i].id + '">' + data[i].localidad.toUpperCase() + '</option>');
                         }else{

                          $('#localidad').append('<option value="' + data[i].id + '">' + data[i].localidad.toUpperCase() + '</option>');
                       // }
                      
                        }
                              cantElement++;
                        });

                         
                    }
                });
        }   




         function sumaPeople(){
            
            var total = 0;

                 
                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                   //     alert("value:" + this.value);
                        total += parseFloat(this.value);
                    }
                }); 


                //total = parseFloat($("#total_people_womans").val()) + parseFloat($("#total_people_mans").val());

            $("#total_personas").val(total);




        }




        function preGuardado(){

            $("#nombreIntegrante_tmp").val($("#nombreIntegrante1").val());

                  var dataForms = $("#form_datos_comité").serialize() + "&" + $("#form_documentoscomite").serialize() + "&" + $("#form_funciones").serialize() + "&" + $("#form_preguntasClave").serialize() + "&" + $("#form_integrante1").serialize();
  
            $.ajax({
                    url: "<?php echo site_url('registro_comite/guardarDatosDelcomite') ?>",
                    type: "POST",
                    data: dataForms,
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;


                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;

                            console.log(data);
                            console.log(obj);
                        });



                        $("#id_comite").val(message);
                        $("#metodo").val("edit");

                        $("#id_comite_documentos").val(message);
                        $("#id_comite_preguntasclave").val(message);
                        $("#id_comite_funciones").val(message);
                        $("#id_comite_integrantes").val(message);



        
 
                    }


                });


 
            
        }


        function guardar(){

                //alert("#listObras").v
                //alert($("#listObras option:selected").text());
                //alert($("#listObras option:selected").val());

                validaCamposComite();
                validaIntegrantes();


                //if($("#msgError").val()!==""){
                if(msgError!=""){
                    swal("Error!", msgError, "error");
                    return;
                }

            

                preGuardado();


                 /*alert("id_comite " + $("#id_comite_documentos").val());
                alert("metodo " + $("#metodo").val());*/


            
            

                swal("Bien hecho!", "El comité ha sido guardado.", "success");

          
        }

        function quitRecurso(elem){

             var noElem = elem.toString();

              var qtyIntegrantes = $("#countResourceIntegrantes").val();
              $("#countResourceIntegrantes").val(qtyIntegrantes-1)

                 var cad = "#nombreIntegrante"+noElem;
                   elem = cad.toString();
                


                   $(elem).val("");

                    var cad = "#cargoIntegrante"+noElem;
                    elem = cad.toString();
                    $(elem).val('0')
 

             $("#view_integrante"+noElem).hide('slow');


         }

        var msgError = "";
        $("#msgError").val("");



        function validaCamposComite(){


            msgError = "";
                    $("#nombreComite").css({"background-color": "white"});  
                    $("#fechaConstitucion").css({"background-color": "white"});        

                    $('input[name=proporcionoResumenEjecutivo]').css({"background-color": "white"});    
                    $('input[name=disposicionQuejas]').css({"background-color": "white"});
                    $('input[name=servidorPublicoNombre]').css({"background-color": "white"});
                      $("#servidorPublicoNombre").css({"background-color": "white"});
                      $("#servidorPublicoCargo").css({"background-color": "white"});
                       $("#localidad").css({"background-color": "white"});
             

                if( $("#nombreComite").val() == ""){
                    msgError+= "- Debe tener Nombre del Comite, \n";
                    $("#nombreComite").css({"background-color": "yellow"});
                }

                 if( $("#fechaConstitucion").val() == ""){
                    msgError+= "- Falta poner Fecha de Constitución:, \n";
                    $("#fechaConstitucion").css({"background-color": "yellow"});
                }

                
                if(   $("#listObras2 option:selected").val() == 0 ){
                     msgError+= "- Debe seleccionar al menos una Accion de Conexion. \n";
                      $("#listObras2").css({"background-color": "yellow"});
                    
                }

                    if(   $("#localidad option:selected").val() == 0 ){
                     msgError+= "- Debe seleccionar una Localidad. \n";
                      $("#localidad").css({"background-color": "yellow"});
                    
                }

                if( $("#servidorPublicoNombre").val() == ""){
                    msgError+= "- Falta Nombre del Servidor Publico:, \n";
                    $("#servidorPublicoNombre").css({"background-color": "yellow"});
                }

               if( $("#servidorPublicoCargo").val() == ""){
                    msgError+= "- Falta Cargo del Servidor Publico:, \n";
                    $("#servidorPublicoCargo").css({"background-color": "yellow"});
                }

                
                
                
                
                if($("#recurso_hidden").val() == 1) {
                    
                    if( $('input[name="funcionesComite[]"]:checked').length < 1){
                         msgError+= "- Debe seleccionar al menos una Funcion aplicada en el Comité. \n";
                        
                    }
                }
                

                if( $('input[name=proporcionoResumenEjecutivo]:checked').val() == undefined){
                        msgError+= "- Debe contestar la Primera pregunta de Pregunta Clave, \n";
                        $('input[name=proporcionoResumenEjecutivo]').css({"background-color": "yellow"});
                       
                }


                if( $('input[name=disposicionQuejas]:checked').val() == undefined){
                        msgError+= "- Debe contestar la Segunda pregunta de Pregunta Clave, \n";
                        $('input[name=disposicionQuejas]').css({"background-color": "yellow"});
                       
                }


                
                //alert($('input:checkbox:checked').length;
                //alert($('input[name="funcionesComite[]"]:checked').length);

                
               


        } 


        function validaIntegrantes(){

            
            var qtyIntegrantes = $("#countResourceIntegrantes").val(); 

            var listCargo = [];

            var secretario = 0;
            var tesorero = 0;
            var presidente = 0;
            

            //qtyIntegrantes


            /*if(qtyIntegrantes==2){
                $(cargoIntegrante1
            }*/
            

                $('input[name="nombreIntegrante[]"]').each(function(){
                      $(elem).css({"background-color": "white"});
                    });



                $('input[name="apPIntegrante[]"]').each(function(){
                      $(elem).css({"background-color": "white"});
                    });
          
                $('input[name="apMIntegrante[]"]').each(function(){
                      $(elem).css({"background-color": "white"});
                    });
          
 

 


                if(qtyIntegrantes>1){



                    var n, elem;
                for(i=1; i<=qtyIntegrantes; i++){
                    


                   //Nombre
                   var cad = "#nombreIntegrante"+i;
                   elem = cad.toString();

                   $(elem).css({"background-color": "white"});
                


                   if($(elem).val()==""){
                     msgError+= "- Debe tener Nombre del Integrante "+i+", \n";
                      $(elem).css({"background-color": "yellow"});
                      $(elem).focus();

                   }


                   //APPaterno
                   var cad = "#apPIntegrante"+i;
                   elem = cad.toString();

                    $(elem).css({"background-color": "white"});
                    




                   if($(elem).val()==""){
                     msgError+= "- Debe tener Apellido Paterno del Integrante "+i+", \n";
                      $(elem).css({"background-color": "yellow"});
                      $(elem).focus();

                   }

                   //APMaterno
                   var cad = "#apMIntegrante"+i;
                   elem = cad.toString();

                     $(elem).css({"background-color": "white"});
                  
                
 
                   if($(elem).val()==""){
                     msgError+= "- Debe tener Apellido Materno del Integrante "+i+", \n";
                      $(elem).css({"background-color": "yellow"});
                      $(elem).focus();

                   }

                   var num = i;
                   num = num.toString();
                        $('input[name=sexoIntegrante'+i+']:checked').css({"background-color": "white"});

                   //Sexo
                    if( $('input[name=sexoIntegrante'+1+']:checked').val() == undefined){
                        msgError+= "- Debe seleccionar sexo del Integrante "+i+", \n";
                        $('#sexoIntegrante'+i).css({"background-color": "yellow"});
                       
                    }

                    //edad
                  var cad = "#edadIntegrante"+i;
                   elem = cad.toString();

                   $(elem).css({"background-color": "white"});
                 
                    if( $(elem).val() == ""){
                        msgError+= "- Debe registrar edad del Integrante "+i+", \n";
                        $(elem).css({"background-color": "yellow"});
                        
                    }


                    //telefonotelefonoIntegrante1
                  /* var cad = "#telefonoIntegrante"+i;
                   elem = cad.toString();
                       $(elem).css({"background-color": "white"});
               

                
                    if( $(elem).val() == ""){
                        msgError+= "- Debe registrar Telefono del Integrante "+i+", \n";
                        $(elem).css({"background-color": "yellow"});
                        
                    }*/
 
                    //FirmaCOnstancia
                     $('input[name=firmaRegistroConstancia'+i+']:checked').css({"background-color": "white"});

              
                    if( $('input[name=firmaRegistroConstancia'+i+']:checked').val() == undefined && v_recurso == 1 && v_esquema == 0){
                        msgError+= "- Debe registrar Firma de Constancia Registro "+i+", \n";
                        $('input[name=firmaRegistroConstancia'+i+']:checked').css({"background-color": "yellow"});
                         
                    }



                    //cortejadoCalle
                      $('input[name=domicilioCortejadoIntegrante'+i+']:checked').css({"background-color": "white"});


                    if( $('input[name=domicilioCortejadoIntegrante'+i+']:checked').val() == undefined){
                        msgError+= "- Debe seleccionar Domicilio Conocido del Integrante "+i+", \n";
                        $('input[name=domicilioCortejadoIntegrante'+i+']:checked').css({"background-color": "yellow"});
                         
                    }


                    //Cargo
                    var cad = "#cargoIntegrante"+i;
                    elem = cad.toString();
                            $(elem).css({"background-color": "white"});


                    if($(elem).val()=="Seleccione.."){
                        $(elem).css({"background-color": "yellow"});
                         $(elem).focus();
                           msgError+= "- Debe seleccionar cargo del Integrante "+i+", \n";


                    }else{
                        listCargo.push($(elem).val());
                    }


                }

            }else{
                 msgError+= "- Debe tener al menos 2 Integrantes en el Comité. \n";
            }   

            //alert(listCargo.length);

               for(i=0; i<listCargo.length; i++)
               {
                //console.log(listCargo[i]);
                if("Secretario"==listCargo[i]){
                    secretario++;
                }

                if("Presidente"==listCargo[i]){
                    presidente++;
                }

                if("Tesorero"==listCargo[i]){
                    tesorero++;
                }
                 
               }
                //console.log(listCargo);

                if(presidente>1){
                     msgError+= "- Hay mas de 2 cargos de Presidente, solo se puede 1. \n";
                }

                if(secretario>1){
                     msgError+= "- Hay mas de 2 cargos de Secretario, solo se puede 1. \n";
                }

                  if(tesorero>1){
                     msgError+= "- Hay mas de 2 cargos de Tesorero, solo se puede 1. \n";
                }
                                                
        }


        function populate_list_obras(fk_program){

 

           <?php

            $accion_conexion = 0;
            if(isset($editData['accion_conexion']) && $editData['accion_conexion']!=""){

                $accion_conexion = $editData['accion_conexion'];

            }

           ?>


           $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('registro_proyecto/ajax_select_obras') ?>/" + fk_program+"/"+<?=$accion_conexion?>,
                            //contentType: "application/json;charset=utf-8",
                            //dataType: 'json',
                            success: function (response) {
                                $('#listObras2').empty();

                             // $('#listObras2').multiselect('destroy');
                                $('#listObras2').append(response);

                                $('#listObras2').multiselect({
                       
                                        buttonWidth: '300px',
                                        maxHeight: 200,
                                        enableCaseInsensitiveFiltering: true,

                                    });

 
                                $('#listObras2').multiselect('rebuild');
                                
                                   //$('#listObras2')..multiselect('rebuild');
                                
                                       populate_localidad($("#listObras2 option:selected").val());


                                
                                 
                            }
                        });

        }





 /* 
        function imprimirFormato2(){

alert("hola");

            //  window.open( 'pdfexample5', '_blank' ); 

             // return;

          var parameters = { datos : '' };
 
            $.ajax({
               url: "<?php echo site_url('pdfexample5') ?>",
                type: 'POST',
                
                data : parameters,
                success: function(result) {
                   // console.log(result);

                   //location.href="exportacion.pdf";
                   window.open( 'pdfexample5', '_blank' ); 
                }
            });





 

        } */



        function loadCatalogFunctions(){

            var resourceSelected = $("#recurso_sel option:selected").val();     
                 
                $("#modulo_funciones_federal").show();
                if(resourceSelected == 2 || v_esquema == 1){ 
                    $("#modulo_funciones_estatal").show("slow");
                    $("#modulo_preguntas_funciones").hide();

                }else{           
                     
                    //$("#modulo_funciones_federal").show("slow");
                    $("#modulo_preguntas_funciones").show("slow");
                    $("#modulo_funciones_estatal").hide();
                }
        }


        function changeProgram(checked){



                if(checked!="undefined"){
               
                swal({
                  title: "Estás seguro de querer cambiar Programa?",
                  text: "",
                  icon: "warning",
                  buttons: true,
                  dangerMode: false, 
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Listo!, Se cambio programa exitosamente", {
                      icon: "success",
                    });
                    //location.reload(true);

            procesarCambio();


                  } else {
                    swal("Lo dejamos como estaba..");
                  }
                });
            }



           
               
        }


        function procesarCambio(){


             var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }

/*
                  if(id_programSelected!="todos"){
                   $("#btnGuardar").fadeIn("slow");
                }*/


               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                            recurso = obj.recurso;
                            esquema = obj.esquema;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                   //$("#titleProgram").fadeIn("slow");

                 //  $("#esquema").val(esquema);


                    if(recurso == 1){
                         $("#patcs_menu").hide();
                         $("#petcs_menu").show("slow");
                             $("#documentos_menu").show("slow");

                          $("#seguimiento_menu").hide("slow");
                          $("#recurso_hidden").val(1);
                          $("#btnImprimir").hide();

                             $("#reuniones_menu").show("slow");
                           $("#informes_menu").show("slow");
                             $("#formatos_menu").hide("slow");

                              $("#divConstanciaFirmada").show("slow");
                             $("#divEscritoLibre").show("slow");
                               $(".firma_constancia_class").show("slow");
                    }

                    v_recurso = recurso;
                    
                    v_esquema = 0;

                    if(esquema==1){
                        v_esquema = 1;
                    }
                     

                    if(recurso == 2 || v_esquema == 1){
                        $("#petcs_menu").hide();
                        $("#patcs_menu").show("slow"); 
                            $("#documentos_menu").show("slow");
                        
                        $("#seguimiento_menu").show("slow");  
                        $("#recurso_hidden").val(2);   

                         $("#reuniones_menu").hide("slow");
                           $("#informes_menu").hide("slow");
                             $("#formatos_menu").show("slow");

                             $("#divConstanciaFirmada").hide("slow");
                             $("#divEscritoLibre").hide("slow");
                             $(".firma_constancia_class").hide("slow");



            <?php
            if($editData["metodo"]=="view" || $editData["metodo"]=="edit") {
            ?>
                        $("#btnImprimir").show();    


            
            <?php

                }
            
            ?>                  
                         
                    }
                            
                      loadCatalogFunctions();

    
                    }
                });

 
          
            populate_list_obras(id_programSelected);



        }



        function agregarNuevoIntegrante(){
 
            var cadenaHTML;
            var contador;


            if($("#countResourceIntegrantes").val()<7){
                 contador = parseInt($("#countResourceIntegrantes").val()) + 1;
     
                 var label = "view_integrante"+contador;
                 
                 $("#"+label).show("slow"); 

                  
                 
                $("#countResourceIntegrantes").val(contador);
            }
       

        }

       

       function resetearForma(){
 
           
           location.href="<?php echo site_url();?>/registro_comite";
            /*$(".form-control").each(function() {
               $(this).val("");  */
                
   
          
        }

        


        function cleanElement(){

            swal({
              title: "Estas seguro de Crear nuevo Registro?",
              text: "",
              icon: "warning",
              buttons: true,
              dangerMode: false,
            })
            .then((willDelete) => {
              if (willDelete) {
                swal("Listo!, Empezamos de nuevo la captura", {
                  icon: "success",
                });
                resetearForma();

              } else {
                swal("Lo dejamos como estaba..");
              }
            });
        }



        
         function  cargarFile(elem,alt,seccion,objeto){
 


     
              var file_data = $('#file'+alt).prop('files')[0];
                        var form_data = new FormData();

                        form_data.set("seccion", "");
                        form_data.set("depto", "");
                        form_data.set("fk_depto", "");
                        form_data.set("programa", "");
                        form_data.set("objeto", "");
                        form_data.append('file', file_data);
                        form_data.append("seccion", seccion);
                        form_data.append("depto", '<?=$departamento?>');
                        form_data.append("fk_depto", '<?=$fk_departamento?>');
                        form_data.append("programa", '<?=$fk_programa?>');
                        form_data.append("objeto", objeto);
         


                $.ajax({
                                 url:'<?php echo base_url();?>index.php/upload/do_upload',
                                 type:"post",
                                 //data:new FormData(elem.form),
                                 data:form_data,
                                 processData:false,
                                 contentType:false,

                                 cache:false,
                                 async:false,
                                  success: function(data){
                                    
                                    
                                    console.log(data);
                                      $("#band"+alt).html("Carga Exitosa...");
                                      $("#url"+alt).html("<a target='_blank' href='"+data+"'>Previsualizar</a>");

                                      if(data!=""){
                                        $("#archivoCargado").val(1);
                                      }

                               }
                             }); 

          }




    </script>

</body>

</html>
