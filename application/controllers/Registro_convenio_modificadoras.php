<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_convenio_modificadoras extends CI_Controller {

  /**
   * Developed by Constantino Kusulas Vazquez   - 2018.
   */



	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
        $this->load->library('session');

    }
 
	public function index()
	{	


     $programaSelected = $this->session->userdata('ProgramasSelecc');
        


     $suscrito = $this->ffinanciamiento_model->getProgramaSuscrito($programaSelected);

     if(isset($suscrito) && count($suscrito)>0){
     $suscrito =  $suscrito[0]['convenio_suscrito'];
      }else{
        $suscrito ="";
      }


    if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }

		 $listMunicipios = $this->ffinanciamiento_model->getMunicipios();

        $listObras = $this->ffinanciamiento_model->getListProysBeneficios($programaSelected,1);
        $listApoyos = $this->ffinanciamiento_model->getListProysBeneficios($programaSelected,2);
        $listServicios = $this->ffinanciamiento_model->getListProysBeneficios($programaSelected,3);

        $data = array(     
          
            'listMunicipios' => $listMunicipios,
            'suscrito' => $suscrito,
            'listObras' => $listObras,
            'listApoyos' => $listApoyos,
            'listServicios' => $listServicios,


        );

		$this->load->view('registro_convenio_modificadoras',$data);
	}


    public function guardarMaterialDifusion(){

        $nombreMaterial         = $this->input->post('nombreMaterial');
        $archivoMaterial        = $this->input->post('archivoMaterial');
        $cantidadProducida      = $this->input->post('cantidadProducida');
        
        $entidadFederativa      = "Querétaro";
     

 

        echo "nombreMaterial: " . $nombreMaterial . "<br>";
        echo "archivoMaterial: " . $archivoMaterial . "<br>";
        echo "cantidadProducida: " . $cantidadProducida . "<br>";
        echo "entidadFederativa: " . $entidadFederativa . "<br>";
 

        die();

    }

      public function changeFormatDateSpEng($data){

        $arrayDate = explode("/", $data);
        $newData = $arrayDate[2] . "-" . $arrayDate[1] . "-"  . $arrayDate[0];
        return($newData);
    }


    public function guardarMaterialDistribucion(){

        $programa         = $this->input->post('programa');
        $nombreMaterial               = $this->input->post('nombreMaterial');
        $tipoMaterial       = $this->input->post('tipoMaterial');
        $municipio       = $this->input->post('municipio');
        $localidad       = $this->input->post('localidad');
        $cantidadDistribuir       = $this->input->post('cantidadDistribuir');
        $fechaDistribuicion       = $this->input->post('fechaDistribuicion');

         if($fechaDistribuicion!="")
            $fechaDistribuicion = $this->changeFormatDateSpEng($fechaDistribuicion);


          echo "programa: " . $programa . "<br>";
          echo "nombreMaterial: " . $nombreMaterial . "<br>";
          echo "tipoMaterial: " . $tipoMaterial . "<br>";
          echo "municipio: " . $municipio . "<br>";
          echo "localidad: " . $localidad . "<br>";
          echo "cantidadDistribuir: " . $cantidadDistribuir . "<br>";
          echo "fechaDistribuicion: " . $fechaDistribuicion . "<br>";

    }


}
