<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIQSEQ V2.0</title>
     <?php $this->view('template/header.php'); ?>   

</head>

<body>
          <?php $this->view('template/mod_nuevo_programa'); ?>

           <?php $this->view('template/mod_distribuir_material'); ?>


    <div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo asset_url();?>img/sicseq-logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING --Salir>
 
          <!-- begin TOP NAVIGATION -->
        <?php $this->view('template/nav_top_messages.php'); ?>       
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
          <?php $this->view('template/nav_left_menus.php'); ?>       
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- begin MAIN PAGE CONTENT -->




        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">
            
            

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                        <form id="form_navbar">
                           <div id="titleProgram">  </div>
                               <table border=0 width="100%">
                                    <tr>
                                        <td>
                                           <h1>
                                               
                                            </h1>
                                         </td>
                                        <td align="right"> 
                                            <!--buttons-->
                                     </td>
                                    </tr>
                                </table>
                            <ol class="breadcrumb">
                            
                                           
                                <li><i class="fa fa-edit"></i>Materiales</i>
                                </li>
                                <li class="active">Listado de Materiales</li>
                              
                              
                                <!--submenu Programas -->
                                  <?php $this->view('template/nav_submenu_programas.php'); ?>       
      


                            </ol>

                            <input type="hidden" name="programSelected" id="programSelected">
                             

                        </div>

                        <input type="hidden" name="counterIntegrante" id="counterIntegrante" value=1>
                    </form>

                    
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin MAIN PAGE ROW -->
                <div class="row">
                
                
                 
                

                    <!-- begin LEFT COLUMN -->
                    <div class="col-lg-12">

                        <div class="row">

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">

                                <div class="portlet portlet-green">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <div id="subTitle"></div>
                                        </div>
                                        <div class="portlet-widgets">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#basicFormExample"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="basicFormExample" class="panel-collapse collapse in">
                                         <div class="portlet-body">
                                                <div id="tablaGeneralMaterial"></div>  
                                        </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
                            <!-- /.col-lg-12 (nested) -->
                            <!-- End Basic Form Example -->

                            <!-- Inline Form Example -->
                         

                    </div>
                    <!-- /.col-lg-6 -->
                    <!-- end LEFT COLUMN -->

                  

                </div>
                <!-- /.row -->
                <!-- end MAIN PAGE ROW -->
                
     

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
     
    <script src="<?php echo asset_url();?>js/plugins/jquery/jquery.numeric.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/bootstrap/bootstrap.min.js"></script>
 
    <script src="<?php echo asset_url();?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/defaults.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   


      <!-- Salir Notification Box -->
    <?php $this->view('template/nav_footer_logout.php'); ?> 
   


    <!-- /#Salir -->
    <!-- Salir Notification jQuery -->
    <script src="<?php echo asset_url();?>js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo asset_url();?>js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo asset_url();?>js/flex.js"></script>
 
    
    
    <link href="<?php echo asset_url();?>bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <script src="<?php echo asset_url();?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    
    




    <script type="text/javascript">

        $(document).ready(function () {

            
         $('body').css('zoom','80%').fadeIn('slow'); /* Webkit browsers */
          $('body').css('zoom','0.8').fadeIn('slow'); /* Other non-webkit browsers */
 


          procesarCambio();
          changeProgram("undefined");




           (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                monthsTitle: "Meses",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
            }(jQuery));





          $( function() {
            $( "#fecha_distribucion" ).datepicker({ dateFormat: 'dd/mm/yy',  language: "es" });
          
            
          } );

             //iconos
              $('.aprobacionobra').click(function() {
                    $("#fechaAprobacionObra").focus();
              });

                $('#fechaAprobacionObra').on('changeDate', function(ev){
                    $(this).datepicker('hide');
                });
    


            $("#catalogResources").change(function () {

                var resourceSelected = $("#catalogResources option:selected").val();     

                populate_programs(resourceSelected);

            });

            function populate_programs(fromResource) {
                var cantElement = 0;
                $('#catalogPrograms').empty();
                $('#catalogPrograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_programs') ?>/" + fromResource,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogPrograms').empty();
                        $('#catalogPrograms').append("<option>Selecciona Programa..</option>");


                        $.each(data, function (i, name) {
                            $('#catalogPrograms').append('<option value="' + data[i].id + '">' + data[i].programa + '</option>');
                            cantElement++;
                        });

                         
                    }
                });
            }   


             $("#catalogPrograms").change(function () {

                var programSelected = $("#catalogPrograms option:selected").val();   

                populate_subprograms(programSelected);

            });


           function populate_subprograms(fromProgram) {
                var cantElement = 0;
                $('#catalogSubprograms').empty();
                $('#catalogSubprograms').append("<option>Loading ....</option>");
                $.ajax({
                    type: "GET",
                    url: "<?php echo site_url('registro_ffinanciamiento/populate_subprograms') ?>/" + fromProgram,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#catalogSubprograms').empty();
                        $('#catalogSubprograms').append("<option>Selecciona subprograma..</option>");
        

                        $.each(data, function (i, name) {
                            $('#catalogSubprograms').removeAttr("disabled")
                            $('#catalogSubprograms').append('<option value="' + data[i].id + '">' + data[i].subprograma + '</option>');
                            cantElement++;
                        });                         

                        if(cantElement==0){
                            $('#catalogSubprograms').attr('disabled', 'disabled');
                        }
                    }
                });
            }



            $('#vigila_federal').numeric(",");
            $('#vigila_estatal').numeric(",");
            $('#vigila_municipal').numeric(",");
            $('#vigila_otros').numeric(",");
            $('#total_acciones').numeric();
            $('#total_people_womans').numeric();
            $('#total_people_mans').numeric();
            
            

          });    


        function sumaVigilado(){

            var total = 0;

                $(".t_vigilado").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

            $("#total_vigilado").val(total);

        }


        function changeProgram(checked){

             if(checked!="undefined"){
               
                swal({
                  title: "Estás seguro de querer cambiar Programa?",
                  text: "",
                  icon: "warning",
                  buttons: true,
                  dangerMode: false, 
                })
                .then((willDelete) => {
                  if (willDelete) {
                    swal("Listo!, Se cambio programa exitosamente", {
                      icon: "success",
                    });
                    //location.reload(true);

            procesarCambio();


                  } else {
                    swal("Lo dejamos como estaba..");
                  }
                });
            }
 
         
           
 
         
               
        }


        function procesarCambio(){
             var id_programSelected;

            id_programSelected = $("#programaSeleccionado option:selected").val();
            
              if(id_programSelected == "agregar"){
                    $('.modal-nuevo').modal("show");
                }



               var programaSeleccionado = $("#programaSeleccionado option:selected").text();
                $("#titleProgram").hide();

                var res = programaSeleccionado.split("-");

                  $.ajax({
                    url: "<?php echo site_url('indicadores_generales/seleccionPrograma') ?>",
                    type: "POST",
                    data: $('#form_navbar').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            titulo_programa = obj.msg;
                        });

                     $("#titleProgram").html("<h1>"+titulo_programa+"</h1>").show("slow");
                         $("#subTitle").html("<h4>Listado de Materiales- "+titulo_programa+"</h4>").show("slow");

 

                   //$("#titleProgram").fadeIn("slow");

    
                    }
                });


                //Mandar a llamar datos del Programa, a Excepciond de asignar Nuevo Programa.  
                if(id_programSelected != "agregar"){

                var idSel = $("#programaSeleccionado").val();
                    
                    $.ajax({
                        url: "<?php echo site_url('listado_materiales/getTablaGeneralMaterial') ?>/"+idSel,
                        aSync: false,
                        dataType: "html",
                               success: function(data) {
                                            $("#tablaGeneralMaterial").html(data).show("slow");
                                             // $("#tablaGeneral").show();
                                        },                                
                        });
                }
        }



        function sumaPeople(){

            var total = 0;

                $(".t_people").each(function() {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        total += parseFloat(this.value);
                    }
                });

              

            $("#total_people").val(total);

        }


        



        function jsUcfirst(string) 
        {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }


        function guardarFF(){

            swal("Bien hecho!", "La Fuente de Financiamiento ha sido guardado.", "success");

        }




        function guardarDistribucion(){


            $("#accionConexion").css({"background-color": "white"});
            $("#cantidad_distribuir").css({"background-color": "white"});
            $("#fecha_distribucion").css({"background-color": "white"});

            var msgError = "";


            if(   $("#accionConexion option:selected").val() == "0" ){
                     msgError+= "- Debe seleccionar al menos una Accion de Conexion.";
                      $("#accionConexion").css({"background-color": "yellow"});                  
            
            }

            if( $("#cantidad_distribuir").val() == ""){
                msgError+= "\n - Debe asignar cantidad a Distrbuir, ";
                $("#cantidad_distribuir").css({"background-color": "yellow"});
            }

            if( $("#fecha_distribucion").val() == ""){
                     msgError+="\n - Debe asignar fecha de distribucion, ";
                $("#fecha_distribucion").css({"background-color": "yellow"});
            }

             if( (parseInt($("#cantidad_inventario").val())+1)<=parseInt($("#cantidad_distribuir").val())){
                     msgError+="\n - Cantidad a Distribuir debe ser menor a Disponibilidad en Inventario, ";
                 $("#cantidad_distribuir").css({"background-color": "yellow"});
            }

    
            //alert("hola 1");

            if(msgError==""){

                     
                  $.ajax({
                    url: "<?php echo site_url('listado_materiales/guardarNuevaDistribucion') ?>",
                    type: "POST",
                    data: $('#form_distribucion').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {


                         var status, message;
                        $.each(data, function (index, obj) {
                            status = true;
                            titulo_programa = "";
                        });


                        
 

                    if(status){ 
                        alert("here");


      
                              

    //                                    location.reload(true);

                    }

    
                    }
                });

                       $('.modal-material').modal("hide");                         

                                     swal("Bien hecho!", "La distribucion ha sido guardada exitosamente.", "success");
                                     $("#cantidad_distribuir").val('');
                                     $("#fecha_distribucion").val('');
                                     $("#accionConexion").val(0);
                       


   
            }else{
                swal("Error!", msgError, "error");
            }

        }


      
        


 



    </script>



</body>

</html>
