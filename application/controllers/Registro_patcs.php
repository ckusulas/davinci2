<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_patcs extends CI_Controller {

   /**
     * Developed by Constantino Kusulas Vazquez   - 2018.
     */


   	public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');

          $this->load->library('session');


    }




	public function index()
	{  

      if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }

        $programSelected = $this->session->userdata('ProgramasSelecc');
        $departamento = $this->session->userdata('departamento');

        $data = array(
          'fk_programa' =>  $programSelected,
          'departamento' => $departamento,
          'fk_departamento' => $departamento,
          'url_documento' =>  $this->ffinanciamiento_model->getURL_adjunto($programSelected, $departamento, "PATCS", "documento"),
        );

     


        $this->load->view('registro_patcs', $data);
	}




	 

}
