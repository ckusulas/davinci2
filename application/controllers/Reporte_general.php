<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporte_general extends CI_Controller {


    /**
     * Developed by Constantino Kusulas Vazquez   - 2018.
     */




    public function __construct() {
        parent::__construct();
        $this->load->model('ffinanciamiento_model');
        $this->load->model('catalogos_model');
        $this->load->library('session');
 
    }


	 
	public function index()
	{
        if($this->session->userdata('is_logued_in')!=TRUE){
        redirect('inicio');
      }


                $data = "";
 

		      $this->load->view('reporte_general', $data);

        
	}

     public function getTablaGeneral($program = ""){

     // sleep(0.7);
          
        $listQuestions = $this->ffinanciamiento_model->getListQuestions(2018);        
        $listRecourses = $this->ffinanciamiento_model->getListRecourses(2018);
        $listIntancePromotor = $this->ffinanciamiento_model->getListInstanciaPromotora(2018);
        

       
        if($program==""){
          $fk_programa = $this->session->userdata('ProgramasSelecc');
          if($fk_programa == "agregar")
          {
            $fk_programa = "";
          }
        }else{
          $fk_programa = $program;
        }

         
        $is_gerencia = false;
        $departam_Admon = array(1,3,4);
        $departamento = $this->session->userdata('departamento');

        if(in_array($departamento, $departam_Admon)){
            $is_gerencia = true;
        } 


        $listProyectos = $this->ffinanciamiento_model->getListProyectos($fk_programa, $departamento);
        $totalRecursoAsignado = $this->ffinanciamiento_model->getTotalRecursoAsignadoVigilar($fk_programa);
        $totalBeneficiados = $this->ffinanciamiento_model->getTotalBeneficiados($fk_programa);
        $qtyObras = $this->ffinanciamiento_model->getQtyBeneficio(1,$fk_programa);
        $qtyApoyo = $this->ffinanciamiento_model->getQtyBeneficio(2,$fk_programa);
        $qtyServicio = $this->ffinanciamiento_model->getQtyBeneficio(3,$fk_programa);
        $qtyComites = $this->ffinanciamiento_model->getQtyComites($fk_programa);

            $data = array();

        unset($data);
 
        $data = array(
            
            'listQuestions' => $listQuestions, 
            'listRecourses' => $listRecourses,
            'listInstanceProm' => $listIntancePromotor,
            'listProyectos' => $listProyectos,
             'totalRecursoAsignado' => $totalRecursoAsignado,        
             'totalBeneficiados' => $totalBeneficiados,           
             'qtyObras' => $qtyObras,           
             'qtyApoyos' => $qtyApoyo,           
             'qtyServicios' => $qtyServicio,    
             'qtyComites' => $qtyComites,    
             'is_gerencia' => $is_gerencia,
             'success' => true,       
             'status' => true,       
          
          
        );
 
       
          $this->load->view('template/tableGeneral_indicadoresfull2', $data);
       
    }







    
}
