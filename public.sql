/*
Navicat PGSQL Data Transfer

Source Server         : postgressDavinci
Source Server Version : 90317
Source Host           : 192.168.169.157:5432
Source Database       : postgres
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90317
File Encoding         : 65001

Date: 2018-08-09 10:29:35
*/


-- ----------------------------
-- Sequence structure for adjuntos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."adjuntos_id_seq";
CREATE SEQUENCE "public"."adjuntos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for beneficios_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."beneficios_id_seq";
CREATE SEQUENCE "public"."beneficios_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for catalogo_ffinanciamiento_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."catalogo_ffinanciamiento_id_seq";
CREATE SEQUENCE "public"."catalogo_ffinanciamiento_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for catalogo_general_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."catalogo_general_id_seq";
CREATE SEQUENCE "public"."catalogo_general_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for catalogo_informes_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."catalogo_informes_id_seq";
CREATE SEQUENCE "public"."catalogo_informes_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for departamento_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."departamento_id_seq";
CREATE SEQUENCE "public"."departamento_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 10
 CACHE 1;
SELECT setval('"public"."departamento_id_seq"', 10, true);

-- ----------------------------
-- Sequence structure for files_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."files_id_seq";
CREATE SEQUENCE "public"."files_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for instancia_ejecutora_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."instancia_ejecutora_id_seq";
CREATE SEQUENCE "public"."instancia_ejecutora_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for instancia_promotora_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."instancia_promotora_id_seq";
CREATE SEQUENCE "public"."instancia_promotora_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for localidades_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."localidades_id_seq";
CREATE SEQUENCE "public"."localidades_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for material_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."material_id_seq";
CREATE SEQUENCE "public"."material_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for materiales_difusion_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."materiales_difusion_id_seq";
CREATE SEQUENCE "public"."materiales_difusion_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for municipios_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."municipios_id_seq";
CREATE SEQUENCE "public"."municipios_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for programas_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."programas_id_seq";
CREATE SEQUENCE "public"."programas_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 32
 CACHE 1;
SELECT setval('"public"."programas_id_seq"', 32, true);

-- ----------------------------
-- Sequence structure for ramos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ramos_id_seq";
CREATE SEQUENCE "public"."ramos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for recursos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."recursos_id_seq";
CREATE SEQUENCE "public"."recursos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for registro_capacitacion_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_capacitacion_id_seq";
CREATE SEQUENCE "public"."registro_capacitacion_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."registro_capacitacion_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for registro_comite_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_comite_id_seq";
CREATE SEQUENCE "public"."registro_comite_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 18
 CACHE 1;
SELECT setval('"public"."registro_comite_id_seq"', 18, true);

-- ----------------------------
-- Sequence structure for registro_comite_integrantes_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_comite_integrantes_id_seq";
CREATE SEQUENCE "public"."registro_comite_integrantes_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 133
 CACHE 1;
SELECT setval('"public"."registro_comite_integrantes_id_seq"', 133, true);

-- ----------------------------
-- Sequence structure for registro_comite_obra_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_comite_obra_id_seq";
CREATE SEQUENCE "public"."registro_comite_obra_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 20
 CACHE 1;
SELECT setval('"public"."registro_comite_obra_id_seq"', 20, true);

-- ----------------------------
-- Sequence structure for registro_informe_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_informe_id_seq";
CREATE SEQUENCE "public"."registro_informe_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for registro_material_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_material_id_seq";
CREATE SEQUENCE "public"."registro_material_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for registro_minuta_asistencia_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_minuta_asistencia_id_seq";
CREATE SEQUENCE "public"."registro_minuta_asistencia_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for registro_minuta_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_minuta_id_seq";
CREATE SEQUENCE "public"."registro_minuta_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for registro_programa_recursos_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_programa_recursos_id_seq";
CREATE SEQUENCE "public"."registro_programa_recursos_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8
 CACHE 1;
SELECT setval('"public"."registro_programa_recursos_id_seq"', 8, true);

-- ----------------------------
-- Sequence structure for registro_proyecto_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."registro_proyecto_id_seq";
CREATE SEQUENCE "public"."registro_proyecto_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 36
 CACHE 1;
SELECT setval('"public"."registro_proyecto_id_seq"', 36, true);

-- ----------------------------
-- Sequence structure for subprogramas_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."subprogramas_id_seq";
CREATE SEQUENCE "public"."subprogramas_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for usuario_programa_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."usuario_programa_id_seq";
CREATE SEQUENCE "public"."usuario_programa_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 69
 CACHE 1;
SELECT setval('"public"."usuario_programa_id_seq"', 69, true);

-- ----------------------------
-- Sequence structure for usuarios_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."usuarios_id_seq";
CREATE SEQUENCE "public"."usuarios_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 11
 CACHE 1;
SELECT setval('"public"."usuarios_id_seq"', 11, true);

-- ----------------------------
-- Table structure for cs_adjuntos
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_adjuntos";
CREATE TABLE "public"."cs_adjuntos" (
"id" int4 DEFAULT nextval('adjuntos_id_seq'::regclass) NOT NULL,
"fk_program" int4,
"tipo" varchar(100) COLLATE "default",
"fk_tipo" int4,
"url" varchar(400) COLLATE "default",
"extension" varchar(20) COLLATE "default",
"typeExtHTML" varchar(60) COLLATE "default",
"name" varchar(300) COLLATE "default",
"fk_user" int4,
"status" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_adjuntos
-- ----------------------------

-- ----------------------------
-- Table structure for cs_beneficios
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_beneficios";
CREATE TABLE "public"."cs_beneficios" (
"id" int4 DEFAULT nextval('beneficios_id_seq'::regclass) NOT NULL,
"beneficio" varchar(32) COLLATE "default",
"updated" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_beneficios
-- ----------------------------
INSERT INTO "public"."cs_beneficios" VALUES ('1', 'OBRA', '2018-03-10 18:26:11');
INSERT INTO "public"."cs_beneficios" VALUES ('2', 'APOYO', '2018-03-10 18:26:15');
INSERT INTO "public"."cs_beneficios" VALUES ('3', 'SERVICIO', '2018-03-10 18:26:21');

-- ----------------------------
-- Table structure for cs_catalogo_ffinanciamiento
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_catalogo_ffinanciamiento";
CREATE TABLE "public"."cs_catalogo_ffinanciamiento" (
"id" int4 DEFAULT nextval('catalogo_ffinanciamiento_id_seq'::regclass) NOT NULL,
"pregunta" varchar(250) COLLATE "default",
"orden" int4,
"año" varchar(10) COLLATE "default",
"categoria" varchar(64) COLLATE "default",
"fk_contenido" int4,
"categoria_foraneo" varchar(100) COLLATE "default",
"status" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_catalogo_ffinanciamiento
-- ----------------------------
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('1', 'ORIGEN DEL RECURSO', '1', '2018', 'DATOS DEL PROGRAMA', '2', 'cs_capacitacion_tematica', '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('2', 'NOMBRE DEL PROGRAMA PRESUPUESTARIO', '2', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('3', 'EJERCICIO FISCAL', '3', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('4', 'SUB-PROGRAMA', '4', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('5', 'RAMO', '5', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('6', 'NOMBRE DEL RAMO', '6', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('7', 'MODALIDAD', '7', '2018', 'DATOS DEL PROGRAMA', '2', 'comite_funciones', '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('8', 'PRESUPUESTO AUTORIZADO EN EL PEF', '8', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('9', 'PRESUPUESTO FEDERAL A VIGILAR POR LA CONTRALORIA SOCIAL', '9', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('10', 'PRESUPUESTO ESTATAL A VIGILAR POR LA CONTRALORIA SOCIAL', '10', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('11', 'PRESUPUESTO ESTATAL A VIGILAR POR LA CONTRALORIA SOCIAL', '11', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('12', 'PRESUPUESTO OTROS A VIGILAR POR LA CONTRALORIA SOCIAL', '12', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('13', 'TOTAL VIGILADO POR LA CONTRALORIA SOCIAL', '13', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('14', 'TOTAL DE ACCIONES', '14', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('15', 'TOTAL DE POBLACION REAL BENEFICIADA', '15', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('16', 'TOTAL DE HOMBRES BENEFICIADOS', '16', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('17', 'TOTAL DE MUJERES BENEFICIADAS', '17', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('18', 'DESCRIPCION DE LA POBLACION OBJETIVO', '18', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('19', 'ENTIDAD FEDERATIVA', '19', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('20', 'OBJETIVO DEL PROGRAMA', '20', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('21', 'INSTANCIA PROMOTORA', '21', '2018', 'DATOS DEL PROGRAMA', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('22', 'FECHA DE ASIGNACION DEL RECURSO', '22', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('23', 'AÑO AL QUE PERTENECE 1', '23', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('24', 'FUENTE DE FINANCIAMIENTO 1', '24', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('25', 'MONTO DE LA FUENTE 1', '25', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('26', 'AÑO AL QUE PERTENECE FUENTE 2', '26', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('27', 'FUENTE DE FINANCIAMIENTO 2', '27', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('28', 'MONTO DE LA FUENTE 2', '28', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('29', 'AÑO AL QUE PERTENECE FUENTE 3', '29', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('30', 'FUENTE DE FINANCIAMIENTO 3', '30', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('31', 'MONTO DE LA FUENTE 3', '31', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('32', 'TOTAL RECURSO FEDERAL', '32', '2018', 'FEDERAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('34', 'FECHA DE ASIGNACION DEL RECURSO', '33', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('35', 'AÑO AL QUE PERTENECE FUENTE 1', '34', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('36', 'FUENTE DE FINANCIAMIENTO 1', '35', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('37', 'MONTO DE LA FUENTE 1', '36', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('38', 'AÑO AL QUE PERTENECE FUENTE 2', '37', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('39', 'FUENTE DE FINANCIAMIENTO 2', '38', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('40', 'MONTO DE LA FUENTE 2', '39', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('41', 'AÑO AL QUE PERTENECE FUENTE 3', '40', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('42', 'FUENTE DE FINANCIAMIENTO 3', '41', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('43', 'MONTO DE LA FUENTE 3', '42', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('44', 'TOTAL RECURSO MUNICIPAL', '43', '2018', 'MUNICIPAL', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('45', 'FECHA DE ASIGNACION DEL RECURSO', '44', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('46', 'FUENTE DE FINANCIAMIENTO 1', '45', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('47', 'MONTO DE LA FUENTE 1', '46', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('48', 'AÑO AL QUE PERTENECE FUENTE 1', '47', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('49', 'FUENTE DE FINANCIAMIENTO 2', '48', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('50', 'MONTO DE LA FUENTE 2', '49', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('51', 'AÑO AL QUE PERTENECE FUENTE 2', '50', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('52', 'FUENTE DE FINANCIAMIENTO 3', '51', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('53', 'MONTO DE LA FUENTE 3', '52', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('54', 'AÑO AL QUE PERTENECE FUENTE 3', '53', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('55', 'TOTAL OTROS RECURSO ', '54', '2018', 'OTROS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('56', 'TOTAL RECURSO FEDERAL', '55', '2018', 'RESUMEN RECURSOS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('57', 'TOTAL RECURSO ESTATAL', '56', '2018', 'RESUMEN RECURSOS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('58', 'TOTAL RECURSO MUNICIPAL', '57', '2018', 'RESUMEN RECURSOS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('59', 'TOTAL OTROS RECURSO', '58', '2018', 'RESUMEN RECURSOS', '1', null, '1');
INSERT INTO "public"."cs_catalogo_ffinanciamiento" VALUES ('60', 'TOTAL DE RECURSOS ASIGNADOS', '59', '2018', 'RESUMEN RECURSOS', '1', null, '1');

-- ----------------------------
-- Table structure for cs_catalogo_ffinanciamiento_respuestas
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_catalogo_ffinanciamiento_respuestas";
CREATE TABLE "public"."cs_catalogo_ffinanciamiento_respuestas" (
"id" int4 NOT NULL,
"fk_ejecutor" int4,
"fk_catalogo_ff" int4,
"respuesta" varchar(600) COLLATE "default",
"updated" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_catalogo_ffinanciamiento_respuestas
-- ----------------------------

-- ----------------------------
-- Table structure for cs_catalogo_general
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_catalogo_general";
CREATE TABLE "public"."cs_catalogo_general" (
"id" int4 DEFAULT nextval('catalogo_general_id_seq'::regclass) NOT NULL,
"campo" varchar(600) COLLATE "default",
"categoria" varchar(200) COLLATE "default",
"orden" int4,
"año" varchar(10) COLLATE "default",
"status" int2,
"updated" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_catalogo_general
-- ----------------------------
INSERT INTO "public"."cs_catalogo_general" VALUES ('1', 'APLICACIÓN DEL APARTADO DE INFORME  Y/O INFORME COMPLETO (ANTES CEDULAS INFORMES', 'capacitacion_tematica', '1', '2018', '1', '2018-03-10 22:37:09');
INSERT INTO "public"."cs_catalogo_general" VALUES ('2', 'INTEGRACION, FUNCIONES Y ACTIVIDADES DE LOS COMITES', 'capacitacion_tematica', '2', '2018', '1', '2018-03-10 22:37:09');
INSERT INTO "public"."cs_catalogo_general" VALUES ('3', 'LA CONTRALORIA SOCIAL Y SUS ACTIVIDADES', 'capacitacion_tematica', '3', '2018', '1', '2018-03-10 22:37:09');
INSERT INTO "public"."cs_catalogo_general" VALUES ('4', 'NORMATIVIDAD DE LA CONTRALORIA SOCIAL', 'capacitacion_tematica', '4', '2018', '1', '2018-03-10 22:37:09');
INSERT INTO "public"."cs_catalogo_general" VALUES ('5', 'NORMATIVIDAD DEL PROGRAMA FEDERAL', 'capacitacion_tematica', '5', '2018', '1', '2018-03-10 22:37:09');
INSERT INTO "public"."cs_catalogo_general" VALUES ('6', 'QUEJAS Y DENNCIAS', 'capacitacion_tematica', '6', '2018', '1', '2018-03-10 22:37:09');
INSERT INTO "public"."cs_catalogo_general" VALUES ('7', 'REGLAS DE OPERACIÓN', 'capacitacion_tematica', '7', '2018', '1', '2018-03-10 22:37:09');
INSERT INTO "public"."cs_catalogo_general" VALUES ('8', 'SISTEMAS DE INFORMACION', 'capacitacion_tematica', '8', '2018', '1', '2018-03-10 22:37:09');
INSERT INTO "public"."cs_catalogo_general" VALUES ('9', 'OTROS TEMAS', 'capacitacion_tematica', '9', '2018', '1', '2018-03-10 22:37:09');
INSERT INTO "public"."cs_catalogo_general" VALUES ('11', 'BENEFICIARIO', 'capacitacion_figura', '1', '2018', '1', '2018-03-10 22:58:22');
INSERT INTO "public"."cs_catalogo_general" VALUES ('12', 'INTEGRANTE DEL COMITÉ', 'capacitacion_figura', '2', '2018', '1', '2018-03-10 22:58:22');
INSERT INTO "public"."cs_catalogo_general" VALUES ('13', 'OTRA FIGURA', 'capacitacion_figura', '3', '2018', '1', '2018-03-10 22:58:22');
INSERT INTO "public"."cs_catalogo_general" VALUES ('14', 'OTRO ORGANISMO', 'capacitacion_figura', '4', '2018', '1', '2018-03-10 22:58:22');
INSERT INTO "public"."cs_catalogo_general" VALUES ('15', 'SERVIDOR PUBLICO ESTATAL', 'capacitacion_figura', '5', '2018', '1', '2018-03-10 22:58:22');
INSERT INTO "public"."cs_catalogo_general" VALUES ('16', 'SERVIDOR PUBLICO FEDERAL', 'capacitacion_figura', '6', '2018', '1', '2018-03-10 22:58:22');
INSERT INTO "public"."cs_catalogo_general" VALUES ('17', 'SERVIDOR PUBLICO MUNICIPAL', 'capacitacion_figura', '7', '2018', '1', '2018-03-10 22:58:22');
INSERT INTO "public"."cs_catalogo_general" VALUES ('18', 'SOLICITAR LA INFORMACIÓN PÚBLICA RELACIONADA CON LA OPERACIÓN DEL PROGRAMA.', 'comite_funciones', '1', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('19', 'VIGILAR QUE SE DIFUNDA INFORMACIÓN SUFICIENTE, VERAZ Y OPORTUNA SOBRE LA OPERACIÓN DEL PROGRAMA FEDERAL', 'comite_funciones', '2', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('20', 'VIGILAR QUE EL EJERCICIO DE LOS RECURSOS PÚBLICOS PARA LAS OBRAS, APOYOS O SERVICIOS SEA OPORTUNO TRANSPARENTE Y CON APEGO A LO ESTABLECIDO EN LAS REGLAS DE OPERACION.', 'comite_funciones', '3', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('21', 'VIGILAR QUE SE DIFUNDA EL PADRÓN DE BENEFICIARIOS. ', 'comite_funciones', '4', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('22', 'VIGILAR QUE LOS BENEFICIARIOS DEL PROGRAMA FEDERAL CUMPLAN CON LOS REQUISITOS PARA TENER ESE CARÁCTER.', 'comite_funciones', '5', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('23', 'VIGILAR QUE SE CUMPLA CON LOS PERIODOS DE EJECUCIÓN DE LAS OBRAS O DE LA ENTREGA DE LOS APOYOS O SERVICIOS.', 'comite_funciones', '6', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('24', 'VIGILAR QUE EXISTA DOCUMENTACIÓN COMPROBATORIA DEL EJERCICIO DE LOS RECURSOS PÚBLICOS Y DE LA ENTREGA DE LAS OBRAS, APOYOS O SERVICIOS.', 'comite_funciones', '7', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('25', 'VIGILAR QUE EL PROGRAMA FEDERAL NO SE UTILICE CON FINES POLÍTICOS, ELECTORALES, DE LUCRO U OTROS DISTINTOS AL OBJETO DEL PROGRAMA FEDERAL.', 'comite_funciones', '8', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('26', 'VIGILAR QUE EL PROGRAMA FEDERAL NO SEA APLICADO AFECTANDO LA IGUALDAD ENTRE MUJERES Y HOMBRES. ', 'comite_funciones', '9', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('27', 'VIGILAR QUE LAS AUTORIDADES COMPETENTES DEN ATENCIÓN A LAS QUEJAS Y DENUNCIAS RELACIONADAS CON EL PROGRAMA FEDERAL.', 'comite_funciones', '10', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('28', 'REGISTRAR EN LOS INFORMES LOS RESULTADOS DE LAS ACTIVIDADES DE CONTRALORÍA SOCIAL REALIZADAS, ASÍ COMO DAR SEGUIMIENTO, EN SU CASO A LOS MISMOS (antes Cedulas)', 'comite_funciones', '11', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('29', 'RECIBIR LAS QUEJAS Y DENUNCIAS SOBRE LA APLICACIÓN Y EJECUCIÓN DE LOS PROGRAMAS FEDERALES, ', 'comite_funciones', '12', '2018', '1', '2018-03-10 22:23:25');
INSERT INTO "public"."cs_catalogo_general" VALUES ('30', 'RECIBIR LAS QUEJAS Y DENUNCIAS QUE PUEDAN DAR LUGAR AL FINCAMIENTO DE RESPONSABILIDADES ADMINISTRATIVAS, CIVILES O PENALES RELACIONADAS CON LOS PROGRAMAS FEDERALES, ASI COMO TUNRARLAS A LAS AUTORIDADES COMPETENTES PARA SU ATENCION.', 'comite_funciones', '13', '2018', '1', '2018-03-10 22:23:26');
INSERT INTO "public"."cs_catalogo_general" VALUES ('31', 'Capturar en el SICS el programa estatal de trabajo.', 'pets_actividades', '1', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('32', 'Capturar en el SICS la distribución de los materiales de capacitación realizada.', 'pets_actividades', '2', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('33', 'Registrar la información de las obras, apoyos y servicios programadas y ejecutadas con presupuesto f', 'pets_actividades', '3', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('34', 'Capturar en el SICS  los comités de Contraloría Social constituidos.', 'pets_actividades', '4', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('35', 'Capturar reuniones con los comités realizadas.', 'pets_actividades', '5', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('36', 'Capturar en el SICS el o los apartados de los informes de los informes con las respuestas de los int', 'pets_actividades', '6', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('37', 'Capturar en el SICS el o los Informes completos con las respuestas de los integrantes de comité (en ', 'pets_actividades', '7', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('38', 'Suscripción de Programa de Trabajo.', 'pets_actividades', '9', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('39', 'Difusión sobre el programa.', 'pets_actividades', '10', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('40', 'Constituir comités de Contraloría Social.', 'pets_actividades', '11', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('41', 'Capacitación a comités  en materia de Contraloría Social.', 'pets_actividades', '12', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('42', 'Entrega de Resumen Técnico de Obras, Apoyos y/o Servicios.', 'pets_actividades', '13', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('43', 'Seguimiento y asesoría.', 'pets_actividades', '14', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('44', 'Envío de información de las obras, apoyos o servicios aprobados.', 'pets_actividades', '15', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('45', 'Registro de las acciones de promoción y operación de la contraloría social.', 'pets_actividades', '16', '2018', '1', '2018-03-10 10:30:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('192', 'CD 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:38');
INSERT INTO "public"."cs_catalogo_general" VALUES ('193', 'CD 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:38');
INSERT INTO "public"."cs_catalogo_general" VALUES ('194', 'CD 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:38');
INSERT INTO "public"."cs_catalogo_general" VALUES ('195', 'CD 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:38');
INSERT INTO "public"."cs_catalogo_general" VALUES ('196', 'CD 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:38');
INSERT INTO "public"."cs_catalogo_general" VALUES ('197', 'FOLLETOS 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:38');
INSERT INTO "public"."cs_catalogo_general" VALUES ('198', 'FOLLETOS 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:38');
INSERT INTO "public"."cs_catalogo_general" VALUES ('199', 'FOLLETOS 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('200', 'FOLLETOS 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('201', 'FOLLETOS 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('202', 'GUIA PARA BENEFICIARIOS 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('203', 'GUIA PARA BENEFICIARIOS 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('204', 'GUIA PARA BENEFICIARIOS 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('205', 'GUIA PARA BENEFICIARIOS 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('206', 'GUIA PARA BENEFICIARIOS 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('207', 'GUIA PARA CIUDADANOS 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('208', 'GUIA PARA CIUDADANOS 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('209', 'GUIA PARA CIUDADANOS 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('210', 'GUIA PARA CIUDADANOS 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('211', 'GUIA PARA CIUDADANOS 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('212', 'GUIA PARA INTEGRANTESD E COMITÉ 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('213', 'GUIA PARA INTEGRANTESD E COMITÉ 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('214', 'GUIA PARA INTEGRANTESD E COMITÉ 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('215', 'GUIA PARA INTEGRANTESD E COMITÉ 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('216', 'GUIA PARA INTEGRANTESD E COMITÉ 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('217', 'GUIA PARA SERVIDORES PUBLICOS 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('218', 'GUIA PARA SERVIDORES PUBLICOS 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('219', 'GUIA PARA SERVIDORES PUBLICOS 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('220', 'GUIA PARA SERVIDORES PUBLICOS 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('221', 'GUIA PARA SERVIDORES PUBLICOS 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:39');
INSERT INTO "public"."cs_catalogo_general" VALUES ('222', 'INTERNET 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('223', 'INTERNET 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('224', 'INTERNET 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('225', 'INTERNET 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('226', 'INTERNET 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('227', 'MANUAL PARA BENEFICIARIOS 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('228', 'MANUAL PARA BENEFICIARIOS 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('229', 'MANUAL PARA BENEFICIARIOS 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('230', 'MANUAL PARA BENEFICIARIOS 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('231', 'MANUAL PARA BENEFICIARIOS 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('232', 'MANUAL PARA CIUDADANOS 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('233', 'MANUAL PARA CIUDADANOS 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('234', 'MANUAL PARA CIUDADANOS 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('235', 'MANUAL PARA CIUDADANOS 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('236', 'MANUAL PARA CIUDADANOS 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('237', 'MANUAL PARA INTEGRANTESD E COMITÉ 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('238', 'MANUAL PARA INTEGRANTESD E COMITÉ 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('239', 'MANUAL PARA INTEGRANTESD E COMITÉ 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('240', 'MANUAL PARA INTEGRANTESD E COMITÉ 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('241', 'MANUAL PARA INTEGRANTESD E COMITÉ 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('242', 'MANUAL PARA SERVIDORES PUBLICOS 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:40');
INSERT INTO "public"."cs_catalogo_general" VALUES ('243', 'MANUAL PARA SERVIDORES PUBLICOS 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('244', 'MANUAL PARA SERVIDORES PUBLICOS 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('245', 'MANUAL PARA SERVIDORES PUBLICOS 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('246', 'MANUAL PARA SERVIDORES PUBLICOS 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('247', 'PERIFONEO 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('248', 'PERIFONEO 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('249', 'PERIFONEO 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('250', 'PERIFONEO 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('251', 'PERIFONEO 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('252', 'PRESENTACION DE POWER POINT 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('253', 'PRESENTACION DE POWER POINT 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('254', 'PRESENTACION DE POWER POINT 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('255', 'PRESENTACION DE POWER POINT 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('256', 'PRESENTACION DE POWER POINT 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('257', 'RADIO 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('258', 'RADIO 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('259', 'RADIO 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('260', 'RADIO 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('261', 'RADIO 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:41');
INSERT INTO "public"."cs_catalogo_general" VALUES ('262', 'TELEVISION 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('263', 'TELEVISION 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('264', 'TELEVISION 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('265', 'TELEVISION 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('266', 'TELEVISION 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('267', 'VIDEOS 1', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('268', 'VIDEOS 2', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('269', 'VIDEOS 3', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('270', 'VIDEOS 4', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('271', 'VIDEOS 5', 'material_capacitacion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('272', 'ASAMBLEA COMUNITARIAS 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('273', 'ASAMBLEA COMUNITARIAS 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('274', 'ASAMBLEA COMUNITARIAS 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('275', 'ASAMBLEA COMUNITARIAS 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('276', 'ASAMBLEA COMUNITARIAS 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('277', 'CARTELES 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('278', 'CARTELES 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('279', 'CARTELES 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('280', 'CARTELES 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('281', 'CARTELES 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('282', 'ESPECTACULARES 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:42');
INSERT INTO "public"."cs_catalogo_general" VALUES ('283', 'ESPECTACULARES 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('284', 'ESPECTACULARES 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('285', 'ESPECTACULARES 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('286', 'ESPECTACULARES 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('287', 'FOLLETOS 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('288', 'FOLLETOS 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('289', 'FOLLETOS 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('290', 'FOLLETOS 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('291', 'FOLLETOS 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('292', 'GUIAS Y MANUALES 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('293', 'GUIAS Y MANUALES 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('294', 'GUIAS Y MANUALES 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('295', 'GUIAS Y MANUALES 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('296', 'GUIAS Y MANUALES 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('297', 'INTERNET 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('298', 'INTERNET 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('299', 'INTERNET 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('300', 'INTERNET 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('301', 'INTERNET 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:43');
INSERT INTO "public"."cs_catalogo_general" VALUES ('302', 'MANTA 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('303', 'MANTA 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('304', 'MANTA 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('305', 'MANTA 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('306', 'MANTA 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('307', 'PERIFONEO 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('308', 'PERIFONEO 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('309', 'PERIFONEO 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('310', 'PERIFONEO 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('311', 'PERIFONEO 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('312', 'PERIODICO MURAL 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('313', 'PINTADA DE BARDA 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('314', 'PINTADA DE BARDA 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('315', 'PINTADA DE BARDA 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('316', 'PINTADA DE BARDA 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('317', 'PINTADA DE BARDA 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('318', 'RADIO 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('319', 'RADIO 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('320', 'RADIO 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('321', 'RADIO 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:44');
INSERT INTO "public"."cs_catalogo_general" VALUES ('322', 'RADIO 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('323', 'TELEVISION 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('324', 'TELEVISION 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('325', 'TELEVISION 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('326', 'TELEVISION 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('327', 'TELEVISION 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('328', 'TRIPTICO 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('329', 'TRIPTICO 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('330', 'TRIPTICO 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('331', 'TRIPTICO 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('332', 'TRIPTICO 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('333', 'VIDEO 1', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('334', 'VIDEO 2', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('335', 'VIDEO 3', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('336', 'VIDEO 4', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');
INSERT INTO "public"."cs_catalogo_general" VALUES ('337', 'VIDEO 5', 'material_difusion', null, null, '1', '2018-05-18 09:53:45');

-- ----------------------------
-- Table structure for cs_catalogo_informes
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_catalogo_informes";
CREATE TABLE "public"."cs_catalogo_informes" (
"id" int4 DEFAULT nextval('catalogo_informes_id_seq'::regclass) NOT NULL,
"catalogo" varchar(300) COLLATE "default",
"seccion" varchar(30) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_catalogo_informes
-- ----------------------------
INSERT INTO "public"."cs_catalogo_informes" VALUES ('1', 'Solicitar información de las obras , apoyos o servicios ', 'actividades');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('2', 'Verificar el cumplimiento de las obras , apoyos o servicios ', 'actividades');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('3', 'Vigilar el uso correcto de los recursos del Programa', 'actividades');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('4', 'Informar a otros(as) beneficiarios(as) sobre el Programa', 'actividades');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('5', 'Verificar la entrega a tiempo de la obra, apoyo o servicio.', 'actividades');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('6', 'Contestar informes de Contraloria Social', 'actividades');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('7', 'Reunirse con Servidores Publicos y/o beneficiarios(as)', 'actividades');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('8', 'No deseamos responder / No sabemos', 'actividades');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('9', 'Para gestionar o tramitar las obras , apoyos o servicios del Programa', 'utilidad_cs');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('10', '
Para recibir oportunadamente las obras, apoyos o servicios', 'utilidad_cs');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('11', 'Para recibir mejor calidad en las obras, apoyos, o servicios del Programa', 'utilidad_cs');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('12', 'Para conocer y ejercer nuestros derechos como beneficiarios(as)', 'utilidad_cs');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('13', 'Para cumplir nuestras responsabilidades como beneficiarios(as)', 'utilidad_cs');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('14', 'Para que se atiendan nuestras quejas', 'utilidad_cs');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('15', 'Para que el programa funcione mejor', 'utilidad_cs');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('16', 'Para que los servidores públicos rindan cuentas de los recursos del Programa', 'utilidad_cs');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('17', 'No deseamos responder / No sabemos', 'utilidad_cs');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('18', 'Objetivos del Programa', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('19', 'Beneficios que otorga el Programa', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('20', 'Requisitos para ser beneficiario(a)', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('21', 'Tipo y monto de las obras, apoyos o servicios a realizarse', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('22', 'Dependencias que aportan los recursos para el Programa', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('23', 'Conformacion y funciones del comite o vocal', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('24', 'Donde presentar quejas y denuncias', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('25', 'Derechos y obligaciones de quienes operan el programa', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('26', 'Derechos y obligaciones de los beneficiarios(as)', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('27', 'Formas de hacer contraloria social', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('28', 'Periodo de ejecucion d ela obra o fechas de enrega de los apoyos o servicios', 'informacion_conocen');
INSERT INTO "public"."cs_catalogo_informes" VALUES ('29', 'No deseamos responder / No sabemos', 'informacion_conocen');

-- ----------------------------
-- Table structure for cs_comite_obra
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_comite_obra";
CREATE TABLE "public"."cs_comite_obra" (
"id" int4 NOT NULL,
"fk_comite" int4,
"fk_obra" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_comite_obra
-- ----------------------------

-- ----------------------------
-- Table structure for cs_contenido
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_contenido";
CREATE TABLE "public"."cs_contenido" (
"id" int4 NOT NULL,
"contenido" varchar(100) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_contenido
-- ----------------------------
INSERT INTO "public"."cs_contenido" VALUES ('1', 'texto');
INSERT INTO "public"."cs_contenido" VALUES ('2', 'catalogo');

-- ----------------------------
-- Table structure for cs_departamento
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_departamento";
CREATE TABLE "public"."cs_departamento" (
"id" int4 DEFAULT nextval('departamento_id_seq'::regclass) NOT NULL,
"departamento" varchar(64) COLLATE "default",
"tipo" varchar(64) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_departamento
-- ----------------------------
INSERT INTO "public"."cs_departamento" VALUES ('1', 'CS', 'CONTRALORIA');
INSERT INTO "public"."cs_departamento" VALUES ('2', 'SEDESOQ', 'EJECUTORA');
INSERT INTO "public"."cs_departamento" VALUES ('3', 'ADMINCS', 'GENERAL');
INSERT INTO "public"."cs_departamento" VALUES ('4', 'SUPERADMIN', 'SISTEMA');
INSERT INTO "public"."cs_departamento" VALUES ('5', 'USEBEQ', 'EJECUTORA');
INSERT INTO "public"."cs_departamento" VALUES ('6', 'SEDEA', 'EJECUTORA');
INSERT INTO "public"."cs_departamento" VALUES ('7', 'CONALEP', 'EJECUTORA');
INSERT INTO "public"."cs_departamento" VALUES ('8', 'CEIQ', 'EJECUTORA');
INSERT INTO "public"."cs_departamento" VALUES ('9', 'CEA', 'EJECUTORA');

-- ----------------------------
-- Table structure for cs_files
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_files";
CREATE TABLE "public"."cs_files" (
"id" int4 DEFAULT nextval('files_id_seq'::regclass) NOT NULL,
"filename" varchar(300) COLLATE "default",
"seccion" varchar(4) COLLATE "default",
"subseccion" varchar(100) COLLATE "default",
"title" int4,
"updated" date
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_files
-- ----------------------------

-- ----------------------------
-- Table structure for cs_instancia_ejecutora
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_instancia_ejecutora";
CREATE TABLE "public"."cs_instancia_ejecutora" (
"id" int4 DEFAULT nextval('instancia_ejecutora_id_seq'::regclass) NOT NULL,
"instancia_ejecutora" varchar(150) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_instancia_ejecutora
-- ----------------------------
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('1', 'CEA');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('2', 'CEIQ');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('3', 'CDI');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('4', 'DIF');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('5', 'IIFEQ');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('6', 'P.M.AMEALCO DE BONFIL');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('7', 'P.M.ARROYO SECO');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('8', 'P.M.CADEREYTA DE MONTES');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('9', 'P.M.COLÓN');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('10', 'P.M.CORREGIDORA');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('11', 'P.M.EL MARQUÉS');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('12', 'P.M.EZEQUIEL MONTES');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('13', 'P.M.HUIMILPAN');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('14', 'P.M.JALPAN DE SERRA');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('15', 'P.M.LANDA DE MATAMOROS');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('16', 'P.M.PEDRO ESCOBEDO');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('17', 'P.M.PEÑAMILLER');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('18', 'P.M.PINAL DE AMOLES');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('19', 'P.M.QUERÉTARO');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('20', 'P.M.SAN JOAQUÍN');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('21', 'P.M.SAN JUAN DEL RÍO');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('22', 'P.M.TEQUISQUIAPAN');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('23', 'P.M.TOLIMÁN');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('24', 'PROSPERA');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('25', 'SDUOP');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('26', 'SEDEQ');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('27', 'SEDESOQ');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('28', 'SEDIF');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('29', 'SESEQ');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('30', 'SNEQ');
INSERT INTO "public"."cs_instancia_ejecutora" VALUES ('31', 'USEBEQ');

-- ----------------------------
-- Table structure for cs_instancia_promotora
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_instancia_promotora";
CREATE TABLE "public"."cs_instancia_promotora" (
"id" int4 DEFAULT nextval('instancia_promotora_id_seq'::regclass) NOT NULL,
"instancia_promotora" varchar(150) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_instancia_promotora
-- ----------------------------
INSERT INTO "public"."cs_instancia_promotora" VALUES ('1', 'CEA');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('2', 'CEIQ');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('3', 'CDI');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('4', 'DIF');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('5', 'IIFEQ');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('6', 'P.M.AMEALCO DE BONFIL');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('7', 'P.M.ARROYO SECO');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('8', 'P.M.CADEREYTA DE MONTES');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('9', 'P.M.COLÓN');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('10', 'P.M.CORREGIDORA');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('11', 'P.M.EL MARQUÉS');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('12', 'P.M.EZEQUIEL MONTES');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('13', 'P.M.HUIMILPAN');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('14', 'P.M.JALPAN DE SERRA');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('15', 'P.M.LANDA DE MATAMOROS');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('16', 'P.M.PEDRO ESCOBEDO');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('17', 'P.M.PEÑAMILLER');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('18', 'P.M.PINAL DE AMOLES');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('19', 'P.M.QUERÉTARO');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('20', 'P.M.SAN JOAQUÍN');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('21', 'P.M.SAN JUAN DEL RÍO');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('22', 'P.M.TEQUISQUIAPAN');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('23', 'P.M.TOLIMÁN');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('24', 'PROSPERA');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('25', 'SDUOP');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('26', 'SEDEQ');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('27', 'SEDESOQ');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('28', 'SEDIF');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('29', 'SESEQ');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('30', 'SNEQ');
INSERT INTO "public"."cs_instancia_promotora" VALUES ('31', 'USEBEQ');

-- ----------------------------
-- Table structure for cs_localidades
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_localidades";
CREATE TABLE "public"."cs_localidades" (
"id" int4 DEFAULT nextval('localidades_id_seq'::regclass) NOT NULL,
"clave" varchar(20) COLLATE "default" NOT NULL,
"localidad" varchar(200) COLLATE "default" NOT NULL,
"fk_municipio" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_localidades
-- ----------------------------
INSERT INTO "public"."cs_localidades" VALUES ('1', '10008', 'El Capulín', '1');
INSERT INTO "public"."cs_localidades" VALUES ('2', '10029', 'Santiago Mexquititlán Barrio 5to. (El Pastoreo)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('3', '10047', 'Santiago Mexquititlán Barrio 3ro.', '1');
INSERT INTO "public"."cs_localidades" VALUES ('4', '10070', 'Yosphí', '1');
INSERT INTO "public"."cs_localidades" VALUES ('5', '10092', 'Barrio la Esperanza (San Bartolo)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('6', '10097', 'El Jaral (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('7', '10112', 'Barrio la Joya', '1');
INSERT INTO "public"."cs_localidades" VALUES ('8', '10117', 'El Colorín', '1');
INSERT INTO "public"."cs_localidades" VALUES ('9', '10120', 'La Cañada del Varal', '1');
INSERT INTO "public"."cs_localidades" VALUES ('10', '10121', 'Cuisillo (Barrio de San Ildefonso)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('11', '10135', 'La Loma del Rosario', '1');
INSERT INTO "public"."cs_localidades" VALUES ('12', '10143', 'Tesquedó (Puerto del Chivato)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('13', '10172', 'El Río (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('14', '10174', 'Loma de los Blases', '1');
INSERT INTO "public"."cs_localidades" VALUES ('15', '10177', 'Cerro del Gallo', '1');
INSERT INTO "public"."cs_localidades" VALUES ('16', '10184', 'La Esperanza (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('17', '10189', 'San Mateo (Palos Altos)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('18', '10003', 'El Apartadero', '1');
INSERT INTO "public"."cs_localidades" VALUES ('19', '10004', 'El Aserrín (Ojo de Agua del Hornito)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('20', '10005', 'El Atorón', '1');
INSERT INTO "public"."cs_localidades" VALUES ('21', '10006', 'El Batán', '1');
INSERT INTO "public"."cs_localidades" VALUES ('22', '10009', 'El Carmen (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('23', '10010', 'La Cofradía', '1');
INSERT INTO "public"."cs_localidades" VALUES ('24', '10012', 'El Varal', '1');
INSERT INTO "public"."cs_localidades" VALUES ('25', '10013', 'Chitejé de la Cruz', '1');
INSERT INTO "public"."cs_localidades" VALUES ('26', '10014', 'Donicá', '1');
INSERT INTO "public"."cs_localidades" VALUES ('27', '10016', 'Galindillo', '1');
INSERT INTO "public"."cs_localidades" VALUES ('28', '10017', 'Hacienda Blanca', '1');
INSERT INTO "public"."cs_localidades" VALUES ('29', '10018', 'La Isla', '1');
INSERT INTO "public"."cs_localidades" VALUES ('30', '10019', 'La Concepción (La Concha)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('31', '10021', 'Laguna de Servín', '1');
INSERT INTO "public"."cs_localidades" VALUES ('32', '10022', 'El Lindero', '1');
INSERT INTO "public"."cs_localidades" VALUES ('33', '10023', 'Loma Linda', '1');
INSERT INTO "public"."cs_localidades" VALUES ('34', '10024', 'La Manzana', '1');
INSERT INTO "public"."cs_localidades" VALUES ('35', '10025', 'Mesillas', '1');
INSERT INTO "public"."cs_localidades" VALUES ('36', '10028', 'Palos Altos', '1');
INSERT INTO "public"."cs_localidades" VALUES ('37', '10030', 'El Picacho', '1');
INSERT INTO "public"."cs_localidades" VALUES ('38', '10031', 'Jacal de la Piedad', '1');
INSERT INTO "public"."cs_localidades" VALUES ('39', '10032', 'El Pino', '1');
INSERT INTO "public"."cs_localidades" VALUES ('40', '10033', 'Quiotillos', '1');
INSERT INTO "public"."cs_localidades" VALUES ('41', '10034', 'El Rincón', '1');
INSERT INTO "public"."cs_localidades" VALUES ('42', '10035', 'El Salvador', '1');
INSERT INTO "public"."cs_localidades" VALUES ('43', '10036', 'Barrio San Antonio (San Bartolo)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('44', '10037', 'San Bartolomé del Pino (San Bartolo)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('45', '10038', 'San Felipe (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('46', '10039', 'San Ildefonso Tultepec (Centro)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('47', '10040', 'San José Ithó', '1');
INSERT INTO "public"."cs_localidades" VALUES ('48', '10041', 'San Juan Dehedó', '1');
INSERT INTO "public"."cs_localidades" VALUES ('49', '10042', 'San Martín', '1');
INSERT INTO "public"."cs_localidades" VALUES ('50', '10044', 'San Miguel Tlaxcaltepec (Barrio Centro)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('51', '10045', 'San Pablo', '1');
INSERT INTO "public"."cs_localidades" VALUES ('52', '10046', 'San Pedro Tenango', '1');
INSERT INTO "public"."cs_localidades" VALUES ('53', '10048', 'Santiago Mexquititlán Barrio 4to.', '1');
INSERT INTO "public"."cs_localidades" VALUES ('54', '10049', 'Santiago Mexquititlán Barrio 2do.', '1');
INSERT INTO "public"."cs_localidades" VALUES ('55', '10050', 'Santiago Mexquititlán Barrio 1ro.', '1');
INSERT INTO "public"."cs_localidades" VALUES ('56', '10051', 'El Saucito', '1');
INSERT INTO "public"."cs_localidades" VALUES ('57', '10052', 'La Soledad', '1');
INSERT INTO "public"."cs_localidades" VALUES ('58', '10053', 'Tenasdá (Barrio de San Ildefonso)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('59', '10054', 'El Tepozán (Barrio de San Ildefonso)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('60', '10055', 'Guadalupe el Terrero', '1');
INSERT INTO "public"."cs_localidades" VALUES ('61', '10057', 'La Alameda del Rincón', '1');
INSERT INTO "public"."cs_localidades" VALUES ('62', '10059', 'El Bothé', '1');
INSERT INTO "public"."cs_localidades" VALUES ('63', '10060', 'La Cruz (San Bartolo)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('64', '10062', 'Chitejé de Garabato', '1');
INSERT INTO "public"."cs_localidades" VALUES ('65', '10063', 'El Granjeno', '1');
INSERT INTO "public"."cs_localidades" VALUES ('66', '10065', 'La Piní', '1');
INSERT INTO "public"."cs_localidades" VALUES ('67', '10066', 'San Antonio la Labor', '1');
INSERT INTO "public"."cs_localidades" VALUES ('68', '10072', 'El Rayo', '1');
INSERT INTO "public"."cs_localidades" VALUES ('69', '10074', 'El Rincón de San Ildefonso', '1');
INSERT INTO "public"."cs_localidades" VALUES ('70', '10075', 'Los Arcos', '1');
INSERT INTO "public"."cs_localidades" VALUES ('71', '10077', 'La Estancia', '1');
INSERT INTO "public"."cs_localidades" VALUES ('72', '10080', 'Mal Paso (La Laguna)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('73', '10084', 'La Beata', '1');
INSERT INTO "public"."cs_localidades" VALUES ('74', '10086', 'La Mesa', '1');
INSERT INTO "public"."cs_localidades" VALUES ('75', '10087', 'Ejido de San Juan Dehedó', '1');
INSERT INTO "public"."cs_localidades" VALUES ('76', '10088', 'Rincón de la Florida', '1');
INSERT INTO "public"."cs_localidades" VALUES ('77', '10089', 'Los Árboles', '1');
INSERT INTO "public"."cs_localidades" VALUES ('78', '10090', 'El Capulín', '1');
INSERT INTO "public"."cs_localidades" VALUES ('79', '10093', 'Loma de las Víboras (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('80', '10094', 'El Cacahuate (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('81', '10095', 'Barrio de Santa Teresa (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('82', '10096', 'Tierras Negras (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('83', '10098', 'Amárcigo (San Luis)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('84', '10099', 'Los Arenales (San Juan Dehedó)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('85', '10100', 'Arroyo Hondo', '1');
INSERT INTO "public"."cs_localidades" VALUES ('86', '10101', 'El Rincón de Agua Buena (San Miguel Tlaxcaltepec)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('87', '10102', 'El Pueblito (San Miguel Tlaxcaltepec)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('88', '10103', 'Barrio de la Cruz (San Miguel Tlaxcaltepec)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('89', '10104', 'Barrio de la Isla (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('90', '10105', 'Barrio de la Ladera (San Miguel Tlaxcaltepec)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('91', '10106', 'La Venta (Santiago Mexquititlán Barrio 6to.)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('92', '10107', 'Barrio de San José (San Bartolo)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('93', '10108', 'Barrio Ojo de Agua (San Miguel Tlaxcaltepec)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('94', '10109', 'Barrio del Barco (San Miguel Tlaxcaltepec)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('95', '10111', 'Las Salvas', '1');
INSERT INTO "public"."cs_localidades" VALUES ('96', '10114', 'Barrio Presa del Tecolote (El Lindero)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('97', '10118', 'El Coyote', '1');
INSERT INTO "public"."cs_localidades" VALUES ('98', '10124', 'Las Grullas', '1');
INSERT INTO "public"."cs_localidades" VALUES ('99', '10125', 'Llano Largo', '1');
INSERT INTO "public"."cs_localidades" VALUES ('100', '10127', 'Loma de las Liebres (Los Árboles)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('101', '10128', 'Bordos Cuates', '1');
INSERT INTO "public"."cs_localidades" VALUES ('102', '10132', 'Rancho el Sol (Chitejé de la Cruz)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('103', '10137', 'San Antonio', '1');
INSERT INTO "public"."cs_localidades" VALUES ('104', '10139', 'Los Argueta', '1');
INSERT INTO "public"."cs_localidades" VALUES ('105', '10144', 'Tierra Negra', '1');
INSERT INTO "public"."cs_localidades" VALUES ('106', '10147', 'Vista Real', '1');
INSERT INTO "public"."cs_localidades" VALUES ('107', '10148', 'Xajay', '1');
INSERT INTO "public"."cs_localidades" VALUES ('108', '10151', 'Familia Rojas Narváez (Rancho Miranda)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('109', '10153', 'Santa Clara', '1');
INSERT INTO "public"."cs_localidades" VALUES ('110', '10156', 'El Baño', '1');
INSERT INTO "public"."cs_localidades" VALUES ('111', '10164', 'El Saucito Sección la Muralla', '1');
INSERT INTO "public"."cs_localidades" VALUES ('112', '10166', 'Callejón Bosdá', '1');
INSERT INTO "public"."cs_localidades" VALUES ('113', '10167', 'Cerro de los Gallos', '1');
INSERT INTO "public"."cs_localidades" VALUES ('114', '10168', 'Vera [Granja]', '1');
INSERT INTO "public"."cs_localidades" VALUES ('115', '10173', 'El Juvilete', '1');
INSERT INTO "public"."cs_localidades" VALUES ('116', '10180', 'El Cerrito', '1');
INSERT INTO "public"."cs_localidades" VALUES ('117', '10182', 'Rinconada de Bonfil [Fraccionamiento]', '1');
INSERT INTO "public"."cs_localidades" VALUES ('118', '10185', 'La Purísima', '1');
INSERT INTO "public"."cs_localidades" VALUES ('119', '10186', 'Llano Largo Maravillas', '1');
INSERT INTO "public"."cs_localidades" VALUES ('120', '10187', 'Loma de los Julianes', '1');
INSERT INTO "public"."cs_localidades" VALUES ('121', '10188', 'Loma del Chivo', '1');
INSERT INTO "public"."cs_localidades" VALUES ('122', '10001', 'Amealco de Bonfil', '1');
INSERT INTO "public"."cs_localidades" VALUES ('123', '10002', 'Agua Blanca', '1');
INSERT INTO "public"."cs_localidades" VALUES ('124', '10020', 'La Ladera', '1');
INSERT INTO "public"."cs_localidades" VALUES ('125', '10027', 'La Muralla', '1');
INSERT INTO "public"."cs_localidades" VALUES ('126', '10043', 'San Miguel Dehetí', '1');
INSERT INTO "public"."cs_localidades" VALUES ('127', '10056', 'San Nicolás de la Torre', '1');
INSERT INTO "public"."cs_localidades" VALUES ('128', '10076', 'Ejido el Rincón (La Botija)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('129', '10081', 'Las Lajas', '1');
INSERT INTO "public"."cs_localidades" VALUES ('130', '10085', 'Los Reyes', '1');
INSERT INTO "public"."cs_localidades" VALUES ('131', '10091', 'San José de los Encinos (Las Cabañas) [Fraccionamiento]', '1');
INSERT INTO "public"."cs_localidades" VALUES ('132', '10110', 'Barrio del Coyote (San Bartolo)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('133', '10113', 'Barrio la Paloma (San Bartolo)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('134', '10115', 'Bosques del Renacimiento', '1');
INSERT INTO "public"."cs_localidades" VALUES ('135', '10116', 'Buenos Aires (El Apartadero)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('136', '10119', 'La Cruz del Apartadero (La Garita)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('137', '10122', 'Rancho Casa Blanca', '1');
INSERT INTO "public"."cs_localidades" VALUES ('138', '10123', 'Ninguno', '1');
INSERT INTO "public"."cs_localidades" VALUES ('139', '10126', 'La Loma del Apartadero', '1');
INSERT INTO "public"."cs_localidades" VALUES ('140', '10129', 'Rancho San Andrés', '1');
INSERT INTO "public"."cs_localidades" VALUES ('141', '10130', 'Rancho el Capiro', '1');
INSERT INTO "public"."cs_localidades" VALUES ('142', '10131', 'Rancho el Siete (La Palizada)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('143', '10133', 'Rancho General Manuel Soberanes', '1');
INSERT INTO "public"."cs_localidades" VALUES ('144', '10134', 'Rancho la Mora', '1');
INSERT INTO "public"."cs_localidades" VALUES ('145', '10136', 'Rancho Monte Mercedes', '1');
INSERT INTO "public"."cs_localidades" VALUES ('146', '10138', 'San Carlos', '1');
INSERT INTO "public"."cs_localidades" VALUES ('147', '10140', 'Rancho Sin Fortuna', '1');
INSERT INTO "public"."cs_localidades" VALUES ('148', '10141', 'Rancho el Fresno', '1');
INSERT INTO "public"."cs_localidades" VALUES ('149', '10142', 'Sección Sur de San Juan Dehedó', '1');
INSERT INTO "public"."cs_localidades" VALUES ('150', '10146', 'Veinte de Noviembre', '1');
INSERT INTO "public"."cs_localidades" VALUES ('151', '10149', 'Las Américas (La Haciendita)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('152', '10150', 'La Perita el Sillar', '1');
INSERT INTO "public"."cs_localidades" VALUES ('153', '10152', 'Rancho San Antonio', '1');
INSERT INTO "public"."cs_localidades" VALUES ('154', '10154', 'Arroyo Colorado', '1');
INSERT INTO "public"."cs_localidades" VALUES ('155', '10155', 'La Atarjea', '1');
INSERT INTO "public"."cs_localidades" VALUES ('156', '10157', 'Mesa de San Martín', '1');
INSERT INTO "public"."cs_localidades" VALUES ('157', '10158', 'Familia Carapia', '1');
INSERT INTO "public"."cs_localidades" VALUES ('158', '10159', 'Nuevo Amanecer', '1');
INSERT INTO "public"."cs_localidades" VALUES ('159', '10160', 'La Perita [Fraccionamiento]', '1');
INSERT INTO "public"."cs_localidades" VALUES ('160', '10161', 'Rancho el Durazno (San Bartolo)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('161', '10162', 'Rancho la Guadalupana', '1');
INSERT INTO "public"."cs_localidades" VALUES ('162', '10163', 'Rancho los Tres Luises (Chitejé de la Cruz)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('163', '10165', 'Familia Anaya', '1');
INSERT INTO "public"."cs_localidades" VALUES ('164', '10169', 'Rancho los Altares', '1');
INSERT INTO "public"."cs_localidades" VALUES ('165', '10170', 'Rancho los Álvarez', '1');
INSERT INTO "public"."cs_localidades" VALUES ('166', '10171', 'Rancho el Durazno (San Miguel Deheti)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('167', '10175', 'El Pinar', '1');
INSERT INTO "public"."cs_localidades" VALUES ('168', '10176', 'CDI Amealco', '1');
INSERT INTO "public"."cs_localidades" VALUES ('169', '10178', 'Colonia México', '1');
INSERT INTO "public"."cs_localidades" VALUES ('170', '10179', 'Desviación a la Manzana', '1');
INSERT INTO "public"."cs_localidades" VALUES ('171', '10181', 'El Plan (San José Ithó)', '1');
INSERT INTO "public"."cs_localidades" VALUES ('172', '10183', 'Rinconada la Perita [Fraccionamiento]', '1');
INSERT INTO "public"."cs_localidades" VALUES ('173', '10190', 'Ejido Emiliano Zapata', '1');
INSERT INTO "public"."cs_localidades" VALUES ('174', '10191', 'La Noria', '1');
INSERT INTO "public"."cs_localidades" VALUES ('175', '10192', 'Quinta la Paz', '1');
INSERT INTO "public"."cs_localidades" VALUES ('1059', '20003', 'Adjunta de Gatos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1060', '20005', 'Aguacate de Morelos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1061', '20019', 'Cerro del Carmen', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1062', '20020', 'Carricillo Media Luna', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1063', '20021', 'El Carrizalito', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1064', '20027', 'Cuatro Palos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1065', '20034', 'Epazotes Grandes', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1066', '20041', 'Joyas de Bucareli', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1067', '20043', 'Loma de Guadalupe', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1068', '20046', 'Llano de San Francisco', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1069', '20052', 'Medias Coloradas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1070', '20053', 'Mesa de Ramírez', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1071', '20058', 'Otomites', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1072', '20059', 'Palo Grande', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1073', '20060', 'Pie de la Cuesta', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1074', '20074', 'El Rodezno', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1075', '20075', 'Puerto del Sabino', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1076', '20076', 'San Antonio del Pelón', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1077', '20078', 'San José Cochinito', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1078', '20094', 'La Muñeca', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1079', '20107', 'El Mastranto', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1080', '20110', 'El Mezquite', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1081', '20134', 'La Charca', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1082', '20138', 'Loma de las Minas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1083', '20144', 'El Pedregal', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1084', '20147', 'La Gallina', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1085', '20165', 'San Isidro', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1086', '20167', 'Hornitos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1087', '20179', 'Puerto Hondo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1088', '20181', 'Las Escaleras', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1089', '20186', 'La Sierrita', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1090', '20187', 'Agua Fría de la Barranca', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1091', '20196', 'Cruz Verde', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1092', '20197', 'La Escondida', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1093', '20211', 'La Joya', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1094', '20227', 'La Mojonera', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1095', '20239', 'Mesa Chiquita (Mesa del Comalito)', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1096', '20242', 'Casa Blanca', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1097', '20243', 'Cerro del Magueyal', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1098', '20002', 'Las Adjuntas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1099', '20004', 'Agua Amarga', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1100', '20006', 'Agua del Maíz', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1101', '20007', 'Agua Enterrada', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1102', '20009', 'Agua Fría de Gudiño', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1103', '20010', 'Agua Verde', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1104', '20012', 'Alejandría de Morelos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1105', '20013', 'El Arpa', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1106', '20014', 'Barranca del Plátano', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1107', '20015', 'La Barranca', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1108', '20017', 'Bucareli', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1109', '20018', 'El Cantón', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1110', '20022', 'La Cebolla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1111', '20023', 'Ciénega de San Juan', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1112', '20024', 'La Ciénega', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1113', '20025', 'Coatlán de los Ángeles', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1114', '20026', 'La Colgada', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1115', '20028', 'Cuesta Blanca', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1116', '20029', 'El Chuveje', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1117', '20030', 'Derramadero de Bucareli', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1118', '20031', 'Derramadero de Juárez', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1119', '20032', 'Durazno de San Francisco', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1120', '20033', 'El Durazno Grande', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1121', '20035', 'Epazotitos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1122', '20036', 'Escanelilla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1123', '20037', 'El Gallo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1124', '20039', 'Huazquilíco', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1125', '20040', 'Huilotla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1126', '20042', 'Joyas del Real', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1127', '20044', 'Loma Larga (Santa Cecilia)', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1128', '20047', 'Maby', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1129', '20049', 'Maguey Blanco', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1130', '20050', 'Magueycitos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1131', '20051', 'La Meca', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1132', '20054', 'La Mohonera', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1133', '20055', 'La Morita', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1134', '20056', 'El Murciélago', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1135', '20057', 'El Naranjo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1136', '20061', 'El Pilón', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1137', '20062', 'Los Pinos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1138', '20063', 'El Plátano', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1139', '20064', 'Potrerillos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1140', '20065', 'Puerto Colorado', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1141', '20066', 'Puerto del Derramadero', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1142', '20067', 'Puerto de Amoles', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1143', '20068', 'Puerto de Pujunguía', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1144', '20069', 'Puerto de Escanelilla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1145', '20070', 'Quirambal', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1146', '20071', 'El Ranchito', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1147', '20072', 'Rancho Nuevo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1148', '20073', 'Río Escanela', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1149', '20077', 'San Gaspar', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1150', '20079', 'San Pedro Viejo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1151', '20080', 'San Pedro Escanela', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1152', '20081', 'Santa Águeda', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1153', '20082', 'Sauz de Guadalupe', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1154', '20083', 'Puerto de Tejamaníl', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1155', '20084', 'El Timbre de Guadalupe', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1156', '20086', 'Tonatico', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1157', '20087', 'Tres Cruces', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1158', '20088', 'La Yerbabuena', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1159', '20089', 'El Limón', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1160', '20090', 'Puerto del Rodezno', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1161', '20091', 'Puerto de Huilotla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1162', '20092', 'Puerto de Vigas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1163', '20093', 'El Encino', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1164', '20096', 'El Tejocote', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1165', '20097', 'Cuesta de Huazmazontla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1166', '20098', 'El Refugio', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1167', '20100', 'Naranjo de Escanelilla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1168', '20102', 'Temazcales', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1169', '20103', 'La Esperanza', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1170', '20104', 'La Cañada', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1171', '20108', 'San Isidro', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1172', '20109', 'Casas Viejas (Agua Enterrada San Carlos)', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1173', '20114', 'Agua Fría', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1174', '20115', 'Aguacate de San Pedro', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1175', '20123', 'Las Mesas de Santa Inés', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1176', '20125', 'Las Cruces', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1177', '20126', 'Puerto de los Velázquez', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1178', '20128', 'Tierras Coloradas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1179', '20132', 'La Barrosa', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1180', '20136', 'La Joya de Ahuacatlán', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1181', '20137', 'Carrizal de Adjuntas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1182', '20139', 'Puerto de Alejandría', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1183', '20141', 'Sauz de Arroyo Hondo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1184', '20142', 'Arquitos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1185', '20145', 'Piedra Grande', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1186', '20150', 'La Quebradora', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1187', '20151', 'Encino de Ahuacatlán', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1188', '20152', 'El Llano de Huazquilíco', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1189', '20154', 'Puerto del Perico', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1190', '20157', 'El Cuervo de San Pedro', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1191', '20158', 'El Salto', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1192', '20159', 'Cuervo de San Rafael', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1193', '20160', 'Barrio de la Loma de San Pedro', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1194', '20161', 'Las Guayabas (El Limón)', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1195', '20162', 'Las Majaditas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1196', '20169', 'El Madroño', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1197', '20171', 'El Cuervo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1198', '20172', 'La Troja', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1199', '20177', 'Adjuntas de Ahuacatlán', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1200', '20178', 'Piedra Parada de Santa Rosa', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1201', '20182', 'Puerto de Tenamaxtle', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1202', '20184', 'La Mesa', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1203', '20188', 'Puerto del Oro', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1204', '20191', 'La Mesa de la Barranca', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1205', '20192', 'La Loma de la Ciénega', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1206', '20194', 'La Angostura', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1207', '20198', 'Puerto Hacienda Vieja', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1208', '20201', 'Las Mesitas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1209', '20202', 'Mesa del Soyatal', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1210', '20207', 'Puerto Pinto', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1211', '20208', 'Rancho Nuevo Dos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1212', '20210', 'San Martín', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1213', '20212', 'Mesas de San José', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1214', '20214', 'El Tambor de Huazquilíco', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1215', '20217', 'La Cañada', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1216', '20219', 'La Curva del Chuveje', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1217', '20222', 'La Joya de Santa Águeda', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1218', '20223', 'Joyas del Derramadero', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1219', '20224', 'El Limón de la Cruz', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1220', '20228', 'Buenavista', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1221', '20234', 'Curva Colorada', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1222', '20240', 'El Naranjo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1223', '20246', 'Laguna de Pitzquintla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1224', '20248', 'Lindero del Tejamanil', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1225', '20249', 'Loma Alta de Puerto Hondo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1226', '20250', 'Manzanillos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1227', '20252', 'Puerto Hondo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1228', '20253', 'Rincón de Pitzquintla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1229', '20001', 'Pinal de Amoles', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1230', '20011', 'Ahuacatlán de Guadalupe', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1231', '20038', 'Huajáles', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1232', '20085', 'La Tinaja', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1233', '20105', 'Tepozán de Derramadero', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1234', '20112', 'Cuesta de Santa Florentina', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1235', '20116', 'El Apartadero', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1236', '20119', 'La Fábrica', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1237', '20120', 'La Joya del Leal', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1238', '20122', 'Mazatiapán', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1239', '20124', 'Ojo de Agua', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1240', '20131', 'La Peña Colorada', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1241', '20135', 'Peña Alta', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1242', '20140', 'Arroyo Hondo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1243', '20143', 'Cuesta Colorada de Huazquilíco', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1244', '20146', 'El Bernalito', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1245', '20148', 'Puerto de Tepozán', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1246', '20149', 'Puerto del Pino (Puerta del Cielo)', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1247', '20153', 'Puerto del Molino', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1248', '20155', 'La Dinamita', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1249', '20166', 'La Cieneguilla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1250', '20170', 'Arroyo Hondo de Huilotla', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1251', '20173', 'El Zarzal', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1252', '20174', 'El Limón de Bucareli', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1253', '20176', 'Barbechos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1254', '20185', 'Cerro Grande', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1255', '20189', 'Santa Rita', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1256', '20190', 'El Tigre', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1257', '20193', 'Agua Cazuela', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1258', '20195', 'Apartadero', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1259', '20199', 'Loma de la Cañada', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1260', '20200', 'Loma del Revolcadero (La Joya)', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1261', '20204', 'Cerro de la Pingüica', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1262', '20205', 'El Puertecito', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1263', '20206', 'Puerto del Naranjo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1264', '20209', 'Las Vegas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1265', '20213', 'Huerta Don Manuel (Ojo de Agua)', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1266', '20215', 'Agua Caliente Chiquita', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1267', '20216', 'El Cantoncito', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1268', '20218', 'Los Chotes', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1269', '20220', 'La Gachupina', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1270', '20221', 'El Galéme', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1271', '20225', 'Los Lirios', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1272', '20229', 'Pueblo Nuevo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1273', '20230', 'Puerto Canoas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1274', '20231', 'El Ranchito', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1275', '20232', 'Familia Monroy Ángeles', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1276', '20233', 'El Soyatal', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1277', '20235', 'El Llano', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1278', '20236', 'Familia Gerardo', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1279', '20237', 'Las Cabañas Cinco Pinos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1280', '20238', 'Las Joyas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1281', '20241', 'Puerto de los Chinos', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1282', '20244', 'La Nopalera', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1283', '20245', 'La Tinaja', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1284', '20247', 'Las Tinajitas', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1285', '20251', 'Puerto de los Bueyes', '2');
INSERT INTO "public"."cs_localidades" VALUES ('1286', '30012', 'Cerro Blanco', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1287', '30027', 'La Loma', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1288', '30035', 'Panales', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1289', '30049', 'El Sabino', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1290', '30084', 'La Escondida de Hidalgo', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1291', '30099', 'Agua Fría de los Fresnos', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1292', '30002', 'Las Adjuntas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1293', '30003', 'El Aguacate', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1294', '30006', 'Ayutla', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1295', '30008', 'El Bosque', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1296', '30010', 'La Cantera', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1297', '30013', 'La Ciénega', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1298', '30016', 'El Durazno', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1299', '30018', 'La Estancia', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1300', '30019', 'Tanque Viejo', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1301', '30020', 'La Florida', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1302', '30023', 'El Jardín', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1303', '30024', 'La Laguna de Caballos (El Sabino)', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1304', '30025', 'Laguna de la Cruz', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1305', '30026', 'La Lagunita', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1306', '30030', 'Mesas de Agua Fría', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1307', '30032', 'La Mohonera', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1308', '30034', 'El Nogal', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1309', '30036', 'El Pino', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1310', '30038', 'El Pocito', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1311', '30044', 'El Refugio', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1312', '30045', 'El Rejalgar', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1313', '30047', 'Río del Carrizal', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1314', '30048', 'El Sabinito', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1315', '30050', 'El Salitrillo', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1316', '30053', 'Sanguijuela', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1317', '30054', 'San José de las Flores', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1318', '30055', 'San José del Tepame', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1319', '30057', 'Santa María de los Cocos', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1320', '30059', 'El Tepozán', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1321', '30060', 'Tierras Prietas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1322', '30061', 'Vegas Cuatas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1323', '30068', 'El Crucero del Sabinito', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1324', '30069', 'Las Trancas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1325', '30070', 'San Isidro', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1326', '30081', 'Tuna Manza', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1327', '30085', 'Puerto Ayutla', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1328', '30088', 'El Quirino', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1329', '30103', 'Casas Viejas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1330', '30108', 'El Ceronal (La Loma)', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1331', '30122', 'El Coyote', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1332', '30125', 'Las Adjuntas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1333', '30129', 'El Barrito', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1334', '30140', 'Barrio de la Cruz', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1335', '30143', 'La Ceja', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1336', '30145', 'Los Guillenes', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1337', '30152', 'El Riachuelo', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1338', '30156', 'La Tinaja', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1339', '30001', 'Arroyo Seco', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1340', '30005', 'Alpujarras', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1341', '30007', 'El Barro', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1342', '30009', 'Buenavista', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1343', '30014', 'Concá', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1344', '30015', 'El Charco', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1345', '30017', 'La Escondida de Guadalupe', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1346', '30022', 'La Huastequita', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1347', '30029', 'Mesa de Palo Blanco', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1348', '30031', 'Milpas Viejas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1349', '30037', 'El Pitahayo', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1350', '30040', 'Puerto de la Codicia', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1351', '30041', 'El Trapiche', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1352', '30042', 'Purísima de Arista', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1353', '30043', 'El Potrero del Rayo', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1354', '30052', 'San Felipe', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1355', '30056', 'San Juan Buenaventura', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1356', '30062', 'Rancho las Herraduras', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1357', '30063', 'Jesús María', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1358', '30064', 'El Ébano', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1359', '30066', 'El Platanito', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1360', '30067', 'Mesa del Coco', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1361', '30079', 'Puerto de la Cruz Verde', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1362', '30082', 'Puerto de Fátima', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1363', '30083', 'La Maroma', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1364', '30100', 'Los Álamos', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1365', '30111', 'El Naranjo', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1366', '30113', 'Ayutla (La Tapona) [Hotel]', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1367', '30114', 'Los Nogales', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1368', '30117', 'Hacienda Concá [Hotel]', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1369', '30118', 'Mesa de los Uribe', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1370', '30119', 'Las Moras', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1371', '30120', 'La Mora', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1372', '30121', 'Huerta Mariana', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1373', '30123', 'La Bodega (Crucero al Jardín)', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1374', '30127', 'La Matilla', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1375', '30128', 'Las Tablas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1376', '30130', 'El Rejalgar', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1377', '30131', 'Mesa del Platanito', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1378', '30133', 'Rancho la Ceiba', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1379', '30134', 'Los Cuisillos', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1380', '30135', 'Paculilla', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1381', '30136', 'El Rayo', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1382', '30137', 'San Francisco', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1383', '30138', 'Villa Riviera', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1384', '30139', 'El Paso de los Álamos', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1385', '30141', 'La Loma', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1386', '30142', 'El Capulín de los Briones (El Higuerón)', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1387', '30144', 'Localidad Sin Nombre (Familia Castañeda)', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1388', '30146', 'La Huastequita', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1389', '30147', 'Las Lagunitas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1390', '30148', 'La Pechuga', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1391', '30149', 'La Rana', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1392', '30150', 'Rancho las Colmenas', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1393', '30151', 'Rancho Nuevo', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1394', '30153', 'Los Capulines', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1395', '30154', 'El Potrero del Rayo (La Guardaraya)', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1396', '30155', 'Sección Oeste de Arroyo Seco', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1397', '30157', 'La Purísima [Granja]', '3');
INSERT INTO "public"."cs_localidades" VALUES ('1398', '40005', 'Altamira (La Bondotita)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1399', '40019', 'Carricillo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1400', '40046', 'El Hortelano', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1401', '40049', 'La Lagunita', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1402', '40055', 'La Luz (Carrizalito)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1403', '40061', 'Los Martínez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1404', '40065', 'La Mesa Providencia', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1405', '40066', 'Mesa del Castillo (La Fajilla)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1406', '40070', 'Ocotitlán', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1407', '40086', 'Puerto de la Luz', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1408', '40116', 'Tierras Coloradas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1409', '40117', 'El Timbre', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1410', '40132', 'Rancho la Honda (San Nicolás)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1411', '40141', 'Las Joyas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1412', '40144', 'El Banco (Cara de Palo)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1413', '40145', 'La Blanca', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1414', '40146', 'Vega de Ramírez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1415', '40183', 'El Naranjo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1416', '40191', 'Adjuntitas Dos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1417', '40208', 'La Calera (Los Guerreros)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1418', '40216', 'Santa Inés', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1419', '40231', 'Domuhí', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1420', '40234', 'Curva de la Doctorcilla', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1421', '40236', 'El Zothi (La Calera)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1422', '40251', 'El Chivo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1423', '40253', 'Escalerillas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1424', '40266', 'Las Peñitas (El Cuervo)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1425', '40287', 'Loma Ancha (La Maroma)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1426', '40288', 'Puerto del Zenthé', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1427', '40300', 'Adjuntitas Uno', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1428', '40340', 'Valle el Solís', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1429', '40343', 'El Torno', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1430', '40348', 'Mesa de San Ramón', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1431', '40350', 'Santa Mónica', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1432', '40353', 'Las Joyas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1433', '40359', 'Loma de San Pedro', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1434', '40367', 'El Jagüeycito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1435', '40371', 'Las Pilas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1436', '40003', 'El Aguacate', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1437', '40004', 'Agua Fría', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1438', '40006', 'Los Amolitos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1439', '40007', 'Apartaderito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1440', '40009', 'Rancho los Arteaga', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1441', '40010', 'Arroyo de Zituní', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1442', '40011', 'El Banco', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1443', '40012', 'Barrio de Guadalupe (Charco Frío)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1444', '40013', 'Boxasní', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1445', '40017', 'Camarones', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1446', '40018', 'La Carbonera', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1447', '40023', 'Cerro Colorado', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1448', '40024', 'Cerro Prieto', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1449', '40025', 'Corral Blanco', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1450', '40028', 'La Culata', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1451', '40030', 'Barrio de Santiago (Charco Frío)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1452', '40031', 'Chavarrías', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1453', '40032', 'El Chilar', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1454', '40035', 'El Divino Pastor', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1455', '40039', 'San Juan de Enramadas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1456', '40041', 'La Florida', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1457', '40047', 'Jabalí', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1458', '40048', 'Los Juárez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1459', '40050', 'La Laja', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1460', '40052', 'Los Lirios', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1461', '40054', 'Loma de Guadalupe', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1462', '40056', 'Los Llanitos de Pathé', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1463', '40058', 'Llanitos de Santa Bárbara', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1464', '40062', 'Los Maqueda', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1465', '40063', 'El Membrillo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1466', '40067', 'Mintehé', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1467', '40068', 'La Mora', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1468', '40074', 'El Ranchito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1469', '40076', 'La Pastilla', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1470', '40077', 'Pathé', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1471', '40078', 'Los Piñones', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1472', '40079', 'Portezuelo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1473', '40082', 'La Puerta', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1474', '40083', 'Puerto de la Concepción', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1475', '40084', 'Puerto de Chiquihuite', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1476', '40087', 'El Púlpito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1477', '40088', 'El Ranchito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1478', '40089', 'Rancho de Guadalupe', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1479', '40092', 'Rancho Nuevo Sombrerete', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1480', '40093', 'Rancho Quemado', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1481', '40094', 'Rancho Viejo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1482', '40095', 'El Rincón', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1483', '40096', 'Los Ríos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1484', '40097', 'San Antonio de la Cañada', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1485', '40099', 'San Javier', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1486', '40101', 'San Juan de la Rosa', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1487', '40102', 'San Martín Florida', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1488', '40104', 'Santa Bárbara', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1489', '40105', 'Santa María de Gracia', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1490', '40106', 'Santo Domingo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1491', '40107', 'Santo Tomás', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1492', '40108', 'El Sarro', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1493', '40110', 'El Socavón', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1494', '40111', 'Sombrerete', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1495', '40112', 'El Soyatal', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1496', '40113', 'El Suspiro', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1497', '40114', 'El Tepozán', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1498', '40115', 'El Terrero', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1499', '40118', 'La Tinaja', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1500', '40119', 'Tziquia', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1501', '40124', 'La Veracruz', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1502', '40125', 'Las Viguitas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1503', '40130', 'Yonthé', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1504', '40131', 'Zituní', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1505', '40133', 'La Mojonera', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1506', '40134', 'La Rinconada', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1507', '40135', 'El Pinalito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1508', '40142', 'El Devisadero', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1509', '40148', 'Banthí', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1510', '40152', 'Boyecito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1511', '40155', 'Xodhé', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1512', '40159', 'Tzibantzá', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1513', '40160', 'Taxhidó', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1514', '40165', 'El Carricillo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1515', '40169', 'Los Espinos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1516', '40172', 'El Huizache', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1517', '40174', 'Familia Trejo Reséndiz', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1518', '40180', 'El Plan', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1519', '40190', 'Los Remedios', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1520', '40192', 'San José Tepozán', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1521', '40195', 'Barranca del Sordo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1522', '40196', 'El Arbolito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1523', '40203', 'Cruz de Encino', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1524', '40204', 'La Adarga', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1525', '40205', 'Cerro Blanco', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1526', '40206', 'El Poblano (El Pacífico)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1527', '40207', 'El Limón', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1528', '40215', 'La Presa (La Nueva Presita)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1529', '40221', 'La Mesa', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1530', '40222', 'Mesa del Niño', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1531', '40225', 'Los Hernández', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1532', '40226', 'Rancho Nuevo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1533', '40227', 'Rancho Guadalupe', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1534', '40229', 'Cerro Boludo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1535', '40230', 'La Haciendita', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1536', '40232', 'Sierra Alta (El Fiscal)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1537', '40238', 'Xidhí', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1538', '40239', 'Adjuntas de Rancho Quemado', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1539', '40240', 'El Agua Salada', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1540', '40242', 'Barrio Alto', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1541', '40243', 'Barrio de los Barrones (La Concepción)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1542', '40245', 'Barrio los Sánchez (La Concepción)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1543', '40246', 'Barrio los Silvestres (La Concepción)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1544', '40249', 'Rancho Nuevo Sombrerete (Las Carreras)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1545', '40250', 'Casas Viejas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1546', '40252', 'Las Cuevas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1547', '40255', 'Nuevo Rancho Sombrerete (La Garita)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1548', '40257', 'La Lajita', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1549', '40258', 'La Loma Bonita', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1550', '40260', 'Loma Larga (La Maroma)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1551', '40261', 'Los Mateos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1552', '40267', 'Pie de la Loma', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1553', '40281', 'El Yeso', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1554', '40289', 'Puerto Verde', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1555', '40290', 'San José del Catiteo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1556', '40296', 'El Ranchito (Mintehé)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1557', '40307', 'Cerro Blanco Sección Dos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1558', '40309', 'El Charcón', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1559', '40361', 'La Bóveda', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1560', '40370', 'La Mora (La Mesa)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1561', '40372', 'Los Palmitos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1562', '40375', 'Colonia los Llanitos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1563', '40001', 'Cadereyta de Montes', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1564', '40015', 'Boyé', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1565', '40026', 'Los Cuates', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1566', '40029', 'Culebras', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1567', '40037', 'El Doctor', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1568', '40040', 'La Esperanza', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1569', '40045', 'Higuerillas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1570', '40059', 'Maconí', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1571', '40064', 'Mesa de León', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1572', '40069', 'La Nopalera', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1573', '40072', 'El Palmar (Santa María del Palmar)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1574', '40085', 'Puerto del Salitre', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1575', '40122', 'El Venado', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1576', '40123', 'La Venta', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1577', '40126', 'Villa Guerrero', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1578', '40127', 'Villa Nueva', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1579', '40129', 'Vizarrón de Montes', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1580', '40143', 'San Bartolo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1581', '40147', 'La Yerbabuena', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1582', '40162', 'Peñita Blanca', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1583', '40168', 'El Charco', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1584', '40175', 'Las Aguas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1585', '40178', 'Río Grande', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1586', '40179', 'La Hacienda de Tovares', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1587', '40197', 'Puerto de las Cabezas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1588', '40198', 'El Sauz', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1589', '40199', 'Tierra Colorada', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1590', '40200', 'El Duraznito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1591', '40212', 'Campamento CFE Los Espino', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1592', '40213', 'Bella Vista del Río', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1593', '40214', 'Mesa de León [Campamento]', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1594', '40219', 'Los Hornitos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1595', '40228', 'El Cercado (Palmarcito)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1596', '40233', 'Rancho de Canohitas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1597', '40235', 'Puerto de Guadalupe', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1598', '40237', 'Puerto Torres', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1599', '40241', 'Las Ánimas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1600', '40244', 'Barrio los González', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1601', '40247', 'Los Barrones', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1602', '40248', 'Calcimexicana', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1603', '40254', 'La Fosa (La Parada del Puerto)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1604', '40256', 'Cadereyta [Granja]', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1605', '40259', 'Loma del Gacho', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1606', '40262', 'Las Milpitas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1607', '40263', 'Misión de Maconí', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1608', '40264', 'Las Norias Derramadero', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1609', '40265', 'Los Olivos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1610', '40268', 'Ninguno', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1611', '40269', 'Procesadora de Mármol', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1612', '40270', 'Puerto del Durazno', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1613', '40271', 'Puerto del Mezquite', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1614', '40272', 'Rancho el Magueyal', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1615', '40273', 'Rancho Hacienda San Antonio', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1616', '40274', 'Rancho JB', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1617', '40275', 'Rancho la Luz', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1618', '40276', 'Rancho las Cenizas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1619', '40277', 'Rancho San Antonio', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1620', '40278', 'San José la Palma', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1621', '40280', 'Ninguno [Taller de Mármoles]', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1622', '40282', 'Arcia', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1623', '40283', 'El Bocoa', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1624', '40284', 'Corral de Encino', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1625', '40285', 'El Espolón (La Cabeza del Viborón)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1626', '40286', 'La Hacienda (El Organal)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1627', '40291', 'Tuna Manza', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1628', '40292', 'Alojamiento de Seguridad Física', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1629', '40293', 'Cerrito Colorado', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1630', '40294', 'Zimapán [Maquiladora]', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1631', '40295', 'La Mina', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1632', '40297', 'Rancho el Organal', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1633', '40298', 'Familia Medina', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1634', '40299', 'Familia Mendoza', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1635', '40301', 'Atarjeas las Ánimas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1636', '40302', 'Los Badillo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1637', '40303', 'Bordo de Santo Niño', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1638', '40304', 'Bordo del Tigre', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1639', '40305', 'Banco de Arena', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1640', '40306', 'Casas Viejas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1641', '40308', 'El Chaparral', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1642', '40310', 'El Chinillal', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1643', '40311', 'El Desdá', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1644', '40312', 'La Estancia (El Establo)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1645', '40313', 'Ex-Hacienda La Nopalera', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1646', '40314', 'Ex-hacienda Santa Bárbara', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1647', '40315', 'Familia Cortés', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1648', '40316', 'Familia Ezquivel', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1649', '40317', 'Familia González de la Cruz', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1650', '40318', 'Familia Martínez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1651', '40319', 'Familia Ramírez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1652', '40320', 'Familia Trejo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1653', '40321', 'Las Fuentes (La Peña)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1654', '40322', 'Diana [Granja]', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1655', '40323', 'El Doctor [Industria Marmolera]', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1656', '40324', 'El Llanito Redondo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1657', '40325', 'Predio las Alondras', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1658', '40326', 'El Mezquital', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1659', '40327', 'El Alacrán', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1660', '40328', 'Molinas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1661', '40329', 'Pie del Cerro', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1662', '40330', 'El Plancito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1663', '40331', 'Ranchito las Maravillas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1664', '40332', 'La Puerta de San Pedro', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1665', '40333', 'Puerto de Charco FrÍo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1666', '40334', 'Puerto el Atajo', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1667', '40335', 'Puerto La Majada', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1668', '40336', 'Rancho las Huertas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1669', '40337', 'Rancho los Enchilados', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1670', '40338', 'La Sábila (El Durazno)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1671', '40339', 'Rancho los Nachos', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1672', '40341', 'Martínez [Tabiquera]', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1673', '40342', 'Los Tepetates', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1674', '40344', 'Yexthó', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1675', '40346', 'Familia Alvarado Trejo (Lomas Verdes)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1676', '40347', 'Las Cruces (Familia Cruz Martínez)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1677', '40349', 'El Posdhú', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1678', '40351', 'Agua de Chivo (Los Luna)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1679', '40352', 'El Manantial', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1680', '40354', 'La Presa Vieja', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1681', '40355', 'La Loma (Corral Blanco)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1682', '40356', 'Unidad Habitacional CTM', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1683', '40357', 'Los Vázquez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1684', '40358', 'Llano Blanco', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1685', '40360', 'Rancho los MartÍnez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1686', '40362', 'Rancho la Morenita', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1687', '40363', 'Los García (El Vergel)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1688', '40364', 'Barrio Solares', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1689', '40365', 'El Arbolito 2', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1690', '40366', 'El Bufujá', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1691', '40368', 'El Refugio', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1692', '40369', 'Familia Ramírez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1693', '40373', 'Los Velázquez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1694', '40374', 'Maguey Verde', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1695', '40376', 'Los Manantiales', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1696', '40377', 'Barreras', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1697', '40378', 'Ejido Villa Guerrero Boñú Parcela 56 (Familia Trejo Ruedas)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1698', '40379', 'El Cerrito', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1699', '40380', 'El Depósito (Familia García Trejo)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1700', '40381', 'El Llano', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1701', '40382', 'El Planecito (Familia Martínez Flores)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1702', '40383', 'Familia Cruz Martínez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1703', '40384', 'Familia Garrido Palacios', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1704', '40385', 'Familia Mariño', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1705', '40386', 'Familia Ramírez Barrón', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1706', '40387', 'Revolución [Fraccionamiento]', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1707', '40388', 'El Arbolito [Hotel]', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1708', '40389', 'La Garita', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1709', '40390', 'La Iglesia Vieja', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1710', '40391', 'La Mesita (Familia Reséndiz Chávez)', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1711', '40392', 'Las Lomas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1712', '40393', 'Las Mesitas', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1713', '40394', 'Puerto Doctor', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1714', '40395', 'Puerto Grande', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1715', '40396', 'Punto de Arribo Xodhé', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1716', '40397', 'Rancho Antonio de Jesús', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1717', '40398', 'Rancho de Pedro Ramírez', '4');
INSERT INTO "public"."cs_localidades" VALUES ('1718', '50003', 'El Álamo Cuate', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1719', '50019', 'La Joya', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1720', '50029', 'Panales', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1721', '50065', 'La Concepción', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1722', '50102', 'La Palmita', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1723', '50112', 'Tierra Adentro', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1724', '50130', 'El Infiernillo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1725', '50166', 'La Zanja Grande', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1726', '50004', 'Nuevo Álamos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1727', '50005', 'Los Benitos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1728', '50006', 'El Blanco', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1729', '50007', 'Ejido Patria', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1730', '50008', 'La Carbonera', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1731', '50009', 'El Carrizal', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1732', '50011', 'Las Cenizas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1733', '50012', 'El Coyote', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1734', '50013', 'Esperanza', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1735', '50016', 'El Fuenteño', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1736', '50018', 'El Gallo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1737', '50020', 'El Leoncito', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1738', '50023', 'México Lindo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1739', '50026', 'Nogales', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1740', '50027', 'Purísima de Cubos (La Purísima)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1741', '50030', 'Peña Colorada', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1742', '50033', 'La Pila', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1743', '50034', 'El Poleo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1744', '50035', 'El Potrero', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1745', '50036', 'Puerta de Enmedio', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1746', '50037', 'El Mezote', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1747', '50039', 'Puerto de San Antonio', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1748', '50040', 'Los Quiotes (San José los Quiotes)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1749', '50041', 'Salitrera (Presa de la Soledad)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1750', '50042', 'San Francisco', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1751', '50043', 'San Ildefonso', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1752', '50047', 'Santa María Nativitas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1753', '50048', 'Santa Rosa de Lima (Santa Rosa Poblado)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1754', '50049', 'San Vicente el Alto (San Vicente)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1755', '50051', 'El Saucillo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1756', '50055', 'Los Trigos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1757', '50056', 'Urecho', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1758', '50058', 'Vista Hermosa', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1759', '50059', 'El Zamorano', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1760', '50060', 'La Zorra', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1761', '50062', 'Tanquecitos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1762', '50066', 'El Organal', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1763', '50073', 'Palmas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1764', '50074', 'Tierra Dura', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1765', '50096', 'Puerto del Coyote', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1766', '50099', 'Ailitos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1767', '50104', 'Santa Rosa Finca', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1768', '50152', 'Entronque Ajuchitlán', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1769', '50167', 'Ejido Colón Fracción del Moral', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1770', '50169', 'Rancho San Antonio (Colonia Álvaro Obregón)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1771', '50204', 'Nuevo Progreso', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1772', '50210', 'Ojo de Agua', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1773', '50211', 'La Quebradora', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1774', '50225', 'Ampliación Galeras Oeste', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1775', '50227', 'Barrio las Crucitas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1776', '50231', 'San Gabriel', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1777', '50001', 'Colón', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1778', '50002', 'Ajuchitlán', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1779', '50010', 'Casas Viejas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1780', '50015', 'El Estanco', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1781', '50017', 'Galeras', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1782', '50021', 'El Lindero', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1783', '50022', 'La Llosa (Rancho el Mezquite)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1784', '50025', 'Rancho la Montañesa', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1785', '50031', 'San José la Peñuela (La Peñuela)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1786', '50032', 'Piedras Negras', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1787', '50044', 'Rancho San Isidro', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1788', '50045', 'San Martín', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1789', '50046', 'Santa María de Guadalupe (Santa María del Mexicano)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1790', '50050', 'Rancho San Vicente (San Vicente el Bajo)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1791', '50052', 'Sauz Seco', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1792', '50053', 'Rancho la Soledad', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1793', '50057', 'Viborillas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1794', '50063', 'Peña Blanca', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1795', '50064', 'Las Palmas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1796', '50069', 'El Salitrillo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1797', '50070', 'El Sauz', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1798', '50071', 'Rancho Vista Hermosa', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1799', '50072', 'San Jorge', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1800', '50075', 'El Burral (Rancho Hermanos Flores)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1801', '50079', 'Rancho el Rocío', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1802', '50080', 'Rancho San Fernando', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1803', '50082', 'La Colmena', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1804', '50084', 'Mesa de la Cruz', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1805', '50087', 'El Terremote', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1806', '50088', 'El Arte', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1807', '50089', 'El Leoncito', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1808', '50095', 'Rancho RAY', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1809', '50097', 'El Cilguero (Pueblo Nuevo)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1810', '50098', 'Palo Alto', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1811', '50100', 'Pueblo Nuevo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1812', '50103', 'El Nuevo Rumbo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1813', '50105', 'El Crucero', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1814', '50106', 'La Ponderosa', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1815', '50107', 'Granjas Unidas Tolimán', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1816', '50109', 'Rancho San Antonio', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1817', '50110', 'Cultivando Ideas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1818', '50111', 'Las Adjuntas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1819', '50113', 'Cerro del Pinal del Zamorano', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1820', '50114', 'Laguna Seca', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1821', '50115', 'El Terrero', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1822', '50116', 'El Alfalfar [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1823', '50117', 'Rancho Galeras (Rancho los Olivos)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1824', '50118', 'El Cerrito de Don Félix', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1825', '50119', 'Hontoria (Marquesado de Guadalupe) [Establo]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1826', '50120', 'Ejido el Muerto (El Infiernillo)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1827', '50121', '4ta. Fracción Hontoria (Marquesado de Guadalupe)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1828', '50122', 'Amaral [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1829', '50123', 'El Milagro [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1830', '50124', 'La Luz [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1831', '50125', 'La Purísima [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1832', '50126', 'María del Carmen [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1833', '50127', 'Río [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1834', '50128', 'Feja [Maquiladora]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1835', '50129', 'La Hondonada', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1836', '50131', 'Mal Paso', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1837', '50132', 'El Nogalito', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1838', '50134', 'Rancho los Laureles', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1839', '50135', 'La Mora', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1840', '50136', 'El Rancho', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1841', '50137', 'Rancho El Cascabel', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1842', '50138', 'Rancho el Jaguar', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1843', '50139', 'Rancho San Francisco', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1844', '50140', 'Rancho Guadalupe', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1845', '50141', 'Rancho la Aurora', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1846', '50142', 'Rancho la Mesita', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1847', '50143', 'Rancho la Perla', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1848', '50144', 'Rancho la Purísima', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1849', '50145', 'Rancho los Arrayanes', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1850', '50146', 'Rancho Milpillas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1851', '50147', 'Rancho Palo Seco', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1852', '50148', 'Rancho San Felipe de Jesús', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1853', '50149', 'Rancho San Judas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1854', '50150', 'Rancho San Martín', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1855', '50151', 'Rancho Santa Sofía', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1856', '50153', 'Familia Balderas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1857', '50156', 'Familia Nieves', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1858', '50157', 'Familia Trejo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1859', '50161', 'Acceso a Urecho', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1860', '50162', 'Familia Trejo Montes', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1861', '50164', 'El Terrero', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1862', '50165', 'Yerbabuena', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1863', '50168', 'Zona Granjas Zorrillo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1864', '50170', 'Zona Salida a Tolimán (La Frontera)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1865', '50171', 'Conamegra [Mina]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1866', '50172', 'La Esperanza [Fraccionamiento]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1867', '50173', 'Granja Buenos Aires', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1868', '50174', 'La Lomita', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1869', '50175', 'Nuevo Rancho Guadalupe', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1870', '50176', 'Rancho Florisol', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1871', '50177', 'Rancho la Trinidad', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1872', '50178', 'Rancho Los Cadetes', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1873', '50179', 'Rancho San Carlos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1874', '50180', 'Rancho San Sebastián (Loma Buenos Aires)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1875', '50181', 'Alamitos (La Higuerita)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1876', '50182', 'Ninguno [Avícola la Peñuela]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1877', '50183', 'Ninguno [Distribuidora de Explosivos Oviedo]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1878', '50184', 'Rancho el Griego', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1879', '50185', 'Ejido Purísima de Cubos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1880', '50186', 'Ejido San Ildefonso', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1881', '50187', 'Ejido San Vicente el Alto Pozo Dos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1882', '50188', 'Familia Mandujano', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1883', '50189', 'Familia Pérez', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1884', '50190', 'Gazu', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1885', '50191', 'Nuevo México [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1886', '50192', 'La Mina de Iris', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1887', '50193', 'El Saucito', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1888', '50194', 'Rancho el Conejo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1889', '50195', 'Rancho el Gavillero (Los Olivos)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1890', '50196', 'Rancho la Redonda', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1891', '50197', 'Rancho los Dos Carnales', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1892', '50198', 'Rancho los Guilles', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1893', '50199', 'San José', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1894', '50200', 'San Martín', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1895', '50201', 'Familia Hurtado', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1896', '50202', 'Acceso a Piedras Negras', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1897', '50203', 'Ninguno [Blockera San Martín]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1898', '50205', 'Fracc. H. Ferrocarrileros', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1899', '50206', 'Valvaco [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1900', '50207', 'José Apolinar Ramírez', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1901', '50208', 'Juanita Godoy de Feregrino', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1902', '50209', 'Kilómetro Cinco', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1903', '50212', 'Rancho el Moral', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1904', '50213', 'Rancho los Pericos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1905', '50214', 'Rancho San Agustín', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1906', '50215', 'Familia Ríos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1907', '50216', 'Rancho Cactus', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1908', '50217', 'Familia Dorantes Trejo (El Crucero)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1909', '50218', 'Ninguno [Carretera Querétaro Bernal Kilómetro 5.5]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1910', '50219', 'Mesa del Pino', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1911', '50220', 'Rancho el Salitrillo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1912', '50221', 'Rancho los Celajes', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1913', '50222', 'Sección Sur de Ajuchitlán', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1914', '50223', 'La Enramada', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1915', '50224', 'Rancho San José', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1916', '50226', 'Campo deportivo Ajuchitlan', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1917', '50228', 'Los Naranjos [Fraccionamiento]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1918', '50229', 'Rancho la Joya de los Quiotes', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1919', '50230', 'Rancho Santa Martha', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1920', '50232', 'Ampliación el Blanco', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1921', '50233', 'La Nueva Esperanza', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1922', '50234', 'El Puente (Familia Ávila Martínez)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1923', '50235', 'Familia Cabrera Arellano', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1924', '50236', 'Familia Maldonado Pérez', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1925', '50237', 'Familia Nicolás Bárcenas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1926', '50238', 'Familia Uribe Ugalde', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1927', '50239', 'María Guadalupe Velázquez [Granja]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1928', '50240', 'Mesa del Rebaño', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1929', '50241', 'Rancho Don Memo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1930', '50242', 'Rancho San Isidro (Luis Blanco Caballero)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1931', '50243', 'Antiguo Rastro de Pollos [Salida al Zamorano]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1932', '50244', 'Sección Sureste de Colón (Camino a la Pila)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1933', '50245', 'Sección Suroeste de Ajuchitlán', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1934', '50246', 'Ninguno [Carretera a Tolimán Kilómetro 14]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1935', '50247', 'Familia Hernández Chindo', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1936', '50248', 'La Ladera (Pedro Nicolás)', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1937', '50249', 'Palmas', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1938', '50250', 'Rancho el Toro Cachetón', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1939', '50251', 'Rancho Purísima de Cubos', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1940', '50252', 'Rancho San Ignacio [Centro Universitario SEICKOR]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1941', '50253', 'Ninguno [Unidad de Riego Ajuchitlán Tres] [Sección Suroeste de Ajuchitlán]', '5');
INSERT INTO "public"."cs_localidades" VALUES ('1942', '60043', 'Buenos Aires', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1943', '60073', 'Pozo Uno (Familia Castillo Mendoza)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1944', '60005', 'Hacienda el Batán', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1945', '60008', 'La Cantera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1946', '60011', 'Charco Blanco', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1947', '60013', 'El Jaral', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1948', '60021', 'La Poza', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1949', '60025', 'Puerta de San Rafael', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1950', '60026', 'La Purísima de la Cueva', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1951', '60032', 'San Rafael', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1952', '60060', 'Rancho la Pichona', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1953', '60078', 'Rancho Vanegas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1954', '60086', 'Taponas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1955', '60090', 'Rancho Camino Real', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1956', '60099', 'Colonia Popular Ecológica Valle de Oro', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1957', '60103', 'Ejido el Jaral', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1958', '60116', 'El Paraíso', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1959', '60125', 'Rancho San Isidro', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1960', '60143', 'Vista Hermosa', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1961', '60148', 'Lomas de la Cruz', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1962', '60176', 'Ninguno [Al Fondo del Callejón de la Saca]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1963', '60186', 'Bosques de Lourdes', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1964', '60187', 'Valle Dorado Dos Mil', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1965', '60226', 'San Felipe Calichar', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1966', '60231', 'Lomas de Zaragoza', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1967', '60235', 'Lomas de Charco Blanco', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1968', '60241', '20 de Enero', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1969', '60244', 'Colonia Doctores', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1970', '60255', 'Valle de los Pinos', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1971', '60257', 'Pan de Vida [Casa Hogar]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1972', '60278', 'Colonia Ricardo Flores Magón', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1973', '60284', 'La Cócona', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1974', '60286', 'Praderas de Lourdes', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1975', '60290', 'Las Cabañas (Colinas de los Ángeles)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1976', '60001', 'El Pueblito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1977', '60002', 'Arroyo Hondo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1978', '60004', 'Ex-hacienda Balvanera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1979', '60006', 'Bravo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1980', '60007', 'El Calichar', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1981', '60009', 'Ex-Hacienda El Cerrito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1982', '60010', 'Colonia los Ángeles', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1983', '60014', 'La Cueva', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1984', '60015', 'Lourdes', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1985', '60017', 'La Negreta', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1986', '60019', 'Los Olvera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1987', '60020', 'Pita', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1988', '60023', 'Presa de Bravo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1989', '60024', 'El Progreso (Las Trojas)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1990', '60027', 'Purísima de San Rafael', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1991', '60028', 'El Ranchito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1992', '60029', 'El Romeral', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1993', '60030', 'San Francisco', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1994', '60036', 'La Tinaja', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1995', '60037', 'Rancho Trojitas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1996', '60041', 'Las Torcas [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1997', '60044', 'San José de los Olvera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1998', '60046', 'Santa Virginia', '6');
INSERT INTO "public"."cs_localidades" VALUES ('1999', '60047', 'La Palma', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2000', '60048', 'Rivera del Río', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2001', '60049', 'Pozo la Corregidora', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2002', '60051', 'La Vega [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2003', '60052', 'Familia Mayorga', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2004', '60053', 'Pozo Cuatro', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2005', '60054', 'Establo Godínez', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2006', '60055', 'La Noria', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2007', '60056', 'Granjas el Cerro', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2008', '60057', 'Rancho el Sabino', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2009', '60058', 'Rancho Monteverde', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2010', '60059', 'Rancho el Chilicuil', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2011', '60068', 'Ex-Hacienda Tejeda', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2012', '60072', 'Colonia Amanecer Balvanera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2013', '60074', 'Colonia las Flores', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2014', '60076', 'Rancho el Cantarito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2015', '60077', 'San Miguelito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2016', '60080', 'Candiles', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2017', '60081', 'Vista Real y Country Club', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2018', '60083', 'Rancho la Borrasca', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2019', '60085', 'Fracción San Luis', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2020', '60087', 'Granja el Chacho', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2021', '60089', 'Familia Lira', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2022', '60091', 'Familia Rodríguez Hernández', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2023', '60093', 'Pozo Cinco', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2024', '60094', 'Familia Mendoza Pantoja', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2025', '60095', 'Bomba la Trinidad', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2026', '60096', 'Fracción dos el Cerrito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2027', '60097', 'Jardines de la Corregidora', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2028', '60098', 'Balvanera Polo y Country Club', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2029', '60104', 'Los Pinos (Familia Alanís Guillén)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2030', '60105', 'Los Pérez', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2031', '60107', 'El Encino', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2032', '60109', 'La Grilla [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2033', '60110', 'Rancho los Sauces', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2034', '60111', 'Obrajuelo [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2035', '60113', 'Colonia Maravillas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2036', '60115', 'Noviciado Marianista', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2037', '60117', 'Industrial Forrajera (Productor)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2038', '60118', 'Pozo Ex-hacienda el Cerrito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2039', '60119', 'Rancho el Álamo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2040', '60120', 'Rancho el Húngaro', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2041', '60121', 'Rancho Guadalupe', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2042', '60122', 'Rancho Pilares', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2043', '60124', 'Rancho San Fernando', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2044', '60126', 'Rancho San Juanico', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2045', '60127', 'Cañadas del Lago', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2046', '60128', 'Rancho Santa María de los Cobos', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2047', '60129', 'Santa María [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2048', '60130', 'Rancho Tres Marías', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2049', '60131', 'Colinda con Huimilpan', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2050', '60132', 'La Pasadita', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2051', '60133', 'Privada Tierras Negras', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2052', '60134', 'Localidad Sin Nombre (Familia Rivera)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2053', '60135', 'Localidad Sin Nombre (Familia Méndez)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2054', '60136', 'San Antonio del Puente', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2055', '60137', 'Familia de Alba', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2056', '60138', 'Rancho el Huerto', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2057', '60139', 'San Ignacio', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2058', '60140', 'Familia Romero Olvera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2059', '60142', 'Venceremos', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2060', '60144', 'El Álamo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2061', '60145', 'Rancho los Arrayanes', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2062', '60146', 'San José de los Olvera Sección Uno', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2063', '60147', 'San José de los Olvera Sección Dos', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2064', '60149', 'Camino a Taponas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2065', '60150', 'SER [Escuela]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2066', '60151', 'Casting Metals', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2067', '60152', 'EAS [Centro de Capacitación]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2068', '60153', 'El Cerrito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2069', '60154', 'Ninguno [Club Halcones de Querétaro]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2070', '60155', 'Los Cipreses', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2071', '60156', 'Colonia Bernardo Quintana', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2072', '60157', 'La Rinconada', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2073', '60158', 'Loma Bonita', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2074', '60159', 'Quinta las Jacarandas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2075', '60160', 'Loma del Sur', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2076', '60161', 'Pailería Industrial', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2077', '60162', 'Rancho la Asunción', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2078', '60163', 'La Pobreza [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2079', '60164', 'Don Manuel', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2080', '60165', 'Jaral de Charco Blanco', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2081', '60166', 'Familia Cortés', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2082', '60167', 'Casa [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2083', '60168', 'Familia García', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2084', '60169', 'Familia Guillén', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2085', '60170', 'Familia Linares', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2086', '60171', 'Familia Luna', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2087', '60172', 'Familia Mejía', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2088', '60173', 'Familia Ochoa', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2089', '60174', 'Familia Oria', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2090', '60175', 'El Conde [Fraccionamiento]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2091', '60177', 'Familia Sarti', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2092', '60178', 'Familia Schneider', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2093', '60179', 'Familia Arteaga Jiménez', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2094', '60180', 'El Saloncito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2095', '60181', 'Las Adjuntas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2096', '60182', 'Sección Norte de Colonia los Ángeles', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2097', '60183', 'Ninguno', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2098', '60184', 'Casa de Piedra', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2099', '60185', 'El Cerrito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2100', '60188', 'Vistas del Sol', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2101', '60189', 'Ninguno', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2102', '60190', 'Montero [Establo]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2103', '60191', 'Ninguno [Camino al Calichar Kilómetro 1.8]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2104', '60192', 'Familia López Ortíz', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2105', '60193', 'Familia Gutiérrez Mendoza', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2106', '60194', 'Familia Luna Morales', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2107', '60195', 'Familia Martínez Castillo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2108', '60196', 'Familia Martínez', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2109', '60197', 'Familia Lira Girón', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2110', '60198', 'Familia Montero Leal', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2111', '60199', 'Familia Natera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2112', '60200', 'Localidad Sin Nombre (Familia Rangel Hernández)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2113', '60201', 'Familia Rangel Olmos', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2114', '60202', 'Familia Salinas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2115', '60203', 'Granja Don Antonio', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2116', '60204', 'Don Lupe [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2117', '60205', 'Huerto la Soledad', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2118', '60206', 'Instituto Santa María Balvanera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2119', '60207', 'Doña Inés [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2120', '60209', 'Rancho el Zapote', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2121', '60210', 'Granjas Misioneras Marianas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2122', '60211', 'Pozo Don Paulino', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2123', '60212', 'Rancho del Álamo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2124', '60213', 'Rancho Jan', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2125', '60214', 'Rancho la Escarda', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2126', '60215', 'Familia Girón [Relleno Sanitario]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2127', '60216', 'El Compa', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2128', '60217', 'Cerro de la Huerta', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2129', '60218', 'Diana Laura', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2130', '60219', 'Colonia Los Pinos', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2131', '60220', 'Familia Alegría', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2132', '60221', 'Familia Herrera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2133', '60222', 'Familia Rangel Herrera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2134', '60223', 'Finca J. J.', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2135', '60224', 'El Calentano [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2136', '60225', 'Kilómetro Trece Punto Cinco', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2137', '60227', 'Sección Suroeste de Colonia Santa Bárbara', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2138', '60228', 'Ninguno', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2139', '60229', 'La Purísima [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2140', '60230', 'Triturados Briones', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2141', '60232', 'Conjunto Habitacional', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2142', '60233', 'Las Pirámides', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2143', '60234', 'Las Trojes', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2144', '60236', 'Parcela 127 Ejido del Pueblito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2145', '60237', 'Santuario del Cerrito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2146', '60238', 'Valle Real Residencial', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2147', '60239', 'Zona Noroeste de los Olvera', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2148', '60240', 'Zona Norte de Tejeda', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2149', '60242', 'Boulevares del Cimatario', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2150', '60243', 'Cabaña', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2151', '60245', 'Franco [Establo]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2152', '60246', 'Familia Alcocer Erbach', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2153', '60247', 'Familia García', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2154', '60248', 'Villa Dorada', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2155', '60249', 'Familia Hernández Mendoza', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2156', '60250', 'Familia López López', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2157', '60251', 'Familia Madrigal', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2158', '60252', 'Gonzalo Alcocer', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2159', '60253', 'Rancho el Rinconcito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2160', '60254', 'Rancho La Alborada', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2161', '60256', 'Ampliación los Ángeles', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2162', '60258', 'Ejido el Retablo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2163', '60259', 'San Francisco (Familia Avila)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2164', '60260', 'Rancho Valentín', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2165', '60261', 'La Joya', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2166', '60262', 'Marylola [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2167', '60263', 'Niños Héroes de Chapultepec', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2168', '60264', 'Real del Bosque', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2169', '60265', 'Rancho los Potrillos', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2170', '60266', 'San Agustín [Fraccionamiento]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2171', '60267', 'Luz María [Fraccionamiento]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2172', '60268', 'Pueblito Colonial Residencial', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2173', '60269', 'Paseos del Bosque Residencial', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2174', '60270', 'Colonia San Miguelito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2175', '60271', 'Santa Fe [Residencial]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2176', '60272', 'Familia Arroyo Carranza', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2177', '60273', 'Puerta Real Residencial & Country Club', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2178', '60274', 'Bahamas [Fraccionamiento]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2179', '60275', 'Ejido el Retablo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2180', '60276', 'Bosque de Viena', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2181', '60277', 'Casa Magna', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2182', '60279', 'El Paraíso [Relleno Sanitario]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2183', '60280', 'Familia Vázquez', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2184', '60281', 'La Princesa [Granja]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2185', '60282', 'Vid Abundante [Asociación religiosa]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2186', '60283', 'La Carambada', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2187', '60285', 'Parcela 4 Ejido Modelo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2188', '60287', 'Rancho las Cuatro Lupes', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2189', '60288', 'Rancho los Pérez', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2190', '60289', 'Terranova [Residencial]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2191', '60291', 'Punta Esmeralda [Fraccionamiento]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2192', '60292', 'El Borrego', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2193', '60293', 'El Cerrito', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2194', '60294', 'Las Mariposas', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2195', '60295', 'Los Aguilares (Majada Verde)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2196', '60296', 'Los Pinacates', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2197', '60297', 'San Jerónimo', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2198', '60298', 'Familia Álvarez Zúñiga', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2199', '60299', 'Familia García Campos', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2200', '60300', 'Familia Morán', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2201', '60301', 'La Vida (Fraccionamiento)', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2202', '60302', 'Los Agáves [Residencial]', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2203', '60303', 'Paisajes de Lourdes', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2204', '60304', 'Palmas de Charco Blanco', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2205', '60305', 'Praderas del Bosque', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2206', '60306', 'San Ángel', '6');
INSERT INTO "public"."cs_localidades" VALUES ('2207', '70041', 'Familia Martínez Hernández (Los Soto)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2208', '70094', 'Rancho la Coneja', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2209', '70174', 'Familia Camacho', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2210', '70229', 'El Sindicato', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2211', '70242', 'Familia Montes Trejo', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2212', '70274', 'Arboledas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2213', '70275', 'Loma Dorada', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2214', '70002', 'Barreras', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2215', '70004', 'El Bondotal', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2216', '70008', 'El Ciervo', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2217', '70010', 'El Coyote', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2218', '70015', 'Guanajuatito', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2219', '70021', 'Loberas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2220', '70025', 'Alfredo V. Bonfil (Los Pérez)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2221', '70028', 'La Purísima', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2222', '70029', 'Los Ramírez', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2223', '70031', 'Las Rosas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2224', '70033', 'San Antonio', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2225', '70035', 'Ejido Arroyo Colorado (Familia González Reséndiz)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2226', '70036', 'San José de los Trejo (Las Adelitas)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2227', '70045', 'Tunas Blancas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2228', '70047', 'Villa Progreso', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2229', '70057', 'El Cardonal (La Tijera)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2230', '70065', 'El Cerrito', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2231', '70072', 'San José del Jagüey', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2232', '70087', 'La Sala', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2233', '70102', 'Los Sánchez', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2234', '70103', 'Rancho Alba', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2235', '70119', 'Rancho el Bondotal', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2236', '70120', 'La Lomita', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2237', '70121', 'Rancho San Carlos', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2238', '70124', 'Los Arrollitos (La Candela)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2239', '70135', 'La Nueva Unidad', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2240', '70159', 'El Cerrito', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2241', '70165', 'Cerritos Barrio San Miguel', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2242', '70167', 'La Ermita', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2243', '70172', 'El Paraiso', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2244', '70179', 'Familia Vega Soto', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2245', '70182', 'Rancho Bothijí', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2246', '70185', 'La Luna', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2247', '70186', 'La Providencia (Las Norbertas)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2248', '70187', 'Valle Colorado', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2249', '70188', 'Ejido Arroyo Colorado', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2250', '70221', 'Rancho la Soledad', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2251', '70241', 'Sección Sur de Villa Progreso', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2252', '70247', 'La Soledad', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2253', '70254', 'Las Flores', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2254', '70255', 'Villa Nueva', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2255', '70265', 'Piedras Negras', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2256', '70271', 'Barrio Alto', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2257', '70279', 'Loma Alta', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2258', '70284', 'Santa Lucía', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2259', '70001', 'Ezequiel Montes', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2260', '70003', 'Bernal', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2261', '70005', 'Rancho Quemado', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2262', '70011', 'Los Cuates', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2263', '70012', 'Rancho el Depósito', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2264', '70013', 'Rancho los Encinos', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2265', '70016', 'La Higuera', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2266', '70017', 'El Jagüey Grande', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2267', '70023', 'La Palma [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2268', '70040', 'Sombrerete', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2269', '70042', 'Tasbatá', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2270', '70044', 'La Redonda (Familia Montes Vega)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2271', '70055', 'Rancho el Bellorín', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2272', '70056', 'Granja La Catalana', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2273', '70058', 'Rancho los Feregrinos', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2274', '70068', 'La Redonda de Abajo [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2275', '70077', 'Rancho Los Cadetes', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2276', '70081', 'La Esperanza', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2277', '70082', 'Rancho el Parral', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2278', '70091', 'Rancho Santa Martha', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2279', '70092', 'Los Zarazuas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2280', '70096', 'Montequis las Coloradas [Fraccionamiento]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2281', '70098', 'La Redonda [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2282', '70101', 'Rancho Juan Carlos', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2283', '70105', 'San Joaquín [Balneario]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2284', '70108', 'Rancho la Eria', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2285', '70109', 'Rancho el Queretano', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2286', '70110', 'La Laguna', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2287', '70111', 'El Aguacate', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2288', '70112', 'Rancho la Caja', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2289', '70114', 'Fraccionamiento y Balneario Termas del Rey', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2290', '70115', 'Hacienda de los Charcos', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2291', '70117', 'Rancho los Antonios', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2292', '70118', 'Rancho la Esperanza', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2293', '70122', 'Rancho el Machorril', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2294', '70123', 'Bothijí [Avícola]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2295', '70125', 'Cerro Chiquito', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2296', '70126', 'Rancho el Ciprés', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2297', '70127', 'El Conquistador [Club Deportivo]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2298', '70128', 'Cortijo de San Rafael', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2299', '70129', 'Ejido el Ciervo', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2300', '70130', 'Buenos Aires [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2301', '70131', 'San Antonio [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2302', '70132', 'La Mejor [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2303', '70133', 'La Purísima [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2304', '70134', 'Mina la Fe', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2305', '70136', 'El Ordaz', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2306', '70137', 'Proyecto de Vida IAP [Casa Hogar]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2307', '70138', 'Rancho Altamira (Los Soto)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2308', '70139', 'Rancho Daniela', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2309', '70140', 'Rancho el Berrendo', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2310', '70141', 'Rancho el Cacahuate', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2311', '70142', 'Rancho el Calabozo', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2312', '70143', 'Rancho el Milagro', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2313', '70144', 'Rancho el Romani', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2314', '70145', 'Rancho el Seis', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2315', '70146', 'Rancho el Socorro', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2316', '70147', 'Rancho Fracción de San Antonio', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2317', '70148', 'Rancho la Rinconada', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2318', '70149', 'Rancho las Coloradas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2319', '70150', 'Rancho la Palma', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2320', '70151', 'Giuliana [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2321', '70152', 'Rancho San Andrés', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2322', '70153', 'Rancho San Antonio', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2323', '70154', 'Vergel de la Peña', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2324', '70155', 'Rancho San Isidro (Familia Montes Palomino)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2325', '70156', 'Rancho la Puerta de San José', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2326', '70157', 'Rancho San José de Guadalupe', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2327', '70158', 'Rancho San Sebastián (Loma Buenos Aires)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2328', '70160', 'Rancho San Cristobal', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2329', '70161', 'Rancho las Jacarandas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2330', '70162', 'Rancho el Socorro (Familia Quijada González)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2331', '70163', 'Rancho Tasdejé', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2332', '70164', 'Rancho Xisdá', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2333', '70166', 'Localidad Sin Nombre (Familia Castillo Palma)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2334', '70168', 'Casa de Madera', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2335', '70169', 'El Zocoyote', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2336', '70170', 'Taller de Carrocerías', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2337', '70171', 'Palo Seco', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2338', '70173', 'Familia Bárcenas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2339', '70175', 'Familia Montes Hernández', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2340', '70176', 'Familia Hernández Lomas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2341', '70177', 'Familia Padilla Sagaz', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2342', '70178', 'Rancho Santa Elena (Familia Moreno)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2343', '70180', 'La Nueva Unidad Cardenista', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2344', '70181', 'Al Norte de Rancho la Rinconada', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2345', '70183', 'Basurero Municipal', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2346', '70184', 'El Campo del Diablo', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2347', '70189', 'Ejido los Pérez', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2348', '70190', 'Reynaldo Vega [Establo]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2349', '70191', 'Familia Aguilar Martínez', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2350', '70192', 'Familia Camacho', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2351', '70193', 'Familia Cruz Aguilar (El Coyote)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2352', '70194', 'Familia Martínez Martínez', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2353', '70195', 'Familia Ramírez Barrón', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2354', '70196', 'Granja Avícola La Providencia', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2355', '70197', 'El Cachito [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2356', '70198', 'El Chamizal [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2357', '70199', 'Hacienda Mexicana los Aztecas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2358', '70200', 'Los Arvizu [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2359', '70201', 'El Coyote [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2360', '70202', 'Familia Ángeles Madariaga (Las Granjas)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2361', '70203', 'Hacienda Real la Nogalera', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2362', '70204', 'Kilómetro 36 (La Bodega)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2363', '70205', 'Barajas [Mina]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2364', '70206', 'El Ordaz (Familia Hernández Hernández)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2365', '70207', 'El Ordaz (Familia Arteaga Ávila)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2366', '70208', 'La Propiedad (Campamento Palabra de Vida)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2367', '70209', 'Quinta Lupita', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2368', '70210', 'Rancho Arvizu Dorantes', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2369', '70211', 'Rancho Don Juan', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2370', '70212', 'Rancho Bordo Largo', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2371', '70213', 'Rancho el Carrizal', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2372', '70214', 'Rancho el Forastero', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2373', '70215', 'Rancho el Paraíso', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2374', '70216', 'Rancho el Tical', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2375', '70217', 'Rancho el Vergel', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2376', '70218', 'Rancho la Divina Providencia', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2377', '70219', 'Rancho la Purísima', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2378', '70220', 'Rancho la Querencia', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2379', '70222', 'Rancho los Camacho', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2380', '70223', 'Rancho los Cuates', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2381', '70224', 'Rancho los Olivos', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2382', '70225', 'Rancho Montes Dorantes', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2383', '70226', 'Rancho Noche Buena', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2384', '70227', 'Rancho el Tampiqueño', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2385', '70228', 'Rancho Rinconada los Truenos', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2386', '70230', 'Rancho San Isidro (Familia Montes)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2387', '70231', 'Rancho San Isidro (Familia Dorantes)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2388', '70232', 'Rancho las Coronelas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2389', '70233', 'Rancho la Divina Ilusión', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2390', '70234', 'Rancho la Puerta de San José', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2391', '70235', 'Rancho Santa Teresa', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2392', '70236', 'La Redonda de Abajo (Familia Montes Rubio)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2393', '70237', 'Campestre los Arcos [Residencial]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2394', '70238', 'Familia Arellano Hernández', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2395', '70239', 'Familia Hernández', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2396', '70240', 'La Redonda', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2397', '70243', 'Familia Montes Feregrino', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2398', '70244', 'Familia Martínez Reyes', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2399', '70245', 'Familia Cabrera Ramírez', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2400', '70246', 'Familia Pérez', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2401', '70248', 'Tiopiztlán', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2402', '70249', 'Unidad de Engorda del Ejido', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2403', '70250', 'Localidad Sin Nombre (Familia Reséndiz Palacios)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2404', '70251', 'Granja El Retoño', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2405', '70252', 'Granja La Catalana', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2406', '70253', 'Las Fuentes [Fraccionamiento]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2407', '70256', 'Familia Ángeles', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2408', '70257', 'Familia Reséndiz Reséndiz', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2409', '70258', 'Jardines de la Peña', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2410', '70259', 'Los García (El Vergel)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2411', '70260', 'Rancho Blanquita', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2412', '70261', 'Rancho el Papalote', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2413', '70262', 'Rancho el Rocío', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2414', '70263', 'Rancho Santa Cruz', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2415', '70264', 'La Poza (La Parada del Puerto)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2416', '70266', 'Nuevo Rancho Guadalupe', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2417', '70267', 'Acceso a Piedras Negras', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2418', '70268', 'H. Ferrocarrileros [Fraccionamiento]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2419', '70269', 'Kilómetro Cinco', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2420', '70270', 'Rancho el Milagro (Familia Reséndiz Vega)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2421', '70272', 'Barrio el Puerto', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2422', '70273', 'Barrio Nuevo Bernal (Macho Viejo)', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2423', '70276', 'Familia Montes', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2424', '70277', 'Ejido de Villa Progreso', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2425', '70278', 'Embuja', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2426', '70280', 'Rancho Cartujano', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2427', '70281', 'Rancho el Capricho', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2428', '70282', 'Rancho el Fresno', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2429', '70283', 'Rancho el Gallo Giro', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2430', '70285', 'Sector Canoa', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2431', '70286', 'Ninguno [Parque Industrial Cadereyta]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2432', '70287', 'La Cruz [Colonia]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2433', '70288', 'El Fresno', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2434', '70289', 'Familia Cruz Martínez', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2435', '70290', 'Famila Jiménez Aguirre', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2436', '70291', 'Familia León Ocampo', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2437', '70292', 'Familia Pérez Vega', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2438', '70293', 'La Mina Colorada', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2439', '70294', 'La Poma', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2440', '70295', 'Las Camelinas', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2441', '70296', 'Los Joseses [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2442', '70297', 'Miramar [Granja]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2443', '70298', 'Rancho Arroyo Negro', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2444', '70299', 'Rancho el Porvenir', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2445', '70300', 'Rancho el Sauz', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2446', '70301', 'Rancho los Canarios', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2447', '70302', 'Rancho San Pedro III', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2448', '70303', 'Rancho Taurino', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2449', '77005', 'Ninguno [Parque Industrial Cadereyta]', '7');
INSERT INTO "public"."cs_localidades" VALUES ('2450', '80091', 'Los Timoteo', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2451', '80002', 'Apapátaro', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2452', '80004', 'Los Bordos', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2453', '80006', 'Huitrón', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2454', '80008', 'Carranza (San Antonio)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2455', '80010', 'Los Cues', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2456', '80011', 'La Cuesta', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2457', '80012', 'El Fresno', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2458', '80013', 'El Granjeno', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2459', '80014', 'Guadalupe Primero', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2460', '80015', 'La Haciendita', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2461', '80017', 'Lagunillas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2462', '80018', 'El Milagro', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2463', '80019', 'Las Monjas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2464', '80020', 'Nevería II', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2465', '80021', 'La Noria', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2466', '80022', 'Paniagua', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2467', '80023', 'La Peña', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2468', '80024', 'El Peral', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2469', '80026', 'Puerta del Tepozán', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2470', '80028', 'San Antonio la Galera', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2471', '80031', 'San Pedrito', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2472', '80032', 'El Sauz', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2473', '80034', 'Santa Teresa', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2474', '80035', 'Las Taponas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2475', '80036', 'San José Tepuzas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2476', '80037', 'El Vegil', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2477', '80038', 'El Zorrillo (Santa Cruz)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2478', '80040', 'Guadalupe Segundo Fracción Tres', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2479', '80041', 'El Mirador', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2480', '80042', 'El Rincón', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2481', '80044', 'Rancho la Cascada', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2482', '80045', 'El Garruñal (San Felipe de Jesús)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2483', '80049', 'El Salto de la Cantera', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2484', '80052', 'Pío XII', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2485', '80055', 'La Mesita Lagunillas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2486', '80077', 'Rancho la Mora', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2487', '80083', 'La Bomba', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2488', '80089', 'Guadalupe Segundo Fracción Primera', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2489', '80092', 'El Mogote', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2490', '80103', 'Nuevo Apapátaro', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2491', '80125', 'San Pedro Sector Norte', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2492', '80131', 'Al Noreste de las Taponas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2493', '80135', 'La Ceja', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2494', '80138', 'Los Corrales', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2495', '80142', 'Sección Norte de Lagunillas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2496', '80145', 'El Centenario', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2497', '80001', 'Huimilpan', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2498', '80003', 'El Bimbalete', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2499', '80005', 'Buenavista', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2500', '80007', 'Capula', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2501', '80009', 'Ceja de Bravo', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2502', '80016', 'La Joya', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2503', '80025', 'Piedras Lisas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2504', '80027', 'El Salitrillo', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2505', '80029', 'San Francisco (San Francisco Nevería)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2506', '80030', 'San Ignacio', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2507', '80033', 'San Pedro', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2508', '80043', 'El Salto', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2509', '80046', 'Guadalupe Segundo Fracción 2 (La Peña Colorada)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2510', '80047', 'La Presita (Palo Blanco)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2511', '80050', 'La Nueva Joya', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2512', '80051', 'La Trasquila', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2513', '80053', 'San Antonio del Puente', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2514', '80054', 'Rancho el Patol', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2515', '80057', 'Ex-hacienda San Antonio de la Galera', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2516', '80058', 'Las Lupitas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2517', '80059', 'Rancho el Milagro', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2518', '80060', 'Rancho el Salto', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2519', '80061', 'Rancho Cerro de San Pedro', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2520', '80062', 'Rancho el Cid', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2521', '80063', 'El Paliacate', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2522', '80064', 'Localidad Sin Nombre (Familia Ayala)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2523', '80065', 'Familia Durán', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2524', '80066', 'El Saltito', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2525', '80067', 'Localidad Sin Nombre (Hilario Olvera)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2526', '80068', 'La Ceja (José Luis Olvera)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2527', '80069', 'La Ceja (Zona Limítrofe)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2528', '80070', 'Granja Cruz de Mayo', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2529', '80071', 'Ranchito los Niños', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2530', '80072', 'Rancho el Lagartijero', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2531', '80073', 'Familia Borja', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2532', '80074', 'Localidad Sin Nombre (Familia Durán)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2533', '80075', 'Barrio de las Cruces', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2534', '80076', 'Finca Nuestra Señora del Carmen', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2535', '80078', 'Rancho Santa Teresa', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2536', '80079', 'Localidad Sin Nombre', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2537', '80080', 'La Tierra Blanca', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2538', '80081', 'Ex-Hacienda la Galera (La Troje)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2539', '80082', 'La Dichosa Choza', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2540', '80084', 'Las Bugambilias', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2541', '80085', 'Rancho la Rana', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2542', '80086', 'Localidad Sin Nombre (Familia González)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2543', '80087', 'Roberto de Alba [Granja]', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2544', '80088', 'Granja el Mirador', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2545', '80090', 'Avícola Gala', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2546', '80093', 'Rancho el Milagro (El Llano)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2547', '80094', 'Rancho la Soledad', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2548', '80095', 'Rancho Familia Tellez', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2549', '80096', 'San Judas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2550', '80097', 'Cañada de la Virgen', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2551', '80098', 'El Charco Prieto', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2552', '80099', 'Rancho las Delicias', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2553', '80100', 'Los Cabrera', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2554', '80101', 'Rancho el Palomar', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2555', '80102', 'La Meseta de las Abejas', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2556', '80104', 'El Mitaño', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2557', '80105', 'Rancho el Novillo', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2558', '80106', 'Rancho el Pajarito', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2559', '80107', 'Rancho El Salto', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2560', '80108', 'Rancho La Alborada', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2561', '80109', 'Rancho los Betos', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2562', '80110', 'Ninguno', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2563', '80111', 'Los Arrayanes', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2564', '80112', 'Banco de Material', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2565', '80113', 'Los Peques', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2566', '80114', 'El Cascabel (El Pozo)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2567', '80116', 'Cumbres del Cimatario', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2568', '80117', 'Campestre Bosques del Sur [Fraccionamiento]', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2569', '80118', 'Los González', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2570', '80119', 'Nevería Sector Norte (Neverías los Martínez)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2571', '80120', 'Rancho New Holland', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2572', '80121', 'Rancho los Martínez', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2573', '80122', 'Rancho los Seis Amigos', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2574', '80123', 'La Chicalota (Ganadería los Cues)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2575', '80124', 'El Riscal', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2576', '80126', 'Familia Cornejo García', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2577', '80127', 'Rancho los 3 Venados', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2578', '80128', 'Mi Pequeño Ranchito', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2579', '80129', 'La Carambada', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2580', '80130', 'Casa de Campo (Don Mario)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2581', '80132', 'La Bomba', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2582', '80133', 'La Magueyada', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2583', '80134', 'Rancho la Montaña', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2584', '80136', 'El Pequeño Ranchito del Señor de la Misericordia', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2585', '80137', 'Rancho los Cuates', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2586', '80139', 'Quinta Santiago', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2587', '80140', 'Rancho Antonio Herrera', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2588', '80141', 'Rancho el Pinturero', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2589', '80143', 'La Ladera (Presa Nueva)', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2590', '80144', 'El Zapote', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2591', '80146', 'El Cerrito', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2592', '80147', 'Madre Tierra', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2593', '80148', 'El Sueño [Granja]', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2594', '80149', 'Familia Rincón Nava', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2595', '80150', 'Finca Agua Prieta', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2596', '80151', 'La Boquilla [Pozo Uno]', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2597', '80152', 'La Terquedad', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2598', '80153', 'Los Siete Venados', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2599', '80154', 'Rancho el Alba', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2600', '80155', 'Rancho Moral', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2601', '80156', 'Rancho Santander', '8');
INSERT INTO "public"."cs_localidades" VALUES ('2602', '90018', 'Carrizal de los Durán', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2603', '90020', 'Carrizalito', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2604', '90023', 'La Ciénega', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2605', '90031', 'Fin del Llano', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2606', '90033', 'El Fraile', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2607', '90037', 'Los Jasso', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2608', '90038', 'El Refugio', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2609', '90042', 'Orilla del Plan (La Laguna)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2610', '90071', 'Río Adentro', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2611', '90085', 'Soledad del Refugio', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2612', '90146', 'El Naranjo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2613', '90177', 'El Pocito', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2614', '90208', 'Jagüey Blanco', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2615', '90209', 'Jagüey Cuate (La Cuchilla)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2616', '90212', 'Ninguno [Avícola la Presa]', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2617', '90215', 'La Cercada', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2618', '90259', 'Luis Donaldo Colosio', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2619', '90261', 'La Cruz Blanca (Movimiento Social)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2620', '90275', 'La Cebolla', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2621', '90002', 'Acatitlán del Río', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2622', '90003', 'Agua Amarga', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2623', '90004', 'Agua Enterrada', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2624', '90005', 'Agua Fría', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2625', '90007', 'La Arena', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2626', '90010', 'Arroyo de las Cañas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2627', '90012', 'Barreales', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2628', '90015', 'Capulines', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2629', '90017', 'Carrera de Tancáma', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2630', '90019', 'Carrizal de los Sánchez', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2631', '90025', 'La Cuchilla', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2632', '90026', 'El Divisadero', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2633', '90027', 'El Embocadero', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2634', '90029', 'Espadañuela', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2635', '90032', 'Las Flores', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2636', '90041', 'Laguna de Pitzquintla', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2637', '90043', 'El Limoncito', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2638', '90044', 'Limón de la Peña', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2639', '90045', 'El Lindero', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2640', '90047', 'Madroño', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2641', '90048', 'Manzanillos', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2642', '90049', 'Mesa del Sauz', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2643', '90051', 'Moctezumas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2644', '90054', 'Lomas de Juárez', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2645', '90056', 'Ojo de Agua', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2646', '90057', 'Ojo de Agua del Lindero', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2647', '90058', 'Ojo de Agua de los Mar', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2648', '90060', 'Valle Verde', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2649', '90062', 'Petzcola', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2650', '90065', 'Puerto de Ánimas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2651', '90067', 'Puerto Hondo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2652', '90069', 'Rancho Nuevo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2653', '90070', 'Rincón de Pitzquintla', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2654', '90072', 'Sabino Chico', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2655', '90073', 'Sabino Grande', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2656', '90074', 'Saldiveña', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2657', '90075', 'San Antonio Tancoyol', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2658', '90078', 'San Juan de los Durán', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2659', '90080', 'San Vicente', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2660', '90081', 'Saucillo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2661', '90084', 'Soledad de Guadalupe', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2662', '90086', 'Tancáma', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2663', '90087', 'Tancoyol', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2664', '90088', 'Tancoyolillo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2665', '90089', 'Teocho', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2666', '90090', 'Tierra Fría', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2667', '90093', 'Yerbabuena', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2668', '90094', 'El Zapote', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2669', '90095', 'Zoyapilca', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2670', '90105', 'Charco Prieto', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2671', '90108', 'El Rincón', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2672', '90116', 'La Mesa del Pino', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2673', '90120', 'El Rayo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2674', '90122', 'Los Naranjitos', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2675', '90129', 'Cuesta de Timbal', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2676', '90131', 'Puerto de Hoyos', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2677', '90132', 'El Rincón (Ojo de Agua)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2678', '90134', 'Malila', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2679', '90136', 'Puerto de San Vicente', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2680', '90139', 'El Cuisillo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2681', '90142', 'El Limón', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2682', '90145', 'El Limón', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2683', '90147', 'Guayabos', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2684', '90150', 'El Saucito', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2685', '90164', 'San Francisco', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2686', '90178', 'San Isidro', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2687', '90185', 'El Cañón', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2688', '90188', 'Jagüey Nuevo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2689', '90193', 'Mohonera de Osorio', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2690', '90194', 'Arenitas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2691', '90195', 'El Álamo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2692', '90196', 'El Pino', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2693', '90197', 'Loma Delgada', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2694', '90198', 'Los Charcos', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2695', '90200', 'Cuesta del Sabino', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2696', '90204', 'La Ceiba', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2697', '90207', 'El Tepamal', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2698', '90219', 'Jagüey (Jagüey Grande)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2699', '90224', 'Mohonera de Gudiño', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2700', '90233', 'Las Nuevas Flores', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2701', '90237', 'Loma Alta de Pitzquintla', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2702', '90239', 'Puerto del Naranjo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2703', '90264', 'Las Terrazas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2704', '90278', 'Loma Alta de Puerto Hondo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2705', '90001', 'Jalpan de Serra', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2706', '90006', 'La Alberquita', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2707', '90024', 'El Coco', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2708', '90030', 'La Esperanza', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2709', '90034', 'Guayabos Saucillo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2710', '90055', 'Puerta del Naranjo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2711', '90063', 'Piedras Anchas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2712', '90064', 'Piedras Negras', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2713', '90066', 'Puerto de Tamales (Puerto de Tancáma)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2714', '90068', 'Quirambitos', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2715', '90083', 'El Sauz (Familia Godoy Landaverde)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2716', '90091', 'El Tigre', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2717', '90097', 'El Tepozán', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2718', '90098', 'La Camarona', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2719', '90100', 'Joya del Maguey', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2720', '90102', 'Rancho el Noventa y Nueve', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2721', '90106', 'Puerto del Mezquite', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2722', '90109', 'La Joya', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2723', '90118', 'Paculilla', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2724', '90128', 'Las Lagunitas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2725', '90137', 'La Nopalera', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2726', '90140', 'El Becerro', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2727', '90152', 'Laguna del Hueso', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2728', '90155', 'La Arenita Dos', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2729', '90158', 'El Muerto', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2730', '90159', 'El Naranjo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2731', '90160', 'Ojo de Agua (San Francisco)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2732', '90165', 'San José de los Paredones', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2733', '90167', 'Tanchanaquito', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2734', '90171', 'El Aserradero', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2735', '90172', 'La Bajadita (La Mojonera)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2736', '90173', 'La Barranca', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2737', '90174', 'La Isla', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2738', '90176', 'El Lucero', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2739', '90179', 'Tanquizul', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2740', '90182', 'Los Arados', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2741', '90183', 'La Banqueta (Orilla del Plan)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2742', '90187', 'Cerritos', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2743', '90189', 'El Sabinito', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2744', '90190', 'San Martín', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2745', '90191', 'Barrio el Platanito', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2746', '90192', 'El Tigre', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2747', '90201', 'Estoque', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2748', '90202', 'Arroyo Hondo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2749', '90203', 'La Crucita', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2750', '90205', 'El Carrizo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2751', '90211', 'La Mezclita', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2752', '90213', 'La Cabaña', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2753', '90214', 'La Carrera (La Perla)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2754', '90216', 'Loma de la Guerra', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2755', '90217', 'El Depósito', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2756', '90218', 'Sección Norte de Colonia la Cruz', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2757', '90220', 'El Junco', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2758', '90221', 'El Llano', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2759', '90222', 'Maromitas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2760', '90223', 'Huerta las Misiones', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2761', '90227', 'El Pilón', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2762', '90228', 'Rancho el Callejón', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2763', '90229', 'Rancho el Carrizo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2764', '90230', 'Rancho Tres Cruces', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2765', '90232', 'Familia Martínez Sandoval', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2766', '90234', 'San Martín', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2767', '90235', 'El Conejo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2768', '90236', 'Bosques de la Sierra', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2769', '90238', 'Las Misiones', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2770', '90240', 'Rancho el Exilio', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2771', '90241', 'Localidad Sin Nombre (Familia Rojo Ramírez)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2772', '90242', 'Localidad Sin Nombre (Familia Rosales Sánchez)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2773', '90243', 'Basurero Municipal', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2774', '90244', 'La Cuchilla', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2775', '90245', 'Las Lagunas del Muerto', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2776', '90246', 'El Llano', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2777', '90247', 'El Llano', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2778', '90248', 'Rancho el Rodeo', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2779', '90249', 'El Sauz', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2780', '90251', 'La Casa Blanca (Movimiento Social)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2781', '90252', 'La Chimenea', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2782', '90253', 'Entronque a Piedras Anchas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2783', '90254', 'Familia Salinas Morales', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2784', '90255', 'Las Lagunitas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2785', '90256', 'La Pedrera', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2786', '90257', 'La Presa', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2787', '90258', 'Las Sávilas', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2788', '90260', 'Linda Vista', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2789', '90262', 'Entronque a Malila', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2790', '90263', 'Las Auroras', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2791', '90265', 'Barrio de la Santa Cruz', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2792', '90266', 'Modelo [Fraccionamiento]', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2793', '90267', 'Villas Jalpan', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2794', '90268', 'La Ceiba', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2795', '90269', 'Santa Celia', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2796', '90270', 'Santa Inés', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2797', '90271', 'El Chijol', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2798', '90272', 'El Invernadero', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2799', '90273', 'Familia Cruz (Mohonera de Gudiño)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2800', '90274', 'Familia Ibarra Alvarado', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2801', '90276', 'La Colmena', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2802', '90277', 'La Ponderosa', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2803', '90279', 'Maldonado Nuevo (La Cebolla)', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2804', '90280', 'Matlán', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2805', '90281', 'El Polvorín', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2806', '90282', 'Jardines de la Sierra', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2807', '90283', 'Los Corrales', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2808', '90284', 'Ninguno [Camino a Capulines Barrio la Presa]', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2809', '90285', 'Ninguno [Sección Oeste de Jalpan]', '9');
INSERT INTO "public"."cs_localidades" VALUES ('2810', '100005', 'La Agüita', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2811', '100031', 'Piedra Blanca', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2812', '100090', 'La Loma', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2813', '100002', 'Acatitlán de Zaragoza', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2814', '100003', 'El Aguacate', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2815', '100004', 'Agua Zarca', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2816', '100006', 'El Alambre', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2817', '100007', 'La Alberca', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2818', '100008', 'Las Ánimas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2819', '100010', 'Camarones', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2820', '100011', 'Encino Solo', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2821', '100013', 'La Florida', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2822', '100014', 'El Humo', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2823', '100015', 'Jacalilla', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2824', '100016', 'Jagüey Colorado', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2825', '100018', 'El Lobo', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2826', '100020', 'Malpaís', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2827', '100022', 'Mesa del Corozo', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2828', '100023', 'Mesa del Fortín', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2829', '100024', 'Mesa del Jagüey (Jagüey del Muerto)', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2830', '100025', 'La Mora', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2831', '100026', 'El Naranjo', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2832', '100027', 'Neblinas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2833', '100028', 'Otates', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2834', '100029', 'Cerro de la Palma', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2835', '100030', 'Palo Verde', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2836', '100032', 'Pinalito de la Cruz', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2837', '100034', 'Potrero del Llano (La Joya)', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2838', '100035', 'Puerto de Guadalupe', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2839', '100036', 'Puerto Hondo', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2840', '100037', 'La Reforma', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2841', '100039', 'Rincón de Piedra Blanca (La Pechuga)', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2842', '100041', 'El Sabinito', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2843', '100042', 'Puerto de San Agustín', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2844', '100044', 'San Juanito', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2845', '100045', 'San Onofre', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2846', '100046', 'Santa Inés', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2847', '100047', 'Tangojó', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2848', '100049', 'La Tinaja', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2849', '100050', 'Tres Lagunas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2850', '100051', 'Las Vallas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2851', '100052', 'Valle de Guadalupe', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2852', '100053', 'La Vuelta', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2853', '100054', 'La Yerbabuena', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2854', '100055', 'La Yesca', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2855', '100059', 'Los Charcos', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2856', '100060', 'Mesa de la Cruz', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2857', '100061', 'Lagunita de San Diego', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2858', '100063', 'La Margarita', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2859', '100065', 'El Carnicero', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2860', '100066', 'El Banco', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2861', '100068', 'El Aguacate (Neblinas)', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2862', '100071', 'La Silleta', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2863', '100072', 'Puerto del Sabino', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2864', '100075', 'La Campana', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2865', '100078', 'El Charco', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2866', '100080', 'El Retén', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2867', '100081', 'Tres Lagunas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2868', '100082', 'Cerro de San Agustín', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2869', '100085', 'Laguna de San Miguel', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2870', '100086', 'La Lima', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2871', '100087', 'Rancho la Cruz', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2872', '100092', 'El Capulín', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2873', '100094', 'Barrio de la Luz', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2874', '100095', 'Barrio de los Pérez', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2875', '100096', 'Barrio de Santa Teresita', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2876', '100099', 'El Encinito', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2877', '100100', 'El Gavilán', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2878', '100101', 'La Huastequita', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2879', '100104', 'Rancho Nuevo', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2880', '100106', 'El Sabino', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2881', '100107', 'La Sierrita', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2882', '100110', 'Barrio las Ortigas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2883', '100111', 'Arboledas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2884', '100112', 'San José', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2885', '100113', 'La Camelina (Los Pocitos)', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2886', '100114', 'Las Mesitas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2887', '100116', 'El Zacatal', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2888', '100119', 'La Reforma', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2889', '100127', 'Barrio Chacatlán', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2890', '100131', 'Sección Sureste de Landa de Matamoros', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2891', '100001', 'Landa de Matamoros', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2892', '100009', 'La Calera', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2893', '100017', 'La Lagunita', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2894', '100019', 'El Madroño', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2895', '100021', 'Matzacintla', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2896', '100033', 'La Polvareda', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2897', '100038', 'El Refugio', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2898', '100040', 'Río Verdito', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2899', '100048', 'Tilaco', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2900', '100058', 'Mesa del Tepame', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2901', '100064', 'Malpaisito', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2902', '100067', 'El Capulín', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2903', '100073', 'El Pemoche', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2904', '100074', 'La Joya', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2905', '100079', 'La Joya Chiquita de San Antonio', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2906', '100084', 'Las Pilas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2907', '100088', 'Agua de la Peña', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2908', '100089', 'La Espuela', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2909', '100091', 'El Gavilán', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2910', '100093', 'Buenavista', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2911', '100097', 'Crucero a Tancoyol', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2912', '100098', 'Ejido Río Tancuilín', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2913', '100102', 'El Llanito', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2914', '100103', 'Pío X', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2915', '100105', 'Laguna de las Ánimas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2916', '100108', 'Familia Balderas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2917', '100109', 'Viborillas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2918', '100117', 'La Cañada', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2919', '100118', 'La Mesa', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2920', '100120', 'Familia Acosta', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2921', '100121', 'Familia García', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2922', '100123', 'La Ceiba', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2923', '100125', 'Familia Rubio', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2924', '100126', 'Familia Sánchez Andablo', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2925', '100128', 'El Llano Chiquito', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2926', '100129', 'Puerto de Guayabitas', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2927', '100130', 'La Primera y la Última', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2928', '100132', 'Tlacuilola', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2929', '100133', 'Barrio San Esteban', '10');
INSERT INTO "public"."cs_localidades" VALUES ('2930', '110084', 'San Martín de Porres', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2931', '110132', 'Familia Rodríguez González', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2932', '110295', 'Ninguno [Banco de Arenilla la Griega]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2933', '110307', 'Familia Elías', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2934', '110349', 'Rancho Medina', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2935', '110363', 'Cumbres de Conín', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2936', '110427', 'Monte de la Calavera', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2937', '110435', 'Buenavistilla', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2938', '110436', 'Segundo Barrio de Dolores', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2939', '110452', 'La Nueva Roma', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2940', '110471', 'Las Palmitas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2941', '110476', 'Sección Noreste de Tierra Blanca', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2942', '110004', 'Alfajayucan', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2943', '110006', 'Atongo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2944', '110007', 'Santa María de los Baños', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2945', '110012', 'Rancho Guadalupe', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2946', '110016', 'Cerro Prieto', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2947', '110018', 'Rancho el Conejo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2948', '110021', 'Rancho el Coyme', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2949', '110027', 'Dolores', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2950', '110037', 'La Laborcilla', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2951', '110038', 'Las Lajitas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2952', '110039', 'El Lobo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2953', '110042', 'Matanzas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2954', '110050', 'Los Pocitos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2955', '110053', 'Presa de Rayas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2956', '110054', 'El Rodeo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2957', '110062', 'San Miguel Amazcala', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2958', '110066', 'San Rafael', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2959', '110067', 'Santa Cruz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2960', '110068', 'Santa María Begoña', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2961', '110069', 'San Vicente Ferrer', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2962', '110073', 'Tierra Blanca', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2963', '110078', 'Presa del Carmen', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2964', '110082', 'La Mariola (El Rosario)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2965', '110083', 'Santa María Ticomán', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2966', '110090', 'González Blanco', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2967', '110140', 'Casa Blanca', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2968', '110147', 'Agua Caliente', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2969', '110153', 'Barrientos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2970', '110154', 'Ejido Jesús María Uno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2971', '110155', 'Ejido Jesús María Dos (Barrientos)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2972', '110167', 'Grupo San Pedro', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2973', '110175', 'Las Palmas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2974', '110202', 'Rancho la Luz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2975', '110219', 'San Pedro Amazcala', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2976', '110221', 'San Cristobal (El Colorado)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2977', '110244', 'Loma de la Cruz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2978', '110249', 'La Laborcilla', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2979', '110252', 'Familia Esteban', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2980', '110257', 'El Monte', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2981', '110281', 'La Ladera', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2982', '110286', 'Palo Blanco', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2983', '110296', 'San Pedro Amazcala [Colonia]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2984', '110341', 'San Gabriel', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2985', '110350', 'Sección Oeste de Santa Cruz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2986', '110351', 'Sección Sur de Santa Cruz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2987', '110356', 'Sección Este de Atongo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2988', '110358', 'Sección Este de la Griega', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2989', '110362', 'La Ardilla', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2990', '110364', 'El Calero', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2991', '110366', 'El Durazno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2992', '110368', 'Familia Nieves', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2993', '110382', 'Sección Sur de San Isidro Miranda', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2994', '110383', 'Segunda Sección de Conín', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2995', '110387', 'Nueva Colonia la Campana', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2996', '110391', 'Barrio Buenavistilla', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2997', '110403', 'Familia Moreno Mercado', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2998', '110431', 'Ampliación Jesús María', '11');
INSERT INTO "public"."cs_localidades" VALUES ('2999', '110433', 'Cumbres de Conín Tercera Sección', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3000', '110434', 'San Pedro', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3001', '110447', 'El Paraíso', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3002', '110454', 'Familia Becerra', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3003', '110462', 'Familia Gómez Hernández', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3004', '110477', 'Sección Sur de Tierra Blanca', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3005', '117001', 'Ninguno [Parque Industrial la Cruz]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3006', '110001', 'La Cañada', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3007', '110002', 'Agua Azul', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3008', '110003', 'Rancho Alameda', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3009', '110005', 'Amazcala', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3010', '110010', 'Calamanda', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3011', '110013', 'El Carmen', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3012', '110015', 'Cerrito Colorado (La Curva)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3013', '110017', 'General Lázaro Cárdenas (El Colorado)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3014', '110020', 'Rancho Cotita', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3015', '110022', 'Coyotillos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3016', '110026', 'Chichimequillas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3017', '110029', 'Araceli [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3018', '110030', 'Rancho la Granja', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3019', '110031', 'Estación la Griega', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3020', '110032', 'La Griega', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3021', '110033', 'Guadalupe la Venta', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3022', '110036', 'Jesús María', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3023', '110040', 'La Loma', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3024', '110041', 'La Luz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3025', '110045', 'San José Navajas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3026', '110047', 'Palo Alto', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3027', '110048', 'El Paraíso', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3028', '110049', 'La Piedad (San Miguel Colorado)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3029', '110056', 'El Rosario', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3030', '110058', 'Saldarriaga', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3031', '110059', 'Rancho San Antonio', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3032', '110060', 'Campestre San Isidro [Fraccionamiento]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3033', '110063', 'Rancho San Miguel Barrientos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3034', '110065', 'San Pedro Zacatenco', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3035', '110075', 'Rancho el Yaqui', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3036', '110079', 'Los Leones', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3037', '110080', 'Rancho Bordo Colorado', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3038', '110081', 'La Trinidad', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3039', '110086', 'La Noria', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3040', '110087', 'Rancho Isabel', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3041', '110088', 'Rancho la Charca', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3042', '110089', 'Rancho Cantabria', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3043', '110094', 'Estación de Chichimequillas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3044', '110095', 'Rancho Santa Teresa', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3045', '110096', 'La Haciendita', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3046', '110097', 'Rancho Ex-hacienda de Chichimequillas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3047', '110098', 'Rancho el Mesón', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3048', '110099', 'Rancho el Marqués', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3049', '110100', 'Rancho el Rosario (Álvarez y Lara)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3050', '110101', 'Base Seis [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3051', '110102', 'Rancho los Arrayanes', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3052', '110103', 'Rancho el Fresno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3053', '110104', 'Familia Suárez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3054', '110106', 'Amazcala [Fraccionamiento]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3055', '110107', 'Rancho H', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3056', '110110', 'Rancho Nuevo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3057', '110111', 'Rancho los Balandra', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3058', '110112', 'La Puerta del Lobo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3059', '110114', 'Rancho los Nogales', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3060', '110115', 'San Julián [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3061', '110116', 'Unidad Blanco', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3062', '110117', 'Rancho la Curva', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3063', '110118', 'Parque Industrial El Tepeyac', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3064', '110119', 'El Convento [Establo]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3065', '110120', 'La Concha [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3066', '110121', 'Rancho el Hueso', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3067', '110122', 'La Peña Colorada', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3068', '110123', 'Compañía Agroindustrial Queretana [Industria]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3069', '110124', 'Rancho la Herencia', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3070', '110127', 'Rancho el Molino', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3071', '110129', 'Ninguno [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3072', '110130', 'Rancho el Mitlán', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3073', '110133', 'Rancho los Aguilares', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3074', '110134', 'Rancho Cicuaque', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3075', '110135', 'Rancho Agua Nueva', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3076', '110136', 'Rancho La Grieguita', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3077', '110138', 'Rancho el Cute', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3078', '110139', 'Rancho los Muros', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3079', '110141', 'El Fraile', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3080', '110142', 'Rancho el Refugio', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3081', '110143', 'Rancho Trojitas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3082', '110144', 'La Nueva Jerusalén', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3083', '110145', 'Agropecuaria Tepeyac', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3084', '110146', 'Los Arredondo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3085', '110148', 'Los Álvarez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3086', '110149', 'Rancho Arroyo Seco', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3087', '110150', 'El Artón', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3088', '110151', 'Ninguno [Autódromo]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3089', '110152', 'Aztlán Centro de Rescate Ecológico', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3090', '110156', 'Ejido Santa María Ticomán', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3091', '110157', 'El Lindero', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3092', '110158', 'Antares [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3093', '110159', 'Caro [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3094', '110160', 'La Chavela [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3095', '110161', 'Familia Hernández', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3096', '110162', 'La Merced [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3097', '110163', 'La Serpentina', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3098', '110165', 'El Marqués [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3099', '110166', 'Puma [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3100', '110168', 'Hornos de Barrientos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3101', '110169', 'El Hoyo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3102', '110170', 'Centro Nacional de Metrología', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3103', '110171', 'Lomas Hacienda Dolores', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3104', '110172', 'Martín Torres', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3105', '110173', 'La Noria de San Lorenzo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3106', '110174', 'Noria el Tecolote', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3107', '110176', 'Wamerú [Zoológico]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3108', '110177', 'Los Huizaches', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3109', '110178', 'Rancho el Pinalito (La Yerbabuena)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3110', '110179', 'Pilgrims Pride', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3111', '110180', 'El Potrero (Familia Rendón)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3112', '110181', 'El Potrero (Familia Rendón Carpintero)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3113', '110182', 'Pozo Barrientos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3114', '110183', 'Pozo Calamanda Uno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3115', '110185', 'Puerta del Palo Dulce', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3116', '110186', 'San Francisco Corrales', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3117', '110187', 'Rancho Coyotillos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3118', '110188', 'Colinas de la Piedad', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3119', '110189', 'Rancho el Árbol', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3120', '110190', 'Rancho el Cardenal', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3121', '110191', 'Rancho Cerrito Colorado', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3122', '110192', 'El Marqués [Polo Club]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3123', '110193', 'Rancho Sagrado Corazón', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3124', '110194', 'Rancho Don Clemente', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3125', '110195', 'Herminio Correa [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3126', '110196', 'Rancho el Momento', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3127', '110197', 'Hacienda Vieja', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3128', '110198', 'Rancho q Flor (El Trébol)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3129', '110199', 'Rancho el Trébol', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3130', '110200', 'Rancho la Cruz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3131', '110201', 'Rancho Loma Linda', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3132', '110203', 'Rancho la Soledad', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3133', '110204', 'Hacienda Mita', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3134', '110205', 'Rancho Las Yeguas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3135', '110206', 'Rancho los Sauces', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3136', '110207', 'Rancho Nogales', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3137', '110208', 'Rancho San Felipe', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3138', '110209', 'Rancho San Francisco', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3139', '110210', 'Rancho San José', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3140', '110211', 'Rancho San Marcos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3141', '110212', 'Rancho San Sebastián de Aparicio', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3142', '110213', 'Rancho San Vicente', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3143', '110214', 'Rancho Santa Fe', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3144', '110215', 'Rancho Santa Mónica', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3145', '110216', 'Rancho Surja', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3146', '110217', 'Alcega 2000', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3147', '110218', 'San Gabriel', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3148', '110220', 'El Bordo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3149', '110222', 'Familia Hernández Ramírez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3150', '110223', 'Terapias de Diálisis [Industria]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3151', '110224', 'Familia Hernández Trejo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3152', '110226', 'Familia Alvarado', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3153', '110227', 'El Crucero', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3154', '110228', 'Familia Pérez Granados', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3155', '110229', 'Familia Crespo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3156', '110230', 'Familia Gutiérrez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3157', '110231', 'Rancho los Tres Magueyes', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3158', '110232', 'Familia Hernández', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3159', '110233', 'Familia Lira', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3160', '110234', 'Rancho los Martínez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3161', '110235', 'Familia Martínez Uribe', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3162', '110236', 'Rancho la Joya', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3163', '110237', 'Familia Villegas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3164', '110238', 'Familia Uribe', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3165', '110239', 'Familia Pérez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3166', '110240', 'Granja Bachoco', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3167', '110241', 'Al Norte del Rancho Trojitas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3168', '110242', 'Oriente de Rancho Trojitas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3169', '110243', 'Rancho el Potrero', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3170', '110245', 'El Tamian', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3171', '110250', 'Rancho Don Chepe', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3172', '110251', 'Rancho la Palizada', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3173', '110253', 'Canteras y Recintos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3174', '110254', 'El Entronque', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3175', '110255', 'El Granero', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3176', '110256', 'El Huizache', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3177', '110258', 'El Vivero', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3178', '110259', 'Empacadora Calamanda', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3179', '110260', 'Gas Elite', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3180', '110261', 'Ninguno [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3181', '110262', 'Irma [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3182', '110263', 'El Campillo [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3183', '110264', 'Grupo Empresarial Energético', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3184', '110265', 'La Cuadra', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3185', '110266', 'Rancho el Dorado', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3186', '110267', 'Rancho la Machorra', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3187', '110268', 'Familia Pérez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3188', '110269', 'Familia Lira', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3189', '110270', 'Familia Martínez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3190', '110271', 'Familia Almaraz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3191', '110272', 'Rancho San Esteban', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3192', '110273', 'Familia Moreno Ugalde', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3193', '110274', 'Familia Martínez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3194', '110275', 'Familia Pérez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3195', '110276', 'Familia Piña', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3196', '110277', 'Familia Ramírez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3197', '110278', 'Familia Ríos Mendoza', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3198', '110279', 'Familia Robles', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3199', '110280', 'Familia Trejo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3200', '110282', 'Familia Valencia', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3201', '110283', 'Fracción Trojitas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3202', '110284', 'Familia Ugalde', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3203', '110285', 'Familia Larrondo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3204', '110287', 'Sección Sur de la Piedad', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3205', '110288', 'La Griega [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3206', '110289', 'El Rosario', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3207', '110290', 'Ninguno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3208', '110291', 'Pozo Dos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3209', '110292', 'Reyes', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3210', '110293', 'Pablo Arredondo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3211', '110294', 'Banco de Arena', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3212', '110297', 'Las Cruces', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3213', '110298', 'División del Norte', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3214', '110299', 'Familia Rangel', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3215', '110300', 'Entrada al Panteón', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3216', '110301', 'El Establito', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3217', '110302', 'Familia Solís', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3218', '110303', 'Familia Aguilar', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3219', '110304', 'Familia Bárcenas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3220', '110305', 'Familia Bárcenas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3221', '110306', 'Familia Centeno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3222', '110308', 'Familia Gómez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3223', '110309', 'Familia González', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3224', '110310', 'Familia González', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3225', '110311', 'Familia González', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3226', '110312', 'Familia Molina', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3227', '110313', 'Familia Molina', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3228', '110314', 'El Gallo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3229', '110315', 'Gasolinera', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3230', '110316', 'Base Ocho [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3231', '110317', 'Cerro Prieto [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3232', '110318', 'Ex-rancho de Dolores', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3233', '110319', 'Paulina [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3234', '110320', 'Juliana Hernández Cata [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3235', '110321', 'Sangremal [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3236', '110322', 'San Isidro [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3237', '110323', 'La Granja', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3238', '110324', 'El Piecito', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3239', '110325', 'Los Pinos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3240', '110326', 'Rancho Dolores', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3241', '110327', 'Rancho San Elías', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3242', '110328', 'Rancho San Rafael', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3243', '110329', 'Rancho Santa Lucía', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3244', '110330', 'Rancho Santa Marina', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3245', '110331', 'El Sabinito', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3246', '110332', 'Reca', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3247', '110333', 'Ninguno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3248', '110334', 'Ninguno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3249', '110335', 'Ninguno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3250', '110336', 'Familia Paulín', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3251', '110337', 'Rancho San Isidro', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3252', '110338', 'La Soledad', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3253', '110339', 'Vecinos del Rodeo (La Curvita)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3254', '110340', 'Zona Limítrofe de Amazcala', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3255', '110342', 'Ejido el Coyme', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3256', '110343', 'Familia Bárcenas Mérida', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3257', '110344', 'Familia Frayle', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3258', '110345', 'Familia Hernández', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3259', '110346', 'Familia Mendoza', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3260', '110347', 'Familia Nieves Reséndiz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3261', '110348', 'Familia Ochoa', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3262', '110352', 'Villas de San José', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3263', '110353', 'Ninguno [Ejido San Rafael la Purísima]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3264', '110354', 'Sección Norte de Atongo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3265', '110355', 'Sección Oeste de Atongo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3266', '110357', 'La Griega Sección Norte', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3267', '110359', 'La GriegaSección Sur', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3268', '110360', 'Ninguno [Ejido Atongo B]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3269', '110361', 'Sección Oeste de la Griega', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3270', '110365', 'El Pozo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3271', '110367', 'Familia Fernández', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3272', '110369', 'Fertiplus [Industria]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3273', '110370', 'El Huizache', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3274', '110371', 'Jardines de la Esperanza', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3275', '110372', 'Nuevo Torreón (Almudejar)', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3276', '110373', 'El Campanario', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3277', '110374', 'Rancho Aldea Conejos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3278', '110375', 'Rancho el Faro', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3279', '110376', 'Rancho la Barranca', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3280', '110377', 'Rancho las Bugambilias', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3281', '110378', 'Rancho los Duraznitos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3282', '110379', 'El Rosario', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3283', '110380', 'San Isidro Miranda', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3284', '110381', 'Sección Norte de San Isidro Miranda', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3285', '110384', 'Ninguno [Trituradora Ejido la Machorra]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3286', '110385', 'Ninguno [Trituradora el Rosario Uno]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3287', '110386', 'Ampliación del Paraíso', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3288', '110388', 'El Cerillo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3289', '110389', 'Cuarta Generación A', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3290', '110390', 'Don Mayo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3291', '110392', 'Familia Barrón', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3292', '110393', 'Familia Cruz Lucio', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3293', '110394', 'Familia Genovés', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3294', '110395', 'Familia González Pichardo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3295', '110396', 'Familia Guerrero Ruiz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3296', '110397', 'Familia Hernández de Jesús', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3297', '110398', 'Familia Herrera Valdez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3298', '110399', 'Familia López Zepeda', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3299', '110400', 'Familia Mandujano Herrera', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3300', '110401', 'Familia Martínez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3301', '110402', 'Familia Mata Verde', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3302', '110404', 'Familia Olvera Lira', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3303', '110405', 'Familia Ortíz Mendoza', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3304', '110406', 'Familia Rivera Pérez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3305', '110407', 'Ninguno', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3306', '110408', 'Familia Ugalde Pérez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3307', '110409', 'Familia Vázquez Almaraz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3308', '110410', 'Familia Valencia Hurtado', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3309', '110411', 'La Finca de los Robles', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3310', '110412', 'La Paz [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3311', '110413', 'El Potrero', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3312', '110414', 'CONAFOR', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3313', '110415', 'Familia García Arredondo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3314', '110416', 'San Francisco', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3315', '110417', 'Zona Oriente la Griega', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3316', '110418', 'Zona Oriente de Amazcala', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3317', '110419', 'El Crucero', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3318', '110420', 'Ninguno [Bloquera Amazcala Ladrillera]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3319', '110421', 'Familia Briones', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3320', '110422', 'Familia Flores', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3321', '110423', 'Familia Mata', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3322', '110424', 'Familia Robledo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3323', '110425', 'Finca Mejía', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3324', '110426', 'Los Gavilanes', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3325', '110428', 'Pozo Chichimequillas Número 1', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3326', '110429', 'Rancho el Sabinito', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3327', '110430', 'Hacienda Calamanda', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3328', '110432', 'Rinconada del Pedregal', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3329', '110437', 'Familia Rivera de Ruiz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3330', '110438', 'El Mirador', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3331', '110439', 'La Pradera', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3332', '110440', 'Fraccionamiento del Parque Residencial', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3333', '110441', 'Los Heróes', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3334', '110442', 'Paseos del Marqués', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3335', '110443', 'Hacienda la Cruz [Fraccionamiento]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3336', '110444', 'Santa Fe Libertadores', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3337', '110445', 'Los Encinos [Ganadería]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3338', '110446', 'Rancho Abandonado', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3339', '110448', 'Los Pinos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3340', '110449', 'Ejido San Vicente', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3341', '110450', 'Zibatá [Fraccionamiento]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3342', '110451', 'Familia Ramírez Alcántar', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3343', '110453', 'Familia Alcántara', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3344', '110455', 'Familia Bonilla', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3345', '110456', 'Familia Campos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3346', '110457', 'Familia Elías', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3347', '110458', 'Familia Ibarra', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3348', '110459', 'Familia Lugo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3349', '110460', 'Los Francos', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3350', '110461', 'Familia Martínez Flores', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3351', '110463', 'Familia Munguía', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3352', '110464', 'Familia Ramírez', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3353', '110465', 'Familia Real Camargo', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3354', '110466', 'Familia Rendón', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3355', '110467', 'Familia Rubio de la Cruz', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3356', '110468', 'Familia Salazar Villanueva', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3357', '110469', 'Ninguno [Forrajera la Cruz]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3358', '110470', 'La Quinta', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3359', '110472', 'Rancho Cerrito Colorado Segunda Fracción', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3360', '110473', 'Rancho la Herencia', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3361', '110474', 'Rancho las Avestruces', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3362', '110475', 'Rancho San Francisco', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3363', '110478', 'Sección Suroeste de Tierra Blanca', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3364', '110479', 'Real Paraíso [Fraccionamiento]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3365', '110480', 'Zen [Fraccionamiento]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3366', '110481', 'La Meseta de las Abejas', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3367', '110482', 'Ninguno [Parque Industrial la Cruz]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3368', '110483', 'Ninguno [Parque Industrial Bernardo Quintana]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3369', '110484', 'Ninguno (FINSA) [Fraccionamiento Industrial del Norte]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3370', '110485', 'Ninguno [Parque Industrial la Noria]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3371', '110486', 'Ninguno [Parque Industrial Aeropuerto]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3372', '110487', 'Cortijo San Fermín', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3373', '110488', 'Delia [Granja]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3374', '110489', 'Ejido la Laborcilla', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3375', '110490', 'El Airoso', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3376', '110491', 'Familia Arana', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3377', '110492', 'Familia Morales Hernández', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3378', '110493', 'Finca Alni', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3379', '110494', 'Finca el Devizadero', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3380', '110495', 'La Casa Blanca', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3381', '110496', 'La Nueva Esperanza', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3382', '110497', 'Ninguno [Al Este de San Isidro Miranda]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3383', '117003', 'Ninguno [Parque Industrial Bernardo Quintana]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3384', '117015', 'Ninguno (FINSA) [Fraccionamiento Industrial del Norte]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3385', '117016', 'Ninguno [Parque Industrial la Noria]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3386', '117019', 'Ninguno [Parque Industrial Aeropuerto]', '11');
INSERT INTO "public"."cs_localidades" VALUES ('3387', '120146', 'Loma Bonita', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3388', '120155', '20 de Enero', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3389', '120008', 'La Ceja', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3390', '120010', 'Chintepec', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3391', '120011', 'La D', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3392', '120012', 'Dolores de Ajuchitlancito', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3393', '120019', 'La Purísima', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3394', '120020', 'La Lira', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3395', '120022', 'Noria Nueva', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3396', '120024', 'Las Postas', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3397', '120028', 'San Clemente', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3398', '120078', 'Ejido la Palma', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3399', '120079', 'Pozo Uno', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3400', '120084', 'Sección Norte de San Clemente', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3401', '120095', 'Francisco Villa', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3402', '120099', 'Vicente Guerrero', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3403', '120106', 'Moctezuma', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3404', '120137', 'Villas de Escobedo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3405', '120139', 'Sección Noroeste del Sauz', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3406', '120140', 'Sección Sureste del Chamizal', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3407', '120141', 'El Chamizal', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3408', '120142', 'Sección Suroeste del Sauz', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3409', '120143', 'Sección Este de Epigmenio González', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3410', '120148', 'Sección Este de Pedro Escobedo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3411', '120151', 'La Media Luna [Colonia]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3412', '120152', 'La Quinta Diana', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3413', '120162', 'Familia Rivera González (Hacienda San Clemente)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3414', '120163', 'La Lira [Fraccionamiento]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3415', '120165', 'Valle Dorado', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3416', '120177', 'La Cantera [Fraccionamiento]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3417', '120181', 'La Presa', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3418', '120182', 'La Araña de la Venta (El Crucero)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3419', '120185', 'Sección Oeste de Epigmenio González', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3420', '120001', 'Pedro Escobedo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3421', '120002', 'Epigmenio González (El Ahorcado)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3422', '120003', 'Ajuchitlancito', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3423', '120004', 'Los Álvarez', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3424', '120005', 'Rancho la Asturiana', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3425', '120006', 'Los Benitos (El Revolcadero)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3426', '120009', 'Rancho las Coronelas', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3427', '120013', 'Escolásticas', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3428', '120014', 'La Escondida', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3429', '120017', 'Guadalupe Septién', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3430', '120018', 'Ignacio Pérez (El Muerto)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3431', '120021', 'Rancho Montecristo (La Cotera)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3432', '120023', 'La Palma', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3433', '120025', 'Quintanares', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3434', '120026', 'San Antonio la D', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3435', '120027', 'San Cirilo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3436', '120029', 'San Fandila', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3437', '120030', 'Rancho San Roque', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3438', '120031', 'El Sauz (Sauz Alto, Sauz Bajo)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3439', '120033', 'El Tesoro del Campo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3440', '120034', 'La Venta de Ajuchitlancito', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3441', '120039', 'Rancho el Diamante', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3442', '120040', 'El Chaparral', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3443', '120041', 'Rancho San Guillermo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3444', '120042', 'Rancho Agua Caliente', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3445', '120043', 'Estación el Ahorcado', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3446', '120044', 'Narconón México [Industria]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3447', '120045', 'Rancho el Venado', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3448', '120046', 'Rancho el Profeta', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3449', '120047', 'Rancho Casa Blanca', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3450', '120050', 'Rancho las Hormigas', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3451', '120051', 'Rancho los Landeros (El Nopal)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3452', '120052', 'Rancho la Teresita', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3453', '120053', 'Maya Destetes Porcino', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3454', '120054', 'Ninguno [Lechera Calamanda]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3455', '120055', 'Ninguno [Instituto]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3456', '120056', 'Rancho San Fandila', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3457', '120057', 'Rancho las Gladiolas', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3458', '120058', 'El Mezquital', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3459', '120059', 'Lomasea [Granja]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3460', '120060', 'Rancho la Alberca', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3461', '120061', 'La Cantera', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3462', '120062', 'Potrero Grande', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3463', '120063', 'Bordo de la Campana', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3464', '120064', 'FIDEQRO', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3465', '120065', 'Central Termoeléctrica el Sauz', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3466', '120066', 'Hacienda Huertas Viejas', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3467', '120067', 'Rancho los Olivares', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3468', '120068', 'OVO ADRO [Granja]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3469', '120069', 'Rancho el Gavillero', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3470', '120070', 'Rancho la Laguna', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3471', '120071', 'Ninguno [Agroindustrias Siglo Veintiuno]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3472', '120073', 'Rancho del Carmen', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3473', '120074', 'Rancho Bordo Chico', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3474', '120075', 'Bordo Don Blas', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3475', '120076', 'La Casita', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3476', '120077', 'Ejido Ajuchitlancito', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3477', '120080', 'Ex-hacienda San Clemente (Fracción Tercera)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3478', '120081', 'Bachoco Planta 3770 [Granja]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3479', '120082', 'Ejido Quintanares', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3480', '120083', 'El Chamizal', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3481', '120085', 'Parada los Terraza', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3482', '120087', 'El Pozo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3483', '120088', 'Rancho la Mora (La Joya)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3484', '120090', 'Familia Pacheco', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3485', '120092', 'El Chivatito', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3486', '120093', 'Montecarlo [Club Hípico]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3487', '120094', 'Ejido Santiago Atepetlac', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3488', '120096', 'Liratongo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3489', '120097', 'Rancho Chico el Carmen', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3490', '120098', 'Familia Botello Uribe', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3491', '120100', 'El Rodeo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3492', '120101', 'Familia Botello', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3493', '120102', 'Las Torres', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3494', '120103', 'Hacienda Calamanda', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3495', '120104', 'Base la Curva', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3496', '120105', 'El Sauz', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3497', '120107', 'Familia García', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3498', '120108', 'San Agustín', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3499', '120109', 'Familia Guevara', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3500', '120110', 'Localidad Sin Nombre (Familia Juárez)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3501', '120111', 'Familia Nieves', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3502', '120112', 'Familia Piña', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3503', '120113', 'Familia Solís', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3504', '120114', 'El Pozo de Don Fidencio', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3505', '120115', 'Finca Jomai', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3506', '120117', 'La Nauyaca [Granja]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3507', '120118', 'Hacienda San Clemente', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3508', '120119', 'La Loma', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3509', '120120', 'Mega Rancho (Viñedos Hidalgo)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3510', '120121', 'El Original', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3511', '120122', 'La Pinturita', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3512', '120123', 'El Puente', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3513', '120124', 'ALPECSA', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3514', '120125', 'Rancho la Escondida', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3515', '120126', 'Rancho Noria Nueva', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3516', '120127', 'Sauz Alto', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3517', '120128', 'Localidad Sin Nombre', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3518', '120129', 'Cerrito Blanco', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3519', '120130', 'La Colmena', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3520', '120131', 'Familia Gómez', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3521', '120132', 'El Horno', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3522', '120133', 'Loma Bonita', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3523', '120135', 'Pozo Cuatro Emiliano Zapata', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3524', '120136', 'Rancho la Alondra', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3525', '120138', 'Sección Este de San Clemente', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3526', '120144', 'Epigmenio González Sección Norte', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3527', '120145', 'La Bolsa', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3528', '120147', 'La Pradera [Granja]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3529', '120149', 'Rancho el Carmen', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3530', '120150', 'Familia Araujo Sánchez', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3531', '120153', 'Sección Noroeste de la D', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3532', '120154', 'Rancho Alejandro Mendoza', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3533', '120156', 'Ampliación Chamizal', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3534', '120157', 'Presidentes', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3535', '120158', 'Sección Suroeste Pedro Escobedo', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3536', '120159', 'Ampliación Sanfandila', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3537', '120160', 'Familia Eligio Morales', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3538', '120161', 'Familia Herrera Martínez', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3539', '120164', 'Pozo No. 2 de los 11', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3540', '120166', 'Ampliación Ajuchitlancito', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3541', '120167', 'El Encanto II [Fraccionamiento]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3542', '120168', 'Rancho Buenaventura', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3543', '120169', 'Familia Álvarez Moreno', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3544', '120170', 'Familia Araujo Mejía', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3545', '120171', 'Familia Araujo Ramírez', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3546', '120172', 'Familia Barroso Barrios', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3547', '120173', 'Familia Mata Gómez', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3548', '120174', 'Familia Moreno Quintanar', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3549', '120175', 'Familia Romero Hernández', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3550', '120176', 'Familia Valencia de la Cruz', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3551', '120178', 'Real de San Pedro [Fraccionamiento]', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3552', '120179', 'Valle Anáhuac', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3553', '120180', 'Las Peñitas', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3554', '120183', 'Sección Este de la Venta', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3555', '120184', 'Las Peñitas (La Venta)', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3556', '120186', 'Sección Este de Epigmenio González', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3557', '120187', 'La Santa Cruz', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3558', '120188', 'Ejido San Clemente', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3559', '120189', 'Bicentenario', '12');
INSERT INTO "public"."cs_localidades" VALUES ('3560', '130037', 'El Moral', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3561', '130064', 'La Zancona', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3562', '130082', 'La Nopalera', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3563', '130085', 'Puerto Blanco', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3564', '130092', 'El Tepozán', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3565', '130098', 'El Saucito II', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3566', '130099', 'Corral Viejo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3567', '130100', 'Loma Blanca', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3568', '130106', 'Puerto del Ojo de Agua', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3569', '130113', 'La Escondida', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3570', '130122', 'La Laja', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3571', '130160', 'Las Flores', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3572', '130166', 'El Cantoncito', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3573', '130002', 'Adjuntas de Higueras', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3574', '130003', 'Adjuntas de los Guillen', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3575', '130004', 'Agua Caliente', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3576', '130006', 'Agua de Pedro', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3577', '130007', 'Agua Fría', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3578', '130008', 'El Alamito', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3579', '130009', 'El Álamo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3580', '130011', 'Alto Bonito', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3581', '130012', 'Aposentos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3582', '130013', 'El Arte', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3583', '130014', 'Boquillas', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3584', '130015', 'Camargo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3585', '130016', 'El Carrizal', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3586', '130017', 'El Carrizalillo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3587', '130018', 'Cerro Colorado', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3588', '130019', 'Puerto del Cobre', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3589', '130020', 'La Colonia I', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3590', '130021', 'El Comedero', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3591', '130022', 'Cruz del Milagro', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3592', '130023', 'Cuesta Colorada', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3593', '130024', 'Cuesta de los Ibarra', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3594', '130025', 'Los Encinos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3595', '130026', 'Las Enramadas', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3596', '130027', 'El Frontoncillo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3597', '130028', 'El Garambullal', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3598', '130029', 'El Guamúchil', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3599', '130030', 'La Higuera', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3600', '130032', 'El Lindero', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3601', '130033', 'Los Llanos de Buenavista', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3602', '130034', 'Maguey Verde', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3603', '130035', 'Las Mesas', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3604', '130036', 'Molinitos (El Molino)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3605', '130038', 'Morenos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3606', '130039', 'El Nogalito', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3607', '130041', 'La Ordeña', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3608', '130043', 'Peña Blanca', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3609', '130044', 'El Pilón', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3610', '130045', 'El Pitahayo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3611', '130046', 'La Plazuela', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3612', '130047', 'El Portugués', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3613', '130048', 'Puerto del Aire', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3614', '130050', 'Río Blanco', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3615', '130051', 'San Isidro (San Isidro Boquillas)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3616', '130053', 'San Lorenzo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3617', '130055', 'El Saucillo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3618', '130056', 'El Saucito I', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3619', '130057', 'El Sauz', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3620', '130059', 'El Tequezquite', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3621', '130060', 'El Torbellino', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3622', '130061', 'La Vega (Las Vegas)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3623', '130062', 'La Yerbabuena', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3624', '130065', 'El Zapote II', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3625', '130066', 'Piedra Gorda', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3626', '130068', 'La Tecozautla', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3627', '130073', 'Mesa del Troje', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3628', '130074', 'La Colonia II', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3629', '130075', 'La Reforma', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3630', '130079', 'La Tinaja', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3631', '130083', 'El Motoshí (La Paz)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3632', '130084', 'La Paz', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3633', '130089', 'El Zapote II', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3634', '130091', 'El Ojo de Agua', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3635', '130095', 'La Colonia Tres', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3636', '130096', 'Puerto de Santa María', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3637', '130097', 'Los Cerritos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3638', '130101', 'Puerto Amarillo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3639', '130102', 'El Aguacatito', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3640', '130104', 'Panales', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3641', '130105', 'Mentiras', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3642', '130109', 'Los Pleitos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3643', '130112', 'La Estacada (Cruz del Milagro)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3644', '130114', 'Puerto Rico', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3645', '130115', 'El Salado', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3646', '130119', 'Piedra Grande', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3647', '130120', 'El Campamento', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3648', '130121', 'El Puerto de la Guitarra', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3649', '130123', 'El Atorón', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3650', '130125', 'El Paraíso', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3651', '130126', 'Miranda', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3652', '130127', 'El Higuerón', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3653', '130128', 'El Magueyal (Cuesta de los Ibarra)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3654', '130129', 'El Alamito II', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3655', '130133', 'Pueblo Nuevo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3656', '130140', 'La Curva', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3657', '130141', 'El Encino', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3658', '130142', 'La Era', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3659', '130144', 'Loma Bonita', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3660', '130145', 'Milpillas', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3661', '130146', 'Los Morales', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3662', '130147', 'El Pajarito', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3663', '130149', 'Puerto del Ahorcado', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3664', '130152', 'Vista Alegre', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3665', '130167', 'Ampliación Colonia San Marcos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3666', '130168', 'Villas de Guadalupe', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3667', '130001', 'Peñamiller', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3668', '130010', 'Los Álamos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3669', '130040', 'Los Ocotes', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3670', '130052', 'San Juanico', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3671', '130054', 'San Miguel Palmas (Misión de Palmas)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3672', '130058', 'Sebastianes', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3673', '130063', 'Villa Emiliano Zapata (Extoraz)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3674', '130067', 'El Aguaje', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3675', '130069', 'El Tepozán', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3676', '130071', 'El Plan', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3677', '130072', 'Canoas', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3678', '130076', 'Milpa Blanca', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3679', '130077', 'Puerto de Ramos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3680', '130078', 'La Ciénega', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3681', '130088', 'La Estación', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3682', '130090', 'Las Mesitas', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3683', '130093', 'Los Orozco', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3684', '130094', 'El Sauco', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3685', '130103', 'El Carricillo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3686', '130108', 'El Cantoncito', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3687', '130110', 'La Liga', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3688', '130111', 'Salinas', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3689', '130116', 'La Pindonga', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3690', '130130', 'La Cuisha', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3691', '130131', 'Las Camelinas', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3692', '130132', 'La Resendeña', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3693', '130134', 'Santa Ana', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3694', '130135', 'Cuesta del Vino', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3695', '130136', 'Agua de Ángel', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3696', '130137', 'La Alcaparosa', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3697', '130138', 'El Ardillero', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3698', '130139', 'El Cajón', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3699', '130143', 'La Estancia', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3700', '130148', 'El Piloncillo (El Puente)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3701', '130150', 'El Pozo (Familia Aguilar)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3702', '130151', 'Solidaridad', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3703', '130153', 'El Durazno', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3704', '130154', 'Cruz de Orozco', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3705', '130155', 'Arroyo de los Muertos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3706', '130156', 'La Ladera', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3707', '130157', 'El Limón', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3708', '130158', 'La Mesa del Zoilo', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3709', '130159', 'Las Águilas', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3710', '130161', 'Los Tajos', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3711', '130162', 'Ampliación Barrio del Sótano', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3712', '130163', 'El Amargoso', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3713', '130164', 'La Concentradora (Milpa Larga)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3714', '130165', 'Los Cuartos (El Salado)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3715', '130169', 'El Eucalipto (Familia Botello)', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3716', '130170', 'La Ermita', '13');
INSERT INTO "public"."cs_localidades" VALUES ('3717', '140261', 'Rancho la Chata (Palo Bobar)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3718', '140332', 'Fracción Ejido el Retablo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3719', '140343', 'Los Hornos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3720', '140350', 'Prados de Miranda', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3721', '140385', 'Familia Ortíz Hernández', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3722', '140434', 'San José el Alto Zona II', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3723', '140517', 'La Ladera (Pedro Nicolás)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3724', '140518', 'La Joyita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3725', '140520', 'Anexo Colonia la Cruz', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3726', '140031', 'La Barreta', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3727', '140035', 'Casa Blanca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3728', '140040', 'Cerro Colorado (El Colorado)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3729', '140041', 'Charape la Joya', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3730', '140042', 'Charape de los Pelones', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3731', '140046', 'La Estacada', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3732', '140047', 'Estancia de la Rochera', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3733', '140048', 'La Estancia de Palo Dulce', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3734', '140051', 'La Gotera', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3735', '140052', 'El Herrero', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3736', '140055', 'La Joya', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3737', '140062', 'La Luz', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3738', '140063', 'Llano de la Rochera', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3739', '140072', 'La Palma', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3740', '140073', 'Palo Alto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3741', '140074', 'El Patol', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3742', '140075', 'Pie de Gallo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3743', '140076', 'El Pie', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3744', '140080', 'Presa de Becerra', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3745', '140081', 'Presita de San Antonio', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3746', '140084', 'El Puertecito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3747', '140085', 'Puerto de Aguirre', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3748', '140096', 'San Isidro el Alto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3749', '140098', 'San Isidro el Viejo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3750', '140099', 'San José Buenavista', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3751', '140103', 'San Miguelito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3752', '140110', 'La Solana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3753', '140111', 'La Tinaja de la Estancia', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3754', '140113', 'Tlacote el Alto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3755', '140115', 'El Tránsito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3756', '140117', 'La Versolilla', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3757', '140135', 'La Puerta de Santiaguillo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3758', '140143', 'San Pedrito el Alto (El Obraje)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3759', '140158', 'Cerro Prieto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3760', '140180', 'El Rincón de Ojo de Agua', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3761', '140194', 'Rancho Menchaca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3762', '140211', 'El Potrero', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3763', '140213', 'El Zapote', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3764', '140214', 'La Cantera', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3765', '140229', 'Gobernantes', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3766', '140254', 'El Huizachal de las Tetillas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3767', '140255', 'Prados del Rincón', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3768', '140274', 'Ejido Jofrito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3769', '140275', 'Ampliación Ejido Modelo Dos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3770', '140278', 'Familia Gutiérrez Yañez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3771', '140281', 'Nuevo Horizonte', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3772', '140292', 'Campo Real', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3773', '140302', 'Anáhuac', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3774', '140304', 'Santa Catarina [Asociación de Colonos]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3775', '140307', 'Bosques de la Hacienda', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3776', '140308', 'Patria Nueva', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3777', '140315', 'Diana Laura (Rinconada)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3778', '140318', 'Las Águilas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3779', '140322', 'Santa Juanita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3780', '140323', 'Sergio Villaseñor', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3781', '140324', 'Tenochtitlan', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3782', '140331', 'Ejido San Pablo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3783', '140344', 'Jardines de Jurica', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3784', '140348', 'Bosques de Buenavista', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3785', '140357', 'Rancho la Curva', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3786', '140366', 'Ampliación Sur del Salitre', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3787', '140367', 'San José de los Perales', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3788', '140370', 'Chicago [Colonia]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3789', '140383', 'Bordo de San Carlos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3790', '140388', 'Familia Torres', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3791', '140392', 'Familia Bautista', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3792', '140394', 'La Nueva Palma', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3793', '140398', 'Las Camelinas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3794', '140399', 'Arroyo de los Tepetates', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3795', '140400', 'Colinas de Santa Rosa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3796', '140401', 'Arboledas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3797', '140404', 'San Francisco', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3798', '140406', 'Ampliación Piano', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3799', '140410', 'Los Jiménez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3800', '140421', 'Anexo Ejido Bolaños', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3801', '140426', 'Valle de San Pedro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3802', '140433', 'Primero de Junio', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3803', '140435', 'San José el Alto Zona III', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3804', '140436', 'San José el Alto Zona IV', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3805', '140437', 'Pedregal de San José', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3806', '140440', 'Jardines de San José 3ra. Sección', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3807', '140442', 'San José el Alto Zona X', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3808', '140448', 'Sección Sureste de la Solana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3809', '140450', 'Desarrollo San Ángel', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3810', '140451', 'Sección Oeste del Ejido Santa María', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3811', '140458', 'Jardines de San José 1ra. y 2da. Sección', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3812', '140459', 'Bosques de Iturbide', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3813', '140460', 'La Ciénega', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3814', '140461', 'El Pedregal', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3815', '140466', 'Rinconada las Joyas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3816', '140467', 'Vista Hermosa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3817', '140470', 'Sección Noroeste de la Solana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3818', '140478', 'El Tepeyac', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3819', '140479', 'Linda Vista', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3820', '140481', 'El Bordo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3821', '140486', 'Sección Sureste de Santa Rosa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3822', '140487', 'Familia Luna Jiménez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3823', '140488', 'Sección Oeste de Santa Rosa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3824', '140498', 'Rancho de los Guardados', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3825', '140504', 'Sección Sur de San Miguelito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3826', '140505', 'El Huizachal (El Calero)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3827', '140515', 'Lomas de Menchaca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3828', '140521', 'Jardines de Azucenas (La Floresta)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3829', '140532', 'Sección Este de San Miguelito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3830', '140534', 'Bosques de San Pablo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3831', '140536', 'Lomas del Mirador', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3832', '140537', 'Proyección 2000', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3833', '140541', 'Mártires de la Libertad I', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3834', '140542', 'Mártires de la Libertad II', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3835', '140546', 'Sección Sur de San José el Alto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3836', '140548', 'Vistas de San Pablo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3837', '140550', 'Sección Noroeste de la Gotera', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3838', '140551', 'Sección Oeste de Montenegro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3839', '140554', 'Sección Sureste de San Miguelito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3840', '140001', 'Santiago de Querétaro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3841', '140030', 'Acequia Blanca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3842', '140032', 'Buenavista', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3843', '140034', 'La Carbonera', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3844', '140037', 'Cerro de la Cruz', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3845', '140039', 'Sonterra [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3846', '140044', 'El Durazno', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3847', '140050', 'San Francisco de la Palma', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3848', '140053', 'Jofre', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3849', '140054', 'Jofrito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3850', '140058', 'Juriquilla', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3851', '140059', 'Loma del Chino', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3852', '140064', 'Mompaní', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3853', '140066', 'Provincia Juriquilla', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3854', '140068', 'La Monja', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3855', '140069', 'Montenegro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3856', '140070', 'El Nabo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3857', '140071', 'Ojo de Agua', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3858', '140077', 'Pintillo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3859', '140078', 'Pinto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3860', '140079', 'El Pozo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3861', '140082', 'Corea', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3862', '140086', 'La Purísima', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3863', '140087', 'Rancho Largo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3864', '140088', 'El Refugio', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3865', '140090', 'Rancho el Rincón', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3866', '140091', 'El Rosario', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3867', '140092', 'El Salitre', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3868', '140095', 'San Isidro Buenavista', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3869', '140097', 'San Isidro Miranda', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3870', '140101', 'San José el Alto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3871', '140104', 'Ex-hacienda San Pedrito el Alto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3872', '140105', 'San Pedro Mártir', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3873', '140106', 'Santa Catarina', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3874', '140107', 'Santa María Magdalena', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3875', '140108', 'Santa Rosa Jáuregui', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3876', '140109', 'Santo Niño de Praga', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3877', '140114', 'Tlacote el Bajo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3878', '140118', 'Santa María del Zapote', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3879', '140122', 'Rancho Cerrito Colorado', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3880', '140132', 'Nuevo Torreón (Almudejar)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3881', '140136', 'La Purísima', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3882', '140169', 'Pitita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3883', '140173', 'Piletas (El Derramadero)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3884', '140176', 'Huertas la Joya [Fraccionamiento Campestre]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3885', '140177', 'San Antonio de Trojes', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3886', '140181', 'Los Arquitos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3887', '140182', 'La Esperanza', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3888', '140183', 'La Azteca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3889', '140184', 'Rancho las Flores', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3890', '140185', 'Puerta Verona [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3891', '140187', 'Zapote el Alto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3892', '140188', 'Rancho la Pared', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3893', '140190', 'Rancho la Esperanza', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3894', '140192', 'Colonia Doce de Diciembre (Alberto Juárez Blancas)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3895', '140193', 'Rancho las Tres Fuentes', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3896', '140195', 'Ejido Bolaños', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3897', '140196', 'Paseos del Pedregal', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3898', '140197', 'La Hacienda (El Ranchito)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3899', '140198', 'La Jeca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3900', '140199', 'Raquet Club [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3901', '140200', 'La Dinamita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3902', '140201', 'La Cócona', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3903', '140202', 'Ninguno', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3904', '140203', 'Satélite [Granja]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3905', '140204', 'Avícola Don José', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3906', '140207', 'Santa Isabel', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3907', '140209', 'Ojo de Agua del Sauz', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3908', '140210', 'Estación Repetidora TV Azteca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3909', '140212', 'Rancho de Guadalupe (La Lágrima)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3910', '140215', 'Cañada de la Monja', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3911', '140218', 'Parcela 4 Ejido Modelo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3912', '140220', 'Envasadora Tlacote', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3913', '140221', 'Rancho la Aldea', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3914', '140222', 'Colinas de Santa Cruz Primera Sección', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3915', '140223', 'Rancho las Bugambilias', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3916', '140224', 'El Canelo [Granja]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3917', '140225', 'Capricho', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3918', '140226', 'Cochinos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3919', '140228', 'Cuitláhuac', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3920', '140231', 'Las Mariposas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3921', '140232', 'Comercializadores Eucamex', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3922', '140233', 'Conservas las Granjas (La Champiñonera)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3923', '140234', 'Rancho Mompaní', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3924', '140236', 'Colinas de Santa Cruz Segunda Sección', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3925', '140237', 'El Tintero Dos (Las Margaritas)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3926', '140238', 'El Higo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3927', '140239', 'Rancho Santa Mónica', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3928', '140241', 'Desarrollo Montenegro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3929', '140242', 'Granja las Grullas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3930', '140243', 'El Establo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3931', '140244', 'Instituto Rosa del Carmelo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3932', '140245', 'El Huizache', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3933', '140246', 'Jardines de la Esperanza', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3934', '140247', 'La Mesa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3935', '140248', 'El Milagro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3936', '140249', 'Fertiplus', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3937', '140251', 'La Rica [Fraccionamiento Campestre Ecológico]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3938', '140252', 'Ninguno [CERESO]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3939', '140253', 'Las Pomas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3940', '140256', 'Quintín Lozada', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3941', '140257', 'Rancho los Panchos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3942', '140258', 'Rancho Dos Arroyos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3943', '140259', 'Rancho el Milagro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3944', '140260', 'Rancho el Raspiño', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3945', '140262', 'Rancho la Chinita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3946', '140264', 'Rancho los Arcos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3947', '140265', 'Rancho los Canes', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3948', '140266', 'Rancho los Duraznitos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3949', '140267', 'La Esperanza', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3950', '140268', 'Los Venados', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3951', '140271', 'El Refugio', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3952', '140272', 'Salinas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3953', '140273', 'San Francisco el Alto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3954', '140276', 'Familia Cárdenas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3955', '140277', 'Familia Nieves', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3956', '140279', 'La Casita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3957', '140280', 'Stereo Cristal Ciento Uno', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3958', '140282', 'Tías', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3959', '140283', 'Tico', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3960', '140284', 'Rancho la Toya', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3961', '140285', 'Trituradora Ejido la Machorra', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3962', '140286', 'Trituradora el Rosario Uno', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3963', '140287', 'Grupo Bin', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3964', '140288', 'UVM Campus Querétaro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3965', '140290', 'Agregados y Derivados del Centro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3966', '140291', 'Los Laureles (Quinta la Sexta)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3967', '140293', 'Colonia Che Guevara', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3968', '140295', 'Campi', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3969', '140296', 'Granja Cerrito Colorado', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3970', '140297', 'Luchis [Granja]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3971', '140298', 'Los Muertos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3972', '140299', 'Rancho el Cortijo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3973', '140300', 'Zona Entrada a Tlacote', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3974', '140301', 'Familia Ortíz', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3975', '140303', 'Anexo a la Colonia Renacimiento', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3976', '140305', 'Bomba del Agua (Familia Olvera)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3977', '140306', 'Ninguno [Bombas y Maniobras la Frontera]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3978', '140309', 'El Calero', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3979', '140310', 'El Camino', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3980', '140311', 'El Cerro del Borrego', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3981', '140312', 'Colinas del Sur', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3982', '140313', 'Los Viñedos [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3983', '140314', 'Alborada', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3984', '140316', 'Los Pirules', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3985', '140317', 'Lomas del Mirador', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3986', '140319', 'Colonia los Bordos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3987', '140320', 'Plan Nuevo de Santa María', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3988', '140321', 'San Sebastián', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3989', '140325', 'Tonatiú', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3990', '140326', 'Constructora Martínez Ogando', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3991', '140327', 'Las Cruces Juriquilla', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3992', '140328', 'Ejido de Montenegro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3993', '140329', 'Ejido el Retablo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3994', '140330', 'La Huerta', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3995', '140333', 'Fraccionamiento Cuesta Bonita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3996', '140334', 'Fraccionamiento Lomas del Cimatario', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3997', '140335', 'Casa Blanca [Granja]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3998', '140336', 'El Rosario [Granja]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('3999', '140337', 'La Cuadra [Granja]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4000', '140338', 'Granja Palpan', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4001', '140339', 'Rancho el Nido', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4002', '140340', 'El Granjenal (El Salitre)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4003', '140341', 'Grupo Deyca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4004', '140342', 'Héroes de la Libertad', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4005', '140345', 'Jardines del Sol', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4006', '140346', 'Milenio Tres', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4007', '140347', 'Nuevo Horizonte de Querétaro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4008', '140349', 'Pozo de Ojo de Agua', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4009', '140351', 'Rancho Aldea Conejos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4010', '140352', 'Rancho el Acicate', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4011', '140353', 'Rancho el Canutillo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4012', '140354', 'Rancho el Faro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4013', '140355', 'Rancho el Risco', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4014', '140356', 'Rancho la Barranca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4015', '140358', 'Rancho la Solana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4016', '140359', 'Rancho los Pirules', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4017', '140360', 'Rancho Ojo de Agua', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4018', '140361', 'Rancho Quinta Diana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4019', '140363', 'Rancho Trojes de San Antonio', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4020', '140364', 'Real de Juriquilla', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4021', '140365', 'El Romerillal', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4022', '140368', 'Sector Gobernantes', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4023', '140369', 'Segunda Sección de Conín', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4024', '140371', 'Ejido Casa Blanca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4025', '140372', 'Altos del Salitre', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4026', '140373', 'Familia Pérez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4027', '140374', 'Familia Cruz', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4028', '140375', 'Familia García', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4029', '140376', 'Familia Hernández', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4030', '140377', 'Familia Reyna', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4031', '140378', 'El Ranchito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4032', '140379', 'Familia Cortés', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4033', '140380', 'Familia Fernández', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4034', '140381', 'Terrazas del Mirador [Colonia]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4035', '140384', 'Familia Martínez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4036', '140386', 'Familia Rico Ordóñez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4037', '140387', 'Sección Suroeste de Santa Rosa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4038', '140389', 'Familia Velázquez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4039', '140390', 'Familia Zarazúa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4040', '140391', 'El Bordo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4041', '140393', 'Familia Palacios', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4042', '140395', 'La Guadalupana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4043', '140396', 'El Tecolote', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4044', '140397', 'Palpa [Granja]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4045', '140402', 'Zona Este la Cruz', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4046', '140403', 'Colonia Rosa Reyna', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4047', '140405', 'Santa Rosa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4048', '140407', 'Familia Hernández', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4049', '140408', 'Hacienda la Monja', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4050', '140409', 'El Huerto de la Torna', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4051', '140411', 'Familia Elisea Jiménez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4052', '140412', 'Familia Araujo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4053', '140413', 'Nuevo Juriquilla', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4054', '140414', 'Familia Reyna Olvera', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4055', '140415', 'Rancho el Cerrito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4056', '140416', 'Rancho la Curva', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4057', '140417', 'Tierras Duras (Potrero de San Antonio)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4058', '140418', 'Anexo a Cerrito Colorado', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4059', '140419', 'Anexo Menchaca Tres', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4060', '140420', 'Balcones del Acueducto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4061', '140422', 'Establo Sin Nombre', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4062', '140423', 'Familia Barrios', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4063', '140424', 'Localidad Sin Nombre (Familia Reyes)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4064', '140425', 'Fraccionamiento Arboledas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4065', '140427', 'Leyes de Reforma', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4066', '140428', 'Pedregal de Querétaro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4067', '140429', 'Rancho del Río', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4068', '140430', 'La Solana Sección Norte', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4069', '140431', 'San Isidro Miranda Sección Norte', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4070', '140432', 'San Isidro Miranda Sección Sur', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4071', '140438', 'San José el Alto Zona VI', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4072', '140439', 'San José el Alto Zona VII', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4073', '140441', 'San José el Alto Zona IX', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4074', '140443', 'San José el Alto Zona XI', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4075', '140444', 'San José el Alto Zona XII', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4076', '140445', 'San José el Alto Zona XII', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4077', '140446', 'La Solana Sección Poniente', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4078', '140447', 'La Solana Sección Oriente', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4079', '140449', 'La Solana Sección Suroeste', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4080', '140452', 'El Cerrito', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4081', '140453', 'Privada Independencia', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4082', '140454', 'Fraccionamiento las Palmas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4083', '140455', 'Misión de Concá [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4084', '140456', 'Villas Fontana [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4085', '140457', 'Hacienda la Gloria', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4086', '140462', 'La Mesita de Abajo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4087', '140463', 'La Mesita de Mompaní', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4088', '140464', 'La Nopalera', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4089', '140465', 'Las Campanas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4090', '140468', 'San José el Alto Zona Sureste', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4091', '140469', 'Sección Oeste de Santa María', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4092', '140471', 'Zona Oeste Loma Bonita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4093', '140472', 'Sección Este de la Solana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4094', '140473', 'Sección Este de Montenegro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4095', '140474', 'Zona Sur Colonia Italiana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4096', '140475', 'Villas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4097', '140476', 'Ampliación Geoplazas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4098', '140477', 'Casa Blanca', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4099', '140480', 'Cumbres de Conín Tercera Sección', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4100', '140482', 'El Pirul', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4101', '140483', 'El Plan', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4102', '140484', 'Entrada Arroyo la Chinita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4103', '140485', 'Sección Este de Santa Rosa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4104', '140489', 'Los Olvera', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4105', '140490', 'Kilómetro 2.6 a Pie de Gallo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4106', '140491', 'Familia Velázquez Jiménez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4107', '140492', 'Familia Luna Bárcenas', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4108', '140493', 'Privada de los Portones [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4109', '140494', 'La Ardilla', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4110', '140495', 'La Torna', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4111', '140496', 'Los Cuates', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4112', '140497', 'Quinta los Corrales', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4113', '140499', 'Rancho el Álamo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4114', '140500', 'Rancho el Mesón', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4115', '140501', 'Rancho Escondido', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4116', '140502', 'Familia Ortíz Sánchez', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4117', '140503', 'Zona este Milenio III', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4118', '140506', 'Rancho Bellavista [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4119', '140507', 'El Refugio [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4120', '140508', 'Provincia Santa Elena', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4121', '140509', 'Punta San Carlos [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4122', '140510', 'Los Huertos [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4123', '140511', 'Ninguno [Residencial VIVEICA]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4124', '140512', 'Loma Real de Querétaro [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4125', '140513', 'Puerta del Sol [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4126', '140514', 'Rancho la Tinaja', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4127', '140516', 'Familia Salazar Aguilar', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4128', '140519', 'La Crucita', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4129', '140522', 'Acueducto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4130', '140523', 'El Arcángel [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4131', '140524', 'Rancho San Pedro [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4132', '140525', 'Hacienda Santa Rosa [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4133', '140526', 'Monte Real', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4134', '140527', 'San Jerónimo [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4135', '140528', 'Ninguno', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4136', '140529', 'Kilómetro 2 a la Solana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4137', '140530', 'La Milpa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4138', '140531', 'Sección Este de Colonia Santa Rosa', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4139', '140533', 'Al Oriente de Terminal de Autobuses', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4140', '140535', 'Colinas de San Pablo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4141', '140538', 'Fray Junípero Serra [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4142', '140539', 'Villa Romana [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4143', '140540', 'Orión [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4144', '140543', 'Mártires de la Libertad III', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4145', '140544', 'Parque la Gloria', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4146', '140545', 'Prados del Cimatario', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4147', '140547', 'Sección Suroeste de San José el Alto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4148', '140549', 'Sección Este de la Luz', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4149', '140552', 'San Juan [Granja]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4150', '140553', 'Sección Sureste de Montenegro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4151', '140555', 'Sección Suroeste de Buenavista', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4152', '140556', 'El Puerto', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4153', '140557', 'San Pedro [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4154', '140558', 'Urbilla del Real [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4155', '140559', 'Arboledas Residencial [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4156', '140560', 'Valle de Santiago [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4157', '140561', 'Puertas de San Miguel [Fraccionamiento]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4158', '140562', 'Nuevo México', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4159', '140563', 'Barreiro', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4160', '140564', 'Cuadra Don Peri', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4161', '140565', 'Desarrollo Buenavista', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4162', '140566', 'El Bordo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4163', '140567', 'El Capricho', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4164', '140568', 'El Mogote', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4165', '140569', 'El Pino', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4166', '140570', 'La Granja de Mariana', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4167', '140571', 'La Laguna', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4168', '140572', 'La Pradera de Buenavista', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4169', '140573', 'Las Naranjo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4170', '140574', 'Las Troneras', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4171', '140575', 'Los Dos Pinos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4172', '140576', 'Rancho Naranjo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4173', '140577', 'Rancho No', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4174', '140578', 'Tierras Duras', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4175', '140579', 'Tres Amigos', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4176', '140580', 'Ninguno [Sección Norte de San Miguelito]', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4177', '140581', 'Juriquilla Santa Fe', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4178', '140582', 'Puerta Navarra', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4179', '140583', 'La Arboleda Juriquilla', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4180', '140584', 'Sección Este de Pie de Gallo', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4181', '140585', 'Grand Juriquilla', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4182', '140586', 'Parcela 180 (Privada Laureles)', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4183', '140587', 'Palmas de San José', '14');
INSERT INTO "public"."cs_localidades" VALUES ('4184', '150012', 'Medias Coloradas', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4185', '150047', 'Llanos de Santa Clara', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4186', '150049', 'La Majada', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4187', '150051', 'San Agustín', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4188', '150055', 'Santa Teresa', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4189', '150064', 'Santa Elena', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4190', '150066', 'Zarza y Somerial', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4191', '150078', 'Culebras', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4192', '150080', 'El Mezquite', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4193', '150083', 'Las Tinajas', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4194', '150097', 'Ocotitlán', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4195', '150098', 'Puerto del Durazno', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4196', '150099', 'San Bartolo', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4197', '150002', 'Agua de Venado', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4198', '150003', 'Apartadero', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4199', '150004', 'Los Azóguez', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4200', '150005', 'Canoas (Nuevo San Joaquín)', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4201', '150006', 'Santa Ana', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4202', '150007', 'El Deconí', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4203', '150008', 'Los Hernández', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4204', '150009', 'Los Herrera', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4205', '150011', 'Maravillas', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4206', '150016', 'Los Planes', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4207', '150017', 'El Plátano', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4208', '150018', 'Los Pozos', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4209', '150020', 'Puerto Hondo', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4210', '150021', 'San José Quitasueño', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4211', '150022', 'San Antonio', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4212', '150023', 'San Cristóbal', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4213', '150024', 'San Francisco Gatos', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4214', '150027', 'San Juan Tetla', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4215', '150028', 'San Rafael', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4216', '150032', 'Tierras Coloradas', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4217', '150042', 'El Durazno', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4218', '150046', 'La Soledad', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4219', '150048', 'Las Lomas de San Cristobal', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4220', '150050', 'Los Naranjos', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4221', '150053', 'San Sebastián', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4222', '150056', 'El Mesho', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4223', '150057', 'La Maravilla', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4224', '150060', 'Las Escaleras', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4225', '150062', 'La Guadalupana', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4226', '150063', 'Santa María Álamos', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4227', '150065', 'San José Carrizal', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4228', '150082', 'El Sombrerete', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4229', '150089', 'La Frontera', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4230', '150090', 'Puerto de la Garita', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4231', '150091', 'Puerto de Rosarito', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4232', '150096', 'La Mina Prieta', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4233', '150001', 'San Joaquín', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4234', '150014', 'Las Ovejas', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4235', '150019', 'La Puerta de Guadalupe (Rancho San Nicolás)', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4236', '150033', 'Trincheras', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4237', '150034', 'El Yonthé', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4238', '150035', 'La Zauda', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4239', '150036', 'La Yerbabuena', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4240', '150040', 'La Orduña', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4241', '150052', 'Puerto de Ramírez', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4242', '150054', 'Tierra Colorada', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4243', '150058', 'Mineral de Santo Entierro', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4244', '150059', 'San Pascual', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4245', '150061', 'Palo Grande', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4246', '150073', 'La Cuesta Prieta', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4247', '150074', 'Mesa de San Isidro', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4248', '150076', 'Barrio del Camposanto', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4249', '150077', 'El Bordo', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4250', '150079', 'Las Peritas', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4251', '150081', 'El Planecito', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4252', '150085', 'El Tejocote (Catiteo)', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4253', '150086', 'La Catarina', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4254', '150087', 'Familia González Guerrero (Campo Alegre)', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4255', '150088', 'La Era', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4256', '150092', 'Rancho las Estacas', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4257', '150093', 'El Shesnisal', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4258', '150094', 'Barrio de la Loma', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4259', '150095', 'Campamento Leonardo Rodríguez Alcaine', '15');
INSERT INTO "public"."cs_localidades" VALUES ('4260', '160234', 'El Arenal del Refugio', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4261', '160278', 'Villas de Aragón', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4262', '160318', 'San José', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4263', '160338', 'La Joyita', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4264', '160366', 'Doroteo Arango', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4265', '160377', 'El Presidio', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4266', '160002', 'Arcila', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4267', '160004', 'Barranca de Cocheros', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4268', '160005', 'Buenavista', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4269', '160013', 'El Coto', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4270', '160015', 'El Chaparro', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4271', '160022', 'Estancia de Santa Lucía', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4272', '160023', 'La Estancita', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4273', '160025', 'El Granjeno', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4274', '160029', 'La Laborcilla', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4275', '160031', 'Laguna de Vaquerías', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4276', '160034', 'La Magdalena', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4277', '160035', 'La Mesa de San Antonio', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4278', '160036', 'El Mirador', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4279', '160044', 'Perales', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4280', '160045', 'San Pedro Potrerillos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4281', '160047', 'Puerta de Alegrías (Puerto de Alegrías)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4282', '160051', 'Salto de Vaquerías', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4283', '160052', 'San Antonio Zatlauco', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4284', '160053', 'San Francisco', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4285', '160066', 'Santa Rosa Xajay', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4286', '160068', 'Soledad del Río', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4287', '160069', 'Tuna Mansa', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4288', '160071', 'Vaquerías', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4289', '160081', 'La Corregidora', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4290', '160090', 'Nuevo San Germán', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4291', '160097', 'San Sebastian Loma Linda', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4292', '160099', 'Santa Isabel el Coto', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4293', '160101', 'La Noria', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4294', '160111', 'San Pablo Potrerillos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4295', '160112', 'La Mina', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4296', '160113', 'Rosa de Castilla', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4297', '160119', 'Neria de Dolores Godoy', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4298', '160120', 'La Mora', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4299', '160145', 'Santa Isabel', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4300', '160148', 'La Divina Providencia', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4301', '160150', 'Ejido San Pedro Ahuacatlán', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4302', '160151', 'Ejido San Sebastián de las Barrancas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4303', '160152', 'Ejido Santa Bárbara de la Cueva', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4304', '160161', 'San Francisco', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4305', '160162', 'Los Llanitos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4306', '160172', 'La Peñita Rodada', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4307', '160174', 'El Once', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4308', '160179', 'Rancho el Colorado', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4309', '160185', 'Cofradía Grande', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4310', '160188', 'El Rincón de Santa Rita', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4311', '160191', 'El Once (Familia Trejo Carbajal)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4312', '160197', 'Ejido el Carrizo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4313', '160198', 'Puerta del Sol', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4314', '160211', 'Tercera Sección del Rodeo (El Invernadero)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4315', '160215', 'La Venta', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4316', '160216', 'El Paraíso', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4317', '160220', 'Rancho San Jesusito', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4318', '160229', 'Familia Monroy', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4319', '160237', 'Barrio de la Cruz', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4320', '160248', 'Villas de San José', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4321', '160259', '5ta. Sección del Rodeo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4322', '160261', 'La Noria', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4323', '160262', 'Ojo de Agua de Vaquerías', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4324', '160266', 'Prados de Cerro Gordo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4325', '160267', 'Pueblo Quieto', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4326', '160272', 'San Rafael', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4327', '160273', 'El Rocío', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4328', '160282', 'Familia Paulín Chávez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4329', '160294', 'Cazadero Colonia Santa Anita (Familia Ramírez)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4330', '160297', 'La Caseta (Ampliación Santa Bárbara)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4331', '160312', 'Villas del Sol', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4332', '160314', 'Ejido la Estancia', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4333', '160321', 'Sección Oeste de Galindo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4334', '160322', 'Rancho la Estancia', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4335', '160325', 'La Ladera', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4336', '160327', 'El Cerrito de León', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4337', '160330', 'Insurgentes', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4338', '160332', 'Revolución', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4339', '160337', 'La Guadalupana', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4340', '160340', 'Los Frailes', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4341', '160342', 'Prados de San Juan Bautista', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4342', '160343', 'San Martín', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4343', '160344', 'Ampliación la Loma', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4344', '160355', 'San Antonio la Labor', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4345', '160367', 'Ejido la Llave', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4346', '160373', 'La Guadalupana del Campo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4347', '160378', 'Sección Norte de Cerro Gordo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4348', '160001', 'San Juan del Río', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4349', '160007', 'Casa Blanca', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4350', '160008', 'El Cazadero', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4351', '160009', 'Cuarto Centenario', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4352', '160010', 'Los Cerritos San Miguel', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4353', '160011', 'Cerro Gordo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4354', '160016', 'Dolores Cuadrilla de Enmedio', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4355', '160017', 'Dolores Godoy', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4356', '160020', 'Estancia de Bordos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4357', '160024', 'Galindo (San José Galindo)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4358', '160026', 'Casita San José', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4359', '160028', 'El Jazmín', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4360', '160030', 'Laguna de Lourdes', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4361', '160033', 'La Llave', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4362', '160037', 'San Javier', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4363', '160038', 'Ojo de Agua', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4364', '160039', 'El Organal', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4365', '160040', 'Palma de Romero', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4366', '160041', 'Palmillas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4367', '160042', 'Senegal de las Palomas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4368', '160043', 'Paso de Mata', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4369', '160046', 'Potrero Nuevo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4370', '160048', 'Puerta de Palmillas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4371', '160049', 'El Rosario', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4372', '160050', 'Sabino Chico', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4373', '160054', 'San Germán', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4374', '160056', 'San Miguel Galindo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4375', '160058', 'San Sebastián de las Barrancas Norte', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4376', '160059', 'Santa Bárbara de la Cueva', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4377', '160060', 'Santa Cruz Escandón', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4378', '160063', 'Santa Lucía', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4379', '160064', 'Santa Matilde', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4380', '160065', 'Santa Rita', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4381', '160067', 'El Sitio', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4382', '160070', 'La Valla', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4383', '160072', 'Visthá', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4384', '160073', 'San Antonio de los Pirules', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4385', '160075', 'Rancho Alegre', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4386', '160076', 'Rancho de Enmedio', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4387', '160077', 'El Rodeo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4388', '160080', 'San Miguel Arcángel', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4389', '160083', 'San Sebastián de las Barrancas Sur', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4390', '160087', 'Buenavista Palma de Romero', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4391', '160091', 'Ninguno [Granja]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4392', '160093', 'Loma Linda', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4393', '160094', 'La Estancia', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4394', '160096', 'La Espíndola', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4395', '160098', 'Rancho la Paz', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4396', '160100', 'Rancho el Siete', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4397', '160102', 'Santa Teresa', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4398', '160103', 'Familia Reséndiz Zárraga', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4399', '160104', 'San Gil', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4400', '160105', 'José Reyes Olivar', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4401', '160106', 'Rancho San Germán', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4402', '160107', 'Sin Nombre (Familia Manzano)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4403', '160108', 'Rancho el Refugio', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4404', '160109', 'Rancho el Rojo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4405', '160114', 'Rancho la Tinaja', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4406', '160115', 'El Potosino', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4407', '160117', 'Doxocua', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4408', '160118', 'Rancho de Mata', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4409', '160125', 'Vista Hermosa (Cuasinada)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4410', '160127', 'La Quinta Miseria (Rancho los García)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4411', '160128', 'La Lagunita', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4412', '160129', 'Los Ordaz', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4413', '160130', 'Ninguno [Al Este de Santa Matilde]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4414', '160131', 'Ejido León Grande', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4415', '160132', 'Ninguno [Al Este de San Germán]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4416', '160133', 'Rancho el Séptimo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4417', '160134', 'San José de la Venta', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4418', '160135', 'Hermanos Jiménez [Granja]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4419', '160137', 'Centro de Readaptación Social', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4420', '160138', 'Rancho San Gerónimo (Rancho San Román)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4421', '160139', 'Huerta Arellano', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4422', '160140', 'Barrio San Francisco (Las Torres)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4423', '160141', 'Ninguno [Canteras Rey-Val]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4424', '160142', 'Colonia José María Morelos (2da. Sección del Rodeo)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4425', '160143', 'Josefa Ortíz (4ta. Sección del Rodeo)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4426', '160144', 'Lázaro Cárdenas (1ra. Sección del Rodeo)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4427', '160146', 'Barrio la Concepción', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4428', '160147', 'Familia Pérez Sinecio', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4429', '160149', 'El Carrizo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4430', '160153', 'Rancho Cuatro Vientos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4431', '160154', 'Ex-hacienda Santa Cruz', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4432', '160155', 'La Venta del Refugio [Ganadería]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4433', '160156', 'La Flor [Granja]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4434', '160157', 'La Soledad [Granja]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4435', '160158', 'Hacienda San Jacinto', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4436', '160159', 'Hacienda Santa Rosa Xajay', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4437', '160160', 'El Parián', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4438', '160163', 'Familia Pérez (La Magdalena)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4439', '160164', 'La Pera (Loma del Chereque)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4440', '160165', 'Hotel Misión San Gil', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4441', '160166', 'Familia Nieto', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4442', '160167', 'Rancho el Montesito', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4443', '160168', 'Natural de Alimentos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4444', '160169', 'La Once', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4445', '160170', 'San Pedro', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4446', '160171', 'Huertas el Parián', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4447', '160173', 'El Porvenir [Residencial]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4448', '160175', 'El Ranchito', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4449', '160176', 'Rancho Valvaneda (El Arenal)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4450', '160177', 'Rancho Baratza', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4451', '160178', 'Rancho Banthí (Familia Navarrete)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4452', '160180', 'Ninguno [Agropecuaria Santa Rita]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4453', '160181', 'Rancho la Espiga de Oro', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4454', '160182', 'Milpa la Joya', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4455', '160183', 'La Luna', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4456', '160184', 'Rancho los Mejía', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4457', '160186', 'Rancho San José', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4458', '160187', 'Rancho Tucajay', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4459', '160189', 'San Antonio de los Encinos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4460', '160190', 'Santa Fe', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4461', '160192', 'Al Sur del Llano', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4462', '160193', 'Buenos Aires', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4463', '160195', 'Localidad Sin Nombre (Caballerizas)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4464', '160199', 'Ejido Visthá', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4465', '160200', 'Sin Nombre (Enrique Sinecio)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4466', '160201', 'Localidad Sin Nombre (Familia Arteaga)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4467', '160202', 'El Congo (El Cerrito)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4468', '160203', 'La Loma de los Pinos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4469', '160204', 'Prolongación los Mejía', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4470', '160205', 'San Antonio [Granja]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4471', '160206', 'Familia Flores Gómez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4472', '160207', 'Familia Uribe', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4473', '160208', 'Al Oeste del Pozo Número 12', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4474', '160209', 'El Llano', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4475', '160212', 'Tilas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4476', '160213', 'El Tinacal', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4477', '160214', 'Ninguno [Unión de Artesanos la Corregidora]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4478', '160217', 'Colonia Aquiles Serdán', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4479', '160218', 'La Doble M [Lienzo Charro]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4480', '160219', 'Pueblo Nuevo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4481', '160221', 'Rancho los Tres Juanes', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4482', '160222', 'Camino a Santa Matilde (Familia Aguilar)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4483', '160223', 'Sin Nombre (Familia Cervantes)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4484', '160224', 'Canal Santa Clara', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4485', '160225', 'Al Sur de Montecasinos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4486', '160227', 'Los Arbolitos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4487', '160228', 'Espíritu Santo (Las Gallinas)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4488', '160230', 'Familia Morales', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4489', '160231', 'Los Pinos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4490', '160232', 'La Escondida', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4491', '160233', 'Arcos del Sol', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4492', '160235', 'TRANSENER [Taller de mantenimiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4493', '160236', 'Ninguno [Transportes BJM]áá', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4494', '160238', 'Barrio Espíritu Santo (Familia Quintanar Tovar)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4495', '160239', 'Sección Oeste del Barrio Espíritu Santo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4496', '160240', 'Ninguno [Granja]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4497', '160241', 'San José', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4498', '160242', 'México [Centro Escolar]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4499', '160243', 'Chinches Bravas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4500', '160244', 'Colonia Francisco Villa', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4501', '160245', 'La Nueva Esperanza', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4502', '160246', 'Colonia Los Girasoles Segunda Sección', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4503', '160247', 'Colonia Santa Fe', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4504', '160249', 'Ejido Cerro Gordo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4505', '160250', 'Familia Baltazar', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4506', '160251', 'Familia Ramírez García', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4507', '160252', 'Norte Nuevo Espíritu Santo [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4508', '160253', 'Fraccionamiento Bosques de Banthí', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4509', '160254', 'Fraccionamiento La Peña de San Juan', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4510', '160255', 'Los Fresnos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4511', '160256', 'La Muralla [Ganadería]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4512', '160257', 'La Mora [Granja]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4513', '160258', 'Ninguno', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4514', '160260', 'La Mina', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4515', '160263', 'Paseos de Xhosdá', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4516', '160264', 'Los Pirules', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4517', '160265', 'Pozo 21', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4518', '160268', 'Ejido las Palomas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4519', '160269', 'Rancho la Cortina', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4520', '160270', 'Rancho los Pinos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4521', '160271', 'Rancho los Mejía Cardozo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4522', '160274', 'San Antonio la Manga', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4523', '160275', 'Familia Juárez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4524', '160276', 'Ninguno', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4525', '160277', 'Casa Blanca', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4526', '160279', 'Familia Anaya Pérez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4527', '160280', 'Familia Bárcenas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4528', '160281', 'Familia Martínez Sabino', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4529', '160283', 'Familia Rodríguez Mejía', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4530', '160284', 'Vista Real', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4531', '160285', 'Familia Ugalde Pizaña', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4532', '160286', 'La Nueva Esperanza', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4533', '160287', 'San José Palma de Romero', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4534', '160288', 'Al Sureste de San Pedro Ahuacatlán', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4535', '160289', 'Familia Santiago', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4536', '160290', 'La Tinajita (El Tabacal)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4537', '160291', 'Familia Vázquez López', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4538', '160292', 'Ampliación la Llave', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4539', '160293', 'La Caseta', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4540', '160295', 'Ninguno [CFE]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4541', '160296', 'Chapoteadero San Bernardo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4542', '160298', 'Santa Elena', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4543', '160299', 'Ejido San Isidro 2da. Fracción', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4544', '160300', 'Familia Bayza', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4545', '160301', 'Familia López (Doxocua)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4546', '160302', 'Familia Reséndiz (Doxocua)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4547', '160303', 'La Ladrillera (Ejido la Llave)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4548', '160304', 'San Pedro [Parador Turístico]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4549', '160305', 'Los Polvorines', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4550', '160306', 'Puerta de Palmillas (Familia Castelán)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4551', '160307', 'Rancho el Barranco', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4552', '160308', 'Rancho San Benito Cazador', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4553', '160309', 'Santa Elena', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4554', '160310', 'Sin Nombre (Zona Limítrofe)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4555', '160311', 'Santa Anita', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4556', '160313', 'Ejido El Carmen', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4557', '160315', 'Localidad Sin Nombre (Familia Arteaga Gudiño)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4558', '160316', 'Lomas de la Estancia', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4559', '160317', 'Rancho el Capulín', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4560', '160319', 'Galindo Sección Noreste', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4561', '160320', 'Galindo Sección Oriente', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4562', '160323', 'Las Garcitas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4563', '160324', 'Junto al Pozo 6', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4564', '160326', 'Casa de Piedra', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4565', '160328', 'Las Palapas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4566', '160329', 'El Pedrero', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4567', '160331', 'Colonia Querétaro', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4568', '160333', 'Familia Baena Pérez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4569', '160334', 'Familia Nieto García', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4570', '160335', 'Familia Reséndiz Mendoza', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4571', '160336', 'Familia Trejo Nieto', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4572', '160339', 'La Paz Sección Oriente', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4573', '160341', 'Los Fresnos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4574', '160345', 'Ampliación Loma Linda', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4575', '160346', 'El Pirul', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4576', '160347', 'Ex-Hacienda Banthí', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4577', '160348', 'Familia Cruz Pájaro', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4578', '160349', 'Familia Mancilla Álvarez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4579', '160350', 'Familia Sánchez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4580', '160351', 'Familia Trejo Arellano', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4581', '160352', 'El Carmen [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4582', '160353', 'Los García', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4583', '160354', 'Valle Dorado 2 [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4584', '160356', 'Rancho El Rocío', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4585', '160357', 'Sección Suroeste la Fuente', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4586', '160358', 'El Encanto I [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4587', '160359', 'El Sabino [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4588', '160360', 'Haciendas San Juan [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4589', '160361', 'Jardines de Visthá [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4590', '160362', 'Villas Fundadores [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4591', '160363', 'Ampliación San Francisco', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4592', '160364', 'Banthí el Alto', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4593', '160365', 'Camino Real a Ojo de Agua Número 145', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4594', '160368', 'Familia Díaz Zetina', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4595', '160369', 'Familia Hurtado Vázquez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4596', '160370', 'Familia Álvarez Monroy', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4597', '160371', 'Las Torres [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4598', '160372', 'Jardines de Visthá 3ra. Sección', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4599', '160374', 'La Lagunita', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4600', '160375', 'Las Peñitas (Familia Zuñiga Hurtado)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4601', '160376', 'Lindavista', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4602', '160379', 'Loma Alta [Fraccionamiento]', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4603', '160380', 'Ampliación Nuevo San Isidro Primera Sección', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4604', '160381', 'Ampliación Nuevo San Isidro Segunda Sección', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4605', '160382', 'Colonia Puertas del Cielo (Las Loberas)', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4606', '160383', 'Ejido Santa Matilde', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4607', '160384', 'Emiliano Zapata', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4608', '160385', 'Familia Álvarez Martínez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4609', '160386', 'Familia Bárcenas Pérez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4610', '160387', 'Familia Mejía Martínez', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4611', '160388', 'Familia Zárraga Monroy', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4612', '160389', 'Guadalupe del Rincón', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4613', '160390', 'La Peña', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4614', '160391', 'La Pradera', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4615', '160392', 'Los Desmontes', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4616', '160393', 'Los Pocitos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4617', '160394', 'Nuevo Loma Linda', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4618', '160395', 'Privada Cipreses', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4619', '160396', 'Rancho Cuervo', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4620', '160397', 'Rancho la Arboleda', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4621', '160398', 'Rancho Puertas del Sol', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4622', '160399', 'Rancho San Pedro', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4623', '160400', 'Sección Noroeste del Coto', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4624', '160401', 'Sección Sureste de Potrerillos', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4625', '160402', 'Sección Suroeste de Santa Lucía', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4626', '160403', 'Sección Suroeste de Senegal de las Palomas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4627', '160404', 'Villa de las Flores', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4628', '160405', 'Ampliación Guadalupe de las Peñas', '16');
INSERT INTO "public"."cs_localidades" VALUES ('4629', '170004', 'El Carrizal', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4630', '170042', 'El Sabino', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4631', '170090', 'Rancho largo', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4632', '170111', 'Presa Centenario (La Palapa)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4633', '170180', 'Ninguno [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4634', '170188', 'El Cerrito de las Lozas', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4635', '170189', 'El Colorado', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4636', '170196', 'Sección Sur de la Ermita', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4637', '170005', 'El Cerrito', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4638', '170006', 'Los Cerritos', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4639', '170007', 'La Fuente', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4640', '170014', 'San José de la Laja', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4641', '170019', 'El Tejocote', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4642', '170032', 'Estación Bernal', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4643', '170038', 'Rancho Loma Colorada', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4644', '170054', 'El Jade', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4645', '170058', 'Cerrito San José (La Troje)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4646', '170084', 'Pozo Número Uno (Ejido el Sauz)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4647', '170094', 'Rancho Mancañal', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4648', '170101', 'Ranchito San José', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4649', '170135', 'Sección Este de la Fuente', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4650', '170144', 'Los Arquitos', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4651', '170146', 'Familia Jiménez (Colonia Santa Fe)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4652', '170152', 'Rancho Largo (La Vía)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4653', '170158', 'Familia González Vega', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4654', '170160', 'La Vega', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4655', '170172', 'Las Corraletas (Familia Castillo)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4656', '170178', 'El Bordo', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4657', '170181', 'La Lagunita', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4658', '170185', 'Sección Sur de Tequisquiapan', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4659', '170190', 'El Magueyal', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4660', '170201', 'Ampliación el Paraíso', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4661', '170202', 'Ampliación Santa Fe (La Ermita)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4662', '170206', 'Los Lora', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4663', '170207', 'Piedras Negras y San Antonio', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4664', '170001', 'Tequisquiapan', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4665', '170002', 'Bordo Blanco', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4666', '170003', 'Ninguno [Agroindustria Campus Sofimar]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4667', '170008', 'Fuentezuelas', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4668', '170010', 'La Laja', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4669', '170012', 'Rancho los Naranjos', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4670', '170016', 'San Nicolás', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4671', '170017', 'Santillán', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4672', '170020', 'La Tortuga', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4673', '170021', 'La Trinidad', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4674', '170023', 'San Jorge', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4675', '170026', 'Rancho Guadalupe', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4676', '170027', 'La Peña', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4677', '170028', 'Granjas Residenciales de Tequisquiapan', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4678', '170030', 'Rancho el Salitrillo', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4679', '170034', 'Rancho la Virgen', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4680', '170036', 'La Soledad', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4681', '170037', 'Haciendas de Tequisquiapan [Residencial]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4682', '170039', 'Los Viñedos [Residencial]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4683', '170040', 'Rancho Torre Blanca', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4684', '170041', 'Santa María del Camino', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4685', '170043', 'Rancho El Rocío', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4686', '170044', 'El Llano (Pozo Número Dos)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4687', '170045', 'Paso de Tablas', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4688', '170046', 'Rancho Cañada de la Virgen', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4689', '170047', 'El Gavillero', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4690', '170048', 'Los Lobos', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4691', '170049', 'Las Urracas', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4692', '170050', 'La Gloria', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4693', '170051', 'Las Ánimas', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4694', '170052', 'Valle San Juan', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4695', '170053', 'Fidel Velázquez [Centro Recreativo]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4696', '170055', 'Rancho el Yodio', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4697', '170056', 'Localidad Sin nombre (Familia Trejo Pérez)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4698', '170059', 'Cerro Partido', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4699', '170060', 'La Ciénega (El Shagid)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4700', '170061', 'Las Cuevas [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4701', '170062', 'El Huesito [Establo]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4702', '170063', 'La Presita [Establo]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4703', '170064', 'San Martín [Ganadería]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4704', '170065', 'Ahorcadito [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4705', '170066', 'Tepetates [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4706', '170067', 'Bandolón [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4707', '170068', 'El Arenal [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4708', '170069', 'El Diecinueve [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4709', '170070', 'El Pirul [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4710', '170072', 'El Tecolote [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4711', '170073', 'Las Artonas [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4712', '170074', 'Mesa Uno [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4713', '170075', 'Mesa Dos [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4714', '170076', 'Los Finitos [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4715', '170077', 'Prados [Granja]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4716', '170078', 'La Lajita (Lajitas)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4717', '170079', 'Los Lobos', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4718', '170080', 'El Oasis (Rancho San Agustín)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4719', '170081', 'Rancho el Paraíso Inn', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4720', '170082', 'Peña Blanca', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4721', '170083', 'El Llano (Pozo Número Uno)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4722', '170085', 'Quinta Rojas (Las Quintas)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4723', '170086', 'Rancho el Refugio', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4724', '170087', 'Rancho el Zapote', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4725', '170088', 'Rancho la Carolina', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4726', '170089', 'Rancho la Laja', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4727', '170091', 'Rancho la Parada', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4728', '170092', 'Rancho la Providencia (Saborex de México)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4729', '170093', 'Rancho los Juanes', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4730', '170095', 'Rancho Monserrat', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4731', '170096', 'Rancho San Gabriel', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4732', '170097', 'Rancho San Francisco Javier', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4733', '170098', 'Rancho San José', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4734', '170099', 'Rancho San Miguel', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4735', '170100', 'Rancho los Pastores', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4736', '170102', 'Ejido San Nicolás', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4737', '170103', 'Rancho Santa Rosa', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4738', '170104', 'Rancho el Cardonal', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4739', '170105', 'Familia Ramírez (Ejido de la Fuente)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4740', '170106', 'Localidad sin Nombre (El Baldío)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4741', '170107', 'Ramas Blancas', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4742', '170108', 'Rancho del Pablito', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4743', '170109', 'Familia Ramírez', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4744', '170110', 'El Potrerito', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4745', '170112', 'Los Lobos (Pérez Moreno)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4746', '170113', 'Los Tovar', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4747', '170114', 'Rancho Vista Hermosa', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4748', '170115', 'Rancho los Rosales', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4749', '170116', 'El Crucero', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4750', '170117', 'Granja la Morita', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4751', '170118', 'Rancho el Escuchador', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4752', '170119', 'Centro de Distribución Genética Porcina', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4753', '170120', 'Gas Modelo de Jilotepec', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4754', '170121', 'Granja el Huerto', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4755', '170122', 'Granja el Rincón', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4756', '170123', 'Granja los Ángeles', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4757', '170124', 'Granja los Finitos Sitio Uno', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4758', '170125', 'El Hueso', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4759', '170126', 'El Potrillo', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4760', '170127', 'Rancho el Batán', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4761', '170128', 'Rancho la Borrega', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4762', '170129', 'Rancho los Venados', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4763', '170130', 'La Esperanza', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4764', '170131', 'Familia Aroca', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4765', '170132', 'Ninguno', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4766', '170133', 'Familia Prado Valencia', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4767', '170134', 'Familia Carbajal', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4768', '170136', 'Sitio Dos Destete y Desarrollo', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4769', '170137', 'Sitio Uno Maternidad Porcina', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4770', '170138', 'Ampliación Adolfo López Mateos', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4771', '170139', 'Arroyo Blanco', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4772', '170140', 'Casa de Matanza Municipal', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4773', '170141', 'Club de Golf Tequisquiapan', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4774', '170142', 'Fátima (Ejido de Fuentezuelas)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4775', '170143', 'La Ermita', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4776', '170145', 'Familia Ángeles', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4777', '170147', 'El Hoyo del Tepetate', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4778', '170148', 'El Llano (Pozo Número Cinco)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4779', '170149', 'El Llano (Pozo Número Cuatro)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4780', '170150', 'El Llano (Pozo Número Tres)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4781', '170151', 'Pozo La Calera', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4782', '170153', 'Rancho los Dragones', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4783', '170154', 'Rancho Montecristo', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4784', '170155', 'El Salado', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4785', '170156', 'Familia Chávez Ochoa', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4786', '170157', 'Familia Dorantes Nieto', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4787', '170159', 'Tiradero de Resíduos Sólidos', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4788', '170161', 'Ninguno [Banco las Cenizas]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4789', '170162', 'Familia González Martínez', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4790', '170163', 'Potrerillos [Lienzo Charro]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4791', '170164', 'Sección Este de Tequisquiapan', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4792', '170165', 'Sección Sureste de Tequisquiapan', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4793', '170166', 'La Fuenta Sección Suroeste', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4794', '170167', 'Villa Papón', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4795', '170168', 'Fuentezuelas Sección Poniente', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4796', '170169', 'Camino al Ciervo (Familia Benítez Ordoñez)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4797', '170170', 'Camino al Ciervo (Familia Reséndiz Pérez)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4798', '170171', 'Casa Blanca', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4799', '170173', 'Ejido el Sauz (Familia Cruz Hurtado)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4800', '170174', 'Ejido el Sauz (J. Guadalupe Pérez)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4801', '170175', 'Familia Hernández López', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4802', '170176', 'Familia Jiménez Camacho', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4803', '170177', 'Familia Valencia Aranda', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4804', '170179', 'Familia Ugalde Domínguez', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4805', '170182', 'El Llano (Pozo número seis)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4806', '170183', 'Rancho San Isidro', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4807', '170184', 'El Vivero', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4808', '170186', 'Ninguno [Sección Suroeste de Tequisquiapan]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4809', '170187', 'El Arenal', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4810', '170191', 'La Bolita', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4811', '170192', 'La Cuevita', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4812', '170193', 'Lomita de Bordo Blanco', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4813', '170194', 'Sección Noroeste de Bordo Blanco', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4814', '170195', 'Sección Noroeste de la Fuente', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4815', '170197', 'Termas del Rey [Fraccionamiento]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4816', '170198', 'La Espíndola', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4817', '170199', 'Ejido el Carmen (Familia Jiménez)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4818', '170200', 'Paraíso Escondido', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4819', '170203', 'Nahui Ollin [Fraccionamiento]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4820', '170204', 'Real del Ciervo [Fraccionamiento Campestre]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4821', '170205', 'Las Lagunitas', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4822', '170208', 'El Arenal', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4823', '170209', 'Sección Norte del Tejocote', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4824', '170210', 'La Soledad', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4825', '170211', 'Jardines de Bordo Blanco', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4826', '170212', 'Sección Este de Bordo Blanco', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4827', '170213', 'Alma de Agua', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4828', '170214', 'Congregación de la Pasión (Misioneros Pasionistas)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4829', '170215', 'Ejido Fuentezuelas (Familia Chacón Ramírez)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4830', '170216', 'El Llano (Pozo Número 6)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4831', '170217', 'El Llano [Colonia]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4832', '170218', 'El Sauz (Las Morelianas)', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4833', '170219', 'Hacienda Grande', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4834', '170220', 'La Charretera', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4835', '170221', 'Ninguno [Norte de Bordo Blanco]', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4836', '170222', 'Nueva Ampliación López Mateos', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4837', '170223', 'Potrero Peña Blanca', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4838', '170224', 'Rancho Ciprés', '17');
INSERT INTO "public"."cs_localidades" VALUES ('4839', '180013', 'Corralitos', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4840', '180020', 'García', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4841', '180024', 'El Jabalí', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4842', '180042', 'Ronquillo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4843', '180060', 'Zapote de los Uribe (El Zapote)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4844', '180066', 'El Madroño', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4845', '180081', 'El Saucito', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4846', '180084', 'El Sabino (La Guayaba)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4847', '180106', 'El Puertecito (Zapote de los Uribe)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4848', '180129', 'La Presita', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4849', '180149', 'La Víbora', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4850', '180160', 'Linda Vista', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4851', '180002', 'Adjuntillas', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4852', '180004', 'Bomintzá', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4853', '180006', 'La Cañada', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4854', '180008', 'Carrizalillo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4855', '180009', 'Casa Blanca', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4856', '180010', 'Barrio de Casas Viejas', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4857', '180011', 'Rancho el Cedazo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4858', '180012', 'El Cerrito Parado', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4859', '180014', 'Las Crucitas', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4860', '180016', 'El Chilar', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4861', '180017', 'Derramadero', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4862', '180018', 'Don Lucas', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4863', '180019', 'La Estancia', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4864', '180021', 'Granjeno', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4865', '180022', 'Gudiños', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4866', '180025', 'Rancho de Guadalupe', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4867', '180027', 'Maguey Manso', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4868', '180028', 'Manantial', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4869', '180029', 'La Matamba Uno', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4870', '180030', 'Mesa de Ramírez', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4871', '180032', 'Las Moras', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4872', '180033', 'Nogales', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4873', '180035', 'Panales', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4874', '180036', 'El Patol', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4875', '180037', 'Peña Blanca', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4876', '180039', 'Puerto Blanco', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4877', '180040', 'Rancho Nuevo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4878', '180043', 'Sabino de San Ambrosio', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4879', '180047', 'San Antonio de la Cal', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4880', '180049', 'San Miguel', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4881', '180050', 'San Pablo Tolimán', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4882', '180054', 'Tequesquite (Chalma)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4883', '180055', 'El Terrero', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4884', '180056', 'Tierra Volteada', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4885', '180057', 'La Vereda', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4886', '180058', 'Rancho Viejo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4887', '180061', 'El Zapote', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4888', '180063', 'Bermejo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4889', '180067', 'Mesa de Chagoya', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4890', '180069', 'Lindero', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4891', '180070', 'Diezmeros', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4892', '180072', 'La Cebolleta', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4893', '180074', 'Barrio de García', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4894', '180083', 'Los Rincones (San Pedro de los Eucaliptos)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4895', '180086', 'La Peña', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4896', '180092', 'Tuna Manza', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4897', '180099', 'Las Cuatas', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4898', '180104', 'Longo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4899', '180116', 'Los Rincones (Casas Viejas)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4900', '180117', 'San Pedro de los Eucaliptos', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4901', '180118', 'La Matamba Dos', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4902', '180119', 'El Tule', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4903', '180123', 'La Puerta', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4904', '180124', 'Ciprés', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4905', '180125', 'La Cuchara', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4906', '180126', 'Los González', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4907', '180127', 'Loma de Casa Blanca', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4908', '180128', 'El Pedregal', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4909', '180134', 'Familia Dimas', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4910', '180142', 'Nueva el Granjeno', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4911', '180145', 'El Mezquite', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4912', '180146', 'El Shaminal', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4913', '180147', 'El Terremote', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4914', '180158', 'Familia Ramos', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4915', '180001', 'Tolimán', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4916', '180003', 'Agua Fría', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4917', '180005', 'Rancho Buenavista', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4918', '180023', 'Horno de Cal', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4919', '180031', 'El Molino', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4920', '180034', 'Ojo de Agua', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4921', '180052', 'El Sauz', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4922', '180064', 'El Aguacate', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4923', '180075', 'La Carreta', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4924', '180078', 'El Sauz', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4925', '180079', 'Puerto del Aire', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4926', '180080', 'El Encinal', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4927', '180082', 'El Sauz', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4928', '180085', 'Xihti', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4929', '180087', 'Tuna Mantza (El Sabino)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4930', '180089', 'Ninguno [Incubadora Ojo Claro]', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4931', '180090', 'Rancho la Yerbabuena', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4932', '180091', 'Rancho los Olvera', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4933', '180093', 'El Cardón', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4934', '180094', 'La Redonda', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4935', '180095', 'Laguna de Álvarez', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4936', '180096', 'La Colonia Campesina', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4937', '180097', 'Las Canoas', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4938', '180098', 'Los Cuartos (El Salado)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4939', '180100', 'Las Cuevitas (El Pilón)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4940', '180101', 'La Era', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4941', '180102', 'La Gerenta', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4942', '180103', 'Ninguno [Granja]', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4943', '180105', 'Los Pilares', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4944', '180107', 'Rancho el Cascabel', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4945', '180108', 'Rancho el Patol', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4946', '180109', 'Rancho el Patol', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4947', '180111', 'Rancho las Mujercitas', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4948', '180112', 'Rancho los luque', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4949', '180113', 'Rancho San Agustín de Verónica', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4950', '180114', 'Rancho Santa Fe', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4951', '180115', 'Rancho Torreón', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4952', '180120', 'El Zapote', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4953', '180121', 'El Arte', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4954', '180130', 'El Puerto del Álamo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4955', '180131', 'La Loma', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4956', '180132', 'La Brígida', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4957', '180133', 'La Estancia', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4958', '180135', 'Jesús Conde', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4959', '180136', 'Rancho el Magueyal', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4960', '180137', 'Rancho los Garambullos', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4961', '180138', 'El Sabinal', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4962', '180139', 'Sección Noroeste de San Pablo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4963', '180140', 'El Limón', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4964', '180141', 'El Salto', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4965', '180143', 'Corrales el Fuerte', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4966', '180144', 'El Eucalipto (Familia Botello)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4967', '180148', 'La Lezna', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4968', '180150', 'Rancho Arco Iris', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4969', '180151', 'Rancho Real de Santa María', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4970', '180152', 'Rancho Santa Teresa', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4971', '180153', 'Las Palmas', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4972', '180154', 'Alamitos (La Higuera)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4973', '180155', 'Buenavista', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4974', '180156', 'El Arte', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4975', '180157', 'Familia Hernández', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4976', '180159', 'Finca el Encanto', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4977', '180161', 'Rancho el Papalote', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4978', '180162', 'Sección Noreste de San Pablo', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4979', '180163', 'Rancho la Ponderosa', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4980', '180164', 'Rancho la Ponderos', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4981', '180165', 'El Arroyo del Sabino', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4982', '180166', 'El Ejido (Familia Sánchez Sánchez)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4983', '180167', 'El Rincón (Familia Martínez Martínez)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4984', '180168', 'Familia Montes', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4985', '180169', 'La Concepción', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4986', '180170', 'La Peñita', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4987', '180171', 'Peña Blanca (Familia Granados de la Cruz)', '18');
INSERT INTO "public"."cs_localidades" VALUES ('4988', '180172', 'Rancho la Ciénega', '18');

-- ----------------------------
-- Table structure for cs_material
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_material";
CREATE TABLE "public"."cs_material" (
"id" int4 DEFAULT nextval('material_id_seq'::regclass) NOT NULL,
"tipo_material" varchar(64) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_material
-- ----------------------------
INSERT INTO "public"."cs_material" VALUES ('1', 'Difusion');
INSERT INTO "public"."cs_material" VALUES ('2', 'Capacitacion');

-- ----------------------------
-- Table structure for cs_materiales_difusion
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_materiales_difusion";
CREATE TABLE "public"."cs_materiales_difusion" (
"id" int4 DEFAULT nextval('materiales_difusion_id_seq'::regclass) NOT NULL,
"material" varchar(150) COLLATE "default",
"tipo" varchar(150) COLLATE "default",
"status" int2,
"updated" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_materiales_difusion
-- ----------------------------
INSERT INTO "public"."cs_materiales_difusion" VALUES ('3', 'CARTELES', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('4', 'TRIPTICO', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('5', 'FOLLETOS', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('6', 'GUIAS Y MANUALES', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('7', 'RADIO', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('8', 'PERIFONEO', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('9', 'TELEVISION', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('10', 'VIDEO', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('11', 'INTERNET', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('12', 'PERIODICO MURAL', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('13', 'PINTA DE BARDA', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('14', 'MANTA ', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('15', 'ESPECTACULARES', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('16', 'SAMBLEAS COMUNITARIAS', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('17', 'INTERNET', 'DE DIFUSION', '1', '2018-03-10 23:11:33');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('18', 'CARTELES', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('19', 'TRIPTICO', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('20', 'FOLLETOS', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('21', 'GUIAS Y MANUALES', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('22', 'RADIO', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('23', 'PERIFONEO', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('24', 'TELEVISION', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('25', 'VIDEO', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('26', 'INTERNET', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('27', 'PERIODICO MURAL', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('28', 'PINTA DE BARDA', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('29', 'MANTA ', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('30', 'ESPECTACULARES', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('31', 'SAMBLEAS COMUNITARIAS', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('32', 'INTERNET', 'A DISTRIBUCION', '1', '2018-03-10 23:16:31');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('33', 'GUIA PARA SERVIDORES PUBLICOS', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('34', 'GUIA PARA BENEFICIARIOS', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('35', 'GUIA PARA CIUDADANOS', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('36', 'GUIA PARA INTEGRANTESD E COMITÉ', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('37', 'MANUAL PARA SERVIDORES PUBLICOS', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('38', 'MANUAL PARA BENEFICIARIOS', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('39', 'MANUAL PARA CIUDADANOS', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('40', 'MANUAL PARA INTEGRANTESD E COMITÉ', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('41', 'FOLLETOS', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('42', 'RADIO', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('43', 'PERIFONEO', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('44', 'TELEVISION', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('45', 'VIDEOS', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('46', 'INTERNET', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('47', 'CD', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:16');
INSERT INTO "public"."cs_materiales_difusion" VALUES ('48', 'PRESENTACION DE POWER POINT', 'MATERIAL DE CAPACITACION', '1', '2018-03-10 23:18:17');

-- ----------------------------
-- Table structure for cs_municipios
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_municipios";
CREATE TABLE "public"."cs_municipios" (
"id" int4 DEFAULT nextval('municipios_id_seq'::regclass) NOT NULL,
"municipio" varchar(120) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_municipios
-- ----------------------------
INSERT INTO "public"."cs_municipios" VALUES ('1', 'Amealco de Bonfil');
INSERT INTO "public"."cs_municipios" VALUES ('2', 'Pinal de Amoles');
INSERT INTO "public"."cs_municipios" VALUES ('3', 'Arroyo Seco');
INSERT INTO "public"."cs_municipios" VALUES ('4', 'Cadereyta de Montes');
INSERT INTO "public"."cs_municipios" VALUES ('5', 'Colón');
INSERT INTO "public"."cs_municipios" VALUES ('6', 'Corregidora');
INSERT INTO "public"."cs_municipios" VALUES ('7', 'Ezequiel Montes');
INSERT INTO "public"."cs_municipios" VALUES ('8', 'Huimilpan');
INSERT INTO "public"."cs_municipios" VALUES ('9', 'Jalpan de Serra');
INSERT INTO "public"."cs_municipios" VALUES ('10', 'Landa de Matamoros');
INSERT INTO "public"."cs_municipios" VALUES ('11', 'El Marqués');
INSERT INTO "public"."cs_municipios" VALUES ('12', 'Pedro Escobedo');
INSERT INTO "public"."cs_municipios" VALUES ('13', 'Peñamiller');
INSERT INTO "public"."cs_municipios" VALUES ('14', 'Querétaro');
INSERT INTO "public"."cs_municipios" VALUES ('15', 'San Joaquín');
INSERT INTO "public"."cs_municipios" VALUES ('16', 'San Juan del Río');
INSERT INTO "public"."cs_municipios" VALUES ('17', 'Tequisquiapan');
INSERT INTO "public"."cs_municipios" VALUES ('18', 'Tolimán');

-- ----------------------------
-- Table structure for cs_programas
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_programas";
CREATE TABLE "public"."cs_programas" (
"id" int4 DEFAULT nextval('programas_id_seq'::regclass) NOT NULL,
"programa" varchar(120) COLLATE "default",
"descripcion" varchar(300) COLLATE "default",
"fk_recurso" int4,
"validado" int2,
"año" varchar(10) COLLATE "default",
"updated" timestamp(6) DEFAULT now() NOT NULL,
"status" int2,
"who_updated" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_programas
-- ----------------------------
INSERT INTO "public"."cs_programas" VALUES ('1', 'BECAS DE MANUTENCIÓN', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('2', 'COMUNIDAD DIFERENTE (PROYECTOS PRODUCTIVOS)', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('3', 'COMUNIDAD DIFERENTE (SUBPROGRAMA DE INFRAESTRUCTURA, REHABILITACIÓN Y/O EQUIPAMIENTO DE ESPACIOS ALIMENTARIOS', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('4', 'ESCUELAS AL CIEN', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('5', 'FORTALECIMIENTO A LA ATENCIÓN MÉDICA', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('6', 'PROGRAMA DE AGUA POTABLE, DRENAJE Y TRATAMIENTO (PROAGUA)', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('7', 'PROGRAMA DE APOYO AL EMPLEO (PAE) ', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('8', 'PROGRAMA DE DEVOLUCIÓN DE DERECHOS (PRODDER)', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('9', 'PROGRAMA DE INFRAESTRUCTURA INDÍGENA (PROII)', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('10', 'PROGRAMA DE RESCATE DE ESPACIOS PÚBLICOS (PREP)', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('11', 'PROGRAMA DE TRATAMIENTO DE AGUAS RESIDUALES (PROSAN)', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('12', 'PROGRAMA PARA MEJORAMIENTO DE LA PRODUCCIÓN Y PRODUCTIVIDAD INDÍGENA (PROIN)', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('13', 'PROGRAMA PREVENCIÓN DE RIESGOS', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('14', 'PROSPERA', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('15', 'RAMO 23', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('16', '3X1 PARA MIGRANTES', null, '1', '1', '2018', '2018-03-18 07:22:50', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('17', 'ALIMENTARIOS', null, '2', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('18', 'FONDO DE APORTACIONES MÚLTIPLES (FAM)', null, '2', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('19', 'FONDO DE APORTACIONES PARA EL FORTALECIMIENTO DE LAS ENTIDADES FEDERATIVAS (FAFEF)', null, '2', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('20', 'FONDO DE APORTACIONES PARA LA INFRAESTRUCTURA SOCIAL (FAIS)', null, '2', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('21', 'FONDO DE INFRAESTRUCTURA SOCIAL ESTATAL (FISE)', null, '1', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('22', 'GEQ', null, '1', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('23', 'HOMBRO CON HOMBRO POR UNA VIVIENDA DIGNA', null, '2', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('24', 'HOMBRO CON HOMBRO TU CALLE', null, '2', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('25', 'PATCS SESEQ', null, '2', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('26', 'PATCS USEBEQ', null, '2', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('27', 'PROGRAMA ESPECIAL CEA', null, '2', '1', '2018', '2018-03-18 07:23:19', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('28', 'HOMBRO CON HOMBRO PROYECTOS PRODUCTIVOS', null, '2', '1', null, '2018-04-11 13:11:31', '1', null);
INSERT INTO "public"."cs_programas" VALUES ('30', 'otro', null, '1', null, '2018', '2018-07-25 10:43:49.098661', null, '2');
INSERT INTO "public"."cs_programas" VALUES ('31', 'wwww3', null, '2', null, '2018', '2018-07-25 10:44:27.491608', null, '2');
INSERT INTO "public"."cs_programas" VALUES ('32', 'jjjjj', null, '1', null, '2018', '2018-07-25 10:45:14.74747', null, '2');

-- ----------------------------
-- Table structure for cs_proyecto_status
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_proyecto_status";
CREATE TABLE "public"."cs_proyecto_status" (
"id" int4 NOT NULL,
"proyecto_status" varchar(150) COLLATE "default",
"updated" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_proyecto_status
-- ----------------------------
INSERT INTO "public"."cs_proyecto_status" VALUES ('1', 'INICIADO', '2018-03-10 18:31:29');
INSERT INTO "public"."cs_proyecto_status" VALUES ('2', 'CANCELADO', '2018-03-10 18:31:32');
INSERT INTO "public"."cs_proyecto_status" VALUES ('3', 'TERMINADO', '2018-03-10 18:31:36');

-- ----------------------------
-- Table structure for cs_ramos
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_ramos";
CREATE TABLE "public"."cs_ramos" (
"id" int4 DEFAULT nextval('ramos_id_seq'::regclass) NOT NULL,
"ramo" varchar(30) COLLATE "default",
"descripcion" varchar(200) COLLATE "default",
"status" int2,
"updated" date
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_ramos
-- ----------------------------

-- ----------------------------
-- Table structure for cs_recursos
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_recursos";
CREATE TABLE "public"."cs_recursos" (
"id" int4 DEFAULT nextval('recursos_id_seq'::regclass) NOT NULL,
"nombre" varchar(64) COLLATE "default",
"status" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_recursos
-- ----------------------------
INSERT INTO "public"."cs_recursos" VALUES ('1', 'FEDERAL', '1');
INSERT INTO "public"."cs_recursos" VALUES ('2', 'ESTATAL', '1');

-- ----------------------------
-- Table structure for cs_registro_capacitacion
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_capacitacion";
CREATE TABLE "public"."cs_registro_capacitacion" (
"id" int4 DEFAULT nextval('registro_capacitacion_id_seq'::regclass) NOT NULL,
"nombre_capacitacion" varchar(300) COLLATE "default",
"fk_tematica" int4,
"fk_figura_capacitada" int4,
"entidad_federativa" varchar(64) COLLATE "default",
"fecha_imparticion" date,
"oficio_aprobacion" varchar(10) COLLATE "default",
"esquema_normativo" varchar(10) COLLATE "default",
"fk_municipio" int4,
"fk_localidad" int4,
"num_participantes" int4,
"lista_participantes" varchar(200) COLLATE "default",
"beneficiarios_hombres_capacitados" int4,
"beneficiarios_mujeres_capacitados" int4,
"integrantes_hombres_capacitados" int4,
"integrantes_mujeres_capacitados" int4,
"fk_programa" int4,
"fk_figura_capacitada_otro" int4,
"fk_tematica_otro" int4,
"servidor_publico_capacito" varchar(200) COLLATE "default",
"secon_participo" varchar(20) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_capacitacion
-- ----------------------------

-- ----------------------------
-- Table structure for cs_registro_comite
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_comite";
CREATE TABLE "public"."cs_registro_comite" (
"id" int4 DEFAULT nextval('registro_comite_id_seq'::regclass) NOT NULL,
"nombre_comite" varchar(300) COLLATE "default",
"fecha_constitucion" date,
"clave_registro" varchar(100) COLLATE "default",
"accion_conexion" varchar(200) COLLATE "default",
"funciones_comite" varchar(2000) COLLATE "default",
"proporciono_resumen" varchar(10) COLLATE "default",
"disposicion_quejas" varchar(10) COLLATE "default",
"servidor_publico_constancia" varchar(200) COLLATE "default",
"cargo_servidor_publico" varchar(200) COLLATE "default",
"acta_asamblea" varchar(200) COLLATE "default",
"constancia_firmada" varchar(200) COLLATE "default",
"escrito_libre" varchar(200) COLLATE "default",
"comentarios" varchar(2000) COLLATE "default",
"fk_programa" int4,
"fk_departamento" int4,
"qty_hombres" int4,
"qty_mujeres" int4,
"guardado_oficial" int4,
"eliminado" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_comite
-- ----------------------------
INSERT INTO "public"."cs_registro_comite" VALUES ('13', 'prueba comite 1 -cea', '2018-07-26', 'c1234', '25', '18,19', null, null, 'kfkfkfk', 'kdkdkd', null, null, null, 'ddd', '16', '8', '0', '1', null, null);
INSERT INTO "public"."cs_registro_comite" VALUES ('14', 'comite 2  - cea - riesgos', '2018-07-12', 'c3222', '32', null, null, null, 'ddd2222', 'd2', null, null, null, 'ddd', '13', '9', '0', '1', null, null);
INSERT INTO "public"."cs_registro_comite" VALUES ('17', 'nuevo comite ceiq - GEQ', '2018-07-04', 'd33', '31', '22,23', null, null, 'e333', '333', null, null, null, 'dd', '22', '8', '0', '1', null, null);
INSERT INTO "public"."cs_registro_comite" VALUES ('18', 'prueba comite cea', '2018-08-02', 'jdjdjd222', null, '18,19', null, null, 'djdjd22', 'cargo', null, null, null, 'jdjdjd', '13', '9', '1', '0', null, null);

-- ----------------------------
-- Table structure for cs_registro_comite_integrantes
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_comite_integrantes";
CREATE TABLE "public"."cs_registro_comite_integrantes" (
"id" int4 DEFAULT nextval('registro_comite_integrantes_id_seq'::regclass) NOT NULL,
"fk_comite" int4 NOT NULL,
"nombre_integrante" varchar(150) COLLATE "default",
"appaterno_integrante" varchar(150) COLLATE "default",
"apmaterno_integrante" varchar(150) COLLATE "default",
"sexo_integrante" varchar(10) COLLATE "default",
"edad_integrante" varchar(10) COLLATE "default",
"cargo_integrante" varchar(100) COLLATE "default",
"firma_constancia_registro_integrante" varchar(100) COLLATE "default",
"domicilio_conocido_integrante" varchar(10) COLLATE "default",
"calle_integrante" varchar(200) COLLATE "default",
"numero_integrante" varchar(100) COLLATE "default",
"colonia_integrante" varchar(150) COLLATE "default",
"cpostal_integrante" varchar(10) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_comite_integrantes
-- ----------------------------
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('50', '9', 'constantino', 'tino', 'rikin canayinnnn', 'Masculino', '', 'Tesorero', 'SI', 'SI', null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('51', '9', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('52', '9', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('53', '9', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('54', '9', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('55', '9', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('56', '9', '', '', '', null, '', 'Seleccione..', null, null, null, null, null, null);
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('57', '10', 'AMERICA', 'TEODORO', '', 'Masculino', '', 'Tesorero', 'SI', 'SI', null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('58', '10', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('59', '10', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('60', '10', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('61', '10', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('62', '10', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('63', '10', '', '', '', null, '', 'Seleccione..', null, null, null, null, null, null);
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('71', '11', 'HOMBRO ', 'CON HOMBRO ', 'PROYECTOS PRODUCTIVOS', 'Femenino', '', 'Tesorero', 'SI', 'SI', null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('72', '11', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('73', '11', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('74', '11', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('75', '11', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('76', '11', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('77', '11', '', '', '', null, '', 'Seleccione..', null, null, null, null, null, null);
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('78', '12', 'PROGRAMA PREVENCIÓN DE RIESGOS', 'PROGRAMA PREVENCIÓN DE RIESGOS', 'PROGRAMA PREVENCIÓN DE RIESGOS', 'Femenino', '', 'Tesorero', 'SI', 'SI', null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('79', '12', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('80', '12', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('81', '12', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('82', '12', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('83', '12', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('84', '12', '', '', '', null, '', 'Seleccione..', null, null, null, null, null, null);
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('99', '16', 'nombre', 'apellido', 'apellido2', 'Masculino', '', 'Tesorero', 'NO', 'NO', null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('100', '16', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('101', '16', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('102', '16', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('103', '16', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('104', '16', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('105', '16', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('106', '13', 'integrante 1', 'constsa', 'ku', 'Femenino', '22', 'Tesorero', 'SI', 'SI', null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('107', '13', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('108', '13', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('109', '13', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('110', '13', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('111', '13', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('112', '13', '', '', '', null, '', 'Seleccione..', null, null, null, null, null, null);
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('113', '17', 'integrante', 'apellido1', 'apellido2', 'Femenino', '22', 'Tesorero', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('114', '17', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('115', '17', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('116', '17', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('117', '17', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('118', '17', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('119', '17', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('120', '14', 'nombre', 'kusu', 'apellido', 'Femenino', '22', 'Presidente', 'SI', 'SI', null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('121', '14', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('122', '14', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('123', '14', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('124', '14', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('125', '14', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('126', '14', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('127', '18', 'nombre', 'apellido', '', 'Masculino', '22', 'Presidente', 'NO', null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('128', '18', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('129', '18', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('130', '18', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('131', '18', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('132', '18', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');
INSERT INTO "public"."cs_registro_comite_integrantes" VALUES ('133', '18', '', '', '', null, '', 'Seleccione..', null, null, null, '', '', '');

-- ----------------------------
-- Table structure for cs_registro_comite_obra
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_comite_obra";
CREATE TABLE "public"."cs_registro_comite_obra" (
"id" int4 DEFAULT nextval('registro_comite_obra_id_seq'::regclass) NOT NULL,
"fk_comite" int4,
"fk_obra" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_comite_obra
-- ----------------------------
INSERT INTO "public"."cs_registro_comite_obra" VALUES ('7', '9', '19');
INSERT INTO "public"."cs_registro_comite_obra" VALUES ('9', '10', '20');
INSERT INTO "public"."cs_registro_comite_obra" VALUES ('12', '11', '21');
INSERT INTO "public"."cs_registro_comite_obra" VALUES ('14', '12', '22');
INSERT INTO "public"."cs_registro_comite_obra" VALUES ('18', '13', '25');
INSERT INTO "public"."cs_registro_comite_obra" VALUES ('19', '17', '31');
INSERT INTO "public"."cs_registro_comite_obra" VALUES ('20', '14', '32');

-- ----------------------------
-- Table structure for cs_registro_informe
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_informe";
CREATE TABLE "public"."cs_registro_informe" (
"id" int4 DEFAULT nextval('registro_informe_id_seq'::regclass) NOT NULL,
"nombre_comite" varchar(300) COLLATE "default",
"informe" varchar(100) COLLATE "default",
"apartado_informe" varchar(100) COLLATE "default",
"numero" varchar(100) COLLATE "default",
"clave_comite" varchar(100) COLLATE "default",
"nombre_comite2" varchar(300) COLLATE "default",
"fecha_captura" date,
"no_registro_css" varchar(50) COLLATE "default",
"nombre_proyecto" varchar(500) COLLATE "default",
"fecha_llenado" date,
"periodo_ejecucion" varchar(200) COLLATE "default",
"clave_entidad" varchar(100) COLLATE "default",
"clave_municipio" varchar(100) COLLATE "default",
"clave_localidad" varchar(100) COLLATE "default",
"cumplenRequisitosBenefic" varchar(4) COLLATE "default",
"igualdadHyM" varchar(4) COLLATE "default",
"ProgramaEntreOportunam" varchar(4) COLLATE "default",
"proyectoCumplePrograma" varchar(4) COLLATE "default",
"ProgramaFinPolitico" varchar(4) COLLATE "default",
"recibieronQuejas" varchar(4) COLLATE "default",
"entregaronQuejasAutoridad" varchar(4) COLLATE "default",
"recibieronQuejaAutoridadCompetente" varchar(4) COLLATE "default",
"csInformacionConocen" varchar(64) COLLATE "default",
"csActividades" varchar(64) COLLATE "default",
"csUtilidad" varchar(64) COLLATE "default",
"fk_program" int4,
"fk_subprogram" int4,
"fk_comite" int4,
"updated" date
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_informe
-- ----------------------------

-- ----------------------------
-- Table structure for cs_registro_material
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_material";
CREATE TABLE "public"."cs_registro_material" (
"id" int4 DEFAULT nextval('registro_material_id_seq'::regclass) NOT NULL,
"nombre_material" varchar(200) COLLATE "default",
"archivo_material" varchar(200) COLLATE "default",
"cantidad_producida" int4,
"fk_programa" int4,
"fk_departamento" int4,
"fk_tipo_material" int4,
"entidad" varchar(100) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_material
-- ----------------------------

-- ----------------------------
-- Table structure for cs_registro_minuta
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_minuta";
CREATE TABLE "public"."cs_registro_minuta" (
"id" int4 DEFAULT nextval('registro_minuta_id_seq'::regclass) NOT NULL,
"oficio_aprobacion" varchar(50) COLLATE "default",
"instancia" varchar(120) COLLATE "default",
"cargo" varchar(150) COLLATE "default",
"minuta_firmada" varchar(200) COLLATE "default",
"nombre_comite" varchar(200) COLLATE "default",
"numero_reunion" varchar(120) COLLATE "default",
"lugar_reunion" varchar(200) COLLATE "default",
"fecha_reunion" date,
"fk_municipio" int4,
"recibio_queja" varchar(4) COLLATE "default",
"detalles_queja" varchar(1000) COLLATE "default",
"temas_tratados" varchar(3000) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_minuta
-- ----------------------------

-- ----------------------------
-- Table structure for cs_registro_minuta_asistencia
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_minuta_asistencia";
CREATE TABLE "public"."cs_registro_minuta_asistencia" (
"id" int4 DEFAULT nextval('registro_minuta_asistencia_id_seq'::regclass) NOT NULL,
"nombre_completo" varchar(300) COLLATE "default",
"asistio" varchar(4) COLLATE "default",
"tipo" varchar(100) COLLATE "default",
"fk_minuta" int4,
"updated" date
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_minuta_asistencia
-- ----------------------------

-- ----------------------------
-- Table structure for cs_registro_programa_recursos
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_programa_recursos";
CREATE TABLE "public"."cs_registro_programa_recursos" (
"id" int4 DEFAULT nextval('registro_programa_recursos_id_seq'::regclass) NOT NULL,
"fk_resource" int4,
"fk_programa_proyectos" int4,
"fk_suprograma" int4,
"ejercicio_fiscal" varchar(255) COLLATE "default",
"presupuesto_pef" float8,
"presupuesto_vigilar_cf" float8,
"descripcion_poblacion_objetivo" varchar(4000) COLLATE "default",
"mujeres_beneficiadas" int4,
"hombres_beneficiados" int4,
"total_beneficiados" int4,
"convenio_suscrito" varchar(4) COLLATE "default",
"ramo" int4,
"nombre_ramo" varchar(40) COLLATE "default",
"instancia_primaria" int4,
"instancia_secundaria" int4,
"entidad_federativa" varchar(120) COLLATE "default",
"federal_recurso_fecha_asignacion_recurso1" date,
"federal_recurso_monto_asignado_recurso1" float8,
"federal_recurso_fuente_financiamiento_recurso1" varchar(200) COLLATE "default",
"federal_recurso_anio_recurso1" varchar(30) COLLATE "default",
"federal_recurso_fecha_asignacion_recurso2" date,
"federal_recurso_monto_asignado_recurso2" float8,
"federal_recurso_fuente_financiamiento_recurso2" varchar(200) COLLATE "default",
"federal_recurso_anio_recurso2" varchar(30) COLLATE "default",
"federal_recurso_fecha_asignacion_recurso3" date,
"federal_recurso_monto_asignado_recurso3" float8,
"federal_recurso_fuente_financiamiento_recurso3" varchar(200) COLLATE "default",
"federal_recurso_anio_recurso3" varchar(30) COLLATE "default",
"estatal_recurso_fecha_asignacion_recurso1" date,
"estatal_recurso_monto_asignado_recurso1" float8,
"estatal_recurso_fuente_financiamiento_recurso1" varchar(200) COLLATE "default",
"estatal_recurso_anio_recurso1" varchar(30) COLLATE "default",
"estatal_recurso_fecha_asignacion_recurso2" date,
"estatal_recurso_monto_asignado_recurso2" float8,
"estatal_recurso_fuente_financiamiento_recurso2" varchar(200) COLLATE "default",
"estatal_recurso_anio_recurso2" varchar(30) COLLATE "default",
"estatal_recurso_fecha_asignacion_recurso3" date,
"estatal_recurso_monto_asignado_recurso3" float8,
"estatal_recurso_fuente_financiamiento_recurso3" varchar(200) COLLATE "default",
"estatal_recurso_anio_recurso3" varchar(30) COLLATE "default",
"municipal_recurso_fecha_asignacion_recurso1" date,
"municipal_recurso_monto_asignado_recurso1" float8,
"municipal_recurso_fuente_financiamiento_recurso1" varchar(200) COLLATE "default",
"municipal_recurso_anio_recurso1" varchar(30) COLLATE "default",
"municipal_recurso_fecha_asignacion_recurso2" date,
"municipal_recurso_monto_asignado_recurso2" float8,
"municipal_recurso_fuente_financiamiento_recurso2" varchar(200) COLLATE "default",
"municipal_recurso_anio_recurso2" varchar(30) COLLATE "default",
"municipal_recurso_fecha_asignacion_recurso3" date,
"municipal_recurso_monto_asignado_recurso3" float8,
"municipal_recurso_fuente_financiamiento_recurso3" varchar(200) COLLATE "default",
"municipal_recurso_anio_recurso3" varchar(30) COLLATE "default",
"otros_recurso_fecha_asignacion_recurso1" date,
"otros_recurso_monto_asignado_recurso1" float8,
"otros_recurso_fuente_financiamiento_recurso1" varchar(200) COLLATE "default",
"otros_recurso_anio_recurso1" varchar(30) COLLATE "default",
"otros_recurso_fecha_asignacion_recurso2" date,
"otros_recurso_monto_asignado_recurso2" float8,
"otros_recurso_fuente_financiamiento_recurso2" varchar(200) COLLATE "default",
"otros_recurso_anio_recurso2" varchar(30) COLLATE "default",
"otros_recurso_fecha_asignacion_recurso3" date,
"otros_recurso_monto_asignado_recurso3" float8,
"otros_recurso_fuente_financiamiento_recurso3" varchar(200) COLLATE "default",
"otros_recurso_anio_recurso3" varchar(30) COLLATE "default",
"resumen_total_federal" float8,
"resumen_total_estatal" float8,
"resumen_total_municipal" float8,
"resumen_total_otros" float8,
"resumen_total_asignado" float8,
"fk_departamento" int4,
"updated" timestamp(6) DEFAULT now()
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_programa_recursos
-- ----------------------------
INSERT INTO "public"."cs_registro_programa_recursos" VALUES ('8', '1', '13', null, '2018', '1234566', '1000000', 'kdkdkdkd', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-08-17', '2000000', 'estatal1', '2018', null, null, '', '2018', null, null, '', '2018', null, '0', ' ', '2018', null, '0', ' ', '2018', null, '0', ' ', '2018', null, null, '', '2018', null, null, '', '2018', null, null, '', '2018', '100000000', '2000000', null, null, '102000000', '9', '2018-08-06 13:31:17.574224');

-- ----------------------------
-- Table structure for cs_registro_proyecto
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_registro_proyecto";
CREATE TABLE "public"."cs_registro_proyecto" (
"id" int4 DEFAULT nextval('registro_proyecto_id_seq'::regclass) NOT NULL,
"fk_program" int4,
"fk_subprogram" int4,
"nombre_proyecto" varchar(2000) COLLATE "default",
"fk_beneficio" int4,
"estatus_proyecto" varchar(20) COLLATE "default",
"oficio_aprobacion_picaso" varchar(10) COLLATE "default",
"oficio_aprobacion_equivalente" varchar(30) COLLATE "default",
"fk_instancia_ejecutora" int4,
"mujeres_beneficiadas" float8,
"hombres_beneficiados" float8,
"total_beneficiados" float8,
"fecha_inicio_programada" date,
"fecha_inicio_ejecucion" date,
"fecha_unica_programada" date,
"fecha_unica_ejecucion" date,
"fecha_final_programada" date,
"fecha_final_ejecucion" date,
"fk_municipio" int4,
"fk_localidad" int4,
"domicilio_conocido" varchar(20) COLLATE "default",
"calle" varchar(200) COLLATE "default",
"numero" varchar(100) COLLATE "default",
"colonia" varchar(200) COLLATE "default",
"cpostal" varchar(20) COLLATE "default",
"fecha_asignacion_recurso_federal" date,
"monto_recurso_asignado_vig_federal" float8,
"fecha_ejecucion_recurso_federal" date,
"monto_recurso_asignado_ejec_federal" float8,
"fecha_asignacion_recurso_estatal" date,
"monto_recurso_asignado_vig_estatal" float8,
"fecha_ejecucion_recurso_estatal" date,
"monto_recurso_asignado_ejec_estatal" float8,
"fecha_asignacion_recurso_municipal" date,
"monto_recurso_asignado_vig_municipal" float8,
"fecha_ejecucion_recurso_municipal" date,
"monto_recurso_asignado_ejec_municipal" float8,
"fecha_asignacion_recurso_otros" date,
"monto_recurso_asignado_vig_otros" float8,
"fecha_ejecucion_recurso_otros" date,
"monto_recurso_asignado_ejec_otros" float8,
"recurso_asignado_vigilar" float8,
"recurso_ejecutado_vigilado" float8,
"updated" timestamp(6) DEFAULT now() NOT NULL,
"comentarios_proyecto" varchar(2000) COLLATE "default",
"comentarios_calle" varchar(2000) COLLATE "default",
"fk_departamento" int4,
"guardado_oficial" int4,
"eliminado" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_registro_proyecto
-- ----------------------------
INSERT INTO "public"."cs_registro_proyecto" VALUES ('25', '16', null, 'ceiq 01', '1', 'Iniciado', 'Si', '', '19', '0', '0', '0', null, null, null, null, null, null, '0', '0', null, '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-07-30 12:25:27.451832', 'fsdfsd', '', '8', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('26', '16', null, 'ceiq obra 2', '1', 'Iniciado', 'No', '', '17', '0', '0', '0', null, null, null, null, null, null, '0', '0', null, '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-07-30 12:25:51.507854', 'sss', '', '8', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('27', '16', null, 'ceiq apoyo 1', '2', 'Iniciado', 'No', '', '12', '0', '0', '0', null, null, null, null, null, null, '0', '0', null, '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-07-30 12:26:47.912805', '', '', '8', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('28', '23', null, 'CEA OBRA 1', '1', 'Iniciado', 'Si', '', '19', '30', '30', '60', null, null, null, null, null, null, '0', '0', null, '', '', '', '', null, '0', null, '0', null, '0', null, '0', null, '0', null, '0', null, '0', null, '0', '0', '0', '2018-07-30 12:29:31.630282', 'MESS', '', '9', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('29', '23', null, 'CESA OBRA 2', '1', 'Iniciado', 'Si', '', '12', '0', '0', '0', null, null, null, null, null, null, '0', '0', null, '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-07-30 12:30:11.402125', 'FDFSDFSDFS', '', '9', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('30', '23', null, 'CEA SERVICIO 1', '3', 'Iniciado', 'Si', '', '9', '0', '0', '0', null, null, null, null, null, null, '0', '0', null, '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-07-30 12:31:22.010655', '', '', '9', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('31', '22', null, 'ceiq obra 1', '1', 'Iniciado', 'Si', '', '11', '0', '0', '0', null, null, null, null, null, null, '0', '0', null, '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-07-30 12:42:03.852948', 'dasa', '', '8', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('32', '13', null, 'obra riesgo - cea 1', '1', 'Iniciado', 'Si', 'd222', '18', '222', '333', '555', null, null, null, null, null, null, '0', '0', null, '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-07-30 14:41:47.070696', 'test', '', '9', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('33', '13', null, 'riesgo de CEIQ - apoyo 1', '2', 'Iniciado', '', '', '17', '0', '0', '0', null, null, null, null, null, null, '0', '0', null, '', '', '', '', null, '0', null, '0', null, '0', null, '0', null, '0', null, '0', null, '0', null, '0', '0', '0', '2018-07-30 15:18:54.047943', '', '', '8', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('34', '17', null, '.,.......', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-08-01 12:44:23.601642', '', null, '3', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('35', '16', null, 'lllll', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-08-01 12:46:19.875852', '', null, '3', null, null);
INSERT INTO "public"."cs_registro_proyecto" VALUES ('36', '4', null, 'gtrgfgd', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-08-01 12:53:33.762149', '', null, '3', null, null);

-- ----------------------------
-- Table structure for cs_subprogramas
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_subprogramas";
CREATE TABLE "public"."cs_subprogramas" (
"id" int4 DEFAULT nextval('subprogramas_id_seq'::regclass) NOT NULL,
"subprograma" varchar(120) COLLATE "default",
"descripcion" varchar(300) COLLATE "default",
"fk_programa" int4,
"año" varchar(10) COLLATE "default",
"validado" int2,
"updated" timestamp(6) NOT NULL,
"status" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_subprogramas
-- ----------------------------
INSERT INTO "public"."cs_subprogramas" VALUES ('1', 'APARTADO RURAL', null, '6', '2018', null, '2018-03-18 07:48:53', '1');
INSERT INTO "public"."cs_subprogramas" VALUES ('2', 'APARTADO URBANO', null, '6', '2018', null, '2018-03-18 07:48:53', '1');
INSERT INTO "public"."cs_subprogramas" VALUES ('3', 'APARTADO TRATAMIENTO', null, '6', '2018', null, '2018-03-18 07:48:53', '1');
INSERT INTO "public"."cs_subprogramas" VALUES ('4', 'BECATE', null, '7', '2018', null, '2018-03-18 07:49:31', '1');
INSERT INTO "public"."cs_subprogramas" VALUES ('5', 'FOMENTO AL AUTOEMPLEO', null, '7', '2018', null, '2018-03-18 07:49:31', '1');
INSERT INTO "public"."cs_subprogramas" VALUES ('6', 'DESAYUNOS EN CALIENTE', null, '17', '2018', null, '2018-03-18 07:50:42', '1');
INSERT INTO "public"."cs_subprogramas" VALUES ('7', 'DESAYUNOS EN FRIO', null, '17', '2018', null, '2018-03-18 07:50:42', '1');

-- ----------------------------
-- Table structure for cs_usuario_programa
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_usuario_programa";
CREATE TABLE "public"."cs_usuario_programa" (
"id" int4 DEFAULT nextval('usuario_programa_id_seq'::regclass) NOT NULL,
"fk_user" int4,
"fk_programa" int4,
"fk_subprograma" int4,
"fk_subrograma2" int4,
"updated" date DEFAULT now(),
"periodo" varchar(10) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_usuario_programa
-- ----------------------------
INSERT INTO "public"."cs_usuario_programa" VALUES ('1', '4', '24', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('2', '2', '16', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('3', '2', '24', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('4', '2', '15', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('5', '2', '3', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('6', '2', '4', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('7', '2', '6', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('8', '2', '7', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('9', '2', '10', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('10', '1', '1', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('11', '1', '2', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('12', '1', '3', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('13', '1', '4', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('14', '1', '5', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('15', '1', '6', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('16', '1', '7', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('17', '1', '8', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('18', '1', '9', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('19', '1', '10', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('20', '1', '11', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('21', '1', '12', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('22', '1', '13', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('23', '1', '14', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('24', '1', '15', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('25', '1', '16', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('26', '1', '17', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('27', '1', '24', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('28', '3', '1', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('29', '3', '2', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('30', '3', '3', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('31', '3', '4', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('32', '3', '5', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('33', '3', '6', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('34', '3', '7', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('35', '3', '8', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('36', '3', '9', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('37', '3', '10', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('38', '5', '28', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('39', '5', '23', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('40', '5', '24', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('41', '5', '9', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('42', '5', '21', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('43', '2', '29', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('44', '6', '23', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('45', '6', '16', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('46', '6', '22', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('47', '10', '6', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('48', '10', '13', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('49', '10', '23', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('50', '10', '27', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('51', '9', '13', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('52', '9', '15', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('53', '9', '22', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('53', '9', '28', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('54', '8', '4', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('55', '8', '1', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('55', '8', '7', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('57', '8', '17', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('58', '8', '18', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('59', '7', '9', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('60', '7', '17', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('61', '7', '23', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('62', '7', '24', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('63', '7', '28', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('64', '6', '26', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('65', '6', '13', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('66', '6', '8', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('67', '6', '23', null, null, null, '2018');
INSERT INTO "public"."cs_usuario_programa" VALUES ('68', '6', '28', null, null, null, '2018');

-- ----------------------------
-- Table structure for cs_usuarios
-- ----------------------------
DROP TABLE IF EXISTS "public"."cs_usuarios";
CREATE TABLE "public"."cs_usuarios" (
"id" int4 DEFAULT nextval('usuarios_id_seq'::regclass) NOT NULL,
"nombre" varchar(100) COLLATE "default",
"apPaterno" varchar(100) COLLATE "default",
"apMaterno" varchar(100) COLLATE "default",
"correo" varchar(150) COLLATE "default",
"posicion" varchar(64) COLLATE "default",
"matricula" varchar(20) COLLATE "default",
"login" varchar(20) COLLATE "default",
"password" varchar(20) COLLATE "default",
"updated" timestamp(6) DEFAULT now(),
"status" int2,
"page_startup" varchar(100) COLLATE "default",
"avatar" varchar(150) COLLATE "default",
"fk_departamento" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cs_usuarios
-- ----------------------------
INSERT INTO "public"."cs_usuarios" VALUES ('1', 'Alfonso', 'Chávez', null, 'alfonsop@mail.com', 'Director Contraloria', null, 'alfonso', 'alfonso123', null, null, '3', 'profile-pic_.jpg', '3');
INSERT INTO "public"."cs_usuarios" VALUES ('2', 'Hugo', 'Díaz', null, 'hugod@gob.mx', 'Jefe Area', null, 'hugo', 'hugo123', null, null, '2', 'profile-pic.jpg', '3');
INSERT INTO "public"."cs_usuarios" VALUES ('3', 'Ejecutora', 'Prueba', null, 'ejecutora@gmail.com', 'Ejecutora', null, 'ejecutora', 'ejecutora123', null, null, '4', 'img_avatar2.png', '1');
INSERT INTO "public"."cs_usuarios" VALUES ('4', 'Jose Luis', '/CS', null, 'empleado@email.com', 'Empleado', null, 'empleado', 'empleado123', null, null, null, 'avatar-happy.jpg', '1');
INSERT INTO "public"."cs_usuarios" VALUES ('5', 'SEDESOQ', null, null, null, 'Ejecutora', null, 'sedesoq', 'Admin', null, null, '2', 'sedesoq.jpg', '2');
INSERT INTO "public"."cs_usuarios" VALUES ('6', 'USEBEQ', null, null, 'ckusulas@gmail.com', 'Ejecutora', null, 'usebeq', 'ejecutora', null, null, null, 'usebeq.jpg', '5');
INSERT INTO "public"."cs_usuarios" VALUES ('7', 'SEDEA', null, null, 'ckusulas@gmail.com', 'Ejecutora', null, 'sedea', 'ejecutora', null, null, null, 'usebeq.jpg', '6');
INSERT INTO "public"."cs_usuarios" VALUES ('8', 'CONALEP', null, null, 'ckusulas@gmail.com', 'Ejecutora', null, 'conalep', 'ejecutora', null, null, null, 'usebeq.jpg', '7');
INSERT INTO "public"."cs_usuarios" VALUES ('9', 'CEIQ', null, null, 'ckusulas@gmail.com', 'Ejecutora', null, 'ceiq', 'ejecutora', null, null, null, 'usebeq.jpg', '8');
INSERT INTO "public"."cs_usuarios" VALUES ('10', 'CEA', null, null, 'ckusulas@gmail.com', 'Ejecutora', null, 'cea', 'ejecutora', null, null, null, 'usebeq.jpg', '9');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
